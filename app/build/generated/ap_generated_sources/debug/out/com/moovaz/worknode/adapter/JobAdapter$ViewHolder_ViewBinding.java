// Generated code from Butter Knife. Do not modify!
package com.moovaz.worknode.adapter;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.cardview.widget.CardView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.moovaz.worknode.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class JobAdapter$ViewHolder_ViewBinding implements Unbinder {
  private JobAdapter.ViewHolder target;

  @UiThread
  public JobAdapter$ViewHolder_ViewBinding(JobAdapter.ViewHolder target, View source) {
    this.target = target;

    target.card_view = Utils.findOptionalViewAsType(source, R.id.card_view, "field 'card_view'", CardView.class);
    target.jobStepAddress = Utils.findOptionalViewAsType(source, R.id.jobStepAddress, "field 'jobStepAddress'", TextView.class);
    target.jobStepStatus = Utils.findOptionalViewAsType(source, R.id.jobStepStatus, "field 'jobStepStatus'", TextView.class);
    target.jobCompanyName = Utils.findOptionalViewAsType(source, R.id.jobCompanyName, "field 'jobCompanyName'", TextView.class);
    target.jobDescription = Utils.findOptionalViewAsType(source, R.id.jobDescription, "field 'jobDescription'", TextView.class);
    target.jobReferenceNumber = Utils.findOptionalViewAsType(source, R.id.jobReferenceNumber, "field 'jobReferenceNumber'", TextView.class);
    target.order_sequence_total = Utils.findOptionalViewAsType(source, R.id.order_sequence_total, "field 'order_sequence_total'", TextView.class);
    target.layoutOrderSequence = Utils.findOptionalViewAsType(source, R.id.layoutOrderSequence, "field 'layoutOrderSequence'", LinearLayout.class);
    target.jobDropOffDate = Utils.findOptionalViewAsType(source, R.id.jobDropOffDate, "field 'jobDropOffDate'", TextView.class);
    target.extraData = Utils.findOptionalViewAsType(source, R.id.extraData, "field 'extraData'", TextView.class);
    target.driverNotes = Utils.findOptionalViewAsType(source, R.id.driverNotes, "field 'driverNotes'", TextView.class);
    target.layoutNoInternet = Utils.findOptionalViewAsType(source, R.id.layoutNoInternet, "field 'layoutNoInternet'", LinearLayout.class);
    target.textNoInternet = Utils.findOptionalViewAsType(source, R.id.textNoInternet, "field 'textNoInternet'", TextView.class);
    target.checkBoxLayout = Utils.findOptionalViewAsType(source, R.id.checkBoxLayout, "field 'checkBoxLayout'", RelativeLayout.class);
    target.jobCheckBox = Utils.findOptionalViewAsType(source, R.id.checkBox1, "field 'jobCheckBox'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    JobAdapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.card_view = null;
    target.jobStepAddress = null;
    target.jobStepStatus = null;
    target.jobCompanyName = null;
    target.jobDescription = null;
    target.jobReferenceNumber = null;
    target.order_sequence_total = null;
    target.layoutOrderSequence = null;
    target.jobDropOffDate = null;
    target.extraData = null;
    target.driverNotes = null;
    target.layoutNoInternet = null;
    target.textNoInternet = null;
    target.checkBoxLayout = null;
    target.jobCheckBox = null;
  }
}

// Generated code from Butter Knife. Do not modify!
package com.moovaz.worknode.Activity;

import Utils.TextFitTextView;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.moovaz.worknode.R;
import com.pkmmte.view.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WorkerDetailsActivity_ViewBinding implements Unbinder {
  private WorkerDetailsActivity target;

  @UiThread
  public WorkerDetailsActivity_ViewBinding(WorkerDetailsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WorkerDetailsActivity_ViewBinding(WorkerDetailsActivity target, View source) {
    this.target = target;

    target.workerName = Utils.findOptionalViewAsType(source, R.id.workerName, "field 'workerName'", TextView.class);
    target.workerPhoneNum = Utils.findOptionalViewAsType(source, R.id.workerPhoneNum, "field 'workerPhoneNum'", TextView.class);
    target.workerEmail = Utils.findOptionalViewAsType(source, R.id.workerEmail, "field 'workerEmail'", TextFitTextView.class);
    target.userProfileImage = Utils.findOptionalViewAsType(source, R.id.userProfileImage, "field 'userProfileImage'", CircularImageView.class);
    target.completedOrder = Utils.findOptionalViewAsType(source, R.id.completedOrder, "field 'completedOrder'", TextView.class);
    target.currentOrder = Utils.findOptionalViewAsType(source, R.id.currentOrder, "field 'currentOrder'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WorkerDetailsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.workerName = null;
    target.workerPhoneNum = null;
    target.workerEmail = null;
    target.userProfileImage = null;
    target.completedOrder = null;
    target.currentOrder = null;
  }
}

package com.evfy.evfytracker.gcm;

/**
 * Created by 13041243 on 19/10/2015.
 */
public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}

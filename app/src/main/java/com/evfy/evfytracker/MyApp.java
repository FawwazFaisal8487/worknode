package com.evfy.evfytracker;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.multidex.MultiDexApplication;

import com.lkh012349s.mobileprinter.Utils.LogCustom;

//

public class MyApp extends MultiDexApplication {

    final String PREFS_NAME = "MyPrefsFile";
    boolean isPushBotRegistered = false;

    public boolean isPushBotRegistered() {
        return isPushBotRegistered;
    }

    public void setIsPushBotRegistered(boolean isPushBotRegistered) {
        this.isPushBotRegistered = isPushBotRegistered;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        //final SharedPreferences settings = getSharedPreferences( PREFS_NAME, 0 );
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String userName = settings.getString("user_name", "");

        try {
//            Fabric.with(this, new Crashlytics());
        } catch (final Throwable e) {
            LogCustom.e(e);
        }

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        isPushBotRegistered = false;
    }

}

package com.evfy.evfytracker.OkHttpRequest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.evfy.evfytracker.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by coolasia on 14/12/17.
 */

public class RefreshTokenOkHttp {
    private static final OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static final MediaType MEDIA_JSON = MediaType.parse("application/json");
    private static Headers.Builder headersBuilder;
    private static final String PREFS_NAME = "MyPrefsFile";
    public static String googleToken;


    private static OkHttpClient configureClient() {
        httpClientBuilder.readTimeout(10, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(10, TimeUnit.SECONDS);
        httpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        httpClientBuilder.retryOnConnectionFailure(true);
        return httpClientBuilder.build();
    }

    private static Headers getHeaders(@NonNull Context mContext) {

        // SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);

        boolean isWMS = settings.getBoolean("isWMS", false);
        Log.i("isWMSP:", "" + isWMS);
        String app_name = "";
        if (isWMS) {
            app_name = "WMS";
        } else {
            app_name = "LDS";
        }

        headersBuilder = new Headers.Builder();
        headersBuilder.set("Content-Type", "application/json");
        headersBuilder.set("app_name", app_name);
        return headersBuilder.build();
    }


    public interface RefreshTokenOKHttpNetwork {
        void onSuccess(String body, int responseCode);

        void onFailure(String body, boolean requestCallBackError, int responseCode);
    }


    @NonNull
    public static void refreshToken(@NonNull final Context mContext, final RefreshTokenOKHttpNetwork okHttpCallBack) {

        JSONObject jsonObject = new JSONObject();

        // SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        final String userId = settings.getString("user_name", "");
        final String password = settings.getString("password", "");

        String android_id = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        String deviceModel = android.os.Build.MODEL;
        String deviceManufacturer = android.os.Build.MANUFACTURER;


        googleToken = settings.getString("googleToken", "");
        LogCustom.i("googleTokenLogout", "" + googleToken);

        if (googleToken.equalsIgnoreCase("")) {
            //			String token="";
            try {

                Log.i("tokenObject", "tokenObject");
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (task.isSuccessful()) {
                            try {
                                JSONObject tokenObject = new JSONObject(task.getResult().getToken());
                                googleToken = tokenObject.getString("token");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });


            } catch (Exception e) {

            }
        }

        //This just to make sure google token cannot be null.
        if (!settings.contains("googleToken")) {
            if (!googleToken.equalsIgnoreCase(""))
                settings.edit().putString("googleToken", googleToken).commit();
        } else {
            if (googleToken.equalsIgnoreCase("")) {
                googleToken = settings.getString("googleToken", "");
            }
        }

        try {

            Log.i("android_id", "is : " + android_id);
            jsonObject.put("email", userId)
                    .put("password", password)
                    .put("scope", "worker")
                    .put("grant_type", "password")
                    // .put("device_id","b4ac12952a2057e0")
                    .put("device_id", android_id)
                    .put("google_token", googleToken)
                    .put("device_manufacturer", deviceManufacturer)
                    .put("device_model", deviceModel)
                    .put("device_platform", "Android")
                    .put("require_design_settings", false);

        } catch (Exception e) {

            try {

            } catch (final Throwable t) {
            }

        }

        String requestJson = jsonObject.toString();


        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        Request request = new Request.Builder().headers(getHeaders(mContext)).url(Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_LOGIN).post(body).build();

        Log.e("urlrefreshToken", " " + request.url());

        execute(mContext, request, okHttpCallBack);

    }

    @NonNull
    private static void execute(@NonNull final Context mContext, @NonNull final Request request, final RefreshTokenOKHttpNetwork okHttpCallBack) {

        OkHttpClient client = configureClient();

        LogCustom.i("headers", request.headers());


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage();
                LogCustom.i("onFailureClient", mMessage);
                okHttpCallBack.onFailure(mMessage, true, 0);
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String mMessage = response.body().string();
                int responseCode = response.code();
                LogCustom.i("onResponseClient", mMessage);


                if (response.isSuccessful()) {

                    if (mMessage == null || mMessage.isEmpty()) {

                    } else {
                        try {

                            // SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);
                            final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);

                            final JSONObject data = new JSONObject(mMessage);

                            JSONObject tokenData = data.getJSONObject("token");

                            SharedPreferences.Editor editors = settings.edit();
                            editors.putString("access_token", tokenData.getString("access_token")).commit();
                            editors.putString("refresh_token", tokenData.getString("refresh_token")).commit();
                            editors.putString("googleToken", googleToken).commit();
                            editors.apply();

                            okHttpCallBack.onSuccess(mMessage, responseCode);

                        } catch (JSONException e) {

                            LogCustom.e(e);

                            try {

                            } catch (final Throwable t) {
                            }

                        }
                    }

                } else {
                    LogCustom.i("failed", "onFailure");
                    okHttpCallBack.onFailure(mMessage, false, responseCode);
                }


            }
        });


        // return mMessage;
    }
}


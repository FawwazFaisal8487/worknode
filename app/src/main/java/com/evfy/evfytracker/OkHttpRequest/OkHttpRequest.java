package com.evfy.evfytracker.OkHttpRequest;

import static com.evfy.evfytracker.Constants.SAAS_SERVER_LDS;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.RequestBuilderObject.ExtendedString;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by coolasia on 6/12/17.
 */

public class OkHttpRequest {
    private static final OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static Headers.Builder headersBuilder;
    private static final MediaType MEDIA_JSON = MediaType.parse("application/json");
    private static final MediaType MEDIA_IMAGE = MediaType.parse("image/png");
    private static final int TIMEOUT_IN_MINUTE = 3;
    private static final String PREFS_NAME = "MyPrefsFile";


    private static OkHttpClient configureClient() {
        httpClientBuilder.readTimeout(60, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(60, TimeUnit.SECONDS);
        httpClientBuilder.connectTimeout(60, TimeUnit.SECONDS);
        httpClientBuilder.retryOnConnectionFailure(true);
        return httpClientBuilder.build();
    }

    private static Headers getHeaders(@NonNull Context mContext) {

        //SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);

        boolean isWMS = settings.getBoolean("isWMS", false);
        Log.i("isWMSP:", "" + isWMS);
        String app_name = "";
        if (isWMS) {
            app_name = "WMS";
        } else {
            app_name = "LDS";
        }


        headersBuilder = new Headers.Builder();
//        headersBuilder.set("Accept", "application/json");
        headersBuilder.set("Content-Type", "application/json");
//        headersBuilder.set("X-Requested-With", "XMLHttpRequest");
        headersBuilder.set("app_name", app_name);
        return headersBuilder.build();
    }

    private static Headers getHeadersVersionPlayStore(@NonNull Context mContext) {
        headersBuilder = new Headers.Builder();
        headersBuilder.set("Content-Type", "application/json");
        return headersBuilder.build();
    }

    private static Headers getAccessHeaders(@NonNull Context mContext) {
        headersBuilder = new Headers.Builder();
        // SharedPreferences settings = mContext.getSharedPreferences(PREFS_NAME, 0);
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
        String accessToken = settings.getString("access_token", "");
        LogCustom.i("accessToken", " " + accessToken);
        headersBuilder.set("Authorization", "Bearer " + accessToken);
        headersBuilder.set("Content-Type", "application/json");
        return headersBuilder.build();
    }

    public interface OKHttpNetwork {
        void onSuccess(String body, int responseCode);

        void onFailure(String body, boolean requestCallBackError, int responseCode);
    }

    @NonNull
    public static void profileWorker(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_PROFILE;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();

        Log.e("urlprofileWorker", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, false);
    }

    @NonNull
    public static void appCompanySetting(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_APP_COMPANY_SETTINGS;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();

        Log.e("urlprofileWorker", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, false);
    }

    @NonNull
    public static void batchUpdate(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = SAAS_SERVER_LDS + Constants.API_BATCH_UPDATE;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        Log.e("urlbatchUpdate", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }


    @NonNull
    public static void fetchJobList(@NonNull final Context mContext, final String startDate,
                                    final String endDate, final String orderStatus, final String searchString, final OKHttpNetwork okHttpCallBack) {

        String requestJson = "";
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        String urlFetchJobListWithParams = ExtendedString.getString(
                Constants.API_FETCH_JOBLIST,
                new String[]{startDate, endDate, orderStatus, searchString});

        String url = SAAS_SERVER_LDS + urlFetchJobListWithParams;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();

        Log.e("urlfetchJobList", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, false);
    }

    @NonNull
    public static void fetchRejectReasons(@NonNull final Context mContext, final OKHttpNetwork okHttpCallBack) {

        String requestJson = "";
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        String url = SAAS_SERVER_LDS + "/api/reject_reasons";

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();

        execute(mContext, request, okHttpCallBack, url, body, false);
    }


    @NonNull
    public static void driverLogin(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_LOGIN;

        Request request = new Request.Builder().headers(getHeaders(mContext)).url(url).post(body).build();

        Log.e("urldriverLogin", " " + request.url());

        executeLogin(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void driverUpdateLocation(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_UPDATE_LOCATION + Constants.API_DRIVER_UPDATE_LOCATION_MINUTE;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        Log.e("driverUpdateLocation", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void driverLogout(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_LOGOUT;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        LogCustom.i("driverLogout", request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void driverUpdateProfile(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        //String url = Constants.SAAS_SERVER_AUTH+Constants.API_DRIVER_UPDATE_PROFILE;

        String url = Constants.SAAS_SERVER_UPDATE_LOCATION + Constants.API_DRIVER_UPDATE_LOCATION_MINUTE;

        // Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).put(body).build();
        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        LogCustom.i("driverUpdateProfile", request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void validateUsername(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_VALIDATE_USERNAME;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        LogCustom.i("validateUsername", request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void forgetPassword(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_FORGET_PASSWORD;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        LogCustom.i("forgetPassword", request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void resetPassword(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_DRIVER_RESET_PASSWORD;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).put(body).build();

        LogCustom.i("resetPassword", request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }

    @NonNull
    public static void getJobStepDetails(@NonNull final Context mContext, @NonNull final int jobStepId, final OKHttpNetwork okHttpCallBack) {

        String requestJson = "";
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        String urlFetchJobListWithParams = ExtendedString.getString(
                Constants.API_GET_JOB_STEP_DETAILS,
                new String[]{String.valueOf(jobStepId)});

        String url = SAAS_SERVER_LDS + urlFetchJobListWithParams;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();

        Log.e("urlgetJobStepDetails", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, false);
    }

    @NonNull
    public static void getJobOrderDetails(@NonNull final Context mContext, @NonNull final int jobOrderDetailId, final OKHttpNetwork okHttpCallBack) {

        String requestJson = "";
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        String urlFetchJobListWithParams = ExtendedString.getString(
                Constants.API_GET_JOB_ORDER_DETAILS,
                new String[]{String.valueOf(jobOrderDetailId)});

        String url = SAAS_SERVER_LDS + urlFetchJobListWithParams;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();

        Log.e("urlgetJobOrderDetails", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, false);
    }

    @NonNull
    public static void uploadImageBase64ForUrl(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = Constants.SAAS_SERVER_AUTH + Constants.API_UPLOADIMAGEBASE64FORURL;

        Request request = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(body).build();

        Log.e("urlbatchUpdate", " " + request.url());

        execute(mContext, request, okHttpCallBack, url, body, true);

    }


    /* ========== GET VERSION APP FROM PLAY STORE ======= */
    @NonNull
    public static void getVersionFromPlayStore(@NonNull final Context mContext, final String requestJson, final OKHttpNetwork okHttpCallBack) {
        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");

        Log.e("body", " " + requestJson);

        String url = "https://play.google.com/store/apps/details?id=com.evfy.evfytracker";

        Request request = new Request.Builder().headers(getHeadersVersionPlayStore(mContext)).url(url).build();

        Log.e("getVersionFromPlayStore", " " + request.url());

        executeLogin(mContext, request, okHttpCallBack, url, body, true);

    }


    /* ========== GET VERSION APP FROM PLAY STORE ======= */

    @NonNull
    private static void executeLogin(@NonNull final Context mContext, @NonNull final Request request, final OKHttpNetwork okHttpCallBack,
                                     final String url, final RequestBody bodyData, final boolean isPost) {

        OkHttpClient client = configureClient();

        LogCustom.i("headers", request.headers());
        LogCustom.i("url request", url);


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage();
                LogCustom.i("onFailureClient", mMessage);

                if (mMessage == null || mMessage.equalsIgnoreCase("")) {
                    okHttpCallBack.onFailure(mMessage, true, 0);
                } else {
                    boolean isFailedConnect = mMessage.contains("failed to connect");
                    boolean isFailedConnect2 = mMessage.contains("Unable to resolve host");
                    if (isFailedConnect || isFailedConnect2) {
                        okHttpCallBack.onFailure(mMessage, true, Constants.CANNOT_RESOLVE_HOST);

                    } else {
                        okHttpCallBack.onFailure(mMessage, true, 0);
                    }
                }

                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String mMessage = response.body().string();
                int responseCode = response.code();
                LogCustom.i("onResponseClient", mMessage);
                LogCustom.i("onResponseCode", responseCode);


                if (response.isSuccessful()) {
                    Log.i("success", "in");
                    okHttpCallBack.onSuccess(mMessage, responseCode);

                } else {
                    LogCustom.i("failed", "onFailure");
                    okHttpCallBack.onFailure(mMessage, false, responseCode);
                }


            }
        });


    }


    @NonNull
    private static void execute(@NonNull final Context mContext, @NonNull final Request request, final OKHttpNetwork okHttpCallBack,
                                final String url, final RequestBody bodyData, final boolean isPost) {

        OkHttpClient client = configureClient();

        LogCustom.i("headers", request.headers());
        LogCustom.i("url request", url);
        LogCustom.i("bodyData request", bodyData);


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage();
                LogCustom.i("onFailureClient", mMessage);

                if (mMessage == null || mMessage.equalsIgnoreCase("")) {
                    okHttpCallBack.onFailure(mMessage, true, 0);
                } else {
                    boolean isFailedConnect = mMessage.contains("failed to connect");
                    boolean isFailedConnect2 = mMessage.contains("Unable to resolve host");
                    if (isFailedConnect || isFailedConnect2) {
                        okHttpCallBack.onFailure(mMessage, true, Constants.CANNOT_RESOLVE_HOST);

                    } else {
                        okHttpCallBack.onFailure(mMessage, true, 0);
                    }
                }

                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String mMessage = response.body().string();
                int responseCode = response.code();
                LogCustom.i("onResponseClient", mMessage);
                LogCustom.i("onResponseCode", responseCode);


                if (response.isSuccessful()) {
                    Log.i("success", "in");
                    okHttpCallBack.onSuccess(mMessage, responseCode);

                } else if (response.code() == 401) {
                    okHttpCallBack.onFailure(mMessage, false, responseCode);

//                    RefreshTokenOkHttp refreshTokenOkHttp=new RefreshTokenOkHttp();
//                    try {
//                        refreshTokenOkHttp.refreshToken(mContext,new RefreshTokenOkHttp.RefreshTokenOKHttpNetwork() {
//                                    @Override
//                                    public void onSuccess(final String body, final int responseCode) {
//                                        request.newBuilder().headers(getAccessHeaders(mContext));
//                                        LogCustom.i("headersRefreshToken", request.headers());
//                                        LogCustom.i("headersRefreshToken", request.newBuilder().headers(getAccessHeaders(mContext)));
//                                        Request newRequest;
//                                        if(isPost){
//                                            newRequest = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).post(bodyData).build();
//                                        }else{
//                                            newRequest = new Request.Builder().headers(getAccessHeaders(mContext)).url(url).build();
//                                        }
//
//                                        execute(mContext,newRequest,okHttpCallBack,url,bodyData,isPost);
//                                    }
//
//                                    @Override
//                                    public void onFailure( final String body, final boolean requestCallBackError, final int responseCode) {
//
//                                        okHttpCallBack.onFailure(body,false,responseCode);
//                                    }
//                                });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                } else {
                    LogCustom.i("failed", "onFailure");
                    okHttpCallBack.onFailure(mMessage, false, responseCode);
                }


            }
        });


        // return mMessage;
    }


//@NonNull
//    private static String execute(@NonNull final Context mContext, @NonNull final Request request) {
//        String status ="";
//        try {
//            OkHttpClient client = configureClient();
//            // Execute the request and retrieve the response.
//            Response response = client.newCall(request).execute();
//            if (response.isSuccessful()) {
//                status = response.body().string().trim();
//                LogCustom.i("execute111", status);
//            }
//        } catch (@NonNull UnknownHostException | SocketTimeoutException e) {
//            e.printStackTrace();
//
//            new Handler(Looper.getMainLooper()) {
//                @Override
//                public void handleMessage(Message message) {
//                    Toast.makeText(mContext, "No internet", Toast.LENGTH_SHORT).show();
//                }
//            }.sendEmptyMessage(0);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    LogCustom.i("execute11", status);
//        return status;
//    }


//    public static String GET(@NonNull final Context mContext,@NonNull final String url,final String requestJson) throws IOException {
//        final RequestBody body = RequestBody.create(MEDIA_JSON, (null != requestJson) ? requestJson : "");
//
//        Request request = new Request.Builder().headers(getHeaders(mContext)).url(url).post(body).build();
//        Response response = httpClientBuilder.(request).execute();
//        return response.body().string();
//    }
}

package com.evfy.evfytracker;

import com.evfy.evfytracker.classes.WebService;
import com.evfy.evfytracker.classes.WebServiceWMS;

import app.juaagugui.httpService.services.RESTIntentService;

public class Constants {

    public static final String TIME_FORMAT_TIME_PICK_UP = "dd/MM/yyyy HH:mm";
    public static final String TIME_FORMAT_TIME_DROP_OFF = "dd/MM";
    public static final String TIME_FORMAT_FROM_WEB_SERVICE = "yyyy-MM-dd HH:mm:ss";
    public static final String TIME_FORMAT_DATE = "dd MMM yyyy";
    public static final String TIME_FORMAT_TIME = "hh:mm a";

    public static final int PENDING_WORKER = 1;
    public static final int DISPATCHED = 3;
    public static final int WORKER_COMFIRM = 6;
    public static final int REJECTED = 11;
    public static final int IN_PROGRESS = 4;
    //public static final int COMPLETE = 5;
    //public static final int FAILED = 10;

    public static final int INVALID_DATE = 99;
    public static final int WORKER_FOUND = 20;


    public static final int AWAITING_PICK_UP = 22;
    public static final int PICKED_UP = 12;
    public static final int AWAITING_LOADING = 13;

    public static final int AWAITING_DROP_OFF = 6;
    public static final int LAUNDRY_DROPPED_OFF = 7;
    public static final int DROP_OFF_FAILED = 11;
    public static final WebService WEB_SERVICE_BATCH_UPDATE = new WebService("/api/orders/assign/order", RESTIntentService.POST);
    public static final WebService WEB_SERVICE_CONFIRM_ORDER_PAYMENT = new WebService("/api/orders/%s/confirms_cash", RESTIntentService.POST);

    public static final WebService WEB_SERVICE_EDIT_ORDER = new WebService("/api/orders/%s", RESTIntentService.PUT);
    public static final WebService WEB_SERVICE_GET_CANCEL_REASONS = new WebService("/api/reject_reasons", RESTIntentService.GET);
    public static final WebService WEB_SERVICE_GET_NOTES = new WebService("/api/notes", RESTIntentService.GET);
    public static final WebService WEB_SERVICE_GET_RECIPIENTS = new WebService("/api/recipient_types", RESTIntentService.GET);
    public static final WebServiceWMS WEB_SERVICE_GET_COMPANY_NAME = new WebServiceWMS("/worker/api/customers", RESTIntentService.GET);


    /* ========= HOST DOMAIN =========== */

    /* ========= STAGING =========== */
//    public static final String SAAS_SERVER_AUTH = "https://devstaging-auth-api.evfy.sg";   //auth
//    public static final String SAAS_SERVER_LDS = "https://devstaging-lds-api.evfy.sg";    // lds server
//    public static final String SAAS_SERVER_WMS = "https://devstaging-wms-api.evfy.sg";    // wms server
//    public static final String SAAS_SERVER_UPDATE_LOCATION = "https://location-api.worknode.ai";    // Node JS update location
    /* ========= STAGING =========== */

    /* ========= PRODUCTION =========== */
    public static final String SAAS_SERVER_AUTH = "https://auth-api.evfy.sg";   //auth
    public static final String SAAS_SERVER_LDS = "https://lds-api.evfy.sg";    // lds server
    public static final String SAAS_SERVER_WMS = "https://wms-api.evfy.sg";    // wms server
    public static final String SAAS_SERVER_UPDATE_LOCATION = "https://location-api.worknode.ai";    // Node JS update location
    /* ========= PRODUCTION =========== */

    /* ========= HOST DOMAIN =========== */


    /* ========= URL API =========== */
    public static final String API_DRIVER_LOGIN = "/api/login";
    public static final String API_GET_JOB_STEP_DETAILS = "/api/job_steps/%s";
    public static final String API_GET_JOB_ORDER_DETAILS = "/api/orders/%s";
    public static final String API_DRIVER_LOGOUT = "/api/logout";
    public static final String API_DRIVER_UPDATE_PROFILE = "/worker/api/update_worker";
    public static final String API_BATCH_UPDATE = "/api/orders/assign/order";
    public static final String API_UPLOADIMAGEBASE64FORURL = "/api/upload_base64";
    public static final String API_DRIVER_PROFILE = "/worker/api/profile/workers";
    public static final String API_APP_COMPANY_SETTINGS = "/api/application_company_settings";
    public static final String API_DRIVER_UPDATE_LOCATION = "/worker/api/location/update";
    public static final String API_DRIVER_UPDATE_LOCATION_MINUTE = "/api/locations";
    public static final String API_FETCH_JOBLIST = "/api/workers/me/orders?sort=date&start_date=%s&end_date=%s&page=0&take=9999&pagination_by_date=true&order_status=%s&is_admin=false&search=%s";
    public static final String API_DRIVER_VALIDATE_USERNAME = "/worker/api/validate/username";
    public static final String API_DRIVER_FORGET_PASSWORD = "/worker/api/forgot/password";
    public static final String API_DRIVER_RESET_PASSWORD = "/worker/api/reset/password";
    public static final String API_DRIVER_DETAIL = "/api/";

    /* ========= URL API =========== */


    /* ========== STATUS OF JOB ======== */
    public static final int ASSIGNED = 2;
    public static final int ACKNOWLEDGED = 6;
    //public static final int ARRIVED = 16;
    public static final int COMPLETE = 5;
    public static final int SELF_COLLECT = 14;
    public static final int FAILED = 10;
    public static final int CANCELLED = 9;
    public static final int INCOMPLETED = 17;
    /* ========== STATUS OF JOB ======== */


    /* ======== LIST OF JOB STEP STATUS ========= */
    public static final int JOB_STEP_PENDING = 1;
    public static final int JOB_STEP_INPROGRESS = 2;
    public static final int JOB_STEP_COMPLETED = 3;
    public static final int JOB_STEP_FAILED = 5;
    public static final int JOB_STEP_CANCELLED = 6;
    /* ======== LIST OF JOB STEP STATUS ========= */

    /* ======== LIST OF JOB STEP STATUS NAME ========= */
    public static final String JOB_STEP_PENDING_TEXT = "Pending";
    public static final String JOB_STEP_INPROGRESS_TEXT = "In Progress";
    public static final String JOB_STEP_COMPLETED_TEXT = "Completed";
    public static final String JOB_STEP_FAILED_TEXT = "Failed";
    public static final String JOB_STEP_CANCELLED_TEXT = "Cancelled";
    /* ======== LIST OF JOB STEP STATUS NAME ========= */


    /* ========== LIST OF ORDER STATUS TEXT ======== */
    public static final String ASSIGNED_TEXT = "Assigned";
    public static final String ACKNOWLEDGED_TEXT = "Acknowledged";
    public static final String INPROGRESS_TEXT = "In Progress";
    public static final String COMPLETED_TEXT = "Completed";
    public static final String FAILED_TEXT = "Failed";
    public static final String CANCELLED_TEXT = "Cancelled";
    public static final String SELFCOLLECT_TEXT = "Self Collect";
    /* ========== LIST OF ORDER STATUS TEXT ======== */



    /* ========= GLOBAL PARAMS =========== */

    /* ====== STRING PARAMS ========= */
    public static final String PREFS_NAME = "MyPrefsFile";
    /* ====== STRING PARAMS ========= */

    /* ====== INTEGER PARAMS ======== */
    public static final int CANNOT_RESOLVE_HOST = 876;
    public static final int OPEN_INTERNET_CONNECTION = 123;
    /* ====== INTEGER PARAMS ======== */

    /* ========= GLOBAL PARAMS =========== */


    /* ========= ON ACTIVITY RESULT ========== */
    public static final int INTENT_ACTIVITY_SCAN = 100;
    public static final int INTENT_ACTIVITY_SCAN_ALL_ITEM = 1009;
    public static final int INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION = 222;
    public static final int INTENT_ACTIVITY_ONE_JOB_STEPS = 333;
    public static final int INTENT_ACTIVITY_LIST_ORDER_ATTEMPT = 444;
    public static final int INTENT_ACTIVITY_GPS_SETTINGS = 555;
    /* ========= ON ACTIVITY RESULT ========== */


    /* ========= RESPONSE CODE  ========== */

    public static final int STATUS_RESPONSE_SUCCESS = 200;

    /* ========= BAD REQUEST  ========== */
    /* This response code  because you send wrong param.. need to check error string to know what the error */
    public static final int STATUS_RESPONSE_BAD_REQUEST = 400;
    /* ========= BAD REQUEST  ========== */

    /* ========= UNAUTHORIZED  ========== */
    /* This response code  Device not found, Device is banned , Token expired/other error */
    public static final int STATUS_RESPONSE_UNAUTHORIZED = 401;
    /* ========= UNAUTHORIZED  ========== */

    /* ========= FORBIDDEN  ========== */
    /* This response code  unpaid subscription, quota reached, blacklist , other error */
    public static final int STATUS_RESPONSE_FORBIDDEN = 403;
    /* ========= FORBIDDEN  ========== */


    /* ========= NOT FOUND  ========== */
    /* This response code  because maybe the page is not found */
    public static final int STATUS_RESPONSE_NOT_FOUND = 404;
    /* ========= NOT FOUND  ========== */

    /* ========= INTERNAL SERVER ERROR  ========== */
    /* This response code  because error at server  */
    public static final int STATUS_RESPONSE_INTERNAL_SERVER_ERROR = 500;
    /* ========= INTERNAL SERVER ERROR  ========== */

    /* ========= SERVER DOWN ========== */
    public static final int STATUS_RESPONSE_SERVER_DOWN = 503;

    public static final int STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT = 504;
    /* ========= SERVER DOWN ========== */

    /* ========= RESPONSE CODE  ========== */


    /* ========= STATUS FOR PROCESS TO IDENTIFY POPUP MESSAGE  ========== */
    public static final int CANNOT_ACCEPT_JOB_MESSAGE_POPUP = 1;
    public static final int CANNOT_START_JOB_MESSAGE_POPUP = 2;
    public static final int CANNOT_COMPLETE_JOB_MESSAGE_POPUP = 3;
    public static final int CANNOT_FAIL_JOB_MESSAGE_POPUP = 4;
    /* ========= STATUS FOR PROCESS TO IDENTIFY POPUP MESSAGE  ========== */

}

package com.evfy.evfytracker.Database;//package coolasia.com.wmgworker.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.evfy.evfytracker.classes.AppCompanySettings;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.JobSteps;
import com.evfy.evfytracker.classes.NotificationDatabase;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.evfy.evfytracker.classes.OrderAttemptImage;
import com.evfy.evfytracker.classes.OrderDatabase;
import com.evfy.evfytracker.classes.OrderDetails;
import com.evfy.evfytracker.classes.OrderStatus;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHandlerJobs extends SQLiteOpenHelper {

    /* ============= DECLARATIONS =========*/

    /* ====== DATABASE ======== */
    // Database Version
    private static final int DATABASE_VERSION = 24;

    /* ========= DATABASE NAME ============= */
    private static final String DATABASE_NAME = "DBUpdatedJobs";
    /* ========= DATABASE NAME ============= */


    /* ========= DATABASE TABLE ============= */
    private static final String TABLE_NAME = "UpdatedJobs";
    private static final String TABLE_APP_COMPANY_SETTING = "AppCompanySettings";
    private static final String TABLE_STATUS_NAME = "OrderStatus";
    private static final String TABLE_NOTIFICATION = "Notification";


    private static final String TABLE_JOB_DETAILS = "JobDetails";
    private static final String TABLE_JOB_STEP_DETAILS = "JobStepDetails";
    private static final String TABLE_JOB_ITEM_DETAILS = "JobItemDetails";
    private static final String TABLE_ORDERATTEMPT_DETAILS = "OrderAttemptDetails";
    private static final String TABLE_IMAGE_DETAILS = "ImageDetails";
    private static final String TABLE_JOB_LATEST = "JobLatest";
    /* ========= DATABASE TABLE ============= */

    /* ========= COLUMNS TABLE (UPDATED JOBS) ============= */
    private static final String KEY_ID = "id";
    private static final String KEY_ORDER_ID = "order_id";
    private static final String KEY_JOB_REMOVED = "jobRemoved";
    private static final String KEY_ORDER_JSON = "order_json";
    /* ========= COLUMNS TABLE (UPDATED JOBS) ============= */


    /* ========= COLUMNS TABLE (ORDERS STATUS) ============= */
    //
    private static final String KEY_ORDER_STATUS_NAME = "order_status_name";
    private static final String KEY_ORDER_STATUS_ID = "order_status_id";
    private static final String KEY_ORDER_GENERAL_ID = "order_general_id";
    private static final String KEY_ORDER_IS_ACTIVE = "order_isActive";
    private static final String KEY_ORDER_VIEW_NAME = "order_view_name";
    /* ========= COLUMNS TABLE (ORDERS STATUS) ============= */

    /* ========= COLUMNS TABLE (APP COMPANY SETTINGS) ============= */
    private static final String KEY_ID_APP_COMPANY_SETTINGS = "idAppCompanySettings";
    private static final String KEY_RULE_APP_COMPANY_SETTINGS = "rule";
    private static final String KEY_ENABLE_APP_COMPANY_SETTINGS = "enable";

    private static final String KEY_CURRENT_STATUS_APP_COMPANY_SETTINGS = "currentStatus";
    private static final String KEY_TO_STATUS_APP_COMPANY_SETTINGS = "toStatus";

    private static final String KEY_IS_MANPOWER_WORKER_APP_COMPANY_SETTINGS = "isManPowerWorker";
    private static final String KEY_IS_SCAN_REQUIRED_APP_COMPANY_SETTINGS = "isScanRequired";
    /* ========= COLUMNS TABLE (APP COMPANY SETTINGS) ============= */

    /* ========= COLUMNS TABLE (NOTIFICATION) ============= */
    private static final String KEY_REFERENCE_NUMBER = "referenceNumber";
    private static final String KEY_TITLE_NOTIFICATION = "titleNotification";
    private static final String KEY_CONTENT_NOTIFICATION = "contentNotification";
    private static final String KEY_ATTRIBUTE_NOTIFICATION = "attributeNotification";
    private static final String KEY_JOB_UPDATED = "jobUpdated";
    private static final String KEY_JOB_DESTROY = "jobDestroy";
    private static final String KEY_DONE_READ = "doneRead";
    private static final String KEY_TIME_CREATED = "timeCreated";
    /* ========= COLUMNS TABLE (NOTIFICATION) ============= */


    /* ========= COLUMNS TABLE (JOB DETAILS) ============= */
    private static final String KEY_JOB_UPDATE_WITH_NO_INTERNET = "jobUpdateWithNoInternet";
    private static final String KEY_ORDER_NUMBER = "orderNumber";
    private static final String KEY_COMPANY_NAME = "companyName";
    private static final String KEY_JOB_DROP_OFF_DATE = "jobDropOffDate";
    private static final String KEY_JOB_DROP_OFF_TIME = "jobDropOffTime";
    private static final String KEY_JOB_DROP_OFF_TIME_START = "jobDropOffTimeStart";
    private static final String KEY_JOB_DROP_OFF_TIME_END = "jobDropOffTimeEnd";
    private static final String KEY_JOB_DROP_OFF_DESC = "jobDropOffDesc";
    private static final String KEY_JOB_EXTRA_DATA = "extraData";
    private static final String KEY_JOB_TRACKING_NUMBER = "jobTrackingNumber";
    private static final String KEY_JOB_IS_REATTEMPT = "jobIsReattempt";
    private static final String KEY_REATTEMPT_JOB_STEP_ID = "reattemptJobStepId";
    private static final String KEY_DROP_OFF_CONTACT_NAME = "dropOffContactName";
    private static final String KEY_TOTAL_PACKAGE = "totalPackage";
    private static final String KEY_DROP_OFF_TIME_PLANNED = "dropOffTimePlanned";
    private static final String KEY_JOB_DRIVER_NOTE = "jobDriverNotes";
    private static final String KEY_JOB_TYPE = "jobType";

    /* ========= COLUMNS TABLE (JOB DETAILS) ============= */


    /* ========= COLUMNS TABLE (JOB STEP DETAILS) ============= */
    private static final String KEY_JOB_STEP_ID = "jobStepID";
    private static final String KEY_JOB_STEP_STATUS_ID = "jobStepStatusID";
    private static final String KEY_UPDATED_TIME = "updatedTime";
    private static final String KEY_UPDATED_TIME_DB = "updatedTimeDB";

    private static final String KEY_JOB_STEP_NAME = "jobStepName";
    private static final String KEY_JOB_STEP_LOCATION = "jobStepLocation";
    private static final String KEY_JOB_STEP_LATITUDE = "jobStepLatitude";
    private static final String KEY_JOB_STEP_LONGITUDE = "jobStepLongitude";
    private static final String KEY_JOB_STEP_SEQUENCE = "jobStepSequence";
    private static final String KEY_JOB_STEP_DESC = "jobStepDesc";
    private static final String KEY_JOB_STEP_PIC = "jobStepPic";
    private static final String KEY_JOB_STEP_PIC_CONTACT = "jobStepPicContact";
    private static final String KEY_JOB_STEP_IS_SCAN = "jobStepIsScanRequired";
    private static final String KEY_JOB_STEP_IS_SIGNATURE = "jobStepIsSignatureRequired";
    private static final String KEY_JOB_STEP_UPDATE_WITH_NO_INTERNET = "jobStepUpdateWithNoInternet";
    private static final String KEY_JOB_STEP_DROP_OFF_TIME_END = "jobStepDropOffTimeEnd";
    private static final String KEY_JOB_STEP_DROP_OFF_TIME = "jobStepDropOffTime";
    /* ========= COLUMNS TABLE (JOB STEP DETAILS) ============= */


    /* ========= COLUMNS TABLE (JOB ITEM DETAILS) ============= */
    private static final String KEY_JOB_ITEM_ID = "jobItemID";
    private static final String KEY_JOB_ITEM_QUANTITY = "jobItemQuantity";
    private static final String KEY_JOB_ITEM_DESC = "jobItemDesc";
    private static final String KEY_JOB_ITEM_WEIGHT = "jobItemWeight";
    private static final String KEY_JOB_ITEM_REMARKS = "jobItemRemarks";
    private static final String KEY_JOB_ITEM_UOM = "jobItemUom";
    /* ========= COLUMNS TABLE (JOB ITEM DETAILS) ============= */


    /* ========= COLUMNS TABLE (ORDER ATTEMPT DETAILS) ============= */
    private static final String KEY_ORDERATTEMPT_ID = "orderAttemptID";
    private static final String KEY_RECEIVED_BY = "receivedBy";
    private static final String KEY_REASON = "reason";
    private static final String KEY_NOTE = "note";
    private static final String KEY_SUBMITTED_TIME = "submittedTime";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_ATTEMPT_UPDATE_WITH_NO_INTERNET = "attemptUpdateWithNoInternet";
    /* ========= COLUMNS TABLE (ORDER ATTEMPT DETAILS) ============= */

    /* ========= COLUMNS TABLE (IMAGE DETAILS) ============= */
    private static final String KEY_IMAGE_ID = "imageID";
    private static final String KEY_IMAGE_BASE64 = "imageBase64";
    private static final String KEY_IMAGE_URL = "imageURL";
    private static final String KEY_IMAGE_DESCRIPTION = "imageDescription";
    private static final String KEY_IMAGE_IS_SIGNATURE = "imageIsSignature";
    private static final String KEY_IMAGE_IS_REMOVE = "imageIsRemove";
    private static final String KEY_IMAGE_UPDATE_WITH_NO_INTERNET = "attemptUpdateWithNoInternet";
    /* ========= COLUMNS TABLE (IMAGE DETAILS) ============= */

    /* ====== DATABASE ======== */


    /* ====== ALTER DATABASE ======== */
    private static final String ALTER_JOB_ITEM_TABLE_ADD_UOM =
            "ALTER TABLE JobItemDetails ADD jobItemUom TEXT";

    private static final String ALTER_JOB_DETAILS_ADD_TOTAL_PACKAGE =
            "ALTER TABLE JobDetails ADD totalPackage INTEGER";

    private static final String ALTER_JOB_DETAILS_ADD_DROP_OFF_CONTAC_NAME =
            "ALTER TABLE JobDetails ADD dropOffContactName TEXT";
    /* ====== ALTER DATABASE ======== */


    Context context;
    /* ============= DECLARATIONS =========*/


    public int updateJob(JSONArray orderId, JSONObject orderJson, Integer databaseId) {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.i("", "database order id to string:" + orderId.toString());
        values.put(KEY_ORDER_ID, orderId.toString()); // job jobid

        Log.i("", "database order json to string:" + orderJson.toString());

        String orderJsonString = orderJson.toString();
        values.put(KEY_ORDER_JSON, orderJsonString); // job gotsign


        // updating row
        return db.update(TABLE_NAME, values, KEY_ID + " = ?",
                new String[]{String.valueOf(databaseId)});
    }


    public DatabaseHandlerJobs(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public boolean existsColumnInTable(String columnToCheck) {
        SQLiteDatabase inDatabase = this.getWritableDatabase();
        String inTable = TABLE_NAME;
        Cursor mCursor = null;
        try {
            // Query 1 row
            mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

            // getColumnIndex() gives us the index (0 to ...) of the column - otherwise we get a -1
            return mCursor.getColumnIndex(columnToCheck) != -1;

        } catch (Exception Exp) {
            // Something went wrong. Missing the database? The table?
            return false;
        } finally {
            if (mCursor != null) mCursor.close();
        }
    }


    /* ========= CREATE TABLE =========== */
    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        String CREATE_UPDATEDJOBS_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_ORDER_ID + " TEXT,"
                + KEY_ORDER_JSON + " TEXT" + ")";

        db.execSQL(CREATE_UPDATEDJOBS_TABLE);


        // Table order status
        String CREATE_ORDERSTATUS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_STATUS_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY,"
                + KEY_ORDER_STATUS_NAME + " TEXT,"
                + KEY_ORDER_GENERAL_ID + " INTEGER,"
                + KEY_ORDER_IS_ACTIVE + " INTEGER,"
                + KEY_ORDER_VIEW_NAME + " TEXT,"
                + KEY_ORDER_STATUS_ID + " INTEGER" + ")";

        db.execSQL(CREATE_ORDERSTATUS_TABLE);

        /* =========== TABLE APP COMPANY SETTINGS =========== */
        String CREATE_APP_COMPANY_SETTINGS = "CREATE TABLE IF NOT EXISTS " + TABLE_APP_COMPANY_SETTING + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ID_APP_COMPANY_SETTINGS + " TEXT,"
                + KEY_RULE_APP_COMPANY_SETTINGS + " TEXT,"
                + KEY_ENABLE_APP_COMPANY_SETTINGS + " INTEGER,"
                + KEY_CURRENT_STATUS_APP_COMPANY_SETTINGS + " INTEGER,"
                + KEY_TO_STATUS_APP_COMPANY_SETTINGS + " INTEGER,"
                + KEY_IS_MANPOWER_WORKER_APP_COMPANY_SETTINGS + " INTEGER,"
                + KEY_IS_SCAN_REQUIRED_APP_COMPANY_SETTINGS + " INTEGER" + ")";

        db.execSQL(CREATE_APP_COMPANY_SETTINGS);
        /* =========== TABLE APP COMPANY SETTINGS =========== */

        /* =========== TABLE NOTIFICATIONS =========== */
        String CREATE_NOTIFICATION = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTIFICATION + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ORDER_ID + " TEXT,"
                + KEY_REFERENCE_NUMBER + " TEXT,"
                + KEY_TITLE_NOTIFICATION + " TEXT,"
                + KEY_CONTENT_NOTIFICATION + " TEXT,"
                + KEY_ATTRIBUTE_NOTIFICATION + " TEXT,"
                + KEY_TIME_CREATED + " TEXT,"
                + KEY_JOB_UPDATED + " INTEGER,"
                + KEY_DONE_READ + " INTEGER,"
                + KEY_JOB_DESTROY + " INTEGER" + ")";

        db.execSQL(CREATE_NOTIFICATION);
        /* =========== TABLE NOTIFICATIONS =========== */


        /* =========== TABLE JOB DETAILS =========== */
        String CREATE_JOB_DETAILS = "CREATE TABLE " + TABLE_JOB_DETAILS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ORDER_ID + " INTEGER,"
                + KEY_ORDER_STATUS_ID + " INTEGER,"
                + KEY_JOB_UPDATE_WITH_NO_INTERNET + " INTEGER,"
                + KEY_JOB_DROP_OFF_DATE + " TEXT,"
                + KEY_JOB_DROP_OFF_TIME + " TEXT,"
                + KEY_JOB_DROP_OFF_DESC + " TEXT,"
                + KEY_JOB_EXTRA_DATA + " TEXT,"
                + KEY_JOB_TRACKING_NUMBER + " TEXT,"
                + KEY_UPDATED_TIME_DB + " TEXT,"
                + KEY_ORDER_NUMBER + " TEXT,"
                + KEY_COMPANY_NAME + " TEXT,"
                + KEY_JOB_DROP_OFF_TIME_START + " TEXT,"
                + KEY_JOB_DROP_OFF_TIME_END + " TEXT,"
                + KEY_JOB_IS_REATTEMPT + " INTEGER,"
                + KEY_REATTEMPT_JOB_STEP_ID + " INTEGER,"
                + KEY_DROP_OFF_CONTACT_NAME + " TEXT,"
                + KEY_TOTAL_PACKAGE + " INTEGER,"
                + KEY_DROP_OFF_TIME_PLANNED + " TEXT,"
                + KEY_JOB_DRIVER_NOTE + " TEXT,"
                + KEY_JOB_TYPE + " TEXT,"
                + KEY_UPDATED_TIME + " TEXT" + ")";

        db.execSQL(CREATE_JOB_DETAILS);
        /* =========== TABLE JOB DETAILS =========== */


        /* =========== TABLE JOB STEP DETAILS =========== */
        String CREATE_JOB_STEP_DETAILS = "CREATE TABLE " + TABLE_JOB_STEP_DETAILS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ORDER_ID + " INTEGER,"
                + KEY_JOB_STEP_ID + " INTEGER,"
                + KEY_JOB_STEP_STATUS_ID + " INTEGER,"
                + KEY_UPDATED_TIME_DB + " TEXT,"
                + KEY_JOB_STEP_NAME + " TEXT,"
                + KEY_JOB_STEP_LOCATION + " TEXT,"
                + KEY_JOB_STEP_LATITUDE + " DOUBLE,"
                + KEY_JOB_STEP_LONGITUDE + " DOUBLE,"
                + KEY_JOB_STEP_SEQUENCE + " INTEGER,"
                + KEY_JOB_STEP_PIC + " TEXT,"
                + KEY_JOB_STEP_PIC_CONTACT + " TEXT,"
                + KEY_JOB_STEP_IS_SCAN + " INTEGER,"
                + KEY_JOB_STEP_IS_SIGNATURE + " INTEGER,"
                + KEY_JOB_STEP_DESC + " TEXT,"
                + KEY_JOB_STEP_UPDATE_WITH_NO_INTERNET + " INTEGER,"
                + KEY_JOB_STEP_DROP_OFF_TIME + " TEXT,"
                + KEY_JOB_STEP_DROP_OFF_TIME_END + " TEXT,"
                + KEY_UPDATED_TIME + " TEXT" + ")";

        db.execSQL(CREATE_JOB_STEP_DETAILS);
        /* =========== TABLE JOB STEP DETAILS =========== */


        /* =========== TABLE JOB ITEM DETAILS =========== */
        String CREATE_JOB_ITEM_DETAILS = "CREATE TABLE " + TABLE_JOB_ITEM_DETAILS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ORDER_ID + " INTEGER,"
                + KEY_JOB_ITEM_ID + " INTEGER,"
                + KEY_JOB_ITEM_QUANTITY + " INTEGER,"
                + KEY_JOB_ITEM_DESC + " TEXT,"
                + KEY_JOB_ITEM_WEIGHT + " DOUBLE,"
                + KEY_UPDATED_TIME + " TEXT,"
                + KEY_JOB_ITEM_UOM + " TEXT,"
                + KEY_JOB_ITEM_REMARKS + " TEXT" + ")";

        db.execSQL(CREATE_JOB_ITEM_DETAILS);
        /* =========== TABLE JOB ITEM DETAILS =========== */


        /* =========== TABLE ORDER ATTEMPT DETAILS =========== */
        String CREATE_ORDERATTEMPT_DETAILS = "CREATE TABLE " + TABLE_ORDERATTEMPT_DETAILS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ORDERATTEMPT_ID + " INTEGER,"
                + KEY_RECEIVED_BY + " TEXT,"
                + KEY_REASON + " TEXT,"
                + KEY_NOTE + " TEXT,"
                + KEY_SUBMITTED_TIME + " TEXT,"
                + KEY_LATITUDE + " DOUBLE,"
                + KEY_LONGITUDE + " DOUBLE,"
                + KEY_ATTEMPT_UPDATE_WITH_NO_INTERNET + " INTEGER,"
                + KEY_ORDER_ID + " INTEGER,"
                + KEY_JOB_STEP_ID + " INTEGER" + ")";

        db.execSQL(CREATE_ORDERATTEMPT_DETAILS);
        /* =========== TABLE ORDER ATTEMPT DETAILS =========== */


        /* =========== TABLE IMAGE DETAILS =========== */
        String CREATE_IMAGE_DETAILS = "CREATE TABLE " + TABLE_IMAGE_DETAILS + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_IMAGE_ID + " INTEGER,"
                + KEY_IMAGE_BASE64 + " BLOB,"
                + KEY_IMAGE_DESCRIPTION + " TEXT,"
                + KEY_IMAGE_IS_SIGNATURE + " INTEGER,"
                + KEY_IMAGE_IS_REMOVE + " INTEGER,"
                + KEY_IMAGE_URL + " TEXT,"
                + KEY_IMAGE_UPDATE_WITH_NO_INTERNET + " INTEGER,"
                + KEY_ORDER_ID + " INTEGER,"
                + KEY_ORDERATTEMPT_ID + " INTEGER" + ")";

        db.execSQL(CREATE_IMAGE_DETAILS);
        /* =========== TABLE IMAGE DETAILS =========== */




        /* =========== TABLE JOB LATEST =========== */
        String CREATE_JOB_LATEST = "CREATE TABLE " + TABLE_JOB_LATEST + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_ORDER_ID + " INTEGER,"
                + KEY_REFERENCE_NUMBER + " TEXT,"
                + KEY_JOB_DROP_OFF_TIME + " TEXT,"
                + KEY_JOB_REMOVED + " INTEGER" + ")";

        db.execSQL(CREATE_JOB_LATEST);
        /* =========== TABLE JOB LATEST =========== */


    }
    /* ========= CREATE TABLE =========== */


    /* ========= UPGRADING TABLE =========== */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP_COMPANY_SETTING);
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS_NAME);
        // db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATION);


        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB_STEP_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB_ITEM_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORDERATTEMPT_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGE_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB_LATEST);

//
//        String selectQuery = "SELECT  * FROM " + TABLE_JOB_ITEM_DETAILS + " " ;
//        db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        int index = cursor.getColumnIndex("jobItemUom");
//        if (index == -1) {
//            db.execSQL(ALTER_JOB_ITEM_TABLE_ADD_UOM);
//        } else {
//        }

//        String selectQuery1 = "SELECT  * FROM " + TABLE_JOB_DETAILS + " " ;
//        db = this.getWritableDatabase();
//        Cursor cursor1 = db.rawQuery(selectQuery1, null);
//
//        int index1 = cursor1.getColumnIndex("totalPackage");
//        if (index1 == -1) {
//            db.execSQL(ALTER_JOB_DETAILS_ADD_TOTAL_PACKAGE);
//        } else {
//        }
//
//        String selectQuery2 = "SELECT  * FROM " + TABLE_JOB_DETAILS + " " ;
//        db = this.getWritableDatabase();
//        Cursor cursor2 = db.rawQuery(selectQuery1, null);
//
//        int index2 = cursor2.getColumnIndex("dropOffContactName");
//        if (index2 == -1) {
//            db.execSQL(ALTER_JOB_DETAILS_ADD_DROP_OFF_CONTAC_NAME);
//        } else {
//        }

        // Create tables again
        onCreate(db);
    }
    /* ========= UPGRADING TABLE =========== */


    public boolean isTableExists(String tableName, boolean openDb) {
        SQLiteDatabase db = this.getWritableDatabase();

        if (openDb) {
            if (db == null || !db.isOpen()) {
                db = getReadableDatabase();
            }

            if (!db.isReadOnly()) {
                db.close();
                db = getReadableDatabase();
            }
        }

        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
            db.close();
        }
        return false;
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    /* ========= TABLE ORDERS JOBS=========== */
    public void addOrder(JSONArray orderId, JSONObject orderJson) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.i("", "database order id to string:" + orderId.toString());
        values.put(KEY_ORDER_ID, orderId.toString()); // job jobid

        Log.i("", "database order json to string:" + orderJson.toString());

        String orderJsonString = orderJson.toString();
        values.put(KEY_ORDER_JSON, orderJsonString); // job gotsign

        // Inserting Row
        db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    public List<OrderDatabase> getAllJobs() {
        List<OrderDatabase> jobList = new ArrayList<OrderDatabase>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            if (cursor.moveToFirst()) {
                do {
                    OrderDatabase j = new OrderDatabase();
                    Log.i("", "database get job 2:" + cursor.getString(1));
                    Log.i("", "database get job 1:" + cursor.getString(2));

                    j.setDatabaseId(Integer.parseInt(cursor.getString(0)));
                    j.setOrderId(cursor.getString(1));
                    j.setOrderJson(cursor.getString(2));

                    Log.i("", "database get job 3:" + j.getOrderJson());

                    jobList.add(j);

                } while (cursor.moveToNext());
                return jobList;

            } else
                return jobList;
        } finally {
            cursor.close();
            db.close();
        }

    }

    public int deleteEntryByKEY_ID(Integer id) {
        String where = KEY_ID + " = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        int numberOFEntriesDeleted = db.delete(TABLE_NAME, where, null);
        Log.i("", "number of deleted entries:" + numberOFEntriesDeleted);
        db.close();
        return numberOFEntriesDeleted;
    }

    public void deleteAllData() {

        SQLiteDatabase db = this.getWritableDatabase();
        //db.execSQL("delete from " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        db.close();
    }

    public int getRecordsCount() {
        String countQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = 0;
        try {
            if (cursor.moveToFirst()) {
                count = cursor.getCount();
            }
            return count;
        } finally {
            if (cursor != null) {
                cursor.close();
                db.close();
            }
        }
    }

    public void reset() throws SQLException {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("drop table " + TABLE_NAME);
        db.close();
        this.onCreate(db);
    }
    /* ========= TABLE ORDERS JOBS=========== */


    /* ========= TABLE APP COMPANY SETTINGS =========== */

    /* ========== DELETE TABLE  DATA ============= */
    public void deleteAllDataAppCompanySettings() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_APP_COMPANY_SETTING);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS_NAME);

        db.close();
    }


    public void deleteAllTable() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_JOB_LATEST);
        db.execSQL("delete from " + TABLE_JOB_DETAILS);
        db.execSQL("delete from " + TABLE_JOB_ITEM_DETAILS);
        db.execSQL("delete from " + TABLE_JOB_STEP_DETAILS);
        db.execSQL("delete from " + TABLE_ORDERATTEMPT_DETAILS);
        db.execSQL("delete from " + TABLE_IMAGE_DETAILS);
        db.execSQL("delete from " + TABLE_APP_COMPANY_SETTING);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS_NAME);

        db.close();
    }
    /* ========== DELETE TABLE  DATA ============= */


    /* ========== ADD DATA ============= */
    public void updateAppCompanySettings(AppCompanySettings appCompanySettings) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID_APP_COMPANY_SETTINGS, String.valueOf(appCompanySettings.getId()));
        values.put(KEY_RULE_APP_COMPANY_SETTINGS, appCompanySettings.getRule());
        values.put(KEY_ENABLE_APP_COMPANY_SETTINGS, appCompanySettings.isEnable());
        values.put(KEY_CURRENT_STATUS_APP_COMPANY_SETTINGS, appCompanySettings.getCurrentStatus());
        values.put(KEY_TO_STATUS_APP_COMPANY_SETTINGS, appCompanySettings.getToStatus());
        values.put(KEY_IS_MANPOWER_WORKER_APP_COMPANY_SETTINGS, appCompanySettings.isManPowerWorker());
        values.put(KEY_IS_SCAN_REQUIRED_APP_COMPANY_SETTINGS, appCompanySettings.isScanRequired());

        db.insert(TABLE_APP_COMPANY_SETTING, null, values);
        db.close(); // Closing database connection
        // updating row
        //return db.update(TABLE_NAME, values, KEY_ID_APP_COMPANY_SETTINGS + " = ?",new String[] { String.valueOf(appCompanySettings.getId()) });
    }
    /* ========== ADD DATA ============= */

    /* ========== CHECK STATUS NEED SCAN ========= */
    public boolean checkStatusNeedScanOrManPower(int currentStatus, int toStatus, boolean isNeedToScan, String rule) {
        String selectQuery = "";
        boolean isExist;

        if (isNeedToScan) {
            selectQuery = "SELECT * FROM " + TABLE_APP_COMPANY_SETTING + " WHERE "

                    + KEY_IS_SCAN_REQUIRED_APP_COMPANY_SETTINGS + " = " + 1 + " AND "

                    + KEY_TO_STATUS_APP_COMPANY_SETTINGS + " = " + toStatus + " AND "
                    + KEY_RULE_APP_COMPANY_SETTINGS + " = '" + rule + "' AND "
                    + KEY_ENABLE_APP_COMPANY_SETTINGS + " = " + 1 + " ";
        } else {
            selectQuery = "SELECT * FROM " + TABLE_APP_COMPANY_SETTING + " WHERE "

                    + KEY_IS_MANPOWER_WORKER_APP_COMPANY_SETTINGS + " = " + 1 + " AND "

                    + KEY_TO_STATUS_APP_COMPANY_SETTINGS + " = " + toStatus + " AND "
                    + KEY_RULE_APP_COMPANY_SETTINGS + " = '" + rule + "' AND "
                    + KEY_ENABLE_APP_COMPANY_SETTINGS + " = " + 1 + " ";
        }

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cur = db.rawQuery(selectQuery, null);
        isExist = cur.moveToFirst();

        db.close();

        return isExist;


    }
    /* ========== CHECK STATUS NEED SCAN ========= */


    /* ========= TABLE APP COMPANY SETTINGS =========== */







    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ======== TABLE ORDER STATUS LISTING ========== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    /* ========== ADD DATA ============= */
    public void updateListingOrderStatus(OrderStatus orderStatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ORDER_STATUS_ID, String.valueOf(orderStatus.getOrderStatusId()));
        values.put(KEY_ORDER_STATUS_NAME, orderStatus.getOrderStatusName());
        values.put(KEY_ORDER_GENERAL_ID, orderStatus.getOrderGeneralId());
        values.put(KEY_ORDER_IS_ACTIVE, orderStatus.isOrderIsActive());
        values.put(KEY_ORDER_VIEW_NAME, orderStatus.getOrderViewName());

        db.insert(TABLE_STATUS_NAME, null, values);
        db.close(); // Closing database connection
        // updating row
        //return db.update(TABLE_NAME, values, KEY_ID_APP_COMPANY_SETTINGS + " = ?",new String[] { String.valueOf(appCompanySettings.getId()) });
    }
    /* ========== ADD DATA ============= */


    /* ========== DELETE TABLE  DATA ============= */
    public void deleteAllDataOrderStatus() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_STATUS_NAME);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS_NAME);

        db.close();
    }
    /* ========== DELETE TABLE  DATA ============= */

    /* ========== GET ORDER STATUS ID BASED ON ORDER STATUS NAME ============= */
    public int getOrderStatusIDbasedOnOrderStatusName(String orderStatusName) {
        String selectQuery = "";
        int orderStatusId = 0;


        selectQuery = "SELECT " + KEY_ORDER_STATUS_ID + " as statusID FROM " + TABLE_STATUS_NAME + " WHERE "
                + KEY_ORDER_STATUS_NAME + " = '" + orderStatusName + "' ";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cur = db.rawQuery(selectQuery, null);
        if (cur.moveToFirst()) {
            // totalTips =  cur.getDouble(0);
            orderStatusId = cur.getInt(cur.getColumnIndex("statusID"));
        }


        Log.i("getOrderStatusID", "" + selectQuery);
        Log.i("orderStatusId", " : " + orderStatusId);

        db.close();

        return orderStatusId;
    }
    /* ========== GET ORDER STATUS ID BASED ON ORDER STATUS NAME ============= */

    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ======== TABLE ORDER STATUS LISTING ========== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */







    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============ TABLE NOTIFICATION ============== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */


    public void addNotification(NotificationDatabase notificationDatabase, boolean updateData, boolean directlyReadData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        if (updateData) {
            values.put(KEY_DONE_READ, notificationDatabase.isDoneRead());

            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            if (directlyReadData) {
                db.update(TABLE_NOTIFICATION, values, KEY_ORDER_ID + " = ?", new String[]{String.valueOf(notificationDatabase.getOrderId())});
            } else {
                db.update(TABLE_NOTIFICATION, values, KEY_ID + " = ?", new String[]{String.valueOf(notificationDatabase.getIndexDB())});
            }


        } else {

            if (notificationIdAlreadyExist(notificationDatabase.getOrderId())) {
                values.put(KEY_ORDER_ID, notificationDatabase.getOrderId());
                values.put(KEY_REFERENCE_NUMBER, notificationDatabase.getReferenceNumber());
                values.put(KEY_TITLE_NOTIFICATION, notificationDatabase.getTitleNotification());
                values.put(KEY_CONTENT_NOTIFICATION, notificationDatabase.getContentNotification());
                values.put(KEY_ATTRIBUTE_NOTIFICATION, notificationDatabase.getAttributeUpdated());
                values.put(KEY_TIME_CREATED, notificationDatabase.getTimeCreated());
                values.put(KEY_JOB_UPDATED, notificationDatabase.isJobUpdated());
                values.put(KEY_DONE_READ, notificationDatabase.isDoneRead());
                values.put(KEY_JOB_DESTROY, notificationDatabase.isJobDestroy());

                if (db == null || !db.isOpen()) {
                    DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                    db = dbHelper.getWritableDatabase();
                }

                db.update(TABLE_NOTIFICATION, values, KEY_ORDER_ID + " = ?", new String[]{String.valueOf(notificationDatabase.getOrderId())});


            } else {
                values.put(KEY_ORDER_ID, notificationDatabase.getOrderId());
                values.put(KEY_REFERENCE_NUMBER, notificationDatabase.getReferenceNumber());
                values.put(KEY_TITLE_NOTIFICATION, notificationDatabase.getTitleNotification());
                values.put(KEY_CONTENT_NOTIFICATION, notificationDatabase.getContentNotification());
                values.put(KEY_ATTRIBUTE_NOTIFICATION, notificationDatabase.getAttributeUpdated());
                values.put(KEY_TIME_CREATED, notificationDatabase.getTimeCreated());
                values.put(KEY_JOB_UPDATED, notificationDatabase.isJobUpdated());
                values.put(KEY_DONE_READ, notificationDatabase.isDoneRead());
                values.put(KEY_JOB_DESTROY, notificationDatabase.isJobDestroy());

                if (db == null || !db.isOpen()) {
                    DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                    db = dbHelper.getWritableDatabase();
                }

                db.insert(TABLE_NOTIFICATION, null, values);


            }

        }


        db.close();


        // Closing database connection
        // updating row
        //return
    }

    public int deleteNotificationBasedOrderId(String orderID) {
        String where = KEY_ID + " = " + orderID;

        SQLiteDatabase db = this.getWritableDatabase();
        int numberOFEntriesDeleted = db.delete(TABLE_NAME, where, null);
        Log.i("", "number of deleted entries:" + numberOFEntriesDeleted);
        db.close();
        return numberOFEntriesDeleted;
    }


    public void deleteAllDataNotification() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NOTIFICATION);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_STATUS_NAME);

        db.close();
    }


    public List<NotificationDatabase> getAllNotification() {
        List<NotificationDatabase> notificationDatabaseList = new ArrayList<NotificationDatabase>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION + " GROUP BY order_id ORDER BY id DESC ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            if (cursor.moveToFirst()) {
                do {
                    NotificationDatabase j = new NotificationDatabase();

                    j.setIndexDB(cursor.getInt(0));
                    j.setOrderId(cursor.getString(1));
                    j.setReferenceNumber(cursor.getString(2));
                    j.setTitleNotification(cursor.getString(3));
                    j.setContentNotification(cursor.getString(4));
                    j.setAttributeUpdated(cursor.getString(5));
                    j.setTimeCreated(cursor.getString(6));
                    j.setJobUpdated(cursor.getInt(7));
                    j.setDoneRead(cursor.getInt(8));
                    j.setJobDestroy(cursor.getInt(9));

                    notificationDatabaseList.add(j);

                } while (cursor.moveToNext());
                return notificationDatabaseList;

            } else
                return notificationDatabaseList;
        } finally {
            cursor.close();
            db.close();
        }

    }


    /* Checking either notification order id already exist or no */
    public boolean notificationIdAlreadyExist(String orderId) {

        boolean alreadyExist = false;

        String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE "
                + KEY_ORDER_ID + " = '" + orderId + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }
    /* Checking either notification order id already exist or no */


    /* Checking either notification order id already exist or no */
    public int getTotalNotificationUnRead() {

        int totalNotification = 0;

        String selectQuery = "SELECT * FROM " + TABLE_NOTIFICATION + " WHERE "
                + KEY_DONE_READ + " = " + 0 + " ";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            if (cursor.moveToFirst()) {
                do {
                    totalNotification = totalNotification + 1;

                } while (cursor.moveToNext());
                return totalNotification;
            } else
                return totalNotification;
        } finally {
            cursor.close();
            db.close();
        }

    }
    /* Checking either notification order id already exist or no */


    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============ TABLE NOTIFICATION ============== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */









    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============ TABLE JOB DETAILS =============== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    /*
     * This table just can have unique order ID,
     * This table is for store data while no internet connection
     *
     * */
    public List<JobOrder> getListJobDetailsNoInternet(String jobStartDate) {
        List<JobOrder> jobList = new ArrayList<JobOrder>();

        String selectQuery = "";

        //SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format1.parse(jobStartDate);
            jobStartDate = format2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  " + KEY_JOB_DROP_OFF_DATE + " = '" + jobStartDate + "' order by " + KEY_ORDER_STATUS_ID + " DESC";


//        selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE substr(" + KEY_JOB_DROP_OFF_DATE + ",7) || substr(" +  KEY_JOB_DROP_OFF_DATE + ",4,2)||substr(" + KEY_JOB_DROP_OFF_DATE +",1,2) BETWEEN '"
//                + jobStartDate + "' AND '"+jobEndDate + "'"
//                + " order by " + KEY_ORDER_STATUS_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                JobOrder j = new JobOrder();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setOrderStatusId(Integer.parseInt(cursor.getString(2)));

                j.setUpdateWithNoInternet(Integer.parseInt(cursor.getString(3)) != 0);

                j.setDropOffDate(cursor.getString(4));
                j.setDropOffTime(cursor.getString(5));
                j.setDropOffDesc(cursor.getString(6));
                j.setExtraData(cursor.getString(7));
                j.setItemTrackingNumber(cursor.getString(8));
                j.setUpdatedAtDB(cursor.getString(9));
                j.setOrderNumber(cursor.getString(10));
                j.setCompanyName(cursor.getString(11));
                j.setDropOffTimeStart(cursor.getString(12));
                j.setDropOffTimeEnd(cursor.getString(13));

                j.setReattemptJob(Integer.parseInt(cursor.getString(14)) != 0);

                if (cursor.getString(15) == null) {
                    j.setReattemptJobStepId(null);
                } else {
                    j.setReattemptJobStepId(Integer.parseInt(cursor.getString(15)));
                }

                j.setDropOffContactName(cursor.getString(16));
                if (cursor.getString(17) == null) {
                    j.setTotal_package(0);
                } else {
                    j.setTotal_package(Integer.parseInt(cursor.getString(17)));
                }

                j.setDropOffTimePlanned(cursor.getString(18));
                j.setDriverNotes(cursor.getString(19));
                j.setJobType(cursor.getString(20));
                j.setUpdatedAt(cursor.getString(21));

                jobList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return jobList;
    }

    public JobOrder getJobDetailsBasedOnID(int orderID) {

        JobOrder jobOrder = new JobOrder();

        String orderIDString = String.valueOf(orderID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_DETAILS + " WHERE "
                + KEY_ORDER_ID + " = '" + orderIDString + "' ";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                JobOrder j = new JobOrder();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setOrderStatusId(Integer.parseInt(cursor.getString(2)));

                j.setUpdateWithNoInternet(Integer.parseInt(cursor.getString(3)) != 0);

                j.setDropOffDate(cursor.getString(4));
                j.setDropOffTime(cursor.getString(5));
                j.setDropOffDesc(cursor.getString(6));
                j.setExtraData(cursor.getString(7));
                j.setItemTrackingNumber(cursor.getString(8));
                j.setUpdatedAtDB(cursor.getString(9));
                j.setOrderNumber(cursor.getString(10));
                j.setCompanyName(cursor.getString(11));
                j.setDropOffTimeStart(cursor.getString(12));
                j.setDropOffTimeEnd(cursor.getString(13));

                j.setReattemptJob(Integer.parseInt(cursor.getString(14)) != 0);

                if (cursor.getString(15) == null) {
                    j.setReattemptJobStepId(null);
                } else {
                    j.setReattemptJobStepId(Integer.parseInt(cursor.getString(15)));
                }
                j.setDropOffContactName(cursor.getString(16));
                if (cursor.getString(17) == null) {
                    j.setTotal_package(0);
                } else {
                    j.setTotal_package(Integer.parseInt(cursor.getString(17)));
                }

                j.setDropOffTimePlanned(cursor.getString(18));
                j.setDriverNotes(cursor.getString(19));
                j.setJobType(cursor.getString(20));
                j.setUpdatedAt(cursor.getString(21));

                jobOrder = j;

            } while (cursor.moveToNext());
        }

        db.close();

        return jobOrder;
    }

    public List<JobOrder> getListJobDetailUpdated() {
        List<JobOrder> jobList = new ArrayList<JobOrder>();

        String selectQuery = "";

        selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                JobOrder j = new JobOrder();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setOrderStatusId(Integer.parseInt(cursor.getString(2)));

                j.setUpdateWithNoInternet(Integer.parseInt(cursor.getString(3)) != 0);

                j.setDropOffDate(cursor.getString(4));
                j.setDropOffTime(cursor.getString(5));
                j.setDropOffDesc(cursor.getString(6));
                j.setExtraData(cursor.getString(7));
                j.setItemTrackingNumber(cursor.getString(8));
                j.setUpdatedAtDB(cursor.getString(9));
                j.setOrderNumber(cursor.getString(10));
                j.setCompanyName(cursor.getString(11));
                j.setDropOffTimeStart(cursor.getString(12));
                j.setDropOffTimeEnd(cursor.getString(13));

                j.setReattemptJob(Integer.parseInt(cursor.getString(14)) != 0);

                if (cursor.getString(15) == null) {
                    j.setReattemptJobStepId(null);
                } else {
                    j.setReattemptJobStepId(Integer.parseInt(cursor.getString(15)));
                }
                j.setDropOffContactName(cursor.getString(16));
                if (cursor.getString(17) == null) {
                    j.setTotal_package(0);
                } else {
                    j.setTotal_package(Integer.parseInt(cursor.getString(17)));
                }

                j.setDropOffTimePlanned(cursor.getString(18));
                j.setDriverNotes(cursor.getString(19));
                j.setJobType(cursor.getString(20));
                j.setUpdatedAt(cursor.getString(21));

                jobList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return jobList;
    }

    public void addJobDetails(JobOrder jobOrder) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        if (jobDetailsIdAlreadyExist(jobOrder.getOrderId())) {

            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateToStr = format.format(today);
            System.out.println(dateToStr);
            jobOrder.setUpdatedAtDB(dateToStr);

            Log.i("currentTime", "currentTime:" + dateToStr);

            values.put(KEY_UPDATED_TIME_DB, jobOrder.getUpdatedAtDB());
            values.put(KEY_ORDER_STATUS_ID, jobOrder.getOrderStatusId());


            if (jobOrder.isUpdateWithNoInternet() != null) {
                values.put(KEY_JOB_UPDATE_WITH_NO_INTERNET, jobOrder.isUpdateWithNoInternet());
            }

            if (jobOrder.getItemTrackingNumber() != null) {
                values.put(KEY_JOB_TRACKING_NUMBER, jobOrder.getItemTrackingNumber());
            }

            if (jobOrder.isReattemptJob() != null) {
                values.put(KEY_JOB_IS_REATTEMPT, jobOrder.isReattemptJob());
            }

            if (jobOrder.getDropOffTimeStart() != null) {
                values.put(KEY_JOB_DROP_OFF_TIME_START, jobOrder.getDropOffTimeStart());
            }
            if (jobOrder.getDropOffTimeEnd() != null) {
                values.put(KEY_JOB_DROP_OFF_TIME_END, jobOrder.getDropOffTimeEnd());
            }

            if (jobOrder.getReattemptJobStepId() != null) {
                values.put(KEY_REATTEMPT_JOB_STEP_ID, jobOrder.getReattemptJobStepId());
            }

            if (jobOrder.getDropOffContactName() != null) {
                values.put(KEY_DROP_OFF_CONTACT_NAME, jobOrder.getDropOffContactName());
            }

            if (jobOrder.getTotal_package() != null) {
                values.put(KEY_TOTAL_PACKAGE, jobOrder.getTotal_package());
            }

            if (jobOrder.getDropOffTimePlanned() != null) {
                values.put(KEY_DROP_OFF_TIME_PLANNED, jobOrder.getDropOffTimePlanned());
            }

            if (jobOrder.getDriverNotes() != null) {
                values.put(KEY_JOB_DRIVER_NOTE, jobOrder.getDriverNotes());
            }

            if (jobOrder.getJobType() != null) {
                values.put(KEY_JOB_TYPE, jobOrder.getJobType());
            }


            if (jobOrder.getTotal_package() == 0 || jobOrder.getTotal_package() == null) {
                int totalOrderItem = 0;
                if (jobOrder.getOrderDetailsArrayList().size() > 0) {
                    for (int ii = 0; ii < jobOrder.getOrderDetailsArrayList().size(); ii++) {
                        OrderDetails orderDetails = jobOrder.getOrderDetailsArrayList().get(ii);
                        totalOrderItem = totalOrderItem + orderDetails.getQuantity();
                    }


                }

                values.put(KEY_TOTAL_PACKAGE, totalOrderItem);

                LogCustom.i("totalOrderItemUpdate", ": " + totalOrderItem);

            } else {
                values.put(KEY_TOTAL_PACKAGE, jobOrder.getTotal_package());
                LogCustom.i("totalOrderItemPackageUpdate", ": " + jobOrder.getTotal_package());
            }


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.update(TABLE_JOB_DETAILS, values, KEY_ORDER_ID + " = ?", new String[]{String.valueOf(jobOrder.getOrderId())});


        } else {

            values.put(KEY_ORDER_ID, jobOrder.getOrderId());
            values.put(KEY_ORDER_STATUS_ID, jobOrder.getOrderStatusId());
            values.put(KEY_UPDATED_TIME, jobOrder.getUpdatedAt());
            values.put(KEY_UPDATED_TIME_DB, jobOrder.getUpdatedAtDB());
            values.put(KEY_JOB_UPDATE_WITH_NO_INTERNET, false);
            values.put(KEY_JOB_DROP_OFF_DATE, jobOrder.getDropOffDate());
            values.put(KEY_JOB_DROP_OFF_TIME, jobOrder.getDropOffTime());
            values.put(KEY_JOB_DROP_OFF_DESC, jobOrder.getDropOffDesc());
            values.put(KEY_JOB_EXTRA_DATA, jobOrder.getExtraData());
            values.put(KEY_JOB_TRACKING_NUMBER, jobOrder.getItemTrackingNumber());
            values.put(KEY_ORDER_NUMBER, jobOrder.getOrderNumber());
            values.put(KEY_COMPANY_NAME, jobOrder.getCompanyName());
            values.put(KEY_JOB_IS_REATTEMPT, false);
            values.put(KEY_REATTEMPT_JOB_STEP_ID, jobOrder.getReattemptJobStepId());
            values.put(KEY_DROP_OFF_CONTACT_NAME, jobOrder.getDropOffContactName());

            if (jobOrder.getTotal_package() == 0 || jobOrder.getTotal_package() == null) {
                int totalOrderItem = 0;
                if (jobOrder.getOrderDetailsArrayList().size() > 0) {
                    for (int ii = 0; ii < jobOrder.getOrderDetailsArrayList().size(); ii++) {
                        OrderDetails orderDetails = jobOrder.getOrderDetailsArrayList().get(ii);
                        totalOrderItem = totalOrderItem + orderDetails.getQuantity();
                    }


                }

                values.put(KEY_TOTAL_PACKAGE, totalOrderItem);

                LogCustom.i("totalOrderItemAdd", ": " + totalOrderItem);

            } else {
                values.put(KEY_TOTAL_PACKAGE, jobOrder.getTotal_package());
                LogCustom.i("totalOrderItemPackageAdd", ": " + jobOrder.getTotal_package());
            }

            values.put(KEY_DROP_OFF_TIME_PLANNED, jobOrder.getDropOffTimePlanned());
            values.put(KEY_JOB_DRIVER_NOTE, jobOrder.getDriverNotes());
            values.put(KEY_JOB_TYPE, jobOrder.getJobType());


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.insert(TABLE_JOB_DETAILS, null, values);


        }


        db.close();
    }


    /* Checking either notification order id already exist or no */
    public boolean jobDetailsIdAlreadyExist(int orderId) {

        String orderIDString = String.valueOf(orderId);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_DETAILS + " WHERE "
                + KEY_ORDER_ID + " = '" + orderIDString + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }
    /* Checking either notification order id already exist or no */


    public void deleteJobDetailsInsideAllTable(List<Integer> arrayOrderIDList) {

        if (arrayOrderIDList.size() > 0) {
            SQLiteDatabase db = this.getWritableDatabase();

            for (int i = 0; i < arrayOrderIDList.size(); i++) {
                String where = KEY_ORDER_ID + " = " + arrayOrderIDList.get(i);
                db.delete(TABLE_JOB_DETAILS, where, null);
                db.delete(TABLE_JOB_STEP_DETAILS, where, null);
                db.delete(TABLE_JOB_ITEM_DETAILS, where, null);
                db.delete(TABLE_ORDERATTEMPT_DETAILS, where, null);
                db.delete(TABLE_IMAGE_DETAILS, where, null);
                //db.delete(TABLE_JOB_LATEST, where, null);
            }

            db.close();
        }

    }

    public List<Integer> getListJobDetailNOTUpdated() {
        List<Integer> arrayOrderIDList = new ArrayList<Integer>();

        String selectQuery = "";

        selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 0 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                JobOrder j = new JobOrder();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));

                arrayOrderIDList.add(j.getOrderId());

            } while (cursor.moveToNext());
        }

        db.close();

        return arrayOrderIDList;
    }


    public boolean isJobOrderIDHaveUpdatedIntoDB(int orderID) {
        boolean isJobOrderIDHaveUpdatedIntoDB = false;

        String selectQuery = "";

        String orderIDString = String.valueOf(orderID);

        selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  "
                + KEY_ORDER_ID + " = '" + orderIDString + "' AND " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 ";

        //selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        isJobOrderIDHaveUpdatedIntoDB = cursor.moveToFirst();

        db.close();

        return isJobOrderIDHaveUpdatedIntoDB;
    }


    public boolean isNeedUpdatedJobIntoDBLatest(int orderID, String date) {
        boolean isNeedUpdatedJobIntoDBLatest = false;

        String selectQuery = "";

        String orderIDString = String.valueOf(orderID);

        if (date.isEmpty() || date.equalsIgnoreCase("")) {
            selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  "
                    + KEY_ORDER_ID + " = '" + orderIDString + "' AND " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 ";
        } else {
//            selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  "
//                    + KEY_ORDER_ID + " = '"+ orderIDString +"' AND "+ KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 AND "+ KEY_UPDATED_TIME_DB + " < " + date +" ";


            selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  "
                    + KEY_ORDER_ID + " = '" + orderIDString + "' AND " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 AND " + KEY_UPDATED_TIME_DB + " < '" + date + "' ";
        }


        //selectQuery = "SELECT  * FROM " + TABLE_JOB_DETAILS + " WHERE  " + KEY_JOB_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        isNeedUpdatedJobIntoDBLatest = cursor.moveToFirst();

        db.close();

        return isNeedUpdatedJobIntoDBLatest;
    }


    public void deleteJobOrderBasedOnOrderID(Integer id) {
        String where = KEY_ORDER_ID + " = " + id;

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_JOB_DETAILS, where, null);
        db.delete(TABLE_JOB_STEP_DETAILS, where, null);
        db.delete(TABLE_ORDERATTEMPT_DETAILS, where, null);
        db.delete(TABLE_JOB_ITEM_DETAILS, where, null);
        db.delete(TABLE_IMAGE_DETAILS, where, null);
        db.close();
    }



    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============ TABLE JOB DETAILS =============== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */














    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ========== TABLE JOB STEP DETAILS ============ */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    public void addJobStepDetails(JobSteps jobSteps) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        if (jobStepDetailsIdAlreadyExist(jobSteps.getId())) {

            Date today = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateToStr = format.format(today);
            System.out.println(dateToStr);
            jobSteps.setUpdated_atDB(dateToStr);

            Log.i("currentTime", "currentTime:" + dateToStr);


            values.put(KEY_JOB_STEP_STATUS_ID, jobSteps.getJob_step_status_id());


            if (jobSteps.isJobStepUpdateWithNoInternet() != null) {
                values.put(KEY_JOB_STEP_UPDATE_WITH_NO_INTERNET, jobSteps.isJobStepUpdateWithNoInternet());
                values.put(KEY_UPDATED_TIME_DB, jobSteps.getUpdated_atDB());
            }

            if (jobSteps.getDropOffTime() != null) {
                values.put(KEY_JOB_STEP_DROP_OFF_TIME, jobSteps.getDropOffTime());
            }

            if (jobSteps.getDropOffTimeEnd() != null) {
                values.put(KEY_JOB_STEP_DROP_OFF_TIME_END, jobSteps.getDropOffTimeEnd());
            }


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.update(TABLE_JOB_STEP_DETAILS, values, KEY_JOB_STEP_ID + " = ?", new String[]{String.valueOf(jobSteps.getId())});


        } else {

            values.put(KEY_ORDER_ID, jobSteps.getOrderId());
            values.put(KEY_JOB_STEP_ID, jobSteps.getId());
            values.put(KEY_JOB_STEP_STATUS_ID, jobSteps.getJob_step_status_id());
            values.put(KEY_UPDATED_TIME_DB, jobSteps.getUpdated_atDB());
            values.put(KEY_UPDATED_TIME, jobSteps.getUpdated_at());

            values.put(KEY_JOB_STEP_NAME, jobSteps.getJob_step_name());
            values.put(KEY_JOB_STEP_LOCATION, jobSteps.getLocation());
            values.put(KEY_JOB_STEP_LATITUDE, jobSteps.getLatitude());
            values.put(KEY_JOB_STEP_LONGITUDE, jobSteps.getLongitude());
            values.put(KEY_JOB_STEP_SEQUENCE, jobSteps.getOrder_sequence());
            values.put(KEY_JOB_STEP_PIC, jobSteps.getJob_step_pic());
            values.put(KEY_JOB_STEP_PIC_CONTACT, jobSteps.getJob_step_pic_contact());
            values.put(KEY_JOB_STEP_IS_SCAN, jobSteps.is_scan_required());
            values.put(KEY_JOB_STEP_IS_SIGNATURE, jobSteps.is_signature_required());
            values.put(KEY_JOB_STEP_DESC, jobSteps.getDescription());
            values.put(KEY_JOB_STEP_UPDATE_WITH_NO_INTERNET, false);


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.insert(TABLE_JOB_STEP_DETAILS, null, values);


        }


        db.close();
    }

    public List<JobSteps> getListJobStepsDetailUpdated(int orderID) {
        List<JobSteps> jobStepsList = new ArrayList<JobSteps>();

        String selectQuery = "";


        String orderIDString = String.valueOf(orderID);


        selectQuery = "SELECT  * FROM " + TABLE_JOB_STEP_DETAILS + " WHERE  " + KEY_ORDER_ID + " = '" + orderIDString + "' AND " + KEY_JOB_STEP_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                JobSteps j = new JobSteps();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setId(Integer.parseInt(cursor.getString(2)));
                j.setJob_step_status_id(Integer.parseInt(cursor.getString(3)));
                j.setUpdated_atDB(cursor.getString(4));
                j.setJob_step_name(cursor.getString(5));
                j.setLocation(cursor.getString(6));
                j.setLatitude(Double.parseDouble(cursor.getString(7)));
                j.setLongitude(Double.parseDouble(cursor.getString(8)));
                j.setOrder_sequence(Integer.parseInt(cursor.getString(9)));
                j.setJob_step_pic(cursor.getString(10));
                j.setJob_step_pic_contact(cursor.getString(11));


                j.setIs_scan_required(Integer.parseInt(cursor.getString(12)) != 0);


                j.setIs_signature_required(Integer.parseInt(cursor.getString(13)) != 0);

                j.setDescription(cursor.getString(14));

                j.setJobStepUpdateWithNoInternet(Integer.parseInt(cursor.getString(15)) != 0);

                j.setDropOffTime(cursor.getString(16));
                j.setDropOffTimeEnd(cursor.getString(17));

                j.setUpdated_at(cursor.getString(18));


                jobStepsList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return jobStepsList;
    }


    public List<JobSteps> getListJobStepsDetailsNoInternet(int orderID) {
        List<JobSteps> jobStepsList = new ArrayList<JobSteps>();

        String orderIDString = String.valueOf(orderID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_STEP_DETAILS + " WHERE "
                + KEY_ORDER_ID + " = '" + orderIDString + "' order by " + KEY_JOB_STEP_SEQUENCE + " ASC";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {
                JobSteps j = new JobSteps();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setId(Integer.parseInt(cursor.getString(2)));
                j.setJob_step_status_id(Integer.parseInt(cursor.getString(3)));
                j.setUpdated_atDB(cursor.getString(4));
                j.setJob_step_name(cursor.getString(5));
                j.setLocation(cursor.getString(6));
                j.setLatitude(Double.parseDouble(cursor.getString(7)));
                j.setLongitude(Double.parseDouble(cursor.getString(8)));
                j.setOrder_sequence(Integer.parseInt(cursor.getString(9)));
                j.setJob_step_pic(cursor.getString(10));
                j.setJob_step_pic_contact(cursor.getString(11));


                j.setIs_scan_required(Integer.parseInt(cursor.getString(12)) != 0);


                j.setIs_signature_required(Integer.parseInt(cursor.getString(13)) != 0);

                j.setDescription(cursor.getString(14));

                j.setJobStepUpdateWithNoInternet(Integer.parseInt(cursor.getString(15)) != 0);

                j.setDropOffTime(cursor.getString(16));
                j.setDropOffTimeEnd(cursor.getString(17));

                j.setUpdated_at(cursor.getString(18));


                jobStepsList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return jobStepsList;
    }


    public JobSteps getJobStepsDetailsBasedOnID(int jobStepID) {
        JobSteps jobSteps = new JobSteps();

        String jobStepIDString = String.valueOf(jobStepID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_STEP_DETAILS + " WHERE "
                + KEY_JOB_STEP_ID + " = '" + jobStepIDString + "' ";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {
                JobSteps j = new JobSteps();


                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setId(Integer.parseInt(cursor.getString(2)));
                j.setJob_step_status_id(Integer.parseInt(cursor.getString(3)));
                j.setUpdated_atDB(cursor.getString(4));
                j.setJob_step_name(cursor.getString(5));
                j.setLocation(cursor.getString(6));
                j.setLatitude(Double.parseDouble(cursor.getString(7)));
                j.setLongitude(Double.parseDouble(cursor.getString(8)));
                j.setOrder_sequence(Integer.parseInt(cursor.getString(9)));

                if (cursor.getString(10) == null || cursor.getString(10).equalsIgnoreCase("")) {
                    j.setJob_step_pic("");
                } else {
                    j.setJob_step_pic(cursor.getString(10));
                }

                if (cursor.getString(11) == null || cursor.getString(11).equalsIgnoreCase("")) {
                    j.setJob_step_pic_contact("");
                } else {
                    j.setJob_step_pic_contact(cursor.getString(11));
                }


                j.setIs_scan_required(Integer.parseInt(cursor.getString(12)) != 0);


                j.setIs_signature_required(Integer.parseInt(cursor.getString(13)) != 0);

                j.setDescription(cursor.getString(14));

                j.setJobStepUpdateWithNoInternet(Integer.parseInt(cursor.getString(15)) != 0);

                j.setDropOffTime(cursor.getString(16));
                j.setDropOffTimeEnd(cursor.getString(17));

                j.setUpdated_at(cursor.getString(18));

                jobSteps = j;

            } while (cursor.moveToNext());
        }

        db.close();

        return jobSteps;
    }

    public boolean isJobStepIDHaveUpdatedIntoDB(int jobStepID) {
        boolean isJobStepIDHaveUpdatedIntoDB = false;

        String selectQuery = "";


        String jobStepIDString = String.valueOf(jobStepID);


        selectQuery = "SELECT  * FROM " + TABLE_JOB_STEP_DETAILS + " WHERE  " + KEY_JOB_STEP_ID + " = '" + jobStepIDString + "' AND " + KEY_JOB_STEP_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        isJobStepIDHaveUpdatedIntoDB = cursor.moveToFirst();

        db.close();

        return isJobStepIDHaveUpdatedIntoDB;
    }

    public boolean jobStepDetailsIdAlreadyExist(int jobStepID) {

        String jobStepIDString = String.valueOf(jobStepID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_STEP_DETAILS + " WHERE "
                + KEY_JOB_STEP_ID + " = '" + jobStepIDString + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }

    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ========== TABLE JOB STEP DETAILS ============ */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */








    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ========== TABLE JOB ITEM DETAILS ============ */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    /*
     * */
    public void addJobItemDetails(OrderDetails orderDetails) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        if (jobItemDetailsIdAlreadyExist(orderDetails.getItemId())) {
            //values.put(KEY_JOB_STEP_STATUS_ID, orderDetails.getJob_step_status_id());
            values.put(KEY_UPDATED_TIME, orderDetails.getUpdated_at());
            values.put(KEY_JOB_ITEM_QUANTITY, orderDetails.getQuantity());
            values.put(KEY_JOB_ITEM_DESC, orderDetails.getOrderDescription());
            values.put(KEY_JOB_ITEM_WEIGHT, orderDetails.getWeight());
            values.put(KEY_JOB_ITEM_REMARKS, orderDetails.getRemarks());
            values.put(KEY_JOB_ITEM_UOM, orderDetails.getUom());

            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.update(TABLE_JOB_ITEM_DETAILS, values, KEY_JOB_ITEM_ID + " = ?", new String[]{String.valueOf(orderDetails.getItemId())});


        } else {

            values.put(KEY_ORDER_ID, orderDetails.getOrderId());
            values.put(KEY_JOB_ITEM_ID, orderDetails.getItemId());
            values.put(KEY_JOB_ITEM_QUANTITY, orderDetails.getQuantity());
            values.put(KEY_JOB_ITEM_DESC, orderDetails.getOrderDescription());
            values.put(KEY_JOB_ITEM_WEIGHT, orderDetails.getWeight());
            values.put(KEY_JOB_ITEM_REMARKS, orderDetails.getRemarks());
            values.put(KEY_UPDATED_TIME, orderDetails.getUpdated_at());
            values.put(KEY_JOB_ITEM_UOM, orderDetails.getUom());


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.insert(TABLE_JOB_ITEM_DETAILS, null, values);


        }


        db.close();
    }

    public List<OrderDetails> getListJobItemsDetailsNoInternet(int orderID) {
        List<OrderDetails> jobItemList = new ArrayList<OrderDetails>();

        String orderIDString = String.valueOf(orderID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_ITEM_DETAILS + " WHERE "
                + KEY_ORDER_ID + " = '" + orderIDString + "' order by " + KEY_UPDATED_TIME + " DESC";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {
                OrderDetails j = new OrderDetails();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setOrderDetailsId(Integer.parseInt(cursor.getString(2)));
                j.setQuantity(Integer.parseInt(cursor.getString(3)));
                j.setOrderDescription(cursor.getString(4));
                j.setWeight(Double.parseDouble(cursor.getString(5)));
                j.setUom(cursor.getString(7));
                j.setRemarks(cursor.getString(8));

                jobItemList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return jobItemList;
    }


    /* Checking either notification order id already exist or no */
    public boolean jobItemDetailsIdAlreadyExist(int jobItemID) {

        String jobItemIDString = String.valueOf(jobItemID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_ITEM_DETAILS + " WHERE "
                + KEY_JOB_ITEM_ID + " = '" + jobItemIDString + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }
    /* Checking either notification order id already exist or no */


    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ========== TABLE JOB ITEM DETAILS ============ */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */






    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ======= TABLE ORDER ATTEMPT DETAILS ========== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    public void addOrderAttemptDetails(OrderAttempt orderAttempt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        if (orderAttemptIdAlreadyExist(orderAttempt.getId())) {


            values.put(KEY_RECEIVED_BY, orderAttempt.getReceived_by());
            values.put(KEY_REASON, orderAttempt.getReason());
            values.put(KEY_NOTE, orderAttempt.getNote());
            values.put(KEY_SUBMITTED_TIME, orderAttempt.getSubmitted_time());
            values.put(KEY_LATITUDE, orderAttempt.getLatitude());
            values.put(KEY_LONGITUDE, orderAttempt.getLatitude());
            values.put(KEY_JOB_STEP_ID, orderAttempt.getJobStepID());


            if (orderAttempt.isAttemptUpdateWithNoInternet() != null) {
                values.put(KEY_ATTEMPT_UPDATE_WITH_NO_INTERNET, orderAttempt.isAttemptUpdateWithNoInternet());
            }


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.update(TABLE_ORDERATTEMPT_DETAILS, values, KEY_ORDERATTEMPT_ID + " = ?", new String[]{String.valueOf(orderAttempt.getId())});


        } else {

            values.put(KEY_ORDER_ID, orderAttempt.getOrderId());
            values.put(KEY_ORDERATTEMPT_ID, orderAttempt.getId());
            values.put(KEY_RECEIVED_BY, orderAttempt.getReceived_by());
            values.put(KEY_REASON, orderAttempt.getReason());
            values.put(KEY_NOTE, orderAttempt.getNote());
            values.put(KEY_SUBMITTED_TIME, orderAttempt.getSubmitted_time());
            values.put(KEY_LATITUDE, orderAttempt.getLatitude());
            values.put(KEY_LONGITUDE, orderAttempt.getLatitude());
            values.put(KEY_ATTEMPT_UPDATE_WITH_NO_INTERNET, orderAttempt.isAttemptUpdateWithNoInternet());
            values.put(KEY_JOB_STEP_ID, orderAttempt.getJobStepID());


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.insert(TABLE_ORDERATTEMPT_DETAILS, null, values);


        }


        db.close();
    }

    public List<OrderAttempt> getListOrderAttemptUpdated(int jobStepID) {
        List<OrderAttempt> orderAttemptList = new ArrayList<OrderAttempt>();

        String selectQuery = "";


        String jobStepIDString = String.valueOf(jobStepID);


        selectQuery = "SELECT  * FROM " + TABLE_ORDERATTEMPT_DETAILS + " WHERE  " + KEY_JOB_STEP_ID + " = '" + jobStepIDString + "' AND " + KEY_ATTEMPT_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                OrderAttempt j = new OrderAttempt();

                j.setId(Integer.parseInt(cursor.getString(1)));
                j.setReceived_by(cursor.getString(2));
                j.setReason(cursor.getString(3));
                j.setNote(cursor.getString(4));
                j.setSubmitted_time(cursor.getString(5));
                j.setLatitude(Double.parseDouble(cursor.getString(6)));
                j.setLongitude(Double.parseDouble(cursor.getString(7)));


                j.setAttemptUpdateWithNoInternet(Integer.parseInt(cursor.getString(8)) != 0);


                j.setJobStepID(Integer.parseInt(cursor.getString(9)));

                orderAttemptList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return orderAttemptList;
    }


    public List<OrderAttempt> getListAllOrderAttemptDetailsNoInternet(int jobStepId) {
        List<OrderAttempt> orderAttemptList = new ArrayList<OrderAttempt>();

        String jobStepIdString = String.valueOf(jobStepId);

        String selectQuery = "SELECT * FROM " + TABLE_ORDERATTEMPT_DETAILS + " WHERE "
                + KEY_JOB_STEP_ID + " = '" + jobStepIdString + "'";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {
                OrderAttempt j = new OrderAttempt();

                j.setId(Integer.parseInt(cursor.getString(1)));
                j.setReceived_by(cursor.getString(2));
                j.setReason(cursor.getString(3));
                j.setNote(cursor.getString(4));
                j.setSubmitted_time(cursor.getString(5));
                j.setLatitude(Double.parseDouble(cursor.getString(6)));
                j.setLongitude(Double.parseDouble(cursor.getString(7)));


                j.setAttemptUpdateWithNoInternet(Integer.parseInt(cursor.getString(8)) != 0);


                j.setJobStepID(Integer.parseInt(cursor.getString(9)));

                orderAttemptList.add(j);


            } while (cursor.moveToNext());
        }

        db.close();

        return orderAttemptList;
    }


    public OrderAttempt getOrderAttemptDetailsBasedOnID(int orderAttemptID) {
        OrderAttempt orderAttempt = new OrderAttempt();

        String orderAttemptIDString = String.valueOf(orderAttemptID);

        String selectQuery = "SELECT * FROM " + TABLE_ORDERATTEMPT_DETAILS + " WHERE "
                + KEY_ORDERATTEMPT_ID + " = '" + orderAttemptIDString + "' ";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {

                OrderAttempt j = new OrderAttempt();

                j.setId(Integer.parseInt(cursor.getString(1)));
                j.setReceived_by(cursor.getString(2));
                j.setReason(cursor.getString(3));
                j.setNote(cursor.getString(4));
                j.setSubmitted_time(cursor.getString(5));
                j.setLatitude(Double.parseDouble(cursor.getString(6)));
                j.setLongitude(Double.parseDouble(cursor.getString(7)));


                j.setAttemptUpdateWithNoInternet(Integer.parseInt(cursor.getString(8)) != 0);


                j.setJobStepID(Integer.parseInt(cursor.getString(9)));

                orderAttempt = j;

            } while (cursor.moveToNext());
        }

        db.close();

        return orderAttempt;
    }


    public boolean orderAttemptIdAlreadyExist(int orderAttemptID) {

        String orderAttemptIDString = String.valueOf(orderAttemptID);

        String selectQuery = "SELECT * FROM " + TABLE_ORDERATTEMPT_DETAILS + " WHERE "
                + KEY_ORDERATTEMPT_ID + " = '" + orderAttemptIDString + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }

    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ======= TABLE ORDER ATTEMPT DETAILS ========== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */









    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ======= TABLE IMAGE DETAILS ========== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    public void addImageDetails(OrderAttemptImage orderAttemptImage) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        if (imageIdAlreadyExist(orderAttemptImage.getId())) {


            values.put(KEY_IMAGE_BASE64, orderAttemptImage.getBase64());
            values.put(KEY_IMAGE_DESCRIPTION, orderAttemptImage.getNote());
            values.put(KEY_IMAGE_IS_SIGNATURE, orderAttemptImage.isSignature());
            values.put(KEY_IMAGE_IS_REMOVE, orderAttemptImage.isRemove());


            if (orderAttemptImage.isImageUpdateNoInternet() != null) {
                values.put(KEY_IMAGE_UPDATE_WITH_NO_INTERNET, orderAttemptImage.isImageUpdateNoInternet());
            }


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.update(TABLE_IMAGE_DETAILS, values, KEY_IMAGE_ID + " = ?", new String[]{String.valueOf(orderAttemptImage.getId())});


        } else {

            values.put(KEY_ORDER_ID, orderAttemptImage.getOrderId());
            values.put(KEY_IMAGE_ID, orderAttemptImage.getId());
            values.put(KEY_IMAGE_BASE64, orderAttemptImage.getBase64());
            values.put(KEY_IMAGE_DESCRIPTION, orderAttemptImage.getNote());
            values.put(KEY_IMAGE_IS_SIGNATURE, orderAttemptImage.isSignature());
            values.put(KEY_IMAGE_IS_REMOVE, orderAttemptImage.isRemove());
            values.put(KEY_ORDERATTEMPT_ID, orderAttemptImage.getOrderAttemptID());
            values.put(KEY_IMAGE_UPDATE_WITH_NO_INTERNET, orderAttemptImage.isImageUpdateNoInternet());
            values.put(KEY_IMAGE_URL, orderAttemptImage.getUrl());


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.insert(TABLE_IMAGE_DETAILS, null, values);


        }


        db.close();
    }

    public List<OrderAttemptImage> getListImageUpdated(int orderAttempID) {
        List<OrderAttemptImage> orderAttemptImageList = new ArrayList<OrderAttemptImage>();

        String selectQuery = "";


        String orderAttempIDString = String.valueOf(orderAttempID);


        selectQuery = "SELECT  * FROM " + TABLE_IMAGE_DETAILS + " WHERE  " + KEY_ORDERATTEMPT_ID + " = '" + orderAttempIDString + "' AND " + KEY_ATTEMPT_UPDATE_WITH_NO_INTERNET + " = 1 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                OrderAttemptImage j = new OrderAttemptImage();

                j.setId(Integer.parseInt(cursor.getString(1)));
                //j.setBase64(cursor.getString(2));
                j.setNote(cursor.getString(3));

                j.setSignature(Integer.parseInt(cursor.getString(4)) != 0);

                j.setRemove(Integer.parseInt(cursor.getString(5)) != 0);

                j.setUrl(cursor.getString(6));

                j.setImageUpdateNoInternet(Integer.parseInt(cursor.getString(7)) != 0);

                j.setOrderAttemptID(Integer.parseInt(cursor.getString(8)));

                orderAttemptImageList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return orderAttemptImageList;
    }


    public List<OrderAttemptImage> getListAllImageDetailsNoInternet(int orderAttemptID) {
        List<OrderAttemptImage> orderAttemptImageList = new ArrayList<OrderAttemptImage>();

        String orderAttemptIDString = String.valueOf(orderAttemptID);

        String selectQuery = "SELECT * FROM " + TABLE_IMAGE_DETAILS + " WHERE "
                + KEY_ORDERATTEMPT_ID + " = '" + orderAttemptID + "'";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {
                OrderAttemptImage j = new OrderAttemptImage();

                j.setId(Integer.parseInt(cursor.getString(1)));
                // j.setBase64(cursor.getString(2));
                j.setNote(cursor.getString(3));

                j.setSignature(Integer.parseInt(cursor.getString(4)) != 0);

                j.setRemove(Integer.parseInt(cursor.getString(5)) != 0);

                j.setUrl(cursor.getString(6));

                j.setImageUpdateNoInternet(Integer.parseInt(cursor.getString(7)) != 0);

                j.setOrderAttemptID(Integer.parseInt(cursor.getString(8)));

                orderAttemptImageList.add(j);


            } while (cursor.moveToNext());
        }

        db.close();

        return orderAttemptImageList;
    }


    public OrderAttemptImage getImageDetailsBasedOnID(int imageID) {
        OrderAttemptImage orderAttemptImage = new OrderAttemptImage();

        String imageIDString = String.valueOf(imageID);

        String selectQuery = "SELECT * FROM " + TABLE_IMAGE_DETAILS + " WHERE "
                + KEY_IMAGE_ID + " = '" + imageIDString + "' ";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job step query:", "" + selectQuery);


        if (cursor.moveToFirst()) {
            do {

                OrderAttemptImage j = new OrderAttemptImage();

                j.setId(Integer.parseInt(cursor.getString(1)));
                //j.setBase64(cursor.getString(2));
                j.setNote(cursor.getString(3));

                j.setSignature(Integer.parseInt(cursor.getString(4)) != 0);

                j.setRemove(Integer.parseInt(cursor.getString(5)) != 0);

                j.setUrl(cursor.getString(6));

                j.setImageUpdateNoInternet(Integer.parseInt(cursor.getString(7)) != 0);

                j.setOrderAttemptID(Integer.parseInt(cursor.getString(8)));

                orderAttemptImage = j;

            } while (cursor.moveToNext());
        }

        db.close();

        return orderAttemptImage;
    }


    public boolean imageIdAlreadyExist(int imageID) {

        String imageIDString = String.valueOf(imageID);

        String selectQuery = "SELECT * FROM " + TABLE_IMAGE_DETAILS + " WHERE "
                + KEY_IMAGE_ID + " = '" + imageIDString + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }

    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ======= TABLE IMAGE DETAILS ========== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */





    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ========== TABLE JOB LATEST ============ */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */
    /* ============================================== */

    public void addJobLatest(JobOrder jobOrder) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        if (jobLatestDetailsIdAlreadyExist(jobOrder.getOrderId())) {

            if (jobOrder.getRemoved() != null) {
                values.put(KEY_JOB_REMOVED, jobOrder.getRemoved());
            }

            if (jobOrder.getDropOffTime() != null) {
                values.put(KEY_JOB_DROP_OFF_TIME, jobOrder.getDropOffTime());
            }


            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.update(TABLE_JOB_LATEST, values, KEY_ORDER_ID + " = ?", new String[]{String.valueOf(jobOrder.getOrderId())});


        } else {

            values.put(KEY_ORDER_ID, jobOrder.getOrderId());
            values.put(KEY_REFERENCE_NUMBER, jobOrder.getReference_no());
            values.put(KEY_JOB_REMOVED, jobOrder.getRemoved());
            values.put(KEY_JOB_DROP_OFF_TIME, jobOrder.getDropOffTime());

            if (db == null || !db.isOpen()) {
                DatabaseHandlerJobs dbHelper = new DatabaseHandlerJobs(context); //u can omit this but need to have an extra check dbHelper!=null
                db = dbHelper.getWritableDatabase();
            }

            db.insert(TABLE_JOB_LATEST, null, values);


        }


        db.close();
    }


    public List<JobOrder> getListJobLatest() {
        List<JobOrder> jobList = new ArrayList<JobOrder>();

        String selectQuery = "";

        selectQuery = "SELECT  * FROM " + TABLE_JOB_LATEST + " WHERE  " + KEY_JOB_REMOVED + " = 0 ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        Log.i("job query:", "" + selectQuery);

        if (cursor.moveToFirst()) {
            do {
                JobOrder j = new JobOrder();

                j.setOrderId(Integer.parseInt(cursor.getString(1)));
                j.setReference_no(cursor.getString(2));
                jobList.add(j);

            } while (cursor.moveToNext());
        }

        db.close();

        return jobList;
    }


    public boolean jobLatestDetailsIdAlreadyExist(int jobLatestID) {

        String jobLatestIDString = String.valueOf(jobLatestID);

        String selectQuery = "SELECT * FROM " + TABLE_JOB_LATEST + " WHERE "
                + KEY_ORDER_ID + " = '" + jobLatestIDString + "' ";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        Log.i("", "database cursor:" + cursor);


        try {
            return cursor.moveToFirst();
        } finally {
            cursor.close();
            db.close();
        }

    }

    public void deleteJobLatestArray(List<Integer> arrayOrderIDList) {

        if (arrayOrderIDList.size() > 0) {
            SQLiteDatabase db = this.getWritableDatabase();

            for (int i = 0; i < arrayOrderIDList.size(); i++) {
                String where = KEY_ORDER_ID + " = " + arrayOrderIDList.get(i);
                db.delete(TABLE_JOB_LATEST, where, null);
            }

            db.close();
        }

    }

}

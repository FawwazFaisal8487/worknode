package com.evfy.evfytracker.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.evfy.evfytracker.Activity.ScanBarcode.ScanBarcodeActivity;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.TrackingOrderCheckBox;

import java.util.ArrayList;
import java.util.List;

//import coolasia.com.wmgworker.Activity.ScanActivity;

public class TrackingOrderAdapter extends ArrayAdapter<TrackingOrderCheckBox> {
    private final Context context;
    private final boolean isTransfer;
    private List<TrackingOrderCheckBox> trackingOrderCheckBoxesList = new ArrayList<TrackingOrderCheckBox>();

    public TrackingOrderAdapter(Context context, int row_tracking_order_row, List<TrackingOrderCheckBox> values, boolean isTransfer) {
        super(context, -1, values);
        this.context = context;
        this.trackingOrderCheckBoxesList = values;
        this.isTransfer = isTransfer;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_tracking_order_row, parent, false);
        TextView textView = rowView.findViewById(R.id.trackingNumber);
        CheckBox checkBox = rowView.findViewById(R.id.trackingNumberCheckBox);

        ImageButton btnManualInput = rowView.findViewById(R.id.buttonManualInput);


        btnManualInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                final EditText edittext = new EditText(context);
                builder.setTitle("Enter barcode");
                builder.setView(edittext);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        ((ScanBarcodeActivity) context).verifyBarCode(edittext.getText().toString());


                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.create().show();
            }
        });


        checkBox.setChecked(trackingOrderCheckBoxesList.get(position).isChecked());

        if (isTransfer) {
            btnManualInput.setVisibility(View.GONE);
            checkBox.setVisibility(View.GONE);

        } else {
            btnManualInput.setVisibility(View.GONE);
            checkBox.setVisibility(View.VISIBLE);
        }


        textView.setText(trackingOrderCheckBoxesList.get(position).getTrackingNumber());
        // change the icon for Windows and iPhone

        return rowView;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        super.unregisterDataSetObserver(observer);
    }
}

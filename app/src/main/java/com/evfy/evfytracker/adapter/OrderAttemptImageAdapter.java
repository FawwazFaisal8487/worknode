package com.evfy.evfytracker.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.evfy.evfytracker.R;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by coolasia on 25/8/17.
 */

//http://www.thecrazyprogrammer.com/2016/12/android-image-slider-using-viewpager-example.html

public class OrderAttemptImageAdapter extends PagerAdapter {
    Context context;
    ArrayList<Bitmap> allOrderAttemptImage = new ArrayList<>();
    ArrayList<Integer> allOrderAttemptImageId = new ArrayList<>();
    ArrayList<String> allOrderAttemptImageDescription = new ArrayList<>();

    ArrayList<Bitmap> imagesUriList = new ArrayList<>();
    ArrayList<String> descriptionImage = new ArrayList<>();
    ArrayList<String> newImageUriListString = new ArrayList<>();
    LayoutInflater layoutInflater;
    Bitmap bmp;
    int id;
    TextView txtDescription;
    boolean fromUrl;


    public OrderAttemptImageAdapter(Context context, ArrayList<Bitmap> allOrderAttemptImage, ArrayList<Integer> allOrderAttemptImageId,
                                    ArrayList<String> allOrderAttemptImageDescription, ArrayList<Bitmap> imagesUriList, ArrayList<String> descriptionImage,
                                    boolean fromUrl, ArrayList<String> newImageUriListString) {
        this.context = context;
        this.allOrderAttemptImage = allOrderAttemptImage;
        this.allOrderAttemptImageId = allOrderAttemptImageId;
        this.allOrderAttemptImageDescription = allOrderAttemptImageDescription;
        this.imagesUriList = imagesUriList;
        this.descriptionImage = descriptionImage;
        this.fromUrl = fromUrl;
        this.newImageUriListString = newImageUriListString;


        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (fromUrl) {
            return allOrderAttemptImage.size();
        } else {
            return imagesUriList.size();
        }


    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item_order_attempt_image, container, false);

        TouchImageView imageView = itemView.findViewById(R.id.imageView);
        Button btnRemove = itemView.findViewById(R.id.btnRemove);
        Button btnEdit = itemView.findViewById(R.id.btnEdit);
        TextView txtPositionOfTotalImage = itemView.findViewById(R.id.txtPositionOfTotalImage);
        txtDescription = itemView.findViewById(R.id.txtDescription);
        if (fromUrl) {
            if (position == 0) {
                btnRemove.setVisibility(View.GONE);
                btnEdit.setVisibility(View.GONE);
            }

            txtPositionOfTotalImage.setText(position + 1 + " of " + allOrderAttemptImage.size());
            bmp = allOrderAttemptImage.get(position);
            txtDescription.setText(allOrderAttemptImageDescription.get(position));

        } else {
            txtPositionOfTotalImage.setText(position + 1 + " of " + imagesUriList.size());
            bmp = imagesUriList.get(position);
            txtDescription.setText(descriptionImage.get(position));
        }

        imageView.setImageBitmap(bmp);
        container.addView(itemView);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fromUrl) {
                    LogCustom.i("allOrderAttemptImageSize", "" + allOrderAttemptImage.size());
                    LogCustom.i("allOrderAttemptImageId", "" + allOrderAttemptImageId.size());
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Delete")
                            .setContentText("Are you sure want delete this image?")
                            .setCancelText("No")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmText("Yes")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    allOrderAttemptImage.remove(position);
                                    id = allOrderAttemptImageId.get(position);
                                    allOrderAttemptImageId.remove(position);
                                    allOrderAttemptImageDescription.remove(position);
                                    LogCustom.i("delete id : ", "" + id);
                                    notifyDataSetChanged();
                                }
                            })
                            .show();
                } else {
                    LogCustom.i("imagesUriList", "" + imagesUriList.size());
                    LogCustom.i("descriptionImage", "" + descriptionImage.size());
                    LogCustom.i("newImageUriListString", "" + newImageUriListString.size());
                    new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Delete")
                            .setContentText("Are you sure want delete this image?")
                            .setCancelText("No")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                }
                            })
                            .setConfirmText("Yes")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    imagesUriList.remove(position);
                                    descriptionImage.remove(position);
                                    newImageUriListString.remove(position);
                                    notifyDataSetChanged();
                                }
                            })
                            .show();

                }


            }

        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View alertLayout = layoutInflater.inflate(R.layout.items_layout_description_image, null);
                final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);


                textViewTitle.setText("Edit Description for Image");

                if (fromUrl) {
                    editTextDescription.setText(allOrderAttemptImageDescription.get(position));
                } else {
                    editTextDescription.setText(descriptionImage.get(position));
                }


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(alertLayout);

                builder.setCancelable(false);

                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                final AlertDialog dialog = builder.create();
                dialog.show();


                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogCustom.i("description", editTextDescription.getText().toString());
                        if (fromUrl) {
                            allOrderAttemptImageDescription.set(position, editTextDescription.getText().toString());
                        } else {
                            descriptionImage.set(position, editTextDescription.getText().toString());
                        }

                        notifyDataSetChanged();

                        dialog.dismiss();
                    }

                });


            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

package com.evfy.evfytracker.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.evfy.evfytracker.R;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class EditDeleteOrderAttemptAdapterNoInternet extends PagerAdapter {
    Context context;

    ArrayList<Bitmap> imagesUriList = new ArrayList<>();
    ArrayList<String> descriptionImage = new ArrayList<>();
    LayoutInflater layoutInflater;
    Bitmap bmp;
    int id;
    TextView txtDescription;
    boolean fromUrl;


    public EditDeleteOrderAttemptAdapterNoInternet(Context context, ArrayList<Bitmap> imagesUriList, ArrayList<String> descriptionImage) {
        this.context = context;
        this.imagesUriList = imagesUriList;
        this.descriptionImage = descriptionImage;


        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imagesUriList.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item_order_attempt_image, container, false);

        TouchImageView imageView = itemView.findViewById(R.id.imageView);
        Button btnRemove = itemView.findViewById(R.id.btnRemove);
        Button btnEdit = itemView.findViewById(R.id.btnEdit);
        TextView txtPositionOfTotalImage = itemView.findViewById(R.id.txtPositionOfTotalImage);
        txtDescription = itemView.findViewById(R.id.txtDescription);
        txtPositionOfTotalImage.setText(position + 1 + " of " + imagesUriList.size());

        btnRemove.setVisibility(View.VISIBLE);
        btnEdit.setVisibility(View.VISIBLE);

        bmp = imagesUriList.get(position);
        txtDescription.setText(descriptionImage.get(position));

        //Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(imageView);
        //Glide.with(context).load(imagesUriList.get(position)).into(imageView);
        imageView.setImageBitmap(bmp);
        container.addView(itemView);

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogCustom.i("imagesUriList", "" + imagesUriList.size());
                LogCustom.i("descriptionImage", "" + descriptionImage.size());
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Delete")
                        .setContentText("Are you sure want delete this image?")
                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .setConfirmText("Yes")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                imagesUriList.remove(position);
                                descriptionImage.remove(position);
                                notifyDataSetChanged();
                            }
                        })
                        .show();


            }

        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View alertLayout = layoutInflater.inflate(R.layout.items_layout_description_image, null);
                final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
                final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);


                textViewTitle.setText("Edit Description for Image");

                editTextDescription.setText(descriptionImage.get(position));


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(alertLayout);

                builder.setCancelable(false);

                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                final AlertDialog dialog = builder.create();
                dialog.show();


                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LogCustom.i("description", editTextDescription.getText().toString());

                        descriptionImage.set(position, editTextDescription.getText().toString());

                        notifyDataSetChanged();

                        dialog.dismiss();
                    }

                });


            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }


    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

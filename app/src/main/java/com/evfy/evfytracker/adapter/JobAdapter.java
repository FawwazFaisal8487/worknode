package com.evfy.evfytracker.adapter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.evfy.evfytracker.Activity.JobStepActivity.ManyJobStepsActivity;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.JobSteps;
import com.evfy.evfytracker.classes.OrderDatabase;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.JobHolder> {
    Context context;
    List<JobOrder> jobList;
    LayoutInflater inflater;
    SweetAlertDialog pd;
    boolean mIsViewOnly;
    Date selectedDate;
    boolean isSearching;
    HashMap<Integer, Boolean> checkBoxMap;

    public JobAdapter(Context context, List<JobOrder> jobList, Date selectedDate, boolean isSearching) {

        this.context = context;
//        sort(jobList);
        this.jobList = jobList;
        this.selectedDate = selectedDate;
        inflater = LayoutInflater.from(this.context);
        this.isSearching = isSearching;
        checkBoxMap = new HashMap<>();
        for (int i = 0; i < jobList.size(); i++) {
            checkBoxMap.put(jobList.get(i).getOrderId(), false);
        }
        LogCustom.i("isSearching", "" + isSearching);

        pd = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pd.setTitleText("Updating Job...");
        pd.setCancelable(false);
    }

    public void setIsViewOnly(final boolean isViewOnly) {
        mIsViewOnly = isViewOnly;
    }

    @NonNull
    @Override
    public JobHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_job_taskme, parent, false);
        return new JobHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull JobHolder holder, int position) {
        final JobOrder tempJob = jobList.get(position);
//        sortJobStepsBasedOnSequence(tempJob);
        holder.jobCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                tempJob.setSelected(b);
                checkBoxMap.put(tempJob.getOrderId(), b);
            }
        });
        holder.jobCheckBox.setChecked(checkBoxMap.get(tempJob.getOrderId()));
        final DatabaseHandlerJobs dbUpdateJobs = new DatabaseHandlerJobs(context);

        Log.i("", "job adapter service order database size:" + dbUpdateJobs.getAllJobs().size());


        if (dbUpdateJobs.getAllJobs().size() != 0) {

            for (int i = 0; i < dbUpdateJobs.getAllJobs().size(); i++) {


                final OrderDatabase orderDatabase = dbUpdateJobs.getAllJobs().get(i);
                Log.i("", "job adapter service order database:" + orderDatabase.getOrderJson());

                JSONArray orderIdArray = null;
                try {
                    orderIdArray = new JSONArray(orderDatabase.getOrderId());
                    for (int j = 0; j < orderIdArray.length(); j++) {

                        Log.i("", "job adapter order id:" + orderIdArray.get(j).toString() + "temp job order id:" + tempJob.getOrderId());


                        if (orderIdArray.get(j).toString().equalsIgnoreCase(String.valueOf(tempJob.getOrderId()))) {
                            //holder.jobStatus.setText("SUBMITTING");
                            holder.layoutNoInternet.setVisibility(View.VISIBLE);
                            holder.textNoInternet.setSelected(true);
                        } else {
                            holder.layoutNoInternet.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }


        /*Get order status id based on order status name*/
        final int assignedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.ASSIGNED_TEXT);
        final int ackowledgedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.ACKNOWLEDGED_TEXT);
        final int inprogressOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT);
        final int completedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.COMPLETED_TEXT);
        final int failedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.FAILED_TEXT);
        final int cancelledOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.CANCELLED_TEXT);
        final int selfcollectOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.SELFCOLLECT_TEXT);


        if (isSearching) {
            holder.checkBoxLayout.setVisibility(View.GONE);
        } else {
            holder.checkBoxLayout.setVisibility(View.VISIBLE);
        }

        if (tempJob.getDriverNotes().length() == 0) {
            holder.driverNotes.setVisibility(View.GONE);
        } else {
            holder.driverNotes.setVisibility(View.VISIBLE);
            holder.driverNotes.setText(tempJob.getDriverNotes());

        }

        if (tempJob.getOrderStatusId() == assignedOrderStatusIDFromDB) {
            // holder.checkBoxLayout.setVisibility(View.VISIBLE);

            if (isSearching) {
                holder.checkBoxLayout.setVisibility(View.GONE);
            } else {
                holder.checkBoxLayout.setVisibility(View.VISIBLE);
            }

        } else {
            holder.checkBoxLayout.setVisibility(View.GONE);
        }


        int totalCompleted = 0;

        if (tempJob.getJobType() != null && tempJob.getJobType().length() > 0) {
            holder.jobReferenceNumber.setText(tempJob.getOrderNumber() + " (" + tempJob.getJobType() + ")");

        } else {
            holder.jobReferenceNumber.setText(tempJob.getOrderNumber());


        }


        if (tempJob.getOrderSequenceTotal() == null || tempJob.getOrderSequenceTotal() == 0) {
            holder.layoutOrderSequence.setVisibility(View.GONE);
        } else {
            holder.layoutOrderSequence.setVisibility(View.VISIBLE);
            holder.order_sequence_total.setText("" + tempJob.getOrderSequenceTotal());
        }

        if (tempJob.getJobStepsArrayList().size() > 1) {

            for (int i = 0; i < tempJob.getJobStepsArrayList().size(); i++) {
                JobSteps jobSteps = tempJob.getJobStepsArrayList().get(i);
                if (jobSteps.getJob_step_status_id() == Constants.JOB_STEP_PENDING || jobSteps.getJob_step_status_id() == Constants.JOB_STEP_INPROGRESS) {
                    //holder.jobStepAddress.setText(tempJob.getJobStepsArrayList().get(tempJob.getJobSteps().length-1).getLocation());

                    if (tempJob.getJobStepsArrayList().get(i).getLocation().equalsIgnoreCase("")) {
                        holder.jobStepAddress.setText(" - ");
                    } else {
                        holder.jobStepAddress.setText(tempJob.getJobStepsArrayList().get(i).getLocation());
                    }
                    break;
                } else if (jobSteps.getJob_step_status_id() == Constants.JOB_STEP_CANCELLED || jobSteps.getJob_step_status_id() == Constants.JOB_STEP_FAILED) {
                    if (tempJob.getJobStepsArrayList().get(i).getLocation().equalsIgnoreCase("")) {
                        holder.jobStepAddress.setText(" - ");
                    } else {
                        holder.jobStepAddress.setText(tempJob.getJobStepsArrayList().get(i).getLocation());
                    }
                    break;
                } else {
                    if (tempJob.getJobStepsArrayList().get(i).getLocation().equalsIgnoreCase("")) {
                        holder.jobStepAddress.setText(" - ");
                    } else {
                        holder.jobStepAddress.setText(tempJob.getJobStepsArrayList().get(i).getLocation());
                    }
                    // break;
                }
            }


            //check order status id then check length job step , either job step status is completed
            if (assignedOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                holder.jobStepStatus.setText("0 of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.pendingJobStepColor));
            } else if (ackowledgedOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                holder.jobStepStatus.setText("0 of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.acknowledgedColor));
            } else if (inprogressOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                for (int i = 0; i < tempJob.getJobStepsArrayList().size(); i++) {
                    if (tempJob.getJobStepsArrayList().get(i).getJob_step_status_id() == Constants.JOB_STEP_COMPLETED) {
                        totalCompleted = totalCompleted + 1;
                    }
                }
                holder.jobStepStatus.setText(totalCompleted + " of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.inProgressColor));
            } else if (completedOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                holder.jobStepStatus.setText(tempJob.getJobStepsArrayList().size() + " of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.completedColor));
            } else if (failedOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                for (int i = 0; i < tempJob.getJobStepsArrayList().size(); i++) {
                    if (tempJob.getJobStepsArrayList().get(i).getJob_step_status_id() == Constants.JOB_STEP_COMPLETED) {
                        totalCompleted = totalCompleted + 1;
                    }
                }
                holder.jobStepStatus.setText(totalCompleted + " of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.failedColor));

            } else if (cancelledOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                for (int i = 0; i < tempJob.getJobStepsArrayList().size(); i++) {
                    if (tempJob.getJobStepsArrayList().get(i).getJob_step_status_id() == Constants.JOB_STEP_COMPLETED) {
                        totalCompleted = totalCompleted + 1;
                    }
                }
                holder.jobStepStatus.setText(totalCompleted + " of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.cancelledColor));
            } else if (selfcollectOrderStatusIDFromDB == tempJob.getOrderStatusId()) {
                for (int i = 0; i < tempJob.getJobStepsArrayList().size(); i++) {
                    if (tempJob.getJobStepsArrayList().get(i).getJob_step_status_id() == Constants.JOB_STEP_COMPLETED) {
                        totalCompleted = totalCompleted + 1;
                    }
                }
                holder.jobStepStatus.setText(totalCompleted + " of " + tempJob.getJobStepsArrayList().size() + " steps complete");
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.selfCollectColor));
            }

        } else if (tempJob.getJobStepsArrayList().size() == 1) {

            if (tempJob.getJobStepsArrayList().get(0).getLocation().equalsIgnoreCase("")) {
                holder.jobStepAddress.setText(" - ");
            } else {
                holder.jobStepAddress.setText(tempJob.getJobStepsArrayList().get(0).getLocation());
            }


            if (tempJob.getJobStepsArrayList().get(0).getJob_step_status_id() == Constants.JOB_STEP_PENDING) {
                holder.jobStepStatus.setText(Constants.JOB_STEP_PENDING_TEXT);
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.pendingJobStepColor));
            } else if (tempJob.getJobStepsArrayList().get(0).getJob_step_status_id() == Constants.JOB_STEP_INPROGRESS) {
                holder.jobStepStatus.setText(Constants.JOB_STEP_INPROGRESS_TEXT);
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.inProgressColor));
            } else if (tempJob.getJobStepsArrayList().get(0).getJob_step_status_id() == Constants.JOB_STEP_COMPLETED) {
                holder.jobStepStatus.setText(Constants.JOB_STEP_COMPLETED_TEXT);
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.completedColor));
            } else if (tempJob.getJobStepsArrayList().get(0).getJob_step_status_id() == Constants.JOB_STEP_FAILED) {
                holder.jobStepStatus.setText(Constants.JOB_STEP_FAILED_TEXT);
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.failedColor));
            } else if (tempJob.getJobStepsArrayList().get(0).getJob_step_status_id() == Constants.JOB_STEP_CANCELLED) {
                holder.jobStepStatus.setText(Constants.JOB_STEP_CANCELLED_TEXT);
                holder.jobStepStatus.setTextColor(context.getResources().getColor(R.color.cancelledColor));
            }
        }


        LogCustom.i("getDropOffDate", "" + tempJob.getDropOffDate());
        LogCustom.i("getDropOffTime", "" + tempJob.getDropOffTime());

//        String string = tempJob.getDropOffDate();
//        DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        try {
//            Date date = format.parse(string);
//            LogCustom.i("getDate", "" + date);
//
//            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
//            String currentDateString = sdf.format(date);
//            LogCustom.i("getcurrentDateString", "" + currentDateString);
//            holder.jobDropOffDate.setText(currentDateString);
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        if (tempJob.getDropOffTimePlanned().equalsIgnoreCase("") || tempJob.getDropOffTimePlanned() == null) {
            // holder.jobDropOffDate.setVisibility(View.GONE);

        } else {


            String strDate = tempJob.getDropOffTimePlanned();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date convertedDate = new Date();
            try {
                dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                convertedDate = dateFormat.parse(strDate);
                SimpleDateFormat sdfnewformat = new SimpleDateFormat("HH:mm a");
                String finalDateString = sdfnewformat.format(convertedDate);

                holder.jobDropOffDate.setText(finalDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }

        if (tempJob.getCompanyName().equalsIgnoreCase("") || tempJob.getCompanyName() == null) {
            if (tempJob.getDropOffContactName().equalsIgnoreCase("") || tempJob.getDropOffContactName() == null) {
                holder.jobCompanyName.setText(" - ");
            } else {
                holder.jobCompanyName.setText(tempJob.getDropOffContactName());
            }
        } else {
            holder.jobCompanyName.setText(tempJob.getCompanyName());
        }

        if (tempJob.getExtraData() != null && !tempJob.getExtraData().isEmpty()) {
            holder.extraData.setVisibility(View.VISIBLE);
            holder.extraData.setText(tempJob.getExtraData());
        } else {
//            holder.extraData.setVisibility(View.GONE);
        }

        if (tempJob.getDropOffDesc().equalsIgnoreCase("")) {
            holder.jobDescription.setText(" - ");

        } else {
            holder.jobDescription.setText(tempJob.getDropOffDesc());
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Log.i("internetConnection", "yes");

                boolean showAddOrderItemIcon;

                LogCustom.i("", "item view job adapter isdelivery:" + tempJob.isDelivery());
                Intent intent = new Intent().setClass(v.getContext(), ManyJobStepsActivity.class);
                // intent.putExtra(OrderDetailsActivity.INTENT_EXTRA_IS_VIEW_ONLY, mIsViewOnly);
                intent.putExtra("OrderId", tempJob.getOrderId());
                intent.putExtra("drop_off_worker_id", tempJob.getDropOffWorkerId());
                intent.putExtra("wms_order_id", tempJob.getWmsOrderId());
                intent.putExtra("order_status_id", tempJob.getOrderStatusId());
                intent.putExtra("IsDelivery", tempJob.isDelivery());
                intent.putExtra("order_number", tempJob.getOrderNumber());
                intent.putExtra("SelectedDate", selectedDate);

                if (tempJob.getOrderStatusId() == ackowledgedOrderStatusIDFromDB || tempJob.getOrderStatusId() == inprogressOrderStatusIDFromDB || tempJob.getOrderStatusId() == selfcollectOrderStatusIDFromDB) {
                    showAddOrderItemIcon = false;
                } else {
                    showAddOrderItemIcon = false;
                }

                intent.putExtra("showAddOrderItemIcon", showAddOrderItemIcon);


                ((Activity) v.getContext()).startActivityForResult(intent, 1);
                ((Activity) v.getContext()).overridePendingTransition(R.anim.open_next, R.anim.close_main);

            }
        });
    }

    public void clearList() {
        jobList.clear();
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    public void addNewData(List<JobOrder> jobList) {
        this.jobList.addAll(jobList);
        notifyDataSetChanged();
    }

    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    void sort(final List<JobOrder> orders) {

        try {

            Collections.sort(orders, new Comparator<JobOrder>() {

                @Override
                public int compare(final JobOrder lhs, final JobOrder rhs) {

                    if (lhs == null && rhs == null) return 0;
                    if (lhs == null) return -1;
                    if (rhs == null) return 1;

                    if (lhs.getOrderStatusId() == Constants.COMPLETE) {
                        return compare(rhs.getDropOffTimeEnd(), lhs.getDropOffTimeEnd());
                    }

                    return compare(lhs.getDropOffTime(), rhs.getDropOffTime());

                }

                int compare(final String dateLHS, final String dateRHS) {
                    if (dateLHS == null && dateRHS == null) return 0;
                    if (dateLHS == null) return -1;
                    if (dateRHS == null) return 1;
                    return dateLHS.compareTo(dateRHS);
                }

            });

        } catch (final Throwable e) {
            LogCustom.e(e);
        }

    }

    void sortJobStepsBasedOnSequence(JobOrder orders) {
        try {

            Collections.sort(orders.getJobStepsArrayList(), new Comparator<JobSteps>() {

                @Override
                public int compare(final JobSteps lhs, final JobSteps rhs) {

                    if (lhs == null && rhs == null) return 0;
                    if (lhs == null) return -1;
                    if (rhs == null) return 1;

                    int lowOrderSequence = lhs.getOrder_sequence();
                    int rightOrderSequence = rhs.getOrder_sequence();
                    if (lowOrderSequence > rightOrderSequence) {
                        return 1;
                    } else if (lowOrderSequence < rightOrderSequence) {
                        return -1;
                    } else return 0;

                    //return compare(lhs.getOrder_sequence(), rhs.getOrder_sequence());
                }

            });

        } catch (final Throwable e) {
            LogCustom.e(e);
        }
    }

    public static class JobHolder extends RecyclerView.ViewHolder {
        CardView card_view;
        TextView jobStepAddress;
        TextView jobStepStatus;
        TextView jobCompanyName;
        TextView jobDescription;
        TextView jobReferenceNumber;
        TextView order_sequence_total;
        LinearLayout layoutOrderSequence;
        TextView jobDropOffDate;
        TextView extraData;
        TextView driverNotes;
        LinearLayout layoutNoInternet;
        TextView textNoInternet;
        RelativeLayout checkBoxLayout;
        CheckBox jobCheckBox;

        public JobHolder(View view) {
            super(view);
            card_view = view.findViewById(R.id.card_view);
            jobStepAddress = view.findViewById(R.id.jobStepAddress);
            jobStepStatus = view.findViewById(R.id.jobStepStatus);
            jobCompanyName = view.findViewById(R.id.jobCompanyName);
            jobDescription = view.findViewById(R.id.jobDescription);
            jobReferenceNumber = view.findViewById(R.id.jobReferenceNumber);
            order_sequence_total = view.findViewById(R.id.order_sequence_total);
            layoutOrderSequence = view.findViewById(R.id.layoutOrderSequence);
            jobDropOffDate = view.findViewById(R.id.jobDropOffDate);
            extraData = view.findViewById(R.id.extraData);
            driverNotes = view.findViewById(R.id.driverNotes);
            layoutNoInternet = view.findViewById(R.id.layoutNoInternet);
            textNoInternet = view.findViewById(R.id.textNoInternet);
            checkBoxLayout = view.findViewById(R.id.checkBoxLayout);
            jobCheckBox = view.findViewById(R.id.checkBox1);
        }
    }

}

package com.evfy.evfytracker;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.Activity.JobStepActivity.ManyJobStepsActivity;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.NotificationDatabase;
import com.evfy.evfytracker.fragment.JobFragment;
import com.evfy.evfytracker.fragment.NotificationListingFragment;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

import me.leolin.shortcutbadger.ShortcutBadger;

/**
 * Created by coolasia on 18/10/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    Class cls;
    SharedPreferences settings;
    DatabaseHandlerJobs dbUpdateJobs;
    boolean foregroud;
    NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        // Check if message contains a data payload.
        Log.d(TAG, "REMOTE MESSAGE: " + remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            String message = "", orderId = "", title = "", reference_no = "", updated_at = "";
            boolean isJobRemoved = false, job_destroyed = false, isAfterRemove = false;
            JSONArray job_attributes_updated = new JSONArray();

            Log.i("notification dataChat", "notification data chat" + remoteMessage.getData().toString());
            try {
                Map<String, String> params = remoteMessage.getData();
                JSONObject object = new JSONObject(params);
                Log.i("notificationJSON_OBJECT", "data json object 00:" + object.toString());

                //JSONObject dataObject = new JSONObject(object.getString("data"));

                JSONObject dataObject = new JSONObject(object.getString("notification"));


                message = dataObject.getString("body");
                title = dataObject.getString("title");
                orderId = object.getString("order_id");
                reference_no = object.getString("reference_no");


                try {

                    if (object.has("updated_at") && !(object.isNull("updated_at"))) {
                        updated_at = object.getString("updated_at");
                    } else {
                        updated_at = "";
                    }

                    if (object.has("job_removed") && !(object.isNull("job_removed"))) {
                        isJobRemoved = object.getBoolean("job_removed");
                    } else {
                        isJobRemoved = false;
                    }

                    isAfterRemove = true;

                    if (object.has("job_attributes_updated") && !(object.isNull("job_attributes_updated"))) {

                        job_attributes_updated = new JSONArray(object.getString("job_attributes_updated"));

                    }

                    if (object.has("job_destroyed") && !(object.isNull("job_destroyed"))) {
                        job_destroyed = object.getBoolean("job_destroyed");
                    } else {
                        job_destroyed = false;
                    }

                    LogCustom.i("dataFromJSON", "notification title:" + title + " body:" + message + " order id:" + orderId + "job_attributes_updated" + job_attributes_updated.length());

                    sendNotification(message, orderId, title, reference_no, isJobRemoved, job_attributes_updated, job_destroyed, updated_at);


                } catch (Exception e) {
                    LogCustom.i("dataFromJSON", "notification title:" + title + " body:" + message + " order id:" + orderId + "job_attributes_updated" + job_attributes_updated.length());

                    if (!isAfterRemove || !isJobRemoved) {
                        if (object.has("job_destroyed") && !(object.isNull("job_destroyed"))) {
                            job_destroyed = object.getBoolean("job_destroyed");
                        } else {
                            job_destroyed = false;
                        }
                    }

                    sendNotification(message, orderId, title, reference_no, isJobRemoved, job_attributes_updated, job_destroyed, updated_at);

                }


            } catch (Exception e) {
                Log.i("", "json exception:" + e);
            }


            //handleFirebaseNotificationIntent(remoteMessage.getData().get("click_action"));
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //AdvancedLog.i("get Notification","");
            //  remoteMessage.getNotification().getClickAction();
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            //            AdvancedLog.i("Message Notification Body: " + remoteMessage.getNotification().getBody());
            //sendNotification(remoteMessage.getNotification().getBody());
        }


        /// sendNotification(remoteMessage.getNotification().getBody());
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    class ForegroundCheckTask extends AsyncTask<Context, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Context... params) {
            final Context context = params[0].getApplicationContext();
            return isAppOnForeground(context);
        }

        private boolean isAppOnForeground(Context context) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
            if (appProcesses == null) {
                return false;
            }
            final String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
                if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                    return true;
                }
            }
            return false;
        }
    }

    private String getDate(String ourDate) {
        try {

            LogCustom.i("ourDate", "11" + ourDate);
            TimeZone defaultTimeZone = TimeZone.getDefault();
            String strDefaultTimeZone = defaultTimeZone.getDisplayName(false, TimeZone.SHORT);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(ourDate);

            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getDefault());
            // dateFormatter.setTimeZone(TimeZone.getTimeZone(strDefaultTimeZone));
            ourDate = dateFormatter.format(value);

            LogCustom.i("ourDate", "22" + ourDate);

            //Log.d("ourDate", ourDate);
        } catch (Exception e) {
            ourDate = "0000-00-00 00:00";
        }
        return ourDate;


    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(final String messageBody, final String OrderId, final String title, final String reference_no, final boolean isJobRemoved, final JSONArray job_attributes_updated, final boolean job_destroyed,
                                  final String updated_at) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            public void run() {

                try {

                    dbUpdateJobs = new DatabaseHandlerJobs(getApplicationContext());

                    NotificationDatabase notificationDatabase = new NotificationDatabase();
                    notificationDatabase.setOrderId(OrderId);
                    notificationDatabase.setReferenceNumber(reference_no);
                    notificationDatabase.setTitleNotification(title);
                    notificationDatabase.setContentNotification(messageBody);
                    notificationDatabase.setAttributeUpdated(String.valueOf(job_attributes_updated));
                    notificationDatabase.setDoneRead(0);

                    Long tsLong = System.currentTimeMillis() / 100;
                    String timeCreated = tsLong.toString();
                    LogCustom.i("timeCreated", "is : " + timeCreated);
                    notificationDatabase.setTimeCreated(timeCreated);

                    JobOrder jobOrder = new JobOrder();
                    jobOrder.setOrderId(Integer.parseInt(OrderId));
                    jobOrder.setReference_no(reference_no);

                    if (!isJobRemoved) {
                        notificationDatabase.setJobUpdated(0);
                    } else {
                        notificationDatabase.setJobUpdated(1);
                    }

                    if (!job_destroyed) {
                        notificationDatabase.setJobDestroy(0);
                    } else {
                        notificationDatabase.setJobDestroy(1);
                    }

                    if (isJobRemoved || job_destroyed) {
                        jobOrder.setRemoved(true);


                        dbUpdateJobs.deleteJobOrderBasedOnOrderID(jobOrder.getOrderId());
                        LogCustom.i("deleteJobOrderBasedOnOrderID", "yes");

                    } else {
                        jobOrder.setRemoved(false);
                    }

                    dbUpdateJobs.addNotification(notificationDatabase, false, false);

                    String dateUpdated = "";
                    dateUpdated = updated_at;

                    if (!dateUpdated.isEmpty() || !dateUpdated.equalsIgnoreCase("")) {
                        String dateBasedDevice = getDate(dateUpdated);
                        jobOrder.setDropOffTime(dateBasedDevice);

                        if (dbUpdateJobs.isNeedUpdatedJobIntoDBLatest(jobOrder.getOrderId(), jobOrder.getDropOffTime())) {
                            dbUpdateJobs.addJobLatest(jobOrder);
                            LogCustom.i("addJobLatest", "yesss");
                        } else {
                            LogCustom.i("addJobLatest", "noooo");
                        }
                    }


                    LogCustom.i("messageBody", "is : " + messageBody);
                    LogCustom.i("title", "is : " + title);
                    LogCustom.i("isJobRemoved", "is : " + isJobRemoved);
                    LogCustom.i("job_destroyed", "is : " + job_destroyed);
                    LogCustom.i("job_attributes_updated", "lenght : " + job_attributes_updated.toString());
                    String dataChanges = "";

                    for (int i = 0; i < job_attributes_updated.length(); i++) {
                        try {
                            dataChanges = (String) job_attributes_updated.get(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        LogCustom.i("dataChanges", "is : " + dataChanges);
                    }

                    ShortcutBadger.applyCount(getApplicationContext(), dbUpdateJobs.getTotalNotificationUnRead());

                    /*This function for detect either user inside app and inside fragment, then it will auto reload the app again. foregroud mean inside app*/

                    Intent ii = new Intent(DrawerActivity.ACTION_REFRESH_NOTIFICATION_SIDE_MENU);
                    LocalBroadcastManager.getInstance(getApplicationContext())
                            .sendBroadcast(ii);


                    foregroud = new ForegroundCheckTask().execute(getApplicationContext()).get();
                    LogCustom.i("foregroud", "" + foregroud);
                    if (foregroud) {

                        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        boolean insideFragmentPage = settings.getBoolean("insideFragmentPage", true);

                        if (insideFragmentPage) {
//                            Intent intent = new Intent(MyFirebaseMessagingService.this, DrawerActivity.class);
//                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
//                          //  intent.addFlags( Intent.FLAG_ACTIVITY_NO_HISTORY );
//                            startActivity( intent );

                            Intent i = new Intent(JobFragment.ACTION_REFRESH_JOB_LISTING);
                            LocalBroadcastManager.getInstance(getApplicationContext())
                                    .sendBroadcast(i);

                        } else {
                            boolean insideJobDetails = settings.getBoolean("insideJobDetails", false);
                            boolean insideNotificationListing = settings.getBoolean("insideNotificationListing", false);
                            int orderIDinsideJobDetails = settings.getInt("orderIDinsideJobDetails", 0);
                            if (insideJobDetails && (String.valueOf(orderIDinsideJobDetails).equalsIgnoreCase(OrderId))) {
                                // inside job detail and see same order id
                                // need reload job detail
                                LogCustom.i("need reload job detail", "& show inside ");

                                Intent i = new Intent(ManyJobStepsActivity.ACTION_REFRESH_JOB_DETAIL);
                                i.putExtra("isJobRemoved", isJobRemoved);
                                i.putExtra("job_destroyed", job_destroyed);
                                i.putExtra("job_attributes_updated", job_attributes_updated.toString());
                                LocalBroadcastManager.getInstance(getApplicationContext())
                                        .sendBroadcast(i);
                            } else if (insideNotificationListing) {
                                Intent i = new Intent(NotificationListingFragment.ACTION_REFRESH_NOTIFICATION_LISTING);
                                LocalBroadcastManager.getInstance(getApplicationContext())
                                        .sendBroadcast(i);
                            }
                        }

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


            }
        });


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        //        Notification notification = new NotificationCompat.Builder(this)
        //                .setSmallIcon(R.drawable.ic_stat_ic_notification)
        //                .setContentTitle("BikeBulance")
        //                .setContentText(messageBody)
        //                .setAutoCancel(true)
        //                .setSound(defaultSoundUri)
        //                .setContentIntent(pendingIntent).build();
        //        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        //        nm.notify(8 /* ID of notification */, notification);

        Intent intent = new Intent(MyFirebaseMessagingService.this, DrawerActivity.class);
        //  Intent intent = getPreviousIntent();
        LogCustom.i("setnotificationParams", "true");
//        settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        settings.edit().putBoolean("fromPushNotification", true).apply();
//        settings.edit().putString("OrderIdfromPushNotification", OrderId).apply();
//        settings.edit().putString("OrderNumberfromPushNotification", reference_no).apply();
        intent.putExtra("OrderId", OrderId);
        intent.putExtra("order_number", reference_no);
        intent.putExtra("isJobRemoved", isJobRemoved);
        intent.putExtra("job_destroyed", job_destroyed);
        intent.putExtra("fromPushNotification", true);
        // intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(DrawerActivity.class);
        stackBuilder.addNextIntent(intent);

        int notificationId = (int) ((new Date().getTime() / 100L) % Integer.MAX_VALUE);


        try {
            foregroud = new ForegroundCheckTask().execute(getApplicationContext()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (foregroud) {
            LogCustom.i("foregroud111", "true");
            // PendingIntent pedningIntent =stackBuilder.getPendingIntent(123,PendingIntent.FLAG_UPDATE_CURRENT);
//            PendingIntent pedningIntent =stackBuilder.getPendingIntent(notificationId,PendingIntent.FLAG_ONE_SHOT);
//            mBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this,"com.logisfleet.taskme");
//            mBuilder.setSmallIcon(R.drawable.ic_notification)
//                    .setContentTitle(title)
//                    .setContentText(messageBody)
//                    .setSound(defaultSoundUri)
//                    .setDefaults(Notification.DEFAULT_ALL)
//                    .setPriority(Notification.PRIORITY_HIGH)
//                    .setContentIntent(pedningIntent);
//            mBuilder.setAutoCancel(true);
//            mBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
//            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // mNotificationManager.notify(1, mBuilder.build());

            Intent i = new Intent(DrawerActivity.ACTION_DISPLAY_IN_APP_NOTIFICATION);
            i.putExtra("title", title);
            i.putExtra("message", messageBody);
            i.putExtra("title", title);
            i.putExtra("title", title);
            i.putExtra("OrderId", OrderId);
            i.putExtra("order_number", reference_no);
            i.putExtra("fromPushNotification", true);
            LocalBroadcastManager.getInstance(getApplicationContext())
                    .sendBroadcast(i);


        } else {
            LogCustom.i("foregroud111", "false");
            // PendingIntent pedningIntent =stackBuilder.getPendingIntent(123,PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent pedningIntent = stackBuilder.getPendingIntent(notificationId, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
            mBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this, "com.logisfleet.taskme");
            mBuilder.setSmallIcon(R.drawable.ic_notification)
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pedningIntent);
            mBuilder.setAutoCancel(true);
            mBuilder.getNotification().flags |= Notification.FLAG_AUTO_CANCEL;
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel("com.logisfleet.taskme", "My Notifications", NotificationManager
                        .IMPORTANCE_MAX);

                // Configure the notification channel.
                notificationChannel.setDescription("Channel description");
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                notificationChannel.enableVibration(true);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }

            int notificationIdNotify = (int) ((new Date().getTime() / 100L) % Integer.MAX_VALUE);
            mNotificationManager.notify(notificationIdNotify, mBuilder.build());
            // mNotificationManager.notify(1, mBuilder.build());
        }


        // int notificationIdNotify = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        //  mNotificationManager.notify(notificationIdNotify, mBuilder.build());
    }


    private void handleFirebaseNotificationIntent(Intent intent) {
        String className = intent.getStringExtra("run_activity");
        startSelectedActivity(className, intent.getExtras());
    }

    private void startSelectedActivity(String className, Bundle extras) {

        try {
            cls = Class.forName(className);
        } catch (ClassNotFoundException e) {

        }
        Intent i = new Intent(getApplicationContext(), cls);

        if (i != null) {
            i.putExtras(extras);
            this.startActivity(i);
        }
    }

    private Intent getPreviousIntent() {
        Intent newIntent = null;
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            final List<ActivityManager.AppTask> recentTaskInfos = activityManager.getAppTasks();
            if (!recentTaskInfos.isEmpty()) {
                for (ActivityManager.AppTask appTaskTaskInfo : recentTaskInfos) {
                    if (appTaskTaskInfo.getTaskInfo().baseIntent.getComponent().getPackageName().equals(getApplicationContext().getPackageName())) {
                        newIntent = appTaskTaskInfo.getTaskInfo().baseIntent;
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                }
            }
        } else {
            final List<ActivityManager.RecentTaskInfo> recentTaskInfos = activityManager.getRecentTasks(1024, 0);
            if (!recentTaskInfos.isEmpty()) {
                for (ActivityManager.RecentTaskInfo recentTaskInfo : recentTaskInfos) {
                    if (recentTaskInfo.baseIntent.getComponent().getPackageName().equals(getApplicationContext().getPackageName())) {
                        newIntent = recentTaskInfo.baseIntent;
                        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                }
            }
        }
        if (newIntent == null) newIntent = new Intent();
        return newIntent;
    }
}



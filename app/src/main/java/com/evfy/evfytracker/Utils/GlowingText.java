package com.evfy.evfytracker.Utils;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class GlowingText {

	private final Activity activity;

	private final View view;

	private float startGlowRadius;
	private float minGlowRadius;
	private float maxGlowRadius;
	private float currentGlowRadius = startGlowRadius;
	private final float dx = 0f;
	private final float dy = 0f;
	private boolean isAutoStartGlowing = true;
	private boolean isGlowing = false;
	private int glowingCount = -1;

	private int glowColor = 0xFFffffff, // ffffff defines hexadecimal value of color
			glowSpeed;

	private boolean isDirectionUp = true; // Whether radius should increase or Decrease.

	private Handler handler;
	private Runnable r;

	/**
	 * @param v           The TextView or Button that needs glowing effect.
	 * @param minRadius   Normally 3f - 30f.
	 * @param maxRadius   Normally 3f - 30f.
	 * @param startRadius Normally 3f - 30f.
	 * @param speed       Normally 1.
	 */
	public GlowingText(Activity mActivity, Context context, View v, float minRadius, float maxRadius,
					   float startRadius, int color, int speed, boolean isAutoStartGlowing, int glowingCount) {
		this.activity = mActivity;
		this.view = v;
		this.minGlowRadius = minRadius;
		this.maxGlowRadius = maxRadius;
		this.startGlowRadius = startRadius;
		this.glowColor = color;
		this.glowSpeed = speed;
		this.glowingCount = glowingCount;
		this.isAutoStartGlowing = isAutoStartGlowing;

		if (!(view instanceof TextView) && !(view instanceof Button)) {
			Toast.makeText(context, view.getClass().getName() + " view does not support Glowy Text Animation.",
					Toast.LENGTH_SHORT).show();
			return;
		} else {
			if (startGlowRadius < minGlowRadius || startGlowRadius > maxGlowRadius) {
				Random r = new Random();
				int start = r.nextInt((int) maxGlowRadius - (int) minGlowRadius + 1) + (int) minGlowRadius;
				startGlowRadius = start;
			}

			// Scale Up Glowing Transition as milliseconds
			glowSpeed *= 25;
			if (isAutoStartGlowing) startGlowing();

		}
	}

	public void startGlowing() {

		if (isGlowing) return;
		isGlowing = true;

		handler = new Handler();
		r = new Runnable() {

			public void run() {

				// Check Which View Is this
				if (view instanceof TextView) {
					((TextView) view).setShadowLayer(currentGlowRadius, dx, dy, glowColor);
				} else if (view instanceof Button) {
					((Button) view).setShadowLayer(currentGlowRadius, dx, dy, glowColor);
				}

				/*
				 * currentGlowRadius - Glow radius. dx - Spread of Shadow in X direction dy -
				 * Spread of Shadow in Y direction color - Color used to create Glow (White in our
				 * case )
				 */

				if (isDirectionUp) {
					/* Increase Glow Radius by 1 */
					if (currentGlowRadius < maxGlowRadius) {
						/* Maximun has not reached. Carry On */
						currentGlowRadius++;
					} else {
						/*
						 * Oops !! Max is reached. Stars decreasing glow radius now. Change
						 * the Direction to Down.
						 */
						isDirectionUp = false;
					}
				} else {
					/* Decrease Glow Radius by 1 */
					if (currentGlowRadius > minGlowRadius) {
						/* Minimum has not reached yet. Keep Decreasing */
						currentGlowRadius--;
					} else {
						/*
						 * Oops !! Min is reached. Stars Increasing glow radius again. Change
						 * the Direction to UP
						 */
						isDirectionUp = true;
					}
				}
				// Keep Looping

				if (glowingCount != 1) {
					if (glowingCount != -1) glowingCount--;
					handler.postDelayed(this, glowSpeed);
				}
			}
		};

		// Starts Animation
		handler.postDelayed(r, glowSpeed);
	}

	public void setStartGlowRadius(final float startRadius) {
		activity.runOnUiThread(new Runnable() {

			public void run() {
				startGlowRadius = startRadius;
			}
		});
	}

	public void setMaxGlowRadius(final float maxRadius) {
		activity.runOnUiThread(new Runnable() {

			public void run() {
				maxGlowRadius = maxRadius;
			}
		});
	}

	public void setMinGlowRadius(final float minRadius) {
		activity.runOnUiThread(new Runnable() {

			public void run() {
				minGlowRadius = minRadius;
			}
		});
	}

	public void setTransitionSpeed(final int speed) {
		activity.runOnUiThread(new Runnable() {

			public void run() {
				glowSpeed = speed;
			}
		});
	}

	public void setGlowColor(final int color) {
		activity.runOnUiThread(new Runnable() {

			public void run() {
				glowColor = color;
			}
		});
	}

	public float getStartGlowRadius() {
		return this.startGlowRadius;
	}

	public float getMaxGlowRadius() {
		return this.maxGlowRadius;
	}

	public float getMinGlowRadius() {
		return this.minGlowRadius;
	}

	public float getCurrentGlowRadius() {
		return this.currentGlowRadius;
	}

	public int getTransitionSpeed() {
		return this.glowSpeed;
	}

	public String getGlowColor() {
		return String.format("#%X", glowColor);
	}

	public void stopGlowing() {
		/* Destroy the Runnable r */
		isGlowing = false;
		handler.removeCallbacks(r);
	}

}

package com.evfy.evfytracker.Utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.evfy.evfytracker.R;
import com.lkh012349s.mobileprinter.Utils.JavaOp;

public class FButton extends Button implements View.OnTouchListener {

	boolean isShadowColorDefined = false;
	//Custom values
	private boolean isShadowEnabled = true;
	private final boolean isTouched = false;

	private int mButtonColor;
	private int mShadowColor;
	private int mShadowHeight;
	private int mCornerRadius;
	//Native values
	private int mPaddingLeft;
	private int mPaddingRight;
	private int mPaddingTop;
	private int mPaddingBottom;
	//Background drawable
	private Drawable drawablePressed;
	private Drawable drawableNormal;
	private Drawable drawableDisabled;

	public FButton(Context context) {
		super(context);
		init();
		this.setOnTouchListener(this);
	}

	public FButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
		parseAttrs(context, attrs);
		this.setOnTouchListener(this);
	}

	public FButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
		parseAttrs(context, attrs);
		this.setOnTouchListener(this);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		if (drawableDisabled == null) return;
		final Drawable drawable = enabled ? drawableNormal : drawableDisabled;
		updateBackground(drawable);
		this.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom + mShadowHeight);
	}

	public int getShadowColor() {
		return mShadowColor;
	}

	public void setShadowColor(int shadowColor) {
		this.mShadowColor = shadowColor;
		isShadowColorDefined = true;
		refresh();
	}

	public void refresh() {
		float[] hsv = new float[3];
		Color.colorToHSV(mButtonColor, hsv);
		hsv[2] *= 0.8f; // value component
		//if shadow color was not defined, generate shadow color = 80% brightness
		if (!isShadowColorDefined) {
			mShadowColor = Color.HSVToColor(hsv);
		}
		//Create pressed background and unpressed background drawables
		if (isShadowEnabled) {
			drawablePressed = createDrawable(mCornerRadius, mButtonColor, Color.TRANSPARENT, false);
			drawableNormal = createDrawable(mCornerRadius, mButtonColor, mShadowColor, true);
		} else {
			mShadowHeight = 0;
			drawablePressed = createDrawable(mCornerRadius, mShadowColor, mShadowColor, true);
			drawableNormal = createDrawable(mCornerRadius, mButtonColor, mButtonColor, true);
		}
		drawableDisabled = createDrawable(mCornerRadius, mShadowColor, mShadowColor, true);
		updateBackground(isEnabled() ? drawableNormal : drawableDisabled);
		//Set padding
		this.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom + mShadowHeight);
	}

	public int getButtonColor() {
		return mButtonColor;
	}

	public void setButtonColor(int buttonColor) {
		this.mButtonColor = buttonColor;
		refresh();
	}

	public int getCornerRadius() {
		return mCornerRadius;
	}

	public void setCornerRadius(int cornerRadius) {
		this.mCornerRadius = cornerRadius;
		refresh();
	}

	public int getShadowHeight() {
		return mShadowHeight;
	}

	public void setShadowHeight(int shadowHeight) {
		this.mShadowHeight = shadowHeight;
		refresh();
	}

	//Getter
	public boolean isShadowEnabled() {
		return isShadowEnabled;
	}

	//Setter
	public void setShadowEnabled(boolean isShadowEnabled) {
		this.isShadowEnabled = isShadowEnabled;
		setShadowHeight(0);
		refresh();
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {

		switch (motionEvent.getAction()) {
			case MotionEvent.ACTION_DOWN:
				updateBackground(drawablePressed);
				this.setPadding(mPaddingLeft, mPaddingTop + mShadowHeight, mPaddingRight, mPaddingBottom);
				break;
			case MotionEvent.ACTION_MOVE:
				boolean isInsideButton = JavaOp.between((int) motionEvent.getX(), 0, view.getWidth(), true) && JavaOp.between(
						(int) motionEvent.getY(), 0, view.getHeight(), true);
				if (!isInsideButton) {
					updateBackground(drawableNormal);
					this.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom + mShadowHeight);
				}
				break;
			case MotionEvent.ACTION_OUTSIDE:
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				updateBackground(drawableNormal);
				this.setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom + mShadowHeight);
				break;
		}

		return false;

	}

	public void setFButtonPadding(int left, int top, int right, int bottom) {
		mPaddingLeft = left;
		mPaddingRight = right;
		mPaddingTop = top;
		mPaddingBottom = bottom;
		refresh();
	}

	private void init() {
		//Init default values
		isShadowEnabled = true;
		Resources resources = getResources();
		if (resources == null) return;
		mButtonColor = resources.getColor(R.color.fbutton_default_color);
		mShadowColor = resources.getColor(R.color.fbutton_default_shadow_color);
		mShadowHeight = resources.getDimensionPixelSize(R.dimen.fbutton_default_shadow_height);
		mCornerRadius = resources.getDimensionPixelSize(R.dimen.fbutton_default_conner_radius);
	}

	private void parseAttrs(Context context, AttributeSet attrs) {
		//Load from custom attributes
		TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FButton);
		if (typedArray == null) return;
		for (int i = 0; i < typedArray.getIndexCount(); i++) {
			int attr = typedArray.getIndex(i);
			if (attr == R.styleable.FButton_shadowEnabled) {
				isShadowEnabled = typedArray.getBoolean(attr, true); //Default is true
			} else if (attr == R.styleable.FButton_buttonColor) {
				mButtonColor = typedArray.getColor(attr, Color.GRAY);
			} else if (attr == R.styleable.FButton_shadowColor) {
				mShadowColor = typedArray.getColor(attr, Color.BLACK);
				isShadowColorDefined = true;
			} else if (attr == R.styleable.FButton_shadowHeight) {
				mShadowHeight = typedArray.getDimensionPixelSize(attr, R.dimen.fbutton_default_shadow_height);
			} else if (attr == R.styleable.FButton_cornerRadius) {
				mCornerRadius = typedArray.getDimensionPixelSize(attr, R.dimen.fbutton_default_conner_radius);
			}
		}
		typedArray.recycle();

		//Get paddingLeft, paddingRight
		int[] attrsArray = new int[]{android.R.attr.paddingLeft,  // 0
				android.R.attr.paddingRight, // 1
		};
		TypedArray ta = context.obtainStyledAttributes(attrs, attrsArray);
		if (ta == null) return;
		mPaddingLeft = ta.getDimensionPixelSize(0, 0);
		mPaddingRight = ta.getDimensionPixelSize(1, 0);
		ta.recycle();

		//Get paddingTop, paddingBottom
		int[] attrsArray2 = new int[]{android.R.attr.paddingTop,   // 0
				android.R.attr.paddingBottom,// 1
		};
		TypedArray ta1 = context.obtainStyledAttributes(attrs, attrsArray2);
		if (ta1 == null) return;
		mPaddingTop = ta1.getDimensionPixelSize(0, 0);
		mPaddingBottom = ta1.getDimensionPixelSize(1, 0);
		ta1.recycle();
	}

	private void updateBackground(Drawable background) {
		if (background == null) return;
		//Set button background
		if (Build.VERSION.SDK_INT >= 16) {
			this.setBackground(background);
		} else {
			this.setBackgroundDrawable(background);
		}
	}

	private LayerDrawable createDrawable(int radius, int topColor, int bottomColor, boolean isMainLayerUpper) {

		float[] outerRadius = new float[]{radius, radius, radius, radius, radius, radius, radius, radius};

		//Top
		RoundRectShape topRoundRect = new RoundRectShape(outerRadius, null, null);
		ShapeDrawable topShapeDrawable = new ShapeDrawable(topRoundRect);
		topShapeDrawable.getPaint().setColor(topColor);
		//Bottom
		RoundRectShape roundRectShape = new RoundRectShape(outerRadius, null, null);
		ShapeDrawable bottomShapeDrawable = new ShapeDrawable(roundRectShape);
		bottomShapeDrawable.getPaint().setColor(bottomColor);
		//Create array
		Drawable[] drawArray = {bottomShapeDrawable, topShapeDrawable};
		LayerDrawable layerDrawable = new LayerDrawable(drawArray);

		layerDrawable.setLayerInset(0, 0, 0, 0, 0);
		if (isMainLayerUpper) {
			if (isShadowEnabled) layerDrawable.setLayerInset(1, 0, 0, 0, mShadowHeight);
			else layerDrawable.setLayerInset(1, 0, 0, 0, 0);
		} else {
			layerDrawable.setLayerInset(1, 0, mShadowHeight, 0, 0);
		}

		return layerDrawable;
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		//Update background color
		refresh();
	}
}

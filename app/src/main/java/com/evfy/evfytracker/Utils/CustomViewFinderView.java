package com.evfy.evfytracker.Utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;

import me.dm7.barcodescanner.core.DisplayUtils;
import me.dm7.barcodescanner.core.ViewFinderView;

/**
 * Created by coolasia on 12/5/17.
 */

public class CustomViewFinderView extends ViewFinderView {
    public static final String TRADE_MARK_TEXT = "ZXing";
    public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
    public final Paint PAINT = new Paint();


    private static final float PORTRAIT_WIDTH_RATIO = 6f / 8;
    private static final float PORTRAIT_WIDTH_HEIGHT_RATIO = 0.75f;

    private static final float LANDSCAPE_HEIGHT_RATIO = 5f / 8;
    private static final float LANDSCAPE_WIDTH_HEIGHT_RATIO = 1.4f;
    private static final int MIN_DIMENSION_DIFF = 50;

    private static final float SQUARE_DIMENSION_RATIO = 5f / 4;


    private Rect mFramingRect;


    public CustomViewFinderView(Context context) {
        super(context);
        init();
    }

    public CustomViewFinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        PAINT.setColor(Color.WHITE);
        PAINT.setAntiAlias(true);
        float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                TRADE_MARK_TEXT_SIZE_SP, getResources().getDisplayMetrics());
        PAINT.setTextSize(textPixelSize);
        setSquareViewFinder(true);
    }

    public void setupViewFinder() {
        updateFramingRect();
        invalidate();
    }

    public Rect getFramingRect() {
        return mFramingRect;
    }

    @Override
    public void onDraw(Canvas canvas) {

        if (getFramingRect() == null) {
            return;
        }


        super.onDraw(canvas);
        drawTradeMark(canvas);
    }

    private void drawTradeMark(Canvas canvas) {
        Rect framingRect = getFramingRect();
        float tradeMarkTop;
        float tradeMarkLeft;
        if (framingRect != null) {
            tradeMarkTop = framingRect.bottom + PAINT.getTextSize() + 10;
            tradeMarkLeft = framingRect.left;
        } else {
            tradeMarkTop = 10;
            tradeMarkLeft = canvas.getHeight() - PAINT.getTextSize() - 10;
        }
        canvas.drawText("", tradeMarkLeft, tradeMarkTop, PAINT);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld) {
        updateFramingRect();
    }

    public synchronized void updateFramingRect() {
        Point viewResolution = new Point(getWidth(), getHeight());
        int width;
        int height;
        int orientation = DisplayUtils.getScreenOrientation(getContext());

        if (mSquareViewFinder) {
            if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                height = (int) (getHeight() * SQUARE_DIMENSION_RATIO);
                width = height;
                Log.i("CustomZX", "11");
            } else {
                width = (int) (getWidth() * SQUARE_DIMENSION_RATIO);
                height = width / 2;
                Log.i("CustomZX", "22");
            }
        } else {
            if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                height = (int) (getHeight() * LANDSCAPE_HEIGHT_RATIO);
                width = (int) (LANDSCAPE_WIDTH_HEIGHT_RATIO * height);
                Log.i("CustomZX", "33");
            } else {
                width = (int) (getWidth() * PORTRAIT_WIDTH_RATIO);
                height = (int) (PORTRAIT_WIDTH_HEIGHT_RATIO * width);
                Log.i("CustomZX", "44");
            }
        }

        if (width > getWidth()) {
            width = getWidth() - MIN_DIMENSION_DIFF;
            Log.i("CustomZX", "55");
        }

        if (height > getHeight()) {
            height = getHeight() - MIN_DIMENSION_DIFF;
            Log.i("CustomZX", "66");
        }

        int leftOffset = (viewResolution.x - width) / 2;
        int topOffset = (viewResolution.y - height) / 2;
        // mFramingRect = new Rect(leftOffset, topOffset, leftOffset + width, topOffset + height);
        mFramingRect = new Rect(leftOffset, topOffset, leftOffset + width, topOffset + 150);
        Log.i("CustomZXing", "yes");
        Log.i("CustomZXing", "yes" + leftOffset);
        Log.i("height", "is " + height);
    }
}


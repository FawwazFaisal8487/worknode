package com.evfy.evfytracker.Utils;

import com.evfy.evfytracker.classes.JobOrder;

public interface ItemClickListener {
    void onClick(JobOrder jobOrder);
}

package com.evfy.evfytracker.Utils;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

/**
 * Created by matt on 5/27/14.
 */
public class SecretTextView {

    private SpannableString mSpannableString;

    private double[] mAlphas;
    private boolean mIsVisible;
    private boolean mIsTextResetting = false;
    private int mDuration = 2500;

    private final TextView textView;
    private ValueAnimator animator;
    private OnShowingAnimationEndListener onShowingAnimationEndListener;

    private final ValueAnimator.AnimatorUpdateListener listener = new ValueAnimator.AnimatorUpdateListener() {

        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            Float percent = (Float) valueAnimator.getAnimatedValue();
            resetSpannableString(mIsVisible ? percent : 2.0f - percent);
        }

    };

    private final AnimatorListener animatorListener = new AnimatorListener() {

        @Override
        public void onAnimationStart(Animator animation) {
        }

        @Override
        public void onAnimationRepeat(Animator animation) {
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            if (onShowingAnimationEndListener != null)
                onShowingAnimationEndListener.onShowingAnimationEnd();
        }

        @Override
        public void onAnimationCancel(Animator animation) {
        }

    };

    public SecretTextView(TextView textView, int duration) {
        this.textView = textView;
        this.mDuration = duration;
        init();
        resetIfNeeded();
    }

    private void init() {
        this.mIsVisible = false;
        animator = ValueAnimator.ofFloat(0.0f, 2.0f);
        animator.addUpdateListener(listener);
        animator.setDuration(mDuration);
        animator.addListener(animatorListener);
    }

    public void toggle() {
        if (mIsVisible) {
            hide();
        } else {
            show();
        }
    }

    public void show() {
        mIsVisible = true;
        animator.start();
    }

    public void hide() {
        mIsVisible = false;
        animator.start();
    }

    public void setIsVisible(boolean isVisible) {
        mIsVisible = isVisible;
        resetSpannableString(isVisible == true ? 2.0f : 0.0f);
    }

    public void setOnShowingAnimationEndListener(OnShowingAnimationEndListener onShowingAnimationEndListener) {
        this.onShowingAnimationEndListener = onShowingAnimationEndListener;
    }

    public boolean getIsVisible() {
        return mIsVisible;
    }

    private void resetSpannableString(double percent) {
        mIsTextResetting = true;

        mSpannableString = new SpannableString(textView.getText().toString());
        int color = textView.getCurrentTextColor();
        for (int i = 0; i < mSpannableString.length(); i++) {
            mSpannableString.setSpan(
                    new ForegroundColorSpan(Color.argb(clamp(mAlphas[i] + percent), Color.red(color),
                            Color.green(color), Color.blue(color))), i, i + 1,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        textView.setText(mSpannableString);
        textView.invalidate();

        mIsTextResetting = false;
    }

    private void resetAlphas(int length) {
        mAlphas = new double[length];
        for (int i = 0; i < mAlphas.length; i++) {
            mAlphas[i] = Math.random() - 1;
        }
    }

    private void resetIfNeeded() {
        if (!mIsTextResetting) {
            resetAlphas(textView.getText().toString().length());
            resetSpannableString(mIsVisible ? 2.0f : 0);
        }
    }

    public void setText(String text) {
        textView.setText(text);
        resetIfNeeded();
    }

    public void setText(CharSequence text, TextView.BufferType type) {
        textView.setText(text, type);
        resetIfNeeded();
    }

    private int clamp(double f) {
        return (int) (255 * Math.min(Math.max(f, 0), 1));
    }

    public void setmDuration(int mDuration) {
        this.mDuration = mDuration;
        animator.setDuration(mDuration);
    }

    public interface OnShowingAnimationEndListener {

        void onShowingAnimationEnd();

    }

}

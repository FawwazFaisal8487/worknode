package com.evfy.evfytracker.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

public class CustomViewPager extends ViewPager {

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        return false;

    }

    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {

        // When using Maps V1
//        if(v instanceof MapView){
//            return true;
//        }
//        return super.canScroll(v, checkV, dx, x, y);


        // When using Maps V2
        if (v.getClass().getPackage() != null && v.getClass().getPackage().getName().startsWith("maps.")) {
            return true;
        }
        return super.canScroll(v, checkV, dx, x, y);
    }

}
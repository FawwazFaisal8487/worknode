package com.evfy.evfytracker.Utils

import java.io.File

interface OnImageCompressedCallback {
    fun onCompressionComplete(tempFile: File)
}
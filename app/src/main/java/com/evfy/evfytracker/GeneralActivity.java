package com.evfy.evfytracker;

import static android.content.Context.ACTIVITY_SERVICE;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Activity.Tracking.SelfHostedGPSTrackerService;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

/**
 * Created by coolasia on 19/12/17.
 */

public class GeneralActivity {

    // check either service tracking location is running
    public boolean isMyServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SelfHostedGPSTrackerService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

//    public boolean isMyServiceLocationRunning(Context context) {
//        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
//        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
//            if (LocationService.class.getName().equals(service.service.getClassName())) {
//                return true;
//            }
//        }
//        return false;
//    }

    public boolean isMyServiceLocationRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (ForegroundLocationService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // check internet connection
    public boolean haveInternetConnected(Context context) {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI && activeNetwork.isAvailable() && activeNetwork.isConnected()) {
                result = true;
                // msg="You are connected to a WiFi Network";
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE && activeNetwork.isAvailable() && activeNetwork.isConnected()) {
                result = true;
                //msg="You are connected to a Mobile Network";
            }
        } else {
            //msg = "No internet Connectivity";
            result = false;
        }

        return result;
    }

    // start alarm location
    public void startAlarmManager(Context context) {
        context.startService(new Intent(context, SelfHostedGPSTrackerService.class));
        LogCustom.i("servicesGeneral", "is: " + isMyServiceRunning(context));
    }

//    /* =========  DIALOG POPUP =========== */
//    SweetAlertDialog showErrorDialog(String titleError, String contextError, Context getContext) {
//        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError, getContext);
//        sweetAlertDialog.show();
//        return sweetAlertDialog;
//    }
//
//    SweetAlertDialog getErrorDialog(String titleError, String contextError, Context getContext) {
//        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext, SweetAlertDialog.ERROR_TYPE)
//                .setContentText(contextError)
//                .setTitleText(titleError)
//                .setConfirmText("OK")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        sDialog.dismissWithAnimation();
//                    }
//                });
//
//
//        sweetAlertDialog.setCancelable(false);
//        return sweetAlertDialog;
//    }
//
//    SweetAlertDialog showErrorInternetDialog(Context getContext) {
//        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog(getContext);
//        sweetAlertDialog.show();
//        return sweetAlertDialog;
//    }
//
//    SweetAlertDialog getErrorInternetDialog(final Context getContext) {
//        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext, SweetAlertDialog.ERROR_TYPE)
//                .setContentText("Your device not connect with internet. Please check your internet connection")
//                .setTitleText("No internet connection")
//                .setConfirmText("Open settings")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
//                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
//                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
//                        intent.setComponent(cn);
//                        intent.setFlags(intent.FLAG_ACTIVITY_NEW_TASK);
//                        getContext.startActivity(intent);
//                    }
//                })
//                .setCancelText("Cancel")
//                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        sDialog.dismissWithAnimation();
//                    }
//                });
//
//
//        sweetAlertDialog.setCancelable(false);
//        return sweetAlertDialog;
//    }
//
//    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
//    public void errorHandlingFromResponseCode(final String body, final  boolean requestCallBackError, final int responseCode, final Resources getResources, final Context getContext){
//            if(!requestCallBackError){
//                try {
//                    String errorMessage = "";
//                    if(body != null || !body.equalsIgnoreCase("")){
//                        final JSONObject data = new JSONObject( body );
//
//                        if(responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST){
//                            if(data.has("error") && !(data.isNull("error"))){
//                                try {
//                                    errorMessage = data.getString("error");
//                                }catch (Exception e){
//                                    JSONObject errorMessageArray = data.getJSONObject("error");
//                                    errorMessage = errorMessageArray.getString("error");
//                                }
//                            }
//
//                            if(errorMessage.equalsIgnoreCase("")){
//                                errorMessage = getResources.getString(R.string.pleaseContactAdmin);
//                            }
//
//                            showErrorDialog(getResources.getString(R.string.badRequestErrorTitle),errorMessage,getContext).show();
//
//
//                        } else if(responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED){
//                            if(data.has("device_not_found") && !(data.isNull("device_not_found"))){
//                                if(data.getBoolean("device_not_found")){
//                                    showErrorDialog(getResources.getString(R.string.deviceNotFoundTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                                }
//                            }
//
//                            else if(data.has("device_is_banned") && !(data.isNull("device_is_banned"))){
//                                if(data.getBoolean("device_is_banned")){
//                                    showErrorDialog(getResources.getString(R.string.deviceBannedTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                                }
//                            }else{
//                                showErrorDialog(getResources.getString(R.string.errorLogin),getResources.getString(R.string.pleaseCheckPassword),getContext).show();
//                            }
//                        }else if(responseCode == Constants.STATUS_RESPONSE_FORBIDDEN){
//                            if(data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))){
//                                if(data.getBoolean("unpaid_subscription")){
//                                    showErrorDialog(getResources.getString(R.string.unpaidSubscriptionTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                                }
//                            }
//
//                            else if(data.has("quota_reached") && !(data.isNull("quota_reached"))){
//                                if(data.getBoolean("quota_reached")){
//                                    showErrorDialog(getResources.getString(R.string.quotaReachedTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                                }
//                            }else if(data.has("blacklist") && !(data.isNull("blacklist"))){
//                                if(data.getBoolean("blacklist")){
//                                    showErrorDialog(getResources.getString(R.string.blacklistTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                                }
//                            }
//                            else{
//                                showErrorDialog(getResources.getString(R.string.errorLogin),getResources.getString(R.string.pleaseCheckPassword),getContext).show();
//                            }
//                        }else if(responseCode == Constants.STATUS_RESPONSE_NOT_FOUND){
//                            showErrorDialog(getResources.getString(R.string.notFoundErrorTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//
//                        } else if(responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR){
//                            showErrorDialog(getResources.getString(R.string.internalServerErrorTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//
//                        }else if(responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN){
//                            showErrorDialog(getResources.getString(R.string.serverDownErrorTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                        }
//
//                    }else{
//                        showErrorDialog(getResources.getString(R.string.errorLogin),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }else{
//                if(responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN){
//                    showErrorDialog(getResources.getString(R.string.serverDownErrorTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                }else if (responseCode == Constants.CANNOT_RESOLVE_HOST){
//                    showErrorInternetDialog(getContext).show();
//                }else{
//                    showErrorDialog(getResources.getString(R.string.serverErrorTitle),getResources.getString(R.string.pleaseContactAdmin),getContext).show();
//                }
//                // this for request call back error. Maybe because cannot connect server.
//            }
//        }
//	/* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
}

package com.evfy.evfytracker.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.disegnator.robotocalendar.RobotoCalendarView;
import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.JobAdapter;
import com.evfy.evfytracker.classes.JobOrder;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class SearchFragment extends Fragment implements OnRESTResultCallback, OnHttpEventListener {

	private Activity mActivity;

	List<JobOrder> searchResultJobList = new ArrayList<JobOrder>();

	int sortCol = 4;
	String query = "";
	int sortOrder = 1;
	int mult = 0;
	SweetAlertDialog pd;
	private Handler jobHandler;
	private Runnable jobRunnable;
	boolean endOfList;
	SharedPreferences settings;
	String jobStateId;
	int selectedTab = 0;
	boolean getJobFromSearchBoolean = false;
	int selecedCalendarMonth = 0;

	private ImageButton calendarButton;
	private Calendar cal;
	private int day;
	private int month;
	private int year;
	private EditText et;

	boolean mIsFactoryWorker;
	boolean receivedFromPushBoolean = false;
	int pushJobId = 0;
	// DatabaseHandlerJobs dbUpdateJobs;
	RobotoCalendarView calendarLayout;
	RelativeLayout bottomLayout;
	private int currentMonthIndex;
	private Calendar currentCalendar;
	boolean getAllMonth = false;
	boolean viewCreatedBool = false;
	final String PREFS_NAME = "MyPrefsFile";
	private RestManager restManager;
	HttpConnection connection, connection2;
	RecyclerView searchResultListView;
	JobAdapter listAdapter;
	boolean firstCalendarTap;
	boolean calendarOpen = false;
	float resizeHeight, viewHeight;
	Menu actionBarMenu;
	EditText editsearch;
	boolean needReload, firstLoad;
	AlertDialog alert;
	private final Handler textHandler = new Handler();
	boolean searchBool = false;
	Date selectedDate;

	private final Runnable textRunnable = new Runnable() {

		public void run() {

			//Timber.d( "0000 searching text runnable");
			searchResultJobList.clear();
			mActivity.setProgressBarIndeterminateVisibility(Boolean.TRUE);

			mult = 0;
			endOfList = false;
			if (settings.contains("sort_column")) {
				sortCol = settings.getInt("sort_column", 0);
			} else {
				sortCol = 0;
			}

			if (settings.contains("sort_order")) {
				sortOrder = settings.getInt("sort_order", 0);
			} else {
				sortOrder = 1;
			}

			startSearching(query);

		}

	};

	private final TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {

			// TODO Auto-generated method stub
			String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());
			// Timber.d( "text watcher aafter text change:" + text);

			if (text.length() > 1) {
				query = text;
				textHandler.removeCallbacks(textRunnable);
				textHandler.postDelayed(textRunnable, 1000);
			} else if (text.length() == 0) {
				searchResultJobList.clear();
				reloadJobList(searchResultJobList, needReload, endOfList, true);
			}

		}

	};

	public void receivedFromPush(int jobId) {
		//receivedFromPushBoolean = true;
		//Timber.d( "mactivity null:" + mActivity + "get activity:" + getActivity());

		if (jobId == -1) {

		} else {
			pushJobId = jobId;

		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				Bundle b = data.getExtras();
				if (b != null) {

				}
			} else if (resultCode == 0) {
				System.out.println("RESULT CANCELLED");
			}
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = activity;
		// Timber.d( "on attach mactivity null:" + mActivity + "get activity:" + getActivity());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		restManager = RestManagerFactory.createRestManagerWithHttpEventListener(mActivity, this);
		firstCalendarTap = false;
		pd = new SweetAlertDialog(mActivity, SweetAlertDialog.PROGRESS_TYPE);
		pd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		pd.setTitleText("Fetching Jobs");
		pd.setCancelable(false);
		jobStateId = "1,3";
		//settings = mActivity.getSharedPreferences( PREFS_NAME, 0 );
		settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mIsFactoryWorker = settings.getBoolean("is_factory", false);
		((DrawerActivity) mActivity).getSupportActionBar().setTitle("Global Search");
		setHasOptionsMenu(false);
		View rootView = inflater.inflate(R.layout.search_layout2, container, false);
		searchResultListView = rootView.findViewById(R.id.searchResultList);
		searchResultListView.setHasFixedSize(true);
		rootView.findViewById(R.id.mImageViewSearch).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(final View v) {
				search();
			}

		});

		editsearch = rootView.findViewById(R.id.searchEditText);
		// Capture Text in EditText
		//		editsearch.addTextChangedListener( textWatcher );
		editsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					search();
					return true;
				}

				return false;
			}
		});

		mult = 0;
		needReload = true;

		return rootView;

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	public void startSearching(String searchString) {

		//		LogCustom.logTimeStart();

		try {
			searchString = URLEncoder.encode(searchString, "UTF-8");
			JSONObject jsonObject = new JSONObject();
			SharedPreferences settings;
			//	settings = mActivity.getSharedPreferences( PREFS_NAME, 0 );
			settings = PreferenceManager.getDefaultSharedPreferences(mActivity);

			String accessToken = settings.getString("access_token", "");
			Log.i("", "accessToken:" + accessToken);

			connection2 = new HttpConnection(Constants.SAAS_SERVER_LDS + "/api/orders?search=" + searchString, RESTIntentService.GET, jsonObject,
					this);
			List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();
			Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
			Log.i("", "accessToken 22:" + accessToken);
			header.add(pair);
			connection2.setHeaders(header);
			restManager.sendRequest(connection2);

		} catch (Exception expected) {
		}
	}

	@Override
	public void onRequestInit() {

	}

	@Override
	public void onRequestFinish() {

	}

	@Override
	public void onRESTResult(int returnCode, int code, String result) {

		//		Log.i( "", "on rest result" + result );
		searchResultJobList.clear();

		try {

			final JSONArray data = new JSONArray(result);

			if (data.length() == 0) {
				Toast.makeText(mActivity, "Empty Result.", Toast.LENGTH_SHORT).show();
			} else {

				final List<JobOrder> temps = new ArrayList<>();
				LogCustom.i(data.length(), "data.length() ");

				for (int i = 0; i < data.length(); i++) {
					final JSONObject jsonObject = (JSONObject) data.get(i);
					final JobOrder jobOrder = new JobOrder(jsonObject);
					if (String.valueOf(jobOrder.getOrderId()).contains(query)) temps.add(jobOrder);
					else searchResultJobList.add(jobOrder);
				}

				searchResultJobList.addAll(0, temps);

			}

		} catch (JSONException e) {
			LogCustom.e(e);
		}

		//		LogCustom.logTimeEnd();
		reloadJobList(searchResultJobList, needReload, endOfList, true);

	}

	public void updateAdapter(JobAdapter adapter) {
		searchResultListView.setAdapter(adapter);

	}

	public void reloadJobList(List<JobOrder> jobList, boolean firstLoad, boolean endOfList, boolean showDate) {
		listAdapter = new JobAdapter(mActivity, this.searchResultJobList, new Date(), true);
		listAdapter.setIsViewOnly(mIsFactoryWorker);
		updateAdapter(listAdapter);
	}

	void search() {

		query = editsearch.getText().toString();

		if (query.length() < 3) {
			Toast.makeText(mActivity, "The keyword length must be at least 3.", Toast.LENGTH_SHORT).show();
			return;
		}

		searchResultJobList.clear();

		mult = 0;
		endOfList = false;
		if (settings.contains("sort_column")) {
			sortCol = settings.getInt("sort_column", 0);
		} else {
			sortCol = 0;
		}

		if (settings.contains("sort_order")) {
			sortOrder = settings.getInt("sort_order", 0);
		} else {
			sortOrder = 1;
		}
		needReload = true;

		startSearching(query);

		if (mActivity != null) {
			final InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(editsearch.getWindowToken(), 0);
			//			inputMethodManager.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );
		}

	}

}

package com.evfy.evfytracker.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.R;

public class AboutFragment extends Fragment {


    TextView versionNumber, deviceID;
    SharedPreferences settings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {

        ((DrawerActivity) getActivity()).getSupportActionBar().setTitle("About");

        View v = inflater.inflate(R.layout.fragment_about, container,
                false);


        Context context = getActivity().getApplicationContext(); // or activity.getApplicationContext()
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editors = settings.edit();
        editors.putBoolean("insideFragmentPage", false).commit();
        editors.putBoolean("insideJobDetails", false).commit();
        editors.putBoolean("insideNotificationListing", false).commit();
        editors.apply();

        String myVersionName = "Not Available"; // initialize String
        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        versionNumber = v.findViewById(R.id.textView12);
        deviceID = v.findViewById(R.id.deviceID);
        versionNumber.setText(myVersionName);

        String android_id = Settings.Secure.getString(getActivity().getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        deviceID.setText(android_id);

        deviceID.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
//                ClipboardManager clipboard = (ClipboardManager) getActivity().getApplicationContext().getSystemService(CLIPBOARD_SERVICE);
//                clipboard.setText(deviceID.getText());

                ClipboardManager cManager = (ClipboardManager) getActivity().getApplicationContext().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData cData = ClipData.newPlainText("", deviceID.getText());
                cManager.setPrimaryClip(cData);

                Toast.makeText(getActivity(), "Copied this Device ID", Toast.LENGTH_SHORT).show();
                return false;
            }
        });


        return v;
    }

}

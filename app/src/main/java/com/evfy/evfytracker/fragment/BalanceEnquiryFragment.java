package com.evfy.evfytracker.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.Activity.WMSActivity.BalanceEnquiryActivity;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.CustomerDetails;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;
import com.evfy.evfytracker.classes.DeliveryOrderOutDetails;
import com.evfy.evfytracker.classes.WebServiceWMS;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.exceptions.NoInternetConnectionException;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BalanceEnquiryFragment extends Fragment implements OnRESTResultCallback, OnHttpEventListener {

    private Activity mActivity;
    private RestManager restManager;
    SharedPreferences settings;

    static final String PREFS_NAME = "MyPrefsFile";
    String accessToken;
    HttpConnection connection;

    Runnable runnableGetCompanyName;
    Pair<String, String> pairHeader;
    ArrayList<String> companyNameArray;
    ArrayList<Integer> companyIdArray;
    ArrayAdapter<String> adpCompanyName;
    ListView lvCompanyName;
    AlertDialog alertCompanyName;
    String selectCompanyName;
    LinearLayout layout_companyName;
    TextView txt_companyName;
    CustomerDetails customerDetails;
    Button btnSearchOrder;
    DeliveryOrderDetails deliveryOrderDetails;
    DeliveryOrderOutDetails deliveryOrderOutDetails;

    String textEmptyData = "";
    String textErrorMessage = "";
    SweetAlertDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {

        ((DrawerActivity) getActivity()).getSupportActionBar().setTitle("Search Balance Enquiry");

        View v = inflater.inflate(R.layout.fragment_balance_enquiry, container,
                false);


        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(mActivity, this);
        // settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
        settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
        accessToken = settings.getString("access_token", "");

        btnSearchOrder = v.findViewById(R.id.btnSearchOrder);
        layout_companyName = v.findViewById(R.id.layout_companyName);
        txt_companyName = v.findViewById(R.id.txt_companyName);


        runnableGetCompanyName = new Runnable() {

            @Override
            public void run() {
                final OnRESTResultCallback onRESTResultCallback = new OnRESTResultCallback() {

                    @Override
                    public void onRESTResult(final int returnCode, final int code, final String result) {
                        LogCustom.i(result, "resultCompanyName");
                        LogCustom.i(result, "result");

                        progressDialog.dismiss();

                        if (code != 200) {
                            try {
                                final JSONObject data = new JSONObject(result);
                                textErrorMessage = data.getString("error");
                                showErrorDialog().show();
                            } catch (Exception e) {
                                LogCustom.e(e);
                            }
                        } else {

                            if (result.equalsIgnoreCase("")) {
                                textEmptyData = "No Result for Customer. Please try again";
                                showEmptyDataDialog().show();
                            } else {


                                companyNameArray = new ArrayList<>();
                                companyIdArray = new ArrayList<>();


                                try {

                                    final JSONObject data = new JSONObject(result);
                                    final JSONArray companyData = data.getJSONArray("result");
                                    for (int i = 0; i < companyData.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) companyData.get(i);
                                        customerDetails = new CustomerDetails(jsonObject2);
                                        companyNameArray.add(customerDetails.getCompany_name());
                                        companyIdArray.add(customerDetails.getCustomer_id());
                                        Log.i("companyName", "" + customerDetails.getCompany_name());
                                        Log.i("companyId", "" + customerDetails.getCustomer_id());
                                    }

                                    Log.i("NameOfScanSetting", customerDetails.getNameScanSetting());

                                    selectCompanyName = "";

                                    adpCompanyName = new ArrayAdapter<String>(mActivity,
                                            android.R.layout.simple_list_item_1, companyNameArray) {
                                        @Override
                                        public View getView(int position, View convertView, ViewGroup parent) {
                                            View row = super.getView(position, convertView, parent);

                                            TextView txt1 = row.findViewById(android.R.id.text1);

                                            if (selectCompanyName.equalsIgnoreCase("")) {
                                                if (position == 0) {
                                                    txt1.setTextColor(Color.RED); // some color
                                                }
                                            } else {
                                                txt1.setTextColor(Color.GREEN); // some color
                                            }

                                            return row;
                                        }
                                    };

                                    lvCompanyName = new ListView(mActivity);
                                    lvCompanyName.setAdapter(adpCompanyName);

                                    final AlertDialog.Builder builderRecipient = new AlertDialog.Builder(mActivity);

                                    lvCompanyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                            selectCompanyName = companyNameArray.get(i);
                                            customerDetails.setCustomer_id(companyIdArray.get(i));
                                            txt_companyName.setText(selectCompanyName);
                                            Log.i("companyNameSelect", selectCompanyName);
                                            Log.i("companyIdSelect", "" + customerDetails.getCustomer_id());
                                            checkedAllInserted();
                                            alertCompanyName.dismiss();
                                        }
                                    });

                                    builderRecipient.setView(lvCompanyName);
                                    builderRecipient.setTitle("Name of Company");

                                    builderRecipient.setNegativeButton("Cancel", null);

                                    alertCompanyName = builderRecipient.create();
                                    alertCompanyName.show();

                                } catch (Exception e) {

                                }
                            }
                        }
                    }

                };
                callWebService(Constants.WEB_SERVICE_GET_COMPANY_NAME, onRESTResultCallback);
            }
        };


        btnSearchOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//                Intent i = new Intent(getActivity(), DeliveryOrderDetailActivity.class);
//                Log.i("companyId", "" + companyId);
//                i.putExtra("companyId", companyId);
//                i.putExtra("referenceNumber", selectReferenceNumber);
//                startActivity(i);

                if (customerDetails.getCustomer_id() == 0) {
                    Toast.makeText(getActivity(), "Please select customer first", Toast.LENGTH_SHORT).show();
                } else {
                    getListBalanceEnquiry();
                }
            }

        });

        layout_companyName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                progressDialog = showProgressDialog();
                AndroidOp.runOnMainThread(mActivity, runnableGetCompanyName);
            }
        });


        return v;
    }

    void checkedAllInserted() {
        String companyCheck = txt_companyName.getText().toString().trim();

        if (companyCheck.equals(getActivity().getResources().getString(R.string.txtStringCompanyName))) {

            btnSearchOrder.setBackgroundColor(getActivity().getResources().getColor(R.color.greyLight));
        } else {
            btnSearchOrder.setBackgroundColor(getActivity().getResources().getColor(R.color.green));
        }
    }

    void callWebService(final WebServiceWMS webService, final OnRESTResultCallback onRESTResultCallback) {

        LogCustom.i(webService, "webService");
        pairHeader = Pair.create("Authorization", "Bearer " + accessToken);
        final List<Pair<String, String>> header = new ArrayList<>();
        header.add(pairHeader);
        Log.i("", "access token:" + accessToken);

        final String body = "";

        Log.i("", "header:" + header);

        AndroidOp.runInBackground(mActivity, new Runnable() {

            @Override
            public void run() {
                AndroidOp.runOnMainThread(mActivity, new Runnable() {

                    @Override
                    public void run() {
                        try {
                            restManager.sendRequest(webService.getHttpConnection(header, body, onRESTResultCallback, ""));
                        } catch (final Throwable e) {
                            LogCustom.e(e);
                            Log.i("", "msgNetworkError 22");
                        }
                    }

                });
            }
        });
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        // Timber.d( "on attach mactivity null:" + mActivity + "get activity:" + getActivity())
    }

    @Override
    public void onRESTResult(int returnCode, int code, String result) {

        LogCustom.i(result, "resultBalanceEnquiry");
        LogCustom.i(code, "code");

        String jsonArray = settings.getString("resultData", "");
        Log.i("jsonArray", "Have" + jsonArray);

        progressDialog.dismiss();

        if (code != 200) {
            try {
                final JSONObject data = new JSONObject(result);
                textErrorMessage = data.getString("error");
                showErrorDialog().show();
            } catch (Exception e) {
                LogCustom.e(e);
            }
        } else {
            if (returnCode == 1) {

                try {

                    final JSONObject data = new JSONObject(result);

                    if (data.getJSONArray("result").length() > 0) {
                        if (data.has("status") && !(data.isNull("status"))) {
                            if (data.getBoolean("status")) {

                                //                                DeliveryOrderDetails deliveryOrderDetails = new DeliveryOrderDetails(data);
                                //                                DeliveryOrderOutDetails deliveryOrderOutDetails = new DeliveryOrderOutDetails();
                                //                                deliveryOrderDetails.getDeliveryOrderOutDetails().add(deliveryOrderOutDetails);

                                Intent ii = new Intent(mActivity, BalanceEnquiryActivity.class);
                                settings.edit().putString("resultData", result).commit();
                                //ii.putExtra("resultData", result.toString());
                                // ii.putExtra("deliveryOrderDetails",deliveryOrderDetails);
                                startActivity(ii);
                                LogCustom.i("in11", "in11");

                            }
                        } else {
                            textEmptyData = "No Result. Please try again";
                            showEmptyDataDialog().show();
                        }
                    } else {
                        textEmptyData = "No Result. Please try again";
                        showEmptyDataDialog().show();
                    }

                } catch (Exception e) {
                    LogCustom.e(e);
//                    Crashlytics.log(returnCode, "getListBalanceEnquiryResult", result);
//                    Crashlytics.logException(e);
                }
            }
        }
    }


    public void getListBalanceEnquiry() {

        progressDialog = showProgressDialog();

        Log.i("companyIdget", "" + customerDetails.getCustomer_id());

        try {

            JSONObject jsonObject = new JSONObject();
            connection = new HttpConnection(Constants.SAAS_SERVER_WMS + "/worker/api/order_details?customer_id=" + customerDetails.getCustomer_id() + "&enquiry=true", RESTIntentService.GET, jsonObject, this);

            List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();

            Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
            header.add(pair);
            connection.setHeaders(header);

            Log.i("", "access token:" + accessToken);
            Log.i("connectBalanceEnquiry", " " + connection.toString());

            restManager.sendRequestWithReturn(1, connection);
        } catch (Exception e) {

            if (e instanceof NoInternetConnectionException) {

            } else {

                try {
//                    Crashlytics.logException(e);
                } catch (final Throwable t) {
                }

            }

        }

    }

    @Override
    public void onRequestInit() {

//        try {
//            pd.show();
//        } catch ( final Throwable e ) {
//            LogCustom.e( e );
//        }

    }

    @Override
    public void onRequestFinish() {

//        try {
//            if ( ( pd != null ) && pd.isShowing() ) {
//                pd.dismiss();
//            }
//        } catch ( final IllegalArgumentException e ) {
//            // Handle or log or ignore
//        } catch ( final Exception e ) {
//            // Handle or log or ignore
//        }
    }

    SweetAlertDialog showProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = getProgressDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE).setContentText(
                getString(R.string.msgProcessingProcessing)).setTitleText("");
        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setContentText(textErrorMessage)
                .setTitleText("ERROR")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showEmptyDataDialog() {
        final SweetAlertDialog sweetAlertDialog = getEmptyDataDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getEmptyDataDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setContentText(textEmptyData)
                // .setCustomImage(R.drawable.custom_img)
                .setTitleText("EMPTY")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

}

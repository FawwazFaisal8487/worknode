package com.evfy.evfytracker.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.Activity.WMSActivity.DeliveryOrderDetailActivity;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.CustomerDetails;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;
import com.evfy.evfytracker.classes.WebServiceWMS;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.exceptions.NoInternetConnectionException;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ScanDoFragment extends Fragment implements OnRESTResultCallback, OnHttpEventListener {

    private Activity mActivity;
    Button btnSearchOrder;
    Runnable runnableGetCompanyName;
    private RestManager restManager;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    Pair<String, String> pairHeader;
    ArrayList<String> companyNameArray, referenceNumberArray, outDateArray, nameScanSettingArray;
    ArrayList<Integer> companyIdArray;
    ArrayAdapter<String> adpCompanyName, adpReferenceNumber;
    ListView lvCompanyName, lvReferenceNumber;
    AlertDialog alertCompanyName, alertReferenceNumber;
    String selectCompanyName, selectReferenceNumber, selectOutDate;
    String accessToken;
    HttpConnection connection;
    LinearLayout layout_deliveryOrder, layout_companyName;
    TextView txt_companyName, txt_deliveryOrder, txt_dateOutDO;
    int companyId;
    DeliveryOrderDetails deliveryOrderDetails;
    CustomerDetails customerDetails;
    SweetAlertDialog progressDialog;
    SweetAlertDialog errorDialog;
    SweetAlertDialog emptyDialog;
    String textEmptyData = "";
    String textErrorMessage = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {


        ((DrawerActivity) getActivity()).getSupportActionBar().setTitle("Search Delivery Order Out");

        View v = inflater.inflate(R.layout.fragment_scando, container,
                false);


        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(mActivity, this);
        settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
        accessToken = settings.getString("access_token", "");


        btnSearchOrder = v.findViewById(R.id.btnSearchOrder);
        layout_companyName = v.findViewById(R.id.layout_companyName);
        layout_deliveryOrder = v.findViewById(R.id.layout_deliveryOrder);
        txt_companyName = v.findViewById(R.id.txt_companyName);
        txt_deliveryOrder = v.findViewById(R.id.txt_deliveryOrder);
        txt_dateOutDO = v.findViewById(R.id.txt_dateOutDO);


        enabledDeliveryOrder(false);

        btnSearchOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
//                Intent i = new Intent(getActivity(), DeliveryOrderDetailActivity.class);
//                Log.i("companyId", "" + companyId);
//                i.putExtra("companyId", companyId);
//                i.putExtra("referenceNumber", selectReferenceNumber);
//                startActivity(i);
                getDetailListDO();
            }

        });

        layout_companyName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                progressDialog = showProgressDialog();
                AndroidOp.runOnMainThread(mActivity, runnableGetCompanyName);
            }
        });

        layout_deliveryOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.i("companyIDDO", "" + companyId);
                getReferenceNumber(companyId);
            }
        });


        runnableGetCompanyName = new Runnable() {


            @Override
            public void run() {
                final OnRESTResultCallback onRESTResultCallback = new OnRESTResultCallback() {


                    @Override
                    public void onRESTResult(final int returnCode, final int code, final String result) {
                        LogCustom.i(result, "resultCompanyName");

                        progressDialog.dismiss();

                        if (code != 200) {
                            try {
                                final JSONObject data = new JSONObject(result);
                                textErrorMessage = data.getString("error");
                                showErrorDialog().show();
                            } catch (Exception e) {
                                LogCustom.e(e);
                            }
                        } else {
                            if (result.equalsIgnoreCase("")) {
                                textEmptyData = "No Result for Customer. Please try again";
                                showEmptyDataDialog().show();
                            } else {

                                companyNameArray = new ArrayList<>();
                                companyIdArray = new ArrayList<>();
                                nameScanSettingArray = new ArrayList<>();

                                try {

                                    final JSONObject data = new JSONObject(result);
                                    final JSONArray companyData = data.getJSONArray("result");
                                    // ArrayList <String> listOfApp = new ArrayList<String>();
                                    for (int i = 0; i < companyData.length(); i++) {
                                        JSONObject jsonObject2 = (JSONObject) companyData.get(i);
                                        customerDetails = new CustomerDetails(jsonObject2);
                                        String companyName = jsonObject2.getString("company_name");
                                        int companyId = jsonObject2.getInt("id");
                                        companyNameArray.add(companyName);
                                        companyIdArray.add(companyId);
                                        nameScanSettingArray.add(customerDetails.getNameScanSetting());
                                        Log.i("companyName", "" + companyName);
                                        Log.i("companyId", "" + companyId);
                                    }

                                    Log.i("NameOfScanSetting", customerDetails.getNameScanSetting());

                                    selectCompanyName = "";

                                    adpCompanyName = new ArrayAdapter<String>(mActivity,
                                            android.R.layout.simple_list_item_1, companyNameArray) {
                                        @Override
                                        public View getView(int position, View convertView, ViewGroup parent) {
                                            View row = super.getView(position, convertView, parent);

                                            TextView txt1 = row.findViewById(android.R.id.text1);

                                            if (selectCompanyName.equalsIgnoreCase("")) {
                                                if (position == 0) {
                                                    txt1.setTextColor(Color.RED); // some color
                                                }
                                            } else {
                                                txt1.setTextColor(Color.GREEN); // some color
                                            }

                                            return row;
                                        }
                                    };

                                    lvCompanyName = new ListView(mActivity);
                                    lvCompanyName.setAdapter(adpCompanyName);

                                    final AlertDialog.Builder builderRecipient = new AlertDialog.Builder(mActivity);

                                    lvCompanyName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            //                                    if (i == 0) {
                                            //                                        String companyName = companyNameArray.get(i).substring(11);
                                            //                                        Log.i("companyNameSelect",companyName);
                                            //                                        alertCompanyName.dismiss();
                                            //                                    } else {
                                            //                                        alertCompanyName.dismiss();
                                            //                                    }
                                            txt_deliveryOrder.setText(R.string.txtStringDONumber);
                                            txt_dateOutDO.setText(R.string.txtStringDateOut);
                                            selectCompanyName = companyNameArray.get(i);
                                            companyId = companyIdArray.get(i);
                                            txt_companyName.setText(selectCompanyName);
                                            Log.i("companyNameSelect", selectCompanyName);
                                            Log.i("companyIdSelect", "" + companyId);
                                            customerDetails.setCustomer_id(companyId);
                                            customerDetails.setNameScanSetting(nameScanSettingArray.get(i));
                                            settings.edit().putString("name_scan_setting", customerDetails.getNameScanSetting()).commit();
                                            enabledDeliveryOrder(true);
                                            checkedAllInserted();
                                            alertCompanyName.dismiss();


                                        }
                                    });

                                    builderRecipient.setView(lvCompanyName);
                                    builderRecipient.setTitle("Name of Company");

                                    builderRecipient.setNegativeButton("Cancel", null);

                                    alertCompanyName = builderRecipient.create();
                                    alertCompanyName.show();

                                } catch (Exception e) {
                                    LogCustom.e(e);
                                    Log.i("crash", "crash");
//                                    Crashlytics.log(returnCode, "getCompanyNameResult", result);
//                                    Crashlytics.logException(e);
                                }

                            }
                        }
                    }

                };
                callWebService(Constants.WEB_SERVICE_GET_COMPANY_NAME, onRESTResultCallback);
            }
        };


        return v;
    }

    public void enabledDeliveryOrder(boolean status) {
        layout_deliveryOrder.setEnabled(status);

    }

    void checkedAllInserted() {
        String companyCheck = txt_companyName.getText().toString().trim();
        String referenceCheck = txt_deliveryOrder.getText().toString().trim();

        if (companyCheck.equals(getActivity().getResources().getString(R.string.txtStringCompanyName))
                || referenceCheck.equals(getActivity().getResources().getString(R.string.txtStringDONumber))) {

            btnSearchOrder.setBackgroundColor(getActivity().getResources().getColor(R.color.greyLight));
        } else {
            btnSearchOrder.setBackgroundColor(getActivity().getResources().getColor(R.color.green));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        // Timber.d( "on attach mactivity null:" + mActivity + "get activity:" + getActivity())
    }

    @Override
    public void onRESTResult(int returnCode, int code, String result) {

        progressDialog.dismiss();

        LogCustom.i(result, "resultgetReference");
        LogCustom.i(code, "code");

        if (code != 200) {
            try {
                final JSONObject data = new JSONObject(result);
                textErrorMessage = data.getString("error");
                showErrorDialog().show();
            } catch (Exception e) {
                LogCustom.e(e);
            }
        } else {
            if (returnCode == 1) {

                try {
                    final JSONObject data = new JSONObject(result);

                    if (data.has("status") && !(data.isNull("status"))) {
                        if (data.getBoolean("status")) {

                            referenceNumberArray = new ArrayList<>();
                            outDateArray = new ArrayList<>();

                            try {

                                final JSONArray companyData = data.getJSONArray("order");
                                // ArrayList <String> listOfApp = new ArrayList<String>();
                                for (int i = 0; i < companyData.length(); i++) {
                                    JSONObject jsonObject2 = (JSONObject) companyData.get(i);
                                    String referenceNumber = jsonObject2.getString("reference_no");
                                    String outDate = jsonObject2.getString("handling_out_date");
                                    referenceNumberArray.add(referenceNumber);
                                    outDateArray.add(outDate);
                                    Log.i("referenceNumber", "" + referenceNumber);
                                    Log.i("handlingOutDate", "" + outDate);
                                }


                                selectReferenceNumber = "";

                                adpReferenceNumber = new ArrayAdapter<String>(mActivity,
                                        android.R.layout.simple_list_item_1, referenceNumberArray) {
                                    @Override
                                    public View getView(int position, View convertView, ViewGroup parent) {
                                        View row = super.getView(position, convertView, parent);

                                        TextView txt1 = row.findViewById(android.R.id.text1);

                                        if (selectReferenceNumber.equalsIgnoreCase("")) {
                                            if (position == 0) {
                                                txt1.setTextColor(Color.RED); // some color
                                            }
                                        } else {
                                            txt1.setTextColor(Color.GREEN); // some color
                                        }

                                        return row;
                                    }
                                };

                                lvReferenceNumber = new ListView(mActivity);
                                lvReferenceNumber.setAdapter(adpReferenceNumber);

                                final AlertDialog.Builder builderRecipient = new AlertDialog.Builder(mActivity);

                                lvReferenceNumber.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        //                                    if (i == 0) {
                                        //                                        String companyName = companyNameArray.get(i).substring(11);
                                        //                                        Log.i("companyNameSelect",companyName);
                                        //                                        alertCompanyName.dismiss();
                                        //                                    } else {
                                        //                                        alertCompanyName.dismiss();
                                        //                                    }
                                        selectReferenceNumber = referenceNumberArray.get(i);
                                        selectOutDate = outDateArray.get(i);
                                        txt_deliveryOrder.setText(selectReferenceNumber);
                                        Log.i("referenceNumberSelect", selectReferenceNumber);
                                        Log.i("outDateSelect", selectOutDate);
                                        txt_dateOutDO.setText(selectOutDate);
                                        checkedAllInserted();
                                        alertReferenceNumber.dismiss();
                                    }
                                });

                                builderRecipient.setView(lvReferenceNumber);
                                builderRecipient.setTitle("Delivery Order Number");

                                builderRecipient.setNegativeButton("Cancel", null);

                                alertReferenceNumber = builderRecipient.create();
                                alertReferenceNumber.show();

                            } catch (Exception e) {
                                LogCustom.e(e);
                            }

                        } else {
                            textEmptyData = "No Result for Delivery Order Number. Please try again";
                            showEmptyDataDialog().show();
                        }
                    }
                } catch (Exception e) {
                    LogCustom.e(e);
                    Log.i("crash", "crash");
//                    Crashlytics.log(returnCode, "getReferenceNumberResult", result);
//                    Crashlytics.logException(e);
                }

            }

            //result DetailListDo
            if (returnCode == 2) {

                try {
                    final JSONObject data = new JSONObject(result);

                    Log.i("yes11", result);

                    if (!result.equalsIgnoreCase("") || result.length() > 0) {
                        if (data.has("status") && !(data.isNull("status"))) {
                            if (data.getBoolean("status")) {

                                Intent i = new Intent(getActivity(), DeliveryOrderDetailActivity.class);
                                Log.i("companyId", "" + companyId);
                                i.putExtra("companyId", companyId);
                                i.putExtra("referenceNumber", selectReferenceNumber);
                                i.putExtra("resultIntent", result);
                                startActivity(i);

                            } else {
                                textEmptyData = "No Result for List Order Number. Please try again";
                                showEmptyDataDialog().show();
                            }
                        }
                    }
                } catch (Exception e) {
                    LogCustom.e(e);
                    Log.i("crash", "crash");
//                    Crashlytics.log(returnCode, "getDetailListDOResult", result);
//                    Crashlytics.logException(e);
                }
            }
        }

    }


    void callWebService(final WebServiceWMS webService, final OnRESTResultCallback onRESTResultCallback) {

        LogCustom.i(webService, "webService");
        //LogCustom.i( uri.getPath(), "uri" );
        pairHeader = Pair.create("Authorization", "Bearer " + accessToken);
        final List<Pair<String, String>> header = new ArrayList<>();
        header.add(pairHeader);
        Log.i("", "access token:" + accessToken);

        final String body = "";

        Log.i("", "header:" + header);

        AndroidOp.runInBackground(mActivity, new Runnable() {

            @Override
            public void run() {
                AndroidOp.runOnMainThread(mActivity, new Runnable() {

                    @Override
                    public void run() {
                        try {
                            restManager.sendRequest(webService.getHttpConnection(header, body, onRESTResultCallback, ""));
                        } catch (final Throwable e) {
                            LogCustom.e(e);
                            Log.i("", "msgNetworkError 22");
//                            Crashlytics.log("callWebService error");
//                            Crashlytics.logException(e);

                        }
                    }

                });
            }
        });
    }


    public void getReferenceNumber(int companyId) {

        Log.i("companyIdget", "" + companyId);

        progressDialog = showProgressDialog();


        try {

            JSONObject jsonObject = new JSONObject();
            connection = new HttpConnection(Constants.SAAS_SERVER_WMS + "/worker/api/scan/do_out?customer_id=" + companyId, RESTIntentService.GET, jsonObject, this);

            List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();

            Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
            header.add(pair);
            connection.setHeaders(header);

            Log.i("", "access token:" + accessToken);
            Log.i("connectGetReferenceNo", " " + connection.toString());

            restManager.sendRequestWithReturn(1, connection);
        } catch (Exception e) {

            if (e instanceof NoInternetConnectionException) {

            } else {

                Log.i("crash", "crash");
//                Crashlytics.log("getReferenceNumber");
//                Crashlytics.logException(e);
//                    try {
//                        Crashlytics.logException( e );
//                    } catch ( final Throwable t ) {}

            }

        }

    }

    public void getDetailListDO() {

        progressDialog = showProgressDialog();

        try {

            JSONObject jsonObject = new JSONObject();
            connection = new HttpConnection(Constants.SAAS_SERVER_WMS + "/worker/api/scan/do_out?customer_id=" + companyId + "&reference_no=" + selectReferenceNumber, RESTIntentService.GET, jsonObject, this);

            List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();

            Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
            header.add(pair);
            connection.setHeaders(header);

            Log.i("", "access token:" + accessToken);
            Log.i("connectGetReferenceNo", " " + connection.toString());

            restManager.sendRequestWithReturn(2, connection);
        } catch (Exception e) {

            if (e instanceof NoInternetConnectionException) {

            } else {

                Log.i("crash", "crash");

//                try {
//                    Crashlytics.logException( e );
//                } catch ( final Throwable t ) {}

            }

        }

    }

    @Override
    public void onRequestInit() {

//        try {
//            pd.show();
//        } catch ( final Throwable e ) {
//            LogCustom.e( e );
//        }

    }

    @Override
    public void onRequestFinish() {

//        try {
//            if ( ( pd != null ) && pd.isShowing() ) {
//                pd.dismiss();
//            }
//        } catch ( final IllegalArgumentException e ) {
//            // Handle or log or ignore
//        } catch ( final Exception e ) {
//            // Handle or log or ignore
//        }
    }

    SweetAlertDialog showProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = getProgressDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE).setContentText(
                getString(R.string.msgProcessingProcessing)).setTitleText("");
        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    void stopProgressDialogAndShowMsg(final SweetAlertDialog progressDialog, final Integer msgResId) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismissWithAnimation();
        if (msgResId != null)
            Toast.makeText(getActivity(), msgResId, Toast.LENGTH_SHORT).show();
    }

    SweetAlertDialog showErrorDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                .setContentText(textErrorMessage)
                .setTitleText("ERROR")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showEmptyDataDialog() {
        final SweetAlertDialog sweetAlertDialog = getEmptyDataDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getEmptyDataDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setContentText(textEmptyData)
                // .setCustomImage(R.drawable.custom_img)
                .setTitleText("EMPTY")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }
}



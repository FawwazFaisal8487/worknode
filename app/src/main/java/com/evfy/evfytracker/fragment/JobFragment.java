package com.evfy.evfytracker.fragment;

import static android.content.Context.LOCATION_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.MultiDex;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.disegnator.robotocalendar.RobotoCalendarView;
import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity;
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Activity.Tracking.SelfHostedGPSTrackerService;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.Utils.CustomViewPager;
import com.evfy.evfytracker.Utils.ItemClickListener;
import com.evfy.evfytracker.adapter.JobAdapter;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.JobSteps;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.evfy.evfytracker.classes.OrderAttemptImage;
import com.evfy.evfytracker.classes.OrderDetails;
import com.facebook.stetho.Stetho;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class JobFragment extends Fragment
        implements RobotoCalendarView.RobotoCalendarListener, LocationListener, ItemClickListener {


    /* ======== DECLARATION ================ */

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1, REQUEST_CODE_REFRESH_ACTIVITY = 2;
    public static final String ACTION_REFRESH_JOB_LISTING = "refreshJobListing";
    public static final String ACTION_TOGGLE_ONLINE_OFFLINE = "toggleIsOnlineOffline";
    private static final int MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 1001;
    private final Handler textHandler = new Handler();
    private final List<String> TITLES = new ArrayList<String>();

    /* ==== DATA TYPE ==== */
    /* ==== DATA TYPE ==== */
    int sortCol = 4, sortOrder = 1, mult = 0, selecedCalendarMonth = 0, selectedTab = 0, workerId, resource_owner_id, day, month, year, currentMonthIndex;
    double lat, lon;
    float resizeHeight, viewHeight;
    String query = "", jobStateId, textErrorMessage;
    boolean getAllMonth = false, isSelectAll = false, needReload, firstLoad, firstCalendarTap, calendarOpen = false, searchButtonPressed = false,
            mIsImageUploaded, endOfList, isFactory, isManPower, fromCamera = false, isOnline, isAssignedToAcknowledgedNeedScan = false, isManPowerAssignedToAcknowledged = false;
    LinearLayout layoutAllBtn;
    RelativeLayout layoutPager, bottomLayout;
    Button btnComplete, btnCancel, btnAccept, btnArrived;
    View view5, view6, view7;
    CustomViewPager pagers;
    FloatingActionsMenu floatingActionMenu;
    RecyclerView searchResultListView;
    MenuItem.OnMenuItemClickListener mListenerRefresh;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        long timeLastReceival = 0L;

        @Override
        public void onReceive(final Context context, final Intent intent) {
            LogCustom.i();
            final long now = System.currentTimeMillis();
            if (now - timeLastReceival > DrawerActivity.MIN_INTERVAL && mListenerRefresh != null)
                mListenerRefresh.onMenuItemClick(null);
            timeLastReceival = now;
        }

    };
    RobotoCalendarView calendarLayout;
    Menu actionBarMenu;
    EditText editsearch, et;
    /* ===== LAYOUT ===== */
    AlertDialog alert;
    TextView dateTextView;
    Pair<String, String> pairHeader;
    SharedPreferences settings;
    JobAdapter listAdapter;
    Date selectedDate;
    DatabaseHandlerJobs dbUpdateJobs;
    JobOrder job;
    JSONArray jobOrderIdArray;
    boolean neverShowMessageUpdateLatestVersionCheckBox;
    boolean doForAllJobCheckBox;
    boolean unauthorizedNeedToLoginPage = false;
    int assignedOrderStatusIDFromDB, ackowledgedOrderStatusIDFromDB, inprogressOrderStatusIDFromDB, completedOrderStatusIDFromDB, failedOrderStatusIDFromDB, selfcollectOrderStatusIDFromDB;
    int cancelledOrderStatusIDFromDB;
    /* ============== LIST ARRAY ======== */
    List<JobOrder> assignedJobList = new ArrayList<JobOrder>();
    List<JobOrder> acknowledgedJobList = new ArrayList<JobOrder>();
    List<JobOrder> inProgressJobList = new ArrayList<JobOrder>();
    List<JobOrder> completedJobList = new ArrayList<JobOrder>();
    List<JobOrder> cancelledJobList = new ArrayList<JobOrder>();
    List<JobOrder> failedJobList = new ArrayList<JobOrder>();
    List<JobOrder> selfCollectJobList = new ArrayList<JobOrder>();
    //List<List<JobOrder>> mainStatusJobList = new ArrayList<>();
    List<JobOrder> assignedJobListExtraWorker = new ArrayList<JobOrder>();
    List<JobOrder> allJobList = new ArrayList<JobOrder>();
    List<JobAssignedFragment> jobAssignedFragment = new ArrayList<>();
    List<String> statusName = new ArrayList<>();
    List<JobOrder> searchResultJobList = new ArrayList<JobOrder>();
    List<JobOrder> addJobUpdateDetailsIntoDBNoInternet = new ArrayList<JobOrder>();
    List<JobOrder> addJobDetailsIntoDBNoInternet = new ArrayList<JobOrder>();
    /* ============== LIST ARRAY ======== */
    SweetAlertDialog sweetAlertDialog;
    String mVersionFromPlayStore;
    String currentVersion;
    Dialog pdProcess;
    TextView contextProgressDialog;
    BroadcastReceiver mReceiverRefreshJobListing;
    MenuItem menuSearch;
    /* ===== LAYOUT ===== */
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private ImageButton calendarButton;
    private Calendar cal, jobCalendar, currentCalendar;
    private Activity mActivity;


    /* ======== DECLARATION ================ */
    private final Runnable textRunnable = new Runnable() {

        public void run() {

            searchResultJobList.clear();

            //Timber.d( "0000 searching text runnable");
            mActivity.setProgressBarIndeterminateVisibility(Boolean.TRUE);

            mult = 0;
            endOfList = false;
            if (settings.contains("sort_column")) {
                sortCol = settings.getInt("sort_column", 0);
            } else {
                sortCol = 0;
            }

            if (settings.contains("sort_order")) {
                sortOrder = settings.getInt("sort_order", 0);
            } else {
                sortOrder = 1;
            }

            if (selectedTab == 0) {
                jobAssignedFragment.get(0).clearJobList(selectedDate);

            } else if (selectedTab == 1) {
                jobAssignedFragment.get(1).clearJobList(selectedDate);

            } else if (selectedTab == 2) {
                jobAssignedFragment.get(2).clearJobList(selectedDate);

            } else if (selectedTab == 3) {
                jobAssignedFragment.get(3).clearJobList(selectedDate);

            } else if (selectedTab == 4) {
                jobAssignedFragment.get(4).clearJobList(selectedDate);

            } else if (selectedTab == 5) {
                jobAssignedFragment.get(5).clearJobList(selectedDate);

            } else if (selectedTab == 6) {
                jobAssignedFragment.get(6).clearJobList(selectedDate);
            }

            startSearching(query);

        }

    };
    private final TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void afterTextChanged(Editable s) {
            // TODO Auto-generated method stub
            String text = editsearch.getText().toString().toLowerCase(Locale.getDefault());


            Log.i("text watcher after", "text watcher after text change:" + text);

            if (text.length() > 0) {
                query = text;
                textHandler.removeCallbacks(textRunnable);
                textHandler.postDelayed(textRunnable, 500);
            } else {
                searchResultJobList.clear();
                LogCustom.i("heeree", "111");

                reloadJobList(searchResultJobList, needReload, endOfList, true, false);
            }

        }

    };
    private MyPagerAdapter adapter;
    private ProgressDialog mProgressDialog;

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        floatingActionMenu.collapse();
        fromCamera = true;

        Log.i("", "1212 on activity result:" + requestCode);

        switch (requestCode) {

            //Update UI refresh
            case REQUEST_CODE_REFRESH_ACTIVITY:

                mActivity.finish();
                startActivity(mActivity.getIntent());
                break;

            case 1:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle b = data.getExtras();
                    if (b != null) {

                    }
                } else if (resultCode == 0) {
                    System.out.println("RESULT CANCELLED");
                }
                break;
            case 80:

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mActivity)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", okListener)
                .create()
                .show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        // Timber.d( "on attach mactivity null:" + mActivity + "get activity:" + getActivity());
        MultiDex.install(mActivity);
        LogCustom.i("InstallMultiDex");

    }

    public int compareDate(Date d1, Date d2) {
        if (d1.getYear() != d2.getYear())
            return Constants.INVALID_DATE;
        if (d1.getMonth() != d2.getMonth())
            return Constants.INVALID_DATE;
        return d1.getDate() - d2.getDate();
    }

    public boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(getActivity().ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (SelfHostedGPSTrackerService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {


                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean phoneStateAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean writeExternalStorageAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean readExternalStorageAccepted = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && phoneStateAccepted && writeExternalStorageAccepted && readExternalStorageAccepted) {

                    } else {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                                showMessageOKCancel("You need to allow access to all the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                                                }
                                            }
                                        });
                                return;
                            } else if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_PHONE_STATE)) {
                                showMessageOKCancel("You need to allow access to all the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                                                }
                                            }
                                        });
                                return;
                            } else if (shouldShowRequestPermissionRationale(android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("You need to allow access to all the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                                                }
                                            }
                                        });
                                return;
                            } else if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                showMessageOKCancel("You need to allow access to all the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                            REQUEST_ID_MULTIPLE_PERMISSIONS);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }


                }
            }

        }
    }

    public void setupJobDetailsIntoDBNoInternet() {

        LogCustom.i("internetConnection", "no");
        LogCustom.i("assignedJobList", ": " + assignedJobList.size());
        LogCustom.i("assignedJobListExtraWorker", ": " + assignedJobListExtraWorker.size());

        addJobDetailsIntoDBNoInternet.clear();
        addJobUpdateDetailsIntoDBNoInternet.clear();

        if (assignedJobList.size() != assignedJobListExtraWorker.size() || !isManPower) {

            addJobDetailsIntoDBNoInternet.clear();
            addJobUpdateDetailsIntoDBNoInternet.clear();


            LogCustom.i("NotSearchingAssigned ", "yes");


            for (int i = 0; i < assignedJobList.size(); i++) {

                JobOrder job = assignedJobList.get(i);

                Log.i("", "job size:" + assignedJobList.size());
                Log.i("", "job tracking num:" + job.getItemTrackingNumber());


                job.setUpdateWithNoInternet(false);
                // checkBox for assigned

                if (job.isSelected()) {
                    LogCustom.i("job selected", "job selected:" + assignedJobList.get(i).isSelected());
                    LogCustom.i("job selected 11", "job selected:" + job.getOrderId());


                    job.setUpdateWithNoInternet(true);
                    job.setOrderStatusId(ackowledgedOrderStatusIDFromDB);


                    addJobUpdateDetailsIntoDBNoInternet.add(job);


                } else {
                    //  addJobDetailsIntoDBNoInternet.add(job);
                }


            }


            if (addJobUpdateDetailsIntoDBNoInternet == null || addJobUpdateDetailsIntoDBNoInternet.size() == 0) {
                textErrorMessage = "You must select the job to accept";
                showErrorDialog("Error", textErrorMessage).show();

            } else {

//                for (int i = 0; i < acknowledgedJobList.size(); i++) {
//                    JobOrder job = acknowledgedJobList.get(i);
//                    job.setUpdateWithNoInternet(false);
//                    addJobDetailsIntoDBNoInternet.add(job);
//                }
//
//                for (int i = 0; i < inProgressJobList.size(); i++) {
//                    JobOrder job = inProgressJobList.get(i);
//                    job.setUpdateWithNoInternet(false);
//                    addJobDetailsIntoDBNoInternet.add(job);
//                }
//
//                for (int i = 0; i < completedJobList.size(); i++) {
//                    JobOrder job = completedJobList.get(i);
//                    job.setUpdateWithNoInternet(false);
//                    addJobDetailsIntoDBNoInternet.add(job);
//                }
//
//                for (int i = 0; i < cancelledJobList.size(); i++) {
//                    JobOrder job = cancelledJobList.get(i);
//                    job.setUpdateWithNoInternet(false);
//                    addJobDetailsIntoDBNoInternet.add(job);
//                }
//
//                for (int i = 0; i < failedJobList.size(); i++) {
//                    JobOrder job = failedJobList.get(i);
//                    job.setUpdateWithNoInternet(false);
//                    addJobDetailsIntoDBNoInternet.add(job);
//                }
//
//                for (int i = 0; i < selfCollectJobList.size(); i++) {
//                    JobOrder job = selfCollectJobList.get(i);
//                    job.setUpdateWithNoInternet(false);
//                    addJobDetailsIntoDBNoInternet.add(job);
//                }


                LogCustom.i("acknowledgeddd 1111", "job selected:");
                addJobDetailsIntoDBNoInternet.clear();
                addJobDetailsIntoDBNoInternet(addJobDetailsIntoDBNoInternet, addJobUpdateDetailsIntoDBNoInternet);

            }

        } else {

            isManPowerAssignedToAcknowledged = false;

            checkingFunctionBasedOnStatus();

            if (isManPowerAssignedToAcknowledged) {

                for (int i = 0; i < assignedJobList.size(); i++) {

                    JobOrder job = assignedJobList.get(i);

                    Log.i("", "job size:" + assignedJobList.size());
                    Log.i("", "job tracking num:" + job.getItemTrackingNumber());


                    job.setUpdateWithNoInternet(false);


                    // checkBox for assigned

                    if (job.isSelected()) {
                        LogCustom.i("job selected", "job selected:" + assignedJobList.get(i).isSelected());
                        LogCustom.i("job selected 11", "job selected:" + job.getOrderId());

                        job.setUpdateWithNoInternet(true);
                        job.setOrderStatusId(ackowledgedOrderStatusIDFromDB);

                        addJobUpdateDetailsIntoDBNoInternet.add(job);

                    } else {
                        //  addJobDetailsIntoDBNoInternet.add(job);
                    }


                }


                if (addJobUpdateDetailsIntoDBNoInternet == null || addJobUpdateDetailsIntoDBNoInternet.size() == 0) {
                    textErrorMessage = "You must select the job to accept";
                    showErrorDialog("Error", textErrorMessage).show();

                } else {


                    LogCustom.i("acknowledgeddd 1111", "job selected:");
                    addJobDetailsIntoDBNoInternet.clear();
                    addJobDetailsIntoDBNoInternet(addJobDetailsIntoDBNoInternet, addJobUpdateDetailsIntoDBNoInternet);

                }
            } else {
                showAlertManPower();
                // showErrorDialog(getActivity().getResources().getString(R.string.manPowerErrorTitle),getActivity().getResources().getString(R.string.manPowerErrorMessage)).show();
            }


        }

    }

    void fetchJobWithNoInternet(String currentDateString, String searchString) {

        contextProgressDialog.setText(getResources().getString(R.string.loadingJobListing));

        if (pdProcess.isShowing() == false) {
            pdProcess.show();
            LogCustom.i("load progress", "in");
        } else {
            LogCustom.i("load progress", "off");
        }

        if (sweetAlertDialog != null) {
            LogCustom.i("load progress", "11");
            if (sweetAlertDialog.isShowing()) {
                LogCustom.i("load progress", "22");
                showErrorDialog("", "").dismiss();
                sweetAlertDialog.dismiss();
                showErrorDialog("", "").cancel();
                sweetAlertDialog.cancel();
            }
        }

        String startDate;
        if (searchString.equalsIgnoreCase("")) {
            if (editsearch != null) {
                if (editsearch.getText().toString().trim().equalsIgnoreCase("")) {
                    startDate = currentDateString;
                } else {
                    startDate = "2018-01-01";
                    searchString = editsearch.getText().toString().trim();
                    LogCustom.i("searchString11 progress111", ":" + editsearch.getText().toString().trim());
                }

            } else {
                startDate = currentDateString;
            }


        } else {
            startDate = "2018-01-01";
        }


        searchButtonPressed = false;
        isSelectAll = false;

        try {

            allJobList.clear();
            assignedJobList.clear();
            acknowledgedJobList.clear();
            inProgressJobList.clear();
            completedJobList.clear();
            cancelledJobList.clear();
            failedJobList.clear();
            selfCollectJobList.clear();
            searchResultJobList.clear();
            assignedJobListExtraWorker.clear();
            addJobDetailsIntoDBNoInternet.clear();
            addJobUpdateDetailsIntoDBNoInternet.clear();


            for (int i = 0; i < jobAssignedFragment.size(); i++) {
                jobAssignedFragment.get(i).clearJobList(selectedDate);
            }

            List<JobOrder> fetchJobNoInternetList = new ArrayList<JobOrder>();


            fetchJobNoInternetList = dbUpdateJobs.getListJobDetailsNoInternet(startDate);

            LogCustom.i("loadfetchJobNoInternetList", ": " + fetchJobNoInternetList.size());

            if (fetchJobNoInternetList.size() > 0) {
                for (int j = 0; j < fetchJobNoInternetList.size(); j++) {
                    JobOrder jobOrder = fetchJobNoInternetList.get(j);

                    jobOrder.setJobStepsArrayList(dbUpdateJobs.getListJobStepsDetailsNoInternet(jobOrder.getOrderId()));
                    jobOrder.setOrderDetailsArrayList(dbUpdateJobs.getListJobItemsDetailsNoInternet(jobOrder.getOrderId()));

                    int jobStatus = jobOrder.getOrderStatusId();


                    if (jobStatus == assignedOrderStatusIDFromDB) {
                        if (editsearch != null && editsearch.getText().length() == 0) {
                            try {
                                allJobList.add(jobOrder);
                                assignedJobList.add(jobOrder);

                                if (jobOrder.getDropOffWorkerId() != resource_owner_id) {
                                    assignedJobListExtraWorker.add(jobOrder);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            assignedJobList.add(jobOrder);

                            if (jobOrder.getDropOffWorkerId() != resource_owner_id) {
                                assignedJobListExtraWorker.add(jobOrder);
                            }
                        }
                        searchResultJobList.add(jobOrder);

                        //  break;
                    } else if (jobStatus == ackowledgedOrderStatusIDFromDB) {
                        if (editsearch != null && editsearch.getText().length() == 0) {
                            allJobList.add(jobOrder);
                            acknowledgedJobList.add(jobOrder);
                            LogCustom.i(jobOrder.getDropOffWorkerId(), "getDropOffWorkerIdAcknowledged1");
                            LogCustom.i(resource_owner_id, "resource_owner_idAcknowledged1");

                            if (jobOrder.getDropOffWorkerId() != resource_owner_id) {
                                //  acknowledgedJobListExtraWorker.add(pickUpJobOrder);
                            }

                        } else {
                            acknowledgedJobList.add(jobOrder);
                            LogCustom.i(jobOrder.getDropOffWorkerId(), "getDropOffWorkerIdAcknowledged2");
                            LogCustom.i(resource_owner_id, "resource_owner_idAcknowledged2");

                            if (jobOrder.getDropOffWorkerId() != resource_owner_id) {
                                //  acknowledgedJobListExtraWorker.add(pickUpJobOrder);
                            }
                        }

                        searchResultJobList.add(jobOrder);
                        //  break;

                    } else if (jobStatus == inprogressOrderStatusIDFromDB) {
                        inProgressJobList.add(jobOrder);

                        if (editsearch != null && editsearch.getText().length() == 0) {
                            allJobList.add(jobOrder);
                        }
                        searchResultJobList.add(jobOrder);

                    } else if (jobStatus == completedOrderStatusIDFromDB) {
                        if (editsearch != null && editsearch.getText().length() == 0) {
                            allJobList.add(jobOrder);
                            completedJobList.add(jobOrder);
                        }

                        searchResultJobList.add(jobOrder);
                        //   break;

                    } else if (jobStatus == cancelledOrderStatusIDFromDB) {
                        if (editsearch != null && editsearch.getText().length() == 0) {
                            allJobList.add(jobOrder);
                            cancelledJobList.add(jobOrder);
                        }

                        searchResultJobList.add(jobOrder);
                        //   break;
                    } else if (jobStatus == failedOrderStatusIDFromDB) {
                        if (editsearch != null && editsearch.getText().length() == 0) {
                            allJobList.add(jobOrder);
                            failedJobList.add(jobOrder);
                        }

                        searchResultJobList.add(jobOrder);
                        //   break;

                    } else if (jobStatus == selfcollectOrderStatusIDFromDB) {
                        selfCollectJobList.add(jobOrder);

                        if (editsearch != null && editsearch.getText().length() == 0) {
                            allJobList.add(jobOrder);
                        }
                        searchResultJobList.add(jobOrder);

                        //  break;


                    }
                }

                if (pdProcess.isShowing()) {
                    pdProcess.dismiss();
                    LogCustom.i("load progress", "in");
                } else {
                    LogCustom.i("load progress", "off");
                }
            } else {
                if (pdProcess.isShowing()) {
                    pdProcess.dismiss();
                    LogCustom.i("load progress", "in");
                } else {
                    LogCustom.i("load progress", "off");
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }


        if (editsearch != null && editsearch.getText().length() == 0) {

            //hide show after reload data
            updateHideShowBottomButton(selectedTab);


            searchResultJobList.clear();

            jobAssignedFragment.get(0).reloadJobList(assignedJobList, needReload, endOfList, selectedDate);
            jobAssignedFragment.get(1).reloadJobList(acknowledgedJobList, needReload, endOfList, selectedDate);
            jobAssignedFragment.get(2).reloadJobList(inProgressJobList, needReload, endOfList, selectedDate);
            jobAssignedFragment.get(3).reloadJobList(completedJobList, needReload, endOfList, selectedDate);
            jobAssignedFragment.get(4).reloadJobList(cancelledJobList, needReload, endOfList, selectedDate);
            jobAssignedFragment.get(5).reloadJobList(failedJobList, needReload, endOfList, selectedDate);
            jobAssignedFragment.get(6).reloadJobList(selfCollectJobList, needReload, endOfList, selectedDate);

            updateCalendar();

            tabs.updateJob(assignedJobList.size(), acknowledgedJobList.size(), inProgressJobList.size(), completedJobList.size(), cancelledJobList.size(), failedJobList.size(), selfCollectJobList.size());

        } else {
            LogCustom.i("heeree", "222");
            reloadJobList(searchResultJobList, needReload, endOfList, true, true);
        }


    }

    public void addJobDetailsIntoDBNoInternet(List<JobOrder> addJobDetailsIntoDBNoInternet, List<JobOrder> addJobUpdateDetailsIntoDBNoInternet) {

        LogCustom.i("addJobUpdateDetailsIntoDBNoInternet", ": " + addJobUpdateDetailsIntoDBNoInternet.size());
        LogCustom.i("addJobDetailsIntoDBNoInternet", ": " + addJobDetailsIntoDBNoInternet.size());

        if (addJobUpdateDetailsIntoDBNoInternet.size() > 0) {
            for (int i = 0; i < addJobUpdateDetailsIntoDBNoInternet.size(); i++) {
                JobOrder jobOrder = addJobUpdateDetailsIntoDBNoInternet.get(i);

                if (dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                    jobOrder.setUpdateWithNoInternet(true);
                } else {
                    if (jobOrder.isUpdateWithNoInternet()) {

                    } else {
                        jobOrder.setUpdateWithNoInternet(false);
                    }

                }

                LogCustom.i("isUpdateWithNoInternet", ": " + jobOrder.isUpdateWithNoInternet());

                dbUpdateJobs.addJobDetails(jobOrder);

                if (jobOrder.getJobStepsArrayList().size() > 0) {
                    for (int ii = 0; ii < jobOrder.getJobStepsArrayList().size(); ii++) {
                        JobSteps jobSteps = jobOrder.getJobStepsArrayList().get(ii);
                        jobSteps.setJobStepUpdateWithNoInternet(true);
                        jobSteps.setJob_step_status_id(Constants.JOB_STEP_PENDING);
                        dbUpdateJobs.addJobStepDetails(jobSteps);

                        LogCustom.i("jobStepsOrderAttempt", ": " + jobSteps.getmOrderAttemptsList().size());

                        if (jobSteps.getmOrderAttemptsList().size() > 0) {
                            for (int j = 0; j < jobSteps.getmOrderAttemptsList().size(); j++) {
                                OrderAttempt orderAttempt = jobSteps.getmOrderAttemptsList().get(j);
                                orderAttempt.setOrderId(jobOrder.getOrderId());
                                orderAttempt.setAttemptUpdateWithNoInternet(false);
                                dbUpdateJobs.addOrderAttemptDetails(orderAttempt);

                                if (orderAttempt.getmOrderAttemptImages().size() > 0) {
                                    for (int k = 0; k < orderAttempt.getmOrderAttemptImages().size(); k++) {
                                        OrderAttemptImage orderAttemptImage = orderAttempt.getmOrderAttemptImages().get(k);

                                        orderAttemptImage.setOrderId(jobOrder.getOrderId());
                                        orderAttemptImage.setId(orderAttemptImage.getId());
                                        orderAttemptImage.setNote(orderAttemptImage.getNote());
                                        orderAttemptImage.setSignature(orderAttemptImage.isSignature());
                                        orderAttemptImage.setRemove(false);
                                        orderAttemptImage.setOrderAttemptID(orderAttempt.getId());
                                        orderAttemptImage.setImageUpdateNoInternet(false);
                                        orderAttemptImage.setUrl(orderAttemptImage.getUrl());
                                        dbUpdateJobs.addImageDetails(orderAttemptImage);
                                    }
                                }
                            }
                        }
                    }
                }

                if (jobOrder.getOrderDetailsArrayList().size() > 0) {
                    for (int ii = 0; ii < jobOrder.getOrderDetailsArrayList().size(); ii++) {
                        OrderDetails orderDetails = jobOrder.getOrderDetailsArrayList().get(ii);
                        dbUpdateJobs.addJobItemDetails(orderDetails);
                    }
                }
            }
        }


        if (addJobDetailsIntoDBNoInternet.size() > 0) {
            for (int i = 0; i < addJobDetailsIntoDBNoInternet.size(); i++) {
                JobOrder jobOrder = addJobDetailsIntoDBNoInternet.get(i);


                if (dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                    jobOrder.setUpdateWithNoInternet(true);
                } else {
                    if (jobOrder.isUpdateWithNoInternet()) {

                    } else {
                        jobOrder.setUpdateWithNoInternet(false);
                    }
                }

                dbUpdateJobs.addJobDetails(jobOrder);

                if (jobOrder.getJobStepsArrayList().size() > 0) {
                    for (int ii = 0; ii < jobOrder.getJobStepsArrayList().size(); ii++) {
                        JobSteps jobSteps = jobOrder.getJobStepsArrayList().get(ii);
                        jobSteps.setJobStepUpdateWithNoInternet(false);
                        dbUpdateJobs.addJobStepDetails(jobSteps);

                        LogCustom.i("jobStepsOrderAttempt11", ": " + jobSteps.getId() + "orderID:" + jobSteps.getOrderId() + "size:" + jobSteps.getmOrderAttemptsList().size());

                        if (jobSteps.getmOrderAttemptsList().size() > 0) {
                            for (int j = 0; j < jobSteps.getmOrderAttemptsList().size(); j++) {
                                OrderAttempt orderAttempt = jobSteps.getmOrderAttemptsList().get(j);
                                orderAttempt.setOrderId(jobOrder.getOrderId());
                                orderAttempt.setAttemptUpdateWithNoInternet(false);
                                dbUpdateJobs.addOrderAttemptDetails(orderAttempt);


                                if (orderAttempt.getmOrderAttemptImages().size() > 0) {
                                    for (int k = 0; k < orderAttempt.getmOrderAttemptImages().size(); k++) {
                                        OrderAttemptImage orderAttemptImage = orderAttempt.getmOrderAttemptImages().get(k);

                                        orderAttemptImage.setOrderId(jobOrder.getOrderId());
                                        orderAttemptImage.setId(orderAttemptImage.getId());
                                        orderAttemptImage.setNote(orderAttemptImage.getNote());
                                        orderAttemptImage.setSignature(orderAttemptImage.isSignature());
                                        orderAttemptImage.setRemove(false);
                                        orderAttemptImage.setOrderAttemptID(orderAttempt.getId());
                                        orderAttemptImage.setImageUpdateNoInternet(false);
                                        orderAttemptImage.setUrl(orderAttemptImage.getUrl());
                                        dbUpdateJobs.addImageDetails(orderAttemptImage);
                                    }
                                }

                            }
                        }
                    }
                }

                if (jobOrder.getOrderDetailsArrayList().size() > 0) {
                    for (int ii = 0; ii < jobOrder.getOrderDetailsArrayList().size(); ii++) {
                        OrderDetails orderDetails = jobOrder.getOrderDetailsArrayList().get(ii);
                        dbUpdateJobs.addJobItemDetails(orderDetails);
                    }
                }
            }
        }


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateString = sdf.format(selectedDate);
        fetchJobWithNoInternet(currentDateString, "");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        dbUpdateJobs = new DatabaseHandlerJobs(mActivity);
        boolean isTableExist = dbUpdateJobs.isTableExists("UpdatedJobs", true);

        if (isTableExist == false) {
            dbUpdateJobs.onUpgrade(dbUpdateJobs.getWritableDatabase(), dbUpdateJobs.getWritableDatabase().getVersion(), dbUpdateJobs.getWritableDatabase().getVersion() + 1);
        }

        Stetho.initializeWithDefaults(getActivity());

        /*Get order status id based on order status name*/
        assignedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.ASSIGNED_TEXT);
        ackowledgedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.ACKNOWLEDGED_TEXT);
        inprogressOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT);
        completedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.COMPLETED_TEXT);
        failedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.FAILED_TEXT);
        cancelledOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.CANCELLED_TEXT);
        selfcollectOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.SELFCOLLECT_TEXT);

        LogCustom.i("services", "is: " + isMyServiceRunning());


        unauthorizedNeedToLoginPage = false;
        firstLoad = true;

        firstCalendarTap = true;

        //viewCreatedBool =true;
        jobStateId = "1,3";
        // dbUpdateJobs = new DatabaseHandlerJobs(getActivity());

        Log.i("", "job fragment on create view");
        //Timber.d( "dbUpdateJobs size 00:" + dbUpdateJobs.getAllJobs().size());

        //settings = mActivity.getSharedPreferences(PREFS_NAME, 0);

        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());

        SharedPreferences.Editor editors = settings.edit();
        editors.putBoolean("insideFragmentPage", true).commit();
        editors.putBoolean("insideJobDetails", false).commit();
        editors.putBoolean("insideNotificationListing", false).commit();
        editors.apply();

        workerId = Integer.parseInt(settings.getString("worker_id", "0"));
        ((DrawerActivity) mActivity).getSupportActionBar().setTitle("My Jobs");

        setHasOptionsMenu(true);

        isFactory = settings.getBoolean("is_factory", false);
        isManPower = settings.getBoolean("isManPower", false);
        resource_owner_id = settings.getInt("resource_owner_id", 0);

//        Fabric.with(getContext(), new Crashlytics());

        View rootView = inflater.inflate(R.layout.activity_job_tabs, container, false);
        calendarButton = rootView.findViewById(R.id.imageButton1);
        bottomLayout = rootView.findViewById(R.id.bottomLayout);
        searchResultListView = rootView.findViewById(R.id.searchResultList);
        searchResultListView.setHasFixedSize(true);
        dateTextView = rootView.findViewById(R.id.dateTextView);


        floatingActionMenu = rootView.findViewById(R.id.floatingActionMenu);

        //Update UI
        pagers = rootView.findViewById(R.id.pager);
        layoutAllBtn = rootView.findViewById(R.id.layoutAllBtn);
        layoutPager = rootView.findViewById(R.id.layoutPager);

        btnAccept = rootView.findViewById(R.id.btnAccept);
        btnComplete = rootView.findViewById(R.id.btnComplete);
        btnCancel = rootView.findViewById(R.id.btnCancel);
        btnArrived = rootView.findViewById(R.id.btnArrived);

        view5 = rootView.findViewById(R.id.view5);
        view6 = rootView.findViewById(R.id.view6);
        view7 = rootView.findViewById(R.id.view7);

        //Update UI
        layoutAllBtn.setVisibility(View.VISIBLE);
        view5.setVisibility(View.GONE);
        view6.setVisibility(View.GONE);
        view7.setVisibility(View.GONE);

        btnAccept.setVisibility(View.GONE);
        btnComplete.setVisibility(View.GONE);
        btnCancel.setVisibility(View.GONE);
        btnArrived.setVisibility(View.GONE);

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GeneralActivity generalActivity = new GeneralActivity();
                if (!generalActivity.haveInternetConnected(getContext())) {
                    wantProceedIfNoInternet(1);
                } else {
                    isOnline = settings.getBoolean("isOnline", true);

                    if (checkGPSBeforeUpdate()) {
                        if (isOnline) {
                            if (assignedJobList.size() != assignedJobListExtraWorker.size()) {

                                jobOrderIdArray = new JSONArray();


                                Log.i("NotSearchingAssigned11", "yes");


                                for (int i = 0; i < assignedJobList.size(); i++) {

                                    JobOrder job = assignedJobList.get(i);

                                    Log.i("", "job size:" + assignedJobList.size());
                                    Log.i("", "job tracking num:" + job.getItemTrackingNumber());


                                    // checkBox for assigned

                                    if (job.isSelected()) {
                                        LogCustom.i("job selected", "job selected:" + assignedJobList.get(i).isSelected());
                                        LogCustom.i("job selected 11", "job selected:" + job.getOrderId());
                                        jobOrderIdArray.put(String.valueOf(job.getOrderId()));
                                    }
                                    Log.i("", "job selected:" + assignedJobList.get(i).isSelected());
                                }


                                if (jobOrderIdArray == null || jobOrderIdArray.length() == 0) {
                                    textErrorMessage = "You must select the job to accept";
                                    showErrorDialog("Error", textErrorMessage).show();

                                } else {
                                    LogCustom.i("acknowledgeddd 1111", "job selected:");
                                    acceptJobToAcknowledged(jobOrderIdArray);

                                }

                            } else {

                                isManPowerAssignedToAcknowledged = false;

                                checkingFunctionBasedOnStatus();

                                if (isManPowerAssignedToAcknowledged) {

                                    jobOrderIdArray = new JSONArray();

                                    for (int i = 0; i < assignedJobList.size(); i++) {

                                        JobOrder job = assignedJobList.get(i);

                                        Log.i("", "job size:" + assignedJobList.size());
                                        Log.i("", "job tracking num:" + job.getItemTrackingNumber());


                                        // checkBox for assigned

                                        if (job.isSelected()) {
                                            LogCustom.i("job selected", "job selected:" + assignedJobList.get(i).isSelected());
                                            LogCustom.i("job selected 11", "job selected:" + job.getOrderId());
                                            jobOrderIdArray.put(String.valueOf(job.getOrderId()));
                                        }
                                        Log.i("", "job selected:" + assignedJobList.get(i).isSelected());
                                    }

                                    if (jobOrderIdArray == null || jobOrderIdArray.length() == 0) {
                                        textErrorMessage = "You must select the job to accept";
                                        showErrorDialog("Error", textErrorMessage).show();

                                    } else {
                                        LogCustom.i("acknowledgeddd 1111", "job selected:");
                                        acceptJobToAcknowledged(jobOrderIdArray);

                                    }
                                } else {
                                    showAlertManPower();
                                    // showErrorDialog(getActivity().getResources().getString(R.string.manPowerErrorTitle),getActivity().getResources().getString(R.string.manPowerErrorMessage)).show();
                                }


                            }
                        } else {
                            showNeedOnlineDialog(getActivity().getResources().getString(R.string.offlineAcceptJobTitle), getActivity().getResources().getString(R.string.offlineMessage)).show();
                            LogCustom.i("isOnline", "false");
                        }
                    } else {
                        showNeedOnlineDialog(getActivity().getResources().getString(R.string.offlineAcceptJobTitle), getActivity().getResources().getString(R.string.offlineMessage)).show();
                    }


                }


            }
        });


        calendarLayout = rootView.findViewById(R.id.robotoCalendarPicker);

        calendarLayout.setRobotoCalendarListener(this);

        // Initialize the RobotoCalendarPicker with the current index and date
        currentMonthIndex = 0;
        currentCalendar = Calendar.getInstance(Locale.getDefault());

        // Mark current day
        calendarLayout.markDayAsCurrentDay(currentCalendar.getTime());
        selectedDate = currentCalendar.getTime();
        SimpleDateFormat format1 = new SimpleDateFormat("dd MMMM yyyy");
        if (selectedDate != null && dateTextView != null) {
            dateTextView.setText(format1.format(selectedDate));
        }

        selecedCalendarMonth = currentCalendar.get(Calendar.MONTH) + 1;

        //Timber.d("selected Calendar Month:"+selecedCalendarMonth);

        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        et = rootView.findViewById(R.id.editText);
        jobCalendar = Calendar.getInstance();

        pdProcess = new Dialog(getActivity());
        LayoutInflater inflaters = getActivity().getLayoutInflater();
        View content = inflaters.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        contextProgressDialog.setText(getResources().getString(R.string.loadingJobListing));
        pdProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdProcess.setContentView(content);
        pdProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdProcess.setCancelable(false);

        //checkingDriverStatusAndLocationStatus();


        mReceiverRefreshJobListing = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LogCustom.i("refresh job listing true");
                onResume();
            }
        };

        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mReceiverRefreshJobListing, new IntentFilter(ACTION_REFRESH_JOB_LISTING));

        return rootView;
    }

    public void showAlertManPower() {
        //  showErrorDialog(getActivity().getResources().getString(R.string.manPowerErrorTitle),getActivity().getResources().getString(R.string.manPowerErrorMessage)).show();

        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(getActivity().getResources().getString(R.string.manPowerErrorMessage));
        builder1.setCancelable(false);
        builder1.setTitle(getActivity().getResources().getString(R.string.manPowerErrorTitle));

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public boolean checkGPSBeforeUpdate() {
        boolean alreadyOnGps;
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LogCustom.i("GPS is Enabled in your device true");

            alreadyOnGps = true;

        } else {


            SharedPreferences.Editor editors = settings.edit();
            editors.putBoolean("isOnline", false).apply();

            alreadyOnGps = false;

            Intent ii = new Intent(JobFragment.ACTION_TOGGLE_ONLINE_OFFLINE);
            LocalBroadcastManager.getInstance(getContext())
                    .sendBroadcast(ii);

        }

        return alreadyOnGps;
    }

    public void acceptJobToAcknowledged(JSONArray jobOrderIdArray) {
        try {

            JSONObject jobOrderObject = new JSONObject();

            jobOrderObject.put("id", jobOrderIdArray);

            jobOrderObject.put("order_status_id", ackowledgedOrderStatusIDFromDB);


            JSONObject dataObject = new JSONObject();
            JSONArray jobDataArray = new JSONArray();
            jobDataArray.put(jobOrderObject);
            dataObject.put("data", jobDataArray);
            LogCustom.i("dataObject", dataObject.toString(4));

            btnAccept.setEnabled(false);
            updateJobStatus(dataObject, jobOrderIdArray);

        } catch (JSONException e) {
            e.printStackTrace();
            try {
//                Crashlytics.logException(e);
            } catch (final Throwable t) {
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        needReload = true;
        firstLoad = true;
        tabs = view.findViewById(R.id.tabs);
        pager = view.findViewById(R.id.pager);
        getAllMonth = true;
        TITLES.clear();
        firstCalendarTap = true;
        Display display = mActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        Log.i("TITLES", "" + TITLES.size());

        statusName.clear();
        statusName.add("ASSIGNED");
        statusName.add("ACKNOWLEDGED");
        statusName.add("IN PROGRESS");
        statusName.add("COMPLETED");
        statusName.add("CANCELLED");
        statusName.add("FAILED");
        statusName.add("SELF COLLECT");

        jobAssignedFragment.clear();

        for (int i = 0; i < statusName.size(); i++) {
            jobAssignedFragment.add(new JobAssignedFragment());
            TITLES.add(statusName.get(i));
        }

        width = width / 5;


        adapter = new MyPagerAdapter(getChildFragmentManager(), mActivity);
        //
        pager.setAdapter(adapter);
//        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
//        pager.setPageMargin(pageMargin);
        pager.requestDisallowInterceptTouchEvent(true);

        //Timber.d( "screen width:" + width);

        Resources resource = getResources();
        float dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, 50, resource.getDisplayMetrics());

        //Timber.d("width final:"+(width-dp));
        tabs.setShouldExpand(true); //Works

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);

        tabs.setViewPager(pager);
        //      tabs.setIndicatorColorResource(R.color.greenStart);
        tabs.setIndicatorColorResource(R.color.white_pure);
        //  tabs.setBackgroundColor(Color.parseColor("#4D4D4D"));

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int i) {
                selectedTab = i;
                needReload = true;
                mult = 0;
                updateHideShowBottomButton(selectedTab);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }
    /* ===================== CHECKING FUNCTION FOR EVERY STATUS ======================== */

    /*
     * This func for show alert dialog popup where it success, failed, warning, no internet connection
     * */
    /* ================== SWEET ALERT DIALOG ===================== */

    /* ===================== CHECKING FUNCTION FOR EVERY STATUS ======================== */
    public void checkingFunctionBasedOnStatus() {
        if (dbUpdateJobs.checkStatusNeedScanOrManPower(Constants.ASSIGNED, Constants.ACKNOWLEDGED, true, "is_status_need_scan")) {
            LogCustom.i("Need scannn");
            isAssignedToAcknowledgedNeedScan = true;
        }

        if (dbUpdateJobs.checkStatusNeedScanOrManPower(Constants.ASSIGNED, Constants.ACKNOWLEDGED, false, "is_status_need_manpower")) {
            LogCustom.i("Man Power");
            isManPowerAssignedToAcknowledged = true;
        }

    }

    /* ================================ ONLINE / OFFLINE LOCATION SETTINGS =================================== */
    public void startLocationServices() {

        GeneralActivity generalActivity = new GeneralActivity();
        if (!generalActivity.isMyServiceLocationRunning(getContext())) {
            LogCustom.i("isMyServiceRunning need to start", "true");
            Intent intent = new Intent(getContext(), ForegroundLocationService.class);
            getActivity().startService(intent);

        } else {
            LogCustom.i("isMyServiceRunning already, not need to start", "true");
        }

        updateStatusOnlineOffline(true);
    }

    public void checkingDriverStatusAndLocationStatusToggle() {
        //if status online, location must on. else must be offline
        boolean isOnline = settings.getBoolean("isOnline", true);
        if (isOnline) {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true");
                SharedPreferences.Editor editors = settings.edit();
                editors.putBoolean("isOnline", true).apply();

                Intent ii = new Intent(JobFragment.ACTION_TOGGLE_ONLINE_OFFLINE);
                LocalBroadcastManager.getInstance(getContext())
                        .sendBroadcast(ii);

                startLocationServices();

            } else {
                showGPSDisabledAlertToUser();
            }

        } else {
            SharedPreferences.Editor editors = settings.edit();
            editors.putBoolean("isOnline", false).apply();
            stopLocationServices();
        }
    }

    public void updateStatusOnlineOffline(boolean is_online) {
        try {
            JSONObject jsonObject = new JSONObject();

            LogCustom.i("updateStatusOnlineOffline", "is_online : " + is_online);


            jsonObject.put("is_online", is_online);
            jsonObject.put("latitude", "");
            jsonObject.put("longitude", "");


            OkHttpRequest okHttpRequest = new OkHttpRequest();

            try {
                OkHttpRequest.driverUpdateProfile(getContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {

                                if (getActivity() == null) {

                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            if (body == null || body.isEmpty()) {
                                                LogCustom.i("valueIsNull", "yes");
                                            } else {
                                                LogCustom.i("update profile is success", "yes");
                                            }

                                        }
                                    });
                                }


                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                                if (getActivity() == null) {

                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //   errorHandlingFromResponseCode(body,requestCallBackError,responseCode);
                                        }
                                    });
                                }


                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    public void stopLocationServices() {

        GeneralActivity generalActivity = new GeneralActivity();
        if (generalActivity.isMyServiceLocationRunning(getContext())) {
            LogCustom.i("isMyServiceRunning need to stop", "true");
            Intent intent = new Intent(getContext(), ForegroundLocationService.class);
            getActivity().stopService(intent);
        } else {
            LogCustom.i("isMyServiceRunning not need to stop", "true");
        }

        updateStatusOnlineOffline(false);
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                getActivity().startActivityForResult(callGPSSettingIntent, Constants.INTENT_ACTIVITY_GPS_SETTINGS);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        SharedPreferences.Editor editors = settings.edit();
                        editors.putBoolean("isOnline", false).apply();

                        stopLocationServices();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void wantProceedIfNoInternet(final int optionProcess) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("No internet connection").setMessage("Your device not connect with internet. Please check your internet connection")
                .setCancelable(false)
                .setPositiveButton("Open settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                dialog.cancel();
                                final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                                intent.setComponent(cn);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alertDialogBuilder.setNeutralButton("Continue without internet",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        switch (optionProcess) {
                            case 1:  // Accept Job
                                setupJobDetailsIntoDBNoInternet();
                                break;
                            case 2:  // Reload Job
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                String currentDateString = sdf.format(selectedDate);
                                fetchJobWithNoInternet(currentDateString, "");
                                break;

                        }


                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    SweetAlertDialog showNeedOnlineDialog(String titleMessage, String contentMessage) {
        final SweetAlertDialog sweetAlertDialog = getNeedOnlineDialog(titleMessage, contentMessage);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    /* ================================ ONLINE / OFFLINE LOCATION SETTINGS =================================== */

    SweetAlertDialog getNeedOnlineDialog(String titleMessage, String contentMessage) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setContentText(contentMessage)
                .setTitleText(titleMessage)
                .setConfirmText("Yes")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        SharedPreferences.Editor editors = settings.edit();
                        editors.putBoolean("isOnline", true).apply();

                        checkingDriverStatusAndLocationStatusToggle();

                        sDialog.dismissWithAnimation();
                    }
                })
                .setCancelText("No")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();


                        if (unauthorizedNeedToLoginPage) {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (generalActivity.isMyServiceLocationRunning(getContext())) {
                                LogCustom.i("isMyServiceRunning", "true");
                                Intent intent = new Intent(getContext(), ForegroundLocationService.class);
                                getActivity().stopService(intent);
                            }

                            settings.edit().remove("isOnline").apply();
                            settings.edit().remove("googleToken").apply();
                            settings.edit().putString("user_name", "").commit();
                            unauthorizedNeedToLoginPage = false;

                            Intent i = new Intent(getContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.putExtra("EXIT", true);
                            startActivity(i);
                        }
                    }
                });


        sweetAlertDialog.setCancelable(true);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorNoInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorNoInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorNoInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        sDialog.dismiss();
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivityForResult(intent, Constants.OPEN_INTERNET_CONNECTION);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    ///success dialog
    SweetAlertDialog showSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = getSuccessDialog(title, context);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }
    /* ================== SWEET ALERT DIALOG ===================== */

    SweetAlertDialog getSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.SUCCESS_TYPE)
                .setContentText(context)
                .setTitleText(title)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        btnAccept.setEnabled(true);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String currentDateString = sdf.format(selectedDate);

                        try {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (!generalActivity.haveInternetConnected(getContext())) {
                                Log.i("internetConnection", "no");
                                if (!showErrorInternetDialog().isShowing()) {
                                    showErrorInternetDialog().show();
                                }
                            } else {
                                fetchJobList(currentDateString, "");
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    @Override
    public void onResume() {
        Log.i("onResume", "1212 on resume from camera:" + fromCamera);

        SharedPreferences.Editor editors = settings.edit();
        editors.putBoolean("insideFragmentPage", true).commit();
        editors.apply();


        super.onResume();
        if (fromCamera == false) {
            LocalBroadcastManager.getInstance(mActivity).registerReceiver(mReceiver, new IntentFilter(DrawerActivity.ACTION_REFRESH));

            if (editsearch == null || editsearch.getText().length() == 0) {
                //checkUnsendData();

            }
        } else {
            fromCamera = false;
        }

        checkingFunctionBasedOnStatus();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateString = sdf.format(selectedDate);

        if (pdProcess != null) {
            pdProcess.dismiss();
        }


        GeneralActivity generalActivity = new GeneralActivity();
        if (!generalActivity.haveInternetConnected(getContext())) {
            LogCustom.i("internetConnection onresume", "no");
            // showErrorInternetDialog().show();

            // fetchJobWithNoInternet(currentDateString,"");

            wantProceedIfNoInternet(2);

        } else {
            String userId = settings.getString("user_name", "");
            if (userId.length() == 0 || userId.equalsIgnoreCase("")) {
                settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
                editors.putString("user_name", "").commit();
                editors.putBoolean("isOnline", false).commit();
                editors.apply();

                if (generalActivity.isMyServiceLocationRunning(getContext())) {
                    LogCustom.i("isMyServiceRunning", "true");
                    Intent intent = new Intent(getContext(), ForegroundLocationService.class);
                    getActivity().stopService(intent);
                }

                Intent i = new Intent(getContext(), LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.putExtra("EXIT", true);
                startActivity(i);
            } else {
                // fetchJobList(currentDateString,"");
                //checkUnsendData();
                checkHaveLatestJobUpdate();
            }

        }

    }
    /* ================ UPDATE JOB STATUS ====================== */

    /* ================ UPDATE JOB STATUS ====================== */
    public void updateJobStatus(final JSONObject jobOrderObject, final JSONArray jobOrderIdArray) {


        contextProgressDialog.setText(getResources().getString(R.string.updateAssignedToAcknowledged));

        if (pdProcess.isShowing() == false) {
            pdProcess.show();
            LogCustom.i("load progress", "in");
        } else {
            LogCustom.i("load progress", "off");
        }

        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.batchUpdate(getContext(), jobOrderObject.toString()
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            if (getActivity() == null) {

                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        btnAccept.setEnabled(true);

                                        if ((pdProcess != null) && pdProcess.isShowing()) {
                                            pdProcess.dismiss();
                                        }

                                        try {
                                            final JSONObject data = new JSONObject(body);
                                            JSONArray result = data.getJSONArray("result");
                                            if (data.has("status") && !(data.isNull("status"))) {
                                                if (data.getBoolean("status")) {

                                                    //  menuSearch.collapseActionView();
                                                    showSuccessDialog(getResources().getString(R.string.assignedToacknowledgedTitle), getResources().getString(R.string.assignedToacknowledgedMessage)).show();
                                                }
//                                                else {
//
//
//                                                    if (dbUpdateJobs.getAllJobs().size() != 0) {
//
//                                                        for (int i = 0; i < dbUpdateJobs.getAllJobs().size(); i++) {
//                                                            final OrderDatabase orderDatabase = dbUpdateJobs.getAllJobs().get(i);
//                                                            Log.i("", "job adapter service order database:" + orderDatabase.getOrderJson());
//
//                                                            if (orderDatabase.getOrderId().equalsIgnoreCase(jobOrderIdArray.toString())) {
//                                                                dbUpdateJobs.updateJob(jobOrderIdArray, jobOrderObject, orderDatabase.getDatabaseId());
//                                                            }
//
//                                                        }
//                                                    } else {
//                                                        dbUpdateJobs.addOrder(jobOrderIdArray, jobOrderObject);
//                                                    }
//                                                }
                                            }

                                        } catch (JSONException e) {
                                            LogCustom.i(body, "result");
                                        }
                                    }
                                });
                            }


                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            if (getActivity() == null) {

                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        btnAccept.setEnabled(true);

                                        if ((pdProcess != null) && pdProcess.isShowing()) {
                                            pdProcess.dismiss();
                                        }
                                        errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
                                    }
                                });
                            }


                        }
                    });
        } catch (Exception e) {
            if ((pdProcess != null) && pdProcess.isShowing()) {
                pdProcess.dismiss();
            }
            btnAccept.setEnabled(true);
            e.printStackTrace();
        }


    }

    @Override
    public void onPause() {

        super.onPause();

        try {
            LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mReceiver);
        } catch (final Throwable e) {
            LogCustom.e(e);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.job_list_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
        actionBarMenu = menu;

        menuSearch = menu.findItem(R.id.menu_search);

        RelativeLayout searchLayout = (RelativeLayout) MenuItemCompat.getActionView(menuSearch);

        editsearch = searchLayout.findViewById(R.id.searchEditText);

        editsearch.addTextChangedListener(new TextWatcher() {
            private final long DELAY = 1000; // milliseconds
            private Timer timer = new Timer();

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                Log.i("edittext", "edit text after text change");
                timer.cancel();
                timer = new Timer();
                timer.schedule(
                        new TimerTask() {
                            @Override
                            public void run() {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (editsearch.getText().length() > 0) {
                                            search();

                                        }
                                    }
                                });
                            }
                        },
                        DELAY
                );
            }
        });

        searchLayout.findViewById(R.id.mImageViewSearch).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                search();
            }

        });

        // Capture Text in EditText
        //		editsearch.addTextChangedListener( textWatcher );
        editsearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return actionId == EditorInfo.IME_ACTION_SEARCH && search();
            }

        });


        final MenuItem menuDriver = menu.findItem(R.id.menu_driver);

        // Show the search menu item in menu.xml
        menuDriver.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                String userName = settings.getString("user_name", "");

                AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                builder2.setTitle("Driver");
                builder2.setMessage(userName);
                builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //do nothing
                    }
                });
                AlertDialog dialog2 = builder2.create();
                dialog2.show();
                return false;
            }
        });

        menuDriver.setVisible(false);


        final MenuItem menuCal = menu.findItem(R.id.menu_calendar);

        // Show the search menu item in menu.xml
        menuCal.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if (calendarOpen == false) {
                    if (firstCalendarTap) {
                        viewHeight = bottomLayout.getHeight();
                    }

                    resizeHeight = bottomLayout.getHeight() - calendarLayout.getHeight();

                    Log.i("", "bottom layout height:" + bottomLayout.getHeight() + "calendar layout height:" + calendarLayout.getHeight());
                    ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(),
                            bottomLayout.getWidth(), resizeHeight);
                    resizeAnimation.setDuration(600);
                    bottomLayout.startAnimation(resizeAnimation);
                } else {
                    Log.i("", "bottom layout height:" + bottomLayout.getHeight() + "calendar layout height:" + calendarLayout.getHeight());
                    ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(),
                            bottomLayout.getWidth(), viewHeight);
                    resizeAnimation.setDuration(600);
                    bottomLayout.startAnimation(resizeAnimation);

                }

                calendarOpen = !calendarOpen;
                Log.i("SelectedTabsIs", "" + selectedTab);
                updateHideShowBottomButton(selectedTab);

                return false;
            }
        });

        final MenuItem menuRefresh = menu.findItem(R.id.menu_refresh);

        // Show the search menu item in menu.xml
        mListenerRefresh = new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                GeneralActivity generalActivity = new GeneralActivity();
                if (!generalActivity.haveInternetConnected(getContext())) {
                    Log.i("internetConnection", "no");


                    wantProceedIfNoInternet(2);


                } else {
                    //checkUnsendData();
                    checkHaveLatestJobUpdate();
                }


                return false;

            }
        };

        menuRefresh.setOnMenuItemClickListener(mListenerRefresh);


        // Show the search menu item in menu.xml
        MenuItemCompat.setOnActionExpandListener(menuSearch, new MenuItemCompat.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                Log.i("SEARCHING TAP TAP", " ");
                // Focus on EditText
                menuCal.setVisible(false);

                menuRefresh.setVisible(false);


//                for(int i=0; i<jobAssignedFragment.size(); i++){
//                    jobAssignedFragment.get(i).dateLabelVisibility(false);
//                }

                searchResultListView.setVisibility(View.VISIBLE);
                bottomLayout.setVisibility(View.GONE);
                calendarLayout.setVisibility(View.GONE);

                //Update UI hide layout all button when searching
                layoutAllBtn.setVisibility(View.GONE);


                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Empty EditText to remove text filtering
                // sch.shutdown();
                layoutAllBtn.setVisibility(View.VISIBLE);

                menuRefresh.setVisible(true);

                editsearch.setText("");

                menuCal.setVisible(true);

                searchResultListView.setVisibility(View.GONE);
                bottomLayout.setVisibility(View.VISIBLE);
                calendarLayout.setVisibility(View.VISIBLE);

                for (int i = 0; i < jobAssignedFragment.size(); i++) {
                    jobAssignedFragment.get(i).dateLabelVisibility(true);
                }


                JSONObject jsonObject = new JSONObject();
                SharedPreferences settings;
                //settings = mActivity.getSharedPreferences(PREFS_NAME, 0);

                settings = PreferenceManager.getDefaultSharedPreferences(mActivity);

                String accessToken = settings.getString("access_token", "");


                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String currentDateString = sdf.format(selectedDate);

                Log.i("", "refresh job 44");

                try {
                    GeneralActivity generalActivity = new GeneralActivity();
                    if (!generalActivity.haveInternetConnected(getContext())) {
                        Log.i("internetConnection", "no");
                        if (!showErrorInternetDialog().isShowing()) {
                            showErrorInternetDialog().show();
                        }
                    } else {
                        fetchJobList(currentDateString, "");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editsearch.getWindowToken(), 0);

                return true;
            }
        });

    }

    public void updateHideShowBottomButton(int selectedTab) {
        if (selectedTab == 0) {

            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.addRule(RelativeLayout.ABOVE, R.id.bottomLayout);
            layoutPager.setLayoutParams(lp);

            layoutAllBtn.setVisibility(View.VISIBLE);
            view5.setVisibility(View.GONE);
            view6.setVisibility(View.GONE);
            view7.setVisibility(View.GONE);


            btnComplete.setVisibility(View.GONE);
            btnCancel.setVisibility(View.GONE);
            btnArrived.setVisibility(View.GONE);

            if (assignedJobList.size() == 0) {
                btnAccept.setVisibility(View.GONE);
                layoutAllBtn.setVisibility(View.GONE);
            } else {
                btnAccept.setVisibility(View.VISIBLE);
            }

            jobAssignedFragment.get(selectedTab).reloadJobList(assignedJobList, true, endOfList, selectedDate);

        } else if (selectedTab == 1) {
            layoutAllBtn.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            jobAssignedFragment.get(selectedTab).reloadJobList(acknowledgedJobList, true, endOfList, selectedDate);

        } else if (selectedTab == 2) {
            layoutAllBtn.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            jobAssignedFragment.get(selectedTab).reloadJobList(inProgressJobList, true, endOfList, selectedDate);

        } else if (selectedTab == 3) {
            layoutAllBtn.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            jobAssignedFragment.get(selectedTab).reloadJobList(completedJobList, true, endOfList, selectedDate);

        } else if (selectedTab == 4) {
            layoutAllBtn.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            jobAssignedFragment.get(selectedTab).reloadJobList(cancelledJobList, true, endOfList, selectedDate);

        } else if (selectedTab == 5) {
            layoutAllBtn.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            jobAssignedFragment.get(selectedTab).reloadJobList(failedJobList, true, endOfList, selectedDate);

        } else if (selectedTab == 6) {
            layoutAllBtn.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) layoutPager.getLayoutParams();
            lp.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            jobAssignedFragment.get(selectedTab).changeDate(selectedDate);
            jobAssignedFragment.get(selectedTab).reloadJobList(selfCollectJobList, true, endOfList, selectedDate);

        }

    }

    public void startSearching(String searchString) {

        try {

            searchString = URLEncoder.encode(searchString, "UTF-8");
            JSONObject jsonObject = new JSONObject();
            SharedPreferences settings;
            // settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
            settings = PreferenceManager.getDefaultSharedPreferences(mActivity);

            String accessToken = settings.getString("access_token", "");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateString = sdf.format(selectedDate);

            try {
                GeneralActivity generalActivity = new GeneralActivity();
                if (!generalActivity.haveInternetConnected(getContext())) {
                    LogCustom.i("internetConnection search", "no");
                    if (!showErrorInternetDialog().isShowing()) {
                        showErrorInternetDialog().show();
                    }
                } else {
                    fetchJobList(currentDateString, searchString);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception ignored) {
        }

    }


    @Override
    public void onDateSelected(Date date) {

        Log.i("", "ondateselected:" + date);
        getAllMonth = false;
        selectedDate = date;
        SimpleDateFormat format1 = new SimpleDateFormat("dd MMMM yyyy");
        if (selectedDate != null && dateTextView != null) {
            dateTextView.setText(format1.format(selectedDate));
        }

        calendarLayout.markDayAsSelectedDay(date);
        String intMonthString = (String) android.text.format.DateFormat.format("MM", date); //06
        String yearString = (String) android.text.format.DateFormat.format("yyyy", date); //2013
        String dayString = (String) android.text.format.DateFormat.format("dd", date); //20
        year = Integer.valueOf(yearString);
        month = Integer.valueOf(intMonthString) - 1;
        day = Integer.valueOf(dayString);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateString = sdf.format(selectedDate);

        ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(), bottomLayout.getWidth(),
                viewHeight);
        resizeAnimation.setDuration(600);
        bottomLayout.startAnimation(resizeAnimation);
        calendarOpen = false;


        Log.i("", "on resume");
        // Timber.d( "get jobs api call 11");
        mult = 0;
        needReload = true;


        GeneralActivity generalActivity = new GeneralActivity();
        if (!generalActivity.haveInternetConnected(getContext())) {
            Log.i("internetConnection", "no");
            wantProceedIfNoInternet(2);
        } else {
            //checkUnsendData();
            //fetchJobList(currentDateString,"");
            checkHaveLatestJobUpdate();
        }


    }

    @Override
    public void onRightButtonClick() {
        currentMonthIndex++;
        selecedCalendarMonth = selecedCalendarMonth + 1;

        getAllMonth = true;

        this.mult = 0;

        updateCalendar();

        calendarLayout.post(new Runnable() {

            public void run() {
                resizeHeight = viewHeight - calendarLayout.getHeight();

                Log.i("", "resize height:" + resizeHeight + "bottom layout height:" + bottomLayout.getHeight() + "calendar layout:"
                        + calendarLayout.getHeight());

                if (bottomLayout.getHeight() < resizeHeight) {
                    Log.i("", "bottom layout height:" + bottomLayout.getHeight() + "calendar layout height:" + calendarLayout.getHeight());
                    ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(),
                            bottomLayout.getWidth(),
                            bottomLayout.getHeight() + (resizeHeight - bottomLayout.getHeight()));
                    resizeAnimation.setDuration(600);
                    bottomLayout.startAnimation(resizeAnimation);
                } else {
                    Log.i("", "bottom layout height:" + bottomLayout.getHeight() + "calendar layout height:" + calendarLayout.getHeight());
                    ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(),
                            bottomLayout.getWidth(),
                            bottomLayout.getHeight() - (bottomLayout.getHeight() - resizeHeight));
                    resizeAnimation.setDuration(600);
                    bottomLayout.startAnimation(resizeAnimation);
                }
            }
        });

    }

    @Override
    public void onLeftButtonClick() {

        currentMonthIndex--;
        selecedCalendarMonth = selecedCalendarMonth - 1;
        updateCalendar();

        getAllMonth = true;
        this.mult = 0;

        calendarLayout.post(new Runnable() {

            public void run() {
                resizeHeight = viewHeight - calendarLayout.getHeight();

                Log.i("", "resize height:" + resizeHeight + "bottom layout height:" + bottomLayout.getHeight() + "calendar layout:"
                        + calendarLayout.getHeight());

                if (bottomLayout.getHeight() < resizeHeight) {
                    Log.i("", "bottom layout height:" + bottomLayout.getHeight() + "calendar layout height:" + calendarLayout.getHeight());
                    ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(),
                            bottomLayout.getWidth(),
                            bottomLayout.getHeight() + (resizeHeight - bottomLayout.getHeight()));
                    resizeAnimation.setDuration(600);
                    bottomLayout.startAnimation(resizeAnimation);
                } else {
                    Log.i("", "bottom layout height:" + bottomLayout.getHeight() + "calendar layout height:" + calendarLayout.getHeight());
                    ResizeAnimation resizeAnimation = new ResizeAnimation(bottomLayout, bottomLayout.getWidth(), bottomLayout.getHeight(),
                            bottomLayout.getWidth(),
                            bottomLayout.getHeight() - (bottomLayout.getHeight() - resizeHeight));
                    resizeAnimation.setDuration(600);
                    bottomLayout.startAnimation(resizeAnimation);
                }
            }
        });

    }


    public void alertJobLatestSync(final JobOrder jobOrder, final int totalJobLatestNeedOpenAlert) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.layout_job_update_sync, null);
        final CheckBox neverShowMessage = alertLayout.findViewById(R.id.neverShowMessage);
        final TextView titleJobAlert = alertLayout.findViewById(R.id.titleJobAlert);

        titleJobAlert.setText("Update " + jobOrder.getReference_no() + "?");

        neverShowMessage.setText("Apply this for the next " + totalJobLatestNeedOpenAlert + " jobs");

        doForAllJobCheckBox = false;

        if (totalJobLatestNeedOpenAlert == 1) {
            neverShowMessage.setVisibility(View.GONE);
        }

        neverShowMessage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                doForAllJobCheckBox = isChecked;
            }
        });

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setView(alertLayout);
        alertDialog.setCancelable(false);
        alertDialog.setNegativeButton("UPLOAD CURRENT PROGRESS", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (doForAllJobCheckBox) {

                    List<Integer> arrayOrderId = new ArrayList<>();

                    for (int i = 0; i < dbUpdateJobs.getListJobLatest().size(); i++) {
                        JobOrder jobOrders = new JobOrder();
                        jobOrders = dbUpdateJobs.getListJobLatest().get(i);
                        arrayOrderId.add(jobOrder.getOrderId());
                        dbUpdateJobs.deleteJobLatestArray(arrayOrderId);
                    }

                    LogCustom.i("totalJobLatestNeedOpenAlert1111", ": " + (totalJobLatestNeedOpenAlert - 1));
                    checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1, true);
                } else {
                    List<Integer> arrayOrderId = new ArrayList<>();
                    arrayOrderId.add(jobOrder.getOrderId());
                    dbUpdateJobs.deleteJobLatestArray(arrayOrderId);

                    checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1, false);
                }
            }
        });

        alertDialog.setPositiveButton("UPDATE JOB", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();

                if (doForAllJobCheckBox) {
                    // delete all job

                    // tak update yg dlm db

                    List<Integer> arrayOrderId = new ArrayList<>();

                    for (int i = 0; i < dbUpdateJobs.getListJobLatest().size(); i++) {
                        JobOrder jobOrders = new JobOrder();
                        jobOrders = dbUpdateJobs.getListJobLatest().get(i);
                        dbUpdateJobs.deleteJobOrderBasedOnOrderID(jobOrders.getOrderId());
                        arrayOrderId.add(jobOrders.getOrderId());
                    }

                    dbUpdateJobs.deleteJobLatestArray(arrayOrderId);

                    LogCustom.i("totalJobLatestNeedOpenAlert11", ": " + (totalJobLatestNeedOpenAlert - 1));
                    checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1, true);
                } else {


                    // delete job
                    dbUpdateJobs.deleteJobOrderBasedOnOrderID(jobOrder.getOrderId());

                    List<Integer> arrayOrderId = new ArrayList<>();
                    arrayOrderId.add(jobOrder.getOrderId());
                    dbUpdateJobs.deleteJobLatestArray(arrayOrderId);

                    checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1, false);
                }
            }
        });

        alert = alertDialog.create();

        if (!alert.isShowing()) {
            alert.show();
        }
    }


//    private void alertJobLatestSync(final JobOrder jobOrder, final int totalJobLatestNeedOpenAlert){
//
//
//        LogCustom.i("totalJobLatestNeedOpenAlert11222",""+totalJobLatestNeedOpenAlert);
//
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
//        alertDialogBuilder.setTitle("Synchronise " + jobOrder.getReference_no()).setMessage("There were some changes made when you were away from the internet. " +
//                "By synchonising this job, any changes you have done may get replaced. Do you want to sync right now?")
//                .setCancelable(false)
//                .setPositiveButton("Sync Job",
//                        new DialogInterface.OnClickListener(){
//                            public void onClick(DialogInterface dialog, int id){
//                                dialog.dismiss();
//                                dialog.cancel();
//
//                                // delete job
//                                dbUpdateJobs.deleteJobOrderBasedOnOrderID(jobOrder.getOrderId());
//
//                                List<Integer> arrayOrderId = new ArrayList<>();
//                                arrayOrderId.add(jobOrder.getOrderId());
//                                dbUpdateJobs.deleteJobLatestArray(arrayOrderId);
//
//                                checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1,false);
//                            }
//                        });
//        alertDialogBuilder.setNegativeButton("Skip",
//                new DialogInterface.OnClickListener(){
//                    public void onClick(DialogInterface dialog, int id){
//                        dialog.dismiss();
//                        dialog.cancel();
//
//                        List<Integer> arrayOrderId = new ArrayList<>();
//                        arrayOrderId.add(jobOrder.getOrderId());
//                        dbUpdateJobs.deleteJobLatestArray(arrayOrderId);
//
//                        checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1,false);
//
//                    }
//                });
//
//        if(totalJobLatestNeedOpenAlert <= 1){
////            alertDialogBuilder.setNegativeButton("Skip All",
////                    new DialogInterface.OnClickListener(){
////                        public void onClick(DialogInterface dialog, int id){
////                            dialog.dismiss();
////                            dialog.cancel();
////
////
////                            JobOrder jobOrders = new JobOrder();
////                            List<Integer> arrayOrderId = new ArrayList<>();
////
////                            for(int i = 0; i < dbUpdateJobs.getListJobLatest().size(); i ++){
////                                jobOrders = dbUpdateJobs.getListJobLatest().get(i);
////                                arrayOrderId.add(jobOrder.getOrderId());
////                                dbUpdateJobs.deleteJobLatestArray(arrayOrderId);
////                            }
////
////                            LogCustom.i("totalJobLatestNeedOpenAlert1111",": "+(totalJobLatestNeedOpenAlert-1));
////                            checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1,true);
////
////                        }
////                    });
//
//
//            alertDialogBuilder.setNeutralButton("Sync all " + totalJobLatestNeedOpenAlert +" jobs",
//                    new DialogInterface.OnClickListener(){
//                        public void onClick(DialogInterface dialog, int id){
//                            dialog.dismiss();
//                            dialog.cancel();
//
//                            // delete all job
//                            JobOrder jobOrders = new JobOrder();
//
//                            List<Integer> arrayOrderId = new ArrayList<>();
//
//                            for(int i = 0; i < dbUpdateJobs.getListJobLatest().size(); i ++){
//                                jobOrders = dbUpdateJobs.getListJobLatest().get(i);
//                                dbUpdateJobs.deleteJobOrderBasedOnOrderID(jobOrders.getOrderId());
//                                arrayOrderId.add(jobOrders.getOrderId());
//                            }
//
//                            dbUpdateJobs.deleteJobLatestArray(arrayOrderId);
//
//                            LogCustom.i("totalJobLatestNeedOpenAlert11",": "+(totalJobLatestNeedOpenAlert-1));
//                            checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert - 1,true);
//                        }
//                    });
//        }
//
//
//        alert = alertDialogBuilder.create();
//
//        if(!alert.isShowing()){
//            alert.show();
//        }
//
//    }


    public void checkEitherNeedOpenDialog(int totalJobLatestNeedOpenAlert, boolean syncAll) {

        LogCustom.i("totalJobLatestNeedOpenAlert122222", ": " + totalJobLatestNeedOpenAlert);

        LogCustom.i("jobListUpdated111", ":teste" + dbUpdateJobs.getListJobDetailUpdated().size());

        if (alert != null) {
            alert.dismiss();
        }

        if (totalJobLatestNeedOpenAlert > 0) {
            if (!syncAll) {
                //open dialog
                JobOrder jobOrder = new JobOrder();
                jobOrder = dbUpdateJobs.getListJobLatest().get(totalJobLatestNeedOpenAlert - 1);
                alertJobLatestSync(jobOrder, totalJobLatestNeedOpenAlert);
            } else {
                // stop open dialog
                checkUnsendData();
            }
        } else {
            // stop open dialog
            checkUnsendData();
        }

    }

    public void checkHaveLatestJobUpdate() {
        int totalJobLatestNeedOpenAlert = dbUpdateJobs.getListJobLatest().size();

        if (totalJobLatestNeedOpenAlert > 0) {
            checkEitherNeedOpenDialog(totalJobLatestNeedOpenAlert, false);
        } else {
            checkUnsendData();
        }
    }


    public void checkUnsendData() {


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateString = sdf.format(selectedDate);

        // pdProcess.show();

        LogCustom.i("jobListUpdated", ":checkUnsendData" + dbUpdateJobs.getListJobDetailUpdated().size());


        if (dbUpdateJobs.getListJobDetailUpdated().size() > 0) {

            JSONObject rootObject = new JSONObject();
            JSONArray childArray = new JSONArray();

            List<Integer> allOrderIdNeedToDelete = new ArrayList<>();


            pdProcess.show();


            for (int i = 0; i < dbUpdateJobs.getListJobDetailUpdated().size(); i++) {

                JSONArray idArray = new JSONArray();
                JSONObject childObject = new JSONObject();
                JSONArray jobStepDetailArray = new JSONArray();

                JobOrder jobOrder = dbUpdateJobs.getListJobDetailUpdated().get(i);

                if (jobOrder.getOrderStatusId() == failedOrderStatusIDFromDB || jobOrder.getOrderStatusId() == completedOrderStatusIDFromDB) {
                    idArray.put(String.valueOf(jobOrder.getOrderId()));
                    allOrderIdNeedToDelete.add(jobOrder.getOrderId());

                    List<JobSteps> jobStepsList = new ArrayList<JobSteps>();
                    jobStepsList = dbUpdateJobs.getListJobStepsDetailUpdated(jobOrder.getOrderId());

                    LogCustom.i("jobStepsList failed completed", ":" + jobStepsList.size());

                    if (jobStepsList.size() > 0) {

                        for (int ii = 0; ii < jobStepsList.size(); ii++) {
                            JobSteps jobSteps = new JobSteps();
                            JSONObject jobStepDetailsObject = new JSONObject();
                            JSONArray orderAttemptArray = new JSONArray();
                            jobSteps = jobStepsList.get(ii);


                            List<OrderAttempt> orderAttemptList = new ArrayList<OrderAttempt>();
                            orderAttemptList = dbUpdateJobs.getListOrderAttemptUpdated(jobSteps.getId());

                            LogCustom.i("orderAttemptList", "" + orderAttemptList.size());

                            if (orderAttemptList.size() > 0) {
                                for (int iii = 0; iii < orderAttemptList.size(); iii++) {
                                    JSONObject attemptObject = new JSONObject();
                                    JSONArray orderAttemptImageArray = new JSONArray();

                                    OrderAttempt orderAttempt = new OrderAttempt();
                                    orderAttempt = orderAttemptList.get(iii);

                                    LogCustom.i("orderAttempt IDD", "" + orderAttempt.getId());

                                    List<OrderAttemptImage> orderAttemptImageList = new ArrayList<OrderAttemptImage>();
                                    orderAttemptImageList = dbUpdateJobs.getListImageUpdated(orderAttempt.getId());

                                    LogCustom.i("orderAttemptImageList", "" + orderAttemptImageList.size());

                                    if (orderAttemptImageList.size() > 0) {
                                        for (int j = 0; j < orderAttemptImageList.size(); j++) {
                                            OrderAttemptImage orderAttemptImage = new OrderAttemptImage();
                                            orderAttemptImage = orderAttemptImageList.get(j);


                                            try {
                                                JSONObject orderAttemtImageDetailObject = new JSONObject();

                                                final String[] body = new String[1];
                                                body[0] = "data:image/png;base64," + orderAttemptImage.getBase64();

                                                orderAttemtImageDetailObject.put("raw", body[0]);
                                                orderAttemtImageDetailObject.put("is_signature", orderAttemptImage.isSignature());
                                                orderAttemtImageDetailObject.put("description", orderAttemptImage.getNote());
                                                orderAttemtImageDetailObject.put("remove", orderAttemptImage.isRemove());

                                                orderAttemptImageArray.put(orderAttemtImageDetailObject);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        try {
                                            attemptObject.put("order_attempt_image_base64", orderAttemptImageArray);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                    try {

                                        attemptObject.put("received_by", orderAttempt.getReceived_by());
                                        attemptObject.put("reason", orderAttempt.getReason());
                                        attemptObject.put("note", orderAttempt.getNote());
                                        attemptObject.put("submitted_time", orderAttempt.getSubmitted_time());
                                        attemptObject.put("latitude", orderAttempt.getLatitude());
                                        attemptObject.put("longitude", orderAttempt.getLongitude());

                                        orderAttemptArray.put(attemptObject);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }


                            try {
                                jobStepDetailsObject.put("job_step_id", jobSteps.getId());
                                jobStepDetailsObject.put("order_sequence", jobSteps.getOrder_sequence());
                                jobStepDetailsObject.put("job_step_status_id", jobSteps.getJob_step_status_id());

                                jobStepDetailsObject.put("step_attempts", orderAttemptArray);

                                jobStepDetailArray.put(jobStepDetailsObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }

                    try {
                        childObject.put("id", idArray);
                        childObject.put("drop_off_time", jobOrder.getDropOffTimeStart());
                        childObject.put("drop_off_time_end", jobOrder.getDropOffTimeEnd());
                        childObject.put("order_status_id", ackowledgedOrderStatusIDFromDB);

                        childObject.put("job_steps", jobStepDetailArray);
                        childArray.put(childObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (jobOrder.getOrderStatusId() == inprogressOrderStatusIDFromDB) {
                    idArray.put(String.valueOf(jobOrder.getOrderId()));
                    allOrderIdNeedToDelete.add(jobOrder.getOrderId());

                    List<JobSteps> jobStepsList = new ArrayList<JobSteps>();
                    jobStepsList = dbUpdateJobs.getListJobStepsDetailUpdated(jobOrder.getOrderId());

                    LogCustom.i("jobStepsList failed completed", ":" + jobStepsList.size());

                    if (jobStepsList.size() > 0) {

                        for (int ii = 0; ii < jobStepsList.size(); ii++) {
                            JobSteps jobSteps = new JobSteps();
                            JSONObject jobStepDetailsObject = new JSONObject();
                            JSONArray orderAttemptArray = new JSONArray();
                            jobSteps = jobStepsList.get(ii);


                            List<OrderAttempt> orderAttemptList = new ArrayList<OrderAttempt>();
                            orderAttemptList = dbUpdateJobs.getListOrderAttemptUpdated(jobSteps.getId());

                            LogCustom.i("orderAttemptList", "" + orderAttemptList.size());

                            if (orderAttemptList.size() > 0) {
                                for (int iii = 0; iii < orderAttemptList.size(); iii++) {
                                    JSONObject attemptObject = new JSONObject();
                                    JSONArray orderAttemptImageArray = new JSONArray();

                                    OrderAttempt orderAttempt = new OrderAttempt();
                                    orderAttempt = orderAttemptList.get(iii);

                                    LogCustom.i("orderAttempt IDD", "" + orderAttempt.getId());

                                    List<OrderAttemptImage> orderAttemptImageList = new ArrayList<OrderAttemptImage>();
                                    orderAttemptImageList = dbUpdateJobs.getListImageUpdated(orderAttempt.getId());

                                    LogCustom.i("orderAttemptImageList", "" + orderAttemptImageList.size());

                                    if (orderAttemptImageList.size() > 0) {
                                        for (int j = 0; j < orderAttemptImageList.size(); j++) {
                                            OrderAttemptImage orderAttemptImage = new OrderAttemptImage();
                                            orderAttemptImage = orderAttemptImageList.get(j);


                                            try {
                                                JSONObject orderAttemtImageDetailObject = new JSONObject();

                                                final String[] body = new String[1];
                                                body[0] = "data:image/png;base64," + orderAttemptImage.getBase64();

                                                orderAttemtImageDetailObject.put("raw", body[0]);

                                                orderAttemtImageDetailObject.put("raw", orderAttemptImage.getBase64());
                                                orderAttemtImageDetailObject.put("is_signature", orderAttemptImage.isSignature());
                                                orderAttemtImageDetailObject.put("description", orderAttemptImage.getNote());
                                                orderAttemtImageDetailObject.put("remove", orderAttemptImage.isRemove());

                                                orderAttemptImageArray.put(orderAttemtImageDetailObject);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        try {
                                            attemptObject.put("order_attempt_image_base64", orderAttemptImageArray);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                    try {

                                        attemptObject.put("received_by", orderAttempt.getReceived_by());
                                        attemptObject.put("reason", orderAttempt.getReason());
                                        attemptObject.put("note", orderAttempt.getNote());
                                        attemptObject.put("submitted_time", orderAttempt.getSubmitted_time());
                                        attemptObject.put("latitude", orderAttempt.getLatitude());
                                        attemptObject.put("longitude", orderAttempt.getLongitude());

                                        orderAttemptArray.put(attemptObject);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }


                            try {
                                jobStepDetailsObject.put("job_step_id", jobSteps.getId());
                                jobStepDetailsObject.put("order_sequence", jobSteps.getOrder_sequence());
                                jobStepDetailsObject.put("job_step_status_id", jobSteps.getJob_step_status_id());

                                jobStepDetailsObject.put("step_attempts", orderAttemptArray);

                                jobStepDetailArray.put(jobStepDetailsObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }

                    try {

                        if (jobOrder.isReattemptJob()) {
                            childObject.put("reattempt", true);
                            childObject.put("reattempt_job_step_id", jobOrder.getReattemptJobStepId());
                        }


                        childObject.put("id", idArray);
                        childObject.put("drop_off_time", jobOrder.getDropOffTimeStart());
                        childObject.put("order_status_id", ackowledgedOrderStatusIDFromDB);

                        childObject.put("job_steps", jobStepDetailArray);
                        childArray.put(childObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (jobOrder.getOrderStatusId() != inprogressOrderStatusIDFromDB || jobOrder.getOrderStatusId() != failedOrderStatusIDFromDB || jobOrder.getOrderStatusId() != completedOrderStatusIDFromDB) {
                    idArray.put(String.valueOf(jobOrder.getOrderId()));
                    allOrderIdNeedToDelete.add(jobOrder.getOrderId());

                    List<JobSteps> jobStepsList = new ArrayList<JobSteps>();
                    jobStepsList = dbUpdateJobs.getListJobStepsDetailUpdated(jobOrder.getOrderId());

                    LogCustom.i("jobStepsList", ":" + jobStepsList.size());

                    if (jobStepsList.size() > 0) {

                        for (int ii = 0; ii < jobStepsList.size(); ii++) {
                            JobSteps jobSteps = new JobSteps();
                            JSONObject jobStepDetailsObject = new JSONObject();
                            JSONArray orderAttemptArray = new JSONArray();
                            jobSteps = jobStepsList.get(ii);


                            List<OrderAttempt> orderAttemptList = new ArrayList<OrderAttempt>();
                            orderAttemptList = dbUpdateJobs.getListOrderAttemptUpdated(jobSteps.getId());

                            if (orderAttemptList.size() > 0) {
                                for (int iii = 0; iii < orderAttemptList.size(); iii++) {
                                    JSONObject attemptObject = new JSONObject();
                                    JSONArray orderAttemptImageArray = new JSONArray();

                                    OrderAttempt orderAttempt = new OrderAttempt();
                                    orderAttempt = orderAttemptList.get(iii);

                                    List<OrderAttemptImage> orderAttemptImageList = new ArrayList<OrderAttemptImage>();
                                    orderAttemptImageList = dbUpdateJobs.getListImageUpdated(orderAttempt.getId());

                                    if (orderAttemptImageList.size() > 0) {
                                        for (int j = 0; j < orderAttemptImageList.size(); j++) {
                                            OrderAttemptImage orderAttemptImage = new OrderAttemptImage();
                                            orderAttemptImage = orderAttemptImageList.get(j);

                                            try {

                                                JSONObject orderAttemtImageDetailObject = new JSONObject();

                                                final String[] body = new String[1];
                                                body[0] = "data:image/png;base64," + orderAttemptImage.getBase64();

                                                orderAttemtImageDetailObject.put("raw", body[0]);
                                                orderAttemtImageDetailObject.put("is_signature", orderAttemptImage.isSignature());
                                                orderAttemtImageDetailObject.put("description", orderAttemptImage.getNote());
                                                orderAttemtImageDetailObject.put("remove", orderAttemptImage.isRemove());

                                                orderAttemptImageArray.put(orderAttemtImageDetailObject);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }

                                        try {
                                            attemptObject.put("order_attempt_image_base64", orderAttemptImageArray);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }


                                    try {

                                        attemptObject.put("received_by", orderAttempt.getReceived_by());
                                        attemptObject.put("reason", orderAttempt.getReason());
                                        attemptObject.put("note", orderAttempt.getNote());
                                        attemptObject.put("submitted_time", orderAttempt.getSubmitted_time());
                                        attemptObject.put("latitude", orderAttempt.getLatitude());
                                        attemptObject.put("longitude", orderAttempt.getLongitude());

                                        orderAttemptArray.put(attemptObject);


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }


                            try {
                                jobStepDetailsObject.put("job_step_id", jobSteps.getId());
                                jobStepDetailsObject.put("order_sequence", jobSteps.getOrder_sequence());
                                jobStepDetailsObject.put("job_step_status_id", jobSteps.getJob_step_status_id());

                                jobStepDetailsObject.put("step_attempts", orderAttemptArray);

                                jobStepDetailArray.put(jobStepDetailsObject);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }

                    try {
                        childObject.put("id", idArray);
                        childObject.put("order_status_id", ackowledgedOrderStatusIDFromDB);

                        childObject.put("job_steps", jobStepDetailArray);
                        childArray.put(childObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }


            }


            try {


                rootObject.put("data", childArray);
                LogCustom.i("JsonObjectStructure", rootObject.toString(4));

                try {
                    batchUpdateDatabaseJob(currentDateString, rootObject, allOrderIdNeedToDelete);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        } else {
            try {
                GeneralActivity generalActivity = new GeneralActivity();
                if (!generalActivity.haveInternetConnected(getContext())) {
                    Log.i("internetConnection", "no");
                    wantProceedIfNoInternet(2);
                } else {
                    fetchJobList(currentDateString, "");
                }


            } catch (IOException e) {
                e.printStackTrace();
            }


        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        super.onDestroy();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mReceiverRefreshJobListing);
    }

    void batchUpdateDatabaseJob(final String currentDateString, final JSONObject jsonObject, final List<Integer> allOrderIdNeedToDelete) throws IOException {

        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.batchUpdate(getContext(), jsonObject.toString()
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            if (getActivity() == null) {

                            } else {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        pdProcess.dismiss();

                                        if (body == null || body.isEmpty()) {

                                        } else {
                                            try {
                                                final JSONObject data = new JSONObject(body);

                                                if (data.has("status") && !(data.isNull("status"))) {
                                                    if (data.getBoolean("status")) {

                                                        dbUpdateJobs.deleteJobDetailsInsideAllTable(allOrderIdNeedToDelete);

                                                        try {
                                                            fetchJobList(currentDateString, "");
                                                        } catch (IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                } else {
                                                    searchResultJobList.clear();

                                                    jobAssignedFragment.get(0).reloadJobList(assignedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(1).reloadJobList(acknowledgedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(2).reloadJobList(inProgressJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(3).reloadJobList(completedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(4).reloadJobList(cancelledJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(5).reloadJobList(failedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(6).reloadJobList(selfCollectJobList, needReload, endOfList, selectedDate);


                                                }

                                            } catch (JSONException e) {
                                                searchResultJobList.clear();


                                                jobAssignedFragment.get(0).reloadJobList(assignedJobList, needReload, endOfList, selectedDate);
                                                jobAssignedFragment.get(1).reloadJobList(acknowledgedJobList, needReload, endOfList, selectedDate);
                                                jobAssignedFragment.get(2).reloadJobList(inProgressJobList, needReload, endOfList, selectedDate);
                                                jobAssignedFragment.get(3).reloadJobList(completedJobList, needReload, endOfList, selectedDate);
                                                jobAssignedFragment.get(4).reloadJobList(cancelledJobList, needReload, endOfList, selectedDate);
                                                jobAssignedFragment.get(5).reloadJobList(failedJobList, needReload, endOfList, selectedDate);
                                                jobAssignedFragment.get(6).reloadJobList(selfCollectJobList, needReload, endOfList, selectedDate);


                                                LogCustom.e(e);

                                                try {
//                                                    Crashlytics.log(responseCode, "batchUpdateDatabaseJob", body);
//                                                    Crashlytics.logException(e);
                                                } catch (final Throwable t) {
                                                }

                                            }
                                        }

                                    }
                                });
                            }


                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            final String jsonResponse = body;

                            if (getActivity() == null) {

                            } else {

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        pdProcess.dismiss();


                                        errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
                                    }
                                });
                            }


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {

        if (getActivity() == null) {

        } else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    unauthorizedNeedToLoginPage = false;

                    if (!requestCallBackError) {
                        try {
                            String errorMessage = "";
                            JSONObject data = null;
                            if (body != null || !body.equalsIgnoreCase("")) {

                                try {
                                    data = new JSONObject(body);
                                } catch (Exception e) {
                                    showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                                }

                                if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                    if (data.has("error") && !(data.isNull("error"))) {
                                        try {
                                            errorMessage = data.getString("error");

                                            if (errorMessage.equalsIgnoreCase("Couldn't find OrderStatus with 'id'=0")) {
                                                unauthorizedNeedToLoginPage = true;
                                            }

                                        } catch (Exception e) {
                                            JSONObject errorMessageArray = data.getJSONObject("error");
                                            errorMessage = errorMessageArray.getString("error");
                                        }
                                    }

                                    if (errorMessage.equalsIgnoreCase("")) {
                                        errorMessage = getResources().getString(R.string.pleaseContactAdmin);
                                    }

                                    showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


                                } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {

                                    unauthorizedNeedToLoginPage = true;

                                    if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                        if (data.getBoolean("device_not_found")) {
                                            showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                        }
                                    } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                        if (data.getBoolean("device_is_banned")) {
                                            showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                        }
                                    } else {
                                        showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                    }
                                } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {

                                    unauthorizedNeedToLoginPage = true;

                                    if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                        if (data.getBoolean("unpaid_subscription")) {
                                            showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                        }
                                    } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                        if (data.getBoolean("quota_reached")) {
                                            showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                        }
                                    } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                        if (data.getBoolean("blacklist")) {
                                            showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                        }
                                    } else {
                                        showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                    }
                                } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {

                                    if (data.has("error") && !(data.isNull("error"))) {
                                        try {
                                            errorMessage = data.getString("error");

                                            if (errorMessage.equalsIgnoreCase("Couldn't find OrderStatus with 'id'=0")) {
                                                unauthorizedNeedToLoginPage = true;
                                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                                            }

                                        } catch (Exception e) {
                                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                                        }
                                    } else {
                                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                                    }


                                } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                    showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                                } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                    showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                                } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                    showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                                }

                            } else {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                        } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                            if (!showErrorInternetDialog().isShowing()) {
                                showErrorInternetDialog().show();
                            }
                        } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                        } else {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                        }
                        // this for request call back error. Maybe because cannot connect server.
                    }
                }
            });

        }


    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */


    void fetchJobList(String currentDateString, String searchString) throws IOException {

        contextProgressDialog.setText(getResources().getString(R.string.loadingJobListing));

        if (pdProcess.isShowing() == false) {
            pdProcess.show();
            LogCustom.i("load progress", "in");
        } else {
            LogCustom.i("load progress", "off");
        }

        if (sweetAlertDialog != null) {
            LogCustom.i("load progress", "11");
            if (sweetAlertDialog.isShowing()) {
                LogCustom.i("load progress", "22");
                showErrorDialog("", "").dismiss();
                sweetAlertDialog.dismiss();
                showErrorDialog("", "").cancel();
                sweetAlertDialog.cancel();
            }
        }

        String startDate;
        if (searchString.equalsIgnoreCase("")) {
            if (editsearch != null) {
                if (editsearch.getText().toString().trim().equalsIgnoreCase("")) {
                    startDate = currentDateString;
                } else {
                    startDate = "2018-01-01";
                    searchString = editsearch.getText().toString().trim();
                    LogCustom.i("searchString11 progress111", ":" + editsearch.getText().toString().trim());
                }

            } else {
                startDate = currentDateString;
            }


        } else {
            startDate = "2018-01-01";
        }

        LogCustom.i("searchString11 progress", ":" + searchString);


        try {

            OkHttpRequest okHttpRequest = new OkHttpRequest();
            // String order_status = "1,2,6,9,5,14";
            String order_status = assignedOrderStatusIDFromDB + "," + ackowledgedOrderStatusIDFromDB + "," + inprogressOrderStatusIDFromDB + ","
                    + completedOrderStatusIDFromDB + "," + cancelledOrderStatusIDFromDB + "," + failedOrderStatusIDFromDB + "," + selfcollectOrderStatusIDFromDB;

            try {
                OkHttpRequest.fetchJobList(getContext(), startDate, currentDateString, order_status, searchString
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {

                                LogCustom.i("FetchJob", "success");

                                if (getActivity() == null) {

                                } else {

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if ((pdProcess != null) && pdProcess.isShowing()) {
                                                pdProcess.dismiss();
                                            }
                                            if (body == null || body.isEmpty()) {

                                            } else {
                                                searchButtonPressed = false;
                                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                                String currentDateString = sdf.format(selectedDate);

                                                isSelectAll = false;

                                                try {

                                                    JSONArray dataArray = new JSONArray(body);
                                                    SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                                                    allJobList.clear();

                                                    // status job list
                                                    assignedJobList.clear();
                                                    acknowledgedJobList.clear();
                                                    inProgressJobList.clear();
                                                    completedJobList.clear();
                                                    cancelledJobList.clear();
                                                    failedJobList.clear();
                                                    selfCollectJobList.clear();
                                                    searchResultJobList.clear();
                                                    addJobDetailsIntoDBNoInternet.clear();
                                                    addJobUpdateDetailsIntoDBNoInternet.clear();

                                                    //   mainStatusJobList.clear();

                                                    assignedJobListExtraWorker.clear();

                                                    // This function is want to delete all job details which is not updated
                                                    List<Integer> arrayOrderIDListNeedToDelete = new ArrayList<Integer>();
                                                    arrayOrderIDListNeedToDelete = dbUpdateJobs.getListJobDetailNOTUpdated();
                                                    LogCustom.i("arrayOrderIDListNeedToDelete", ": " + arrayOrderIDListNeedToDelete);
                                                    dbUpdateJobs.deleteJobDetailsInsideAllTable(arrayOrderIDListNeedToDelete);


                                                    for (int i = 0; i < jobAssignedFragment.size(); i++) {
                                                        jobAssignedFragment.get(i).clearJobList(selectedDate);
                                                    }

                                                    for (int j = 0; j < dataArray.length(); j++) {
                                                        JSONObject jsonObject2 = (JSONObject) dataArray.get(j);

                                                        int jobStatus = Integer.valueOf(jsonObject2.getString("order_status_id"));

                                                        LogCustom.i("jobStatus1111", "" + jobStatus);
                                                        LogCustom.i("assignedOrderStatusIDFromDB", "" + assignedOrderStatusIDFromDB);
                                                        LogCustom.i("inprogressOrderStatusIDFromDB", "" + inprogressOrderStatusIDFromDB);

                                                        if (jobStatus == assignedOrderStatusIDFromDB) {
                                                            JSONArray data = jsonObject2.getJSONArray("jobs");


                                                            Log.i("job response array", "job array:" + data.length());

                                                            for (int i = 0; i < data.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) data.get(i);

                                                                LogCustom.i("jobDateAssigned", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobAssigned", "is:" + jsonObject.getInt("total_job"));

                                                                JSONArray assignedJobArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < assignedJobArray.length(); m++) {
                                                                    JSONObject assignedJobJson = (JSONObject) assignedJobArray.get(m);
                                                                    JobOrder jobOrder = new JobOrder(assignedJobJson);
                                                                    LogCustom.i(jobOrder.getOrderId(), "order id");
                                                                    LogCustom.i(jobOrder.getOrderStatusId(), "order status");
                                                                    JobOrder dropOffJobOrder = jobOrder;

                                                                    if (editsearch != null && editsearch.getText().length() == 0) {
                                                                        try {
                                                                            allJobList.add(jobOrder);
                                                                            assignedJobList.add(dropOffJobOrder);
                                                                            LogCustom.i(jobOrder.getDropOffWorkerId(), "getDropOffWorkerIdAssigned");
                                                                            LogCustom.i(resource_owner_id, "resource_owner_idAssigned");

                                                                            if (dropOffJobOrder.getDropOffWorkerId() != resource_owner_id) {
                                                                                assignedJobListExtraWorker.add(dropOffJobOrder);
                                                                            }
                                                                        } catch (Exception e) {
                                                                            e.printStackTrace();
                                                                        }
                                                                    } else {
                                                                        assignedJobList.add(dropOffJobOrder);

                                                                        LogCustom.i(jobOrder.getDropOffWorkerId(), "getDropOffWorkerIdAssigned2");
                                                                        LogCustom.i(resource_owner_id, "resource_owner_idAssigned2");

                                                                        if (dropOffJobOrder.getDropOffWorkerId() != resource_owner_id) {
                                                                            assignedJobListExtraWorker.add(dropOffJobOrder);
                                                                        }
                                                                    }

                                                                    // add new job into database. Not updated Job
                                                                    if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                        jobOrder.setUpdateWithNoInternet(false);
                                                                        addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                    }

                                                                    searchResultJobList.add(dropOffJobOrder);

                                                                    //   mainStatusJobList.add(assignedJobList);
                                                                }
                                                            }
                                                            //  break;
                                                        } else if (jobStatus == ackowledgedOrderStatusIDFromDB) {
                                                            JSONArray acknowledgedData = jsonObject2.getJSONArray("jobs");

                                                            Log.i("job response array", "job array:" + acknowledgedData.length());

                                                            for (int i = 0; i < acknowledgedData.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) acknowledgedData.get(i);


                                                                LogCustom.i("jobDateAcknowledged", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobAcknowledged", "is:" + jsonObject.getInt("total_job"));

                                                                JSONArray acknowledgedJobArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < acknowledgedJobArray.length(); m++) {

                                                                    JSONObject acknowledgedJobJson = (JSONObject) acknowledgedJobArray.get(m);

                                                                    LogCustom.i(acknowledgedJobJson, "jsonObject");
                                                                    JobOrder jobOrder = new JobOrder(acknowledgedJobJson);
                                                                    JobOrder pickUpJobOrder = jobOrder;
                                                                    pickUpJobOrder.setSelected(true);

                                                                    Log.i("", "job order id:" + pickUpJobOrder.getOrderId());
                                                                    pickUpJobOrder.setIsDelivery(false);

                                                                    try {

                                                                        if (editsearch != null && editsearch.getText().length() == 0) {
                                                                            allJobList.add(jobOrder);
                                                                            acknowledgedJobList.add(pickUpJobOrder);
                                                                            LogCustom.i(jobOrder.getDropOffWorkerId(), "getDropOffWorkerIdAcknowledged1");
                                                                            LogCustom.i(resource_owner_id, "resource_owner_idAcknowledged1");

                                                                            if (pickUpJobOrder.getDropOffWorkerId() != resource_owner_id) {
                                                                                //  acknowledgedJobListExtraWorker.add(pickUpJobOrder);
                                                                            }

                                                                        } else {
                                                                            acknowledgedJobList.add(pickUpJobOrder);
                                                                            LogCustom.i(jobOrder.getDropOffWorkerId(), "getDropOffWorkerIdAcknowledged2");
                                                                            LogCustom.i(resource_owner_id, "resource_owner_idAcknowledged2");

                                                                            if (pickUpJobOrder.getDropOffWorkerId() != resource_owner_id) {
                                                                                //  acknowledgedJobListExtraWorker.add(pickUpJobOrder);
                                                                            }
                                                                        }

                                                                        // add new job into database. Not updated Job
                                                                        if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                            jobOrder.setUpdateWithNoInternet(false);
                                                                            addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                        }


                                                                        searchResultJobList.add(pickUpJobOrder);

                                                                        //     mainStatusJobList.add(acknowledgedJobList);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }
                                                            //  break;

                                                        } else if (jobStatus == inprogressOrderStatusIDFromDB) {
                                                            JSONArray inProgressData = jsonObject2.getJSONArray("jobs");

                                                            Log.i("job response array", "job array:" + inProgressData.length());

                                                            for (int i = 0; i < inProgressData.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) inProgressData.get(i);


                                                                LogCustom.i("jobDateSelf", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobSelf", "is:" + jsonObject.getInt("total_job"));

                                                                JSONArray inProgressJobArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < inProgressJobArray.length(); m++) {

                                                                    JSONObject inProgressJobJson = (JSONObject) inProgressJobArray.get(m);
                                                                    JobOrder jobOrder = new JobOrder(inProgressJobJson);

                                                                    LogCustom.i(jobOrder.getOrderId(), "order id");

                                                                    LogCustom.i(jobOrder.getOrderStatusId(), "order status");

                                                                    try {

                                                                        inProgressJobList.add(jobOrder);

                                                                        if (editsearch != null && editsearch.getText().length() == 0) {
                                                                            allJobList.add(jobOrder);
                                                                        }

                                                                        // add new job into database. Not updated Job
                                                                        if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                            jobOrder.setUpdateWithNoInternet(false);
                                                                            addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                        }

                                                                        searchResultJobList.add(jobOrder);

                                                                        //    mainStatusJobList.add(selfCollectJobList);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }

                                                            //   break;

                                                        } else if (jobStatus == completedOrderStatusIDFromDB) {
                                                            JSONArray completeData = jsonObject2.getJSONArray("jobs");

                                                            Log.i("job response array", "job array:" + completeData.length());

                                                            for (int i = 0; i < completeData.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) completeData.get(i);
                                                                LogCustom.i(jsonObject, "jsonObject");

                                                                LogCustom.i("jobDateComplete", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobComplete", "is:" + jsonObject.getInt("total_job"));


                                                                JSONArray completeJobArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < completeJobArray.length(); m++) {

                                                                    JSONObject completeJobJson = (JSONObject) completeJobArray.get(m);
                                                                    JobOrder jobOrder = new JobOrder(completeJobJson);

                                                                    JobOrder pickUpJobOrder = jobOrder;

                                                                    Log.i("", "job order id:" + pickUpJobOrder.getOrderId());
                                                                    pickUpJobOrder.setIsDelivery(false);

                                                                    try {

                                                                        if (editsearch != null && editsearch.getText().length() == 0) {
                                                                            allJobList.add(jobOrder);
                                                                            completedJobList.add(pickUpJobOrder);
                                                                        }

                                                                        // add new job into database. Not updated Job
                                                                        if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                            jobOrder.setUpdateWithNoInternet(false);
                                                                            addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                        }


                                                                        searchResultJobList.add(pickUpJobOrder);

                                                                        //    mainStatusJobList.add(completedJobList);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }


                                                            }
                                                            //   break;

                                                        } else if (jobStatus == cancelledOrderStatusIDFromDB) {
                                                            JSONArray cancelledData = jsonObject2.getJSONArray("jobs");

                                                            Log.i("job response cancelled", "job array:" + cancelledData.length());

                                                            for (int i = 0; i < cancelledData.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) cancelledData.get(i);
                                                                LogCustom.i(jsonObject, "jsonObject");

                                                                LogCustom.i("jobDateCancel", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobCancel", "is:" + jsonObject.getInt("total_job"));

                                                                JSONArray cancelledArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < cancelledArray.length(); m++) {

                                                                    JSONObject cancelledJobJson = (JSONObject) cancelledArray.get(m);
                                                                    JobOrder jobOrder = new JobOrder(cancelledJobJson);

                                                                    try {

                                                                        if (editsearch != null && editsearch.getText().length() == 0) {
                                                                            allJobList.add(jobOrder);
                                                                            cancelledJobList.add(jobOrder);
                                                                        }


                                                                        // add new job into database. Not updated Job
                                                                        if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                            jobOrder.setUpdateWithNoInternet(false);
                                                                            addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                        }

                                                                        searchResultJobList.add(jobOrder);

                                                                        //    mainStatusJobList.add(cancelledJobList);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            }
                                                            //   break;


                                                        } else if (jobStatus == failedOrderStatusIDFromDB) {
                                                            JSONArray failedData = jsonObject2.getJSONArray("jobs");

                                                            Log.i("job response array", "job array:" + failedData.length());

                                                            for (int i = 0; i < failedData.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) failedData.get(i);
                                                                LogCustom.i(jsonObject, "jsonObject");

                                                                LogCustom.i("jobDatefailed", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobfailed", "is:" + jsonObject.getInt("total_job"));

                                                                JSONArray failedArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < failedArray.length(); m++) {

                                                                    JSONObject failedJobJson = (JSONObject) failedArray.get(m);
                                                                    JobOrder jobOrder = new JobOrder(failedJobJson);

                                                                    try {

                                                                        if (editsearch != null && editsearch.getText().length() == 0) {
                                                                            allJobList.add(jobOrder);
                                                                            failedJobList.add(jobOrder);
                                                                        }

                                                                        // add new job into database. Not updated Job
                                                                        if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                            jobOrder.setUpdateWithNoInternet(false);
                                                                            addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                        }

                                                                        searchResultJobList.add(jobOrder);

                                                                        //    mainStatusJobList.add(cancelledJobList);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }


                                                                }
                                                            }
                                                            //   break;

                                                        } else if (jobStatus == selfcollectOrderStatusIDFromDB) {
                                                            JSONArray selfCollectData = jsonObject2.getJSONArray("jobs");

                                                            Log.i("job response array", "job array:" + selfCollectData.length());

                                                            for (int i = 0; i < selfCollectData.length(); i++) {
                                                                JSONObject jsonObject = (JSONObject) selfCollectData.get(i);


                                                                LogCustom.i("jobDateSelf", "is:" + jsonObject.getString("date"));
                                                                LogCustom.i("jobTotalJobSelf", "is:" + jsonObject.getInt("total_job"));

                                                                JSONArray selfCollectJobArray = jsonObject.getJSONArray("job");

                                                                for (int m = 0; m < selfCollectJobArray.length(); m++) {

                                                                    JSONObject selfCollectJobJson = (JSONObject) selfCollectJobArray.get(m);
                                                                    JobOrder jobOrder = new JobOrder(selfCollectJobJson);

                                                                    LogCustom.i(jobOrder.getOrderId(), "order id");

                                                                    LogCustom.i(jobOrder.getOrderStatusId(), "order status");

                                                                    try {

                                                                        selfCollectJobList.add(jobOrder);

                                                                        if (editsearch != null && editsearch.getText().length() == 0) {
                                                                            allJobList.add(jobOrder);
                                                                        }

                                                                        // add new job into database. Not updated Job

                                                                        if (!dbUpdateJobs.isJobOrderIDHaveUpdatedIntoDB(jobOrder.getOrderId())) {
                                                                            jobOrder.setUpdateWithNoInternet(false);
                                                                            addJobDetailsIntoDBNoInternet.add(jobOrder);
                                                                        }


                                                                        searchResultJobList.add(jobOrder);

                                                                        //    mainStatusJobList.add(selfCollectJobList);

                                                                    } catch (Exception e) {
                                                                        e.printStackTrace();
                                                                    }
                                                                }
                                                            }

                                                            //  break;


                                                        }
                                                    }

                                                    Log.i("", "searchlistsize" + searchResultJobList.size());
                                                    if (editsearch != null && editsearch.getText().length() != 0 && searchResultJobList.size() == 0)
                                                        Toast.makeText(mActivity, "Empty Result.", Toast.LENGTH_SHORT).show();

                                                } catch (Throwable e) {
                                                    e.printStackTrace();
                                                }


                                                if (editsearch != null && editsearch.getText().length() == 0) {


                                                    LogCustom.i(assignedJobList.size(), "assigned job list 00");
                                                    LogCustom.i(assignedJobListExtraWorker.size(), "assignedJobListExtraWorker job list 00");
                                                    LogCustom.i(acknowledgedJobList.size(), "acknowledged job list 00");
                                                    LogCustom.i(completedJobList.size(), "complete job list 00");
                                                    LogCustom.i(cancelledJobList.size(), "cancelled job list 00");
                                                    LogCustom.i(selfCollectJobList.size(), "selfCollect job list 00");
                                                    LogCustom.i(addJobDetailsIntoDBNoInternet.size(), "addJobDetailsIntoDBNoInternet job list 00");
                                                    LogCustom.i(allJobList.size(), "allJobList job list 00");
                                                    //  LogCustom.i(mainStatusJobList.size(), "mainStatusJobList job list 00");


                                                    //hide show after reload data
                                                    updateHideShowBottomButton(selectedTab);


                                                    searchResultJobList.clear();

//                                                for(int i=0; i<jobAssignedFragment.size(); i++){
//                                                    jobAssignedFragment.get(i).reloadJobList(mainStatusJobList.get(i), needReload, endOfList, false, selectedDate);
//                                                }

                                                    jobAssignedFragment.get(0).reloadJobList(assignedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(1).reloadJobList(acknowledgedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(2).reloadJobList(inProgressJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(3).reloadJobList(completedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(4).reloadJobList(cancelledJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(5).reloadJobList(failedJobList, needReload, endOfList, selectedDate);
                                                    jobAssignedFragment.get(6).reloadJobList(selfCollectJobList, needReload, endOfList, selectedDate);


                                                    updateCalendar();

                                                    tabs.updateJob(assignedJobList.size(), acknowledgedJobList.size(), inProgressJobList.size(), completedJobList.size(), cancelledJobList.size(), failedJobList.size(), selfCollectJobList.size());


                                                    // Add to db
                                                    addJobDetailsIntoDBNoInternet(addJobDetailsIntoDBNoInternet, addJobUpdateDetailsIntoDBNoInternet);


                                                } else {
                                                    LogCustom.i("heeree", "222");
                                                    reloadJobList(searchResultJobList, needReload, endOfList, true, true);

                                                    //            jobAssignedFragment.reloadJobList(assignedJobList, needReload, endOfList, true);
                                                    //            jobCompletedFragment.reloadJobList(confirmJobList, needReload, endOfList, true);
                                                }

                                            }


//                                            boolean neverShowMessageUpdateLatestVersion = settings.getBoolean("neverShowMessageUpdateLatestVersion", false);
//                                            if(!neverShowMessageUpdateLatestVersion){
//                                                checkingVersionAppPlayStore();
//                                            }


                                        }

                                    });

                                }


                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                                LogCustom.i("FetchJob", "failed");

                                if (getActivity() == null) {

                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if ((pdProcess != null) && pdProcess.isShowing()) {
                                                pdProcess.dismiss();
                                            }

                                            errorHandlingFromResponseCode(body, requestCallBackError, responseCode);

                                        }
                                    });
                                }


                            }
                        });
            } catch (Exception e) {
                if ((pdProcess != null) && pdProcess.isShowing()) {
                    pdProcess.dismiss();
                }
                e.printStackTrace();
                try {
//                    Crashlytics.logException(e);
                } catch (final Throwable t) {
                }

            }
        } catch (Exception e) {
            if ((pdProcess != null) && pdProcess.isShowing()) {
                pdProcess.dismiss();
            }

            try {
//                Crashlytics.logException(e);
            } catch (final Throwable t) {
            }

        }

    }

//    public void stopLocationServices(){
//        GeneralActivity generalActivity = new GeneralActivity();
//        if(generalActivity.isMyServiceLocationRunning(getContext())){
//            // generalActivity.startAlarmManager(getContext());
//            LogCustom.i("stop services","true");
//            getActivity().stopService(new Intent(getActivity(),LocationService.class));
//        }
//    }
//
//    public void  startLocationServices(){
//        GeneralActivity generalActivity = new GeneralActivity();
//        if(!generalActivity.isMyServiceLocationRunning(getContext())){
//            // generalActivity.startAlarmManager(getContext());
//            LogCustom.i("isMyServiceLocationRunning","false");
//            getActivity().startService(new Intent(getActivity(),LocationService.class));
//        }
//    }

//    public void checkingDriverStatusAndLocationStatus(){
//        //if status online, location must on. else must be offline
//        boolean isOnline = settings.getBoolean("isOnline", true);
//        if(isOnline){
//            LocationManager locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
//            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
//                LogCustom.i("GPS is Enabled in your device true");
//
//                SharedPreferences settingss = PreferenceManager.getDefaultSharedPreferences( getActivity());
//                SharedPreferences.Editor editors = settingss.edit();
//                editors.putBoolean("isOnline", true);
//                editors.apply();
//
//                startLocationServices();
//            }else{
//                showGPSDisabledAlertToUser();
//            }
//
//        }else{
//
//            SharedPreferences settingss = PreferenceManager.getDefaultSharedPreferences( getActivity());
//            SharedPreferences.Editor editors = settingss.edit();
//            editors.putBoolean("isOnline", false);
//            editors.apply();
//
//            stopLocationServices();
//        }
//    }

//    private void showGPSDisabledAlertToUser(){
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
//        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
//                .setCancelable(false)
//                .setPositiveButton("Settings",
//                        new DialogInterface.OnClickListener(){
//                            public void onClick(DialogInterface dialog, int id){
//                                Intent callGPSSettingIntent = new Intent(
//                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                                mActivity.startActivityForResult(callGPSSettingIntent,Constants.INTENT_ACTIVITY_GPS_SETTINGS);
//                            }
//                        });
//        alertDialogBuilder.setNegativeButton("Cancel",
//                new DialogInterface.OnClickListener(){
//                    public void onClick(DialogInterface dialog, int id){
//                        dialog.cancel();
//
//                        SharedPreferences settingss = PreferenceManager.getDefaultSharedPreferences( getActivity());
//                        SharedPreferences.Editor editors = settingss.edit();
//                        editors.putBoolean("isOnline", false);
//                        editors.apply();
//
//                        stopLocationServices();
//                    }
//                });
//        AlertDialog alert = alertDialogBuilder.create();
//        alert.show();
//    }

    public void checkingVersionAppPlayStore() {
        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.getVersionFromPlayStore(getContext(), ""
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            if (getActivity() == null) {

                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        String startToken = "softwareVersion\">";
                                        String endToken = "<";
                                        int index = body.indexOf(startToken);

                                        if (index == -1) {
                                            mVersionFromPlayStore = null;
                                            LogCustom.i("index", "-1");

                                        } else {
                                            mVersionFromPlayStore = body.substring(index + startToken.length(), index
                                                    + startToken.length() + 100);
                                            mVersionFromPlayStore = mVersionFromPlayStore.substring(0, mVersionFromPlayStore.indexOf(endToken)).trim();
                                            LogCustom.i("mVersionFromPlayStore", "" + mVersionFromPlayStore);
                                        }

                                        try {
                                            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                                            currentVersion = pInfo.versionName;
                                        } catch (PackageManager.NameNotFoundException e) {
                                            e.printStackTrace();
                                        }

                                        if (mVersionFromPlayStore != null) {
                                            if (mVersionFromPlayStore.compareTo(currentVersion) > 0) {
                                                LogCustom.i("needUpdateApp", "yess");
                                                showPopupUpdateLatestVersionPlayStore();

                                            } else {
                                                LogCustom.i("needUpdateApp", "Noo");
                                            }
                                        }


                                    }
                                });
                            }


                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            if (getActivity() == null) {

                            } else {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        LogCustom.i("failed to check update version", "");
                                    }
                                });
                            }


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopupUpdateLatestVersionPlayStore() {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.layout_update_latest_version, null);
        final CheckBox neverShowMessage = alertLayout.findViewById(R.id.neverShowMessage);

        neverShowMessage.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                neverShowMessageUpdateLatestVersionCheckBox = isChecked;
            }
        });

        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton("LATER", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (neverShowMessageUpdateLatestVersionCheckBox) {
                    // SharedPreferences settingss = getActivity().getSharedPreferences( PREFS_NAME, 0 );
                    SharedPreferences settingss = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editors = settingss.edit();
                    editors.putBoolean("neverShowMessageUpdateLatestVersion", true);
                    editors.apply();
                }
            }
        });

        alert.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                download();
            }
        });
        AlertDialog dialog = alert.create();
        dialog.show();
    }

    private void checkWriteExternalStoragePermission() {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            /** If we have permission than we can Start the Download the task **/
            downloadTask();
        } else {
            /** If we don't have permission than requesting  the permission **/
            requestWriteExternalStoragePermission();
        }
    }

    private void downloadTask() {
        /** This @DownloadApk class is provided by our Library **/
        /** Pass the  Context when creating object of DownlodApk **
         */

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.logisfleet.taskme")));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.logisfleet.taskme")));
        }
    }


    private void requestWriteExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    public void download() {
        /** First check the external storage permission.**/
        checkWriteExternalStoragePermission();
    }

    public void updateAdapter(JobAdapter adapter) {
//        searchResultListView.setLoadingView(R.layout.loading_layout);
        searchResultListView.setAdapter(adapter);

    }


    public void reloadJobList(List<JobOrder> jobList, boolean firstLoad, boolean endOfList, boolean showDate, boolean isSearching) {
        updateAdapter(new JobAdapter(mActivity, this.searchResultJobList, selectedDate, isSearching));
    }


    boolean search() {

        query = editsearch.getText().toString();

        if (query.length() < 3) {
            Toast.makeText(mActivity, "The keyword length must be at least 3.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mActivity != null) {
            final InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(editsearch.getWindowToken(), 0);
            //			inputMethodManager.toggleSoftInput( InputMethodManager.SHOW_FORCED, 0 );
        }

        searchResultJobList.clear();
        mult = 0;
        endOfList = false;
        if (settings.contains("sort_column")) {
            sortCol = settings.getInt("sort_column", 0);
        } else {
            sortCol = 0;
        }

        Log.i("", "query:" + query);
        if (settings.contains("sort_order")) {
            sortOrder = settings.getInt("sort_order", 0);
        } else {
            sortOrder = 1;
        }
        needReload = true;

        for (int i = 0; i < jobAssignedFragment.size(); i++) {
            if (selectedTab == i)
                jobAssignedFragment.get(i).clearJobList(selectedDate);
        }

        searchButtonPressed = true;
        startSearching(query);
        return true;

    }

    private void updateCalendar() {
        currentCalendar = Calendar.getInstance(Locale.getDefault());
        currentCalendar.add(Calendar.MONTH, currentMonthIndex);
        calendarLayout.initializeCalendar(currentCalendar);
        boolean inProgressBool = false;
        boolean completedBool = false;
        boolean firstRound = true;
        // String jobDay = "";

        Date jobDay = new Date();
        for (int j = 0; j < allJobList.size(); j++) {
            JobOrder job = allJobList.get(j);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            Date convertedDate = new Date();


            try {
                if ((job.getOrderStatusId() == assignedOrderStatusIDFromDB || job.getOrderStatusId() == ackowledgedOrderStatusIDFromDB ||
                        job.getOrderStatusId() == inprogressOrderStatusIDFromDB || job.getOrderStatusId() == completedOrderStatusIDFromDB ||
                        job.getOrderStatusId() == failedOrderStatusIDFromDB || job.getOrderStatusId() == selfcollectOrderStatusIDFromDB
                )
                        && job.getDropOffDate() != null) {
                    convertedDate = dateFormat.parse(job.getDropOffDate());
                    LogCustom.i("convertedDate", "" + convertedDate);

                }

                LogCustom.i("convertedDate1", "" + convertedDate);

                String intMonth = (String) android.text.format.DateFormat.format("MM", convertedDate); //06
                int month = currentCalendar.get(Calendar.MONTH) + 1;

                if (Integer.valueOf(intMonth) == month) {
                    if (firstRound) {
                        firstRound = false;
                        jobDay = convertedDate;

                        LogCustom.i("convertedDate22", "" + firstRound);
                        //jobDay = currentCalendar.get(Calendar.DAY_OF_MONTH);
                    } else {
                        String oldJobDay = (String) android.text.format.DateFormat.format("dd", jobDay); //06
                        String newJobDay = (String) android.text.format.DateFormat.format("dd", convertedDate); //06

                        Log.i("", "jobCalendar old job day:" + oldJobDay + "new job day:" + newJobDay);

                        if (Integer.valueOf(oldJobDay) != Integer.valueOf(newJobDay)) {
                            if (completedBool && inProgressBool) {
                                LogCustom.i("convertedDate2222", "yes");
                                calendarLayout.markDayWithStyle(RobotoCalendarView.RED_CIRCLE, jobDay, 0);

                            } else if (completedBool && inProgressBool == false) {
                                LogCustom.i("convertedDate233222", "yes");
                                calendarLayout.markDayWithStyle(RobotoCalendarView.RED_CIRCLE, jobDay, 0);

                            } else if (inProgressBool && completedBool == false) {
                                LogCustom.i("convertedDate244222", "yes");
                                calendarLayout.markDayWithStyle(RobotoCalendarView.RED_CIRCLE, jobDay, 0);

                            }

                            completedBool = false;
                            inProgressBool = false;
                            jobDay = convertedDate;
                        }

                    }


                    if ((job.getOrderStatusId() == assignedOrderStatusIDFromDB || job.getOrderStatusId() == ackowledgedOrderStatusIDFromDB ||
                            job.getOrderStatusId() == inprogressOrderStatusIDFromDB || job.getOrderStatusId() == completedOrderStatusIDFromDB ||
                            job.getOrderStatusId() == failedOrderStatusIDFromDB || job.getOrderStatusId() == selfcollectOrderStatusIDFromDB
                    )
                            && job.getDropOffDate() != null) {
                        completedBool = true;

                    } else if (job.getOrderStatusId() == ackowledgedOrderStatusIDFromDB || job.getOrderStatusId() == inprogressOrderStatusIDFromDB
                            || job.getOrderStatusId() == assignedOrderStatusIDFromDB) {
                        inProgressBool = true;

                    }
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            System.out.println(convertedDate);

        }

        if (completedBool && inProgressBool) {
            LogCustom.i("convertedDate222223323", "yes");
            calendarLayout.markDayWithStyle(RobotoCalendarView.RED_CIRCLE, jobDay, 0);

        } else if (completedBool && inProgressBool == false) {
            LogCustom.i("convertedDate22211222", "yes");
            calendarLayout.markDayWithStyle(RobotoCalendarView.RED_CIRCLE, jobDay, 0);

        } else if (inProgressBool && completedBool == false) {
            LogCustom.i("convertedDate2232132122", "yes");
            calendarLayout.markDayWithStyle(RobotoCalendarView.RED_CIRCLE, jobDay, 0);

        }

        String intMonth = (String) android.text.format.DateFormat.format("MM", selectedDate); //06
        int month = currentCalendar.get(Calendar.MONTH) + 1;

        if (Integer.valueOf(intMonth) == month) {
            calendarLayout.markDayAsSelectedDay(selectedDate);

        }

    }


    @Override
    public void onLocationChanged(Location location) {

        lat = location.getLatitude();
        lon = location.getLongitude();

        Log.i("", "on location cahnged");


    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(JobOrder jobOrder) {

    }


    /**
     * an animation for resizing the view.
     */
    public class ResizeAnimation extends Animation {

        private final View mView;
        private final float mToHeight;
        private final float mFromHeight;

        private final float mToWidth;
        private final float mFromWidth;

        public ResizeAnimation(View v, float fromWidth, float fromHeight, float toWidth, float toHeight) {
            mToHeight = toHeight;
            mToWidth = toWidth;
            mFromHeight = fromHeight;
            mFromWidth = fromWidth;
            mView = v;
            setDuration(300);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height = (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
            float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
            ViewGroup.LayoutParams p = mView.getLayoutParams();
            p.height = (int) height;
            p.width = (int) width;
            mView.requestLayout();
        }
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final Context mContext;

        public MyPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return TITLES.size();
        }

        @Override
        public Object instantiateItem(View pager, int position) {
            TextView v = new TextView(mContext);
            // Timber.d( "v width:" + v.getWidth());
            // v.setWidth(200);
            v.setText(TITLES.get(position));

            ((ViewPager) pager).addView(v, 0);
            return v;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES.get(position);
        }

        public int dpToPx(Context context, float dpValue) {
            final float scale = context.getResources().getDisplayMetrics().density;
            return (int) (dpValue * scale + 0.5f);
        }


        @Override
        public Fragment getItem(int position) {

            Fragment jobReturn = null;

            for (int i = 0; i < jobAssignedFragment.size(); i++) {
                if (position == i) {
                    jobReturn = jobAssignedFragment.get(i);
                }
            }

            return jobReturn;

        }
    }

}

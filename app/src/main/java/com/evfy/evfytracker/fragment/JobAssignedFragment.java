package com.evfy.evfytracker.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.JobAdapter;
import com.evfy.evfytracker.classes.JobOrder;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobAssignedFragment extends Fragment {

	JobAdapter listAdapter;
	RecyclerView jobListView;
	TextView emptyTextView, dateText;
	List<JobOrder> jobList = new ArrayList<JobOrder>();
	boolean endOfList = false;
	Activity mActivity;

	public void changeDate(Date date) {
		SimpleDateFormat format1 = new SimpleDateFormat("dd MMMM yyyy");
		if (date != null && dateText != null) {
			dateText.setText(format1.format(date));
		}

	}

	public void dateLabelVisibility(boolean show) {
		if (show) {
			if (dateText != null) {
				dateText.setVisibility(View.GONE);
			}

		} else {
			if (dateText != null) {
				dateText.setVisibility(View.GONE);
			}
		}

	}

	public void clearJobList(Date date) {
		Log.i("", "clear job list");

		SimpleDateFormat format1 = new SimpleDateFormat("dd MMMM yyyy");
		if (date != null && dateText != null) {
			dateText.setText(format1.format(date));
		}

		if (listAdapter != null) {
			listAdapter.clearList();
		}

	}

	public void reloadJobList(List<JobOrder> jobList, boolean firstLoad, boolean endOfList, Date selectedDate) {

		Log.i("", "reload job list");
		//Timber.d("reload job list:"+assignedJobList);
		this.endOfList = endOfList;
		if (firstLoad) {
			this.jobList.clear();
			this.jobList.addAll(jobList);

			if (mActivity == null) mActivity = getActivity();

			if (mActivity != null) {
				updateAdapter(new JobAdapter(mActivity, this.jobList, selectedDate, false));
			}

			//			LogCustom.i( listAdapter.getCount(), "listAdapter.getCount()" );

		} else {
			((JobAdapter) jobListView.getAdapter()).addNewData(jobList);

		}

		if (emptyTextView != null) {

			LogCustom.i("emptyTextView", "not null");

			if (this.jobList.size() == 0) {
				emptyTextView.setVisibility(View.VISIBLE);

			} else {
				emptyTextView.setVisibility(View.GONE);
				LogCustom.i("jobList", "not zero");
			}
		}
	}

	public void updateAdapter(JobAdapter adapter) {
		jobListView.setAdapter(adapter);


	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mActivity = activity;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		setHasOptionsMenu(true);
		//getActivity().getActionBar().setTitle("Jobs");

		View rootView = inflater.inflate(R.layout.fragment_job_ongoing, container, false);
		getActivity().overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
		// get the listview
		jobListView = rootView.findViewById(R.id.jobList);
		jobListView.setHasFixedSize(true);

		emptyTextView = rootView.findViewById(R.id.empty);
		emptyTextView.setVisibility(View.GONE);
		dateText = rootView.findViewById(R.id.dateTextView);
		dateText.setVisibility(View.GONE);

		SimpleDateFormat format1 = new SimpleDateFormat("dd MMMM yyyy");
		dateText.setText(format1.format(new Date()));
		Log.i("datetext", dateText.getText().toString());
//        BadgeView badge = new BadgeView(getActivity(), ((JobFragment)getParentFragment()).currentTab);
//        badge.setText("1"); //Whatever value you should add
//        badge.show();
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();

	}
}

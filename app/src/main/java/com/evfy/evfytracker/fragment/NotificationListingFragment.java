package com.evfy.evfytracker.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.MultiDex;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.Activity.JobStepActivity.ManyJobStepsActivity;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.TimeAgo;
import com.evfy.evfytracker.classes.NotificationDatabase;
import com.facebook.stetho.Stetho;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class NotificationListingFragment extends Fragment {

    LinearLayout mLayoutListItem;
    Context context;
    DatabaseHandlerJobs dbUpdateJobs;
    private Activity mActivity;
    MenuItem.OnMenuItemClickListener mListenerDelete;
    SharedPreferences settings;

    public static final String ACTION_REFRESH_NOTIFICATION_LISTING = "refreshNotificationListing";
    BroadcastReceiver mReceiverRefreshNotificationListing;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        // Timber.d( "on attach mactivity null:" + mActivity + "get activity:" + getActivity());
        MultiDex.install(mActivity);
        LogCustom.i("InstallMultiDex");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {

        ((DrawerActivity) getActivity()).getSupportActionBar().setTitle("Notifications");

        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.fragment_notification, container,
                false);


        dbUpdateJobs = new DatabaseHandlerJobs(mActivity);

        context = getActivity().getApplicationContext(); // or activity.getApplicationContext()

        mLayoutListItem = v.findViewById(R.id.mLayoutListItem);


        Stetho.initializeWithDefaults(getActivity());

        mLayoutNotificationListing();


        settings = PreferenceManager.getDefaultSharedPreferences(getActivity());

        SharedPreferences.Editor editors = settings.edit();
        editors.putBoolean("insideFragmentPage", false).commit();
        editors.putBoolean("insideJobDetails", false).commit();
        editors.putBoolean("insideNotificationListing", true).commit();
        editors.apply();


        mReceiverRefreshNotificationListing = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LogCustom.i("refresh job listing true");
                onResume();
            }
        };

        LocalBroadcastManager.getInstance(getContext())
                .registerReceiver(mReceiverRefreshNotificationListing, new IntentFilter(ACTION_REFRESH_NOTIFICATION_LISTING));


        return v;
    }

    public void mLayoutNotificationListing() {
        mLayoutListItem.removeAllViews();

        List<NotificationDatabase> notificationDatabaseList = new ArrayList<NotificationDatabase>();

        notificationDatabaseList = dbUpdateJobs.getAllNotification();


        if (notificationDatabaseList.size() != 0) {

            try {

                Collections.sort(notificationDatabaseList, new Comparator<NotificationDatabase>() {

                    @Override
                    public int compare(final NotificationDatabase lhs, final NotificationDatabase rhs) {

//                        if (lhs == null && rhs == null) return 0;
//                        if (lhs == null) return -1;
//                        if (rhs == null) return 1;
                        return ((Integer) rhs.getIndexDB()).compareTo(lhs.getIndexDB());
                    }

                });

            } catch (final Throwable e) {
                LogCustom.e(e);
            }

            for (int i = 0; i < notificationDatabaseList.size(); i++) {
                final NotificationDatabase notificationDatabase = notificationDatabaseList.get(i);

                LogCustom.i("notificationDatabase", "indexDB:" + notificationDatabase.getIndexDB());
                LogCustom.i("notificationDatabase", "listing:" + notificationDatabase.getReferenceNumber());
                LogCustom.i("notificationDatabase", "listing:" + notificationDatabase.isJobDestroy());
                LogCustom.i("notificationDatabase", "listing:" + notificationDatabase.isDoneRead());

                final View view = View.inflate(context, R.layout.view_notification_listing, null);
                mLayoutListItem.addView(view);
                final LinearLayout layoutNotification = view.findViewById(R.id.layoutNotification);
                final TextView titleNotification = view.findViewById(R.id.titleNotification);
                final TextView contentNotification = view.findViewById(R.id.contentNotification);
                final TextView timeCreated = view.findViewById(R.id.timeCreated);
                final ImageView badgeNotification = view.findViewById(R.id.badgeNotification);

                // titleNotification.setTag(i);

                if (notificationDatabase.getAttributeUpdated().length() > 0) {
                    titleNotification.setText("" + notificationDatabase.getReferenceNumber() + " updated");
                    contentNotification.setText(getActivity().getResources().getString(R.string.jobUpdateMessage));
                }

                if (notificationDatabase.isJobDestroy() == 1) {
                    titleNotification.setText("" + notificationDatabase.getReferenceNumber() + " removed");
                    contentNotification.setText(getActivity().getResources().getString(R.string.jobDestroyMessage));
                }

                if (notificationDatabase.getAttributeUpdated().length() <= 0 && notificationDatabase.isJobDestroy() == 0 && notificationDatabase.isJobUpdated() == 0) {
                    titleNotification.setText("" + notificationDatabase.getReferenceNumber() + " created");
                    contentNotification.setText(getActivity().getResources().getString(R.string.jobCreateMessage));
                }

                if (notificationDatabase.isDoneRead() == 1) {
                    badgeNotification.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_notification));
                } else {
                    badgeNotification.setBackground(getActivity().getResources().getDrawable(R.drawable.icon_notification_badge));
                }


                String timeAgo = TimeAgo.getTimeAgo(Long.valueOf(notificationDatabase.getTimeCreated()));
                LogCustom.i("timeAgo", "is:" + timeAgo);

                timeCreated.setText(timeAgo);


                //   final int rowIndex = (Integer) titleNotification.getTag();


                layoutNotification.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        LogCustom.i("notificationDatabase", "getIndexDB:" + notificationDatabase.getIndexDB());
                        LogCustom.i("notificationDatabase", "getIndexDB:" + notificationDatabase.getIndexDB());
                        notificationDatabase.setDoneRead(1);
                        dbUpdateJobs.addNotification(notificationDatabase, true, false);

                        Intent ii = new Intent(DrawerActivity.ACTION_REFRESH_NOTIFICATION_SIDE_MENU);
                        LocalBroadcastManager.getInstance(getContext())
                                .sendBroadcast(ii);

                        if (notificationDatabase.isJobDestroy() == 1 || notificationDatabase.isJobUpdated() == 1) {
                            refreshJobDialog(getActivity().getResources().getString(R.string.jobRemovedTitle), getActivity().getResources().getString(R.string.jobRemovedContent), false);
                        } else {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (!generalActivity.haveInternetConnected(context)) {
                                Log.i("internetConnection", "no");
                                showErrorInternetDialog().show();
                            } else {
                                Log.i("internetConnection", "yes");
                                Intent intent = new Intent().setClass(v.getContext(), ManyJobStepsActivity.class);
                                intent.putExtra("OrderId", Integer.valueOf(notificationDatabase.getOrderId()));
                                intent.putExtra("order_number", notificationDatabase.getReferenceNumber());


                                getActivity().startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.open_next, R.anim.close_main);
                            }
                        }

//                        Intent i = new Intent(getContext(), JobListingSpecificDriver.class);
//                        i.putExtra("workerId", worker.getWorkerId());
//                        i.putExtra("workerName", worker.getFirstName() + " " + worker.getLastName());
//                        startActivity(i);

                    }
                });

            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mLayoutNotificationListing();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.notification_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);


        final MenuItem menuRefresh = menu.findItem(R.id.menu_delete);

        // Show the search menu item in menu.xml
        mListenerDelete = new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                refreshJobDialog(getActivity().getResources().getString(R.string.deleteAllNotificationTitle), getActivity().getResources().getString(R.string.deleteAllNotificationContent), true);
                return false;

            }
        };

        menuRefresh.setOnMenuItemClickListener(mListenerDelete);


    }

    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }


    SweetAlertDialog refreshJobDialog(String titleError, String contextError, boolean deleteJob) {
        final SweetAlertDialog sweetAlertDialog = getRefreshJobDialog(titleError, contextError, deleteJob);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getRefreshJobDialog(String titleError, String contextError, boolean deleteJob) {
        if (deleteJob) {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setCustomImage(getResources().getDrawable(R.drawable.icon_warning))
                    .setContentText(contextError)
                    .setTitleText(titleError)
                    .setCancelText("Cancel")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    })
                    .setConfirmText("Delete All")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            dbUpdateJobs.deleteAllDataNotification();
                            mLayoutNotificationListing();
                            Intent ii = new Intent(DrawerActivity.ACTION_REFRESH_NOTIFICATION_SIDE_MENU);
                            LocalBroadcastManager.getInstance(getContext())
                                    .sendBroadcast(ii);
                        }
                    });


            sweetAlertDialog.setCancelable(false);
            return sweetAlertDialog;
        } else {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                    .setCustomImage(getResources().getDrawable(R.drawable.icon_trash_popup))
                    .setContentText(contextError)
                    .setTitleText(titleError)
                    .setConfirmText("CLOSE")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            mLayoutNotificationListing();
                        }
                    });


            sweetAlertDialog.setCancelable(false);
            return sweetAlertDialog;
        }

    }
}
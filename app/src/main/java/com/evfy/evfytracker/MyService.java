package com.evfy.evfytracker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;

import androidx.annotation.Nullable;

import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.classes.OrderDatabase;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.services.RestManager;

/**
 * Created by jingjiewong on 3/3/17.
 */

public class MyService extends Service implements OnHttpEventListener {
    Calendar cur_cal = Calendar.getInstance();

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        Intent intent = new Intent(this, MyService.class);
        PendingIntent pintent = PendingIntent.getService(getApplicationContext(),
                0, intent, PendingIntent.FLAG_IMMUTABLE);
        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        cur_cal.setTimeInMillis(System.currentTimeMillis());
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cur_cal.getTimeInMillis(),
                60 * 1000 * 5, pintent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        Log.i("", "alarm triggered. check db and send data to server");
        final DatabaseHandlerJobs dbUpdateJobs = new DatabaseHandlerJobs(this);

        for (int i = 0; i < dbUpdateJobs.getAllJobs().size(); i++) {
            try {

                final OrderDatabase orderDatabase = dbUpdateJobs.getAllJobs().get(i);
                Log.i("", "service order database:" + orderDatabase.getOrderJson());
                if (dbUpdateJobs.getAllJobs().get(i).getOrderJson() != null) {
                    final JSONObject jobOrderObject = new JSONObject(dbUpdateJobs.getAllJobs().get(i).getOrderJson());

                    final Runnable runnable = new Runnable() {

                        @Override
                        public void run() {
                            //updateSignature();
                            final OnRESTResultCallback onRESTResultCallback2 = new OnRESTResultCallback() {

                                @Override
                                public void onRESTResult(final int returnCode, final int code, final String result) {
                                    LogCustom.i(result, "result");


                                    dbUpdateJobs.deleteEntryByKEY_ID(orderDatabase.getDatabaseId());


                                }

                            };

                            //SharedPreferences settings = getSharedPreferences("MyPrefsFile", 0);
                            final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                            String accessToken = settings.getString("access_token", "");
                            Pair<String, String> pairHeader = Pair.create("Authorization", "Bearer " + accessToken);
                            final List<Pair<String, String>> header = new ArrayList<>();
                            header.add(pairHeader);
                            Pair<String, String> pairHeader2 = Pair.create("Content-Type", "application/json");

                            header.add(pairHeader2);

                            Log.i("", "access token:" + accessToken);
                            Log.i("", "service job order object:" + jobOrderObject);


                            try {
                                RestManager restManager = RestManagerFactory.createRestManagerWithHttpEventListener(MyService.this, MyService.this);

                                restManager.sendRequest(Constants.WEB_SERVICE_BATCH_UPDATE.getHttpConnection(header, jobOrderObject.toString(), onRESTResultCallback2, ""));

                            } catch (final Throwable e) {
                                LogCustom.e(e);
                                Log.i("", "msgNetworkError 11");


                            }


                        }
                    };

                    AndroidOp.runOnMainThread(this, runnable);

                    final Handler handler = new android.os.Handler();

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            handler.removeCallbacks(runnable);
                            Log.i("", "time out need to store in databse");

                        }
                    }, 15000);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onRequestInit() {

    }

    @Override
    public void onRequestFinish() {

    }
}
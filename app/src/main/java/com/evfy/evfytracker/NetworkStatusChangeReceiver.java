package com.evfy.evfytracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by jingjiewong on 2/3/17.
 */

public class NetworkStatusChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean status = isConnected(context);
        if (!status) {
            Toast.makeText(context, "Network Error !", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Network ok !", Toast.LENGTH_SHORT).show();

        }
    }

    public boolean isConnected(Context context) {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                result = true;
                // msg="You are connected to a WiFi Network";
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                result = true;
                //msg="You are connected to a Mobile Network";
            }
        } else {
            //msg = "No internet Connectivity";
            result = false;
        }

        return result;
    }

}
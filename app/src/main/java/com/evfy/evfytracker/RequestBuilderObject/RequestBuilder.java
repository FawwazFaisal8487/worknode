package com.evfy.evfytracker.RequestBuilderObject;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by coolasia on 6/12/17.
 */

public class RequestBuilder {

    //Login request body
    public static RequestBody LoginBody(String username, String password, String token) {
        return new FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .add("scope", "scope")
                .add("grant_type", "password")
                .add("google_token", token)
                .build();
    }
}

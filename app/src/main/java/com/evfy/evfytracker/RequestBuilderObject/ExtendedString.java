package com.evfy.evfytracker.RequestBuilderObject;

import android.content.Context;

import java.text.DecimalFormat;
import java.util.Vector;

public class ExtendedString {

    /***
     * Display XXdayXXhr or XXhrXXmin or XXminXXsec or XXmin or XXsec format
     *
     * @return
     */

    public static String getString(int id, Context viewContext) {
        String str = viewContext.getString(id);
        return str;
    }

    public static String getString(int id, String[] args, Context viewContext) {
        return String.format(getString(id, viewContext), args);
    }

    public static String getString(String str, String[] args) {
        return String.format(str, args);
    }

    public static String getString(Context viewContext, int id) {
        String str = viewContext.getString(id);
        return str;
    }

    public static String getString(Context viewContext, int id, String[] args) {
        return String.format(getString(viewContext, id), args);
    }

//	public static String formatDateTime(int duration, Context viewContext) {
//		int sec = duration % 60;
//		int min = (duration / 60) % 60;
//		int hr = (duration / 60 / 60) % 60;
//		int day = (duration / 60 / 60 / 24) % 24;
//		System.out.println("Duration:" + duration + ", day:" + day + ", hr:"
//				+ hr + ", min:" + min + ", sec:" + sec);
//		String retTime = "";
//		if (day > 0) {
//			if (hr > 0) {
//				retTime = getString(
//						R.string.lbl_days_hr,
//						new String[] { String.valueOf(day), String.valueOf(hr) },
//						viewContext);
//			} else {
//				retTime = getString(R.string.lbl_days,
//						new String[] { String.valueOf(day) }, viewContext);
//			}
//		} else {
//			if (hr > 0) {
//				if (min > 0) {
//					retTime = getString(R.string.lbl_hr_min, new String[] {
//							String.valueOf(hr), String.valueOf(min) },
//							viewContext);
//				} else {
//					retTime = getString(R.string.lbl_hr,
//							new String[] { String.valueOf(hr) }, viewContext);
//				}
//			} else {
//				if (min > 0) {
//					if (sec > 0) {
//						retTime = getString(R.string.lbl_min, new String[] {
//								String.valueOf(min), String.valueOf(sec) },
//								viewContext);
//					} else {
//						retTime = getString(R.string.lbl_min,
//								new String[] { String.valueOf(min) },
//								viewContext);
//					}
//				} else {
//					retTime = getString(R.string.lbl_sec,
//							new String[] { String.valueOf(sec) }, viewContext);
//				}
//			}
//		}
//		return retTime;
//	}

//	public static String formatDist_m_to_km(int distance, Context viewContext) {
//		String retDistance = "";
//		int dist_km = distance / 1000;
//		return getString(R.string.lbl_km,
//				new String[] { String.valueOf(dist_km) }, viewContext);
//	}

    static DecimalFormat df_km, df_m, df_currency;

    static {
        df_km = new DecimalFormat();
        df_km.setMinimumFractionDigits(1);
        df_km.setMaximumFractionDigits(1);

        df_m = new DecimalFormat();
        df_m.setMinimumFractionDigits(0);
        df_m.setMaximumFractionDigits(0);

        df_currency = new DecimalFormat();
        df_currency.setMinimumFractionDigits(2);
        df_currency.setMaximumFractionDigits(2);

    }

    /***
     *
     * @param distance - distance in meters
     * @param viewContext
     * @return
     */
//	public static String formatDist_m_or_km(double distance, Context viewContext) {
//		distance = distance / 1000.0f;
//		if (distance >= 1) {
//			double dist_km = distance;
//			Log.i("", "FORMAT DISTANCE 00:"+df_km.format(dist_km));
//			return getString(viewContext, R.string.lbl_km,
//					new String[] { String.valueOf(df_km.format(dist_km)) });
//		} else {
//			double dist_m = distance * 1000.0f;
//			Log.i("", "FORMAT DISTANCE 11:"+df_m.format(dist_m));
//
//			return getString(viewContext, R.string.lbl_m,
//					new String[] { String.valueOf(df_m.format(dist_m)) });
//		}
//	}
    public static Vector<String> split(String _strMessage, String _delimiter) {
        Vector<String> result = new Vector<String>();
        int pos = 0;
        int len = _strMessage.length();
        int lenD = _delimiter.length();
        try {
            for (int i = 0; i < len; i++) {
                if (_strMessage.substring(i, i + lenD).equals(_delimiter)) {
                    if (i == pos)
                        result.addElement("");
                    else if (i > pos)
                        result.addElement(_strMessage.substring(pos, i));
                    pos = i + lenD;
                    i += lenD - 1;
                }
            }
            if (pos < len)
                result.addElement(_strMessage.substring(pos, len));
        } catch (Exception ex) {
            System.out.println("WebAPI:ERROR:" + ex.getMessage());
        }
        return result;
    }

    public static String rebuildUrl(String hsURL) {
        Vector argumentV = split(hsURL, " ");
        String newUrl = "";
        for (int i = 0; i < argumentV.size(); i++) {
            if (i == 0) {
                newUrl = argumentV.elementAt(i).toString();
            } else {
                newUrl += "%20" + argumentV.elementAt(i);
            }
        }
        return newUrl;
        /*
         * URLEncodedPostData urlEncoder = new URLEncodedPostData("UTF-8",
         * false); urlEncoder.setData(hsURL); hsURL = urlEncoder.toString();
         * return hsURL;
         */
    }

}

package com.evfy.evfytracker.Activity.WMSActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;
import com.evfy.evfytracker.classes.DeliveryOrderOutDetails;

public class BalanceEnquiryDateOutActivity extends AppCompatActivity {

    DeliveryOrderDetails deliveryOrderDetails;
    LinearLayout mLayoutList;
    DeliveryOrderOutDetails deliveryOrderOutDetails;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_enquiry_date_out);

        deliveryOrderDetails = (DeliveryOrderDetails) getIntent().getSerializableExtra("deliveryOrderDetails");

        Log.i("sizeOfDateOutDO", "" + deliveryOrderDetails.getDeliveryOrderOutDetails().size());

        mLayoutList = findViewById(R.id.mLayoutList);
        toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Date Out");


        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        displayItemList();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void displayItemList() {
        mLayoutList.removeAllViews();

        Log.i("orderDetailListLength", "" + deliveryOrderDetails.getDeliveryOrderOutDetails().size());

        if (deliveryOrderDetails.getDeliveryOrderOutDetails().size() > 0) {
            for (int i = 0; i < deliveryOrderDetails.getDeliveryOrderOutDetails().size(); i++) {

                deliveryOrderOutDetails = deliveryOrderDetails.getDeliveryOrderOutDetails().get(i);
                Log.i("reference_noDO", "" + deliveryOrderOutDetails.getReference_no_DOrderDetail());
                Log.i("out_date", "" + deliveryOrderOutDetails.getHandling_out_date_DOrderDetail());
                Log.i("qty", "" + deliveryOrderOutDetails.getQuantity());
                Log.i("deliveryTo", "" + deliveryOrderOutDetails.getDelivery_to_DOrderDetail());


                final View view = View.inflate(this, R.layout.view_delivery_order_date_out, null);
                mLayoutList.addView(view);
                final TextView txt_doNumber = view.findViewById(R.id.txt_doNumber);
                final TextView txt_dateOut = view.findViewById(R.id.txt_dateOut);
                final TextView txt_to = view.findViewById(R.id.txt_to);
                final TextView txt_qty = view.findViewById(R.id.txt_qtys);
                final LinearLayout layoutList_delivery = view.findViewById(R.id.layoutList_delivery);

                if ((i % 2) == 0) {
                    layoutList_delivery.setBackgroundColor(getResources().getColor(R.color.bgColorOdd));
                } else {
                    layoutList_delivery.setBackgroundColor(getResources().getColor(R.color.bgColorEven));
                }

                txt_doNumber.setText(deliveryOrderOutDetails.getReference_no_DOrderDetail());
                txt_dateOut.setText(deliveryOrderOutDetails.getHandling_out_date_DOrderDetail());
                txt_to.setText(deliveryOrderOutDetails.getDelivery_to_DOrderDetail());
                txt_qty.setText("" + deliveryOrderOutDetails.getQuantity());
            }
        }
    }

}


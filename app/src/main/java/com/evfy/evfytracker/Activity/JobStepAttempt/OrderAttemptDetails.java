package com.evfy.evfytracker.Activity.JobStepAttempt;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.evfy.evfytracker.Activity.ConfirmationActivity.ConfirmationSign;
import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity;
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.OrderAttemptImageAdapter;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.evfy.evfytracker.classes.OrderAttemptImage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.DateTimeOp;
import com.lkh012349s.mobileprinter.Utils.ImageOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;

public class OrderAttemptDetails extends AppCompatActivity implements OnHttpEventListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    OrderAttempt orderAttemptDetail;
    TextView txtTime;
    final ArrayList<String> urlList = new ArrayList<>();
    private SliderLayout mDemoSlider;
    private SliderLayout sliderImage;
    RelativeLayout sliderRelativeLayout;
    LinearLayout layoutReceiveBy, layoutReason, btnCamera;
    EditText txtReceivedBy, txtReason, txtNote, txtSerialNumber, txtNRIC;

    static String FOLDER = "";
    Uri uriPhotoTakenByCamera;
    boolean isFromCompletePhoto = false;
    boolean requestTakeOtherPhoto = false;
    AlertDialog imageDialog;
    ArrayList<String> descriptionImage = new ArrayList<>();
    ArrayList<String> newImageUriListString = new ArrayList<>();
    boolean resultFromGallery;
    ArrayList<Bitmap> imagesUriList = new ArrayList<>();
    String imageBitmap, descriptionImageString, descriptionImageText;
    int totalImage = 0;
    Button btnSubmit;
    Activity mActivity = this;
    int sizeCompressImage = 80;
    double lat, lon;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    RestManager restManager;
    Pair<String, String> pairHeader;
    String image_normalURL;
    int order_status_id, orderId;
    static final int ORDER_ATTEMPT_DETAIL_CODE = 567;
    static final int ORDER_ATTEMPT_IMAGE = 789;
    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");
    int indexImageUpload;

    final ArrayList<String> allOrderAttemptImageURL = new ArrayList<>();
    final ArrayList<String> allOrderAttemptImageDescription = new ArrayList<>();
    final ArrayList<String> allOrderAttemptImageDescriptionBefore = new ArrayList<>();
    final ArrayList<Boolean> allOrderAttemptImageRemove = new ArrayList<>();
    final ArrayList<Integer> allOrderAttemptImageId = new ArrayList<>();
    final ArrayList<Integer> allOrderAttemptImageIdBefore = new ArrayList<>();
    final ArrayList<Integer> allOrderAttemptImageIdNeedToRemove = new ArrayList<>();
    final ArrayList<Integer> allOrderAttemptImageIdNeedToUpdate = new ArrayList<>();

    ArrayList<Bitmap> allOrderAttemptImage = new ArrayList<>();
    OrderAttemptImageAdapter myCustomPagerAdapter;
    boolean removeOrEditImageFromServerBoolean = false;
    int jobStepId, jobStepStatusId, numberOfPage;
    Dialog pdUpdateProcess;
    TextView contextProgressDialog;
    String trackingNumber;
    boolean showCompletedFailedButton, startJobStepsButton, noInternetConnection;
    boolean isOnline, unauthorizedNeedToLoginPage = false;
    DatabaseHandlerJobs dbUpdateJobs;
    int orderAttemptID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_attempt_details);
        FOLDER = getExternalFilesDir(Environment.DIRECTORY_PICTURES + "/" + "KnocKnocKapp").getAbsolutePath();

        unauthorizedNeedToLoginPage = false;
        settings = PreferenceManager.getDefaultSharedPreferences(OrderAttemptDetails.this);

        pdUpdateProcess = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        contextProgressDialog.setText(getResources().getString(R.string.updateJobSteps));
        pdUpdateProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdUpdateProcess.setContentView(content);
        pdUpdateProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdUpdateProcess.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderId = extras.getInt("OrderId");
            order_status_id = extras.getInt("order_status_id", 0);
            jobStepId = extras.getInt("jobStepId");
            numberOfPage = extras.getInt("numberOfPage");
            jobStepStatusId = extras.getInt("jobStepStatusId");
            orderAttemptDetail = (OrderAttempt) getIntent().getSerializableExtra("OrderAttemptImageSend");
            trackingNumber = extras.getString("trackingNumber");
            showCompletedFailedButton = extras.getBoolean("showCompletedFailedButton");
            startJobStepsButton = extras.getBoolean("startJobStepsButton");
            noInternetConnection = extras.getBoolean("noInternetConnection");
            orderAttemptID = extras.getInt("orderAttemptID");

        }


        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(OrderAttemptDetails.this, this);

        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Attempt # " + numberOfPage);

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        dbUpdateJobs = new DatabaseHandlerJobs(this);

        if (noInternetConnection) {
            orderAttemptDetail = dbUpdateJobs.getOrderAttemptDetailsBasedOnID(orderAttemptID);
            orderAttemptDetail.setmOrderAttemptImages(dbUpdateJobs.getListAllImageDetailsNoInternet(orderAttemptDetail.getId()));
        }


        urlList.clear();
        descriptionImage.clear();
        newImageUriListString.clear();
        imagesUriList.clear();
        allOrderAttemptImageURL.clear();
        allOrderAttemptImageDescription.clear();
        allOrderAttemptImageDescriptionBefore.clear();
        allOrderAttemptImageRemove.clear();
        allOrderAttemptImageId.clear();
        allOrderAttemptImageIdBefore.clear();
        allOrderAttemptImageIdNeedToRemove.clear();
        allOrderAttemptImageIdNeedToUpdate.clear();

        initialize();
        if (!noInternetConnection) {
            getLocation();
        }

        getOrderAttemptDetail();

        setEditTextDisable();

        //button camera
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromCompletePhoto = true;
                addImages();
            }
        });

        //button submit
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("", "btn submit click");

                GeneralActivity generalActivity = new GeneralActivity();
                if (!generalActivity.haveInternetConnected(getApplicationContext())) {
                    Log.i("internetConnection", "no");
                    showErrorInternetDialog().show();
                } else {
                    isOnline = settings.getBoolean("isOnline", true);
                    if (isOnline) {
                        btnSubmit.setEnabled(false);
                        SubmitConfirmation();
                    } else {
                        showErrorDialog(getResources().getString(R.string.titleErrorIsNotOnline), getResources().getString(R.string.contextErrorIsNotOnline)).show();
                        LogCustom.i("isOnline", "false");
                    }
                }


            }
        });

        //listener for each event
        txtReceivedBy.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });


        //listener for each event
        txtNRIC.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });

    }

    public void addImages() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null);
        final LinearLayout layoutClickCamera = alertLayout.findViewById(R.id.layoutClickCamera);
        final LinearLayout layoutClickGallery = alertLayout.findViewById(R.id.layoutClickGallery);

        layoutClickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                uriPhotoTakenByCamera = AndroidOp.startCameraForResult(OrderAttemptDetails.this, FOLDER, null);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, AndroidOp.REQUEST_CODE_CAMERA);
            }
        });

        layoutClickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidOp.startGettingImageFromGalleryForResult(OrderAttemptDetails.this);
            }
        });


        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo");
            alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing
                    checkAllInputInserted();

                }
            });
        } else {
            alert.setTitle("Complete action using");
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }


        alert.setView(alertLayout);
        alert.setCancelable(false);


        imageDialog = alert.create();
        if (!imageDialog.isShowing()) {
            imageDialog.show();
        }
    }

    public void addDescription(final Intent data, final boolean resultFromGalleryDescription) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.items_layout_description_image, null);
        final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);


        textViewTitle.setText("Add Description for Image");


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();


        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                descriptionImageText = editTextDescription.getText().toString();

                dialog.dismiss();

                if (!resultFromGalleryDescription) {
                    try {
                        resultFromGallery = false;
                        Bitmap pic = (Bitmap) data.getExtras().get("data");
                        final String curTime = DateTimeOp.getStringNow(DateTimeOp.TimeZone.LOCAL);
                        String filename = curTime.replace(":", ".") + ".jpg";
                        File file = new File(FOLDER + "/" + filename);
                        try {
                            FileOutputStream outputStream = new FileOutputStream(file);
                            pic.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        uriPhotoTakenByCamera = Uri.fromFile(file);
                        showImageCameraGallery(uriPhotoTakenByCamera);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        resultFromGallery = true;
                        Uri photoUri = data.getData();
                        showImageCameraGallery(photoUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        });
    }

    void showImageCameraGallery(final Uri uri) throws IOException {

        //Update show image first
        Log.i("uriPhotoTakenByCamera", "" + uri.getPath());

        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();

        //  descriptionImageString = descriptionImage.get(totalImage - 1);


        if (resultFromGallery) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            File imgFile = new File(filePath);
            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));


            //  url_maps.put("" + descriptionImageString, bmp);

            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

            //  imagesUriList.add(compressedImageBitmap);

            contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
            pdUpdateProcess.show();
            uploadImageBase64ForUrl(compressedImageBitmap);


        } else {

            File imgFile = new File(uri.getPath());

            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));


            //  url_maps.put("" + descriptionImageString, bmp);
            // url_maps.put("Photo No " + totalImage, bmp);
            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

            // imagesUriList.add(compressedImageBitmap);

            contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
            pdUpdateProcess.show();
            uploadImageBase64ForUrl(compressedImageBitmap);

            //  allOrderAttemptImage.add(compressedImageBitmap);

        }

    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;

        switch (requestCode) {

            case AndroidOp.REQUEST_CODE_CAMERA:
                Log.i("requset", "request code camera");


                resultFromGallery = false;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }
                addDescription(data, resultFromGallery);


                break;

            case AndroidOp.REQUEST_CODE_GALLERY:
                Log.i("", "request code gallery");


                resultFromGallery = true;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }
                addDescription(data, resultFromGallery);

                break;
        }

    }

    public void getOrderAttemptDetail() {
        //get data from OrderDetailActivity

        //set Value
        if (orderAttemptDetail.getReason() != null) {
            if (orderAttemptDetail.getReason().length() != 0 || !orderAttemptDetail.getReason().equalsIgnoreCase("")) {
                // layoutReceiveBy.setVisibility(View.VISIBLE);
                layoutReason.setVisibility(View.VISIBLE);
                txtReason.setText(orderAttemptDetail.getReason());

            } else {
                // layoutReceiveBy.setVisibility(View.GONE);
                layoutReason.setVisibility(View.GONE);
            }
        } else {
            layoutReason.setVisibility(View.GONE);
        }


        //set value note
        if (orderAttemptDetail.getNote() != null) {
            if (orderAttemptDetail.getNote().length() != 0 || !orderAttemptDetail.getNote().equalsIgnoreCase("")) {
                txtNote.setText(orderAttemptDetail.getNote());

            } else {
                txtNote.setText(" - ");
            }
        } else {
            txtNote.setText(" - ");
        }

        if (orderAttemptDetail.getReceived_by() != null) {
            if (orderAttemptDetail.getReceived_by().length() != 0 || !orderAttemptDetail.getReceived_by().equalsIgnoreCase("")) {
                txtReceivedBy.setText(orderAttemptDetail.getReceived_by());

            } else {
                txtReceivedBy.setText(" - ");
            }
        } else {
            txtReceivedBy.setText(" - ");
        }


//        if (orderAttemptDetail.getNric().length() != 0 || !orderAttemptDetail.getNric().equalsIgnoreCase("")) {
//            txtNRIC.setText(orderAttemptDetail.getNric());
//
//        } else {
//            txtNRIC.setText(" - ");
//        }
//
//        if (orderAttemptDetail.getSrno().length() != 0 || !orderAttemptDetail.getSrno().equalsIgnoreCase("")) {
//            txtSerialNumber.setText(orderAttemptDetail.getSrno());
//
//        } else {
//            txtSerialNumber.setText(" - ");
//        }


        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        if (!noInternetConnection) {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        Date date = null;

        if (orderAttemptDetail.getSubmitted_time() != null) {
            try {
                date = sdf.parse(orderAttemptDetail.getSubmitted_time());

                SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
                Log.i("OrderTime", fmtOut.format(date));
                txtTime.setText(fmtOut.format(date));

            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            txtTime.setText("N.A");

        }

//        LogCustom.i("submittedTime", "is " + orderAttemptDetail.getSubmitted_time());
//        txtTime.setText(""+orderAttemptDetail.getSubmitted_time());

        LogCustom.i("imageSize", "is " + orderAttemptDetail.getmOrderAttemptImages().size());

        showImageSlider();


    }

    public void loopImageUrlToBitmap() {
        for (int i = 0; i < allOrderAttemptImageURL.size(); i++) {
            getBitmapFromURL(allOrderAttemptImageURL.get(i));
            LogCustom.i("loopAkmal", allOrderAttemptImageURL.get(i));
        }
    }


    public void getBitmapFromURL(final String src) {
        new AsyncTask<String, String, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... strings) {
                Bitmap myBitmap = null;
                try {
                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    myBitmap = BitmapFactory.decodeStream(input);

                } catch (IOException e) {
                }
                return myBitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                if (bitmap != null) {
                    allOrderAttemptImage.add(bitmap);
                    // myCustomPagerAdapter.notifyDataSetChanged();
                }
            }


        }.execute();
    }

    public void showPhotoGaleryImage() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null);
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewPager);

        myCustomPagerAdapter = new OrderAttemptImageAdapter(OrderAttemptDetails.this, null, null, null,
                imagesUriList, descriptionImage, false, newImageUriListString);
        viewPager.setAdapter(myCustomPagerAdapter);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("DONE",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showPhotoGaleryImageAfter();
            }


        });
    }

    public void showEditDeleteImage() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null);
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewPager);

        myCustomPagerAdapter = new OrderAttemptImageAdapter(OrderAttemptDetails.this, allOrderAttemptImage, allOrderAttemptImageId, allOrderAttemptImageDescription,
                null, null, true, newImageUriListString);
        viewPager.setAdapter(myCustomPagerAdapter);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("DONE",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showImageAfterEditDelete();
                LogCustom.i("allOrderAttemptImageSize", "" + allOrderAttemptImage.size());
                LogCustom.i("allOrderAttemptImageId", "" + allOrderAttemptImageId.size());

                LogCustom.i("allOrderAttemptImageId", "" + allOrderAttemptImageId);
                LogCustom.i("allOrderAttemptImageIdBefore", "" + allOrderAttemptImageIdBefore);
                LogCustom.i("allOrderAttemptImageDescription", "" + allOrderAttemptImageDescription);
                LogCustom.i("allOrderAttemptImageDescriptionBefore", "" + allOrderAttemptImageDescriptionBefore);

                allOrderAttemptImageIdNeedToRemove.clear();
                allOrderAttemptImageIdNeedToUpdate.clear();


                for (int i = 0; i < allOrderAttemptImageIdBefore.size(); i++) {
                    if (allOrderAttemptImageId.contains(allOrderAttemptImageIdBefore.get(i))) {
                        LogCustom.i("contain", "" + allOrderAttemptImageIdBefore.get(i));
                        allOrderAttemptImageIdNeedToUpdate.add(allOrderAttemptImageIdBefore.get(i));
                    } else {
                        LogCustom.i("notcontain", "" + allOrderAttemptImageIdBefore.get(i));
                        allOrderAttemptImageIdNeedToRemove.add(allOrderAttemptImageIdBefore.get(i));

                    }
                }

                // boolean aa = Arrays.equals(allOrderAttemptImageId, allOrderAttemptImageIdBefore);
                boolean idNotChanged = allOrderAttemptImageId.equals(allOrderAttemptImageIdBefore);
                boolean descriptionNotChanged = allOrderAttemptImageDescription.equals(allOrderAttemptImageDescriptionBefore);

                if (!idNotChanged || !descriptionNotChanged) {
                    LogCustom.i("idNotChanged", "" + idNotChanged);
                    LogCustom.i("descriptionNotChanged", "" + descriptionNotChanged);
                    removeOrEditImageFromServer();
                }

            }


        });
    }

    void showPhotoGaleryImageAfter() {

        sliderImage.removeAllSliders();

        if (imagesUriList.size() > 0) {
            sliderImage.setVisibility(View.VISIBLE);
        } else {
            sliderImage.setVisibility(View.GONE);
        }

        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();
        for (int i = 0; i < imagesUriList.size(); i++) {
            url_maps.put("" + descriptionImage.get(i), imagesUriList.get(i));
        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(OrderAttemptDetails.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(OrderAttemptDetails.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            sliderImage.addSlider(textSliderView);
        }


        if (totalImage < 2) {
            Log.i("totalImage1 11", "" + totalImage);
            sliderImage.stopAutoCycle();
            sliderImage.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
//
        } else if (totalImage > 1) {
            Log.i("totalImage2 11", "" + totalImage);
            sliderImage.startAutoCycle();
            sliderImage.setPagerTransformer(true, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
        }
    }

    void showImageAfterEditDelete() {

        mDemoSlider.removeAllSliders();
        sliderImage.removeAllSliders();


        LogCustom.i("allOrderAttemptImage11Size", "" + allOrderAttemptImage.size());

        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();
        for (int i = 0; i < allOrderAttemptImage.size(); i++) {
            url_maps.put("" + allOrderAttemptImageDescription.get(i), allOrderAttemptImage.get(i));
        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(OrderAttemptDetails.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(OrderAttemptDetails.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setVisibility(View.VISIBLE);


        if (totalImage < 2) {
            Log.i("totalImage1 22", "" + totalImage);
            mDemoSlider.stopAutoCycle();
            mDemoSlider.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
//
        } else if (totalImage > 1) {
            Log.i("totalImage2 22", "" + totalImage);
            mDemoSlider.startAutoCycle();
            mDemoSlider.setPagerTransformer(true, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
        }


    }

    public void showImageSlider() {


        if (orderAttemptDetail.getmOrderAttemptImages().size() > 0) {

            for (int i = 0; i < orderAttemptDetail.getmOrderAttemptImages().size(); i++) {

                if (noInternetConnection) {

                    urlList.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getBase64());

                    allOrderAttemptImageURL.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getBase64());
                    Log.i("allOrderAttemptImageURL", "" + allOrderAttemptImageURL.size());

                    allOrderAttemptImageDescriptionBefore.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getNote());
                    allOrderAttemptImageDescription.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getNote());

                    allOrderAttemptImageId.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getId());
                    allOrderAttemptImageIdBefore.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getId());

                } else {
                    urlList.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getUrl());

                    allOrderAttemptImageURL.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getUrl());
                    Log.i("allOrderAttemptImageURL", "" + allOrderAttemptImageURL.size());

                    allOrderAttemptImageDescriptionBefore.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getNote());
                    allOrderAttemptImageDescription.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getNote());

                    allOrderAttemptImageId.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getId());
                    allOrderAttemptImageIdBefore.add(orderAttemptDetail.getmOrderAttemptImages().get(i).getId());
                }


            }

            if (!noInternetConnection) {
                loopImageUrlToBitmap();
            }


            Log.i("Orderurllistsize", "2222 url size: " + urlList.size());


            for (int i = 0; i < urlList.size(); i++) {
                try {

                    if (noInternetConnection) {
                        byte[] decodedString = Base64.decode(urlList.get(i), Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();
                        url_maps.put("" + orderAttemptDetail.getmOrderAttemptImages().get(i).getNote(), decodedByte);

                        for (String name : url_maps.keySet()) {
                            TextSliderView textSliderView = new TextSliderView(this);
                            // initialize a SliderLayout
                            textSliderView
                                    .description(name)
                                    .image(url_maps.get(name))
                                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                    .setOnSliderClickListener(this);

                            mDemoSlider.addSlider(textSliderView);
                        }

                    } else {
                        URL url = new URL(urlList.get(i));
                        HashMap<String, String> url_maps = new HashMap<String, String>();
                        url_maps.put("" + orderAttemptDetail.getmOrderAttemptImages().get(i).getNote(), "" + url);
                        Log.i("Orderurl", "2222 bitmap string:" + urlList.get(i));

                        for (String name : url_maps.keySet()) {
                            TextSliderView textSliderView = new TextSliderView(this);
                            // initialize a SliderLayout
                            textSliderView
                                    .description(name)
                                    .image(url_maps.get(name))
                                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                    .setOnSliderClickListener(this);

                            mDemoSlider.addSlider(textSliderView);
                        }
                    }


                    //stop auto cycle slideImage
                    mDemoSlider.stopAutoCycle();
                    mDemoSlider.setPagerTransformer(false, new BaseTransformer() {
                        @Override
                        protected void onTransform(View view, float v) {
                        }
                    });

                } catch (Throwable e) {
                    LogCustom.e(e);
                    return;
                }
            }
        } else {
            mDemoSlider.setBackground(getResources().getDrawable(R.drawable.noimage));
        }
    }

    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("jobStepId", jobStepId);
        ii.putExtra("jobStepStatusId", jobStepStatusId);
        ii.putExtra("OrderId", orderId);
        ii.putExtra("trackingNumber", trackingNumber);
        ii.putExtra("showCompletedFailedButton", showCompletedFailedButton);
        ii.putExtra("startJobStepsButton", startJobStepsButton);
        setResult(Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT, ii);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onRequestInit() {
    }

    @Override
    public void onRequestFinish() {
    }

    public void checkAllInputInserted() {
        if (txtReceivedBy.getText().toString().trim().length() != 0) {
            btnSubmit.setEnabled(true);
            btnSubmit.setTextColor(Color.parseColor("#FFFFFF"));
            btnSubmit.setBackground(getResources().getDrawable(R.drawable.ripple_effect_green));
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setTextColor(Color.parseColor("#EE666666"));
            btnSubmit.setBackgroundColor(Color.parseColor("#D2CFD2"));
        }
    }

    public void removeOrEditImageFromServer() {

        if (jobStepId == 0 || orderId == 0 || jobStepStatusId == 0) {
            // cannot proceed because it 0
            showErrorDialog(getResources().getString(R.string.orderDetailErrorTitle), getResources().getString(R.string.orderDetailError)).show();
        } else {
            try {
                JSONObject jobOrderObject = new JSONObject();
                JSONArray jobOrderIdArray = new JSONArray();


                jobOrderIdArray.put(String.valueOf(orderId));
                jobOrderObject.put("id", jobOrderIdArray);

                jobOrderObject.put("order_status_id", "");

                orderAttemptDetail.getmOrderAttemptImages().clear();
                Log.i("", "image order attempt list 99:" + orderAttemptDetail.getmOrderAttemptImages().size());

                if (allOrderAttemptImageIdNeedToUpdate.size() > 0) {
                    for (int i = 0; i < allOrderAttemptImageIdNeedToUpdate.size(); i++) {
                        OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage(allOrderAttemptImageDescription.get(i), allOrderAttemptImageIdNeedToUpdate.get(i), false, false);
                        orderAttemptDetail.getmOrderAttemptImages().add(imageOrderAttemptImage);
                    }
                }

                if (allOrderAttemptImageIdNeedToRemove.size() > 0) {
                    for (int i = 0; i < allOrderAttemptImageIdNeedToRemove.size(); i++) {
                        OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage("", allOrderAttemptImageIdNeedToRemove.get(i), true, false);
                        orderAttemptDetail.getmOrderAttemptImages().add(imageOrderAttemptImage);
                    }
                }


                JSONObject jobStepObject = new JSONObject();
                JSONArray jobStepArray = new JSONArray();

                jobStepObject.put("job_step_id", jobStepId);
                jobStepObject.put("job_step_status_id", jobStepStatusId);

                JSONArray orderAttemptArray = new JSONArray();

                orderAttemptArray.put(orderAttemptDetail.getJSONObjectEditRemove());
                jobStepObject.put("step_attempts", orderAttemptArray);

                jobStepArray.put(jobStepObject);
                jobOrderObject.put("job_steps", jobStepArray);

                JSONObject dataObject = new JSONObject();
                JSONArray jobDataArray = new JSONArray();
                jobDataArray.put(jobOrderObject);
                dataObject.put("data", jobDataArray);

                Log.i("", "data object:" + dataObject.toString(4));

                removeOrEditImageFromServerBoolean = true;
                updateBatchOrder(dataObject);


            } catch (Exception e) {
                Log.i("", "exception :" + e);

            }
        }


    }

    public void uploadImageBase64ForUrl(final Bitmap bitmapSignature) throws IOException {


        OkHttpRequest okHttpRequest = new OkHttpRequest();

        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        final String signature = ImageOp.convert(bitmapSignature, Bitmap.CompressFormat.JPEG, 80);

        final String[] body = new String[1];
        body[0] = "data:image/png;base64," + signature;

        try {
            jsonObject.put("folder_name", "TestingUploadBase64");
            jsonArray.put(body[0]);
            jsonObject.put("base64_images", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            OkHttpRequest.uploadImageBase64ForUrl(getApplicationContext(), jsonObject.toString()
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess.isShowing()) {
                                            pdUpdateProcess.dismiss();
                                        }
                                    }

                                    if (body == null || body.isEmpty()) {

                                    } else {
                                        try {

                                            final JSONObject data = new JSONObject(body);

                                            final JSONArray resultData = data.getJSONArray("result");
                                            for (int i = 0; i < resultData.length(); i++) {
                                                JSONObject jsonObject2 = (JSONObject) resultData.get(i);

                                                image_normalURL = jsonObject2.getString("image_normal");
                                            }

                                            totalImage = totalImage + 1;
                                            HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();

                                            // descriptionImageString = descriptionImage.get(totalImage - 1);

                                            url_maps.put("" + descriptionImageText, bitmapSignature);

                                            descriptionImage.add(descriptionImageText);
                                            imagesUriList.add(bitmapSignature);
                                            newImageUriListString.add(image_normalURL);

                                            Log.i("InThis", "yes");

                                            for (String name : url_maps.keySet()) {
                                                TextSliderView textSliderView = new TextSliderView(OrderAttemptDetails.this);
                                                // initialize a SliderLayout
                                                textSliderView
                                                        .description(name)
                                                        .image(url_maps.get(name))
                                                        .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                                                        .setOnSliderClickListener(OrderAttemptDetails.this);

                                                textSliderView.bundle(new Bundle());
                                                textSliderView.getBundle()
                                                        .putString("extra", name);


                                                sliderImage.addSlider(textSliderView);
                                            }

                                            sliderImage.setVisibility(View.VISIBLE);


                                            if (totalImage < 2) {
                                                Log.i("totalImage1", "" + totalImage);
                                                sliderImage.stopAutoCycle();
                                                sliderImage.setPagerTransformer(false, new BaseTransformer() {
                                                    @Override
                                                    protected void onTransform(View view, float v) {
                                                    }
                                                });
//
                                            } else if (totalImage > 1) {
                                                Log.i("totalImage2", "" + totalImage);
                                                sliderImage.startAutoCycle();
                                                sliderImage.setPagerTransformer(true, new BaseTransformer() {
                                                    @Override
                                                    protected void onTransform(View view, float v) {
                                                    }
                                                });
                                            }
                                            //Update show image first

                                            requestTakeOtherPhoto = true;
                                            addImages();


                                            LogCustom.i("orderAttemptSieze: ", orderAttemptDetail.getmOrderAttemptImages().size());
                                            LogCustom.i("orderAttempt", orderAttemptDetail.getmOrderAttemptImages().toString());


                                        } catch (JSONException e) {

                                            LogCustom.e(e);

                                            try {

                                            } catch (final Throwable t) {
                                            }

                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess.isShowing()) {
                                            pdUpdateProcess.dismiss();
                                        }
                                    }
                                    errorHandlingFromResponseCode(body, requestCallBackError, responseCode);


                                }
                            });
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void SubmitConfirmation() {

        if (jobStepId == 0 || orderId == 0 || jobStepStatusId == 0) {
            // cannot proceed because it 0
            setEnableAllButton();
            showErrorDialog(getResources().getString(R.string.orderDetailErrorTitle), getResources().getString(R.string.orderDetailError)).show();
        } else {
            try {
                Log.i("", "btn submit click 44");

                JSONObject jobOrderObject = new JSONObject();
                JSONArray jobOrderIdArray = new JSONArray();


                jobOrderIdArray.put(String.valueOf(orderId));
                jobOrderObject.put("id", jobOrderIdArray);
                jobOrderObject.put("order_status_id", "");

                Log.i("", "image uri list:" + imagesUriList.size());
                Log.i("", "image order attempt list 00:" + orderAttemptDetail.getmOrderAttemptImages().size());

                orderAttemptDetail.getmOrderAttemptImages().clear();
                Log.i("", "image order attempt list 99:" + orderAttemptDetail.getmOrderAttemptImages().size());

                if (newImageUriListString.size() > 0) {
                    for (int i = 0; i < newImageUriListString.size(); i++) {
                        OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage(newImageUriListString.get(i), descriptionImage.get(i), false);
                        orderAttemptDetail.getmOrderAttemptImages().add(imageOrderAttemptImage);
                    }
                }

                Location location = getLastLocation();
                Log.i("LastLocation11", "" + location);

                if (location == null) {
                    buildAlertMessageNoGps();

                } else {
                    orderAttemptDetail.setLatitude(location.getLatitude());
                    orderAttemptDetail.setLongitude(location.getLongitude());
                    orderAttemptDetail.setReceived_by(txtReceivedBy.getText().toString());
                    orderAttemptDetail.setNote(txtNote.getText().toString());
                    //  orderAttemptDetail.setNric(txtNRIC.getText().toString());
                    //  orderAttemptDetail.setSrno(txtSerialNumber.getText().toString());

//                    if (orderAttemptDetail.getReason().length() != 0 || !orderAttemptDetail.getReason().equalsIgnoreCase("")) {
//                        orderAttemptDetail.setReason(txtReason.getText().toString());
//                    } else {
//                        orderAttemptDetail.setReason("");
//                    }

                    JSONArray jobStepArray = new JSONArray();
                    JSONObject jobStepObject = new JSONObject();

                    JSONArray orderAttemptArray = new JSONArray();

                    orderAttemptArray.put(orderAttemptDetail.getJSONObjectURLWithID());


                    jobStepObject.put("job_step_id", jobStepId);
                    jobStepObject.put("job_step_status_id", jobStepStatusId);
                    jobStepObject.put("step_attempts", orderAttemptArray);
                    jobStepArray.put(jobStepObject);

                    jobOrderObject.put("job_steps", jobStepArray);


                    JSONObject dataObject = new JSONObject();
                    JSONArray jobDataArray = new JSONArray();
                    jobDataArray.put(jobOrderObject);
                    dataObject.put("data", jobDataArray);

                    Log.i("data object", " : " + dataObject.toString(4));

                    //  updateOrder(dataObject, jobOrderIdArray);
                    updateBatchOrder(dataObject);
                }


            } catch (Exception e) {
                setEnableAllButton();
                Log.i("", "exception :" + e);

            }
        }


    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!requestCallBackError) {

                    unauthorizedNeedToLoginPage = false;

                    try {
                        String errorMessage = "";
                        JSONObject data = null;
                        if (body != null || !body.equalsIgnoreCase("")) {

                            try {
                                data = new JSONObject(body);
                            } catch (Exception e) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                            }

                            if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                if (data.has("error") && !(data.isNull("error"))) {
                                    try {
                                        errorMessage = data.getString("error");
                                    } catch (Exception e) {
                                        JSONObject errorMessageArray = data.getJSONObject("error");
                                        errorMessage = errorMessageArray.getString("error");
                                    }
                                }

                                if (errorMessage.equalsIgnoreCase("")) {
                                    errorMessage = getResources().getString(R.string.pleaseContactAdmin);
                                }

                                showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


                            } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                    if (data.getBoolean("device_not_found")) {
                                        showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                    if (data.getBoolean("device_is_banned")) {
                                        showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                    if (data.getBoolean("unpaid_subscription")) {
                                        showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                    if (data.getBoolean("quota_reached")) {
                                        showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                    if (data.getBoolean("blacklist")) {
                                        showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                            }

                        } else {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                        showErrorInternetDialog().show();
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                    } else {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    }
                    // this for request call back error. Maybe because cannot connect server.
                }
            }
        });


    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */

    public void setEnableAllButton() {
        btnSubmit.setEnabled(true);
    }

    public void updateBatchOrder(JSONObject jsonObject) throws IOException {

        //jobStepId = 183;

        contextProgressDialog.setText(getResources().getString(R.string.updateOrderAttempt));
        pdUpdateProcess.show();

        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.batchUpdate(getApplicationContext(), jsonObject.toString()
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    setEnableAllButton();

                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess.isShowing()) {
                                            pdUpdateProcess.dismiss();
                                        }
                                    }

                                    if (body == null || body.isEmpty()) {

                                    } else {
                                        try {
                                            final JSONObject data = new JSONObject(body);

                                            final JSONArray resultData = data.getJSONArray("result");

                                            LogCustom.i("dataaaBatchUpdate", data.toString());

                                            showSuccessDialog(getResources().getString(R.string.titleSuccess), getResources().getString(R.string.contextAtteptDetailsSuccess)).show();


                                        } catch (JSONException e) {

                                            LogCustom.e(e);

                                            try {

                                            } catch (final Throwable t) {
                                            }

                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            final String jsonResponse = body;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    setEnableAllButton();

                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess.isShowing()) {
                                            pdUpdateProcess.dismiss();
                                        }
                                    }
                                    errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
                                }
                            });
                        }
                    });
        } catch (Exception e) {
            setEnableAllButton();
            e.printStackTrace();
        }


    }

    public void initialize() {
        //Initialize
        txtReceivedBy = findViewById(R.id.txtReceivedBy);
        txtReason = findViewById(R.id.txtReason);
        txtTime = findViewById(R.id.txtTime);
        txtNote = findViewById(R.id.txtNote);
        txtSerialNumber = findViewById(R.id.txtSerialNumber);
        txtNRIC = findViewById(R.id.txtNRIC);
        mDemoSlider = findViewById(R.id.slider);
        sliderImage = findViewById(R.id.sliderImage);
        layoutReceiveBy = findViewById(R.id.layoutReceiveBy);
        layoutReason = findViewById(R.id.layoutReason);
        btnCamera = findViewById(R.id.btnCamera);
        btnSubmit = findViewById(R.id.btnSubmit);
    }

    public void setEditTextEnable() {
        txtReceivedBy.setEnabled(true);
        txtReason.setEnabled(true);
        txtNote.setEnabled(true);
        txtSerialNumber.setEnabled(true);
        txtNRIC.setEnabled(true);
        btnCamera.setVisibility(View.VISIBLE);
        btnSubmit.setVisibility(View.VISIBLE);
    }

    public void setEditTextDisable() {
        txtReceivedBy.setEnabled(false);
        txtReason.setEnabled(false);
        txtNote.setEnabled(false);
        txtSerialNumber.setEnabled(false);
        txtNRIC.setEnabled(false);
        btnCamera.setVisibility(View.GONE);
        btnSubmit.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.menuEditOrderAttempt:
                setEditTextEnable();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        getMenuInflater().inflate(R.menu.menu_order_attempt_details, menu);
        //  return true;


        final MenuItem menuEditOrderAttempt = menu.findItem(R.id.menuEditOrderAttempt);

        menuEditOrderAttempt.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuEditOrderAttempt);
            }
        });

        if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
            // menuEditOrderAttempt.setVisible(true);
            LogCustom.i("jobStepStatusId11", "" + jobStepStatusId);
        } else {
            menuEditOrderAttempt.setVisible(false);
            LogCustom.i("jobStepStatusId22", "" + jobStepStatusId);
        }

        return super.onCreateOptionsMenu(menu);

        //return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        // menu.findItem(R.id.menuEditOrderAttempt).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        Log.i("click slider", "yes" + slider.getScaleType());
        if (slider.getUrl() != null || slider.getScaleType() == BaseSliderView.ScaleType.Fit) {
            Log.i("click first slider", "yes");
            if (allOrderAttemptImageURL.size() > 0) {
                GeneralActivity generalActivity = new GeneralActivity();
                if (!generalActivity.haveInternetConnected(getApplicationContext())) {
                    Log.i("internetConnection", "no");
                    showErrorInternetDialog().show();
                } else {
                    showEditDeleteImage();
                }

            }

        } else {
            Log.i("click second slider", "yes");
            if (imagesUriList.size() > 0) {
                showPhotoGaleryImage();
            }

        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }


    @Override
    public void onLocationChanged(Location location) {

        lat = location.getLatitude();
        lon = location.getLongitude();
        lastLocation = location;

        Log.i("", "on location cahnged lat:" + lat + " lon:" + lon);


    }


    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


    Location lastLocation;
    private GoogleApiClient googleApiClient;
    FusedLocationProviderApi fusedLocationProviderApi;


    LocationRequest locationRequest;

    private void getLocation() {

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(30000);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i(ConfirmationSign.class.getSimpleName(), "Connected to Google Play Services!");

        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {

            lastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);
            Log.i("", "last location in on connected:" + lastLocation);
            if (lastLocation == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            }

        }
    }


    public Location getLastLocation() {

        return lastLocation;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(OrderAttemptDetails.class.getSimpleName(), "Can't connect to Google Play Services!");
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, please turn it on to update job status.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public String getFilename() {
        File file = new File(FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    /* =========  DIALOG POPUP =========== */
    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if (unauthorizedNeedToLoginPage) {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
                                LogCustom.i("isMyServiceRunning", "true");
                                Intent intent = new Intent(getApplicationContext(), ForegroundLocationService.class);
                                stopService(intent);
                            }

                            settings.edit().remove("isOnline").apply();
                            settings.edit().remove("googleToken").apply();
                            settings.edit().putString("user_name", "").commit();
                            unauthorizedNeedToLoginPage = false;

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.putExtra("EXIT", true);
                            startActivity(i);
                        }
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }


    ///success dialog
    SweetAlertDialog showSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = getSuccessDialog(title, context);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText(context)
                .setTitleText(title)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        onBackPressed();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }
    /* =========  DIALOG POPUP =========== */


}

package com.evfy.evfytracker.Activity.JobStepAttempt;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.JobSteps;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class ListJobStepsDetailsWithNoInternet extends AppCompatActivity {

    LinearLayout mLayoutListJobSteps;
    int jobStepId, orderDetailId;
    JobSteps jobSteps;
    TextView contextProgressDialog;
    Dialog pdProcess;
    String trackingNumber;
    boolean showCompletedFailedButton, startJobStepsButton, unauthorizedNeedToLoginPage = false;
    SharedPreferences settings;
    int totalIndex;
    String currentDateString;

    DatabaseHandlerJobs dbUpdateJobs;
    List<OrderAttempt> orderAttemptList = new ArrayList<OrderAttempt>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_job_steps_details);

        initialize();

        dbUpdateJobs = new DatabaseHandlerJobs(this);

        settings = PreferenceManager.getDefaultSharedPreferences(ListJobStepsDetailsWithNoInternet.this);
        unauthorizedNeedToLoginPage = false;


        pdProcess = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        contextProgressDialog.setText(getResources().getString(R.string.updateJobSteps));
        pdProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdProcess.setContentView(content);
        pdProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdProcess.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobStepId = extras.getInt("jobStepId");
            orderDetailId = extras.getInt("OrderId");
            trackingNumber = extras.getString("trackingNumber");
            showCompletedFailedButton = extras.getBoolean("showCompletedFailedButton");
            startJobStepsButton = extras.getBoolean("startJobStepsButton");
        }


        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Attempt in this step");

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        contextProgressDialog.setText(getResources().getString(R.string.loadingListOrderAttempt));
        pdProcess.show();
        getJobStepDetails();


    }

    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("OrderId", orderDetailId);
        ii.putExtra("jobStepId", jobStepId);
        ii.putExtra("trackingNumber", trackingNumber);
        ii.putExtra("showCompletedFailedButton", showCompletedFailedButton);
        ii.putExtra("startJobStepsButton", startJobStepsButton);

        setResult(Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION, ii);
        finish();
        super.onBackPressed();
    }

    public void displayData(JobSteps jobSteps) {
        mLayoutJobStepsDetails(jobSteps);
    }

    public void mLayoutJobStepsDetails(final JobSteps jobSteps) {
        mLayoutListJobSteps.removeAllViews();
        int noBil = 0;
        try {

            Collections.sort(jobSteps.getmOrderAttemptsList(), new Comparator<OrderAttempt>() {

                @Override
                public int compare(final OrderAttempt lhs, final OrderAttempt rhs) {

                    if (lhs == null && rhs == null) return 0;
                    if (lhs == null) return -1;
                    if (rhs == null) return 1;

                    return rhs.getSubmitted_time().compareTo(lhs.getSubmitted_time());
                }

            });

        } catch (final Throwable e) {
            LogCustom.e(e);
        }

        if (jobSteps.getmOrderAttemptsList().size() > 0) {

            totalIndex = 0;

            for (int i = 0; i < jobSteps.getmOrderAttemptsList().size(); i++) {
                final OrderAttempt orderAttempt = jobSteps.getmOrderAttemptsList().get(i);

                if (JavaOp.ifNullThenLog(orderAttempt)) continue;

                final View view = View.inflate(this, R.layout.view_job_step_attempt_details, null);
                mLayoutListJobSteps.addView(view);
                final LinearLayout layoutJobStepAttempt = view.findViewById(R.id.layoutJobStepAttempt);
                final TextView bilJobStepAttempt = view.findViewById(R.id.bilJobStepAttempt);
                final TextView dateJobStepAttempt = view.findViewById(R.id.dateJobStepAttempt);

                totalIndex = totalIndex + 1;

                bilJobStepAttempt.setText("Attempt #" + totalIndex);

                String string = orderAttempt.getSubmitted_time();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                // format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date date = format.parse(string);

                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm:a");
                    sdf.setTimeZone(TimeZone.getDefault());

                    currentDateString = sdf.format(date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dateJobStepAttempt.setText(currentDateString);


                layoutJobStepAttempt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (orderAttempt.getReason().equalsIgnoreCase("")) {
                            Intent i = new Intent(getApplicationContext(), OrderAttemptDetails.class);
                            i.putExtra("jobStepId", jobSteps.getId());
                            i.putExtra("jobStepStatusId", jobSteps.getJob_step_status_id());
                            i.putExtra("orderAttemptID", orderAttempt.getId());
                            i.putExtra("OrderId", jobSteps.getOrderId());
                            i.putExtra("trackingNumber", trackingNumber);
                            i.putExtra("showCompletedFailedButton", showCompletedFailedButton);
                            i.putExtra("startJobStepsButton", startJobStepsButton);
                            i.putExtra("numberOfPage", totalIndex);
                            i.putExtra("noInternetConnection", true);

                            startActivityForResult(i, Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT);
                        } else {
                            Intent i = new Intent(getApplicationContext(), OrderAttemptFailedDetails.class);
                            i.putExtra("jobStepId", jobSteps.getId());
                            i.putExtra("jobStepStatusId", jobSteps.getJob_step_status_id());
                            i.putExtra("orderAttemptID", orderAttempt.getId());
                            i.putExtra("OrderId", jobSteps.getOrderId());
                            i.putExtra("trackingNumber", trackingNumber);
                            i.putExtra("showCompletedFailedButton", showCompletedFailedButton);
                            i.putExtra("startJobStepsButton", startJobStepsButton);

                            i.putExtra("numberOfPage", totalIndex);


                            LogCustom.i("getSubmitted_time", orderAttempt.getSubmitted_time());

                            i.putExtra("reason", orderAttempt.getReason());
                            i.putExtra("time", currentDateString);
                            i.putExtra("noInternetConnection", true);

                            startActivityForResult(i, Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT);
                        }

                    }
                });
            }
        }

    }


    public void getJobStepDetails() {

        orderAttemptList.clear();

        JobSteps jobSteps = new JobSteps();
        jobSteps = dbUpdateJobs.getJobStepsDetailsBasedOnID(jobStepId);
        jobSteps.setmOrderAttemptsList(dbUpdateJobs.getListAllOrderAttemptDetailsNoInternet(jobStepId));

        if (jobSteps.getmOrderAttemptsList().size() > 0) {
            for (int i = 0; i < jobSteps.getmOrderAttemptsList().size(); i++) {
                OrderAttempt orderAttempt = new OrderAttempt();
                orderAttempt = jobSteps.getmOrderAttemptsList().get(i);
                orderAttempt.setmOrderAttemptImages(dbUpdateJobs.getListAllImageDetailsNoInternet(orderAttempt.getId()));

                orderAttemptList.add(orderAttempt);

            }
        }

        jobSteps.setmOrderAttemptsList(null);
        jobSteps.setmOrderAttemptsList(orderAttemptList);
        displayData(jobSteps);

        pdProcess.dismiss();


    }

    public void initialize() {
        mLayoutListJobSteps = findViewById(R.id.mLayoutListJobSteps);
    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */
    // After scan, should check either that step need to sign or not

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("onActivityResult", "on activity result:" + requestCode);

        switch (requestCode) {
            case Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT:

                showCompletedFailedButton = data.getExtras().getBoolean("showCompletedFailedButton");
                startJobStepsButton = data.getExtras().getBoolean("startJobStepsButton");
                trackingNumber = data.getExtras().getString("trackingNumber");
                jobStepId = data.getExtras().getInt("jobStepId");
                orderDetailId = data.getExtras().getInt("OrderId");

                getJobStepDetails();

                break;
        }

    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        getMenuInflater().inflate(R.menu.job_steps_details, menu);
        //  return true;

        return super.onCreateOptionsMenu(menu);

        //return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    /*
     * This func for show alert dialog popup where it success, failed, warning, no internet connection
     * */
    /* ================== SWEET ALERT DIALOG ===================== */
}

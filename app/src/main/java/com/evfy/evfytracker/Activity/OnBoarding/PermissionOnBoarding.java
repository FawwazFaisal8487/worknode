
package com.evfy.evfytracker.Activity.OnBoarding;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.evfy.evfytracker.R;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import agency.tango.materialintroscreen.SlideFragment;

public class PermissionOnBoarding extends SlideFragment {


    LinearLayout btnPermissionCamera, btnPermissionGallery, btnPermissionLocation;
    TextView txtBtnPermissionCamera, txtBtnPermissionGallery, txtBtnPermissionLocation;

    Drawable iconYes, iconNo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_permission_on_boarding, container, false);

        btnPermissionCamera = view.findViewById(R.id.btnPermissionCamera);
        btnPermissionGallery = view.findViewById(R.id.btnPermissionGallery);
        btnPermissionLocation = view.findViewById(R.id.btnPermissionLocation);
        txtBtnPermissionCamera = view.findViewById(R.id.txtBtnPermissionCamera);
        txtBtnPermissionGallery = view.findViewById(R.id.txtBtnPermissionGallery);
        txtBtnPermissionLocation = view.findViewById(R.id.txtBtnPermissionLocation);

        iconYes = getContext().getResources().getDrawable(R.drawable.yespermission);
        iconNo = getContext().getResources().getDrawable(R.drawable.nopermission);


        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.green));
            //btnPermissionCamera.setBackground(getResources().getDrawable(R.drawable.button_shadow));

            txtBtnPermissionCamera.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
            txtBtnPermissionCamera.setText("ALLOWED CAMERA ACCESS");
        } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
            txtBtnPermissionCamera.setText("ALLOW CAMERA ACCESS");
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.green));
            txtBtnPermissionGallery.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
            txtBtnPermissionGallery.setText("ALLOWED GALLERY ACCESS");
        } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
            txtBtnPermissionGallery.setText("ALLOW GALLERY ACCESS");
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.green));
            txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
            txtBtnPermissionLocation.setText("ALLOWED LOCATION SERVICES");
        } else if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
            txtBtnPermissionLocation.setText("ALLOW LOCATION ACCESS");
        }


        btnPermissionCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, 123);
                } else {
                    btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.green));
                    txtBtnPermissionCamera.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                    txtBtnPermissionCamera.setText("ALLOWED CAMERA ACCESS");
                    Toast.makeText(getContext(), "Permission Camera granted.", Toast.LENGTH_SHORT).show();

                }


            }
        });

        btnPermissionGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 456);
                } else {
                    btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.green));
                    txtBtnPermissionGallery.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                    txtBtnPermissionGallery.setText("ALLOWED GALLERY ACCESS");
                    Toast.makeText(getContext(), "Permission Gallery granted.", Toast.LENGTH_SHORT).show();

                }

            }
        });

        btnPermissionLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 789);
                } else {
                    btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.green));
                    txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                    txtBtnPermissionLocation.setText("ALLOWED LOCATION SERVICES");
                    Toast.makeText(getContext(), "Permission Location Services granted.", Toast.LENGTH_SHORT).show();

                }


            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        LogCustom.i("permission", "resultCode" + requestCode);

        if (requestCode == 123) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.green));
                txtBtnPermissionCamera.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                txtBtnPermissionCamera.setText("ALLOWED CAMERA ACCESS");

            } else {

                btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
                txtBtnPermissionCamera.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
                txtBtnPermissionCamera.setText("ALLOW CAMERA ACCESS");

            }

        } else if (requestCode == 456) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.green));
                txtBtnPermissionGallery.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                txtBtnPermissionGallery.setText("ALLOWED GALLERY ACCESS");

            } else {

                btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
                txtBtnPermissionGallery.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
                txtBtnPermissionGallery.setText("ALLOW GALLERY ACCESS");

            }

        } else if (requestCode == 789) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.FOREGROUND_SERVICE
                ) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.FOREGROUND_SERVICE}, 999);
                } else {
                    btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.green));
                    txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                    txtBtnPermissionLocation.setText("ALLOWED LOCATION SERVICES");
                    Toast.makeText(getContext(), "Permission Location Services granted.", Toast.LENGTH_SHORT).show();

                }


            } else {

                btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
                txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
                txtBtnPermissionLocation.setText("ALLOW LOCATION SERVICES");

            }

        } else if (requestCode == 999) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.green));
                txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconYes, null, null, null);
                txtBtnPermissionLocation.setText("ALLOWED LOCATION SERVICES");


            } else {

                btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.incompleteOrange));
                txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
                txtBtnPermissionLocation.setText("ALLOW LOCATION SERVICES");

            }

        }
    }

    @Override
    public int backgroundColor() {
        return R.color.white;
    }

    @Override
    public int buttonsColor() {
        return R.color.darkGreyArrow;
    }

    @Override
    public boolean canMoveFurther() {

        boolean canFinishOnBoarding = false;

//        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
//            btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
//
//            canFinishOnBoarding = false;
//
//        }if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
//            btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
//
//            canFinishOnBoarding = false;
//
//        }if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
//            btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
//
//            canFinishOnBoarding = false;
//        }

        if ((ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            canFinishOnBoarding = true;
        }

        return canFinishOnBoarding;
    }

    @Override
    public String cantMoveFurtherErrorMessage() {
        // return getString(R.string.error_message);

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            btnPermissionCamera.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
            txtBtnPermissionCamera.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
            txtBtnPermissionCamera.setText("ALLOW CAMERA ACCESS");


        }
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            btnPermissionGallery.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
            txtBtnPermissionGallery.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
            txtBtnPermissionGallery.setText("ALLOW GALLERY ACCESS");


        }
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
            txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
            txtBtnPermissionLocation.setText("ALLOW LOCATION SERVICES");
        }
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
            btnPermissionLocation.setBackgroundColor(getResources().getColor(R.color.redFailedPermission));
            txtBtnPermissionLocation.setCompoundDrawablesWithIntrinsicBounds(iconNo, null, null, null);
            txtBtnPermissionLocation.setText("ALLOW LOCATION SERVICES");
        }

        return "You must allow all permissions before proceeding.";
    }
}


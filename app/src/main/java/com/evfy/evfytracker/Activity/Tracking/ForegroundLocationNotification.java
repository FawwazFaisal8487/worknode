package com.evfy.evfytracker.Activity.Tracking;

import android.app.Activity;

public class ForegroundLocationNotification extends Activity {

    @Override
    public void onResume() {

        // finish this activity and return to the last activity
        finish();

        super.onResume();
    }
}
package com.evfy.evfytracker.Activity.HomeActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.evfy.evfytracker.Activity.JobStepActivity.ManyJobStepsActivity;
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.MyService;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.ToolbarNavigationUtil;
import com.evfy.evfytracker.Utils.BadgeDrawable;
import com.evfy.evfytracker.fragment.AboutFragment;
import com.evfy.evfytracker.fragment.BalanceEnquiryFragment;
import com.evfy.evfytracker.fragment.JobFragment;
import com.evfy.evfytracker.fragment.NotificationListingFragment;
import com.evfy.evfytracker.fragment.ScanDoFragment;
import com.github.javiersantos.appupdater.AppUpdater;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.lkh012349s.mobileprinter.Utils.LogCustom;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.BadgeStyle;
import com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.Nameable;
import com.mikepenz.materialdrawer.util.RecyclerViewCacheUtil;
import com.shasin.notificationbanner.Banner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.leolin.shortcutbadger.ShortcutBadger;

public class DrawerActivity extends AppCompatActivity {


    /* ====== DECLARATIONS ====== */

    /* ====== DECLARATIONS ====== */

    /* ====== INTEGER PARAMS ======= */
    private static final int PROFILE_SETTING = 1;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    /* ====== INTEGER PARAMS ======= */

    public static final long MIN_INTERVAL = 3000L;


    //save our header or result
    private AccountHeader headerResult = null;
    private Drawer result = null;

    public static final String ACTION_REFRESH = "ACTION_REFRESH";


    /* ===== FRAGMENT PARAMS ====== */
    JobFragment jobFragment = new JobFragment();
    AboutFragment aboutFragment = new AboutFragment();
    ScanDoFragment scanDoFragment = new ScanDoFragment();
    NotificationListingFragment notificationListingFragment = new NotificationListingFragment();
    BalanceEnquiryFragment balanceEnquiryFragment = new BalanceEnquiryFragment();
    /* ===== FRAGMENT PARAMS ====== */

    /* ===== BOOLEAN PARAMS ===== */
    boolean isFactory = false, isWMS = false, connectionInternet, currentlyTracking;
    /* ===== BOOLEAN PARAMS ===== */

    /* ===== STRING PARAMS ===== */
    String bodyContext, orderId;
    /* ===== STRING PARAMS ===== */

    Switch switchAvaibility;
    Dialog pdUpdateProcess;
    TextView contextProgressDialog;
    public static final String RECEIVE_JSON = "com.thetwon.whereareyou.RECEIVE_JSON";
    Double Latitude, Longitude;
    BroadcastReceiver receiver;
    SharedPreferences settings;
    boolean unauthorizedNeedToLoginPage = false;
    View rootView;
    private FusedLocationProviderClient mFusedLocationClient;
    private final LocationRequest mLocationRequest = LocationRequest.create();
    private Location mLocation;
    private LocationCallback mLocationCallback;
    public static final String ACTION_DISPLAY_IN_APP_NOTIFICATION = "displayInAppNotification";


    DatabaseHandlerJobs dbUpdateJobs;
    public static final String ACTION_REFRESH_NOTIFICATION_SIDE_MENU = "refreshNotificationSideMenu";

    public static final String ACTION_TOGGLE_ONLINE_OFFLINE = "toggleIsOnlineOffline";

    BroadcastReceiver mReceiverRefreshNotificationSideMenu, mReceiverToggleOnlineOffline;

    private Drawable setBadgeCount(Context context, int res, int badgeCount) {
        LayerDrawable icon = (LayerDrawable) ContextCompat.getDrawable(context, R.drawable.ic_badge_drawable);
        Drawable mainIcon = ContextCompat.getDrawable(context, res);
        BadgeDrawable badge = new BadgeDrawable(context);
        badge.setCount(String.valueOf(badgeCount));
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
        icon.setDrawableByLayerId(R.id.ic_main_icon, mainIcon);

        return icon;
    }

    private final BroadcastReceiver mReceiverInAppNotification = new BroadcastReceiver() {

        final long timeLastReceival = 0L;

        @Override
        public void onReceive(final Context context, final Intent intent) {
            Bundle extras = intent.getExtras();
            Log.i("EXTRAS BOOM", "extras in drawer activity: " + extras);
            if (extras != null) {

                String OrderIdfromPushNotification = extras.getString("OrderId");
                String OrderNumberfromPushNotification = extras.getString("order_number");
                String title = extras.getString("title");
                String message = extras.getString("message");
                Banner.make(rootView, getBaseContext(), Banner.INFO, title, message, Banner.TOP, 4000).show();


            }

        }

    };


    private boolean checkIfGooglePlayEnabled() {
        return GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS;
    }


    public void checkingDriverStatusAndLocationStatus() {
        //if status online, location must on. else must be offline
        boolean isOnline = settings.getBoolean("isOnline", true);
        if (isOnline) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true");

                SharedPreferences.Editor editors = settings.edit();
                editors.putBoolean("isOnline", true).apply();

                switchAvaibility.setText("Online");
                switchAvaibility.setChecked(true);

                startLocationServices();

            } else {
                // GPS not open
                LogCustom.i("GPS Not Enabled in your device true");
                SharedPreferences.Editor editors = settings.edit();
                editors.putBoolean("isOnline", false).apply();

                switchAvaibility.setText("Offline");
                switchAvaibility.setChecked(false);

                LogCustom.i("stopLocationServices11");
                stopLocationServices();
            }

        } else {
            SharedPreferences.Editor editors = settings.edit();
            editors.putBoolean("isOnline", false).apply();

            LogCustom.i("stopLocationServices22");
            stopLocationServices();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("requestCode", "onActivity " + requestCode);
        Log.i("resultCode", "onActivity " + resultCode);

        if (requestCode == Constants.OPEN_INTERNET_CONNECTION) {
            driverLogoutProcess();
        }

        if (requestCode == Constants.INTENT_ACTIVITY_GPS_SETTINGS) {
            checkingDriverStatusAndLocationStatus();
        }
    }

    private final OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
            if (drawerItem instanceof Nameable) {
                Log.i("material-drawer", "DrawerItem: " + ((Nameable) drawerItem).getName() + " - toggleChecked: " + isChecked);
            } else {
                Log.i("material-drawer", "toggleChecked: " + isChecked);
            }
        }
    };

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        outState = headerResult.saveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    public void updateStatusOnlineOffline(boolean is_online) {
        try {
            JSONObject jsonObject = new JSONObject();

            LogCustom.i("updateStatusOnlineOffline", "is_online : " + is_online);


            jsonObject.put("is_online", is_online);
            jsonObject.put("latitude", "");
            jsonObject.put("longitude", "");


            OkHttpRequest okHttpRequest = new OkHttpRequest();

            try {
                OkHttpRequest.driverUpdateProfile(getApplicationContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        if (body == null || body.isEmpty()) {
                                            LogCustom.i("valueIsNull", "yes");
                                        } else {
                                            LogCustom.i("update profile is success", "yes");
                                        }

                                    }
                                });


                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        //	errorHandlingFromResponseCode(body,requestCallBackError,responseCode);
                                    }
                                });


                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    public void driverLogoutWithChangeToOffline() {
        try {


            JSONObject jsonObject = new JSONObject();
            jsonObject.put("is_online", false);
            jsonObject.put("latitude", String.valueOf(mLocation.getLatitude()));
            jsonObject.put("longitude", String.valueOf(mLocation.getLongitude()));
//            jsonObject.put("worker_id",PreferenceManager.getDefaultSharedPreferences(this).getString("access_token",""));
            OkHttpRequest okHttpRequest = new OkHttpRequest();

            pdUpdateProcess.show();

            try {
                OkHttpRequest.driverUpdateLocation(getApplicationContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {
                                pdUpdateProcess.dismiss();
                                if (body == null || body.isEmpty()) {
                                    LogCustom.i("valueIsNull", "yes");
                                } else {

                                    try {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.post(new Runnable() {
                                            public void run() {
                                                // Toast.makeText(getApplicationContext(), "Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                                driverLogoutProcess();
                                            }
                                        });
                                        final JSONObject data = new JSONObject(body);
                                        Log.d("HTTP request done", " : ");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
                                pdUpdateProcess.dismiss();
                                if (!requestCallBackError) {
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    handler.post(new Runnable() {
                                        public void run() {
                                            Toast.makeText(getApplicationContext(), body, Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    public void driverLogoutProcess() {
        try {
            JSONObject jsonObject = new JSONObject();

            final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(DrawerActivity.this);

            final String[] googleToken = {settings.getString("googleToken", "")};
            LogCustom.i("googleTokenLogout", "" + googleToken[0]);

            if (googleToken[0].equalsIgnoreCase("")) {
                //			String token="";
                try {

                    Log.i("tokenObject", "tokenObject");
                    FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (task.isSuccessful()) {

                                if (task.isSuccessful()) {
                                    JSONObject tokenObject = null;
                                    try {
                                        tokenObject = new JSONObject(task.getResult().getToken());
                                        googleToken[0] = tokenObject.getString("token");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    });


                } catch (Exception e) {
                }
            }

            jsonObject.put("google_token", googleToken[0]);


            OkHttpRequest okHttpRequest = new OkHttpRequest();

            try {
                OkHttpRequest.driverLogout(getApplicationContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (pdUpdateProcess != null) {
                                            if (pdUpdateProcess.isShowing()) {
                                                pdUpdateProcess.dismiss();
                                            }
                                        }

                                        if (body == null || body.isEmpty()) {
                                            LogCustom.i("valueIsNull", "yes");
                                        } else {
                                            final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(DrawerActivity.this);
                                            SharedPreferences.Editor editors = settings.edit();
                                            editors.putString("user_name", "").commit();
                                            editors.putBoolean("isOnline", false).commit();
                                            editors.remove("googleToken").commit();
                                            editors.apply();

                                            GeneralActivity generalActivity = new GeneralActivity();
                                            if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
                                                LogCustom.i("isMyServiceRunning", "true");
                                                Intent intent = new Intent(DrawerActivity.this, ForegroundLocationService.class);
                                                stopService(intent);
                                            }

                                            Intent i = new Intent(DrawerActivity.this, LoginActivity.class);
                                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                            i.putExtra("EXIT", true);
                                            startActivity(i);
                                        }

                                    }
                                });


                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (pdUpdateProcess != null) {
                                            if (pdUpdateProcess.isShowing()) {
                                                pdUpdateProcess.dismiss();
                                            }
                                        }

                                        errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
                                    }
                                });


                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!requestCallBackError) {
                    try {

                        unauthorizedNeedToLoginPage = false;

                        String errorMessage = "";
                        JSONObject data = null;
                        if (body != null || !body.equalsIgnoreCase("")) {

                            try {
                                data = new JSONObject(body);
                            } catch (Exception e) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                            }

                            if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                if (data.has("error") && !(data.isNull("error"))) {
                                    try {
                                        //errorMessage = data.getString("error");
                                        errorMessage = getResources().getString(R.string.errorMessageUsernamePassword);
                                    } catch (Exception e) {
                                        JSONObject errorMessageArray = data.getJSONObject("error");
                                        errorMessage = errorMessageArray.getString("error");
                                    }
                                }

                                if (errorMessage.equalsIgnoreCase("")) {
                                    errorMessage = getResources().getString(R.string.pleaseContactAdmin);
                                }

                                showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


                            } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                    if (data.getBoolean("device_not_found")) {
                                        showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                    if (data.getBoolean("device_is_banned")) {
                                        showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                    if (data.getBoolean("unpaid_subscription")) {
                                        showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                    if (data.getBoolean("quota_reached")) {
                                        showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                    if (data.getBoolean("blacklist")) {
                                        showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                            }

                        } else {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                        showErrorInternetDialog().show();
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                    } else {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    }
                    // this for request call back error. Maybe because cannot connect server.
                }
            }
        });


    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */

    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if (unauthorizedNeedToLoginPage) {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
                                LogCustom.i("isMyServiceRunning", "true");
                                Intent intent = new Intent(getApplicationContext(), ForegroundLocationService.class);
                                stopService(intent);
                            }

                            settings.edit().remove("isOnline").apply();
                            settings.edit().remove("googleToken").apply();
                            settings.edit().putString("user_name", "").commit();
                            unauthorizedNeedToLoginPage = false;

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.putExtra("EXIT", true);
                            startActivity(i);
                        }
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorServerDialog(String contextText, String titleText) {
        final SweetAlertDialog sweetAlertDialog = getErrorServerDialog(contextText, titleText);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorServerDialog(String contextText, String titleText) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextText)
                .setTitleText(titleText)
                .setConfirmText("Ok")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    public void setSideMenuNotification() {
        if (dbUpdateJobs.getTotalNotificationUnRead() > 0) {
            getSupportActionBar().setHomeAsUpIndicator(setBadgeCount(this, R.drawable.menu_drawer, dbUpdateJobs.getTotalNotificationUnRead()));
        } else {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_drawer);
        }
    }


    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppUpdater appUpdater = new AppUpdater(this);
        appUpdater.start();

        //final SharedPreferences settings = getSharedPreferences( Constants.PREFS_NAME, 0 );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_MAX));
        }

        LocalBroadcastManager.getInstance(getBaseContext())
                .registerReceiver(mReceiverInAppNotification, new IntentFilter(ACTION_DISPLAY_IN_APP_NOTIFICATION));

        DrawerActivity.this.startService(new Intent(this, MyService.class));

        settings = PreferenceManager.getDefaultSharedPreferences(DrawerActivity.this);
        unauthorizedNeedToLoginPage = false;

        dbUpdateJobs = new DatabaseHandlerJobs(this);


        pdUpdateProcess = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        contextProgressDialog.setText(getResources().getString(R.string.logoutLoading));
        pdUpdateProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdUpdateProcess.setContentView(content);
        pdUpdateProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdUpdateProcess.setCancelable(false);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest.setFastestInterval(5);
        mLocationRequest.setInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(15);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (ActivityCompat.checkSelfPermission(DrawerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    mLocation = locationResult.getLastLocation();
                    Toast.makeText(DrawerActivity.this, "Please enable location permissions", Toast.LENGTH_SHORT).show();
                }
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.getMainLooper());
            }
        };


        int locationPermission = ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        int locationPermission2 = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        int readPhonePermission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);

        int writeExternalStorage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readExternalStorage = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE);
        int accessNetworkState = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_NETWORK_STATE);

        int accessWifiState = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_WIFI_STATE);
        int callState = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        int cameraState = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        List<String> listPermissionsNeeded = new ArrayList<>();
        boolean wifiAndNetworkPermission = true;

        if (accessNetworkState != PackageManager.PERMISSION_GRANTED) {
            wifiAndNetworkPermission = false;
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_NETWORK_STATE);
        } else {

        }
        if (accessWifiState != PackageManager.PERMISSION_GRANTED) {
            wifiAndNetworkPermission = false;
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_WIFI_STATE);
        }

        boolean locationEnabled = true;
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            locationEnabled = false;
        }

        if (locationPermission2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            locationEnabled = false;
        }

        if (readPhonePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_PHONE_STATE);
        }
        if (writeExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readExternalStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (callState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }

        if (cameraState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
        }

        isFactory = settings.getBoolean("is_factory", false);
        final String accessToken = settings.getString("access_token", "");
        LogCustom.i(accessToken, "accessToken");

        setContentView(R.layout.activity_sample_dark_toolbar);
        FragmentManager manager = this.getSupportFragmentManager();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        //Remove line to test RTL support
        //getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

        // Handle Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        rootView = findViewById(R.id.frame_container);


        setSupportActionBar(toolbar);

        //toolbar.setNavigationIcon(R.drawable.menu_drawer);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (result != null && result.isDrawerOpen()) {
                    result.closeDrawer();
                } else if (result != null && !result.isDrawerOpen()) {
                    result.openDrawer();
                }
            }
        });

        LogCustom.i("getTotalNotificationUnRead", dbUpdateJobs.getTotalNotificationUnRead());


        mReceiverRefreshNotificationSideMenu = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LogCustom.i("refresh notification side menu true");

                // this one is badges for icon side menu (top)
                setSideMenuNotification();

                // this one for badge for app icon
                ShortcutBadger.applyCount(getApplicationContext(), dbUpdateJobs.getTotalNotificationUnRead());

                if (dbUpdateJobs.getTotalNotificationUnRead() > 0) {
                    result.updateItemAtPosition(new PrimaryDrawerItem().withBadge("" + dbUpdateJobs.getTotalNotificationUnRead()).withBadgeStyle(new BadgeStyle().withColor(getResources().getColor(R.color.red))).withName("Notification").withIcon(R.drawable.notification_menu).withIdentifier(4).withSelectable(true).withSelectedIcon(R.drawable.notification_menu_selected).withSelectedTextColor(getResources().getColor(R.color.bgColorPrimary)),
                            2);
                } else {
                    result.updateItemAtPosition(new PrimaryDrawerItem().withName("Notification").withIcon(R.drawable.notification_menu).withIdentifier(4).withSelectable(true).withSelectedIcon(R.drawable.notification_menu_selected).withSelectedTextColor(getResources().getColor(R.color.bgColorPrimary)),
                            2);
                }
                //this one for side menu item
                //result.updateBadge(4,new StringHolder(dbUpdateJobs.getTotalNotificationUnRead() + ""));
            }
        };

        mReceiverToggleOnlineOffline = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                LogCustom.i("mReceiverToggleOnlineOffline true");

                boolean isOnline = settings.getBoolean("isOnline", true);
                if (isOnline) {
                    switchAvaibility.setText("Online");
                    switchAvaibility.setChecked(true);
                } else {
                    switchAvaibility.setText("Offline");
                    switchAvaibility.setChecked(false);
                }
            }
        };

        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(mReceiverToggleOnlineOffline, new IntentFilter(ACTION_TOGGLE_ONLINE_OFFLINE));


        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(mReceiverRefreshNotificationSideMenu, new IntentFilter(ACTION_REFRESH_NOTIFICATION_SIDE_MENU));

        if (dbUpdateJobs.getTotalNotificationUnRead() > 0) {
            getSupportActionBar().setHomeAsUpIndicator(setBadgeCount(this, R.drawable.menu_drawer, dbUpdateJobs.getTotalNotificationUnRead()));
        } else {

            getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_drawer);

        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowCustomEnabled(true);   // enable overriding the default toolbar layout
        // getSupportActionBar().setDisplayShowTitleEnabled(false);//

        Log.i("toolbar navigation icon", "navigation icon" + ToolbarNavigationUtil.getToolbarNavigationIcon(toolbar));

//		BadgeFactory.create(this)
//				.setTextColor(Color.WHITE)
//				.setWidthAndHeight(25,25)
//				.setBadgeBackground(Color.RED)
//				.setTextSize(10)
//				.setBadgeGravity(Gravity.RIGHT|Gravity.TOP)
//				.setBadgeCount(20)
//				.setShape(BadgeView.SHAPE_CIRCLE)
//				.setSpace(10,10)
//				.bind(ToolbarNavigationUtil.getToolbarNavigationIcon(toolbar).getRootView());

        //BadgeFactory.createDot(this).setBadgeCount(20).bind((AppCompatImageButton)ToolbarNavigationUtil.getToolbarNavigationIcon(toolbar).view);
        //  new QBadgeView(getBaseContext()).bindTarget(ToolbarNavigationUtil.getToolbarNavigationIcon(toolbar)).setBadgeNumber(5);

        // Create a few sample profile
        // NOTE you have to define the loader logic too. See the CustomApplication for more details
        //final IProfile profile = new ProfileDrawerItem().withName(userName).withIcon(R.drawable.knocknock_icon).withIdentifier(100).withTextColor(R
        // .color.black);


        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder().withActivity(this)
                .withTextColor(getResources().getColor(R.color.black))
                .withHeaderBackground(R.color.white)
                .withAlternativeProfileHeaderSwitching(false)
                .withSavedInstance(savedInstanceState)
                .build();


        //Create the drawer
        result = new DrawerBuilder().withActivity(this)
                .withHasStableIds(true)
                .withActionBarDrawerToggle(false)
                .withHeader(R.layout.view_drawer_header).addDrawerItems(
                        new PrimaryDrawerItem().withName("My Jobs").withIcon(R.drawable.jobs_menu).withIdentifier(1).withSelectable(true).withSelectedIcon(R.drawable.jobs_menu_selected).withSelectedTextColor(getResources().getColor(R.color.bgColorPrimary)),

                        new PrimaryDrawerItem().withName("Notification").withIcon(R.drawable.notification_menu).withIdentifier(4).withSelectable(true).withSelectedIcon(R.drawable.notification_menu_selected).withSelectedTextColor(getResources().getColor(R.color.bgColorPrimary)),

                        new PrimaryDrawerItem().withName("About").withIcon(R.drawable.about_menu).withIdentifier(2).withSelectable(true).withSelectedIcon(R.drawable.about_menu_selected).withSelectedTextColor(getResources().getColor(R.color.bgColorPrimary)),

                        new PrimaryDrawerItem().withName("Logout").withIcon(R.drawable.logout_menu).withIdentifier(3).withSelectable(true).withSelectedIcon(R.drawable.logout_menu_selected).withSelectedTextColor(getResources().getColor(R.color.bgColorPrimary))

                ) // add the items we want to use with our Drawer
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

                                                   @Override
                                                   public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                                       //check if the drawerItem is set.
                                                       //there are different reasons for the drawerItem to be null
                                                       //--> click on the header
                                                       //--> click on the footer
                                                       //those items don't contain a drawerItem
                                                       drawerItem.withSetSelected(true);
                                                       FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

                                                       if (drawerItem != null) {
                                                           Intent intent = null;
                                                           if (drawerItem.getIdentifier() == 1) {

                                                               ft.replace(R.id.frame_container, jobFragment);
                                                               if (!DrawerActivity.this.isFinishing()) {
                                                                   ft.commitAllowingStateLoss();
                                                               }
                                                           } else if (drawerItem.getIdentifier() == 2) {
                                                               ft.replace(R.id.frame_container, aboutFragment);
                                                               if (!DrawerActivity.this.isFinishing()) {
                                                                   ft.commitNow();

                                                               }
                                                           } else if (drawerItem.getIdentifier() == 4) {
                                                               ft.replace(R.id.frame_container, notificationListingFragment);
                                                               if (!DrawerActivity.this.isFinishing()) {
                                                                   ft.commitNow();

                                                               }
                                                           } else if (drawerItem.getIdentifier() == 3) {
                                                               //Pushbots.sharedInstance().untag( settings.getString( "worker_id", "" ) );
                                                               final SweetAlertDialog tempAlert = new SweetAlertDialog(DrawerActivity.this,
                                                                       SweetAlertDialog
                                                                               .WARNING_TYPE);
                                                               tempAlert.setTitleText("Logout");
                                                               tempAlert.setContentText("Are you sure you want to logout?");
                                                               tempAlert.setCancelText("No");
                                                               tempAlert.setConfirmText("Yes");
                                                               tempAlert.showCancelButton(true);
                                                               tempAlert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                                                   @Override
                                                                   public void onClick(SweetAlertDialog sweetAlertDialog) {
                                                                       tempAlert.dismiss();
                                                                       //SharedPreferences settings = getSharedPreferences( Constants.PREFS_NAME, 0 );

                                                                       GeneralActivity generalActivity = new GeneralActivity();
                                                                       if (!generalActivity.haveInternetConnected(getApplicationContext())) {
                                                                           showErrorInternetDialog().show();
                                                                       } else {
                                                                           driverLogoutWithChangeToOffline();
                                                                       }

                                                                   }
                                                               });
                                                               tempAlert.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                                                   @Override
                                                                   public void onClick(SweetAlertDialog sDialog) {
                                                                       sDialog.cancel();
                                                                   }
                                                               });
                                                               tempAlert.show();
                                                           }
                                                       }

                                                       result.closeDrawer();
                                                       return false;
                                                   }
                                               }

                ).withSavedInstance(savedInstanceState).build();


        //if you have many different types of DrawerItems you can magically pre-cache those items to get a better scroll performance
        //make sure to init the cache after the DrawerBuilder was created as this will first clear the cache to make sure no old elements are in
        RecyclerViewCacheUtil.getInstance().
                withCacheSize(2).init(result);

        //only set the active selection or active profile if we do not recreate the activity
        if (savedInstanceState == null) {
            // set the selection to the item with the identifier 11
            result.setSelection(1, false);

            //set the active profile
            //headerResult.setActiveProfile(profile);
        }

        String userName = settings.getString("user_name", "");


        Intent ii = new Intent(DrawerActivity.ACTION_REFRESH_NOTIFICATION_SIDE_MENU);
        LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(ii);


        TextView driverName = result.getHeader().findViewById(R.id.driverName);
        driverName.setText(userName);

        switchAvaibility = result.getHeader().findViewById(R.id.switchAvaibility);

        final SharedPreferences.Editor editors = settings.edit();

        boolean isOnline = settings.getBoolean("isOnline", true);
        if (isOnline) {
            switchAvaibility.setText("Online");
            switchAvaibility.setChecked(true);

            settings.edit().putBoolean("isOnline", true).commit();
            editors.putBoolean("isOnline", true).commit();
            editors.apply();

            checkingDriverStatusAndLocationStatusToggle();

        } else {
            switchAvaibility.setText("Offline");
            switchAvaibility.setChecked(false);

            settings.edit().putBoolean("isOnline", false).commit();
            editors.putBoolean("isOnline", false).commit();
            editors.apply();

            checkingDriverStatusAndLocationStatusToggle();
        }


        switchAvaibility.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                GeneralActivity generalActivity = new GeneralActivity();
                if (isChecked) {
                    switchAvaibility.setText("Online");
                    settings.edit().putBoolean("isOnline", true).commit();
                    editors.putBoolean("isOnline", true).commit();
                    editors.apply();

                    checkingDriverStatusAndLocationStatusToggle();

                    LogCustom.i("isChecked1", "is :" + isChecked);


                } else {
                    switchAvaibility.setText("Offline");
                    LogCustom.i("isChecked2", "is :" + isChecked);
                    settings.edit().putBoolean("isOnline", false).commit();
                    editors.putBoolean("isOnline", false).commit();
                    editors.apply();

                    LogCustom.i("stopLocationServices33");
                    stopLocationServices();
                }
            }
        });


        //result.updateBadge(4, new StringHolder(10 + ""));
        ft.replace(R.id.frame_container, jobFragment);

        if (!DrawerActivity.this.isFinishing()) ft.commitAllowingStateLoss();
        Bundle extras = getIntent().getExtras();
        Log.i("EXTRAS BOOM", "extras in drawer activity: " + extras);
        if (extras != null) {

            String OrderIdfromPushNotification = extras.getString("OrderId");
            String OrderNumberfromPushNotification = extras.getString("order_number");
            boolean fromPushNotification = extras.getBoolean("fromPushNotification", false);
            boolean isJobRemoved = extras.getBoolean("isJobRemoved", false);
            boolean job_destroyed = extras.getBoolean("job_destroyed", false);
            if (!fromPushNotification) {
                ft.replace(R.id.frame_container, jobFragment);
            } else {
                ft.replace(R.id.frame_container, notificationListingFragment);

                Log.i("ORDERIDFrom", "order id in drawer activity" + OrderIdfromPushNotification);
                try {
                    if (OrderIdfromPushNotification != null && !OrderIdfromPushNotification.isEmpty() && !OrderIdfromPushNotification.equals("null")) {
                        GeneralActivity generalActivity = new GeneralActivity();
                        if (generalActivity.haveInternetConnected(getApplicationContext())) {
                            Log.i("internetConnection", "no");

                            if (isJobRemoved || job_destroyed) {
                                refreshJobDialog(getResources().getString(R.string.jobRemovedTitle), getResources().getString(R.string.jobRemovedContent),
                                        getResources().getString(R.string.jobRemovedButtonText), isJobRemoved, job_destroyed, getResources().getDrawable(R.drawable.icon_trash_popup));
                            } else {
                                Intent intent1 = new Intent(this, ManyJobStepsActivity.class);
                                intent1.putExtra("OrderId", Integer.parseInt(OrderIdfromPushNotification));
                                intent1.putExtra("order_number", OrderNumberfromPushNotification);
                                intent1.putExtra("OpenPagefromPushNotification", true);
                                startActivity(intent1);
                            }


                        } else {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                            alertDialogBuilder.setTitle("No Internet Connection");
                            alertDialogBuilder.setMessage("You are offline please check your internet connection");
//			Toast.makeText(MainActivity.this, "No Internet Connection", Toast.LENGTH_LONG).show();
                            alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    //Toast.makeText(MainActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }

                    }
                } catch (NumberFormatException e) {

                }
            }


        }

        //googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();

        //	getLocation();


        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        mLocation = location;
                        LogCustom.i("mFusedLocationClient", "mFusedLocationClient true");

                        if (location != null) {
                            // Logic to handle location object
                            LogCustom.i("mFusedLocationClient", "location not null");
                            boolean firsTimeTracking = settings.getBoolean("firstTimeUpdateLocation", true);
                            if (firsTimeTracking) {
                                Log.i("changeLocation", "11111");

                                LogCustom.i("firstTimeUpdateLocation", "lastLocation true");
                                uploadLocationToServer(location);
                                SharedPreferences.Editor editors = settings.edit();
                                editors.putBoolean("firstTimeUpdateLocation", false).commit();
                                editors.apply();
                            }
                        } else {
                            LogCustom.i("firstTimeUpdateLocation", "lastLocation false");
                        }
                    }
                });


    }

    SweetAlertDialog refreshJobDialog(String titleError, String contextError, String buttonText, boolean isJobRemoved, boolean job_destroyed, Drawable icon) {
        final SweetAlertDialog sweetAlertDialog = getRefreshJobDialog(titleError, contextError, buttonText, isJobRemoved, job_destroyed, icon);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getRefreshJobDialog(String titleError, String contextError, String buttonText, final boolean isJobRemoved, final boolean job_destroyed, Drawable icon) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                .setCustomImage(icon)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText(buttonText)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

//						if(isJobRemoved || job_destroyed){
//							onBackPressed();
//						}


                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    public void stopLocationServices() {

        switchAvaibility.setText("Offline");
        switchAvaibility.setChecked(false);

        GeneralActivity generalActivity = new GeneralActivity();
        if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
            LogCustom.i("isMyServiceRunning need to stop", "true");
            Intent intent = new Intent(DrawerActivity.this, ForegroundLocationService.class);
            stopService(intent);
        } else {
            LogCustom.i("isMyServiceRunning not need to stop", "true");
        }

        updateStatusOnlineOffline(false);
    }

    public void startLocationServices() {

        switchAvaibility.setText("Online");
        switchAvaibility.setChecked(true);

        GeneralActivity generalActivity = new GeneralActivity();
        if (!generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
            LogCustom.i("isMyServiceRunning need to start", "true");
            Intent intent = new Intent(DrawerActivity.this, ForegroundLocationService.class);
            startService(intent);

        } else {
            LogCustom.i("isMyServiceRunning already, not need to start", "true");
        }

        updateStatusOnlineOffline(true);
    }

    public void checkingDriverStatusAndLocationStatusToggle() {
        //if status online, location must on. else must be offline
        boolean isOnline = settings.getBoolean("isOnline", true);
        if (isOnline) {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true");
                SharedPreferences.Editor editors = settings.edit();
                editors.putBoolean("isOnline", true).apply();

                startLocationServices();

            } else {
                showGPSDisabledAlertToUser();
            }

        } else {
            SharedPreferences.Editor editors = settings.edit();
            editors.putBoolean("isOnline", false).apply();
            LogCustom.i("stopLocationServices44");
            stopLocationServices();
        }
    }

    private void showGPSDisabledAlertToUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(callGPSSettingIntent, Constants.INTENT_ACTIVITY_GPS_SETTINGS);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        SharedPreferences.Editor editors = settings.edit();
                        editors.putBoolean("isOnline", false).apply();

                        LogCustom.i("stopLocationServices55");
                        stopLocationServices();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void uploadLocationToServer(Location location) {
        try {

            Log.i("upload location", "upload location");
            boolean isOnline = settings.getBoolean("isOnline", true);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("is_online", isOnline);
            jsonObject.put("latitude", String.valueOf(location.getLatitude()))
                    //.put("worker_id",resource_owner_id)
                    .put("longitude", String.valueOf(location.getLongitude()));

            OkHttpRequest okHttpRequest = new OkHttpRequest();

            try {
                OkHttpRequest.driverUpdateLocation(getApplicationContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {
                                Log.i("upload location", "upload location body:" + body);

                                if (body == null || body.isEmpty()) {
                                    LogCustom.i("valueIsNull", "yes");
                                } else {

                                    try {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.post(new Runnable() {
                                            public void run() {
                                                // Toast.makeText(getApplicationContext(), "Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                            }
                                        });
                                        final JSONObject data = new JSONObject(body);
                                        Log.d("HTTP request done", " : ");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
                                if (!requestCallBackError) {
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    handler.post(new Runnable() {
                                        public void run() {
                                            //  Toast.makeText(getApplicationContext(), "False Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }


    public void isConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                connectionInternet = true;
                // msg="You are connected to a WiFi Network";
            }
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                connectionInternet = true;
                //msg="You are connected to a Mobile Network";
            }
        } else {
            //msg = "No internet Connectivity";
            connectionInternet = false;
        }

        Log.i("internetConnection", "" + connectionInternet);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0) {
//					Intent intent = new Intent(DrawerActivity.this, LocationService.class);
//					startService(intent);

                }
            }
        }
    }


}
package com.evfy.evfytracker.Activity.JobStepAttempt;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity;
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.JobSteps;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ListJobStepsDetails extends AppCompatActivity {

    LinearLayout mLayoutListJobSteps;
    int jobStepId, orderDetailId;
    JobSteps jobSteps;
    TextView contextProgressDialog;
    Dialog pdProcess;
    String trackingNumber;
    boolean showCompletedFailedButton, startJobStepsButton, unauthorizedNeedToLoginPage = false;
    SharedPreferences settings;
    int totalIndex;
    String currentDateString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_job_steps_details);

        initialize();

        settings = PreferenceManager.getDefaultSharedPreferences(ListJobStepsDetails.this);
        unauthorizedNeedToLoginPage = false;


        pdProcess = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        contextProgressDialog.setText(getResources().getString(R.string.updateJobSteps));
        pdProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdProcess.setContentView(content);
        pdProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdProcess.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobStepId = extras.getInt("jobStepId");
            orderDetailId = extras.getInt("OrderId");
            trackingNumber = extras.getString("trackingNumber");
            showCompletedFailedButton = extras.getBoolean("showCompletedFailedButton");
            startJobStepsButton = extras.getBoolean("startJobStepsButton");
        }


        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Attempt in this step");

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        try {
            GeneralActivity generalActivity = new GeneralActivity();
            if (!generalActivity.haveInternetConnected(getApplicationContext())) {
                Log.i("internetConnection", "no");
                showErrorInternetDialog().show();
            } else {
                contextProgressDialog.setText(getResources().getString(R.string.loadingListOrderAttempt));
                pdProcess.show();
                getJobStepDetails();
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("OrderId", orderDetailId);
        ii.putExtra("jobStepId", jobStepId);
        ii.putExtra("trackingNumber", trackingNumber);
        ii.putExtra("showCompletedFailedButton", showCompletedFailedButton);
        ii.putExtra("startJobStepsButton", startJobStepsButton);

        setResult(Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION, ii);
        finish();
        super.onBackPressed();
    }

    public void displayData(JobSteps jobSteps) {
        mLayoutJobStepsDetails(jobSteps);
    }

    public void mLayoutJobStepsDetails(final JobSteps jobSteps) {
        mLayoutListJobSteps.removeAllViews();
        int noBil = 0;
        try {

            Collections.sort(jobSteps.getmOrderAttemptsList(), new Comparator<OrderAttempt>() {

                @Override
                public int compare(final OrderAttempt lhs, final OrderAttempt rhs) {

                    if (lhs == null && rhs == null) return 0;
                    if (lhs == null) return -1;
                    if (rhs == null) return 1;

                    return rhs.getSubmitted_time().compareTo(lhs.getSubmitted_time());
                }

            });

        } catch (final Throwable e) {
            LogCustom.e(e);
        }

        if (jobSteps.getmOrderAttemptsList().size() > 0) {

            totalIndex = 0;

            for (int i = 0; i < jobSteps.getmOrderAttemptsList().size(); i++) {
                final OrderAttempt orderAttempt = jobSteps.getmOrderAttemptsList().get(i);

                if (JavaOp.ifNullThenLog(orderAttempt)) continue;

                final View view = View.inflate(this, R.layout.view_job_step_attempt_details, null);
                mLayoutListJobSteps.addView(view);
                final LinearLayout layoutJobStepAttempt = view.findViewById(R.id.layoutJobStepAttempt);
                final TextView bilJobStepAttempt = view.findViewById(R.id.bilJobStepAttempt);
                final TextView dateJobStepAttempt = view.findViewById(R.id.dateJobStepAttempt);

                totalIndex = totalIndex + 1;

                bilJobStepAttempt.setText("Attempt #" + totalIndex);

                String string = orderAttempt.getSubmitted_time();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("UTC"));
                try {
                    Date date = format.parse(string);

                    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, hh:mm:a");
                    sdf.setTimeZone(TimeZone.getDefault());

                    currentDateString = sdf.format(date);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dateJobStepAttempt.setText(currentDateString);


                layoutJobStepAttempt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (orderAttempt.getReason().equalsIgnoreCase("")) {
                            Intent i = new Intent(getApplicationContext(), OrderAttemptDetails.class);
                            i.putExtra("jobStepId", jobSteps.getId());
                            i.putExtra("jobStepStatusId", jobSteps.getJob_step_status_id());
                            i.putExtra("OrderAttemptImageSend", orderAttempt);
                            i.putExtra("OrderId", jobSteps.getOrderId());
                            i.putExtra("trackingNumber", trackingNumber);
                            i.putExtra("showCompletedFailedButton", showCompletedFailedButton);
                            i.putExtra("startJobStepsButton", startJobStepsButton);
                            i.putExtra("numberOfPage", totalIndex);
                            i.putExtra("noInternetConnection", false);

                            startActivityForResult(i, Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT);
                        } else {
                            Intent i = new Intent(getApplicationContext(), OrderAttemptFailedDetails.class);
                            i.putExtra("jobStepId", jobSteps.getId());
                            i.putExtra("jobStepStatusId", jobSteps.getJob_step_status_id());
                            i.putExtra("OrderAttemptImageSend", orderAttempt);
                            i.putExtra("OrderId", jobSteps.getOrderId());
                            i.putExtra("trackingNumber", trackingNumber);
                            i.putExtra("showCompletedFailedButton", showCompletedFailedButton);
                            i.putExtra("startJobStepsButton", startJobStepsButton);

                            i.putExtra("numberOfPage", totalIndex);


                            LogCustom.i("getSubmitted_time", orderAttempt.getSubmitted_time());

                            i.putExtra("reason", orderAttempt.getReason());
                            i.putExtra("time", currentDateString);
                            i.putExtra("noInternetConnection", false);

                            startActivityForResult(i, Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT);
                        }

                    }
                });
            }
        }

    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!requestCallBackError) {

                    unauthorizedNeedToLoginPage = false;

                    try {
                        String errorMessage = "";
                        JSONObject data = null;
                        if (body != null || !body.equalsIgnoreCase("")) {

                            try {
                                data = new JSONObject(body);
                            } catch (Exception e) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                            }

                            if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                if (data.has("error") && !(data.isNull("error"))) {
                                    try {
                                        errorMessage = data.getString("error");
                                    } catch (Exception e) {
                                        JSONObject errorMessageArray = data.getJSONObject("error");
                                        errorMessage = errorMessageArray.getString("error");
                                    }
                                }

                                if (errorMessage.equalsIgnoreCase("")) {
                                    errorMessage = getResources().getString(R.string.pleaseContactAdmin);
                                }

                                showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


                            } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                    if (data.getBoolean("device_not_found")) {
                                        showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                    if (data.getBoolean("device_is_banned")) {
                                        showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                    if (data.getBoolean("unpaid_subscription")) {
                                        showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                    if (data.getBoolean("quota_reached")) {
                                        showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                    if (data.getBoolean("blacklist")) {
                                        showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                            }

                        } else {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                        showErrorNoInternetDialog().show();
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                    } else {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    }
                    // this for request call back error. Maybe because cannot connect server.
                }
            }
        });


    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */

    public void getJobStepDetails() throws IOException {


        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.getJobStepDetails(getApplicationContext(), jobStepId
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pdProcess != null) {
                                        if (pdProcess.isShowing()) {
                                            pdProcess.dismiss();
                                        }
                                    }

                                    if (body == null || body.isEmpty()) {

                                    } else {
                                        try {
                                            final JSONObject data = new JSONObject(body);

                                            LogCustom.i("dataaa", data.toString());

                                            if (data.has("status") && !(data.isNull("status"))) {
                                                if (data.getBoolean("status")) {
                                                    JSONObject jsonObject = data.getJSONObject("result");
                                                    jobSteps = new JobSteps(jsonObject);
                                                    displayData(jobSteps);
                                                }
                                            } else {

                                            }

                                        } catch (JSONException e) {

                                            LogCustom.e(e);

                                            try {

                                            } catch (final Throwable t) {
                                            }

                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if ((pdProcess != null) && pdProcess.isShowing()) {
                                        pdProcess.dismiss();
                                    }

                                    errorHandlingFromResponseCode(body, requestCallBackError, responseCode);

                                }
                            });
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void initialize() {
        mLayoutListJobSteps = findViewById(R.id.mLayoutListJobSteps);
    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */
    // After scan, should check either that step need to sign or not

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("onActivityResult", "on activity result:" + requestCode);

        switch (requestCode) {
            case Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT:

                showCompletedFailedButton = data.getExtras().getBoolean("showCompletedFailedButton");
                startJobStepsButton = data.getExtras().getBoolean("startJobStepsButton");
                trackingNumber = data.getExtras().getString("trackingNumber");
                jobStepId = data.getExtras().getInt("jobStepId");
                orderDetailId = data.getExtras().getInt("OrderId");

                try {
                    getJobStepDetails();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        getMenuInflater().inflate(R.menu.job_steps_details, menu);
        //  return true;

        return super.onCreateOptionsMenu(menu);

        //return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                onBackPressed();
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    /*
     * This func for show alert dialog popup where it success, failed, warning, no internet connection
     * */
    /* ================== SWEET ALERT DIALOG ===================== */
    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if (unauthorizedNeedToLoginPage) {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
                                LogCustom.i("isMyServiceRunning", "true");
                                Intent intent = new Intent(getApplicationContext(), ForegroundLocationService.class);
                                stopService(intent);
                            }

                            settings.edit().remove("isOnline").apply();
                            settings.edit().remove("googleToken").apply();
                            settings.edit().putString("user_name", "").commit();
                            unauthorizedNeedToLoginPage = false;

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.putExtra("EXIT", true);
                            startActivity(i);
                        }
                    }
                });


        sweetAlertDialog.setCancelable(true);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorNoInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorNoInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorNoInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        sDialog.dismiss();
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivityForResult(intent, Constants.OPEN_INTERNET_CONNECTION);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }
}

/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.evfy.evfytracker.Activity.ScanBarcode;

import static com.evfy.evfytracker.R.id.barcode;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.TrackingOrderAdapter;
import com.evfy.evfytracker.classes.TrackingOrderCheckBox;
import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture;
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic;
import com.google.android.gms.vision.barcode.Barcode;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever;

/**
 * Main activity demonstrating how to pass extra parameters to an activity that
 * reads barcodes.
 */
public class ScanActivity extends AppCompatActivity implements BarcodeRetriever {

    // use a compound button so either checkbox or switch widgets work.


    private static final String TAG = "BarcodeMain";
    List<TrackingOrderCheckBox> trackingOrderCheckBoxesList = new ArrayList<TrackingOrderCheckBox>();
    TrackingOrderAdapter adapter;
    CheckBox fromXMl;
    SwitchCompat drawRect, autoFocus, supportMultiple, touchBack, drawText;
    android.widget.Button completeScanButton;
    ListView trackingOrderListView;
    ArrayList<String> trackingOrderList = new ArrayList<String>();
    TextView remainingText;
    ImageButton btnManualInput;
    List<TrackingOrderCheckBox> tempTrackingOrderCheckBoxesList = new ArrayList<TrackingOrderCheckBox>();

    final MediaPlayer mp = new MediaPlayer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_scan);

        trackingOrderList = getIntent().getStringArrayListExtra("trakingNumList");


        final BarcodeCapture barcodeCapture = (BarcodeCapture) getSupportFragmentManager().findFragmentById(barcode);
        barcodeCapture.setRetrieval(this);

        barcodeCapture.setShowDrawRect(true);


        fromXMl = findViewById(R.id.from_xml);
        drawRect = findViewById(R.id.draw_rect);
        autoFocus = findViewById(R.id.focus);
        supportMultiple = findViewById(R.id.support_multiple);
        touchBack = findViewById(R.id.touch_callback);
        remainingText = findViewById(R.id.remainingScanItems);

        drawText = findViewById(R.id.draw_text);
        trackingOrderListView = findViewById(R.id.trackingOrderListView);
        completeScanButton = findViewById(R.id.scanCompleteButton);
        completeScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                setResult(RESULT_OK, intent);
                finish();


            }
        });


        for (int i = 0; i < trackingOrderList.size(); i++) {
            TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
            trackingOrderCheckBoxesList.add(trackingOrderCheckBox);
            tempTrackingOrderCheckBoxesList.add(trackingOrderCheckBox);
        }

        Log.i("", "tracking order list view:" + trackingOrderList.size());
        Log.i("", "tracking order checkbox list view:" + trackingOrderCheckBoxesList.size());
        Log.i("", "tracking order temp cehckbox list view:" + tempTrackingOrderCheckBoxesList.size());

        adapter = new TrackingOrderAdapter(this,
                R.layout.row_tracking_order_row, tempTrackingOrderCheckBoxesList, false);
        trackingOrderListView.setAdapter(adapter);

        remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));

        findViewById(R.id.refresh).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromXMl.isChecked()) {

                } else {
                    barcodeCapture.setShowDrawRect(drawRect.isChecked());
                    barcodeCapture.setSupportMultipleScan(supportMultiple.isChecked());
                    barcodeCapture.setTouchAsCallback(touchBack.isChecked());
                    barcodeCapture.shouldAutoFocus(autoFocus.isChecked());
                    barcodeCapture.setShouldShowText(drawText.isChecked());
                    barcodeCapture.refresh();
                }
            }
        });

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    /**
     * Hides the soft keyboard
     */
    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void verifyBarCode(String barcode) {


        for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {

            Log.i("", "tracking order number 00:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);

            if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().equalsIgnoreCase(barcode)) {
                trackingOrderCheckBoxesList.get(i).setChecked(true);


                //setItems(trackingOrderCheckBoxesList);
                Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


            }
        }

        for (int j = 0; j < tempTrackingOrderCheckBoxesList.size(); j++) {
            if (tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().equalsIgnoreCase(barcode)) {

                tempTrackingOrderCheckBoxesList.remove(j);
                adapter.notifyDataSetChanged();
                remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));

            }
        }


        if (tempTrackingOrderCheckBoxesList.size() == 0) {
            Intent intent = new Intent();
            intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
            setResult(RESULT_OK, intent);
            finish();
        }

    }


    @Override
    public void onRetrieved(final Barcode barcode) {
        Log.d(TAG, "Barcode read: " + barcode.displayValue);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this)
//                        .setTitle("code retrieved")
//                        .setMessage(barcode.displayValue);
//                builder.show();


                if (mp.isPlaying()) {
                    mp.stop();
                }

                try {
                    mp.reset();
                    AssetFileDescriptor afd;
                    afd = getAssets().openFd("beep.mp3");
                    mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mp.prepare();
                    mp.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Log.i("", "tracking order checkbox list:" + trackingOrderCheckBoxesList.size());
                for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {

                    Log.i("", "tracking order number 00:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim() + " barcode display value:" + barcode.displayValue.trim());

                    if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(barcode.displayValue.trim())) {
                        trackingOrderCheckBoxesList.get(i).setChecked(true);


                        //setItems(trackingOrderCheckBoxesList);
                        Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode.displayValue);


                    }
                }

                boolean correctScan = false;
                for (int j = 0; j < tempTrackingOrderCheckBoxesList.size(); j++) {
                    if (tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().trim().equalsIgnoreCase(barcode.displayValue.trim())) {

                        correctScan = true;
                        tempTrackingOrderCheckBoxesList.remove(j);
                        adapter.notifyDataSetChanged();
                        remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));

                    }
                }


                if (correctScan == false) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this)
                            .setTitle("Error")
                            .setMessage("No Job Assigned");
                    builder.show();
                } else {
                    if (tempTrackingOrderCheckBoxesList.size() == 0) {
                        Intent intent = new Intent();
                        intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }


            }
        });


    }

    public void setItems(List<TrackingOrderCheckBox> myList) {
        this.trackingOrderCheckBoxesList.clear();
        this.trackingOrderCheckBoxesList.addAll(myList);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onRetrievedMultiple(final Barcode closetToClick, final List<BarcodeGraphic> barcodeGraphics) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String message = "Code selected : " + closetToClick.displayValue + "\n\nother " +
                        "codes in frame include : \n";
                for (int index = 0; index < barcodeGraphics.size(); index++) {
                    Barcode barcode = barcodeGraphics.get(index).getBarcode();
                    message += (index + 1) + ". " + barcode.displayValue + "\n";
                }
//                AlertDialog.Builder builder = new AlertDialog.Builder(ScanActivity.this)
//                        .setTitle("code retrieved")
//                        .setMessage(message);
//                builder.show();

                if (mp.isPlaying()) {
                    mp.stop();
                }

                try {
                    mp.reset();
                    AssetFileDescriptor afd;
                    afd = getAssets().openFd("beep.mp3");
                    mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mp.prepare();
                    mp.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {
                    Log.i("", "tracking order number 33:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + message);

                    if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().equalsIgnoreCase(message)) {
                        Log.i("", "tracking order number 44:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + message);

                        trackingOrderCheckBoxesList.get(i).setChecked(true);
                        adapter.notifyDataSetChanged();

                    }
                }
            }
        });

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onRetrievedFailed(String reason) {

    }


}

package com.evfy.evfytracker.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.multidex.MultiDex;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.OrderAttemptImageAdapter;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.evfy.evfytracker.classes.OrderAttemptImage;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.ImageOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class UploadImageForIncomplete extends AppCompatActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    static String FOLDER = "";
    static final int UPLOAD_IMAGE_CODE = 890;
    Uri uriPhotoTakenByCamera;
    boolean requestTakeOtherPhoto = false;
    boolean resultFromGallery;
    AlertDialog imageDialog;
    ArrayList<String> descriptionImage = new ArrayList<>();

    String imageBitmap, descriptionImageString;
    int totalImage = 0;
    ArrayList<Bitmap> imagesUriList = new ArrayList<>();
    private SliderLayout sliderImage;
    OrderAttemptImageAdapter myCustomPagerAdapter;
    Button btnSubmit;
    LinearLayout btnCamera;
    Dialog pd;
    JobOrder jobOrder;
    int indexImageUpload;
    Activity mActivity = this;

    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");

    String orderId, customerName;
    Date selectedDate;
    int drop_off_worker_id, order_status_id, wms_order_id;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        LogCustom.i("InstallMultiDex");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image_for_incomplete);
        FOLDER = getExternalFilesDir(Environment.DIRECTORY_PICTURES + "/" + "KnocKnocKapp").getAbsolutePath();
        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Upload Photos");


        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initialize();
        addImages();

        //button submit
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("", "btn submit click");
                SubmitConfirmation();
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("needRefresh", false);
        ii.putExtra("OrderId", orderId);
        setResult(UPLOAD_IMAGE_CODE, ii);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent ii = new Intent();
        ii.putExtra("needRefresh", false);
        ii.putExtra("OrderId", orderId);
        setResult(UPLOAD_IMAGE_CODE, ii);
        finish();
        return true;
    }

    public void initialize() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobOrder = (JobOrder) getIntent().getSerializableExtra("OrderJobSend");

            orderId = extras.getString("OrderId");
            customerName = extras.getString("CustomerName");
            selectedDate = (Date) getIntent().getSerializableExtra("SelectedDate");
            drop_off_worker_id = extras.getInt("drop_off_worker_id", 0);
            order_status_id = extras.getInt("order_status_id", 0);
            wms_order_id = extras.getInt("wms_order_id", 0);
        }


        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.custom_dialog_progress);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pd.setCancelable(false);

        sliderImage = findViewById(R.id.sliderImage);
        btnCamera = findViewById(R.id.btnCamera);
        btnSubmit = findViewById(R.id.btnSubmit);
    }


    //    //inflate custom layout dialog
    public void addImages() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null);
        final LinearLayout layoutClickCamera = alertLayout.findViewById(R.id.layoutClickCamera);
        final LinearLayout layoutClickGallery = alertLayout.findViewById(R.id.layoutClickGallery);

        layoutClickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriPhotoTakenByCamera = AndroidOp.startCameraForResult(UploadImageForIncomplete.this, FOLDER, null);
            }
        });

        layoutClickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidOp.startGettingImageFromGalleryForResult(UploadImageForIncomplete.this);
            }
        });


        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo");
            alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing

                }
            });
        } else {
            alert.setTitle("Complete action using");
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }


        alert.setView(alertLayout);
        alert.setCancelable(false);


        imageDialog = alert.create();
        if (!imageDialog.isShowing()) {
            imageDialog.show();
        }
    }

    public void showPhotoGaleryImage() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null);
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewPager);

        //   myCustomPagerAdapter = new OrderAttemptImageAdapter(UploadImageForIncomplete.this, null, null, null,
        //          imagesUriList, descriptionImage, false);
        viewPager.setAdapter(myCustomPagerAdapter);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("DONE",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showPhotoGaleryImageAfter();
            }


        });
    }

    void showButtonSubmit() {
        btnSubmit.setVisibility(View.VISIBLE);
        btnSubmit.setEnabled(true);
        btnSubmit.setTextColor(Color.parseColor("#FFFFFF"));
        btnSubmit.setBackgroundColor(Color.parseColor("#39B54A"));
    }

    void showPhotoGaleryImageAfter() {

        sliderImage.removeAllSliders();

        if (imagesUriList.size() > 0) {
            sliderImage.setVisibility(View.VISIBLE);
            showButtonSubmit();
        } else {
            btnSubmit.setVisibility(View.GONE);
            sliderImage.setVisibility(View.GONE);
        }

        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();
        for (int i = 0; i < imagesUriList.size(); i++) {
            url_maps.put("" + descriptionImage.get(i), imagesUriList.get(i));
        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(UploadImageForIncomplete.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(UploadImageForIncomplete.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            sliderImage.addSlider(textSliderView);
        }


        if (totalImage < 2) {
            Log.i("totalImage1 11", "" + totalImage);
            sliderImage.stopAutoCycle();
            sliderImage.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
//
        } else if (totalImage > 1) {
            Log.i("totalImage2 11", "" + totalImage);
            sliderImage.startAutoCycle();
            sliderImage.setPagerTransformer(true, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;

        switch (requestCode) {

            case AndroidOp.REQUEST_CODE_CAMERA:
                Log.i("requset", "request code camera");

                resultFromGallery = false;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }

                addDescription(data, resultFromGallery);


                break;

            case AndroidOp.REQUEST_CODE_GALLERY:
                Log.i("", "request code gallery");

                resultFromGallery = true;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }

                addDescription(data, resultFromGallery);

                break;

        }

    }

    public void addDescription(final Intent data, final boolean resultFromGalleryDescription) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.items_layout_description_image, null);
        final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);


        textViewTitle.setText("Add Description for Image");


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descriptionImage.add(editTextDescription.getText().toString());
                dialog.dismiss();


                if (!resultFromGalleryDescription) {
                    try {
                        resultFromGallery = false;
                        showImageCameraGallery(uriPhotoTakenByCamera);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        resultFromGallery = true;
                        Uri photoUri = data.getData();
                        showImageCameraGallery(photoUri);
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }
            }


        });
    }

    void showImageCameraGallery(final Uri uri) throws IOException {

        //Update show image first
        Log.i("uriPhotoTakenByCamera", "" + uri.getPath());

        if (imagesUriList.size() == 0) {
            totalImage = 0;
        }

        totalImage = totalImage + 1;
        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();

        descriptionImageString = descriptionImage.get(totalImage - 1);


        if (resultFromGallery) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            File imgFile = new File(filePath);
            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));


            url_maps.put("" + descriptionImageString, bmp);

            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

            imagesUriList.add(compressedImageBitmap);

            // allOrderAttemptImage.add(compressedImageBitmap);

        } else {

            File imgFile = new File(uri.getPath());

            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));


            url_maps.put("" + descriptionImageString, bmp);
            // url_maps.put("Photo No " + totalImage, bmp);
            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

            imagesUriList.add(compressedImageBitmap);

            //  allOrderAttemptImage.add(compressedImageBitmap);

        }

        Log.i("InThis", "yes");


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(UploadImageForIncomplete.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(UploadImageForIncomplete.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            sliderImage.addSlider(textSliderView);
        }

        sliderImage.setVisibility(View.VISIBLE);


        if (totalImage < 2) {
            Log.i("totalImage1 00", "" + totalImage);
            sliderImage.stopAutoCycle();
            sliderImage.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
//
        } else if (totalImage > 1) {
            Log.i("totalImage2 00", "" + totalImage);
            sliderImage.startAutoCycle();
            sliderImage.setPagerTransformer(true, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
        }
        //Update show image first

        requestTakeOtherPhoto = true;
        showButtonSubmit();
        addImages();


    }

    public void SubmitConfirmation() {
        try {

            OrderAttempt orderAttempt = jobOrder.getmOrderAttemptsList().get(jobOrder.getmOrderAttemptsList().size() - 1);


            JSONObject jobOrderObject = new JSONObject();
            JSONArray jobOrderIdArray = new JSONArray();


            jobOrderIdArray.put(String.valueOf(jobOrder.getOrderId()));
            Log.i("jobOrderIdArray", "" + jobOrderIdArray);


            jobOrderObject.put("id", jobOrderIdArray);


            Log.i("", "image uri list:" + imagesUriList.size());
            Log.i("", "image order attempt list 00:" + orderAttempt.getmOrderAttemptImages().size());

            if (imagesUriList.size() > 0) {

                jobOrderObject.put("order_status_id", Constants.INCOMPLETED);

                indexImageUpload = 0;
                Bitmap bmp = imagesUriList.get(indexImageUpload);

                imageBitmap = ImageOp.convert(bmp, Bitmap.CompressFormat.JPEG, 80);

                String[] body2 = new String[1];
                body2[0] = "data:image/png;base64," + imageBitmap;
                Log.i("", "base 64 length2:" + body2[0].getBytes().length);
                Log.i("", "base 64 readible size2:" + humanReadableByteCount(body2[0].getBytes().length, true));


                Log.i("", "image order attempt list 11:" + orderAttempt.getmOrderAttemptImages().size());

                OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage(body2[0], descriptionImage.get(indexImageUpload), false);
                orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);

                LogCustom.i("descriptionImage", "is:" + descriptionImage.get(indexImageUpload));
            }

            Log.i("", "order attempt image size 22:" + orderAttempt.getmOrderAttemptImages().size());


            JSONArray orderAttemptArray = new JSONArray();
            orderAttemptArray.put(orderAttempt.getJSONObject());
            jobOrderObject.put("order_attempts", orderAttemptArray);


            Log.i("", "order attempt:" + orderAttempt);
            Log.i("", "order attempt json:" + orderAttempt.getJSONObjectImageOnly());
            Log.i("", "job order json:" + jobOrderObject);


            JSONObject dataObject = new JSONObject();
            JSONArray jobDataArray = new JSONArray();
            jobDataArray.put(jobOrderObject);
            dataObject.put("data", jobDataArray);

            Log.i("", "data object:" + dataObject);

            postRequest(dataObject);


        } catch (Exception e) {
            Log.i("", "exception :" + e);

        }
    }


    void repeatUploadImage(int indexImageUpload, int order_id, int orderAttemptId) {
        try {
            Log.i("", "btn submit click 44");


            //OrderAttempt orderAttempt = jobOrder.getmOrderAttemptsList().get(jobOrder.getmOrderAttemptsList().size() - 1);

            OrderAttempt orderAttempt = new OrderAttempt();

            JSONObject jobOrderObject = new JSONObject();
            JSONArray jobOrderIdArray = new JSONArray();


            jobOrderIdArray.put(order_id);
            Log.i("jobOrderIdArray", "" + jobOrderIdArray);


            jobOrderObject.put("id", jobOrderIdArray);

            jobOrderObject.put("order_status_id", Constants.INCOMPLETED);

            Bitmap bmp = imagesUriList.get(indexImageUpload);


            imageBitmap = ImageOp.convert(bmp, Bitmap.CompressFormat.JPEG, 80);

            String[] body2 = new String[1];
            body2[0] = "data:image/png;base64," + imageBitmap;
            Log.i("", "base 64 length2:" + body2[0].getBytes().length);
            Log.i("", "base 64 readible size2:" + humanReadableByteCount(body2[0].getBytes().length, true));


            Log.i("", "image order attempt list 11:" + orderAttempt.getmOrderAttemptImages().size());

            OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage(body2[0], descriptionImage.get(indexImageUpload), false);
            orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);

            orderAttempt.setId(orderAttemptId);

            JSONArray orderAttemptArray = new JSONArray();
            orderAttemptArray.put(orderAttempt.getJSONObjectWithIDRepeatImage());
            jobOrderObject.put("order_attempts", orderAttemptArray);


            Log.i("", "order attempt:" + orderAttempt);
            Log.i("", "order attempt json:" + orderAttempt.getJSONObjectWithIDRepeatImage());
            Log.i("", "job order json:" + jobOrderObject);


            JSONObject dataObject = new JSONObject();
            JSONArray jobDataArray = new JSONArray();
            jobDataArray.put(jobOrderObject);
            dataObject.put("data", jobDataArray);

            Log.i("", "data object:" + dataObject);

            postRequest(dataObject);


        } catch (Exception e) {
            Log.i("", "exception :" + e);

        }

    }

    void postRequest(final JSONObject postBody) throws IOException {

        // OkHttpClient client = new OkHttpClient();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
        String accessToken = settings.getString("access_token", "");

        RequestBody bodys = RequestBody.create(MEDIA_TYPE,
                postBody.toString());

        String postUrl = Constants.SAAS_SERVER_LDS + "/api/orders/assign/order";

        Request request = new Request.Builder()
                .url(postUrl)
                .post(bodys)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer " + accessToken)
                .build();


        pd.show();


        LogCustom.i("bodys", bodys);

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage();
                LogCustom.i("requestFailure", mMessage);
                pd.dismiss();
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String mMessage = response.body().string();

                if (response.isSuccessful()) {
                    try {
                        LogCustom.i("ResponseOkHttp", mMessage);

                        JSONObject resultObject = new JSONObject(mMessage);

                        if (resultObject.has("status") && !(resultObject.isNull("status"))) {
                            if (resultObject.getBoolean("status")) {
                                if (resultObject.has("attempts") && !(resultObject.isNull("attempts"))) {
                                    JSONArray data = resultObject.getJSONArray("attempts");
                                    for (int i = 0; i < data.length(); i++) {

                                        indexImageUpload = indexImageUpload + 1;
                                        if (imagesUriList.size() > 0) {
                                            if (indexImageUpload == imagesUriList.size()) {
                                                Log.i("", "msgNetworkError 00");
                                                pd.dismiss();
                                                Intent ii = new Intent();
                                                ii.putExtra("OrderId", orderId);
                                                ii.putExtra("CustomerName", customerName);
                                                ii.putExtra("SelectedDate", selectedDate);
                                                ii.putExtra("drop_off_worker_id", drop_off_worker_id);
                                                ii.putExtra("order_status_id", jobOrder.getOrderStatusId());
                                                ii.putExtra("wms_order_id", wms_order_id);
                                                ii.putExtra("needRefresh", true);
                                                setResult(UPLOAD_IMAGE_CODE, ii);
                                                finish();
                                            } else {
                                                JSONObject jsonObject2 = (JSONObject) data.get(i);
                                                int order_id = Integer.valueOf(jsonObject2.getString("order_id"));
                                                int order_attempt_id = Integer.valueOf(jsonObject2.getString("id"));
                                                LogCustom.i("order id is : " + order_id, "orderAttemptId is : " + order_attempt_id);
                                                repeatUploadImage(indexImageUpload, order_id, order_attempt_id);
                                            }
                                        } else {
                                            Log.i("", "msgNetworkError 00");
                                            pd.dismiss();
                                            Intent ii = new Intent();
                                            ii.putExtra("OrderId", orderId);
                                            ii.putExtra("CustomerName", customerName);
                                            ii.putExtra("SelectedDate", selectedDate);
                                            ii.putExtra("drop_off_worker_id", drop_off_worker_id);
                                            ii.putExtra("order_status_id", jobOrder.getOrderStatusId());
                                            ii.putExtra("wms_order_id", wms_order_id);
                                            ii.putExtra("needRefresh", true);
                                            setResult(UPLOAD_IMAGE_CODE, ii);
                                            finish();
                                        }

                                    }

                                }
                            }
                        } else {
                            pd.dismiss();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        pd.dismiss();
                    }

                }


            }
        });
    }


    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

        Log.i("click second slider", "yes");
        if (imagesUriList.size() > 0) {
            showPhotoGaleryImage();
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    public static float humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes;
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return (float) (bytes / Math.pow(unit, exp));
    }

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    public String getFilename() {
        File file = new File(FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

}

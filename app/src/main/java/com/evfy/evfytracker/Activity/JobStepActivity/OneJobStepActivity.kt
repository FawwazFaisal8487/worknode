package com.evfy.evfytracker.Activity.JobStepActivity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Transformers.BaseTransformer
import com.evfy.evfytracker.Activity.ConfirmationActivity.JobStepSignConfirmation
import com.evfy.evfytracker.Activity.ConfirmationActivity.JobStepSignConfirmationWithNoInternet
import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity
import com.evfy.evfytracker.Activity.JobStepAttempt.ListJobStepsDetails
import com.evfy.evfytracker.Activity.JobStepAttempt.ListJobStepsDetailsWithNoInternet
import com.evfy.evfytracker.Activity.ScanBarcode.ScanDoActivity
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService
import com.evfy.evfytracker.Constants
import com.evfy.evfytracker.Constants.SAAS_SERVER_AUTH
import com.evfy.evfytracker.CountingRequestBody
import com.evfy.evfytracker.Database.DatabaseHandlerJobs
import com.evfy.evfytracker.GeneralActivity
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest.OKHttpNetwork
import com.evfy.evfytracker.R
import com.evfy.evfytracker.adapter.EditDeleteOrderAttemptAdapter
import com.evfy.evfytracker.classes.JobOrder
import com.evfy.evfytracker.classes.JobSteps
import com.evfy.evfytracker.classes.OrderAttempt
import com.evfy.evfytracker.classes.OrderAttemptImage
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.textfield.TextInputEditText
import com.lkh012349s.mobileprinter.Utils.AndroidOp
import com.lkh012349s.mobileprinter.Utils.LogCustom
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import lucifer.org.snackbartest.Icon
import lucifer.org.snackbartest.MySnack.SnackBuilder
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class OneJobStepActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    LocationListener, OnSliderClickListener {
    val urlImageStringArray = ArrayList<String?>()
    val urlDescriptionStringArray = ArrayList<String?>()
    val imageBitmapArray = ArrayList<Bitmap>()
    val orderAttemptImagesForFailed = ArrayList<OrderAttemptImage>()
    var uriPhotoTakenByCamera: Uri? = null
    var selectedUri: Uri? = null
    var requestTakeOtherPhoto = false
    var imageDialog: AlertDialog? = null
    var descriptionImageText: String? = null
    var myCustomPagerAdapter: EditDeleteOrderAttemptAdapter? = null
    var image_normalURL: String? = null
    var takeAnotherPhotoBool = false
    var resultFromGallery = false
    var tempFile: File? = null
    private var slider: SliderLayout? = null
    private var isItemSelected = false
    var orderAttempt: OrderAttempt? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var lastLocation: Location? = null
    var mCurrLocationMarker: Marker? = null
    var mLocationRequest: LocationRequest? = null
    var btnFailedJobStep: Button? = null
    var btnStartOrCompletedJobStep: Button? = null
    var totalAttemptJobStep: TextView? = null
    var jobStepContact: TextView? = null
    var jobStepDescription: TextView? = null
    var jobStepTrackingNo: TextView? = null
    var jobStepAddress: TextView? = null
    var jobStepStatus: TextView? = null
    var jobStepCompany: TextView? = null
    var jobStepNavigate: TextView? = null
    var jobStepWhatsApp: TextView? = null
    var jobStepPic: TextView? = null
    var scrollview: ScrollView? = null
    var jobStepId = 0
    var notes: EditText? = null
    var jobSteps: JobSteps? = null
    var jobStepStatusId = 0
    var totalOrderItem = 0
    var trackingNumber: String? = null
    var companyName: String? = null
    var orderNumber: String? = null
    var jobStepName: String? = null
    var startJobStepsButton = false
    var showCompletedFailedButton = false
    var unauthorizedNeedToLoginPage = false
    var layoutClickStepAttempt: LinearLayout? = null
    var layoutClickCall: LinearLayout? = null
    var layoutJobStepStatus: LinearLayout? = null
    var layoutClickNavigate: LinearLayout? = null
    var layoutClickWhatsApp: LinearLayout? = null
    var settings: SharedPreferences? = null
    var pdUpdateProcess: Dialog? = null
    var contextProgressDialog: TextView? = null
    var orderDetailId = 0
    var OrderStatusId = 0
    var mapFragment: SupportMapFragment? = null
    var dbUpdateJobs: DatabaseHandlerJobs? = null
    var assignedOrderStatusIDFromDB = 0
    var ackowledgedOrderStatusIDFromDB = 0
    var inprogressOrderStatusIDFromDB = 0
    var completedOrderStatusIDFromDB = 0
    var failedOrderStatusIDFromDB = 0
    var cancelledOrderStatusIDFromDB = 0
    var selfcollectOrderStatusIDFromDB = 0
    var isLastJobStep = false
    var scanRequiredForAllItem = false
    var reasonFailed: String? = null
    var isManPowerCanUpdateProcess = false
    var alert: AlertDialog? = null
    var mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            for (location in locationResult.locations) {
                lastLocation = location
            }
        }
    }
    private var mMap: GoogleMap? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private fun wantProceedIfNoInternet(optionProcess: Int) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("No internet connection")
            .setMessage("Your device not connect with internet. Please check your internet connection")
            .setCancelable(false)
            .setPositiveButton(
                "Open settings"
            ) { dialog, id ->
                dialog.dismiss()
                dialog.cancel()
                val intent = Intent(Intent.ACTION_MAIN, null)
                intent.addCategory(Intent.CATEGORY_LAUNCHER)
                val cn =
                    ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings")
                intent.component = cn
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        alertDialogBuilder.setNegativeButton(
            "Cancel"
        ) { dialog, id ->
            dialog.dismiss()
            dialog.cancel()
            onBackPressed()
        }
        alertDialogBuilder.setNeutralButton(
            "Continue without internet"
        ) { dialog, id ->
            dialog.dismiss()
            dialog.cancel()
            val isManPower = settings!!.getBoolean("isManPower", false)
            when (optionProcess) {
                1 -> {
                    jobSteps = dbUpdateJobs!!.getJobStepsDetailsBasedOnID(jobStepId)
                    jobStepStatusId = jobSteps?.job_step_status_id ?: 0
                    jobSteps?.setmOrderAttemptsList(
                        dbUpdateJobs!!.getListAllOrderAttemptDetailsNoInternet(
                            jobSteps?.id ?: 0
                        )
                    )
                    LogCustom.i(
                        "setmOrderAttemptsList", "" + (jobSteps?.getmOrderAttemptsList()?.size
                            ?: 0)
                    )
                    val orderAttemptList: MutableList<OrderAttempt> = ArrayList()
                    var j = 0
                    while (j < jobSteps?.getmOrderAttemptsList()?.size ?: 0) {
                        val orderAttempt = jobSteps!!.getmOrderAttemptsList()[j]
                        orderAttempt.setmOrderAttemptImages(
                            dbUpdateJobs!!.getListAllImageDetailsNoInternet(
                                orderAttempt.id
                            )
                        )
                        orderAttemptList.add(orderAttempt)
                        j++
                    }
                    jobSteps?.setmOrderAttemptsList(null)
                    jobSteps?.setmOrderAttemptsList(orderAttemptList)
                    setupDisplayData(jobSteps)
                }
                2 -> if (orderDetailId != 0 && jobStepId != 0) {
                    // btnFailedJobStep.setEnabled(false);
                    if (isManPower) {
                        isManPowerCanUpdateProcess = false
                        checkingFunctionBasedOnStatus(
                            jobStepStatusId,
                            Constants.FAILED,
                            false,
                            "is_status_need_manpower"
                        )
                        /*if (isManPowerCanUpdateProcess) {*/fetchReasonListAndShowFailedDialog()
                        /*} else {
                                                    // cannot update
                                                    showAlertManPower();
                                                    //showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                                }*/
                    } else {
                        fetchReasonListAndShowFailedDialog()
                    }
                    // jsonStructure(Constants.JOB_STEP_FAILED);
                } else {
                    showErrorDialog(
                        resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                            R.string.orderDetailError
                        )
                    ).show()
                }
                3 -> {

                    // Job must be in progress
                    // job step also must be in progress
                    //update job detail
                    //update job step
                    //drop off time start
                    LogCustom.i("step pending", "11")
                    if (orderDetailId != 0 && jobStepId != 0) {
                        if (isManPower) {
                            isManPowerCanUpdateProcess = false
                            checkingFunctionBasedOnStatus(
                                Constants.JOB_STEP_PENDING,
                                Constants.IN_PROGRESS,
                                false,
                                "is_status_need_manpower"
                            )
                            if (isManPowerCanUpdateProcess) {
                                if (totalOrderItem > 0 || scanRequiredForAllItem) {
                                    //need scan
                                    scanProcessAllItem(true)
                                } else {
                                    jsonStructureCompleteWithNoInternet(
                                        Constants.JOB_STEP_INPROGRESS,
                                        false
                                    )
                                }
                            } else {
                                // cannot update
                                showAlertManPower()
                                //showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                            }
                        } else {
                            if (totalOrderItem > 0 || scanRequiredForAllItem) {
                                //need scan
                                scanProcessAllItem(true)
                            } else {
                                jsonStructureCompleteWithNoInternet(
                                    Constants.JOB_STEP_INPROGRESS,
                                    false
                                )
                            }
                        }
                    } else {
                        showErrorDialog(
                            resources.getString(R.string.orderDetailErrorTitle),
                            resources.getString(
                                R.string.orderDetailError
                            )
                        ).show()
                    }
                }
                4 -> if (isManPower) {
                    isManPowerCanUpdateProcess = false
                    checkingFunctionBasedOnStatus(
                        Constants.JOB_STEP_INPROGRESS,
                        Constants.COMPLETE,
                        false,
                        "is_status_need_manpower"
                    )
                    if (isManPowerCanUpdateProcess) {
                        completeThisJobProcess()
                    } else {
                        // cannot update
                        showAlertManPower()
                        // showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                    }
                } else {
                    completeThisJobProcess()
                }
            }
        }
        alert = alertDialogBuilder.create()
        if (alert?.isShowing == false) {
            alert?.show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one_job_step)
        FOLDER =
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.absolutePath
        intialize()
        settings = PreferenceManager.getDefaultSharedPreferences(this@OneJobStepActivity)
        unauthorizedNeedToLoginPage = false
        val extras = intent.extras
        if (extras != null) {
            orderDetailId = extras.getInt("OrderId")
            OrderStatusId = extras.getInt("OrderStatusId")
            companyName = extras.getString("companyName")
            jobStepName = extras.getString("jobStepName")
            jobStepId = extras.getInt("jobStepId")
            jobStepStatusId = extras.getInt("jobStepStatusId")
            totalOrderItem = extras.getInt("totalOrderItem")
            trackingNumber = extras.getString("trackingNumber")
            showCompletedFailedButton = extras.getBoolean("showCompletedFailedButton")
            startJobStepsButton = extras.getBoolean("startJobStepsButton")
            isLastJobStep = extras.getBoolean("isLastJobStep")
            scanRequiredForAllItem = extras.getBoolean("scanRequiredForAllItem")
        }
        dbUpdateJobs = DatabaseHandlerJobs(this)
        LogCustom.i("totalOrderItem", ":$totalOrderItem")
        val editors = settings?.edit()
        editors?.putBoolean("insideJobDetails", false)?.apply()
        editors?.putBoolean("insideNotificationListing", false)?.apply()
        editors?.apply()
        pdUpdateProcess = Dialog(this)
        val inflater = layoutInflater
        val content = inflater.inflate(R.layout.custom_progress_dialog_process, null)
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog)
        contextProgressDialog?.text = resources.getString(R.string.updateJobSteps)
        pdUpdateProcess!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        pdUpdateProcess!!.setContentView(content)
        pdUpdateProcess!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        pdUpdateProcess!!.setCancelable(false)
        LogCustom.i("showCompletedFailedButton", "" + showCompletedFailedButton)
        LogCustom.i("startJobStepsButton", "" + startJobStepsButton)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setBackgroundColor(resources.getColor(R.color.actionbar_color))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        this.supportActionBar!!.title = jobStepName

        // action bar for app
        val actionBar = supportActionBar
        if (actionBar != null) {
            val colorDrawable = ColorDrawable(Color.parseColor("#2e86aa"))
            actionBar.setBackgroundDrawable(colorDrawable)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission()
        }
        try {
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {
                Log.i("internetConnection", "no")
                wantProceedIfNoInternet(1)
            } else {
                if (jobStepId != 0) {
                    jobStepDetails
                } else {
                    showErrorDialog(
                        resources.getString(R.string.jobStepErrorTitle), resources.getString(
                            R.string.jobStepDetailError
                        )
                    ).show()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        btnFailedJobStep!!.setOnClickListener {
            orderAttemptImagesForFailed.clear()
            if (jobStepStatusId == Constants.JOB_STEP_PENDING || jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    wantProceedIfNoInternet(2)
                } else {
                    settings =
                        PreferenceManager.getDefaultSharedPreferences(this@OneJobStepActivity)
                    val isOnline = settings?.getBoolean("isOnline", true)
                    if (checkGPSBeforeUpdate()) {
                        if (isOnline == true) {
                            if (orderDetailId != 0 && jobStepId != 0) {
                                // btnFailedJobStep.setEnabled(false);
                                val isManPower = settings?.getBoolean("isManPower", false)
                                if (isManPower == true) {
                                    isManPowerCanUpdateProcess = false
                                    checkingFunctionBasedOnStatus(
                                        jobStepStatusId,
                                        Constants.FAILED,
                                        false,
                                        "is_status_need_manpower"
                                    )
                                    if (isManPowerCanUpdateProcess) {
                                        fetchReasonListAndShowFailedDialog()
                                    } else {
                                        // cannot update
                                        showAlertManPower()
                                        //showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                    }
                                } else {
                                    fetchReasonListAndShowFailedDialog()
                                }
                                // jsonStructure(Constants.JOB_STEP_FAILED);
                            } else {
                                showErrorDialog(
                                    resources.getString(R.string.orderDetailErrorTitle),
                                    resources.getString(
                                        R.string.orderDetailError
                                    )
                                ).show()
                            }
                        } else {
                            showNeedOnlineDialog(
                                resources.getString(R.string.offlineFailJobTitle),
                                resources.getString(
                                    R.string.offlineMessage
                                )
                            ).show()
                        }
                    } else {
                        showNeedOnlineDialog(
                            resources.getString(R.string.offlineFailJobTitle), resources.getString(
                                R.string.offlineMessage
                            )
                        ).show()
                    }
                }
            }
        }
        layoutClickNavigate!!.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=" + jobSteps!!.location)
            )
            startActivity(intent)
        }
        layoutClickWhatsApp!!.setOnClickListener {
            var contactNum = ""
            contactNum = jobSteps!!.job_step_pic_contact
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://api.whatsapp.com/send?phone=$contactNum&text=")
            )
            startActivity(intent)
        }
        layoutClickStepAttempt!!.setOnClickListener {
            if (jobSteps!!.getmOrderAttemptsList().size > 0) {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    val i =
                        Intent(applicationContext, ListJobStepsDetailsWithNoInternet::class.java)
                    i.putExtra("jobStepId", jobSteps!!.id)
                    i.putExtra("OrderId", orderDetailId)
                    i.putExtra("trackingNumber", trackingNumber)
                    i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
                    i.putExtra("startJobStepsButton", startJobStepsButton)
                    startActivityForResult(i, Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION)
                } else {
                    val i = Intent(applicationContext, ListJobStepsDetails::class.java)
                    i.putExtra("jobStepId", jobSteps!!.id)
                    i.putExtra("OrderId", orderDetailId)
                    i.putExtra("trackingNumber", trackingNumber)
                    i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
                    i.putExtra("startJobStepsButton", startJobStepsButton)
                    startActivityForResult(i, Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION)
                }
            } else {
                SnackBuilder(findViewById(R.id.mainPage))
                    .setText("No Attempt")
                    .setTextColor("#ffffff") //optional
                    .setTextSize(15f)
                    .setBgColor("#C1272D")
                    .setIcon(Icon.ERROR)
                    .setDurationInSeconds(1)
                    .build()
            }
        }
        layoutClickCall!!.setOnClickListener(View.OnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    applicationContext,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                SnackBuilder(findViewById(R.id.mainPage))
                    .setText("You must allow permission for call phone")
                    .setTextColor("#ffffff") //optional
                    .setTextSize(15f)
                    .setBgColor("#C1272D")
                    .setIcon(Icon.ERROR)
                    .setDurationInSeconds(1)
                    .build()
                return@OnClickListener
            } else {
                val isCallingDirectly = true
                var contactNum = ""

                //Update UI (no need for pickupContact)
                contactNum = jobSteps!!.job_step_pic_contact
                val uri = "tel:$contactNum"
                val intent3 = Intent(
                    if (isCallingDirectly) Intent.ACTION_CALL else Intent.ACTION_DIAL,
                    Uri.parse(uri)
                )
                startActivity(intent3)
                //
            }
        })
        btnStartOrCompletedJobStep!!.setOnClickListener {
            LogCustom.i("in2222s", ": $jobStepStatusId")
            settings = PreferenceManager.getDefaultSharedPreferences(this@OneJobStepActivity)
            val isOnline = settings?.getBoolean("isOnline", true)
            val isManPower = settings?.getBoolean("isManPower", false)
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {
                Log.i("internetConnection", "no")
                if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                    wantProceedIfNoInternet(3)
                } else {
                    wantProceedIfNoInternet(4)
                }
            } else {
                if (checkGPSBeforeUpdate()) {
                    if (isOnline == true) {
                        if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                            // start job
                            LogCustom.i("step pending", "11")
                            LogCustom.i("totalOrderItem", "" + totalOrderItem)
                            if (scanRequiredForAllItem) {
                                if (trackingNumber == null || trackingNumber.equals(
                                        "",
                                        ignoreCase = true
                                    )
                                ) {
                                    if (orderDetailId != 0 && jobStepId != 0) {
                                        if (isManPower == true) {
                                            isManPowerCanUpdateProcess = false
                                            checkingFunctionBasedOnStatus(
                                                Constants.JOB_STEP_PENDING,
                                                Constants.IN_PROGRESS,
                                                false,
                                                "is_status_need_manpower"
                                            )
                                            if (isManPowerCanUpdateProcess) {
                                                btnStartOrCompletedJobStep!!.isEnabled = false
                                                jsonStructure(Constants.JOB_STEP_INPROGRESS)
                                            } else {
                                                // cannot update
                                                showAlertManPower()
                                            }
                                        } else {
                                            btnStartOrCompletedJobStep!!.isEnabled = false
                                            jsonStructure(Constants.JOB_STEP_INPROGRESS)
                                        }
                                    } else {
                                        showErrorDialog(
                                            resources.getString(R.string.orderDetailErrorTitle),
                                            resources.getString(
                                                R.string.orderDetailError
                                            )
                                        ).show()
                                    }
                                } else {
                                    if (orderDetailId != 0 && jobStepId != 0) {
                                        if (isManPower == true) {
                                            isManPowerCanUpdateProcess = false
                                            checkingFunctionBasedOnStatus(
                                                Constants.JOB_STEP_PENDING,
                                                Constants.IN_PROGRESS,
                                                false,
                                                "is_status_need_manpower"
                                            )
                                            if (isManPowerCanUpdateProcess) {
                                                if (totalOrderItem > 0 || scanRequiredForAllItem) {
                                                    //need scan
                                                    scanProcessAllItem(true)
                                                } else {
                                                    btnStartOrCompletedJobStep!!.isEnabled = false
                                                    jsonStructure(Constants.JOB_STEP_INPROGRESS)
                                                }
                                            } else {
                                                // cannot update
                                                showAlertManPower()
                                                //showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                            }
                                        } else {
                                            if (totalOrderItem > 0 || scanRequiredForAllItem) {
                                                //need scan
                                                scanProcessAllItem(true)
                                            } else {
                                                btnStartOrCompletedJobStep!!.isEnabled = false
                                                jsonStructure(Constants.JOB_STEP_INPROGRESS)
                                            }
                                        }
                                    } else {
                                        showErrorDialog(
                                            resources.getString(R.string.orderDetailErrorTitle),
                                            resources.getString(
                                                R.string.orderDetailError
                                            )
                                        ).show()
                                    }
                                }
                            } else {
                                btnStartOrCompletedJobStep!!.isEnabled = false
                                jsonStructure(Constants.JOB_STEP_INPROGRESS)
                            }
                        } else if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                            // complete job
                            if (isManPower == true) {
                                isManPowerCanUpdateProcess = false
                                checkingFunctionBasedOnStatus(
                                    Constants.JOB_STEP_INPROGRESS,
                                    Constants.COMPLETE,
                                    false,
                                    "is_status_need_manpower"
                                )
                                if (isManPowerCanUpdateProcess) {
                                    completeThisJobProcess()
                                } else {
                                    // cannot update
                                    showAlertManPower()
                                    // showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                }
                            } else {
                                completeThisJobProcess()
                            }
                        }
                    } else {
                        if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                            showNeedOnlineDialog(
                                resources.getString(R.string.offlineStartJobTitle),
                                resources.getString(
                                    R.string.offlineMessage
                                )
                            ).show()
                        } else {
                            if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                                showNeedOnlineDialog(
                                    resources.getString(R.string.offlineCompleteJobTitle),
                                    resources.getString(
                                        R.string.offlineMessage
                                    )
                                ).show()
                            }
                        }
                        //cannot update because not online
                    }
                } else {
                    if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                        showNeedOnlineDialog(
                            resources.getString(R.string.offlineStartJobTitle), resources.getString(
                                R.string.offlineMessage
                            )
                        ).show()
                    } else {
                        if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                            showNeedOnlineDialog(
                                resources.getString(R.string.offlineCompleteJobTitle),
                                resources.getString(
                                    R.string.offlineMessage
                                )
                            ).show()
                        }
                    }
                }
            }
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    fun addImages() {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null)
        val layoutClickCamera = alertLayout.findViewById<LinearLayout>(R.id.layoutClickCamera)
        val layoutClickGallery = alertLayout.findViewById<LinearLayout>(R.id.layoutClickGallery)
        layoutClickCamera.setOnClickListener { //                uriPhotoTakenByCamera = AndroidOp.startCameraForResult(OneJobStepActivity.this, FOLDER, null);
            uriPhotoTakenByCamera =
                AndroidOp.startCameraForResult(
                    this,
                    getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    null
                )
        }
        layoutClickGallery.setOnClickListener { AndroidOp.startGettingImageFromGalleryForResult(this@OneJobStepActivity) }
        val alert = AlertDialog.Builder(this)
        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo")
            alert.setPositiveButton("Done") { dialogInterface, i ->
                //do nothing
            }
        } else {
            alert.setTitle("Complete action using")
            alert.setNegativeButton("Cancel") { dialog, which -> }
        }
        alert.setView(alertLayout)
        alert.setCancelable(false)
        imageDialog = alert.create()
        if (imageDialog?.isShowing == false) {
            imageDialog?.show()
        }
    }

    fun showImageAfterUpload(openImageAgain: Boolean) {
        //Update show image first


        //  totalImage = totalImage + 1;
        val url_maps = HashMap<String, String?>()

        // descriptionImageString = descriptionImage.get(totalImage - 1);
        imageBitmapArray.clear()
        slider!!.removeAllSliders()
        if (urlImageStringArray.size > 0) {
            for (i in urlImageStringArray.indices) {
                url_maps["" + urlDescriptionStringArray[i]] = urlImageStringArray[i]
            }
        }
        for (name in url_maps.keys) {
            val textSliderView = TextSliderView(this@OneJobStepActivity)
            // initialize a SliderLayout
            textSliderView
                .description(name)
                .image(url_maps[name])
                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                .setOnSliderClickListener(this@OneJobStepActivity)
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                .putString("extra", name)
            slider!!.addSlider(textSliderView)
        }
        slider!!.visibility = View.VISIBLE
        if (urlImageStringArray.size > 0) {
            slider!!.visibility = View.VISIBLE
            if (urlImageStringArray.size < 2) {
                Log.i("totalImage1", "" + urlImageStringArray.size)
                slider!!.stopAutoCycle()
                slider!!.setPagerTransformer(false, object : BaseTransformer() {
                    override fun onTransform(view: View, v: Float) {}
                })
                //
            } else if (urlImageStringArray.size > 1) {
                Log.i("totalImage2", "" + urlImageStringArray.size)
                slider!!.startAutoCycle()
                slider!!.setPagerTransformer(true, object : BaseTransformer() {
                    override fun onTransform(view: View, v: Float) {}
                })
            }
        } else if (urlImageStringArray.size == 0) {
            slider!!.removeAllSliders()
            slider!!.visibility = View.GONE
        }
        //Update show image first
        if (openImageAgain) {
            takeAnotherPhotoBool = false
            requestTakeOtherPhoto = true
            addImages()
        }
    }

    fun addDescription(data: Intent?, resultFromGalleryDescription: Boolean) {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.items_layout_description_image, null)
        val textViewTitle = alertLayout.findViewById<TextView>(R.id.textViewTitle)
        val editTextDescription: TextInputEditText =
            alertLayout.findViewById(R.id.editTextDescription)
        textViewTitle.text = "Add Description for Image"
        val builder = AlertDialog.Builder(this)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        builder.setPositiveButton(
            "OK"
        ) { dialog, which ->
            descriptionImageText = editTextDescription.text.toString()
            dialog.dismiss()
            if (!resultFromGalleryDescription) {
                try {
                    resultFromGallery = false
                    showImageCameraGallery(uriPhotoTakenByCamera)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                try {
                    resultFromGallery = true
                    val photoUri = data!!.data
                    showImageCameraGallery(photoUri)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    @Throws(IOException::class)
    fun showImageCameraGallery(uri: Uri?) {
        var bitmap: Bitmap?
        selectedUri = uri
        if (resultFromGallery) {
            val selectedImagePath = getFullPathFromContentUri(
                applicationContext, uri
            )
            bitmap = BitmapFactory.decodeFile(selectedImagePath)
        } else {
            bitmap = BitmapFactory.decodeFile(uri!!.path)
        }
        fileFromBitmap(bitmap!!)
    }

    fun showEditDeletePhotoGaleryImage() {
        val inflater = layoutInflater
        val alertLayout =
            inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null)
        val viewPager: ViewPager = alertLayout.findViewById(R.id.viewPager)
        myCustomPagerAdapter = EditDeleteOrderAttemptAdapter(
            this@OneJobStepActivity,
            urlImageStringArray,
            urlDescriptionStringArray
        )
        viewPager.adapter = myCustomPagerAdapter
        val builder = AlertDialog.Builder(this)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        builder.setPositiveButton(
            "DONE"
        ) { dialog, which -> }
        val dialog = builder.create()
        dialog.show()

        //Grab the window of the dialog, and change the width
        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = lp
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            dialog.dismiss()
            showImageAfterUpload(false)
        }
    }

    fun requestBody(imageFile: File?, selectedImagePath: String?, isSignature: Boolean) {
        val image = imageFile!!.asRequestBody("image/jpg".toMediaTypeOrNull())
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("picture", selectedImagePath, image)
            .addFormDataPart("picture_name", imageFile.name)
            .addFormDataPart("folder_name", "" + orderDetailId + " - " + jobSteps!!.orderId)
            .build()
        Log.e("JobStep picture_name", " : " + imageFile.name)
        Log.e("JobStep folder_name", " : " + orderDetailId + " - " + jobSteps!!.id)
        val progressListener = CountingRequestBody.Listener { bytesRead, contentLength ->
            if (bytesRead >= contentLength) {
            } else {
                if (contentLength > 0) {
                    val progress = (bytesRead.toDouble() / contentLength * 100).toInt()
                    Log.e("uploadProgress called", "$progress ")
                }
            }
        }
        val imageUploadClient: OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(Interceptor { chain ->
                val originalRequest = chain.request()
                if (originalRequest.body == null) {
                    return@Interceptor chain.proceed(originalRequest)
                }
                val progressRequest = originalRequest.newBuilder()
                    .method(
                        originalRequest.method,
                        CountingRequestBody(originalRequest.body, progressListener)
                    )
                    .build()
                chain.proceed(progressRequest)
            }).connectTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES)
            .build()
        val settings = PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )
        val accessToken = settings?.getString("access_token", "")
        val request: Request = Request.Builder()
            .url(SAAS_SERVER_AUTH + "/api/upload") //                .url("https://devstaging-auth-api.worknode.ai/api/upload")
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer $accessToken")
            .post(requestBody)
            .build()
        imageUploadClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    val mMessage = e.message
                    Toast.makeText(
                        this@OneJobStepActivity,
                        "Error uploading file",
                        Toast.LENGTH_LONG
                    ).show()
                    if (pdUpdateProcess != null && pdUpdateProcess!!.isShowing) {
                        pdUpdateProcess!!.dismiss()
                    }
                }
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                runOnUiThread {
                    Log.d("ImageUpload", body)
                    if (response.code == 200) {
                        runOnUiThread {
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            if (body == null || body.isEmpty()) {
                            } else {
                                try {
                                    val data = JSONObject(body)
                                    val resultData = data.getJSONObject("result")
                                    image_normalURL = resultData.getString("image_medium_url")

                                    /*check either it signature or image upload. If image upload, show image slider and add image again.
                                                                                If signature, direct update order.
                                                                                */if (isSignature) {
                                        val signatureOrderAttemptImage = OrderAttemptImage(
                                            image_normalURL,
                                            "Recipient’s Signature",
                                            false
                                        )
                                        orderAttempt!!.getmOrderAttemptImages()
                                            .add(signatureOrderAttemptImage)
                                        orderAttemptImagesForFailed.add(signatureOrderAttemptImage)
                                        var imageOrderAttemptImage: OrderAttemptImage
                                        //                                               orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);
                                        if (urlImageStringArray.size > 0) {
                                            for (i in urlImageStringArray.indices) {
                                                imageOrderAttemptImage = OrderAttemptImage(
                                                    urlImageStringArray[i],
                                                    urlDescriptionStringArray[i],
                                                    false
                                                )
                                                orderAttempt!!.getmOrderAttemptImages()
                                                    .add(imageOrderAttemptImage)
                                            }
                                        }

//                                                try {
//                                                    jsonStructureBatchUpdate();
//                                                } catch (IOException e) {
//                                                    e.printStackTrace();
//                                                }
                                    } else {
                                        val signatureOrderAttemptImage = OrderAttemptImage(
                                            image_normalURL,
                                            descriptionImageText,
                                            false
                                        )
                                        orderAttemptImagesForFailed.add(signatureOrderAttemptImage)
                                        urlImageStringArray.add(image_normalURL)
                                        urlDescriptionStringArray.add(descriptionImageText)
                                        showImageAfterUpload(false)
                                    }
                                } catch (e: JSONException) {
                                    LogCustom.e(e)
                                    try {
                                    } catch (t: Throwable) {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }

    fun checkAllInputInserted(): Boolean {
        if (slider!!.visibility == View.GONE && orderAttemptImagesForFailed.isEmpty()) {
            Toast.makeText(this, "Please add at least one image", Toast.LENGTH_SHORT).show()
            return false
        }
        //        if (!isItemSelected) {
//            Toast.makeText(this, "Please select a reason", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if (notes != null && notes.getText().toString().isEmpty()) {
//            Toast.makeText(this, "Please enter a note", Toast.LENGTH_SHORT).show();
//            return false;
//        }
        return true
    }

    fun showAlertManPower() {
        //  showErrorDialog(getActivity().getResources().getString(R.string.manPowerErrorTitle),getActivity().getResources().getString(R.string.manPowerErrorMessage)).show();
        val builder1 = AlertDialog.Builder(this)
        builder1.setMessage(resources.getString(R.string.manPowerErrorMessage))
        builder1.setCancelable(false)
        builder1.setTitle(resources.getString(R.string.manPowerErrorTitle))
        builder1.setPositiveButton(
            "OK"
        ) { dialog, id -> dialog.cancel() }
        val alert11 = builder1.create()
        alert11.show()
    }

    fun checkGPSBeforeUpdate(): Boolean {
        val alreadyOnGps: Boolean
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LogCustom.i("GPS is Enabled in your device true")
            alreadyOnGps = true
        } else {
            val editors = settings!!.edit()
            editors?.putBoolean("isOnline", false)?.apply()
            alreadyOnGps = false
            val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
            LocalBroadcastManager.getInstance(applicationContext)
                .sendBroadcast(ii)
        }
        return alreadyOnGps
    }

    fun completeThisJobProcess() {
        LogCustom.i("totalOrderItem scan", "" + totalOrderItem)
        if (jobSteps!!.is_scan_required && totalOrderItem != 0) {
            // need to scan first
            LogCustom.i("need scan", "11")
            if (totalOrderItem > 0 || scanRequiredForAllItem) {
                //need scan
                scanProcessAllItem(false)
            } else {
                btnStartOrCompletedJobStep!!.isEnabled = false
                jsonStructure(Constants.JOB_STEP_INPROGRESS)
            }
        } else {
            //not need to scan
            if (jobSteps!!.is_signature_required) {
                // need to sign
                LogCustom.i("need sign", "11")
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    openSignConfirmationPageWithNoInternet()
                } else {
                    openSignConfirmationPage()
                }
            } else {
                //not need to sign
                LogCustom.i("not need sign", "11")
                if (orderDetailId != 0 && jobStepId != 0) {
                    btnStartOrCompletedJobStep!!.isEnabled = false
                    val generalActivity = GeneralActivity()
                    if (!generalActivity.haveInternetConnected(applicationContext)) {
                        Log.i("internetConnection", "no")
                        jsonStructureCompleteWithNoInternet(Constants.JOB_STEP_COMPLETED, true)
                    } else {
                        jsonStructure(Constants.JOB_STEP_COMPLETED)
                    }
                } else {
                    showErrorDialog(
                        resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                            R.string.orderDetailError
                        )
                    ).show()
                }
            }
        }
    }

    fun jsonStructureCompleteWithNoInternet(nextOrderStatusID: Int, isCompletedJobStep: Boolean) {
        if (pdUpdateProcess != null) {
            if (pdUpdateProcess!!.isShowing) {
            } else {
                pdUpdateProcess!!.show()
            }
        }
        var jobOrder = JobOrder()
        val locale = Locale("en", "US")
        val printFormat = SimpleDateFormat("hh:mm a", locale)
        val dropOffTime = printFormat.format(Date())
        jobOrder = dbUpdateJobs!!.getJobDetailsBasedOnID(orderDetailId)
        if (isLastJobStep) {
            if (isCompletedJobStep) {
                jobOrder.orderStatusId =
                    dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.COMPLETED_TEXT)
                jobOrder.dropOffTimeEnd = dropOffTime
            } else {
                jobOrder.orderStatusId =
                    dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT)
                jobOrder.dropOffTimeStart = dropOffTime
            }
        } else {
            jobOrder.orderStatusId =
                dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT)
            jobOrder.dropOffTimeStart = dropOffTime
        }
        jobOrder.isUpdateWithNoInternet = true
        jobOrder.isReattemptJob = false
        dbUpdateJobs!!.addJobDetails(jobOrder)
        if (isCompletedJobStep) {
            jobSteps!!.job_step_status_id = Constants.JOB_STEP_COMPLETED
        } else {
            jobSteps!!.job_step_status_id = Constants.JOB_STEP_INPROGRESS
        }
        jobSteps!!.isJobStepUpdateWithNoInternet = true
        dbUpdateJobs!!.addJobStepDetails(jobSteps)
        if (isCompletedJobStep) {
            val uniqueIDOrderAttempt = (Date().time / 1000L % Int.MAX_VALUE).toInt()
            orderAttempt = OrderAttempt()
            orderAttempt!!.id = uniqueIDOrderAttempt
            val location = lastLocation
            if (location != null) {
                orderAttempt!!.setLatitude(location.latitude)
                orderAttempt!!.setLongitude(location.longitude)
            }
            orderAttempt!!.received_by = "-"
            orderAttempt!!.reason = ""
            orderAttempt!!.isAttemptUpdateWithNoInternet = true
            orderAttempt!!.jobStepID = jobSteps!!.id
            orderAttempt!!.orderId = orderDetailId
            val format2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val date = Date()
            val currentDate = format2.format(date)
            orderAttempt!!.submitted_time = currentDate
            dbUpdateJobs!!.addOrderAttemptDetails(orderAttempt)
        }
        jobSteps = dbUpdateJobs!!.getJobStepsDetailsBasedOnID(jobStepId)
        jobStepStatusId = jobSteps?.job_step_status_id ?: 0
        // finish();
        if (pdUpdateProcess != null) {
            if (pdUpdateProcess!!.isShowing) {
                pdUpdateProcess!!.dismiss()
            }
        }
        setupDisplayData(jobSteps)
    }

    /* ===================== CHECKING FUNCTION FOR EVERY STATUS ======================== */ /* ===================== CHECKING FUNCTION FOR EVERY STATUS ======================== */
    fun checkingFunctionBasedOnStatus(
        currentStatus: Int,
        toStatus: Int,
        isNeedScan: Boolean,
        rule: String?
    ) {
        if (dbUpdateJobs!!.checkStatusNeedScanOrManPower(
                currentStatus,
                toStatus,
                isNeedScan,
                rule
            )
        ) {
            LogCustom.i("Man Power")
            isManPowerCanUpdateProcess = true
        }
    }

    private fun fetchReasonListAndShowFailedDialog() {
        val dialog = SweetAlertDialog(this@OneJobStepActivity, SweetAlertDialog.PROGRESS_TYPE)
        dialog.show()
        OkHttpRequest.fetchRejectReasons(
            applicationContext, object : OKHttpNetwork {
                override fun onSuccess(body: String, responseCode: Int) {
                    try {
                        dialog.dismissWithAnimation()
                        val data = JSONObject(body)
                        if (data.has("status") && !data.isNull("status")) {
                            if (!data.getBoolean("status")) {
                                showErrorDialog(
                                    resources.getString(R.string.globalErrorMessageTitle),
                                    resources.getString(
                                        R.string.errorMessageUsernamePassword
                                    )
                                ).show()
                            } else {
                                val reasonList = ArrayList<String>()
                                reasonList.add("SELECT")
                                for (i in 0 until data.getJSONArray("result").length()) {
                                    reasonList.add(
                                        data.getJSONArray("result").getJSONObject(i)
                                            .optString("reject_reason")
                                    )
                                }
                                runOnUiThread { addReason(false, reasonList) }
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onFailure(
                    body: String,
                    requestCallBackError: Boolean,
                    responseCode: Int
                ) {
                    try {
                        dialog.dismissWithAnimation()
                        errorHandlingFromResponseCode(body, requestCallBackError, responseCode)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }

    fun addReason(isNoInternetConnection: Boolean, reasonList: ArrayList<String>?) {
        val builder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.add_reason_layout, null)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        val dialog = builder.create()
        val textViewTitle = alertLayout.findViewById<TextView>(R.id.textViewTitle)
        notes = alertLayout.findViewById(R.id.notes)
        val reasonSpinner = alertLayout.findViewById<Spinner>(R.id.reasonSpinner)
        reasonSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                isItemSelected = adapterView.selectedItem.toString() != "SELECT"
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, reasonList!!)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        reasonSpinner.adapter = adapter
        val btnCamera = alertLayout.findViewById<LinearLayout>(R.id.btnCamera)
        slider = alertLayout.findViewById(R.id.slider)
        val btnOk = alertLayout.findViewById<Button>(R.id.btnOk)
        val btnCancel = alertLayout.findViewById<Button>(R.id.btnCancel)
        btnCancel.setOnClickListener { dialog.dismiss() }
        btnOk.setOnClickListener(View.OnClickListener {
            if (!checkAllInputInserted()) {
                Toast.makeText(
                    this@OneJobStepActivity,
                    "Please add at least one image",
                    Toast.LENGTH_SHORT
                ).show()
                return@OnClickListener
            }
            if (isNoInternetConnection) {
                alert!!.dismiss()
                val location = lastLocation
                if (location == null) {
                    buildAlertMessageNoGps()
                } else {
                    val locale = Locale("en", "US")
                    val printFormat = SimpleDateFormat("hh:mm a", locale)
                    val dropOffTime = printFormat.format(Date())
                    val jobOrder = JobOrder()
                    jobOrder.orderId = orderDetailId
                    jobOrder.orderStatusId = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                        Constants.FAILED_TEXT
                    )
                    jobOrder.isUpdateWithNoInternet = true
                    jobOrder.dropOffTimeEnd = dropOffTime
                    jobOrder.isReattemptJob = false
                    dbUpdateJobs!!.addJobDetails(jobOrder)
                    jobSteps!!.job_step_status_id = Constants.JOB_STEP_FAILED
                    jobSteps!!.isJobStepUpdateWithNoInternet = true
                    dbUpdateJobs!!.addJobStepDetails(jobSteps)
                    val uniqueIDOrderAttempt = (Date().time / 1000L % Int.MAX_VALUE).toInt()
                    orderAttempt = OrderAttempt()
                    orderAttempt!!.id = uniqueIDOrderAttempt
                    orderAttempt!!.setLatitude(location.latitude)
                    orderAttempt!!.setLongitude(location.longitude)
                    orderAttempt!!.reason = reasonSpinner.selectedItem.toString()
                    orderAttempt!!.note = notes?.text.toString()
                    orderAttempt!!.isAttemptUpdateWithNoInternet = true
                    orderAttempt!!.jobStepID = jobStepId
                    orderAttempt!!.orderId = orderDetailId
                    val format2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val date = Date()
                    val currentDate = format2.format(date)
                    orderAttempt!!.submitted_time = currentDate
                    dbUpdateJobs!!.addOrderAttemptDetails(orderAttempt)
                    jobSteps = dbUpdateJobs!!.getJobStepsDetailsBasedOnID(jobStepId)
                    jobStepStatusId = jobSteps?.job_step_status_id ?: 0
                    setupDisplayData(jobSteps)
                }
            } else {
                reasonFailed = ""
                dialog.dismiss()
                reasonFailed = reasonSpinner.selectedItem.toString()
                btnFailedJobStep!!.isEnabled = false
                jsonStructure(Constants.JOB_STEP_FAILED)
            }
        })
        textViewTitle.text = "Enter a reason to fail this job"
        btnCamera.setOnClickListener {
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {
                Log.i("internetConnection", "no")
                showErrorInternetDialog().show()
            } else {
                addImages()
            }
        }
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).visibility = View.GONE
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    /*===== SCAN PROCESS ======= */ /*
     * This is for aleart dialog.
     * */
    /* ================================ SWEET ALERT DIALOG =================================== */
    fun scanProcessAllItem(changeToInprogress: Boolean) {
        val intent = Intent().setClass(this, ScanDoActivity::class.java)
        intent.putExtra("trackingNumber", trackingNumber)
        intent.putExtra("totalOrderItem", totalOrderItem)
        intent.putExtra("changeToInprogress", changeToInprogress)
        startActivityForResult(intent, Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM)
    }

    /* ================================ ONLINE / OFFLINE LOCATION SETTINGS =================================== */
    fun checkingDriverStatusAndLocationStatus() {
        //if status online, location must on. else must be offline
        val isOnline = settings!!.getBoolean("isOnline", true)
        if (isOnline) {
            val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true")
                val editors = settings!!.edit()
                editors?.putBoolean("isOnline", true)?.apply()
                startLocationServices()
            } else {
                // GPS not open
                LogCustom.i("GPS Not Enabled in your device true")
                val editors = settings!!.edit()
                editors?.putBoolean("isOnline", false)?.apply()
                stopLocationServices()
            }
        } else {
            val editors = settings!!.edit()
            editors?.putBoolean("isOnline", false)?.apply()
            stopLocationServices()
        }
    }

    fun startLocationServices() {
        val generalActivity = GeneralActivity()
        if (!generalActivity.isMyServiceLocationRunning(applicationContext)) {
            LogCustom.i("isMyServiceRunning need to start", "true")
            val intent = Intent(this@OneJobStepActivity, ForegroundLocationService::class.java)
            startService(intent)
        } else {
            LogCustom.i("isMyServiceRunning already, not need to start", "true")
        }
        updateStatusOnlineOffline(true)
    }

    fun checkingDriverStatusAndLocationStatusToggle() {
        //if status online, location must on. else must be offline
        val isOnline = settings!!.getBoolean("isOnline", true)
        if (isOnline) {
            val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true")
                val editors = settings!!.edit()
                editors?.putBoolean("isOnline", true)?.apply()
                val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(ii)
                startLocationServices()
            } else {
                showGPSDisabledAlertToUser()
            }
        } else {
            val editors = settings!!.edit()
            editors?.putBoolean("isOnline", false)?.apply()
            stopLocationServices()
        }
    }

    fun updateStatusOnlineOffline(is_online: Boolean) {
        try {
            val jsonObject = JSONObject()
            LogCustom.i("updateStatusOnlineOffline", "is_online : $is_online")
            jsonObject.put("is_online", is_online)
            jsonObject.put("latitude", "")
            jsonObject.put("longitude", "")
            val okHttpRequest = OkHttpRequest()
            try {
                OkHttpRequest.driverUpdateProfile(
                    applicationContext, jsonObject.toString(), object : OKHttpNetwork {
                        override fun onSuccess(body: String, responseCode: Int) {
                            runOnUiThread {
                                if (body == null || body.isEmpty()) {
                                    LogCustom.i("valueIsNull", "yes")
                                } else {
                                    LogCustom.i("update profile is success", "yes")
                                }
                            }
                        }

                        override fun onFailure(
                            body: String,
                            requestCallBackError: Boolean,
                            responseCode: Int
                        ) {
                            runOnUiThread {
                                errorHandlingFromResponseCode(
                                    body,
                                    requestCallBackError,
                                    responseCode
                                )
                            }
                        }
                    })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } catch (e: Exception) {
        }
    }

    fun stopLocationServices() {
        val generalActivity = GeneralActivity()
        if (generalActivity.isMyServiceLocationRunning(applicationContext)) {
            LogCustom.i("isMyServiceRunning need to stop", "true")
            val intent = Intent(this@OneJobStepActivity, ForegroundLocationService::class.java)
            stopService(intent)
        } else {
            LogCustom.i("isMyServiceRunning not need to stop", "true")
        }
        updateStatusOnlineOffline(false)
    }

    private fun showGPSDisabledAlertToUser() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
            .setCancelable(false)
            .setPositiveButton(
                "Settings"
            ) { dialog, id ->
                val callGPSSettingIntent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
                )
                startActivityForResult(callGPSSettingIntent, Constants.INTENT_ACTIVITY_GPS_SETTINGS)
            }
        alertDialogBuilder.setNegativeButton(
            "Cancel"
        ) { dialog, id ->
            dialog.cancel()
            val editors = settings!!.edit()
            editors?.putBoolean("isOnline", false)?.apply()
            stopLocationServices()
        }
        val alert = alertDialogBuilder.create()
        alert.show()
    }

    fun showNeedOnlineDialog(titleMessage: String?, contentMessage: String?): SweetAlertDialog {
        val sweetAlertDialog = getNeedOnlineDialog(titleMessage, contentMessage)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    /* ================================ ONLINE / OFFLINE LOCATION SETTINGS =================================== */
    fun getNeedOnlineDialog(titleMessage: String?, contentMessage: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setContentText(contentMessage)
            .setTitleText(titleMessage)
            .setConfirmText("Yes")
            .setConfirmClickListener { sDialog ->
                val editors = settings!!.edit()
                editors?.putBoolean("isOnline", true)?.apply()
                checkingDriverStatusAndLocationStatusToggle()
                sDialog.dismissWithAnimation()
            }
            .setCancelText("No")
            .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    /* =========  DIALOG POPUP =========== */
    fun showErrorDialog(titleError: String?, contextError: String?): SweetAlertDialog {
        val sweetAlertDialog = getErrorDialog(titleError, contextError)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    fun getErrorDialog(titleError: String?, contextError: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setContentText(contextError)
            .setTitleText(titleError)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                if (unauthorizedNeedToLoginPage) {
                    val generalActivity = GeneralActivity()
                    if (generalActivity.isMyServiceLocationRunning(applicationContext)) {
                        LogCustom.i("isMyServiceRunning", "true")
                        val intent = Intent(
                            applicationContext, ForegroundLocationService::class.java
                        )
                        stopService(intent)
                    }
                    settings!!.edit().remove("isOnline").apply()
                    settings!!.edit().remove("googleToken").apply()
                    settings!!.edit().putString("user_name", "").commit()
                    unauthorizedNeedToLoginPage = false
                    val i = Intent(applicationContext, LoginActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    i.putExtra("EXIT", true)
                    startActivity(i)
                }
            }
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    ///success dialog
    fun showSuccessDialog(title: String?, context: String?): SweetAlertDialog {
        val sweetAlertDialog = getSuccessDialog(title, context)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    /* =========  DIALOG POPUP =========== */
    fun getSuccessDialog(title: String?, context: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setContentText(context)
            .setTitleText(title)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog -> sDialog.dismissWithAnimation() }
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    fun showErrorInternetDialog(): SweetAlertDialog {
        val sweetAlertDialog = errorInternetDialog
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    val errorInternetDialog: SweetAlertDialog
        get() {
            val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener {
                    val intent = Intent(Intent.ACTION_MAIN, null)
                    intent.addCategory(Intent.CATEGORY_LAUNCHER)
                    val cn = ComponentName(
                        "com.android.settings",
                        "com.android.settings.wifi.WifiSettings"
                    )
                    intent.component = cn
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
                .setCancelText("Cancel")
                .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
            sweetAlertDialog.setCancelable(false)
            return sweetAlertDialog
        }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, please turn it on to update job status.")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    fun jsonStructure(nextJobStepStatusId: Int) {
        try {
            val rootObject = JSONObject()
            val childArray = JSONArray()

            // put array of id in object ID
            val childObject = JSONObject()
            val idArray = JSONArray()
            idArray.put(orderDetailId.toString())
            childObject.put("id", idArray)
            if (isLastJobStep) {
                if (nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.COMPLETED_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.INPROGRESS_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_FAILED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.FAILED_TEXT
                        )
                    )
                }
            } else {
                if (nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.INPROGRESS_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.INPROGRESS_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_FAILED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.FAILED_TEXT
                        )
                    )
                }
            }
            val jobStepDetailsObject: JSONObject
            val orderDetailArray = JSONArray()
            jobStepDetailsObject = JSONObject()
            orderAttempt = OrderAttempt()
            val locale = Locale("en", "US")
            val printFormat = SimpleDateFormat("hh:mm a", locale)
            if (nextJobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                val dropOffTime = printFormat.format(Date())
                LogCustom.i("time", "currentTime$dropOffTime")
                childObject.put("drop_off_time", dropOffTime)
                jobStepDetailsObject.put("job_step_id", jobStepId)
                jobStepDetailsObject.put("job_step_status_id", nextJobStepStatusId)
                orderDetailArray.put(jobStepDetailsObject)
                childObject.put("job_steps", orderDetailArray)
                childArray.put(childObject)
                rootObject.put("data", childArray)
                LogCustom.i("JsonObjectStructure", rootObject.toString(4))
                updateBatchOrder(rootObject)
            } else if (nextJobStepStatusId == Constants.JOB_STEP_FAILED || nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                val location = lastLocation
                LogCustom.i("location", "latitude" + location!!.latitude)
                if (location == null) {
                    buildAlertMessageNoGps()
                } else {
                    val dropOffTime = printFormat.format(Date())
                    LogCustom.i("time", "currentTime$dropOffTime")
                    childObject.put("drop_off_time_end", dropOffTime)
                    if (nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                        orderAttempt!!.received_by = "-"
                    }
                    orderAttempt!!.setLatitude(location.latitude)
                    orderAttempt!!.setLongitude(location.longitude)
                    if (nextJobStepStatusId == Constants.JOB_STEP_FAILED) {
                        orderAttempt!!.reason = reasonFailed
                        orderAttempt!!.setmOrderAttemptImages(orderAttemptImagesForFailed)
                    }
                    val orderAttemptArray = JSONArray()
                    val DATEFORMAT = "yyyy-MM-dd HH:mm:ss"
                    val sdf = SimpleDateFormat(DATEFORMAT)
                    sdf.timeZone = TimeZone.getTimeZone("UTC")
                    val utcTime = sdf.format(Date())
                    orderAttempt!!.submitted_time = utcTime
                    orderAttemptArray.put(orderAttempt!!.jsonObjectJobStepFailed)
                    jobStepDetailsObject.put("step_attempts", orderAttemptArray)
                    jobStepDetailsObject.put("job_step_id", jobStepId)
                    jobStepDetailsObject.put("job_step_status_id", nextJobStepStatusId)
                    orderDetailArray.put(jobStepDetailsObject)
                    childObject.put("job_steps", orderDetailArray)
                    childArray.put(childObject)
                    rootObject.put("data", childArray)
                    LogCustom.i("JsonObjectStructure", rootObject.toString(4))
                    updateBatchOrder(rootObject)
                }
            }
        } catch (ex: Exception) {
            setEnableAllButton()
            ex.printStackTrace()
        }
    }

    @Throws(IOException::class)
    fun updateBatchOrder(jsonObject: JSONObject) {

        //jobStepId = 183;
        contextProgressDialog!!.text = resources.getString(R.string.updateJobSteps)
        if (pdUpdateProcess != null) {
            if (pdUpdateProcess!!.isShowing) {
            } else {
                pdUpdateProcess!!.show()
            }
        }
        val okHttpRequest = OkHttpRequest()
        try {
            OkHttpRequest.batchUpdate(
                applicationContext, jsonObject.toString(), object : OKHttpNetwork {
                    override fun onSuccess(body: String, responseCode: Int) {
                        runOnUiThread {
                            setEnableAllButton()
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            if (body == null || body.isEmpty()) {
                            } else {
                                try {
                                    val data = JSONObject(body)
                                    val resultData = data.getJSONArray("result")
                                    LogCustom.i("dataaaBatchUpdate", data.toString())
                                    for (i in 0 until resultData.length()) {
                                        val jsonObject2 = resultData[i] as JSONObject
                                        val jobStepsArray = jsonObject2.getJSONArray("job_steps")
                                        for (j in 0 until jobStepsArray.length()) {
                                            val jsonObject3 = jobStepsArray[j] as JSONObject
                                            if (jsonObject3.getInt("id") == jobStepId) {
                                                jobSteps = JobSteps(jsonObject3)
                                                jobStepStatusId = jobSteps!!.job_step_status_id
                                            }
                                        }
                                    }
                                    LogCustom.i("dataaaStatusId", jobSteps!!.job_step_status_id)
                                    if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                                        showSuccessDialog(
                                            resources.getString(R.string.jobStepStartedTitle),
                                            resources.getString(
                                                R.string.jobStepStartedMessage
                                            )
                                        ).show()
                                    } else if (jobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                                        if (isLastJobStep) {
                                            showSuccessDialog(
                                                resources.getString(R.string.jobLastStepCompletedTitle),
                                                resources.getString(
                                                    R.string.jobLastStepCompletedMessage
                                                )
                                            ).show()
                                        } else {
                                            showSuccessDialog(
                                                resources.getString(R.string.jobStepCompletedTitle),
                                                resources.getString(
                                                    R.string.jobStepCompletedMessage
                                                )
                                            ).show()
                                        }
                                    } else if (jobStepStatusId == Constants.JOB_STEP_FAILED) {
                                        showSuccessDialog(
                                            resources.getString(R.string.jobFailedTitle),
                                            resources.getString(
                                                R.string.jobFailedMessage
                                            )
                                        ).show()
                                    }
                                    setupDisplayData(jobSteps)
                                    invalidateOptionsMenu()
                                    //finish();
                                } catch (e: JSONException) {
                                    LogCustom.e(e)
                                    try {
                                    } catch (t: Throwable) {
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(
                        body: String,
                        requestCallBackError: Boolean,
                        responseCode: Int
                    ) {
                        val jsonResponse = body
                        runOnUiThread {
                            setEnableAllButton()
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            errorHandlingFromResponseCode(body, requestCallBackError, responseCode)
                        }
                    }
                })
        } catch (e: Exception) {
            setEnableAllButton()
            e.printStackTrace()
        }
    }

    fun setEnableAllButton() {
        btnFailedJobStep!!.isEnabled = true
        btnStartOrCompletedJobStep!!.isEnabled = true
    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */ /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    fun errorHandlingFromResponseCode(
        body: String?,
        requestCallBackError: Boolean,
        responseCode: Int
    ) {
        runOnUiThread {
            if (!requestCallBackError) {
                unauthorizedNeedToLoginPage = false
                try {
                    var errorMessage = ""
                    var data: JSONObject? = null
                    if (body != null || !body.equals("", ignoreCase = true)) {
                        try {
                            data = JSONObject(body)
                        } catch (e: Exception) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitle),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        }
                        if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                            if (data!!.has("error") && !data.isNull("error")) {
                                errorMessage = try {
                                    data.getString("error")
                                } catch (e: Exception) {
                                    val errorMessageArray = data.getJSONObject("error")
                                    errorMessageArray.getString("error")
                                }
                            }
                            if (errorMessage.equals("", ignoreCase = true)) {
                                errorMessage = resources.getString(R.string.pleaseContactAdmin)
                            }
                            showErrorDialog(
                                resources.getString(R.string.badRequestErrorTitle),
                                errorMessage
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {
                            unauthorizedNeedToLoginPage = true
                            if (data!!.has("device_not_found") && !data.isNull("device_not_found")) {
                                if (data.getBoolean("device_not_found")) {
                                    showErrorDialog(
                                        resources.getString(R.string.deviceNotFoundTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else if (data.has("device_is_banned") && !data.isNull("device_is_banned")) {
                                if (data.getBoolean("device_is_banned")) {
                                    showErrorDialog(
                                        resources.getString(R.string.deviceBannedTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else {
                                showErrorDialog(
                                    resources.getString(R.string.errorLogin), resources.getString(
                                        R.string.pleaseCheckPassword
                                    )
                                ).show()
                            }
                        } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {
                            unauthorizedNeedToLoginPage = true
                            if (data!!.has("unpaid_subscription") && !data.isNull("unpaid_subscription")) {
                                if (data.getBoolean("unpaid_subscription")) {
                                    showErrorDialog(
                                        resources.getString(R.string.unpaidSubscriptionTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else if (data.has("quota_reached") && !data.isNull("quota_reached")) {
                                if (data.getBoolean("quota_reached")) {
                                    showErrorDialog(
                                        resources.getString(R.string.quotaReachedTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else if (data.has("blacklist") && !data.isNull("blacklist")) {
                                if (data.getBoolean("blacklist")) {
                                    showErrorDialog(
                                        resources.getString(R.string.blacklistTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else {
                                showErrorDialog(
                                    resources.getString(R.string.errorLogin), resources.getString(
                                        R.string.pleaseCheckPassword
                                    )
                                ).show()
                            }
                        } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorTimeoutMessageContent
                                )
                            ).show()
                        }
                    } else {
                        showErrorDialog(
                            resources.getString(R.string.globalErrorMessageTitle),
                            resources.getString(
                                R.string.globalErrorMessageContent
                            )
                        ).show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                    showErrorDialog(
                        resources.getString(R.string.globalErrorMessageTitleSorry),
                        resources.getString(
                            R.string.globalErrorMessageContent
                        )
                    ).show()
                } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                    showErrorInternetDialog().show()
                } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                    showErrorDialog(
                        resources.getString(R.string.globalErrorMessageTitleSorry),
                        resources.getString(
                            R.string.globalErrorTimeoutMessageContent
                        )
                    ).show()
                } else {
                    showErrorDialog(
                        resources.getString(R.string.globalErrorMessageTitleSorry),
                        resources.getString(
                            R.string.globalErrorMessageContent
                        )
                    ).show()
                }
                // this for request call back error. Maybe because cannot connect server.
            }
        }
    }

    fun openSignConfirmationPage() {
        if (orderDetailId != 0) {
            val i = Intent(applicationContext, JobStepSignConfirmation::class.java)
            i.putExtra("jobSteps", jobSteps)
            i.putExtra("OrderId", orderDetailId)
            i.putExtra("IsFromOrderDetails", true)
            i.putExtra("trackingNumber", trackingNumber)
            i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
            i.putExtra("startJobStepsButton", startJobStepsButton)
            i.putExtra("isLastJobStep", isLastJobStep)
            startActivityForResult(i, Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION)
        } else {
            showErrorDialog(
                resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                    R.string.orderDetailError
                )
            ).show()
        }
    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */ // After scan, should check either that step need to sign or not
    fun openSignConfirmationPageWithNoInternet() {
        if (orderDetailId != 0) {
            val i = Intent(applicationContext, JobStepSignConfirmationWithNoInternet::class.java)
            i.putExtra("jobSteps", jobSteps)
            i.putExtra("OrderId", orderDetailId)
            i.putExtra("IsFromOrderDetails", true)
            i.putExtra("trackingNumber", trackingNumber)
            i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
            i.putExtra("startJobStepsButton", startJobStepsButton)
            i.putExtra("isLastJobStep", isLastJobStep)
            startActivityForResult(i, Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION)
        } else {
            showErrorDialog(
                resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                    R.string.orderDetailError
                )
            ).show()
        }
    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("onActivityResult", "on activity result:$requestCode")
        when (requestCode) {
            Constants.INTENT_ACTIVITY_SCAN -> {
                val doneUpdateScan = data!!.extras!!.getBoolean("doneUpdateScan")
                LogCustom.i("doneUpdateScan", "Answer: $doneUpdateScan")

                //successfully to scan
                if (doneUpdateScan) {
                    if (jobSteps!!.is_signature_required) {
                        // need to sign
                        val generalActivity = GeneralActivity()
                        if (!generalActivity.haveInternetConnected(applicationContext)) {
                            Log.i("internetConnection", "no")
                            openSignConfirmationPageWithNoInternet()
                        } else {
                            openSignConfirmationPage()
                        }
                    } else {
                        //not need to sign
                        val generalActivity = GeneralActivity()
                        if (!generalActivity.haveInternetConnected(applicationContext)) {
                            Log.i("internetConnection", "no")
                            jsonStructureCompleteWithNoInternet(Constants.JOB_STEP_COMPLETED, true)
                        } else {
                            jsonStructure(Constants.JOB_STEP_COMPLETED)
                        }
                    }
                } else {
                    // need to scan first if want to proceed process
                }
            }
            Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM -> {
                val doneUpdateScanAllItem = data!!.extras!!.getBoolean("doneUpdateScan")
                val changeToInprogress = data.extras!!.getBoolean("changeToInprogress")
                LogCustom.i("doneUpdateScan", "Answer: $doneUpdateScanAllItem")
                LogCustom.i("changeToInprogress", "Answer: $changeToInprogress")

                //successfully to scan
                if (doneUpdateScanAllItem) {
                    if (changeToInprogress) {
                        val generalActivity = GeneralActivity()
                        if (!generalActivity.haveInternetConnected(applicationContext)) {
                            Log.i("internetConnection", "no")
                            jsonStructureCompleteWithNoInternet(
                                Constants.JOB_STEP_INPROGRESS,
                                false
                            )
                        } else {
                            Toast.makeText(
                                applicationContext,
                                resources.getString(R.string.updateJobSteps),
                                Toast.LENGTH_LONG
                            ).show()
                            jsonStructure(Constants.JOB_STEP_INPROGRESS)

//                            Handler handler = new Handler();
//                            handler.post(new Runnable() {
//                                @SuppressWarnings("unchecked")
//                                public void run() {
//                                    try {
//                                        contextProgressDialog.setText(getResources().getString(R.string.updateJobSteps));
//                                        pdUpdateProcess.show();
//
//
//                                    }
//                                    catch (Exception e) {
//                                        // TODO Auto-generated catch block
//                                    }
//                                }
//                            });
                        }
                    } else {
                        if (jobSteps!!.is_signature_required) {
                            // need to sign
                            val generalActivity = GeneralActivity()
                            if (!generalActivity.haveInternetConnected(applicationContext)) {
                                Log.i("internetConnection", "no")
                                openSignConfirmationPageWithNoInternet()
                            } else {
                                openSignConfirmationPage()
                            }
                        } else {
                            //not need to sign
                            val generalActivity = GeneralActivity()
                            if (!generalActivity.haveInternetConnected(applicationContext)) {
                                Log.i("internetConnection", "no")
                                jsonStructureCompleteWithNoInternet(
                                    Constants.JOB_STEP_COMPLETED,
                                    true
                                )
                            } else {
                                jsonStructure(Constants.JOB_STEP_COMPLETED)
                            }
                        }
                    }
                } else {
                    // need to scan first if want to proceed process
                }
            }
            Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION -> {
                if (data!!.extras != null) {
                    showCompletedFailedButton =
                        data.extras!!.getBoolean("showCompletedFailedButton")
                    startJobStepsButton = data.extras!!.getBoolean("startJobStepsButton")
                    trackingNumber = data.extras!!.getString("trackingNumber")
                    jobStepStatusId = data.extras!!.getInt("jobStepStatusId")
                    jobStepId = data.extras!!.getInt("jobStepId")
                    orderDetailId = data.extras!!.getInt("OrderId")
                    try {
                        val generalActivity = GeneralActivity()
                        if (!generalActivity.haveInternetConnected(applicationContext)) {
                            Log.i("internetConnection", "no")
                            //  showErrorInternetDialog().show();
                            wantProceedIfNoInternet(1)
                        } else {
                            if (jobStepId != 0) {
                                jobStepDetails
                            } else {
                                showErrorDialog(
                                    resources.getString(R.string.jobStepErrorTitle),
                                    resources.getString(
                                        R.string.jobStepDetailError
                                    )
                                ).show()
                            }
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    return
                }
                checkingDriverStatusAndLocationStatus()
                val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(ii)
            }
            Constants.INTENT_ACTIVITY_GPS_SETTINGS -> {
                checkingDriverStatusAndLocationStatus()
                val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(ii)
            }
        }
        if (resultCode != RESULT_OK) return
        when (requestCode) {
            AndroidOp.REQUEST_CODE_CAMERA -> {
                Log.i("requset", "request code camera")
                resultFromGallery = false
                if (imageDialog == null) {
                } else {
                    if (imageDialog!!.isShowing) {
                        imageDialog!!.dismiss()
                    }
                }

                addDescription(data, resultFromGallery)
            }
            AndroidOp.REQUEST_CODE_GALLERY -> {
                Log.i("", "request code gallery")
                resultFromGallery = true
                if (imageDialog == null) {
                } else {
                    if (imageDialog!!.isShowing) {
                        imageDialog!!.dismiss()
                    }
                }
                addDescription(data, resultFromGallery)
            }
            80 -> {}
        }
    }

    override fun onResume() {
        super.onResume()
        if (alert != null) if (alert!!.isShowing) {
            alert!!.dismiss()
        }
        try {
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {

                //  showErrorInternetDialog().show();
                wantProceedIfNoInternet(1)
            } else {
                LogCustom.i("jobStepId ", ":$jobStepId")
                if (jobStepId != 0) {
                    jobStepDetails
                } else {
                    showErrorDialog(
                        resources.getString(R.string.jobStepErrorTitle), resources.getString(
                            R.string.jobStepDetailError
                        )
                    ).show()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED
            ) {
                buildGoogleApiClient()
                // mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient()
            // mMap.setMyLocationEnabled(true);
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient?.connect()
    }

    @SuppressLint("RestrictedApi")
    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority =
            LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback,
                Looper.myLooper()
            )

            // LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    override fun onConnectionSuspended(i: Int) {}
    override fun onLocationChanged(location: Location) {
        lastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }
        //stop location updates
        if (mGoogleApiClient != null) {
            // LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
        }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}
    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Output format
        val output = "json"

        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters"
    }

    /**
     * A method to download json data from url
     */
    @Throws(IOException::class)
    private fun downloadUrl(strUrl: String): String {
        var data = ""
        var iStream: InputStream? = null
        var urlConnection: HttpURLConnection? = null
        try {
            val url = URL(strUrl)

            // Creating an http connection to communicate with url
            urlConnection = url.openConnection() as HttpURLConnection

            // Connecting to url
            urlConnection.connect()

            // Reading data from url
            iStream = urlConnection.inputStream
            val br = BufferedReader(InputStreamReader(iStream))
            val sb = StringBuffer()
            var line: String? = ""
            while (br.readLine().also { line = it } != null) {
                sb.append(line)
            }
            data = sb.toString()
            br.close()
        } catch (e: Exception) {
            Log.d("Exception ", e.toString())
        } finally {
            iStream!!.close()
            urlConnection!!.disconnect()
        }
        return data
    }

    /**
     * A class to download data from Google Directions URL
     */
    //    private class DownloadTask extends AsyncTask<String, Void, String> {
    //
    //        // Downloading data in non-ui thread
    //        @Override
    //        protected String doInBackground(String... url) {
    //
    //            // For storing data from web service
    //            String data = "";
    //
    //            try{
    //                // Fetching the data from web service
    //                data = downloadUrl(url[0]);
    //            }catch(Exception e){
    //                Log.d("Background Task",e.toString());
    //            }
    //            return data;
    //        }
    //
    //        // Executes in UI thread, after the execution of
    //        // doInBackground()
    //        @Override
    //        protected void onPostExecute(String result) {
    //            super.onPostExecute(result);
    //
    //            ParserTask parserTask = new ParserTask();
    //
    //            // Invokes the thread for parsing the JSON data
    //            parserTask.execute(result);
    //        }
    //    }
    fun checkLocationPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        )
                        == PackageManager.PERMISSION_GRANTED
                    ) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient()
                        }
                        mMap!!.isMyLocationEnabled = true
                    }
                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        menuInflater.inflate(R.menu.job_steps_details, menu)
        //  return true;
        return super.onCreateOptionsMenu(menu)

        //return true;
    }

    override fun onBackPressed() {
        val i = Intent()
        i.putExtra("OrderId", orderDetailId)
        setResult(Constants.INTENT_ACTIVITY_SCAN, i)
        finish()
        // super.onBackPressed();
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @get:Throws(IOException::class)
    val jobStepDetails: Unit
        get() {
            contextProgressDialog!!.text =
                resources.getString(R.string.loadingupdateJobStepsDetails)
            pdUpdateProcess!!.show()
            val okHttpRequest = OkHttpRequest()
            try {
                OkHttpRequest.getJobStepDetails(
                    applicationContext, jobStepId, object : OKHttpNetwork {
                        override fun onSuccess(body: String, responseCode: Int) {
                            runOnUiThread {
                                if (pdUpdateProcess != null) {
                                    if (pdUpdateProcess!!.isShowing) {
                                        pdUpdateProcess!!.dismiss()
                                    }
                                }
                                if (body == null || body.isEmpty()) {
                                } else {
                                    try {
                                        val data = JSONObject(body)
                                        LogCustom.i("dataaa", data.toString())
                                        if (data.has("status") && !data.isNull("status")) {
                                            if (data.getBoolean("status")) {
                                                val jsonObject = data.getJSONObject("result")
                                                jobSteps = JobSteps(jsonObject)
                                                jobStepStatusId = jobSteps!!.job_step_status_id
                                                setupDisplayData(jobSteps)
                                            }
                                        } else {
                                        }
                                    } catch (e: JSONException) {
                                        LogCustom.e(e)
                                        try {
                                        } catch (t: Throwable) {
                                        }
                                    }
                                }
                            }
                        }

                        override fun onFailure(
                            body: String,
                            requestCallBackError: Boolean,
                            responseCode: Int
                        ) {
                            val jsonResponse = body
                            runOnUiThread {
                                if (pdUpdateProcess != null) {
                                    if (pdUpdateProcess!!.isShowing) {
                                        pdUpdateProcess!!.dismiss()
                                    }
                                }
                                errorHandlingFromResponseCode(
                                    body,
                                    requestCallBackError,
                                    responseCode
                                )
                            }
                        }
                    })
            } catch (e: Exception) {
                if (pdUpdateProcess != null) {
                    if (pdUpdateProcess!!.isShowing) {
                        pdUpdateProcess!!.dismiss()
                    }
                }
                e.printStackTrace()
            }
        }

    fun setupDisplayData(jobSteps: JobSteps?) {
        totalAttemptJobStep!!.text = "" + jobSteps!!.getmOrderAttemptsList().size
        LogCustom.i("jobStepPic", "" + jobSteps.job_step_pic)
        if (jobSteps.job_step_pic == null || jobSteps.job_step_pic.equals("", ignoreCase = true)) {
            jobStepPic!!.visibility = View.GONE
        } else {
            jobStepPic!!.text = jobSteps.job_step_pic
        }
        if (companyName.equals("", ignoreCase = true)) {
            jobStepCompany!!.visibility = View.GONE
        } else {
            jobStepCompany!!.text = jobSteps.job_step_pic_contact
        }
        if (!intent.getBooleanExtra("isSingleJobStep", false)) {
            if (jobSteps.job_step_pic_contact == null || jobSteps.job_step_pic_contact.equals(
                    "",
                    ignoreCase = true
                )
            ) {
                jobStepContact!!.visibility = View.GONE
                layoutClickCall!!.visibility = View.GONE
                layoutClickWhatsApp!!.visibility = View.GONE
            } else {
                jobStepContact!!.text = jobSteps.job_step_pic_contact
                jobStepWhatsApp!!.text = jobSteps.job_step_pic_contact
            }
        } else {
            if (jobSteps.job_step_pic_contact == null || jobSteps.job_step_pic_contact.equals(
                    "",
                    ignoreCase = true
                )
            ) {
                jobSteps.job_step_pic_contact = intent.getStringExtra("contact")
            }
            jobStepContact!!.text = jobSteps.job_step_pic_contact
            jobStepWhatsApp!!.text = jobSteps.job_step_pic_contact
        }
        if (jobSteps.location.equals("", ignoreCase = true)) {
            jobStepNavigate!!.text = " - "
        } else {
            jobStepNavigate!!.text = jobSteps.location
        }
        if (jobSteps.description.equals("", ignoreCase = true)) {
            jobStepDescription!!.visibility = View.GONE
        } else {
            jobStepDescription!!.text = jobSteps.description
        }
        if (trackingNumber.equals("", ignoreCase = true)) {
            jobStepTrackingNo!!.text = "Tracking number not available"
        } else {
            jobStepTrackingNo!!.text = trackingNumber
        }
        if (jobSteps.location.equals("", ignoreCase = true)) {
            jobStepAddress!!.visibility = View.GONE
        } else {
            jobStepAddress!!.text = jobSteps.location
        }
        if (showCompletedFailedButton) {
            if (startJobStepsButton) {
                btnStartOrCompletedJobStep!!.text = "Start this Job Step"
            } else {
                // complete button
                btnStartOrCompletedJobStep!!.text = "Complete this Job Step"
            }
            btnFailedJobStep!!.visibility = View.VISIBLE
            btnStartOrCompletedJobStep!!.visibility = View.VISIBLE
        } else {
            // not need to show button
            btnFailedJobStep!!.visibility = View.GONE
            btnStartOrCompletedJobStep!!.visibility = View.GONE
        }
        if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
            jobStepStatus!!.text = resources.getText(R.string.pendingJobStepCapital)
            layoutJobStepStatus!!.setBackgroundColor(resources.getColor(R.color.pendingJobStepColor))
            jobStepStatus!!.text = "PENDING"
        } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
            jobStepStatus!!.text = resources.getText(R.string.inProgressJobStepCapital)
            layoutJobStepStatus!!.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColor))
            btnStartOrCompletedJobStep!!.text = "Complete this Job"
            btnStartOrCompletedJobStep!!.visibility = View.VISIBLE
            btnFailedJobStep!!.visibility = View.VISIBLE
        } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
            jobStepStatus!!.text = resources.getText(R.string.completedJobStepCapital)
            layoutJobStepStatus!!.setBackgroundColor(resources.getColor(R.color.completedJobStepColor))
            btnStartOrCompletedJobStep!!.visibility = View.GONE
            btnFailedJobStep!!.visibility = View.GONE
        } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_CANCELLED) {
            jobStepStatus!!.text = resources.getText(R.string.cancelledJobStepCapital)
            layoutJobStepStatus!!.setBackgroundColor(resources.getColor(R.color.cancelledColor))
            btnStartOrCompletedJobStep!!.visibility = View.GONE
            btnFailedJobStep!!.visibility = View.GONE
        } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
            jobStepStatus!!.text = resources.getText(R.string.failedJobStepCapital)
            layoutJobStepStatus!!.setBackgroundColor(resources.getColor(R.color.failedJobStepColor))
            btnStartOrCompletedJobStep!!.visibility = View.GONE
            btnFailedJobStep!!.visibility = View.GONE
        }
        assignedOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.ASSIGNED_TEXT
        )
        ackowledgedOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.ACKNOWLEDGED_TEXT
        )
        inprogressOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.INPROGRESS_TEXT
        )
        completedOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.COMPLETED_TEXT
        )
        failedOrderStatusIDFromDB =
            dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.FAILED_TEXT)
        cancelledOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.CANCELLED_TEXT
        )
        selfcollectOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.SELFCOLLECT_TEXT
        )
        if (OrderStatusId == assignedOrderStatusIDFromDB || OrderStatusId == completedOrderStatusIDFromDB || OrderStatusId == failedOrderStatusIDFromDB || OrderStatusId == cancelledOrderStatusIDFromDB) {
            btnStartOrCompletedJobStep!!.visibility = View.GONE
            btnFailedJobStep!!.visibility = View.GONE
        }
        if (jobSteps != null) {
            if (jobSteps.latitude != 0.0 || jobSteps.longitude != 0.0) {
                LogCustom.i("Latitude", "" + jobSteps.latitude)
                LogCustom.i("Longitude", "" + jobSteps.longitude)
                val destination = LatLng(jobSteps.latitude, jobSteps.longitude)
                val markerOptionsDestination = MarkerOptions()
                markerOptionsDestination.position(destination)
                if (jobSteps.location.equals("", ignoreCase = true)) {
                    markerOptionsDestination.title("Destination Position")
                } else {
                    markerOptionsDestination.title(jobSteps.location)
                }
                markerOptionsDestination.icon(
                    BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_AZURE
                    )
                )
                mMap!!.addMarker(markerOptionsDestination)
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(destination, 13f))
                mMap!!.animateCamera(CameraUpdateFactory.zoomTo(13f))

            } else {
                mapFragment!!.requireView().visibility = View.GONE
            }
        } else {
            mapFragment!!.requireView().visibility = View.GONE
        }
    }

    fun intialize() {
        totalAttemptJobStep = findViewById(R.id.totalAttemptJobStep)
        jobStepContact = findViewById(R.id.jobStepContact)
        jobStepDescription = findViewById(R.id.jobStepDescription)
        jobStepTrackingNo = findViewById(R.id.jobStepTrackingNo)
        jobStepAddress = findViewById(R.id.jobStepAddress)
        jobStepStatus = findViewById(R.id.jobStepStatus)
        jobStepCompany = findViewById(R.id.jobStepCompany)
        jobStepNavigate = findViewById(R.id.jobStepNavigate)
        jobStepWhatsApp = findViewById(R.id.jobStepWhatsApp)
        jobStepPic = findViewById(R.id.jobStepPic)
        scrollview = findViewById(R.id.mScrollView)
        btnFailedJobStep = findViewById(R.id.btnFailedJobStep)
        btnStartOrCompletedJobStep = findViewById(R.id.btnStartOrCompletedJobStep)
        layoutClickStepAttempt = findViewById(R.id.layoutClickStepAttempt)
        layoutClickCall = findViewById(R.id.layoutClickCall)
        layoutJobStepStatus = findViewById(R.id.layoutJobStepStatus)
        layoutClickNavigate = findViewById(R.id.layoutClickNavigate)
        layoutClickWhatsApp = findViewById(R.id.layoutClickWhatsApp)
    }

    override fun onSliderClick(slider: BaseSliderView) {
        Log.i("click slider", "yes" + slider.scaleType)
        if (slider.url != null || slider.scaleType == BaseSliderView.ScaleType.FitCenterCrop) {
            showEditDeletePhotoGaleryImage()
        }
    }

    fun fileFromBitmap(bitmap: Bitmap) {
        var file = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temporary_file.jpg")
        CoroutineScope(Dispatchers.Main).launch {
            var baos = ByteArrayOutputStream()
            var quality = 100
            do {
                baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos)
                quality -= 5
            } while (baos.toByteArray().size > 4200000 && quality > 0)
            FileOutputStream(file).use {
                it.write(baos.toByteArray())
            }
        }.invokeOnCompletion {
            contextProgressDialog!!.text =
                resources.getString(R.string.loadingUploadImage)
            pdUpdateProcess!!.show()
            requestBody(file, file.path, false)
        }

    }

    companion object {
        const val ACTION_TOGGLE_ONLINE_OFFLINE = "toggleIsOnlineOffline"

        /**
         * A class to parse the Google Directions in JSON format
         */
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        var FOLDER = ""
        fun getFullPathFromContentUri(context: Context, uri: Uri?): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                    // ExternalStorageProvider
                    if ("com.android.externalstorage.documents" == uri!!.authority) {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":".toRegex()).toTypedArray()
                        val type = split[0]
                        if ("primary".equals(type, ignoreCase = true)) {
                            return FOLDER + "/" + split[1]
                        }

                        // TODO handle non-primary volumes
                    } else if ("com.android.providers.downloads.documents" == uri.authority) {
                        val id = DocumentsContract.getDocumentId(uri)
                        val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            java.lang.Long.valueOf(id)
                        )
                        return getDataColumn(context, contentUri, null, null)
                    } else if ("com.android.providers.media.documents" == uri.authority) {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":".toRegex()).toTypedArray()
                        val type = split[0]
                        var contentUri: Uri? = null
                        if ("image" == type) {
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        } else if ("video" == type) {
                            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        } else if ("audio" == type) {
                            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                        }
                        val selection = "_id=?"
                        val selectionArgs = arrayOf(
                            split[1]
                        )
                        var cursor: Cursor? = null
                        val column = "_data"
                        val projection = arrayOf(
                            column
                        )
                        try {
                            cursor = context.contentResolver.query(
                                uri, projection, selection, selectionArgs,
                                null
                            )
                            if (cursor != null && cursor.moveToFirst()) {
                                val column_index = cursor.getColumnIndexOrThrow(column)
                                return cursor.getString(column_index)
                            }
                        } finally {
                            cursor?.close()
                        }
                        return null
                    }
                } else if ("content".equals(uri!!.scheme, ignoreCase = true)) {
                    return getDataColumn(context, uri, null, null)
                } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                    return uri.path
                }
            }
            return null
        }

        private fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(
                column
            )
            try {
                cursor = context.contentResolver.query(
                    uri!!, projection, selection, selectionArgs,
                    null
                )
                if (cursor != null && cursor.moveToFirst()) {
                    val column_index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(column_index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }
    }
}
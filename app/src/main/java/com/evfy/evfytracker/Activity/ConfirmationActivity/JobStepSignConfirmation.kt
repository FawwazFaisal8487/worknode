package com.evfy.evfytracker.Activity.ConfirmationActivity

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.media.ExifInterface
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Pair
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.multidex.MultiDex
import androidx.viewpager.widget.ViewPager
import app.juaagugui.httpService.RestManagerFactory
import app.juaagugui.httpService.listeners.OnHttpEventListener
import app.juaagugui.httpService.model.HttpConnection
import app.juaagugui.httpService.services.RestManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Transformers.BaseTransformer
import com.daimajia.slider.library.Tricks.ViewPagerEx
import com.evfy.evfytracker.*
import com.evfy.evfytracker.Activity.ConfirmationActivity.JobStepSignConfirmation
import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService
import com.evfy.evfytracker.Constants.SAAS_SERVER_AUTH
import com.evfy.evfytracker.Database.DatabaseHandlerJobs
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest.OKHttpNetwork
import com.evfy.evfytracker.R
import com.evfy.evfytracker.Utils.OnImageCompressedCallback
import com.evfy.evfytracker.adapter.EditDeleteOrderAttemptAdapter
import com.evfy.evfytracker.classes.JobSteps
import com.evfy.evfytracker.classes.OrderAttempt
import com.evfy.evfytracker.classes.OrderAttemptImage
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.material.textfield.TextInputEditText
import com.lkh012349s.mobileprinter.Utils.AndroidOp
import com.lkh012349s.mobileprinter.Utils.ImageOp
import com.lkh012349s.mobileprinter.Utils.LogCustom
import com.simplify.ink.InkView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class JobStepSignConfirmation : AppCompatActivity(), OnHttpEventListener, LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    OnSliderClickListener, ViewPagerEx.OnPageChangeListener, OnImageCompressedCallback {
    val urlImageStringArray = ArrayList<String?>()
    val urlDescriptionStringArray = ArrayList<String?>()
    val imageBitmapArray = ArrayList<Bitmap>()

    //declare
    var txtLengthNote: TextView? = null
    var txtAddPhoto: TextView? = null
    var tempFile: File? = null
    var editTextRecipientName: TextInputEditText? = null
    var editTxtNote: TextInputEditText? = null
    var btnSubmit: Button? = null
    var btnClear: Button? = null
    var btnCamera: LinearLayout? = null
    var inkView: InkView? = null
    var mActivity: Activity = this
    var restManager: RestManager? = null
    var pairHeader: Pair<String, String>? = null
    var settings: SharedPreferences? = null
    var isFromCompletePhoto = false
    var uriPhotoTakenByCamera: Uri? = null
    var selectedUri: Uri? = null
    var takeAnotherPhotoBool = false
    var lat = 0.0
    var lon = 0.0
    var bitmapSignature: Bitmap? = null

    //int totalImage = 0;
    var connection: HttpConnection? = null
    var imageDialog: AlertDialog? = null
    var imagesUriList = ArrayList<Bitmap>()
    var dbUpdateJobs: DatabaseHandlerJobs? = null
    var newjobOrderIdArrays: JSONArray? = null
    var isFromOrderDetail = false
    var requestTakeOtherPhoto = false
    var resultFromGallery = false
    var indexImageUpload = 0
    var jobSteps: JobSteps? = null
    var image_normalURL: String? = null
    var orderAttempt: OrderAttempt? = null
    var descriptionImageText: String? = null
    var orderId = 0
    var trackingNumber: String? = null
    var startJobStepsButton = false
    var showCompletedFailedButton = false
    var unauthorizedNeedToLoginPage = false
    var pdUpdateProcess: Dialog? = null
    var contextProgressDialog: TextView? = null
    var isLastJobStep = false
    var myCustomPagerAdapter: EditDeleteOrderAttemptAdapter? = null
    var lastLocation: Location? = null
    var locationRequest: LocationRequest? = null
    var mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            for (location in locationResult.locations) {
                lastLocation = location
            }
        }
    }
    var progressDialog: SweetAlertDialog? = null
    private var slider: SliderLayout? = null
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var googleApiClient: GoogleApiClient? = null
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
        LogCustom.i("InstallMultiDex")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        MultiDex.install(this);
//        LogCustom.i("InstallMultiDex");
        dbUpdateJobs = DatabaseHandlerJobs(this)
        val isTableExist = dbUpdateJobs!!.isTableExists("UpdatedJobs", true)
        if (isTableExist == false) {
            dbUpdateJobs!!.onUpgrade(
                dbUpdateJobs!!.writableDatabase,
                dbUpdateJobs!!.writableDatabase.version,
                dbUpdateJobs!!.writableDatabase.version + 1
            )
        }
        setContentView(R.layout.activity_job_step_sign_confirmation)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        unauthorizedNeedToLoginPage = false
        settings = PreferenceManager.getDefaultSharedPreferences(this@JobStepSignConfirmation)
        pdUpdateProcess = Dialog(this)
        val inflater = layoutInflater
        val content = inflater.inflate(R.layout.custom_progress_dialog_process, null)
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog)
        pdUpdateProcess!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        pdUpdateProcess!!.setContentView(content)
        pdUpdateProcess!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        pdUpdateProcess!!.setCancelable(false)
        orderId = intent.getIntExtra("OrderId", 0)
        trackingNumber = intent.getStringExtra("trackingNumber")
        jobSteps = intent.getSerializableExtra("jobSteps") as JobSteps?
        isFromOrderDetail = intent.getBooleanExtra("IsFromOrderDetails", false)
        startJobStepsButton = intent.extras!!.getBoolean("startJobStepsButton")
        showCompletedFailedButton = intent.extras!!.getBoolean("showCompletedFailedButton")
        isLastJobStep = intent.extras!!.getBoolean("isLastJobStep")
        val jsonArray = intent.getStringExtra("jobOrderIdArrays")
        if (jsonArray != null) {
            try {
                newjobOrderIdArrays = JSONArray(jsonArray)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setBackgroundColor(resources.getColor(R.color.actionbar_color))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        this.supportActionBar!!.title = "Confirmation"

        // action bar for app
        val actionBar = supportActionBar
        if (actionBar != null) {
            val colorDrawable = ColorDrawable(Color.parseColor("#A664CCC9"))
            actionBar.setBackgroundDrawable(colorDrawable)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
        initialize()
        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(
            this@JobStepSignConfirmation,
            this
        )

        //listener for each event
        editTextRecipientName!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkAllInputInserted()
            }
        })

        //listener for each event
        editTextRecipientName!!.setText(jobSteps!!.job_step_pic)
        editTxtNote!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                checkAllInputInserted()
            }
        })
        inkView!!.setOnTouchListener { v, event ->
            btnClear!!.visibility = View.VISIBLE
            btnClear!!.bringToFront()
            checkAllInputInserted()
            false
        }


        //button clear signature
        btnClear!!.setOnClickListener {
            LogCustom.i("Click clear button")
            inkView!!.clear()
            btnClear!!.visibility = View.GONE
            checkAllInputInserted()
        }

        //button camera
        btnCamera!!.setOnClickListener {
            isFromCompletePhoto = true
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {
                Log.i("internetConnection", "no")
                showErrorInternetDialog().show()
            } else {
                addImages()
            }
        }

        //button submit
        btnSubmit!!.setOnClickListener {
            Log.i("", "btn submit click")
            val location = lastLocation
            Log.i("LastLocation22", "" + location)
            if (location == null) {
                buildAlertMessageNoGps()
            } else {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    showErrorInternetDialog().show()
                } else {
                    if (orderId != 0 || newjobOrderIdArrays!!.length() != 0) {
                        SubmitConfirmation()
                    } else {
                        showErrorDialog(
                            resources.getString(R.string.orderDetailErrorTitle),
                            resources.getString(
                                R.string.orderDetailError
                            )
                        ).show()
                    }
                }
            }
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        location
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    //    //inflate custom layout dialog
    fun addImages() {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null)
        val layoutClickCamera = alertLayout.findViewById<LinearLayout>(R.id.layoutClickCamera)
        val layoutClickGallery = alertLayout.findViewById<LinearLayout>(R.id.layoutClickGallery)
        layoutClickCamera.setOnClickListener {
            uriPhotoTakenByCamera =
                AndroidOp.startCameraForResult(
                    this@JobStepSignConfirmation,
                    getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                    null
                )
        }
        layoutClickGallery.setOnClickListener { AndroidOp.startGettingImageFromGalleryForResult(this@JobStepSignConfirmation) }
        val alert = AlertDialog.Builder(this)
        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo")
            alert.setPositiveButton("Done") { dialogInterface, i ->
                //do nothing
            }
        } else {
            alert.setTitle("Complete action using")
            alert.setNegativeButton("Cancel") { dialog, which -> }
        }
        alert.setView(alertLayout)
        alert.setCancelable(false)
        imageDialog = alert.create()
        if (imageDialog?.isShowing == false) {
            imageDialog?.show()
        }
    }

    override fun onRequestInit() {}
    override fun onRequestFinish() {}
    fun initialize() {
        slider = findViewById(R.id.slider)
        inkView = findViewById(R.id.inkView)
        btnCamera = findViewById(R.id.btnCamera)
        btnClear = findViewById(R.id.btnClear)
        btnSubmit = findViewById(R.id.btnSubmit)
        editTextRecipientName = findViewById(R.id.editTextRecipientName)
        editTxtNote = findViewById(R.id.editTxtNote)
        txtAddPhoto = findViewById(R.id.txtAddPhoto)
        orderAttempt = OrderAttempt()
    }

    //ImageUploadNew
    fun checkAllInputInserted() {
        if (inkView!!.isDrawn && slider!!.visibility == View.VISIBLE) {
            btnSubmit!!.isEnabled = true
            btnSubmit!!.setTextColor(Color.parseColor("#FFFFFF"))
            btnSubmit!!.background = resources.getDrawable(R.drawable.ripple_effect_green)
        } else {
            btnSubmit!!.isEnabled = false
            btnSubmit!!.setTextColor(Color.parseColor("#EE666666"))
            btnSubmit!!.setBackgroundColor(Color.parseColor("#D2CFD2"))
        }
    }

    fun addDescription(data: Intent?, resultFromGalleryDescription: Boolean) {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.items_layout_description_image, null)
        val textViewTitle = alertLayout.findViewById<TextView>(R.id.textViewTitle)
        val editTextDescription: TextInputEditText =
            alertLayout.findViewById(R.id.editTextDescription)
        textViewTitle.text = "Add Description for Image"
        val builder = AlertDialog.Builder(this)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        builder.setPositiveButton(
            "OK"
        ) { dialog, which -> }
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            descriptionImageText = editTextDescription.text.toString()
            dialog.dismiss()
            if (!resultFromGalleryDescription) {
                try {
                    resultFromGallery = false
                    showImageCameraGallery(uriPhotoTakenByCamera)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                try {
                    resultFromGallery = true
                    val photoUri = data!!.data
                    showImageCameraGallery(photoUri)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_OK) return
        when (requestCode) {
            AndroidOp.REQUEST_CODE_CAMERA -> {
                Log.i("requset", "request code camera")
                resultFromGallery = false
                if (imageDialog == null) {
                } else {
                    if (imageDialog!!.isShowing) {
                        imageDialog!!.dismiss()
                    }
                }
                addDescription(data, resultFromGallery)
            }
            AndroidOp.REQUEST_CODE_GALLERY -> {
                Log.i("", "request code gallery")
                resultFromGallery = true
                if (imageDialog == null) {
                } else {
                    if (imageDialog!!.isShowing) {
                        imageDialog!!.dismiss()
                    }
                }
                addDescription(data, resultFromGallery)
            }
            80 -> {}
        }
    }

    fun reuploadImageCompress(imageFile: File?, selectedImagePath: String?, isSignature: Boolean) {
        if (imageFile!!.length() > 5000000) {
            reuploadImageCompress(saveBitmapToFile(imageFile), selectedImagePath, isSignature)
        } else {
            contextProgressDialog!!.text =
                resources.getString(R.string.loadingUploadImage)
            pdUpdateProcess!!.show()
            requestBody(imageFile, selectedImagePath, isSignature)
        }
    }

    fun requestBody(imageFile: File?, selectedImagePath: String?, isSignature: Boolean) {
        val image = imageFile!!.asRequestBody("image/jpg".toMediaTypeOrNull())
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("picture", selectedImagePath, image)
            .addFormDataPart("picture_name", imageFile.name)
            .addFormDataPart("folder_name", "" + orderId + " - " + jobSteps!!.orderId)
            .build()
        Log.e("JobStep picture_name", " : " + imageFile.name)
        Log.e("JobStep folder_name", " : " + orderId + " - " + jobSteps!!.id)
        val progressListener = CountingRequestBody.Listener { bytesRead, contentLength ->
            if (bytesRead >= contentLength) {
            } else {
                if (contentLength > 0) {
                    val progress = (bytesRead.toDouble() / contentLength * 100).toInt()
                    Log.e("uploadProgress called", "$progress ")
                }
            }
        }
        val imageUploadClient: OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(Interceptor { chain ->
                val originalRequest = chain.request()
                if (originalRequest.body == null) {
                    return@Interceptor chain.proceed(originalRequest)
                }
                val progressRequest = originalRequest.newBuilder()
                    .method(
                        originalRequest.method,
                        CountingRequestBody(originalRequest.body, progressListener)
                    )
                    .build()
                chain.proceed(progressRequest)
            }).connectTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES)
            .build()
        val settings = PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )
        val accessToken = settings.getString("access_token", "")
        val request: Request = Request.Builder()
            .url(SAAS_SERVER_AUTH + "/api/upload") //                .url("https://devstaging-auth-api.worknode.ai/api/upload")
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer $accessToken")
            .post(requestBody)
            .build()
        imageUploadClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    val mMessage = e.message
                    Toast.makeText(
                        this@JobStepSignConfirmation,
                        "Error uploading file",
                        Toast.LENGTH_LONG
                    ).show()
                    if (pdUpdateProcess != null && pdUpdateProcess!!.isShowing) {
                        pdUpdateProcess!!.dismiss()
                    }
                }
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val body = response.body!!.string()
                runOnUiThread {
                    Log.d("ImageUpload", body)
                    if (response.code == 200) {
                        if (pdUpdateProcess != null) {
                            if (pdUpdateProcess!!.isShowing) {
                                pdUpdateProcess!!.dismiss()
                            }
                        }
                        if (body == null || body.isEmpty()) {
                        } else {
                            try {
                                val data = JSONObject(body)
                                val resultData = data.getJSONObject("result")
                                image_normalURL = resultData.getString("image_medium_url")

                                /*check either it signature or image upload. If image upload, show image slider and add image again.
                                                                            If signature, direct update order.
                                                                            */if (isSignature) {
                                    val signatureOrderAttemptImage = OrderAttemptImage(
                                        image_normalURL,
                                        "Recipient’s Signature",
                                        true
                                    )
                                    orderAttempt!!.getmOrderAttemptImages()
                                        .add(signatureOrderAttemptImage)
                                    var imageOrderAttemptImage: OrderAttemptImage
                                    //                                               orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);
                                    if (urlImageStringArray.size > 0) {
                                        for (i in urlImageStringArray.indices) {
                                            imageOrderAttemptImage = OrderAttemptImage(
                                                urlImageStringArray[i],
                                                urlDescriptionStringArray[i],
                                                false
                                            )
                                            orderAttempt!!.getmOrderAttemptImages()
                                                .add(imageOrderAttemptImage)
                                        }
                                    }
                                    try {
                                        jsonStructureBatchUpdate()
                                    } catch (e: IOException) {
                                        e.printStackTrace()
                                    }
                                } else {
                                    urlImageStringArray.add(image_normalURL)
                                    urlDescriptionStringArray.add(descriptionImageText)
                                    showImageAfterUpload(true)
                                }
                                checkAllInputInserted()
                                LogCustom.i(
                                    "orderAttemptSieze: ",
                                    orderAttempt!!.getmOrderAttemptImages().size
                                )
                                LogCustom.i(
                                    "orderAttempt",
                                    orderAttempt!!.getmOrderAttemptImages().toString()
                                )
                            } catch (e: JSONException) {
                                LogCustom.e(e)
                                try {
                                } catch (t: Throwable) {
                                }
                            }
                        }
                    }
                }


            }
        })
    }

    fun saveBitmapToFile(file: File?): File? {
        return try {

            // BitmapFactory options to downsize the image
            val o = BitmapFactory.Options()
            o.inJustDecodeBounds = true
            o.inSampleSize = 6
            // factor of downsizing the image
            var inputStream = FileInputStream(file)
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o)
            inputStream.close()

            // The new size we want to scale to
            val REQUIRED_SIZE = 75

            // Find the correct scale value. It should be the power of 2.
            var scale = 1
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                o.outHeight / scale / 2 >= REQUIRED_SIZE
            ) {
                scale *= 2
            }
            val o2 = BitmapFactory.Options()
            o2.inSampleSize = scale
            inputStream = FileInputStream(file)
            val selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2)
            inputStream.close()

            // here i override the original image file
            file!!.createNewFile()
            val outputStream = FileOutputStream(file)
            selectedBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            file
        } catch (e: Exception) {
            null
        }
    }

    // FusedLocationProviderApi fusedLocationProviderApi;
    @Throws(IOException::class)
    fun showImageCameraGallery(uri: Uri?) {
        selectedUri = uri
        if (resultFromGallery) {

            //After this, try call get path and do new func and get code uploadImageBase func
            //ImageUploadNew
            val selectedImagePath = getFullPathFromContentUri(
                applicationContext, uri
            )
            decodeFile(selectedImagePath)

//            File imageFile = new File(getFullPathFromContentUri(getApplicationContext(),uri));
//
////            //7306866
////            if(imageFile.length() > 5000000) {
////                reuploadImageCompress(saveBitmapToFile(imageFile),selectedImagePath,false);
////
////            } else {
////                contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
////                pdUpdateProcess.show();
////                requestBody(imageFile,selectedImagePath,false);
////            }
//
//            contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
//            pdUpdateProcess.show();
//            requestBody(imageFile,selectedImagePath,false);
            //ImageUploadNew
        } else {
            decodeFile(uri!!.path)
            //ImageUploadNew
            // File imgFile = new File(uri.getPath());

            //7306866
//            if(imgFile.length() > 5000000) {
//                reuploadImageCompress(saveBitmapToFile(imgFile),uri.getPath(),false);
//
//            } else {
//                contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
//                pdUpdateProcess.show();
//                requestBody(imgFile,uri.getPath(),false);
//            }

//            contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
//            pdUpdateProcess.show();
//            requestBody(imgFile,uri.getPath(),false);
            //ImageUploadNew
        }
    }

    override fun onLocationChanged(location: Location) {
        lat = location.latitude
        lon = location.longitude
        lastLocation = location
        Log.i("", "on location cahnged lat:$lat lon:$lon")
    }

    override fun onStart() {
        super.onStart()
        if (googleApiClient != null) {
            googleApiClient!!.connect()
        }
    }

    override fun onStop() {
        googleApiClient!!.disconnect()
        super.onStop()
    }

    private val location: Unit
        private get() {
            locationRequest = LocationRequest.create()
            locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest?.interval = 10000
            locationRequest?.fastestInterval = 30000
            googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
            if (googleApiClient != null) {
                googleApiClient!!.connect()
            }
        }

    override fun onConnected(bundle: Bundle?) {
        Log.i(JobStepSignConfirmation::class.java.simpleName, "Connected to Google Play Services!")
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED
        ) {
            if (lastLocation == null) {
                mFusedLocationClient!!.requestLocationUpdates(
                    locationRequest,
                    mLocationCallback,
                    Looper.myLooper()
                )
            }

//            lastLocation = LocationServices.FusedLocationApi
//                    .getLastLocation(googleApiClient);
//            Log.i("", "last location in on connected:" + lastLocation);
//            if (lastLocation == null) {
//                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
//            }
        }
    }

    override fun onConnectionSuspended(i: Int) {}
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(
            JobStepSignConfirmation::class.java.simpleName,
            "Can't connect to Google Play Services!"
        )
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, please turn it on to update job status.")
            .setCancelable(false)
            .setPositiveButton("Yes") { dialog, id -> startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) }
            .setNegativeButton("No") { dialog, id -> dialog.cancel() }
        val alert = builder.create()
        alert.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSliderClick(slider: BaseSliderView) {
        Log.i("click slider", "yes" + slider.scaleType)
        if (slider.url != null || slider.scaleType == BaseSliderView.ScaleType.FitCenterCrop) {
            showEditDeletePhotoGaleryImage()
        }
    }

    fun showEditDeletePhotoGaleryImage() {
        val inflater = layoutInflater
        val alertLayout =
            inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null)
        val viewPager: ViewPager = alertLayout.findViewById(R.id.viewPager)
        myCustomPagerAdapter = EditDeleteOrderAttemptAdapter(
            this@JobStepSignConfirmation,
            urlImageStringArray,
            urlDescriptionStringArray
        )
        viewPager.adapter = myCustomPagerAdapter
        val builder = AlertDialog.Builder(this)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        builder.setPositiveButton(
            "DONE"
        ) { dialog, which -> }
        val dialog = builder.create()
        dialog.show()

        //Grab the window of the dialog, and change the width
        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = lp
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            dialog.dismiss()
            showImageAfterUpload(false)
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) {}
    override fun onPageScrollStateChanged(state: Int) {}

    @Throws(IOException::class)
    fun uploadImageBase64ForUrl(isSignature: Boolean, bitmapSignature: Bitmap?) {
        if (!isSignature) {
            // show message upload image in background
        }
        val okHttpRequest = OkHttpRequest()

//        if (isFromOrderDetail == false) {
//            orderAttempt = new OrderAttempt();
//            jobSteps.getmOrderAttemptsList().add(orderAttempt);
//        }
//        orderAttempt = jobSteps.getmOrderAttemptsList().get(jobSteps.getmOrderAttemptsList().size() - 1);
        //  jobSteps.getmOrderAttemptsList().add(orderAttempt);
        val jsonObject = JSONObject()
        val jsonArray = JSONArray()
        val signature = ImageOp.convert(bitmapSignature, Bitmap.CompressFormat.JPEG, 80)
        val body = arrayOfNulls<String>(1)
        body[0] = "data:image/png;base64,$signature"
        try {
            jsonObject.put("folder_name", "TestingUploadBase64")
            jsonArray.put(body[0])
            jsonObject.put("base64_images", jsonArray)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        try {
            OkHttpRequest.uploadImageBase64ForUrl(
                applicationContext, jsonObject.toString(), object : OKHttpNetwork {
                    override fun onSuccess(body: String, responseCode: Int) {
                        runOnUiThread {
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            if (body == null || body.isEmpty()) {
                            } else {
                                try {
                                    val data = JSONObject(body)
                                    val resultData = data.getJSONArray("result")
                                    for (i in 0 until resultData.length()) {
                                        val jsonObject2 = resultData[i] as JSONObject
                                        image_normalURL = jsonObject2.getString("image_normal")
                                    }

                                    /*check either it signature or image upload. If image upload, show image slider and add image again.
                                                            If signature, direct update order.
                                                            */if (isSignature) {
                                        val signatureOrderAttemptImage = OrderAttemptImage(
                                            image_normalURL,
                                            "Recipient’s Signature",
                                            true
                                        )
                                        orderAttempt!!.getmOrderAttemptImages()
                                            .add(signatureOrderAttemptImage)
                                        var imageOrderAttemptImage: OrderAttemptImage
                                        //                                               orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);
                                        if (urlImageStringArray.size > 0) {
                                            for (i in urlImageStringArray.indices) {
                                                imageOrderAttemptImage = OrderAttemptImage(
                                                    urlImageStringArray[i],
                                                    urlDescriptionStringArray[i],
                                                    false
                                                )
                                                orderAttempt!!.getmOrderAttemptImages()
                                                    .add(imageOrderAttemptImage)
                                            }
                                        }
                                        try {
                                            jsonStructureBatchUpdate()
                                        } catch (e: IOException) {
                                            e.printStackTrace()
                                        }
                                    } else {
                                        urlImageStringArray.add(image_normalURL)
                                        urlDescriptionStringArray.add(descriptionImageText)
                                        showImageAfterUpload(true)
                                    }
                                    LogCustom.i(
                                        "orderAttemptSieze: ",
                                        orderAttempt!!.getmOrderAttemptImages().size
                                    )
                                    LogCustom.i(
                                        "orderAttempt",
                                        orderAttempt!!.getmOrderAttemptImages().toString()
                                    )
                                } catch (e: JSONException) {
                                    LogCustom.e(e)
                                    try {
                                    } catch (t: Throwable) {
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(
                        body: String,
                        requestCallBackError: Boolean,
                        responseCode: Int
                    ) {
                        val jsonResponse = body
                        runOnUiThread {
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            errorHandlingFromResponseCode(body, requestCallBackError, responseCode)
                        }
                    }
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun showImageAfterUpload(openImageAgain: Boolean) {
        //Update show image first


        //  totalImage = totalImage + 1;
        val url_maps = HashMap<String, String?>()

        // descriptionImageString = descriptionImage.get(totalImage - 1);
        imageBitmapArray.clear()
        slider!!.removeAllSliders()
        if (urlImageStringArray.size > 0) {
            for (i in urlImageStringArray.indices) {
                url_maps["" + urlDescriptionStringArray[i]] = urlImageStringArray[i]
            }
        }
        for (name in url_maps.keys) {
            val textSliderView = TextSliderView(this@JobStepSignConfirmation)
            // initialize a SliderLayout
            textSliderView
                .description(name)
                .image(url_maps[name])
                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                .setOnSliderClickListener(this@JobStepSignConfirmation)
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                .putString("extra", name)
            slider!!.addSlider(textSliderView)
        }
        slider!!.visibility = View.VISIBLE
        if (urlImageStringArray.size > 0) {
            slider!!.visibility = View.VISIBLE
            if (urlImageStringArray.size < 2) {
                Log.i("totalImage1", "" + urlImageStringArray.size)
                slider!!.stopAutoCycle()
                slider!!.setPagerTransformer(false, object : BaseTransformer() {
                    override fun onTransform(view: View, v: Float) {}
                })
                //
            } else if (urlImageStringArray.size > 1) {
                Log.i("totalImage2", "" + urlImageStringArray.size)
                slider!!.startAutoCycle()
                slider!!.setPagerTransformer(true, object : BaseTransformer() {
                    override fun onTransform(view: View, v: Float) {}
                })
            }
        } else if (urlImageStringArray.size == 0) {
            slider!!.removeAllSliders()
            slider!!.visibility = View.GONE
        }
        //Update show image first
        if (openImageAgain) {
            takeAnotherPhotoBool = false
            requestTakeOtherPhoto = true
            addImages()
        }
    }

    @Throws(IOException::class)
    fun jsonStructureBatchUpdate() {
        try {
            Log.i("", "btn submit click 44")
            val jobOrderObject = JSONObject()
            val jobOrderIdArray = JSONArray()
            jobOrderIdArray.put(orderId.toString())
            Log.i("jobOrderIdArray", "" + jobOrderIdArray)


            //Update Passing from JobFragement
            // jobOrderObject.put("id", jobOrderIdArray);
            if (newjobOrderIdArrays != null) {
                jobOrderObject.put("id", newjobOrderIdArrays)
            } else {
                jobOrderObject.put("id", jobOrderIdArray)
            }
            if (isLastJobStep) {
                jobOrderObject.put(
                    "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                        Constants.COMPLETED_TEXT
                    )
                )
            } else {
                jobOrderObject.put(
                    "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                        Constants.INPROGRESS_TEXT
                    )
                )
            }
            val locale = Locale("en", "US")
            val printFormat = SimpleDateFormat("hh:mm a", locale)
            val dropOffTime = printFormat.format(Date())
            LogCustom.i("time", "currentTime$dropOffTime")
            jobOrderObject.put("drop_off_time_end", dropOffTime)
            val location = lastLocation
            Log.i("LastLocation11", "" + location)
            if (location == null) {
                buildAlertMessageNoGps()
            } else {
                orderAttempt!!.setLatitude(location.latitude)
                orderAttempt!!.setLongitude(location.longitude)
                orderAttempt!!.received_by = editTextRecipientName!!.text.toString()
                orderAttempt!!.note = editTxtNote!!.text.toString()
                orderAttempt!!.reason = ""
                val orderAttemptArray = JSONArray()
                val jobStepArray = JSONArray()
                val jobStepObject = JSONObject()
                val DATEFORMAT = "yyyy-MM-dd HH:mm:ss"
                val sdf = SimpleDateFormat(DATEFORMAT)
                sdf.timeZone = TimeZone.getTimeZone("UTC")
                val utcTime = sdf.format(Date())
                orderAttempt!!.submitted_time = utcTime
                orderAttemptArray.put(orderAttempt!!.jsonObjectJobSteps)
                jobStepObject.put("step_attempts", orderAttemptArray)
                jobStepObject.put("job_step_id", jobSteps!!.id)
                jobStepObject.put("job_step_status_id", Constants.JOB_STEP_COMPLETED)
                jobStepArray.put(jobStepObject)
                jobOrderObject.put("job_steps", jobStepArray)
                val dataObject = JSONObject()
                val jobDataArray = JSONArray()
                jobDataArray.put(jobOrderObject)
                dataObject.put("data", jobDataArray)
                Log.i("data object:", ":" + dataObject.toString(4))

                //   updateOrder(dataObject, jobOrderIdArray);
                updateBatchOrder(dataObject)
            }
        } catch (e: Exception) {
            Log.i("", "exception :$e")
        }
    }

    fun SubmitConfirmation() {
        Log.i("", "btn submit click 22")
        if (!inkView!!.isDrawn) {
            val sweetAlertDialog = SweetAlertDialog(mActivity, SweetAlertDialog.ERROR_TYPE)
            sweetAlertDialog.titleText = getString(R.string.titleError)
            sweetAlertDialog.contentText = getString(R.string.msgErrorSignFirst)
            sweetAlertDialog.confirmText = getString(R.string.titleOk)
            sweetAlertDialog.show()
            return
        }

        bitmapSignature = inkView!!.getBitmap(resources.getColor(R.color.white))
        contextProgressDialog!!.text =
            resources.getString(R.string.loadingUploadImage)
        pdUpdateProcess!!.show()
        convertSignatureBitmapToFile(
            bitmapSignature,
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)?.absolutePath + File.separator + "temporary_file.jpg"
        )

    }

    @Throws(IOException::class)
    fun updateBatchOrder(jsonObject: JSONObject) {
        contextProgressDialog!!.text = resources.getString(R.string.confirmOrderLoading)
        pdUpdateProcess!!.show()
        val okHttpRequest = OkHttpRequest()
        try {
            OkHttpRequest.batchUpdate(
                applicationContext, jsonObject.toString(), object : OKHttpNetwork {
                    override fun onSuccess(body: String, responseCode: Int) {
                        runOnUiThread {
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            if (body == null || body.isEmpty()) {
                            } else {
                                try {
                                    val data = JSONObject(body)
                                    LogCustom.i("dataObject", data.toString(4))
                                    if (isLastJobStep) {
                                        showSuccessDialog(
                                            resources.getString(R.string.jobLastStepCompletedTitle),
                                            resources.getString(
                                                R.string.jobLastStepCompletedMessage
                                            )
                                        ).show()
                                    } else {
                                        showSuccessDialog(
                                            resources.getString(R.string.jobStepCompletedTitle),
                                            resources.getString(
                                                R.string.jobStepCompletedMessage
                                            )
                                        ).show()
                                    }
                                } catch (e: JSONException) {
                                    LogCustom.e(e)
                                    try {
                                    } catch (t: Throwable) {
                                    }
                                }
                            }
                        }
                    }

                    override fun onFailure(
                        body: String,
                        requestCallBackError: Boolean,
                        responseCode: Int
                    ) {
                        val jsonResponse = body
                        runOnUiThread {
                            if (pdUpdateProcess != null) {
                                if (pdUpdateProcess!!.isShowing) {
                                    pdUpdateProcess!!.dismiss()
                                }
                            }
                            errorHandlingFromResponseCode(body, requestCallBackError, responseCode)
                        }
                    }
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        val ii = Intent()
        ii.putExtra("OrderId", orderId)
        ii.putExtra("jobStepId", jobSteps!!.id)
        ii.putExtra("jobStepStatusId", jobSteps!!.job_step_status_id)
        ii.putExtra("trackingNumber", trackingNumber)
        ii.putExtra("showCompletedFailedButton", showCompletedFailedButton)
        ii.putExtra("startJobStepsButton", startJobStepsButton)
        setResult(Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION, ii)
        finish()
        super.onBackPressed()
    }

    fun compressImage(imageUri: String): String {
        val filePath = getRealPathFromURI(imageUri)
        var scaledBitmap: Bitmap? = null
        val options = BitmapFactory.Options()

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true
        var bmp = BitmapFactory.decodeFile(filePath, options)
        var actualHeight = options.outHeight
        var actualWidth = options.outWidth

//      max Height and width values of the compressed image is taken as 816x612
        val maxHeight = 816.0f
        val maxWidth = 612.0f
        var imgRatio = (actualWidth / actualHeight).toFloat()
        val maxRatio = maxWidth / maxHeight

//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight
                actualWidth = (imgRatio * actualWidth).toInt()
                actualHeight = maxHeight.toInt()
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth
                actualHeight = (imgRatio * actualHeight).toInt()
                actualWidth = maxWidth.toInt()
            } else {
                actualHeight = maxHeight.toInt()
                actualWidth = maxWidth.toInt()
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)
        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }
        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f
        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
        val canvas = Canvas(scaledBitmap!!)
        canvas.setMatrix(scaleMatrix)
        canvas.drawBitmap(
            bmp,
            middleX - bmp.width / 2,
            middleY - bmp.height / 2,
            Paint(Paint.FILTER_BITMAP_FLAG)
        )

//      check the rotation of the image and display it properly
        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath!!)
            val orientation = exif.getAttributeInt(
                ExifInterface.TAG_ORIENTATION, 0
            )
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            if (orientation == 6) {
                matrix.postRotate(90f)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 3) {
                matrix.postRotate(180f)
                Log.d("EXIF", "Exif: $orientation")
            } else if (orientation == 8) {
                matrix.postRotate(270f)
                Log.d("EXIF", "Exif: $orientation")
            }
            scaledBitmap = Bitmap.createBitmap(
                scaledBitmap, 0, 0,
                scaledBitmap.width, scaledBitmap.height, matrix,
                true
            )
        } catch (e: IOException) {
            e.printStackTrace()
        }
        var out: FileOutputStream? = null
        val filename = filename
        try {
            out = FileOutputStream(filename)

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return filename
    }

    val filename: String
        get() {
            val file = File(
                Environment.getExternalStorageDirectory().path,
                "MyFolder/Images"
            )
            if (!file.exists()) {
                file.mkdirs()
            }
            return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"
        }

    private fun getRealPathFromURI(contentURI: String): String? {
        val contentUri = Uri.parse(contentURI)
        val cursor = contentResolver.query(contentUri, null, null, null, null)
        return if (cursor == null) {
            contentUri.path
        } else {
            cursor.moveToFirst()
            val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(index)
        }
    }

    fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1
        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }
        return inSampleSize
    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    fun errorHandlingFromResponseCode(
        body: String?,
        requestCallBackError: Boolean,
        responseCode: Int
    ) {
        runOnUiThread {
            if (!requestCallBackError) {
                unauthorizedNeedToLoginPage = false
                try {
                    var errorMessage = ""
                    var data: JSONObject? = null
                    if (body != null || !body.equals("", ignoreCase = true)) {
                        try {
                            data = JSONObject(body)
                        } catch (e: Exception) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitle),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        }
                        if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                            if (data!!.has("error") && !data.isNull("error")) {
                                errorMessage = try {
                                    data.getString("error")
                                } catch (e: Exception) {
                                    val errorMessageArray = data.getJSONObject("error")
                                    errorMessageArray.getString("error")
                                }
                            }
                            if (errorMessage.equals("", ignoreCase = true)) {
                                errorMessage = resources.getString(R.string.pleaseContactAdmin)
                            }
                            showErrorDialog(
                                resources.getString(R.string.badRequestErrorTitle),
                                errorMessage
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {
                            unauthorizedNeedToLoginPage = true
                            if (data!!.has("device_not_found") && !data.isNull("device_not_found")) {
                                if (data.getBoolean("device_not_found")) {
                                    showErrorDialog(
                                        resources.getString(R.string.deviceNotFoundTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else if (data.has("device_is_banned") && !data.isNull("device_is_banned")) {
                                if (data.getBoolean("device_is_banned")) {
                                    showErrorDialog(
                                        resources.getString(R.string.deviceBannedTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else {
                                showErrorDialog(
                                    resources.getString(R.string.errorLogin), resources.getString(
                                        R.string.pleaseCheckPassword
                                    )
                                ).show()
                            }
                        } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {
                            unauthorizedNeedToLoginPage = true
                            if (data!!.has("unpaid_subscription") && !data.isNull("unpaid_subscription")) {
                                if (data.getBoolean("unpaid_subscription")) {
                                    showErrorDialog(
                                        resources.getString(R.string.unpaidSubscriptionTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else if (data.has("quota_reached") && !data.isNull("quota_reached")) {
                                if (data.getBoolean("quota_reached")) {
                                    showErrorDialog(
                                        resources.getString(R.string.quotaReachedTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else if (data.has("blacklist") && !data.isNull("blacklist")) {
                                if (data.getBoolean("blacklist")) {
                                    showErrorDialog(
                                        resources.getString(R.string.blacklistTitle),
                                        resources.getString(
                                            R.string.pleaseContactAdmin
                                        )
                                    ).show()
                                }
                            } else {
                                showErrorDialog(
                                    resources.getString(R.string.errorLogin), resources.getString(
                                        R.string.pleaseCheckPassword
                                    )
                                ).show()
                            }
                        } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitleSorry),
                                resources.getString(
                                    R.string.globalErrorTimeoutMessageContent
                                )
                            ).show()
                        }
                    } else {
                        showErrorDialog(
                            resources.getString(R.string.globalErrorMessageTitle),
                            resources.getString(
                                R.string.globalErrorMessageContent
                            )
                        ).show()
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                    showErrorDialog(
                        resources.getString(R.string.globalErrorMessageTitleSorry),
                        resources.getString(
                            R.string.globalErrorMessageContent
                        )
                    ).show()
                } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                    showErrorInternetDialog().show()
                } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                    showErrorDialog(
                        resources.getString(R.string.globalErrorMessageTitleSorry),
                        resources.getString(
                            R.string.globalErrorTimeoutMessageContent
                        )
                    ).show()
                } else {
                    showErrorDialog(
                        resources.getString(R.string.globalErrorMessageTitleSorry),
                        resources.getString(
                            R.string.globalErrorMessageContent
                        )
                    ).show()
                }
                // this for request call back error. Maybe because cannot connect server.
            }
        }
    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */ /*
     * This is for aleart dialog.
     * */
    /* ================================ SWEET ALERT DIALOG =================================== */
    fun showErrorDialog(titleError: String?, contextError: String?): SweetAlertDialog {
        val sweetAlertDialog = getErrorDialog(titleError, contextError)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    fun getErrorDialog(titleError: String?, contextError: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setContentText(contextError)
            .setTitleText(titleError)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                if (unauthorizedNeedToLoginPage) {
                    val generalActivity = GeneralActivity()
                    if (generalActivity.isMyServiceLocationRunning(applicationContext)) {
                        LogCustom.i("isMyServiceRunning", "true")
                        val intent = Intent(
                            applicationContext, ForegroundLocationService::class.java
                        )
                        stopService(intent)
                    }
                    settings!!.edit().remove("isOnline").apply()
                    settings!!.edit().remove("googleToken").apply()
                    settings!!.edit().putString("user_name", "").commit()
                    unauthorizedNeedToLoginPage = false
                    val i = Intent(applicationContext, LoginActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    i.putExtra("EXIT", true)
                    startActivity(i)
                }
            }
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    ///success dialog
    fun showSuccessDialog(title: String?, context: String?): SweetAlertDialog {
        val sweetAlertDialog = getSuccessDialog(title, context)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    fun getSuccessDialog(title: String?, context: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setContentText(context)
            .setTitleText(title)
            .setConfirmText("OK")
            .setConfirmClickListener { sDialog ->
                sDialog.dismissWithAnimation()
                onBackPressed()
            }
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    fun showErrorInternetDialog(): SweetAlertDialog {
        val sweetAlertDialog = errorInternetDialog
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    val errorInternetDialog: SweetAlertDialog
        get() {
            val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener {
                    val intent = Intent(Intent.ACTION_MAIN, null)
                    intent.addCategory(Intent.CATEGORY_LAUNCHER)
                    val cn = ComponentName(
                        "com.android.settings",
                        "com.android.settings.wifi.WifiSettings"
                    )
                    intent.component = cn
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)
                }
                .setCancelText("Cancel")
                .setCancelClickListener { sDialog -> sDialog.dismissWithAnimation() }
            sweetAlertDialog.setCancelable(false)
            return sweetAlertDialog
        }

    /* ================================ SWEET ALERT DIALOG =================================== */
    fun decodeFile(filePath: String?) {

        // Decode image size
        val o = BitmapFactory.Options()
        o.inJustDecodeBounds = true
        BitmapFactory.decodeFile(filePath, o)

        // The new size we want to scale to
        val REQUIRED_SIZE = 1024

        // Find the correct scale value. It should be the power of 2.
        val width_tmp = o.outWidth
        val height_tmp = o.outHeight
        val scale = 1
        //        while (true) {
//            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
//                break;
//            width_tmp /= 2;
//            height_tmp /= 2;
//            scale *= 2;
//        }

        // Decode with inSampleSize
        val o2 = BitmapFactory.Options()
        o2.inSampleSize = scale
        val bitmap = BitmapFactory.decodeFile(filePath, o2)
        CompressImage(this, bitmap.width, bitmap.height, filePath!!).execute()

        //new fileFromBitmap(b,this);


        contextProgressDialog!!.text =
            resources.getString(R.string.loadingUploadImage)
        pdUpdateProcess?.show()
    }

    inner class CompressImage(
        val context: Context,
        val width: Int,
        val height: Int,
        val uriPath: String
    ) : AsyncTask<Void, Int, File>() {
        override fun doInBackground(vararg p0: Void?): File {
            var saveImgFile = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                    .toString() + File.separator + "temporary_file.jpg"
            )
            FileOutputStream(saveImgFile).use {
                it.write(compressBitmap(uriPath, width, height, 4250000))
                return saveImgFile
            }
        }

        fun compressBitmap(
            file: String?,
            width: Int,
            height: Int,
            maxSizeBytes: Int
        ): ByteArray {
            val bmpFactoryOptions: BitmapFactory.Options = BitmapFactory.Options()
            bmpFactoryOptions.inJustDecodeBounds = true
            val bitmap: Bitmap
            val heightRatio =
                Math.ceil((bmpFactoryOptions.outHeight / height.toFloat()).toDouble()).toInt()
            val widthRatio =
                Math.ceil((bmpFactoryOptions.outWidth / width.toFloat()).toDouble()).toInt()
            if (heightRatio > 1 || widthRatio > 1) {
                if (heightRatio > widthRatio) {
                    bmpFactoryOptions.inSampleSize = heightRatio
                } else {
                    bmpFactoryOptions.inSampleSize = widthRatio
                }
            }
            bmpFactoryOptions.inJustDecodeBounds = false
            bitmap = BitmapFactory.decodeFile(file, bmpFactoryOptions)
            var currSize: Int
            var currQuality = 100
            var baos = ByteArrayOutputStream()
            do {
                bitmap.compress(Bitmap.CompressFormat.JPEG, currQuality, baos)
                currSize = baos.toByteArray().size
                currQuality -= 5
                if (currSize >= maxSizeBytes) {
                    baos = ByteArrayOutputStream()
                }
                // limit quality by 5 percent every time
            } while (currSize >= maxSizeBytes && currQuality >= 5)
            return baos.toByteArray()
        }

        override fun onPostExecute(result: File?) {
            super.onPostExecute(result)
            requestBody(result, result?.path, false)
        }

    }

    override fun onCompressionComplete(tempFile: File) {

    }

    fun convertSignatureBitmapToFile(bitmap: Bitmap?, fileNameToSave: String) {
        var file = File(
            fileNameToSave
        )
        var bos: ByteArrayOutputStream
        var quality = 100
        CoroutineScope(Dispatchers.Main).launch {
            do {
                bos = ByteArrayOutputStream()
                bitmap!!.compress(Bitmap.CompressFormat.JPEG, quality, bos)
                quality -= 5
            } while (bos.toByteArray().size > 4200000 && quality > 0)
            FileOutputStream(file).use { it.write(bos.toByteArray()) }
        }.invokeOnCompletion {
            contextProgressDialog!!.text =
                resources.getString(R.string.loadingUploadImage)
            pdUpdateProcess!!.show()
            requestBody(file, file.path, true)
        }
    }


    companion object {
        val MEDIA_TYPE: MediaType = "application/json".toMediaTypeOrNull()!!
        const val PREFS_NAME = "MyPrefsFile"
        val FOLDER = AndroidOp.getPathExternalStorage("KnocKnocKapp")
        const val FAIL_COMPLETE_CODE = 100

        //ImageUploadNew
        fun getFullPathFromContentUri(context: Context, uri: Uri?): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                    // ExternalStorageProvider
                    if ("com.android.externalstorage.documents" == uri!!.authority) {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":").toTypedArray()
                        val type = split[0]
                        if ("primary".equals(type, ignoreCase = true)) {
                            return Environment.getExternalStorageDirectory()
                                .toString() + "/" + split[1]
                        }

                        // TODO handle non-primary volumes
                    } else if ("com.android.providers.downloads.documents" == uri.authority) {
                        val id = DocumentsContract.getDocumentId(uri)
                        val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            java.lang.Long.valueOf(id)
                        )
                        return getDataColumn(context, contentUri, null, null)
                    } else if ("com.android.providers.media.documents" == uri.authority) {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":").toTypedArray()
                        val type = split[0]
                        var contentUri: Uri? = null
                        if ("image" == type) {
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        } else if ("video" == type) {
                            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        } else if ("audio" == type) {
                            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                        }
                        val selection = "_id=?"
                        val selectionArgs = arrayOf(
                            split[1]
                        )
                        var cursor: Cursor? = null
                        val column = "_data"
                        val projection = arrayOf(
                            column
                        )
                        try {
                            cursor = context.contentResolver.query(
                                uri, projection, selection, selectionArgs,
                                null
                            )
                            if (cursor != null && cursor.moveToFirst()) {
                                val column_index = cursor.getColumnIndexOrThrow(column)
                                return cursor.getString(column_index)
                            }
                        } finally {
                            cursor?.close()
                        }
                        return null
                    }
                } else if ("content".equals(uri!!.scheme, ignoreCase = true)) {
                    return getDataColumn(context, uri, null, null)
                } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                    return uri.path
                }
            }
            return null
        }

        private fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(
                column
            )
            try {
                cursor = context.contentResolver.query(
                    uri!!, projection, selection, selectionArgs,
                    null
                )
                if (cursor != null && cursor.moveToFirst()) {
                    val column_index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(column_index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }



        fun humanReadableByteCount(bytes: Long, si: Boolean): Float {
            val unit = if (si) 1000 else 1024
            if (bytes < unit) return bytes.toFloat()
            val exp = (Math.log(bytes.toDouble()) / Math.log(unit.toDouble())).toInt()
            val pre = (if (si) "kMGTPE" else "KMGTPE")[exp - 1].toString() + if (si) "" else "i"
            return (bytes / Math.pow(unit.toDouble(), exp.toDouble())).toFloat()
        }
    }
}
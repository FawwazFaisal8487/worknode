package com.evfy.evfytracker.Activity.ScanBarcode;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.evfy.evfytracker.MessageDialogFragment;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.TrackingOrderAdapter;
import com.evfy.evfytracker.classes.TrackingOrderCheckBox;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import app.juaagugui.httpService.listeners.OnHttpEventListener;
import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.zbar.BarcodeFormat;
import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class ScanBarcodeActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler, MessageDialogFragment.MessageDialogListener, OnHttpEventListener {

    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";
    private boolean mFlash;
    private boolean mAutoFocus;
    private int mCameraId = -1;
    static final String PREFS_NAME = "MyPrefsFile";
    private static final int ZBAR_CAMERA_PERMISSION = 1;
    List<TrackingOrderCheckBox> trackingOrderCheckBoxesList = new ArrayList<TrackingOrderCheckBox>();
    TrackingOrderAdapter adapter;
    CheckBox fromXMl;
    SwitchCompat drawRect, autoFocus, supportMultiple, touchBack, drawText;
    android.widget.Button completeScanButton;
    ListView trackingOrderListView;
    ArrayList<String> trackingOrderList = new ArrayList<String>();
    TextView remainingText;
    ImageButton btnManualInput;
    List<TrackingOrderCheckBox> tempTrackingOrderCheckBoxesList = new ArrayList<TrackingOrderCheckBox>();
    private ArrayList<Integer> mSelectedIndices;
    Boolean inProgressBool, isTransferBool = false;
    boolean correctScan = false;
    LinearLayout scanRemainingLayout;
    final MediaPlayer mp = new MediaPlayer();


    ZBarScannerView mZBarScannerView;

    public void showMessageDialog(String message, String title) {
        DialogFragment fragment = MessageDialogFragment.newInstance(title, message, ScanBarcodeActivity.this);
        fragment.show(getSupportFragmentManager(), "scan_results");
    }

    public void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if (mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<Integer>();
            for (int i = 0; i < BarcodeFormat.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for (int index : mSelectedIndices) {
            formats.add(BarcodeFormat.ALL_FORMATS.get(index));
        }
        if (mZBarScannerView != null) {
            mZBarScannerView.setFormats(formats);
        }
    }


    @Override
    public void handleResult(final Result rawResult) {


        //showMessageDialog("Tracking Number:  " + rawResult.getContents() );

        final String barcode = rawResult.getContents();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                if (mp.isPlaying()) {
                    mp.stop();
                }

                try {
                    mp.reset();
                    AssetFileDescriptor afd;
                    afd = getAssets().openFd("beep.mp3");
                    mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                    mp.prepare();
                    mp.start();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.i("", "tracking order checkbox list:" + trackingOrderCheckBoxesList.size());

                if (isTransferBool) {

                    boolean correctScan = false;

                    for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {

                        Log.i("", "tracking order number 00:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim() + " barcode display value:" + barcode.trim());

                        if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(barcode.trim())) {

                            if (trackingOrderCheckBoxesList.get(i).isChecked()) {
                                showMessageDialog("Repeated scan", "");

                            } else {
                                correctScan = true;
                                trackingOrderCheckBoxesList.get(i).setChecked(true);
                                TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
                                tempTrackingOrderCheckBoxesList.add(trackingOrderCheckBox);

                            }


                            //setItems(trackingOrderCheckBoxesList);
                            Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


                        }
                    }


                    adapter = new TrackingOrderAdapter(ScanBarcodeActivity.this,
                            R.layout.row_tracking_order_row, tempTrackingOrderCheckBoxesList, isTransferBool);
                    trackingOrderListView.setAdapter(adapter);


                    if (correctScan == false) {
                        if (inProgressBool) {
                            showMessageDialog("Wrong Barcode Scan", "");

                        } else {
                            showMessageDialog("No Job Assigned", "");

                        }
                        new CountDownTimer(3000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //here you can have your logic to set text to edittext
                            }

                            public void onFinish() {

                                Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                                if (prev != null) {
                                    DialogFragment df = (DialogFragment) prev;
                                    if (df.isResumed()) {
                                        df.dismiss();
                                    }
                                    mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                }
                            }

                        }.start();
                    } else {

                        if (tempTrackingOrderCheckBoxesList.size() == trackingOrderList.size()) {
                            Intent intent = new Intent();
                            intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                            intent.putExtra("transfer_bool", true);

                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            showMessageDialog("Tracking Number:  " + rawResult.getContents(), "Succesfully Scan");

                            new CountDownTimer(3000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    //here you can have your logic to set text to edittext
                                    Log.i("", "on tick:" + millisUntilFinished);

                                }

                                public void onFinish() {
                                    Log.i("", "");

                                    Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                    Log.i("", "prev fragment:" + prev);
                                    if (prev != null) {
                                        DialogFragment df = (DialogFragment) prev;
                                        if (df.isResumed()) {
                                            df.dismiss();
                                        }
                                        mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                    }
                                }

                            }.start();

                        }


                    }


                } else {

                    for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {

                        //           Log.i("", "tracking order number 00:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim() + " barcode display value:" + barcode.trim());

                        if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(barcode.trim())) {

                            if (trackingOrderCheckBoxesList.get(i).isChecked()) {
                                showMessageDialog("Repeated scan", "");
                                repeatedScan = true;

                            } else {
                                trackingOrderCheckBoxesList.get(i).setChecked(true);
                                repeatedScan = false;

                            }


                            //setItems(trackingOrderCheckBoxesList);
                            Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


                        }
                    }

                    boolean correctScan = false;
                    for (int j = 0; j < tempTrackingOrderCheckBoxesList.size(); j++) {


                        if (tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().trim().equalsIgnoreCase(barcode)) {
                            correctScan = true;
                            tempTrackingOrderCheckBoxesList.remove(j);
                            adapter.notifyDataSetChanged();
                            remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));


                        }
                    }


                    if (repeatedScan == false) {
                        if (correctScan == false) {
//					AlertDialog.Builder builder = new AlertDialog.Builder(ScanBarcodeActivity.this)
//							.setTitle("Error")
//							.setMessage("No Job Assigned");
//					builder.show();


                            if (inProgressBool) {
                                showMessageDialog("Wrong Barcode Scan", "");

                            } else {
                                showMessageDialog("No Job Assigned", "");

                            }
                            new CountDownTimer(3000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    //here you can have your logic to set text to edittext
                                }

                                public void onFinish() {

                                    Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                                    if (prev != null) {
                                        DialogFragment df = (DialogFragment) prev;
                                        if (df.isResumed()) {
                                            df.dismiss();
                                        }
                                        mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                    }
                                }

                            }.start();

                        } else {
                            if (tempTrackingOrderCheckBoxesList.size() == 0) {
                                Intent intent = new Intent();
                                intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                                setResult(RESULT_OK, intent);
                                finish();
                            } else {
                                showMessageDialog("Tracking Number:  " + rawResult.getContents(), "Succesfully Scan");

                                new CountDownTimer(3000, 1000) {

                                    public void onTick(long millisUntilFinished) {
                                        //here you can have your logic to set text to edittext
                                        Log.i("", "on tick:" + millisUntilFinished);

                                    }

                                    public void onFinish() {
                                        Log.i("", "");

                                        Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                        Log.i("", "prev fragment:" + prev);
                                        if (prev != null) {
                                            DialogFragment df = (DialogFragment) prev;
                                            if (df.isResumed()) {
                                                df.dismiss();
                                            }
                                            mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                        }
                                    }

                                }.start();

                            }
                        }
                    } else {
                        new CountDownTimer(3000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //here you can have your logic to set text to edittext
                                Log.i("", "on tick:" + millisUntilFinished);

                            }

                            public void onFinish() {
                                Log.i("", "");

                                Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                Log.i("", "prev fragment:" + prev);
                                if (prev != null) {
                                    DialogFragment df = (DialogFragment) prev;
                                    if (df.isResumed()) {
                                        df.dismiss();
                                    }
                                    mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                }
                            }

                        }.start();


                    }

                }


            }
        });


    }

    @Override
    public void onRequestInit() {
    }

    @Override
    public void onRequestFinish() {
    }

    @Override
    public void onPause() {
        super.onPause();
        mZBarScannerView.stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        } else {
            mZBarScannerView.setResultHandler(this);
            mZBarScannerView.startCamera(mCameraId);
            mZBarScannerView.setFlash(mFlash);
            mZBarScannerView.setAutoFocus(mAutoFocus);
        }


    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.activity_scan_barcode, menu);

        final MenuItem menuKeyboard = menu.findItem(R.id.mMenuItemKeyboard);

        // Show the search menu item in menu.xml
        menuKeyboard.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ScanBarcodeActivity.this);
                final EditText edittext = new EditText(ScanBarcodeActivity.this);
                builder.setTitle("Enter barcode");
                builder.setView(edittext);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        verifyBarCode(edittext.getText().toString());


                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.create().show();
                return false;
            }
        });

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mZBarScannerView.startCamera();
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                onBackPressed();
                return true;
//			case R.id.mMenuItemSwitchCamera:
//				mZBarScannerView.startCamera();
//				//mZBarScannerView.switchCamera();
//				return true;
        }

        return super.onOptionsItemSelected(item);

    }

    SweetAlertDialog showProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        sweetAlertDialog.setTitleText(getString(R.string.msgProcessingProcessing));
        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    void showError(final SweetAlertDialog sweetAlertDialog, final Integer msgResId) {
        Toast.makeText(ScanBarcodeActivity.this, msgResId == null ? R.string.msgErrorOccurredOnScan : msgResId, Toast.LENGTH_SHORT).show();
        if (sweetAlertDialog != null && sweetAlertDialog.isShowing())
            sweetAlertDialog.dismissWithAnimation();
        //mZBarScannerView.setIsScanning( true );
    }

    void showErrorMsg(final SweetAlertDialog sweetAlertDialog, final String msgResId) {
        Toast.makeText(ScanBarcodeActivity.this, msgResId, Toast.LENGTH_LONG).show();
        if (sweetAlertDialog != null && sweetAlertDialog.isShowing())
            sweetAlertDialog.dismissWithAnimation();
        //mZBarScannerView.setIsScanning( true );
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mFlash = savedInstanceState.getBoolean(FLASH_STATE, false);
            mAutoFocus = savedInstanceState.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = savedInstanceState.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = savedInstanceState.getInt(CAMERA_ID, -1);
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }


        setContentView(R.layout.activity_scan_barcode);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        scanRemainingLayout = findViewById(R.id.scanRemainingLayout);
        ViewGroup contentFrame = findViewById(R.id.content_frame);
        mZBarScannerView = new ZBarScannerView(this);
        setupFormats();
        contentFrame.addView(mZBarScannerView);


        trackingOrderList = getIntent().getStringArrayListExtra("trakingNumList");
        inProgressBool = getIntent().getExtras().getBoolean("in_progress_bool");
        isTransferBool = getIntent().getExtras().getBoolean("transfer_bool");

        //mZBarScannerView = ( ZBarScannerView ) findViewById( R.id.mZBarScannerView );


        //setupFormats();


        //mZBarScannerView.setResultHandler( this );


        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#194e90"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        remainingText = findViewById(R.id.remainingScanItems);

        trackingOrderListView = findViewById(R.id.trackingOrderListView);
        completeScanButton = findViewById(R.id.scanCompleteButton);
        completeScanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                intent.putExtra("transfer_bool", isTransferBool);
                setResult(RESULT_OK, intent);
                finish();


            }
        });


        Log.i("", "tracking order list view:" + trackingOrderList.size());
        Log.i("", "tracking order checkbox list view:" + trackingOrderCheckBoxesList.size());
        Log.i("", "tracking order temp cehckbox list view:" + tempTrackingOrderCheckBoxesList.size());

        if (isTransferBool == false) {

            for (int i = 0; i < trackingOrderList.size(); i++) {
                TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
                trackingOrderCheckBoxesList.add(trackingOrderCheckBox);
                tempTrackingOrderCheckBoxesList.add(trackingOrderCheckBox);
            }

            adapter = new TrackingOrderAdapter(this,
                    R.layout.row_tracking_order_row, tempTrackingOrderCheckBoxesList, isTransferBool);
            trackingOrderListView.setAdapter(adapter);

            remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));
        } else {
            for (int i = 0; i < trackingOrderList.size(); i++) {
                TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
                trackingOrderCheckBoxesList.add(trackingOrderCheckBox);
            }
            scanRemainingLayout.setVisibility(View.GONE);
        }


    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        mZBarScannerView.resumeCameraPreview(this);

    }

    boolean repeatedScan = false;

    public void verifyBarCode(final String barcode) {


        Log.i("", "tracking order checkbox list:" + trackingOrderCheckBoxesList.size());

        String originalLast4TrackingNum = "";
        String scanLast4TrackingNum = "";
        int matchCount = 0;
        final List<Integer> trackingOrderIndexArray = new ArrayList<Integer>();

        if (isTransferBool) {
            for (int j = 0; j < trackingOrderCheckBoxesList.size(); j++) {

                if (trackingOrderCheckBoxesList.get(j).getTrackingNumber().length() == 4) {
                    originalLast4TrackingNum = trackingOrderCheckBoxesList.get(j).getTrackingNumber();
                } else if (trackingOrderCheckBoxesList.get(j).getTrackingNumber().length() > 4) {
                    originalLast4TrackingNum = trackingOrderCheckBoxesList.get(j).getTrackingNumber().substring(trackingOrderCheckBoxesList.get(j).getTrackingNumber().length() - 4);
                } else {
                    // whatever is appropriate in this case
                    //throw new IllegalArgumentException("word has less than 3 characters!");
                }

                if (barcode.trim().length() == 4) {
                    scanLast4TrackingNum = barcode.trim();
                } else if (barcode.trim().length() > 4) {
                    scanLast4TrackingNum = barcode.trim().substring(barcode.trim().length() - 4);
                } else {
                    // whatever is appropriate in this case
                    //throw new IllegalArgumentException("word has less than 3 characters!");
                }


                LogCustom.i(scanLast4TrackingNum, "scanLast4TrackingNum");
                LogCustom.i(originalLast4TrackingNum, "originalLast4TrackingNum");

                if (originalLast4TrackingNum.equalsIgnoreCase(scanLast4TrackingNum)) {

                    matchCount++;
                    trackingOrderIndexArray.add(j);

                }
            }
        } else {
            for (int j = 0; j < tempTrackingOrderCheckBoxesList.size(); j++) {

                if (tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().length() == 4) {
                    originalLast4TrackingNum = tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber();
                } else if (tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().length() > 4) {
                    originalLast4TrackingNum = tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().substring(tempTrackingOrderCheckBoxesList.get(j).getTrackingNumber().length() - 4);
                } else {
                    // whatever is appropriate in this case
                    //throw new IllegalArgumentException("word has less than 3 characters!");
                }

                if (barcode.trim().length() == 4) {
                    scanLast4TrackingNum = barcode.trim();
                } else if (barcode.trim().length() > 4) {
                    scanLast4TrackingNum = barcode.trim().substring(barcode.trim().length() - 4);
                } else {
                    // whatever is appropriate in this case
                    //throw new IllegalArgumentException("word has less than 3 characters!");
                }


                LogCustom.i(scanLast4TrackingNum, "scanLast4TrackingNum");
                LogCustom.i(originalLast4TrackingNum, "originalLast4TrackingNum");

                if (originalLast4TrackingNum.equalsIgnoreCase(scanLast4TrackingNum)) {

                    matchCount++;
                    trackingOrderIndexArray.add(j);

                }
            }
        }


        if (matchCount == 1) {
            correctScan = true;
            repeatedScan = false;
            int index = trackingOrderIndexArray.get(0);


            if (isTransferBool) {

                boolean correctScan = false;

                for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {


                    if (tempTrackingOrderCheckBoxesList.size() != 0) {
                        if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(tempTrackingOrderCheckBoxesList.get(index).getTrackingNumber())) {

                            if (trackingOrderCheckBoxesList.get(i).isChecked()) {
                                repeatedScan = true;

                                showMessageDialog("Repeated scan", "");

                            } else {
                                correctScan = true;
                                repeatedScan = false;

                                trackingOrderCheckBoxesList.get(i).setChecked(true);
                                TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
                                tempTrackingOrderCheckBoxesList.add(trackingOrderCheckBox);

                            }


                            //setItems(trackingOrderCheckBoxesList);
                            Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


                        }
                    } else {
                        correctScan = true;
                        repeatedScan = false;

                        trackingOrderCheckBoxesList.get(i).setChecked(true);
                        TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
                        tempTrackingOrderCheckBoxesList.add(trackingOrderCheckBox);

                    }


                }


                adapter = new TrackingOrderAdapter(ScanBarcodeActivity.this,
                        R.layout.row_tracking_order_row, tempTrackingOrderCheckBoxesList, isTransferBool);
                trackingOrderListView.setAdapter(adapter);


                if (repeatedScan == false) {
                    if (correctScan == false) {
                        if (inProgressBool) {
                            showMessageDialog("Wrong Barcode Scan", "");

                        } else {
                            showMessageDialog("No Job Assigned", "");

                        }
                        new CountDownTimer(3000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //here you can have your logic to set text to edittext
                            }

                            public void onFinish() {

                                Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                                if (prev != null) {
                                    DialogFragment df = (DialogFragment) prev;
                                    if (df.isResumed()) {
                                        df.dismiss();
                                    }
                                    mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                }
                            }

                        }.start();
                    } else {

                        if (tempTrackingOrderCheckBoxesList.size() == trackingOrderList.size()) {
                            Intent intent = new Intent();
                            intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                            intent.putExtra("transfer_bool", true);

                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            showMessageDialog("Tracking Number:  " + barcode, "Matched");

                            new CountDownTimer(3000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    //here you can have your logic to set text to edittext
                                    Log.i("", "on tick:" + millisUntilFinished);

                                }

                                public void onFinish() {
                                    Log.i("", "");

                                    Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                    Log.i("", "prev fragment:" + prev);
                                    if (prev != null) {
                                        DialogFragment df = (DialogFragment) prev;
                                        if (df.isResumed()) {
                                            df.dismiss();
                                        }
                                        mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                    }
                                }

                            }.start();

                        }


                    }

                }


            } else {

                for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {

                    Log.i("", "tracking order number 00:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim() + " barcode display value:" + barcode.trim());

                    if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(tempTrackingOrderCheckBoxesList.get(index).getTrackingNumber())) {

                        if (trackingOrderCheckBoxesList.get(i).isChecked()) {
                            showMessageDialog("Repeated scan", "");
                            repeatedScan = true;


                        } else {
                            trackingOrderCheckBoxesList.get(i).setChecked(true);
                            repeatedScan = false;

                        }


                        //setItems(trackingOrderCheckBoxesList);
                        Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


                    }
                }

                tempTrackingOrderCheckBoxesList.remove(index);

                adapter.notifyDataSetChanged();
                remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));


                if (repeatedScan == false) {
                    if (correctScan == false) {
//					AlertDialog.Builder builder = new AlertDialog.Builder(ScanBarcodeActivity.this)
//							.setTitle("Error")
//							.setMessage("No Job Assigned");
//					builder.show();


                        if (inProgressBool) {
                            showMessageDialog("Wrong Barcode Scan", "");

                        } else {
                            showMessageDialog("No Job Assigned", "");

                        }

                        new CountDownTimer(3000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                //here you can have your logic to set text to edittext
                            }

                            public void onFinish() {

                                Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                                if (prev != null) {
                                    DialogFragment df = (DialogFragment) prev;
                                    if (df.isResumed()) {
                                        df.dismiss();
                                    }
                                    mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                }
                            }

                        }.start();

                    } else {
                        if (tempTrackingOrderCheckBoxesList.size() == 0) {
                            Intent intent = new Intent();
                            intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            showMessageDialog("Tracking Number:  " + barcode, "Matched");

                            new CountDownTimer(3000, 1000) {

                                public void onTick(long millisUntilFinished) {
                                    //here you can have your logic to set text to edittext
                                    Log.i("", "on tick:" + millisUntilFinished);

                                }

                                public void onFinish() {
                                    Log.i("", "");

                                    Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                    Log.i("", "prev fragment:" + prev);
                                    if (prev != null) {
                                        DialogFragment df = (DialogFragment) prev;
                                        if (df.isResumed()) {
                                            df.dismiss();
                                        }
                                        mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                    }
                                }

                            }.start();

                        }
                    }
                }

            }


        } else {

            List<String> listItems = new ArrayList<String>();


            for (int i = 0; i < trackingOrderIndexArray.size(); i++) {
                int index = trackingOrderIndexArray.get(i);
                if (isTransferBool) {
                    listItems.add(trackingOrderCheckBoxesList.get(index).getTrackingNumber());

                } else {
                    listItems.add(tempTrackingOrderCheckBoxesList.get(index).getTrackingNumber());

                }
            }

            Log.i("", "list item count:" + listItems.size());


            if (listItems.size() > 0) {
                final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);


                AlertDialog.Builder builder = new AlertDialog.Builder(ScanBarcodeActivity.this);
                final AlertDialog alert = builder.create();
                builder.setTitle("Multiple Tracking Number:");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        alert.dismiss();
                        int index = trackingOrderIndexArray.get(item);


                        if (isTransferBool) {
                            boolean correctScan = false;

                            for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {


                                if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(trackingOrderCheckBoxesList.get(index).getTrackingNumber())) {

                                    if (trackingOrderCheckBoxesList.get(i).isChecked()) {
                                        repeatedScan = true;

                                        showMessageDialog("Repeated scan", "");

                                    } else {
                                        correctScan = true;
                                        repeatedScan = false;

                                        trackingOrderCheckBoxesList.get(i).setChecked(true);
                                        TrackingOrderCheckBox trackingOrderCheckBox = new TrackingOrderCheckBox(trackingOrderList.get(i), false);
                                        tempTrackingOrderCheckBoxesList.add(trackingOrderCheckBox);

                                    }


                                    //setItems(trackingOrderCheckBoxesList);
                                    Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


                                }
                            }


                            adapter = new TrackingOrderAdapter(ScanBarcodeActivity.this,
                                    R.layout.row_tracking_order_row, tempTrackingOrderCheckBoxesList, isTransferBool);
                            trackingOrderListView.setAdapter(adapter);


                            if (repeatedScan == false) {
                                if (correctScan == false) {
                                    if (inProgressBool) {
                                        showMessageDialog("Wrong Barcode Scan", "");

                                    } else {
                                        showMessageDialog("No Job Assigned", "");

                                    }
                                    new CountDownTimer(3000, 1000) {

                                        public void onTick(long millisUntilFinished) {
                                            //here you can have your logic to set text to edittext
                                        }

                                        public void onFinish() {

                                            Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                                            if (prev != null) {
                                                DialogFragment df = (DialogFragment) prev;
                                                if (df.isResumed()) {
                                                    df.dismiss();
                                                }
                                                mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                            }
                                        }

                                    }.start();
                                } else {

                                    if (tempTrackingOrderCheckBoxesList.size() == trackingOrderList.size()) {
                                        Intent intent = new Intent();
                                        intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                                        intent.putExtra("transfer_bool", true);

                                        setResult(RESULT_OK, intent);
                                        finish();
                                    } else {
                                        showMessageDialog("Tracking Number:  " + barcode, "Matched");

                                        new CountDownTimer(3000, 1000) {

                                            public void onTick(long millisUntilFinished) {
                                                //here you can have your logic to set text to edittext
                                                Log.i("", "on tick:" + millisUntilFinished);

                                            }

                                            public void onFinish() {
                                                Log.i("", "");

                                                Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                                Log.i("", "prev fragment:" + prev);
                                                if (prev != null) {
                                                    DialogFragment df = (DialogFragment) prev;
                                                    if (df.isResumed()) {
                                                        df.dismiss();
                                                    }
                                                    mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                                }
                                            }

                                        }.start();

                                    }


                                }

                            }

                        } else {
                            correctScan = false;

                            for (int i = 0; i < trackingOrderCheckBoxesList.size(); i++) {

                                Log.i("", "tracking order number 00:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim() + " barcode display value:" + barcode.trim());

                                if (trackingOrderCheckBoxesList.get(i).getTrackingNumber().trim().equalsIgnoreCase(tempTrackingOrderCheckBoxesList.get(index).getTrackingNumber())) {

                                    correctScan = true;
                                    if (trackingOrderCheckBoxesList.get(i).isChecked()) {
                                        showMessageDialog("Repeated scan", "");
                                        repeatedScan = true;

                                    } else {
                                        trackingOrderCheckBoxesList.get(i).setChecked(true);
                                        repeatedScan = false;

                                    }


                                    //setItems(trackingOrderCheckBoxesList);
                                    Log.i("", "tracking order number 11:" + trackingOrderCheckBoxesList.get(i).getTrackingNumber() + " barcode display value:" + barcode);


                                }
                            }


                            Log.i("", "scanning index:" + index);
                            tempTrackingOrderCheckBoxesList.remove(index);
                            adapter.notifyDataSetChanged();
                            remainingText.setText(String.valueOf(tempTrackingOrderCheckBoxesList.size()));

                            Log.i("", "repeated scan:" + repeatedScan + "correct scan:" + correctScan);

                            if (repeatedScan == false) {
                                if (correctScan == false) {
//					AlertDialog.Builder builder = new AlertDialog.Builder(ScanBarcodeActivity.this)
//							.setTitle("Error")
//							.setMessage("No Job Assigned");
//					builder.show();


                                    if (inProgressBool) {
                                        showMessageDialog("Wrong Barcode Scan", "");

                                    } else {
                                        showMessageDialog("No Job Assigned", "");

                                    }
                                    new CountDownTimer(3000, 1000) {

                                        public void onTick(long millisUntilFinished) {
                                            //here you can have your logic to set text to edittext
                                        }

                                        public void onFinish() {

                                            Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                                            if (prev != null) {
                                                DialogFragment df = (DialogFragment) prev;
                                                if (df.isResumed()) {
                                                    df.dismiss();
                                                }
                                                mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                            }
                                        }

                                    }.start();

                                } else {
                                    if (tempTrackingOrderCheckBoxesList.size() == 0) {
                                        Intent intent = new Intent();
                                        intent.putExtra("tracking_order_list", (Serializable) trackingOrderCheckBoxesList);
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    } else {
                                        showMessageDialog("Tracking Number:  " + barcode, "Matched");

                                        new CountDownTimer(3000, 1000) {

                                            public void onTick(long millisUntilFinished) {
                                                //here you can have your logic to set text to edittext
                                                Log.i("", "on tick:" + millisUntilFinished);

                                            }

                                            public void onFinish() {
                                                Log.i("", "");

                                                Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");

                                                Log.i("", "prev fragment:" + prev);
                                                if (prev != null) {
                                                    DialogFragment df = (DialogFragment) prev;
                                                    if (df.isResumed()) {
                                                        df.dismiss();
                                                    }
                                                    mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                                                }
                                            }

                                        }.start();

                                    }
                                }
                            }

                        }


                    }
                });
                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                    }
                });
                alert.show();
            } else {
                if (inProgressBool) {
                    showMessageDialog("Wrong Barcode Scan", "");

                } else {
                    showMessageDialog("No Job Assigned", "");

                }
                new CountDownTimer(3000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {

                        Fragment prev = getSupportFragmentManager().findFragmentByTag("scan_results");
                        if (prev != null) {
                            DialogFragment df = (DialogFragment) prev;
                            if (df.isResumed()) {
                                df.dismiss();
                            }
                            mZBarScannerView.resumeCameraPreview(ScanBarcodeActivity.this);

                        }
                    }

                }.start();
            }

        }


    }

    public void setItems(List<TrackingOrderCheckBox> myList) {
        this.trackingOrderCheckBoxesList.clear();
        this.trackingOrderCheckBoxesList.addAll(myList);
        adapter.notifyDataSetChanged();
    }


}

package com.evfy.evfytracker.Activity.Tracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Broadcast receiver to enable the self hosted GPS tracker at boot.
 * Created by Kees Jongenburger on 08/12/15.
 */
public class SelfHostedGPSTrackerAtBootReceiver extends BroadcastReceiver {

    private static final String TAG = SelfHostedGPSTrackerAtBootReceiver.class.getSimpleName();

    /**
     * Upon receiving the boot complete intent start the Self hosted GPS tracker
     * service if configured do to so
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            /* If the device is configured to start on boot start the application */
//            Intent serviceIntent = new Intent(context, SelfHostedGPSTrackerService.class);
//            context.startService(serviceIntent);
        }
    }
}

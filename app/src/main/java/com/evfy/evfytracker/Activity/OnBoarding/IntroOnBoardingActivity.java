package com.evfy.evfytracker.Activity.OnBoarding;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.FloatRange;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceManager;

import com.lkh012349s.mobileprinter.Utils.LogCustom;

import agency.tango.materialintroscreen.MaterialIntroActivity;
import agency.tango.materialintroscreen.animations.IViewTranslation;

/**
 * Created by coolasia on 26/2/18.
 */

public class IntroOnBoardingActivity extends MaterialIntroActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        enableLastSlideAlphaExitTransition(true);

        getBackButtonTranslationWrapper()
                .setEnterTranslation(new IViewTranslation() {
                    @Override
                    public void translate(View view, @FloatRange(from = 0, to = 1.0) float percentage) {
                        view.setAlpha(percentage);
                    }
                });

        addSlide(new CustomSlideIntroOnBoarding());

        addSlide(new SecondPageOnBoarding());

        addSlide(new ThreePageOnBoarding());

        addSlide(new FourPageOnBoarding());

        addSlide(new FivePageOnBoarding());

        //   addSlide(new PermissionOnBoarding());

//        addSlide(new SlideFragmentBuilder()
//                        .backgroundColor(R.color.blueFirstPage)
//                        .image(R.drawable.ic_keyboard_w)
//                        .title("Welcome!")
//                        .description("Would you try?")
//                        .build(),
//                new MessageButtonBehaviour(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        showMessage("We provide solutions to make you love your work");
//                    }
//                }, "Work with love"));


//
//        addSlide(new SlideFragmentBuilder()
//                .backgroundColor(R.color.blue_btn_bg_color)
//                .buttonsColor(R.color.blue_btn_bg_pressed_color)
//                .title("Want more?")
//                .description("Go on")
//                .build());


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            LogCustom.i("back press", "yes");
            // finish();
            finishAffinity();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onFinish() {
        super.onFinish();
        //Toast.makeText(this, "Try this library in your project! :)", Toast.LENGTH_SHORT).show();
        // final SharedPreferences settings = getSharedPreferences(Constants.PREFS_NAME, 0 );
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(IntroOnBoardingActivity.this);

        SharedPreferences.Editor editors = settings.edit();
        //  editors.putBoolean("firstTimeUserOnBoarding", false).commit();
        editors.apply();

        Intent i = new Intent(getApplicationContext(), PermissionOnBoardingActivity.class);
        startActivity(i);

    }
}

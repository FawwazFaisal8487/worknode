package com.evfy.evfytracker.Activity.WMSActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.Activity.ScanBarcode.ScanDoActivity;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.exceptions.NoInternetConnectionException;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class DeliveryOrderDetailActivity extends AppCompatActivity implements OnRESTResultCallback, OnHttpEventListener {

    LinearLayout mLayoutList;
    String accessToken;
    HttpConnection connection;
    private RestManager restManager;
    String referenceNumber, name_scan_setting, resultIntent;
    int companyId;
    DeliveryOrderDetails deliveryOrderDetails;
    JSONArray orderDetailsList;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    double totalBalance = 0.0;
    TextView txt_totalBalance, txt_stringLotOrSerial;
    Toolbar toolbar;
    Button btnScan;
    SweetAlertDialog pd;
    int leftBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_order_detail);

        intialize();

        resultIntent = getIntent().getExtras().getString("resultIntent");
        referenceNumber = getIntent().getExtras().getString("referenceNumber");
        companyId = getIntent().getExtras().getInt("companyId");

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle(referenceNumber);
        this.getSupportActionBar().setTitle(referenceNumber);


        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Log.i("referenceNumberDO", "" + referenceNumber);
        Log.i("companyIdDO", "" + companyId);


        showDetailListDo();


        //       getDetailListDO();

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });

//        btnScan.setOnClickListener( new View.OnClickListener() {
//
//            @Override
//            public void onClick ( View view ) {
//                Intent i = new Intent(DeliveryOrderDetailActivity.this, ScanDoActivity.class);
//                i.putExtra("orderDetailsList", orderDetailsList.toString());
//                startActivityForResult(i,1);
//            }
//
//        } );

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void intialize() {
        mLayoutList = findViewById(R.id.mLayoutList);
        txt_totalBalance = findViewById(R.id.txt_totalBalance);
        txt_stringLotOrSerial = findViewById(R.id.txt_stringLotOrSerial);
        toolbar = findViewById(R.id.toolbar);
        btnScan = findViewById(R.id.btnScan);

        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(DeliveryOrderDetailActivity.this, this);
        settings = this.getSharedPreferences(PREFS_NAME, 0);
        accessToken = settings.getString("access_token", "");
        name_scan_setting = settings.getString("name_scan_setting", "");

        if (name_scan_setting.equalsIgnoreCase("lot_no")) {
            txt_stringLotOrSerial.setText("LOT NO");
        } else if (name_scan_setting.equalsIgnoreCase("serial_no")) {
            txt_stringLotOrSerial.setText("SERIAL NO");
        }

        pd = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));

    }


    void displayReferenceList(JSONArray orderDetailsList) {
        mLayoutList.removeAllViews();

        Log.i("orderDetailListLength", "" + orderDetailsList.length());

        if (orderDetailsList.length() > 0) {
            for (int i = 0; i < orderDetailsList.length(); i++) {

                try {
                    JSONObject jsonObject2 = (JSONObject) orderDetailsList.get(i);
                    final DeliveryOrderDetails doDetails = new DeliveryOrderDetails(jsonObject2);
                    final View view = View.inflate(this, R.layout.view_layout_list_delivery_number, null);
                    mLayoutList.addView(view);
                    final TextView txt_partNo = view.findViewById(R.id.txt_partNo);
                    final TextView txt_lotNo = view.findViewById(R.id.txt_lotNo);
                    final TextView txt_balance = view.findViewById(R.id.txt_balance);
                    final LinearLayout layoutList_delivery = view.findViewById(R.id.layoutList_delivery);

                    if ((i % 2) == 0) {
                        layoutList_delivery.setBackgroundColor(getResources().getColor(R.color.bgColorOdd));
                    } else {
                        layoutList_delivery.setBackgroundColor(getResources().getColor(R.color.bgColorEven));
                    }

                    int totalBalance = doDetails.getQuantity() - doDetails.getQuantity_scanned();

                    txt_partNo.setText(doDetails.getPart_no());

                    if (name_scan_setting.equalsIgnoreCase("lot_no")) {
                        txt_lotNo.setText(doDetails.getLot_no());
                    } else if (name_scan_setting.equalsIgnoreCase("serial_no")) {
                        txt_lotNo.setText(doDetails.getSerial_no());
                    }


                    txt_balance.setText("" + totalBalance);

                    layoutList_delivery.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent i = new Intent(getApplicationContext(), ScanDoActivity.class);
                            i.putExtra("deliveryOrderDetails", doDetails);
                            i.putExtra("referenceNumber", referenceNumber);
                            i.putExtra("companyId", companyId);
                            startActivityForResult(i, 1);
                        }
                    });

                } catch (Exception e) {
                    LogCustom.e(e);
//                    Crashlytics.logException(e);
                }

            }
        }
    }

    public void showDetailListDo() {
        try {
            final JSONObject data = new JSONObject(resultIntent);
            orderDetailsList = data.getJSONArray("order_details");
            // ArrayList <String> listOfApp = new ArrayList<String>();
            for (int m = 0; m < orderDetailsList.length(); m++) {

                JSONObject orderDetailsListJson = (JSONObject) orderDetailsList.get(m);
                deliveryOrderDetails = new DeliveryOrderDetails(orderDetailsListJson);
                LogCustom.i(deliveryOrderDetails.getBalance(), "balance");
                leftBalance = deliveryOrderDetails.getQuantity() - deliveryOrderDetails.getQuantity_scanned();
                totalBalance = totalBalance + leftBalance;
            }

            Log.i("totalBalance1", "" + totalBalance);
            txt_totalBalance.setText("" + (int) totalBalance);


            displayReferenceList(orderDetailsList);
        } catch (Exception e) {
            LogCustom.e(e);
//            Crashlytics.logException(e);
        }
    }

    @Override
    public void onRESTResult(int returnCode, int code, String result) {

        LogCustom.i(result, "resultgetReference");
        LogCustom.i(code, "code");

        totalBalance = 0;

        if (returnCode == 2) {
            try {
                final JSONObject data = new JSONObject(result);
                orderDetailsList = data.getJSONArray("order_details");
                // ArrayList <String> listOfApp = new ArrayList<String>();
                for (int m = 0; m < orderDetailsList.length(); m++) {

                    JSONObject orderDetailsListJson = (JSONObject) orderDetailsList.get(m);
                    deliveryOrderDetails = new DeliveryOrderDetails(orderDetailsListJson);
                    LogCustom.i(deliveryOrderDetails.getBalance(), "balance");
                    leftBalance = deliveryOrderDetails.getQuantity() - deliveryOrderDetails.getQuantity_scanned();
                    totalBalance = totalBalance + leftBalance;
                }

                Log.i("totalBalance1", "" + totalBalance);
                txt_totalBalance.setText("" + (int) totalBalance);

                displayReferenceList(orderDetailsList);
            } catch (Exception e) {
                LogCustom.e(e);
                Log.i("crash", "crash");
//                Crashlytics.log(returnCode, "getDetailListDOResult", result);
//                Crashlytics.logException(e);
            }
        }

    }

    public void getDetailListDO() {

        try {

            JSONObject jsonObject = new JSONObject();
            connection = new HttpConnection(Constants.SAAS_SERVER_WMS + "/worker/api/scan/do_out?customer_id=" + companyId + "&reference_no=" + referenceNumber, RESTIntentService.GET, jsonObject, this);

            List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();

            Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
            header.add(pair);
            connection.setHeaders(header);

            Log.i("", "access token:" + accessToken);
            Log.i("connectGetReferenceNo", " " + connection.toString());

            restManager.sendRequestWithReturn(2, connection);
        } catch (Exception e) {

            if (e instanceof NoInternetConnectionException) {

            } else {

                Log.i("crash", "crash");
//                Crashlytics.log("getDetailListDO");
//                Crashlytics.logException(e);

            }

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("requestCode", "Answer: " + requestCode);

        if (requestCode == 1) {
            //Bundle extra=data.getExtras();
            boolean doneUpdateScan = data.getExtras().getBoolean("doneUpdateScan");
            Log.i("doneUpdateScan", "Answer: " + doneUpdateScan);

            referenceNumber = getIntent().getExtras().getString("referenceNumber");
            companyId = getIntent().getExtras().getInt("companyId");

            if (doneUpdateScan) {
//                finish();
//                startActivity(getIntent());
                getDetailListDO();
            }

        }
    }


    @Override
    public void onRequestInit() {

    }

    @Override
    public void onRequestFinish() {

    }

}


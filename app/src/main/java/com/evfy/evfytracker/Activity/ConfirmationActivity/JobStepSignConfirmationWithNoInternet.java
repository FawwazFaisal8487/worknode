package com.evfy.evfytracker.Activity.ConfirmationActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;
import androidx.viewpager.widget.ViewPager;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity;
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.adapter.EditDeleteOrderAttemptAdapterNoInternet;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.JobSteps;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.evfy.evfytracker.classes.OrderAttemptImage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.DateTimeOp;
import com.lkh012349s.mobileprinter.Utils.ImageOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;
import com.simplify.ink.InkView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.zelory.compressor.Compressor;
import okhttp3.MediaType;

public class JobStepSignConfirmationWithNoInternet extends AppCompatActivity implements OnHttpEventListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    //declare
    TextView txtLengthNote, txtAddPhoto;
    EditText editTextRecipientName, editTxtNote;
    Button btnSubmit, btnClear;
    private SliderLayout slider;
    LinearLayout btnCamera;
    InkView inkView;
    Activity mActivity = this;
    RestManager restManager;
    Pair<String, String> pairHeader;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    boolean isFromCompletePhoto = false;
    static String FOLDER = "";
    Uri uriPhotoTakenByCamera;
    boolean takeAnotherPhotoBool = false;
    double lat, lon;
    Bitmap bitmapSignature;
    //int totalImage = 0;
    HttpConnection connection;
    AlertDialog imageDialog;
    ArrayList<Bitmap> imagesUriList = new ArrayList<>();
    DatabaseHandlerJobs dbUpdateJobs;
    JSONArray newjobOrderIdArrays;
    boolean isFromOrderDetail = false;
    boolean requestTakeOtherPhoto = false;
    boolean resultFromGallery;
    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");

    static final int FAIL_COMPLETE_CODE = 100;
    int indexImageUpload;
    JobSteps jobSteps;
    String image_normalURL;
    OrderAttempt orderAttempt;
    int orderId;

    String trackingNumber;
    boolean startJobStepsButton, showCompletedFailedButton, unauthorizedNeedToLoginPage = false;
    Dialog pdUpdateProcess;
    TextView contextProgressDialog;
    boolean isLastJobStep;
    EditDeleteOrderAttemptAdapterNoInternet myCustomPagerAdapter;
    final ArrayList<Bitmap> imageBitmapArray = new ArrayList<>();
    int totalImage = 0;
    String imageBitmap, descriptionImageString;
    ArrayList<String> descriptionImage = new ArrayList<>();

    private FusedLocationProviderClient mFusedLocationClient;

    Bitmap photo;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        LogCustom.i("InstallMultiDex");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        MultiDex.install(this);
//        LogCustom.i("InstallMultiDex");

        dbUpdateJobs = new DatabaseHandlerJobs(this);

        boolean isTableExist = dbUpdateJobs.isTableExists("UpdatedJobs", true);

        if (isTableExist == false) {
            dbUpdateJobs.onUpgrade(dbUpdateJobs.getWritableDatabase(), dbUpdateJobs.getWritableDatabase().getVersion(), dbUpdateJobs.getWritableDatabase().getVersion() + 1);
        }

        setContentView(R.layout.activity_job_step_sign_confirmation);
        FOLDER = getExternalFilesDir(Environment.DIRECTORY_PICTURES + "/" + "KnocKnocKapp").getAbsolutePath();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        unauthorizedNeedToLoginPage = false;
        settings = PreferenceManager.getDefaultSharedPreferences(JobStepSignConfirmationWithNoInternet.this);

        pdUpdateProcess = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        pdUpdateProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdUpdateProcess.setContentView(content);
        pdUpdateProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdUpdateProcess.setCancelable(false);

        orderId = getIntent().getIntExtra("OrderId", 0);
        trackingNumber = getIntent().getStringExtra("trackingNumber");
        jobSteps = (JobSteps) getIntent().getSerializableExtra("jobSteps");
        isFromOrderDetail = getIntent().getBooleanExtra("IsFromOrderDetails", false);
        startJobStepsButton = getIntent().getExtras().getBoolean("startJobStepsButton");
        showCompletedFailedButton = getIntent().getExtras().getBoolean("showCompletedFailedButton");
        isLastJobStep = getIntent().getExtras().getBoolean("isLastJobStep");

        String jsonArray = getIntent().getStringExtra("jobOrderIdArrays");

        if (jsonArray != null) {
            try {
                newjobOrderIdArrays = new JSONArray(jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Confirmation");

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initialize();
        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(JobStepSignConfirmationWithNoInternet.this, this);

        //listener for each event
        editTextRecipientName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });

        //listener for each event

        editTextRecipientName.setText(jobSteps.getJob_step_pic());

        editTxtNote.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });


        inkView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                btnClear.setVisibility(View.VISIBLE);
                btnClear.bringToFront();
                checkAllInputInserted();
                return false;
            }
        });


        //button clear signature
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogCustom.i("Click clear button");
                inkView.clear();
                btnClear.setVisibility(View.GONE);
                checkAllInputInserted();
            }
        });

        //button camera
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromCompletePhoto = true;
                addImages();

            }
        });

        //button submit
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("", "btn submit click");
                Location location = getLastLocation();
                Log.i("LastLocation22", "" + location);

                if (location == null) {
                    buildAlertMessageNoGps();

                } else {
                    if (orderId != 0 || newjobOrderIdArrays.length() != 0) {
                        SubmitConfirmation(location);
                    } else {
                        showErrorDialog(getResources().getString(R.string.orderDetailErrorTitle), getResources().getString(R.string.orderDetailError)).show();
                    }

                }

            }
        });

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        getLocation();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //    //inflate custom layout dialog
    public void addImages() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null);
        final LinearLayout layoutClickCamera = alertLayout.findViewById(R.id.layoutClickCamera);
        final LinearLayout layoutClickGallery = alertLayout.findViewById(R.id.layoutClickGallery);

        layoutClickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                uriPhotoTakenByCamera = AndroidOp.startCameraForResult(JobStepSignConfirmationWithNoInternet.this, FOLDER, null);
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, AndroidOp.REQUEST_CODE_CAMERA);
            }
        });

        layoutClickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidOp.startGettingImageFromGalleryForResult(JobStepSignConfirmationWithNoInternet.this);
            }
        });


        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo");
            alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing

                }
            });
        } else {
            alert.setTitle("Complete action using");
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }


        alert.setView(alertLayout);
        alert.setCancelable(false);


        imageDialog = alert.create();
        if (!imageDialog.isShowing()) {
            imageDialog.show();
        }

    }


    @Override
    public void onRequestInit() {
    }

    @Override
    public void onRequestFinish() {
    }


    public void initialize() {

        slider = findViewById(R.id.slider);
        inkView = findViewById(R.id.inkView);
        btnCamera = findViewById(R.id.btnCamera);
        btnClear = findViewById(R.id.btnClear);
        btnSubmit = findViewById(R.id.btnSubmit);
        editTextRecipientName = findViewById(R.id.editTextRecipientName);
        editTxtNote = findViewById(R.id.editTxtNote);
        txtAddPhoto = findViewById(R.id.txtAddPhoto);

        orderAttempt = new OrderAttempt();

    }

    public void checkAllInputInserted() {
        if (inkView.isDrawn() && editTextRecipientName.getText().toString().trim().length() != 0) {
            btnSubmit.setEnabled(true);
            btnSubmit.setTextColor(Color.parseColor("#FFFFFF"));
            btnSubmit.setBackground(getResources().getDrawable(R.drawable.ripple_effect_green));
        } else {
            btnSubmit.setEnabled(false);
            btnSubmit.setTextColor(Color.parseColor("#EE666666"));
            btnSubmit.setBackgroundColor(Color.parseColor("#D2CFD2"));
        }

    }


    public void addDescription(final Intent data, final boolean resultFromGalleryDescription) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.items_layout_description_image, null);
        final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);


        textViewTitle.setText("Add Description for Image");


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                descriptionImage.add(editTextDescription.getText().toString());

                dialog.dismiss();


                if (!resultFromGalleryDescription) {
                    try {
                        resultFromGallery = false;
                        Bitmap pic = (Bitmap) data.getExtras().get("data");
                        final String curTime = DateTimeOp.getStringNow(DateTimeOp.TimeZone.LOCAL);
                        String filename = curTime.replace(":", ".") + ".jpg";
                        File file = new File(FOLDER + "/" + filename);
                        try {
                            FileOutputStream outputStream = new FileOutputStream(file);
                            pic.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        uriPhotoTakenByCamera = Uri.fromFile(file);
                        showImageCameraGallery(uriPhotoTakenByCamera);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        resultFromGallery = true;
                        Uri photoUri = data.getData();
                        showImageCameraGallery(photoUri);
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }
            }


        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;

        switch (requestCode) {

            case AndroidOp.REQUEST_CODE_CAMERA:
                Log.i("requset", "request code camera");

                resultFromGallery = false;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }

                addDescription(data, resultFromGallery);


                break;

            case AndroidOp.REQUEST_CODE_GALLERY:
                Log.i("", "request code gallery");

                resultFromGallery = true;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }

                addDescription(data, resultFromGallery);

                break;
            case 80:
                break;

        }

    }

//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        if (savedInstanceState.containsKey("cameraImageUri")) {
//            uriPhotoTakenByCamera = Uri.parse(savedInstanceState.getString("cameraImageUri"));
//        }
//        if (savedInstanceState.containsKey("descriptionImage")) {
//            descriptionImage = savedInstanceState.getStringArrayList("descriptionImage");
//        }
//
//        if (savedInstanceState.containsKey("imagesUriList")) {
//            imagesUriList = (ArrayList<Bitmap>) savedInstanceState.getSerializable("imagesUriList");
//        }
//
//        LogCustom.i("descriptionImage restore:","?"+descriptionImage.size());
//        LogCustom.i("imagesUriList restore:","?"+imagesUriList.size());
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        if (uriPhotoTakenByCamera != null) {
//            outState.putString("cameraImageUri", uriPhotoTakenByCamera.toString());
//        }
//
//        if (descriptionImage != null) {
//            outState.putStringArrayList("descriptionImage", descriptionImage);
//        }
//
//        if (imagesUriList != null) {
//            outState.putSerializable("imagesUriList", imagesUriList);
//        }
//    }


    void showImageCameraGallery(final Uri uri) throws IOException {


        totalImage = totalImage + 1;
        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();

        descriptionImageString = descriptionImage.get(totalImage - 1);


        if (resultFromGallery) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            File imgFile = new File(filePath);
            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));

            url_maps.put("" + descriptionImageString, bmp);

            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

//            contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
//            pdUpdateProcess.show();
//            uploadImageBase64ForUrl(false,compressedImageBitmap);

            imagesUriList.add(compressedImageBitmap);

        } else {

            File imgFile = new File(uri.getPath());


            Log.i("", "image file url 00:" + uriPhotoTakenByCamera);
            Log.i("", "image file url 11:" + uri);

            Log.i("", "image file url get path:" + uri.getPath());

            Log.i("", "image file:" + imgFile);
            Log.i("", "image file absolute:" + imgFile.getAbsolutePath());


            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));
            url_maps.put("" + descriptionImageString, bmp);

            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

//            contextProgressDialog.setText(getResources().getString(R.string.loadingUploadImage));
//            pdUpdateProcess.show();
//            uploadImageBase64ForUrl(false,compressedImageBitmap);
            imagesUriList.add(compressedImageBitmap);
        }

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(JobStepSignConfirmationWithNoInternet.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(JobStepSignConfirmationWithNoInternet.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            slider.addSlider(textSliderView);
        }

        slider.setVisibility(View.VISIBLE);


        if (totalImage < 2) {
            Log.i("totalImage1", "" + totalImage);
            slider.stopAutoCycle();
            slider.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
//
        } else if (totalImage > 1) {
            Log.i("totalImage2", "" + totalImage);
            slider.startAutoCycle();
            slider.setPagerTransformer(true, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
        }
        //Update show image first

        requestTakeOtherPhoto = true;
        addImages();


    }


    @Override
    public void onLocationChanged(Location location) {

        lat = location.getLatitude();
        lon = location.getLongitude();
        lastLocation = location;

        Log.i("", "on location cahnged lat:" + lat + " lon:" + lon);


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


    Location lastLocation;
    private GoogleApiClient googleApiClient;
    // FusedLocationProviderApi fusedLocationProviderApi;


    LocationRequest locationRequest;

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                lastLocation = location;

            }
        }

    };

    private void getLocation() {

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(30000);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i(JobStepSignConfirmationWithNoInternet.class.getSimpleName(), "Connected to Google Play Services!");

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            if (lastLocation == null) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
            }

        }
    }


    public Location getLastLocation() {

        return lastLocation;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(JobStepSignConfirmation.class.getSimpleName(), "Can't connect to Google Play Services!");
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, please turn it on to update job status.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Log.i("click slider", "yes" + slider.getScaleType());
        if (slider.getUrl() != null || slider.getScaleType() == BaseSliderView.ScaleType.FitCenterCrop) {

            showEditDeletePhotoGaleryImage();


        }
    }

    public void showEditDeletePhotoGaleryImage() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null);
        final ViewPager viewPager = alertLayout.findViewById(R.id.viewPager);

        myCustomPagerAdapter = new EditDeleteOrderAttemptAdapterNoInternet(JobStepSignConfirmationWithNoInternet.this, imagesUriList, descriptionImage);
        viewPager.setAdapter(myCustomPagerAdapter);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("DONE",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        //Grab the window of the dialog, and change the width
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
//This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showImageAfterUpload(false);

            }


        });
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }


    public static float humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes;
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return (float) (bytes / Math.pow(unit, exp));
    }


    public void showImageAfterUpload(boolean openImageAgain) {
        //Update show image first


        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();

        imageBitmapArray.clear();
        slider.removeAllSliders();

        if (imagesUriList.size() > 0) {
            for (int i = 0; i < imagesUriList.size(); i++) {
                url_maps.put("" + descriptionImage.get(i), imagesUriList.get(i));
            }
        }


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(JobStepSignConfirmationWithNoInternet.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(JobStepSignConfirmationWithNoInternet.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            slider.addSlider(textSliderView);
        }

        slider.setVisibility(View.VISIBLE);


        if (imagesUriList.size() > 0) {
            slider.setVisibility(View.VISIBLE);

            if (imagesUriList.size() < 2) {
                Log.i("totalImage1", "" + imagesUriList.size());
                slider.stopAutoCycle();
                slider.setPagerTransformer(false, new BaseTransformer() {
                    @Override
                    protected void onTransform(View view, float v) {
                    }
                });
//
            } else if (imagesUriList.size() > 1) {
                Log.i("totalImage2", "" + imagesUriList.size());
                slider.startAutoCycle();
                slider.setPagerTransformer(true, new BaseTransformer() {
                    @Override
                    protected void onTransform(View view, float v) {
                    }
                });
            }
        } else if (imagesUriList.size() == 0) {
            slider.removeAllSliders();
            slider.setVisibility(View.GONE);
        }

    }

    public void jsonStructureBatchUpdate() throws IOException {

        try {
            Log.i("", "btn submit click 44");


            JSONObject jobOrderObject = new JSONObject();
            JSONArray jobOrderIdArray = new JSONArray();


            jobOrderIdArray.put(String.valueOf(orderId));
            Log.i("jobOrderIdArray", "" + jobOrderIdArray);


            //Update Passing from JobFragement
            // jobOrderObject.put("id", jobOrderIdArray);
            if (newjobOrderIdArrays != null) {
                jobOrderObject.put("id", newjobOrderIdArrays);
            } else {
                jobOrderObject.put("id", jobOrderIdArray);
            }

            jobOrderObject.put("order_status_id", "");

            Locale locale = new Locale("en", "US");
            SimpleDateFormat printFormat = new SimpleDateFormat("hh:mm a", locale);
            String dropOffTime = printFormat.format(new Date());
            LogCustom.i("time", "currentTime" + dropOffTime);

            jobOrderObject.put("drop_off_time_end", dropOffTime);

            Location location = getLastLocation();

            Log.i("LastLocation11", "" + location);


            if (location == null) {
                buildAlertMessageNoGps();

            } else {
                orderAttempt.setLatitude(location.getLatitude());
                orderAttempt.setLongitude(location.getLongitude());
                orderAttempt.setReceived_by(editTextRecipientName.getText().toString());
                orderAttempt.setNote(editTxtNote.getText().toString());

                orderAttempt.setReason("");

                JSONArray orderAttemptArray = new JSONArray();
                JSONArray jobStepArray = new JSONArray();
                JSONObject jobStepObject = new JSONObject();

                orderAttemptArray.put(orderAttempt.getJSONObjectJobSteps());
                jobStepObject.put("step_attempts", orderAttemptArray);

                jobStepObject.put("job_step_id", jobSteps.getId());
                jobStepObject.put("job_step_status_id", Constants.JOB_STEP_COMPLETED);
                jobStepArray.put(jobStepObject);
                jobOrderObject.put("job_steps", jobStepArray);


                JSONObject dataObject = new JSONObject();
                JSONArray jobDataArray = new JSONArray();
                jobDataArray.put(jobOrderObject);
                dataObject.put("data", jobDataArray);

                Log.i("data object:", ":" + dataObject.toString(4));

                //   updateOrder(dataObject, jobOrderIdArray);

                updateBatchOrder(dataObject);
            }


        } catch (Exception e) {
            Log.i("", "exception :" + e);

        }


    }

    public void SubmitConfirmation(Location location) {
        Log.i("", "btn submit click 22");

        if (!inkView.isDrawn()) {
            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mActivity, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(getString(R.string.titleError));
            sweetAlertDialog.setContentText(getString(R.string.msgErrorSignFirst));
            sweetAlertDialog.setConfirmText(getString(R.string.titleOk));
            sweetAlertDialog.show();
            return;
        }

//        if (editTextRecipientName.getText().toString().trim().length() == 0) {
//            final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mActivity, SweetAlertDialog.ERROR_TYPE);
//            sweetAlertDialog.setTitleText(getString(R.string.titleError));
//            sweetAlertDialog.setContentText(getString(R.string.msgErrorRecipient));
//            sweetAlertDialog.setConfirmText(getString(R.string.titleOk));
//            sweetAlertDialog.show();
//            return;
//        }


        Log.i("", "btn submit click 33");


        bitmapSignature = inkView.getBitmap(getResources().getColor(R.color.white));

        // LogCustom.i("Submitt", ": " + imagesUriList.size());
        // LogCustom.i("Submitt", ": " + imagesUriList.get(0));

        final String signature = ImageOp.convert(bitmapSignature, Bitmap.CompressFormat.JPEG, 80);

        // id
        // check either it last step or not , if last then change it to complete, if not just change status to in progress
        // complete then send drop_off_time_end
        // step id, step status, all step detail
        //image

        Locale locale = new Locale("en", "US");
        SimpleDateFormat printFormat = new SimpleDateFormat("hh:mm a", locale);
        String dropOffTime = printFormat.format(new Date());


        JobOrder jobOrder = new JobOrder();

        jobOrder = dbUpdateJobs.getJobDetailsBasedOnID(orderId);
        if (isLastJobStep) {
            jobOrder.setOrderStatusId(dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.COMPLETED_TEXT));
            jobOrder.setDropOffTimeEnd(dropOffTime);
        } else {
            jobOrder.setOrderStatusId(dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT));
        }

        jobOrder.setUpdateWithNoInternet(true);
        jobOrder.setReattemptJob(false);
        dbUpdateJobs.addJobDetails(jobOrder);


        jobSteps.setJob_step_status_id(Constants.JOB_STEP_COMPLETED);
        jobSteps.setJobStepUpdateWithNoInternet(true);
        dbUpdateJobs.addJobStepDetails(jobSteps);

        int uniqueIDOrderAttempt = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        orderAttempt = new OrderAttempt();

        orderAttempt.setId(uniqueIDOrderAttempt);
        orderAttempt.setLatitude(location.getLatitude());
        orderAttempt.setLongitude(location.getLongitude());
        orderAttempt.setReceived_by(editTextRecipientName.getText().toString());
        orderAttempt.setNote(editTxtNote.getText().toString());
        orderAttempt.setReason("");
        orderAttempt.setAttemptUpdateWithNoInternet(true);
        orderAttempt.setJobStepID(jobSteps.getId());
        orderAttempt.setOrderId(orderId);


        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String currentDate = format2.format(date);


        orderAttempt.setSubmitted_time(currentDate);

        dbUpdateJobs.addOrderAttemptDetails(orderAttempt);


        OrderAttemptImage orderAttemptImageSignature = new OrderAttemptImage();
        int uniqueIDOrderAttemptImageSignature = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);

        LogCustom.i("uniqueIDOrderAttemptImageSignature", "" + uniqueIDOrderAttemptImageSignature);

        orderAttemptImageSignature.setOrderAttemptID(uniqueIDOrderAttempt);
        orderAttemptImageSignature.setId(uniqueIDOrderAttemptImageSignature);
        orderAttemptImageSignature.setBase64(signature);
        orderAttemptImageSignature.setNote("Recipient’s Signature");
        orderAttemptImageSignature.setRemove(false);
        orderAttemptImageSignature.setSignature(true);
        orderAttemptImageSignature.setImageUpdateNoInternet(true);
        orderAttemptImageSignature.setUrl("");
        orderAttemptImageSignature.setOrderId(orderId);
        dbUpdateJobs.addImageDetails(orderAttemptImageSignature);

        if (imagesUriList.size() > 0) {
            for (int i = 0; i < imagesUriList.size(); i++) {
                OrderAttemptImage orderAttemptImage = new OrderAttemptImage();
                int uniqueIDOrderAttemptImage = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE) + i + 1;

                LogCustom.i("uniqueIDOrderAttemptImage", "" + uniqueIDOrderAttemptImage);

                final String photoBase64 = ImageOp.convert(imagesUriList.get(i), Bitmap.CompressFormat.JPEG, 80);

                orderAttemptImage.setOrderAttemptID(uniqueIDOrderAttempt);
                orderAttemptImage.setId(uniqueIDOrderAttemptImage);
                orderAttemptImage.setBase64(photoBase64);
                orderAttemptImage.setNote(descriptionImage.get(i));
                orderAttemptImage.setRemove(false);
                orderAttemptImage.setSignature(false);
                orderAttemptImage.setImageUpdateNoInternet(true);
                orderAttemptImage.setUrl("");
                orderAttemptImage.setOrderId(orderId);
                dbUpdateJobs.addImageDetails(orderAttemptImage);
            }
        }


        if (isLastJobStep) {
            showSuccessDialog(getResources().getString(R.string.jobLastStepCompletedTitle), getResources().getString(R.string.jobLastStepCompletedMessage)).show();
        } else {
            showSuccessDialog(getResources().getString(R.string.jobStepCompletedTitle), getResources().getString(R.string.jobStepCompletedMessage)).show();
        }

    }

    public void updateBatchOrder(final JSONObject jsonObject) throws IOException {

        contextProgressDialog.setText(getResources().getString(R.string.confirmOrderLoading));
        pdUpdateProcess.show();

        OkHttpRequest okHttpRequest = new OkHttpRequest();


        try {
            OkHttpRequest.batchUpdate(getApplicationContext(), jsonObject.toString()
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess.isShowing()) {
                                            pdUpdateProcess.dismiss();
                                        }
                                    }

                                    if (body == null || body.isEmpty()) {

                                    } else {
                                        try {
                                            final JSONObject data = new JSONObject(body);
                                            LogCustom.i("dataObject", data.toString(4));


                                            if (isLastJobStep) {
                                                showSuccessDialog(getResources().getString(R.string.jobLastStepCompletedTitle), getResources().getString(R.string.jobLastStepCompletedMessage)).show();
                                            } else {
                                                showSuccessDialog(getResources().getString(R.string.jobStepCompletedTitle), getResources().getString(R.string.jobStepCompletedMessage)).show();
                                            }


                                        } catch (JSONException e) {

                                            LogCustom.e(e);

                                            try {

                                            } catch (final Throwable t) {
                                            }

                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            final String jsonResponse = body;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess.isShowing()) {
                                            pdUpdateProcess.dismiss();
                                        }
                                    }

                                    errorHandlingFromResponseCode(body, requestCallBackError, responseCode);


                                }
                            });
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    SweetAlertDialog progressDialog;


    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("OrderId", orderId);
        ii.putExtra("jobStepId", jobSteps.getId());
        ii.putExtra("jobStepStatusId", jobSteps.getJob_step_status_id());
        ii.putExtra("trackingNumber", trackingNumber);
        ii.putExtra("showCompletedFailedButton", showCompletedFailedButton);
        ii.putExtra("startJobStepsButton", startJobStepsButton);

        setResult(Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION, ii);
        finish();
        super.onBackPressed();
    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!requestCallBackError) {

                    unauthorizedNeedToLoginPage = false;

                    try {
                        String errorMessage = "";
                        JSONObject data = null;
                        if (body != null || !body.equalsIgnoreCase("")) {

                            try {
                                data = new JSONObject(body);
                            } catch (Exception e) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                            }

                            if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                if (data.has("error") && !(data.isNull("error"))) {
                                    try {
                                        errorMessage = data.getString("error");
                                    } catch (Exception e) {
                                        JSONObject errorMessageArray = data.getJSONObject("error");
                                        errorMessage = errorMessageArray.getString("error");
                                    }
                                }

                                if (errorMessage.equalsIgnoreCase("")) {
                                    errorMessage = getResources().getString(R.string.pleaseContactAdmin);
                                }

                                showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


                            } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                    if (data.getBoolean("device_not_found")) {
                                        showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                    if (data.getBoolean("device_is_banned")) {
                                        showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                    if (data.getBoolean("unpaid_subscription")) {
                                        showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                    if (data.getBoolean("quota_reached")) {
                                        showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                    if (data.getBoolean("blacklist")) {
                                        showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                            }

                        } else {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                        showErrorInternetDialog().show();
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                    } else {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    }
                    // this for request call back error. Maybe because cannot connect server.
                }
            }
        });


    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */


    /*
     * This is for aleart dialog.
     * */
    /* ================================ SWEET ALERT DIALOG =================================== */

    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if (unauthorizedNeedToLoginPage) {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
                                LogCustom.i("isMyServiceRunning", "true");
                                Intent intent = new Intent(getApplicationContext(), ForegroundLocationService.class);
                                stopService(intent);
                            }

                            settings.edit().remove("isOnline").apply();
                            settings.edit().remove("googleToken").apply();
                            settings.edit().putString("user_name", "").commit();
                            unauthorizedNeedToLoginPage = false;

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.putExtra("EXIT", true);
                            startActivity(i);
                        }
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    ///success dialog
    SweetAlertDialog showSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = getSuccessDialog(title, context);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText(context)
                .setTitleText(title)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        onBackPressed();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }


    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    /* ================================ SWEET ALERT DIALOG =================================== */


}

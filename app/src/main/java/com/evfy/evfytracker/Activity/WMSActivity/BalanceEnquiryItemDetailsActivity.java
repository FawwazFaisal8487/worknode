package com.evfy.evfytracker.Activity.WMSActivity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;

public class BalanceEnquiryItemDetailsActivity extends AppCompatActivity {

    DeliveryOrderDetails deliveryOrderDetails;
    TextView txt_partNumber, txt_lotNumber, txt_balance, txt_serialNumber, txt_expiryDate, txt_cr, txt_location, txt_dateIn;
    LinearLayout layout_dateOut;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_enquiry_item_details);

        initialize();

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Item Details");

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        deliveryOrderDetails = (DeliveryOrderDetails) getIntent().getSerializableExtra("deliveryOrderDetails");

        Log.i("sizeOfDateOutDO", "" + deliveryOrderDetails.getDeliveryOrderOutDetails().size());

        setTextForItems();


        layout_dateOut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (deliveryOrderDetails.getDeliveryOrderOutDetails().size() == 0) {
                    Toast.makeText(BalanceEnquiryItemDetailsActivity.this, "This Item not have date out", Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(getApplicationContext(), BalanceEnquiryDateOutActivity.class);
                    i.putExtra("deliveryOrderDetails", deliveryOrderDetails);
                    startActivity(i);
                }


            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void initialize() {
        toolbar = findViewById(R.id.toolbar);
        txt_partNumber = findViewById(R.id.txt_partNumber);
        txt_lotNumber = findViewById(R.id.txt_lotNumber);
        txt_balance = findViewById(R.id.txt_balance);
        txt_serialNumber = findViewById(R.id.txt_serialNumber);
        txt_expiryDate = findViewById(R.id.txt_expiryDate);
        txt_cr = findViewById(R.id.txt_cr);
        txt_location = findViewById(R.id.txt_location);
        txt_dateIn = findViewById(R.id.txt_dateIn);

        layout_dateOut = findViewById(R.id.layout_dateOut);
    }

    void setTextForItems() {
        txt_partNumber.setText(deliveryOrderDetails.getPart_no());
        txt_lotNumber.setText(deliveryOrderDetails.getLot_no());
        txt_balance.setText("" + deliveryOrderDetails.getBalance());
        txt_serialNumber.setText(deliveryOrderDetails.getSerial_no());
        txt_expiryDate.setText(deliveryOrderDetails.getExpiry_date());
        txt_cr.setText(deliveryOrderDetails.getContainer_receipt());
        txt_location.setText(deliveryOrderDetails.getLocation());
        txt_dateIn.setText(deliveryOrderDetails.getHandling_in_date_DOrderDetail());
    }
}

package com.evfy.evfytracker.Activity.Tracking;

import android.Manifest;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


//import com.google.android.gms.common.ConnectionResult;
//import com.google.android.gms.common.api.GoogleApiClient;
//import com.google.android.gms.location.FusedLocationProviderApi;
//import com.google.android.gms.location.LocationRequest;
//import com.google.android.gms.location.LocationServices;

public class SelfHostedGPSTrackerService extends IntentService implements LocationListener {

    public static final String NOTIFICATION = "fr.herverenault.selfhostedgpstracker";

    public static boolean isRunning;
    public static Calendar runningSince;
    public static String lastServerResponse;

    public Calendar stoppedOn;

    private final static String MY_TAG = "SelfHostedGPSTrackerSrv";

    private SharedPreferences preferences;
    private String urlText;
    private LocationManager locationManager;
    private int pref_gps_updates;
    private long latestUpdate;
    private int pref_max_run_time;
    private boolean pref_timestamp;
    private Location lastLocation = null;
    double speed = 0;
    float locationBearing = 0;
    SharedPreferences settings;
    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask doTask;

    public SelfHostedGPSTrackerService() {
        super("SelfHostedGPSTrackerService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(MY_TAG, "in onCreate, init GPS stuff");

        // settings = this.getSharedPreferences("MyPrefsFile",0);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            onProviderEnabled(LocationManager.GPS_PROVIDER);
        } else {
            onProviderDisabled(LocationManager.GPS_PROVIDER);
        }

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("stoppedOn", 0);
        editor.commit();
        pref_gps_updates = Integer.parseInt(preferences.getString("pref_gps_updates", "60")); // seconds
        pref_max_run_time = Integer.parseInt(preferences.getString("pref_max_run_time", "24")); // hours
        pref_timestamp = preferences.getBoolean("pref_timestamp", false);
        urlText = preferences.getString("URL", "");
        if (urlText.contains("?")) {
            urlText = urlText + "&";
        } else {
            urlText = urlText + "?";
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.i("", "self hosted gps tracker 00");

            return;
        } else {
            Log.i("", "self hosted gps tracker 11");
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    preferences.getBoolean("pref_accuracy", false) ? 1 : pref_gps_updates * 1000,
                    preferences.getBoolean("pref_accuracy", false) ? 0 : 1,
                    this);


        }

        handler = new Handler();
        timer = new Timer();


//        if (googleApiClient != null) {
//            googleApiClient.connect();
//        }
//
//        getLocation();


    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(MY_TAG, "in onHandleIntent, run for maximum time set in preferences");

        timerCount();

        isRunning = true;
        runningSince = Calendar.getInstance();
        Intent notifIntent = new Intent(NOTIFICATION);
        sendBroadcast(notifIntent);


        Notification.Builder builder = new Notification.Builder(this);
        Intent notificationIntent = new Intent(this, DrawerActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);
        builder.setSmallIcon(R.drawable.ic_leeway_notification)
                .setContentTitle("Tracking")
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notification = builder.getNotification();
        // notificationManager.notify(R.drawable.notification_template_icon_bg, notification);


//        Notification notification = new Notification(R.drawable.ic_notification, getText(R.string.toast_service_running), System.currentTimeMillis());
//        Intent notificationIntent = new Intent(this, MainDrawerActivity.class);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//
//
//        notification.setLatestEventInfo(this, getText(R.string.app_name), getText(R.string.toast_service_running), pendingIntent);

        // For hide the notification
        //  startForeground(1000, notification);

        long endTime = System.currentTimeMillis() + pref_max_run_time * 60 * 60 * 1000;
        while (System.currentTimeMillis() < endTime) {
            try {
                Thread.sleep(60 * 1000); // note: when device is sleeping, it may last up to 5 minutes or more
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroy() {
        // (user clicked the stop button, or max run time has been reached)
        Log.d(MY_TAG, "in onDestroy, stop listening to the GPS");
        new SelfHostedGPSTrackerRequest().execute(urlText + "tracker=stop");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            locationManager.removeUpdates(this);

            isRunning = false;
            stoppedOn = Calendar.getInstance();

            SharedPreferences.Editor editor = preferences.edit();
            editor.putLong("stoppedOn", stoppedOn.getTimeInMillis());
            editor.commit();

            Intent notifIntent = new Intent(NOTIFICATION);
            sendBroadcast(notifIntent);
            // googleApiClient.disconnect();

        }

    }

//    private GoogleApiClient googleApiClient;
//    FusedLocationProviderApi fusedLocationProviderApi;
//
//
//    LocationRequest locationRequest;

    private void getLocation() {

//        locationRequest = LocationRequest.create();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(10000);
//        locationRequest.setFastestInterval(30000);
//        fusedLocationProviderApi = LocationServices.FusedLocationApi;
//        googleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(LocationServices.API)
//                .addConnectionCallbacks(this)
//                .addOnConnectionFailedListener(this)
//                .build();
//        if (googleApiClient != null) {
//            googleApiClient.connect();
//        }
    }


    /* -------------- GPS stuff -------------- */

    public void timerCount() {
        doTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            // new SelfHostedGPSTrackerRequest().execute(urlText + "lat=" + lastLocation.getLatitude() + "&lon=" + lastLocation.getLongitude());
                            if (lastLocation != null) {
                                LogCustom.i("getLatitude", "" + lastLocation.getLatitude());
                                LogCustom.i("getLongitude", "" + lastLocation.getLongitude());
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doTask, 0, 10000);
    }

    @Override
    public void onLocationChanged(final Location location) {
        if ((System.currentTimeMillis() - latestUpdate) < pref_gps_updates * 1000) {
            return;
        } else {
            latestUpdate = System.currentTimeMillis();
        }

        lastLocation = location;

        Log.i("changeLocation", "in");
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    public class SelfHostedGPSTrackerRequest extends AsyncTask<String, Void, Void> {
        private final static String MY_TAG = "SelfHostedGPSTrackerRequest";

        protected Void doInBackground(String... urlText) {


            settings.edit().putBoolean("isUpdateNeeded", false).apply();


            int resource_owner_id = settings.getInt("resource_owner_id", 0);
            boolean isOnline = settings.getBoolean("isOnline", true);


            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("is_online", isOnline);
                jsonObject.put("latitude", String.valueOf(lastLocation.getLatitude()))
                        //  .put("worker_id",resource_owner_id)
                        .put("longitude", String.valueOf(lastLocation.getLongitude()));

                OkHttpRequest okHttpRequest = new OkHttpRequest();

                try {
                    OkHttpRequest.driverUpdateLocation(getApplicationContext(), jsonObject.toString()
                            , new OkHttpRequest.OKHttpNetwork() {
                                @Override
                                public void onSuccess(final String body, final int responseCode) {
                                    if (body == null || body.isEmpty()) {
                                        LogCustom.i("valueIsNull", "yes");
                                    } else {

                                        try {
                                            Handler handler = new Handler(Looper.getMainLooper());
                                            handler.post(new Runnable() {
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Update location!" + " lat:" + lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),
                                                            Toast.LENGTH_LONG).show();
                                                }
                                            });
                                            final JSONObject data = new JSONObject(body);
                                            Log.d("HTTP request done", " : ");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
                                    if (!requestCallBackError) {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.post(new Runnable() {
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "False Update location!" + " lat:" + lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),
                                                        Toast.LENGTH_LONG).show();
                                            }
                                        });
//                                        if(body == null || body.isEmpty()){
//                                            LogCustom.i("valueIsNull", "yes" );
//                                        }else{
//                                            LogCustom.i("HTTP request done", " : " );
//                                        }
                                    }
                                }
                            });
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Log.i("send location ","bearer:"+accessToken+ " lat:"+String.valueOf(lastLocation.getLatitude())+" lon:"+String.valueOf(lastLocation.getLongitude()));
//                URL url = new URL(Constants.SAAS_SERVER_AUTH + Constants.DRIVER_UPDATE_LOCATION);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setReadTimeout(10000 /* milliseconds */);
//                conn.setConnectTimeout(15000 /* milliseconds */);
//                conn.setRequestMethod("POST");
//                conn.setRequestProperty("Authorization","Bearer "+accessToken);
//                conn.setRequestProperty("latitude",String.valueOf(lastLocation.getLatitude()));
//                conn.setRequestProperty("longitude",String.valueOf(lastLocation.getLongitude()));
//                conn.setDoInput(true);
//                conn.connect();
//                int response = conn.getResponseCode();
//
//                Log.d("HTTP request done", " : " + response);
                // that's ok, nothing more to do here
            } catch (Exception e) {
                // we cannot do anything about that : network may be temporarily down
                //Log.d(MY_TAG, "HTTP request failed");
            }
            return null;
        }
    }

}


package com.evfy.evfytracker.Activity.JobStepActivity

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager.widget.ViewPager
import cn.pedant.SweetAlert.SweetAlertDialog
import cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener
import com.bumptech.glide.Glide
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.daimajia.slider.library.SliderLayout
import com.daimajia.slider.library.SliderTypes.BaseSliderView
import com.daimajia.slider.library.SliderTypes.BaseSliderView.OnSliderClickListener
import com.daimajia.slider.library.SliderTypes.TextSliderView
import com.daimajia.slider.library.Transformers.BaseTransformer
import com.evfy.evfytracker.Activity.ConfirmationActivity.JobStepSignConfirmation
import com.evfy.evfytracker.Activity.ConfirmationActivity.JobStepSignConfirmationWithNoInternet
import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity
import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity
import com.evfy.evfytracker.Activity.OrderItem.ItemsOrderDetails
import com.evfy.evfytracker.Activity.OrderItem.ItemsOrderDetailsNoInternet
import com.evfy.evfytracker.Activity.ScanBarcode.ScanDoActivity
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService
import com.evfy.evfytracker.Constants
import com.evfy.evfytracker.Constants.SAAS_SERVER_AUTH
import com.evfy.evfytracker.CountingRequestBody
import com.evfy.evfytracker.Database.DatabaseHandlerJobs
import com.evfy.evfytracker.GeneralActivity
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest.OKHttpNetwork
import com.evfy.evfytracker.R
import com.evfy.evfytracker.adapter.EditDeleteOrderAttemptAdapter
import com.evfy.evfytracker.classes.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.material.textfield.TextInputEditText
import com.lkh012349s.mobileprinter.Utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import lucifer.org.snackbartest.Icon
import lucifer.org.snackbartest.MySnack.SnackBuilder
import me.leolin.shortcutbadger.ShortcutBadger
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class ManyJobStepsActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener, OnSliderClickListener {
    val urlImageStringArray = ArrayList<String?>()
    val urlDescriptionStringArray = ArrayList<String?>()
    val imageBitmapArray = ArrayList<Bitmap>()
    val orderAttemptImagesForFailed = ArrayList<OrderAttemptImage>()
    var uriPhotoTakenByCamera: Uri? = null
    var selectedUri: Uri? = null
    var requestTakeOtherPhoto = false
    var imageDialog: AlertDialog? = null
    var descriptionImageText: String? = null
    var myCustomPagerAdapter: EditDeleteOrderAttemptAdapter? = null
    var image_normalURL: String? = null
    var takeAnotherPhotoBool = false
    var resultFromGallery = false
    var tempFile: File? = null
    var orderAttempt: OrderAttempt? = null
    var orderstatusdetail: TextView? = null
    var trackingNumberOrderDetails: TextView? = null
    var descriptionOrderDetails: TextView? = null
    var totalItemOrderDetails: TextView? = null
    var phNo_sim: TextView? = null
    var phNo_whatsapp: TextView? = null
    var trackingNumberLabel: TextView? = null
    var companyNameLabel: TextView? = null
    var driverNotesLabel: TextView? = null
    var layoutItemOrderDetails: LinearLayout? = null
    var mLayoutJobSteps: LinearLayout? = null
    var layoutJobStatus: LinearLayout? = null
    var mClickCallLayout_whatsapp: LinearLayout? = null
    var mClickCallLayout_sim: LinearLayout? = null
    var btnAcceptJob: Button? = null
    var btnReattemptJob: Button? = null
    var order_number: String? = null
    var jobOrder: JobOrder? = null
    var selectedDate: Date? = null
    var indexTotal = 0
    var dbUpdateJobs: DatabaseHandlerJobs? = null
    var handler: Handler? = null
    var notes: EditText? = null
    var runnable: Runnable? = null
    var showAddOrderItemIcon = false
    var OpenPagefromPushNotification = false
    var pdProcess: Dialog? = null
    var contextProgressDialog: TextView? = null
    var orderDetailId = 0
    var noBil = 0
    var scanRequiredForAllItem = false
    var assignedOrderStatusIDFromDB = 0
    var ackowledgedOrderStatusIDFromDB = 0
    var inprogressOrderStatusIDFromDB = 0
    var completedOrderStatusIDFromDB = 0
    var failedOrderStatusIDFromDB = 0
    var cancelledOrderStatusIDFromDB = 0
    var selfcollectOrderStatusIDFromDB = 0
    var settings: SharedPreferences? = null
    var unauthorizedNeedToLoginPage = false
    var isOnline = false
    var isManPowerCanUpdateProcess = false
    var mReceiver: BroadcastReceiver? = null
    var job_attributes_updated: JSONArray? = null
    var isJobRemoved = false
    var job_destroyed = false
    var jsonArray: String? = null
    var reasonFailed: String? = null
    var alert: AlertDialog? = null
    var imageBitmap: Bitmap? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var lastLocation: Location? = null
    var mLocationRequest: LocationRequest? = null
    var pdUpdateProcess: Dialog? = null
    var startJobStepsButton = false
    var showCompletedFailedButton = false
    var stepCompleteBoolean = false
    var mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            for (location: Location? in locationResult.locations) {
                lastLocation = location
            }
        }
    }
    private var slider: SliderLayout? = null
    private var isItemSelected = false
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_many_job_steps)
        FOLDER =
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!.absolutePath
        initialize()
        dbUpdateJobs = DatabaseHandlerJobs(this)
        OpenPagefromPushNotification = false
        scanRequiredForAllItem = false
        val extras = intent.extras
        if (extras != null) {
            orderDetailId = extras.getInt("OrderId")
            order_number = extras.getString("order_number")
            showAddOrderItemIcon = extras.getBoolean("showAddOrderItemIcon")
            OpenPagefromPushNotification = extras.getBoolean("OpenPagefromPushNotification")
            selectedDate = intent.getSerializableExtra("SelectedDate") as Date?
        }
        unauthorizedNeedToLoginPage = false
        settings = PreferenceManager.getDefaultSharedPreferences(this@ManyJobStepsActivity)
        val editors = settings?.edit()
        editors?.putBoolean("insideFragmentPage", false)?.apply()
        editors?.putBoolean("insideJobDetails", true)?.apply()
        editors?.putBoolean("insideNotificationListing", false)?.apply()
        editors?.putInt("orderIDinsideJobDetails", orderDetailId)?.apply()
        editors?.apply()
        pdProcess = Dialog(this)
        val inflater = layoutInflater
        val content = inflater.inflate(R.layout.custom_progress_dialog_process, null)
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog)
        contextProgressDialog?.text = resources.getString(R.string.updateJobSteps)
        pdProcess!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        pdProcess!!.setContentView(content)
        pdProcess!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        pdProcess!!.setCancelable(false)
        pdUpdateProcess = Dialog(this)
        val inflater2 = layoutInflater
        val content2 = inflater2.inflate(R.layout.custom_progress_dialog_process, null)
        contextProgressDialog = content2.findViewById(R.id.contextProgressDialog)
        contextProgressDialog?.text = resources.getString(R.string.updateJobSteps)
        pdUpdateProcess!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        pdUpdateProcess!!.setContentView(content2)
        pdUpdateProcess!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        pdUpdateProcess!!.setCancelable(false)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setBackgroundColor(resources.getColor(R.color.actionbar_color))
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)


        // action bar for app
        val actionBar = supportActionBar
        if (actionBar != null) {
            val colorDrawable = ColorDrawable(Color.parseColor("#2e86aa"))
            actionBar.setBackgroundDrawable(colorDrawable)
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
        callGetJobStepDetails()
        layoutItemOrderDetails!!.setOnClickListener(View.OnClickListener {
            if (jobOrder!!.orderDetailsArrayList.size > 0) {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    val i = Intent(applicationContext, ItemsOrderDetailsNoInternet::class.java)
                    i.putExtra("OrderId", orderDetailId)
                    i.putExtra("orderDetailStatusId", jobOrder!!.orderStatusId)
                    if (handler != null) {
                        handler!!.removeCallbacks((runnable)!!)
                    }
                    startActivity(i)
                } else {
                    val i = Intent(applicationContext, ItemsOrderDetails::class.java)
                    i.putExtra("OrderId", orderDetailId)
                    i.putExtra("orderDetailStatusId", jobOrder!!.orderStatusId)
                    if (handler != null) {
                        handler!!.removeCallbacks((runnable)!!)
                    }
                    startActivity(i)
                }
            } else {
                // cannot open item list page because dont have any items
                SnackBuilder(findViewById(R.id.mainPage))
                    .setText("No items")
                    .setTextColor("#ffffff") //optional
                    .setTextSize(15f)
                    .setBgColor("#C1272D") // .setActionBtnColor("#f44336")
                    .setIcon(Icon.ERROR) //or  .setIcon(R.drawable.ic_info_black_24dp)
                    .setDurationInSeconds(1) //                            .setActionListener("Ok", new View.OnClickListener() {
                    //                                @Override
                    //                                public void onClick(View view) {
                    //                                    Toast.makeText(getApplicationContext(),"done",Toast.LENGTH_LONG).show();
                    //                                }
                    //                            })
                    .build()
            }
        })
        mClickCallLayout_sim!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.CALL_PHONE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    SnackBuilder(findViewById(R.id.mainPage))
                        .setText("You must allow permission for call phone")
                        .setTextColor("#ffffff") //optional
                        .setTextSize(15f)
                        .setBgColor("#C1272D")
                        .setIcon(Icon.ERROR)
                        .setDurationInSeconds(1)
                        .build()
                    return
                } else {
                    val isCallingDirectly = true
                    var contactNum = ""

                    //Update UI (no need for pickupContact)
                    contactNum =
                        if (jobOrder!!.contactNum.isEmpty()) jobOrder!!.dropOffContact else jobOrder!!.contactNum
                    val uri = "tel:$contactNum"
                    val intent3 = Intent(
                        if (isCallingDirectly) Intent.ACTION_CALL else Intent.ACTION_DIAL,
                        Uri.parse(uri)
                    )
                    startActivity(intent3)
                }
            }
        })
        mClickCallLayout_whatsapp!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                if (ActivityCompat.checkSelfPermission(
                        applicationContext,
                        Manifest.permission.CALL_PHONE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    SnackBuilder(findViewById(R.id.mainPage))
                        .setText("You must allow permission for call phone")
                        .setTextColor("#ffffff") //optional
                        .setTextSize(15f)
                        .setBgColor("#C1272D")
                        .setIcon(Icon.ERROR)
                        .setDurationInSeconds(1)
                        .build()
                    return
                } else {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://api.whatsapp.com/send?phone=" + (if (jobOrder!!.contactNum.isEmpty()) jobOrder!!.dropOffContact else jobOrder!!.contactNum) + "&text=")
                    )
                    startActivity(intent)
                }
            }
        })
        btnAcceptJob!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    val isManPower = settings?.getBoolean("isManPower", false)
                    if (isManPower == true) {
                        isManPowerCanUpdateProcess = false
                        checkingFunctionBasedOnStatus(
                            Constants.ASSIGNED,
                            Constants.ACKNOWLEDGED,
                            false,
                            "is_status_need_manpower"
                        )
                        if (isManPowerCanUpdateProcess) {
                            wantProceedIfNoInternet(1)
                        } else {
                            showAlertManPower()
                        }
                    } else {
                        wantProceedIfNoInternet(1)
                    }
                } else {
                    isOnline = settings?.getBoolean("isOnline", true) ?: true
                    if (checkGPSBeforeUpdate()) {
                        if (isOnline) {
                            isManPowerCanUpdateProcess = false
                            checkingFunctionBasedOnStatus(
                                Constants.ASSIGNED,
                                Constants.ACKNOWLEDGED,
                                false,
                                "is_status_need_manpower"
                            )
                            val resource_owner_id = settings?.getInt("resource_owner_id", 0)
                            if (jobOrder!!.dropOffWorkerId != resource_owner_id) {
                                if (isManPowerCanUpdateProcess) {
                                    acceptJobToAcknowledged()
                                } else {
                                    // cannot update
                                    showAlertManPower()
                                    //showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                }
                            } else {
                                acceptJobToAcknowledged()
                            }
                        } else {
                            showNeedOnlineDialog(
                                resources.getString(R.string.offlineAcceptJobTitle),
                                resources.getString(
                                    R.string.offlineMessage
                                )
                            ).show()
                            LogCustom.i("isOnline", "false")
                        }
                    } else {
                        showNeedOnlineDialog(
                            resources.getString(R.string.offlineAcceptJobTitle),
                            resources.getString(
                                R.string.offlineMessage
                            )
                        ).show()
                    }
                }
            }
        })
        btnReattemptJob!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    val isManPower = settings?.getBoolean("isManPower", false)
                    if (isManPower == true) {
                        isManPowerCanUpdateProcess = false
                        checkingFunctionBasedOnStatus(
                            Constants.FAILED,
                            Constants.IN_PROGRESS,
                            false,
                            "is_status_need_manpower"
                        )
                        if (isManPowerCanUpdateProcess) {
                            wantProceedIfNoInternet(2)
                        } else {
                            showAlertManPower()
                        }
                    } else {
                        wantProceedIfNoInternet(2)
                    }
                } else {
                    isOnline = settings?.getBoolean("isOnline", true) ?: true
                    if (checkGPSBeforeUpdate()) {
                        if (isOnline) {
                            val isManPower = settings?.getBoolean("isManPower", false)
                            if (isManPower == true) {
                                isManPowerCanUpdateProcess = false
                                checkingFunctionBasedOnStatus(
                                    Constants.FAILED,
                                    Constants.IN_PROGRESS,
                                    false,
                                    "is_status_need_manpower"
                                )
                                if (isManPowerCanUpdateProcess) {
                                    reattemptJobFailedJsonStructure()
                                } else {
                                    // cannot update
                                    //  showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                    showAlertManPower()
                                }
                            } else {
                                reattemptJobFailedJsonStructure()
                            }
                        } else {
                            showNeedOnlineDialog(
                                resources.getString(R.string.offlineReattemptJobTitle),
                                resources.getString(
                                    R.string.offlineMessage
                                )
                            ).show()
                            LogCustom.i("isOnline", "false")
                        }
                    } else {
                        showNeedOnlineDialog(
                            resources.getString(R.string.offlineReattemptJobTitle),
                            resources.getString(
                                R.string.offlineMessage
                            )
                        ).show()
                    }
                }
            }
        })
        mReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                LogCustom.i("refresh job details true")
                val extras = intent.extras
                if (extras != null) {
                    isJobRemoved = extras.getBoolean("isJobRemoved")
                    job_destroyed = extras.getBoolean("job_destroyed")
                    jsonArray = extras.getString("job_attributes_updated")
                }
                try {
                    LogCustom.i("refresh job")
                    job_attributes_updated = JSONArray(jsonArray)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                LogCustom.i("refresh job11", "" + isJobRemoved)
                LogCustom.i("refresh job22", "" + job_destroyed)
                LogCustom.i("refresh job33", "" + job_attributes_updated)
                if (job_destroyed || isJobRemoved) {
                    //job deleted
                    refreshJobDialog(
                        resources.getString(R.string.jobRemovedTitle),
                        resources.getString(R.string.jobRemovedContent),
                        resources.getString(R.string.jobRemovedButtonText),
                        isJobRemoved,
                        job_destroyed,
                        job_attributes_updated,
                        resources.getDrawable(
                            R.drawable.icon_trash_popup
                        )
                    )
                } else if (job_attributes_updated!!.length() > 0) {
                    //job updated
                    refreshJobDialog(
                        resources.getString(R.string.jobUpdatedTitle),
                        resources.getString(R.string.jobUpdatedContent),
                        resources.getString(R.string.jobUpdatedButtonText),
                        isJobRemoved,
                        job_destroyed,
                        job_attributes_updated,
                        resources.getDrawable(
                            R.drawable.icon_refresh
                        )
                    )
                }
            }
        }
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(mReceiver!!, IntentFilter(ACTION_REFRESH_JOB_DETAIL))
        if (OpenPagefromPushNotification) {
            val notificationDatabase = NotificationDatabase()
            notificationDatabase.isDoneRead = 1
            notificationDatabase.orderId = orderDetailId.toString()
            dbUpdateJobs!!.addNotification(notificationDatabase, true, true)
            ShortcutBadger.applyCount(applicationContext, dbUpdateJobs!!.totalNotificationUnRead)
            val ii = Intent(DrawerActivity.ACTION_REFRESH_NOTIFICATION_SIDE_MENU)
            LocalBroadcastManager.getInstance(applicationContext)
                .sendBroadcast(ii)
        }
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                        == PackageManager.PERMISSION_GRANTED)
            ) {
                buildGoogleApiClient()
                // mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient()
            // mMap.setMyLocationEnabled(true);
        }
    }

    fun addImages() {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null)
        val layoutClickCamera = alertLayout.findViewById<LinearLayout>(R.id.layoutClickCamera)
        val layoutClickGallery = alertLayout.findViewById<LinearLayout>(R.id.layoutClickGallery)
        layoutClickCamera.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                uriPhotoTakenByCamera =
                    AndroidOp.startCameraForResult(
                        this@ManyJobStepsActivity,
                        getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                        null
                    )
            }
        })
        layoutClickGallery.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                AndroidOp.startGettingImageFromGalleryForResult(this@ManyJobStepsActivity)
            }
        })
        val alert = AlertDialog.Builder(this)
        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo")
            alert.setPositiveButton("Done", object : DialogInterface.OnClickListener {
                override fun onClick(dialogInterface: DialogInterface, i: Int) {
                    //do nothing
                }
            })
        } else {
            alert.setTitle("Complete action using")
            alert.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, which: Int) {}
            })
        }
        alert.setView(alertLayout)
        alert.setCancelable(false)
        imageDialog = alert.create()
        if (imageDialog?.isShowing == false) {
            imageDialog?.show()
        }
    }

    fun showImageAfterUpload(openImageAgain: Boolean) {
        //Update show image first


        //  totalImage = totalImage + 1;
        val url_maps = HashMap<String, String?>()

        // descriptionImageString = descriptionImage.get(totalImage - 1);
        imageBitmapArray.clear()
        slider!!.removeAllSliders()
        if (urlImageStringArray.size > 0) {
            for (i in urlImageStringArray.indices) {
                url_maps["" + urlDescriptionStringArray.get(i)] = urlImageStringArray.get(i)
            }
        }
        for (name: String in url_maps.keys) {
            val textSliderView = TextSliderView(this@ManyJobStepsActivity)
            // initialize a SliderLayout
            textSliderView
                .description(name)
                .image(url_maps[name])
                .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                .setOnSliderClickListener(this@ManyJobStepsActivity)
            textSliderView.bundle(Bundle())
            textSliderView.bundle
                .putString("extra", name)
            slider!!.addSlider(textSliderView)
        }
        slider!!.visibility = View.VISIBLE
        if (urlImageStringArray.size > 0) {
            slider!!.visibility = View.VISIBLE
            if (urlImageStringArray.size < 2) {
                Log.i("totalImage1", "" + urlImageStringArray.size)
                slider!!.stopAutoCycle()
                slider!!.setPagerTransformer(false, object : BaseTransformer() {
                    override fun onTransform(view: View, v: Float) {}
                })
                //
            } else if (urlImageStringArray.size > 1) {
                Log.i("totalImage2", "" + urlImageStringArray.size)
                slider!!.startAutoCycle()
                slider!!.setPagerTransformer(true, object : BaseTransformer() {
                    override fun onTransform(view: View, v: Float) {}
                })
            }
        } else if (urlImageStringArray.size == 0) {
            slider!!.removeAllSliders()
            slider!!.visibility = View.GONE
        }
        //Update show image first
        if (openImageAgain) {
            takeAnotherPhotoBool = false
            requestTakeOtherPhoto = true
            addImages()
        }
    }

    fun addDescription(data: Intent?, resultFromGalleryDescription: Boolean) {
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.items_layout_description_image, null)
        val textViewTitle = alertLayout.findViewById<TextView>(R.id.textViewTitle)
        val editTextDescription: TextInputEditText =
            alertLayout.findViewById(R.id.editTextDescription)
        textViewTitle.text = "Add Description for Image"
        val builder = AlertDialog.Builder(this)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        builder.setPositiveButton("OK",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, which: Int) {
                    descriptionImageText = editTextDescription.text.toString()
                    dialog.dismiss()
                    if (!resultFromGalleryDescription) {
                        try {
                            resultFromGallery = false
                            showImageCameraGallery(uriPhotoTakenByCamera)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    } else {
                        try {
                            resultFromGallery = true
                            val photoUri = data!!.data
                            showImageCameraGallery(photoUri)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            })
        val dialog = builder.create()
        dialog.show()
    }

    @Throws(IOException::class)
    fun showImageCameraGallery(uri: Uri?) {
        var bitmap: Bitmap?
        selectedUri = uri
        if (resultFromGallery) {
            val selectedImagePath = OneJobStepActivity.getFullPathFromContentUri(
                applicationContext, uri
            )
            bitmap = BitmapFactory.decodeFile(selectedImagePath)
        } else {
            bitmap = BitmapFactory.decodeFile(uri!!.path)
        }
        fileFromBitmap(bitmap!!)
    }

    fun showEditDeletePhotoGaleryImage() {
        val inflater = layoutInflater
        val alertLayout =
            inflater.inflate(R.layout.item_activity_order_attempt_detail_image_inflate, null)
        val viewPager: ViewPager = alertLayout.findViewById(R.id.viewPager)
        myCustomPagerAdapter = EditDeleteOrderAttemptAdapter(
            this@ManyJobStepsActivity,
            urlImageStringArray,
            urlDescriptionStringArray
        )
        viewPager.adapter = myCustomPagerAdapter
        val builder = AlertDialog.Builder(this)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        builder.setPositiveButton("DONE",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, which: Int) {}
            })
        val dialog = builder.create()
        dialog.show()

        //Grab the window of the dialog, and change the width
        val lp = WindowManager.LayoutParams()
        val window = dialog.window
        lp.copyFrom(window!!.attributes)
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.MATCH_PARENT
        window.attributes = lp
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View) {
                    dialog.dismiss()
                    showImageAfterUpload(false)
                }
            })
    }

    fun requestBody(imageFile: File?, selectedImagePath: String?, isSignature: Boolean) {
        val image = (imageFile)!!.asRequestBody("image/jpg".toMediaTypeOrNull())
        val requestBody: RequestBody = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("picture", selectedImagePath, image)
            .addFormDataPart("picture_name", imageFile.name)
            .addFormDataPart("folder_name", "" + orderDetailId + " - " + jobOrder!!.orderId)
            .build()
        Log.e("JobStep picture_name", " : " + imageFile.name)
        Log.e("JobStep folder_name", " : " + orderDetailId + " - " + jobOrder!!.orderId)
        val progressListener: CountingRequestBody.Listener = object : CountingRequestBody.Listener {
            override fun onRequestProgress(bytesRead: Long, contentLength: Long) {
                if (bytesRead >= contentLength) {
                } else {
                    if (contentLength > 0) {
                        val progress = ((bytesRead.toDouble() / contentLength) * 100).toInt()
                        Log.e("uploadProgress called", "$progress ")
                    }
                }
            }
        }
        val imageUploadClient: OkHttpClient = OkHttpClient.Builder()
            .addNetworkInterceptor(object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): Response {
                    val originalRequest: Request = chain.request()
                    if (originalRequest.body == null) {
                        return chain.proceed(originalRequest)
                    }
                    val progressRequest = originalRequest.newBuilder()
                        .method(
                            originalRequest.method,
                            CountingRequestBody(originalRequest.body, progressListener)
                        )
                        .build()
                    return chain.proceed(progressRequest)
                }
            }).connectTimeout(10, TimeUnit.MINUTES)
            .writeTimeout(10, TimeUnit.MINUTES)
            .readTimeout(10, TimeUnit.MINUTES)
            .build()
        val settings = PreferenceManager.getDefaultSharedPreferences(
            applicationContext
        )
        val accessToken = settings?.getString("access_token", "")
        val request: Request = Request.Builder()
            .url(SAAS_SERVER_AUTH + "/api/upload") //                .url("https://devstaging-auth-api.worknode.ai/api/upload")
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .header("Authorization", "Bearer $accessToken")
            .post(requestBody)
            .build()
        imageUploadClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread(object : Runnable {
                    override fun run() {
                        val mMessage = e.message
                        Toast.makeText(
                            this@ManyJobStepsActivity,
                            "Error uploading file",
                            Toast.LENGTH_LONG
                        ).show()
                        if (pdUpdateProcess != null && pdUpdateProcess!!.isShowing) {
                            pdUpdateProcess!!.dismiss()
                        }
                    }
                })
            }

            @Throws(IOException::class)
            override fun onResponse(call: Call, response: Response) {
                val body: String = response.body!!.string()
                runOnUiThread(object : Runnable {
                    override fun run() {
                        Log.d("ImageUpload", (body))
                        if (response.code == 200) {
                            runOnUiThread(object : Runnable {
                                override fun run() {
                                    if (pdUpdateProcess != null) {
                                        if (pdUpdateProcess!!.isShowing) {
                                            pdUpdateProcess!!.dismiss()
                                        }
                                    }
                                    if (body == null || body.isEmpty()) {
                                    } else {
                                        try {
                                            val data = JSONObject(body)
                                            val resultData = data.getJSONObject("result")
                                            image_normalURL =
                                                resultData.getString("image_medium_url")

                                            /*check either it signature or image upload. If image upload, show image slider and add image again.
                                            If signature, direct update order.
                                            */if (isSignature) {
                                                val signatureOrderAttemptImage = OrderAttemptImage(
                                                    image_normalURL,
                                                    "Recipient’s Signature",
                                                    false
                                                )
                                                orderAttempt!!.getmOrderAttemptImages()
                                                    .add(signatureOrderAttemptImage)
                                                orderAttemptImagesForFailed.add(
                                                    signatureOrderAttemptImage
                                                )
                                                var imageOrderAttemptImage: OrderAttemptImage
                                                //                                               orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);
                                                if (urlImageStringArray.size > 0) {
                                                    for (i in urlImageStringArray.indices) {
                                                        imageOrderAttemptImage = OrderAttemptImage(
                                                            urlImageStringArray[i],
                                                            urlDescriptionStringArray[i],
                                                            false
                                                        )
                                                        orderAttempt!!.getmOrderAttemptImages()
                                                            .add(imageOrderAttemptImage)
                                                    }
                                                }

//                                                try {
//                                                    jsonStructureBatchUpdate();
//                                                } catch (IOException e) {
//                                                    e.printStackTrace();
//                                                }
                                            } else {
                                                val signatureOrderAttemptImage = OrderAttemptImage(
                                                    image_normalURL,
                                                    descriptionImageText,
                                                    false
                                                )
                                                orderAttemptImagesForFailed.add(
                                                    signatureOrderAttemptImage
                                                )
                                                urlImageStringArray.add(image_normalURL)
                                                urlDescriptionStringArray.add(descriptionImageText)
                                                showImageAfterUpload(false)
                                            }
                                        } catch (e: JSONException) {
                                            LogCustom.e(e)
                                            try {
                                            } catch (t: Throwable) {
                                            }
                                        }
                                    }
                                }
                            })
                        }
                    }
                })
            }
        })
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
        mGoogleApiClient?.connect()
    }

    private fun wantProceedIfNoInternet(optionProcess: Int) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("No internet connection")
            .setMessage("Your device not connect with internet. Please check your internet connection")
            .setCancelable(false)
            .setPositiveButton("Open settings",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, id: Int) {
                        dialog.dismiss()
                        dialog.cancel()
                        val intent = Intent(Intent.ACTION_MAIN, null)
                        intent.addCategory(Intent.CATEGORY_LAUNCHER)
                        val cn = ComponentName(
                            "com.android.settings",
                            "com.android.settings.wifi.WifiSettings"
                        )
                        intent.component = cn
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    }
                })
        alertDialogBuilder.setNegativeButton("Cancel",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, id: Int) {
                    dialog.dismiss()
                    dialog.cancel()
                    onBackPressed()
                }
            })
        alertDialogBuilder.setNeutralButton("Continue without internet",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, id: Int) {
                    dialog.dismiss()
                    dialog.cancel()
                    when (optionProcess) {
                        1 -> changeJobStatusWithoutInternetConnection(true)
                        2 -> changeJobStatusWithoutInternetConnection(false)
                        3 -> {
                            jobOrder = JobOrder()
                            jobOrder = dbUpdateJobs!!.getJobDetailsBasedOnID(orderDetailId)
                            jobOrder?.jobStepsArrayList =
                                dbUpdateJobs!!.getListJobStepsDetailsNoInternet(
                                    jobOrder?.orderId ?: 0
                                )
                            jobOrder?.orderDetailsArrayList =
                                dbUpdateJobs!!.getListJobItemsDetailsNoInternet(
                                    jobOrder?.orderId ?: 0
                                )
                            displayData(jobOrder)
                        }
                    }
                }
            })
        alert = alertDialogBuilder.create()
        if (alert?.isShowing == false) {
            alert?.show()
        }
    }

    fun changeJobStatusWithoutInternetConnection(acceptJob: Boolean) {
        jobOrder!!.isUpdateWithNoInternet = true
        if (acceptJob) {
            jobOrder!!.orderStatusId = ackowledgedOrderStatusIDFromDB
            jobOrder!!.isReattemptJob = false
        } else {
            jobOrder!!.orderStatusId = inprogressOrderStatusIDFromDB
            jobOrder!!.isReattemptJob = true
        }
        if (jobOrder!!.jobStepsArrayList.size > 0) {
            for (ii in jobOrder!!.jobStepsArrayList.indices) {
                val jobSteps = jobOrder!!.jobStepsArrayList[ii]
                if (acceptJob) {
                    jobSteps.isJobStepUpdateWithNoInternet = true
                    jobSteps.job_step_status_id = Constants.JOB_STEP_PENDING
                } else {
                    if (jobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                        jobSteps.isJobStepUpdateWithNoInternet = true
                        jobSteps.job_step_status_id = Constants.JOB_STEP_INPROGRESS
                        jobOrder!!.reattemptJobStepId = jobSteps.id
                    } else {
                        jobSteps.isJobStepUpdateWithNoInternet = false
                    }
                }
                dbUpdateJobs!!.addJobStepDetails(jobSteps)
                if (jobSteps.getmOrderAttemptsList().size > 0) {
                    for (j in jobSteps.getmOrderAttemptsList().indices) {
                        val orderAttempt = jobSteps.getmOrderAttemptsList()[j]
                        orderAttempt.orderId = jobOrder!!.orderId
                        orderAttempt.isAttemptUpdateWithNoInternet = false
                        dbUpdateJobs!!.addOrderAttemptDetails(orderAttempt)
                        if (orderAttempt.getmOrderAttemptImages().size > 0) {
                            for (k in orderAttempt.getmOrderAttemptImages().indices) {
                                val orderAttemptImage = orderAttempt.getmOrderAttemptImages()[k]
                                orderAttemptImage.orderId = orderAttemptImage.orderId
                                orderAttemptImage.id = orderAttemptImage.id
                                orderAttemptImage.note = orderAttemptImage.note
                                orderAttemptImage.isSignature = orderAttemptImage.isSignature
                                orderAttemptImage.isRemove = false
                                orderAttemptImage.orderAttemptID = orderAttempt.id
                                orderAttemptImage.isImageUpdateNoInternet = false
                                orderAttemptImage.url = orderAttemptImage.url
                                dbUpdateJobs!!.addImageDetails(orderAttemptImage)
                            }
                        }
                    }
                }
            }
        }
        dbUpdateJobs!!.addJobDetails(jobOrder)
        if (jobOrder!!.orderDetailsArrayList.size > 0) {
            for (ii in jobOrder!!.orderDetailsArrayList.indices) {
                val orderDetails = jobOrder!!.orderDetailsArrayList[ii]
                dbUpdateJobs!!.addJobItemDetails(orderDetails)
            }
        }
        jobOrder = JobOrder()
        jobOrder = dbUpdateJobs!!.getJobDetailsBasedOnID(orderDetailId)
        jobOrder?.jobStepsArrayList = dbUpdateJobs!!.getListJobStepsDetailsNoInternet(
            jobOrder?.orderId
                ?: 0
        )
        jobOrder?.orderDetailsArrayList = dbUpdateJobs!!.getListJobItemsDetailsNoInternet(
            jobOrder?.orderId
                ?: 0
        )
        displayData(jobOrder)
    }

    fun showAlertManPower() {
        //  showErrorDialog(getActivity().getResources().getString(R.string.manPowerErrorTitle),getActivity().getResources().getString(R.string.manPowerErrorMessage)).show();
        val builder1 = AlertDialog.Builder(this)
        builder1.setMessage(resources.getString(R.string.manPowerErrorMessage))
        builder1.setCancelable(false)
        builder1.setTitle(resources.getString(R.string.manPowerErrorTitle))
        builder1.setPositiveButton(
            "OK",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, id: Int) {
                    dialog.cancel()
                }
            })
        val alert11 = builder1.create()
        alert11.show()
    }

    fun checkGPSBeforeUpdate(): Boolean {
        val alreadyOnGps: Boolean
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LogCustom.i("GPS is Enabled in your device true")
            LogCustom.i("GPS is Enabled in your device true")
            alreadyOnGps = true
        } else {
            val editors = settings!!.edit()
            editors.putBoolean("isOnline", false).apply()
            alreadyOnGps = false
            val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
            LocalBroadcastManager.getInstance(applicationContext)
                .sendBroadcast(ii)
        }
        return alreadyOnGps
    }

    fun reattemptJobFailedJsonStructure() {
        try {
            var lastJobSteps = JobSteps()
            val jobOrderObject = JSONObject()
            val jobOrderIdArray = JSONArray()
            jobOrderIdArray.put(jobOrder!!.orderId.toString())
            jobOrderObject.put("id", jobOrderIdArray)

            // inprogressOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT);
            jobOrderObject.put(
                "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                    Constants.INPROGRESS_TEXT
                )
            )
            jobOrderObject.put("reattempt", true)
            for (i in jobOrder!!.jobStepsArrayList.indices) {
                val jobSteps = jobOrder!!.jobStepsArrayList[i]
                if (jobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                    lastJobSteps = jobSteps
                    break
                }
            }
            jobOrderObject.put("reattempt_job_step_id", lastJobSteps.id)
            val dataObject = JSONObject()
            val jobDataArray = JSONArray()
            jobDataArray.put(jobOrderObject)
            dataObject.put("data", jobDataArray)
            LogCustom.i("dataObject", dataObject.toString(4))
            updateJobStatus(dataObject, jobOrderIdArray, true)
        } catch (e: JSONException) {
            e.printStackTrace()
            try {
            } catch (t: Throwable) {
            }
        }
    }

    fun callGetJobStepDetails() {
        try {
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {
                Log.i("internetConnection", "no")
                wantProceedIfNoInternet(3)
            } else {
                if (orderDetailId != 0) {
                    jobStepDetails
                } else {
                    showErrorDialog(
                        resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                            R.string.orderDetailError
                        )
                    ).show()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver((mReceiver)!!)
    }

    fun acceptJobToAcknowledged() {
        try {
            ackowledgedOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                Constants.ACKNOWLEDGED_TEXT
            )
            val jobOrderObject = JSONObject()
            val jobOrderIdArray = JSONArray()
            jobOrderIdArray.put(jobOrder!!.orderId.toString())
            jobOrderObject.put("id", jobOrderIdArray)
            jobOrderObject.put("order_status_id", ackowledgedOrderStatusIDFromDB)
            val dataObject = JSONObject()
            val jobDataArray = JSONArray()
            jobDataArray.put(jobOrderObject)
            dataObject.put("data", jobDataArray)
            LogCustom.i("dataObject", dataObject.toString(4))
            updateJobStatus(dataObject, jobOrderIdArray, false)
        } catch (e: JSONException) {
            e.printStackTrace()
            try {
            } catch (t: Throwable) {
            }
        }
    }

    /* ===================== CHECKING FUNCTION FOR EVERY STATUS ======================== */ /* ===================== CHECKING FUNCTION FOR EVERY STATUS ======================== */
    fun checkingFunctionBasedOnStatus(
        currentStatus: Int,
        toStatus: Int,
        isNeedScan: Boolean,
        rule: String?
    ) {
        if (dbUpdateJobs!!.checkStatusNeedScanOrManPower(
                currentStatus,
                toStatus,
                isNeedScan,
                rule
            )
        ) {
            LogCustom.i("Man Power")
            isManPowerCanUpdateProcess = true
        }
    }

    /* ================ UPDATE JOB STATUS ====================== */ /* ================ UPDATE JOB STATUS ====================== */
    fun updateJobStatus(
        jobOrderObject: JSONObject,
        jobOrderIdArray: JSONArray?,
        reattemptProcess: Boolean
    ) {
        if (reattemptProcess) {
            contextProgressDialog!!.text = resources.getString(R.string.reattemptThisJOb)
        } else {
            contextProgressDialog!!.text =
                resources.getString(R.string.updateAssignedToAcknowledged)
        }
        if (pdProcess!!.isShowing == false) {
            pdProcess!!.show()
            LogCustom.i("load progress", "in")
        } else {
            LogCustom.i("load progress", "off")
        }
        val okHttpRequest = OkHttpRequest()
        try {
            OkHttpRequest.batchUpdate(
                applicationContext, jobOrderObject.toString(), object : OKHttpNetwork {
                    override fun onSuccess(body: String, responseCode: Int) {
                        runOnUiThread(object : Runnable {
                            override fun run() {
                                if ((pdProcess != null) && pdProcess!!.isShowing) {
                                    pdProcess!!.dismiss()
                                }
                                try {
                                    val data = JSONObject(body)
                                    val result = data.getJSONArray("result")
                                    if (data.has("status") && !(data.isNull("status"))) {
                                        if (data.getBoolean("status")) {

                                            //  menuSearch.collapseActionView();
                                            if (reattemptProcess) {
                                                showSuccessDialog(
                                                    resources.getString(R.string.reattemptTitle),
                                                    resources.getString(
                                                        R.string.reattemptMessage
                                                    )
                                                ).show()
                                            } else {
                                                showSuccessDialog(
                                                    resources.getString(R.string.assignedToacknowledgedTitle),
                                                    resources.getString(
                                                        R.string.assignedToacknowledgedMessage
                                                    )
                                                ).show()
                                            }
                                        }
                                    }
                                } catch (e: JSONException) {
                                    LogCustom.i(body, "result")
                                }
                            }
                        })
                    }

                    override fun onFailure(
                        body: String,
                        requestCallBackError: Boolean,
                        responseCode: Int
                    ) {
                        runOnUiThread(object : Runnable {
                            override fun run() {
                                if ((pdProcess != null) && pdProcess!!.isShowing) {
                                    pdProcess!!.dismiss()
                                }
                                errorHandlingFromResponseCode(
                                    body,
                                    requestCallBackError,
                                    responseCode
                                )
                            }
                        })
                    }
                })
        } catch (e: Exception) {
            if ((pdProcess != null) && pdProcess!!.isShowing) {
                pdProcess!!.dismiss()
            }
            e.printStackTrace()
        }
    }

    ///success dialog
    fun showSuccessDialog(title: String?, context: String?): SweetAlertDialog {
        val sweetAlertDialog = getSuccessDialog(title, context)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    fun getSuccessDialog(title: String?, context: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
            .setContentText(context)
            .setTitleText(title)
            .setConfirmText("OK")
            .setConfirmClickListener(object : OnSweetClickListener {
                override fun onClick(sDialog: SweetAlertDialog) {
                    sDialog.dismissWithAnimation()
                    onBackPressed()
                }
            })
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */ /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    fun errorHandlingFromResponseCode(
        body: String?,
        requestCallBackError: Boolean,
        responseCode: Int
    ) {
        runOnUiThread(object : Runnable {
            override fun run() {
                if (!requestCallBackError) {
                    unauthorizedNeedToLoginPage = false
                    try {
                        var errorMessage = ""
                        var data: JSONObject? = null
                        if (body != null || !body.equals("", ignoreCase = true)) {
                            try {
                                data = JSONObject(body)
                            } catch (e: Exception) {
                                showErrorDialog(
                                    resources.getString(R.string.globalErrorMessageTitle),
                                    resources.getString(
                                        R.string.globalErrorMessageContent
                                    )
                                ).show()
                            }
                            if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                if (data!!.has("error") && !(data.isNull("error"))) {
                                    try {
                                        errorMessage = data.getString("error")
                                    } catch (e: Exception) {
                                        val errorMessageArray = data.getJSONObject("error")
                                        errorMessage = errorMessageArray.getString("error")
                                    }
                                }
                                if (errorMessage.equals("", ignoreCase = true)) {
                                    errorMessage = resources.getString(R.string.pleaseContactAdmin)
                                }
                                showErrorDialog(
                                    resources.getString(R.string.badRequestErrorTitle),
                                    errorMessage
                                ).show()
                            } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {
                                unauthorizedNeedToLoginPage = true
                                if (data!!.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                    if (data.getBoolean("device_not_found")) {
                                        showErrorDialog(
                                            resources.getString(R.string.deviceNotFoundTitle),
                                            resources.getString(
                                                R.string.pleaseContactAdmin
                                            )
                                        ).show()
                                    }
                                } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                    if (data.getBoolean("device_is_banned")) {
                                        showErrorDialog(
                                            resources.getString(R.string.deviceBannedTitle),
                                            resources.getString(
                                                R.string.pleaseContactAdmin
                                            )
                                        ).show()
                                    }
                                } else {
                                    showErrorDialog(
                                        resources.getString(R.string.errorLogin),
                                        resources.getString(
                                            R.string.pleaseCheckPassword
                                        )
                                    ).show()
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {
                                unauthorizedNeedToLoginPage = true
                                if (data!!.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                    if (data.getBoolean("unpaid_subscription")) {
                                        showErrorDialog(
                                            resources.getString(R.string.unpaidSubscriptionTitle),
                                            resources.getString(
                                                R.string.pleaseContactAdmin
                                            )
                                        ).show()
                                    }
                                } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                    if (data.getBoolean("quota_reached")) {
                                        showErrorDialog(
                                            resources.getString(R.string.quotaReachedTitle),
                                            resources.getString(
                                                R.string.pleaseContactAdmin
                                            )
                                        ).show()
                                    }
                                } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                    if (data.getBoolean("blacklist")) {
                                        showErrorDialog(
                                            resources.getString(R.string.blacklistTitle),
                                            resources.getString(
                                                R.string.pleaseContactAdmin
                                            )
                                        ).show()
                                    }
                                } else {
                                    showErrorDialog(
                                        resources.getString(R.string.errorLogin),
                                        resources.getString(
                                            R.string.pleaseCheckPassword
                                        )
                                    ).show()
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                                showErrorDialog(
                                    resources.getString(R.string.globalErrorMessageTitleSorry),
                                    resources.getString(
                                        R.string.globalErrorMessageContent
                                    )
                                ).show()
                            } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                showErrorDialog(
                                    resources.getString(R.string.globalErrorMessageTitleSorry),
                                    resources.getString(
                                        R.string.globalErrorMessageContent
                                    )
                                ).show()
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                showErrorDialog(
                                    resources.getString(R.string.globalErrorMessageTitleSorry),
                                    resources.getString(
                                        R.string.globalErrorMessageContent
                                    )
                                ).show()
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                showErrorDialog(
                                    resources.getString(R.string.globalErrorMessageTitleSorry),
                                    resources.getString(
                                        R.string.globalErrorTimeoutMessageContent
                                    )
                                ).show()
                            }
                        } else {
                            showErrorDialog(
                                resources.getString(R.string.globalErrorMessageTitle),
                                resources.getString(
                                    R.string.globalErrorMessageContent
                                )
                            ).show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                } else {
                    if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        showErrorDialog(
                            resources.getString(R.string.globalErrorMessageTitleSorry),
                            resources.getString(
                                R.string.globalErrorMessageContent
                            )
                        ).show()
                    } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                        showErrorInternetDialog().show()
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        showErrorDialog(
                            resources.getString(R.string.globalErrorMessageTitleSorry),
                            resources.getString(
                                R.string.globalErrorTimeoutMessageContent
                            )
                        ).show()
                    } else {
                        showErrorDialog(
                            resources.getString(R.string.globalErrorMessageTitleSorry),
                            resources.getString(
                                R.string.globalErrorMessageContent
                            )
                        ).show()
                    }
                    // this for request call back error. Maybe because cannot connect server.
                }
            }
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (handler != null) {
                handler!!.removeCallbacks((runnable)!!)
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    fun initialize() {
        layoutItemOrderDetails = findViewById(R.id.layoutItemOrderDetails)
        mClickCallLayout_sim = findViewById(R.id.layoutClickCall_sim)
        mClickCallLayout_whatsapp = findViewById(R.id.layoutClickCall_whatsapp)
        mLayoutJobSteps = findViewById(R.id.mLayoutJobSteps)
        layoutJobStatus = findViewById(R.id.layoutJobStatus)
        orderstatusdetail = findViewById(R.id.orderstatusdetail)
        trackingNumberOrderDetails = findViewById(R.id.trackingNumberOrderDetails)
        descriptionOrderDetails = findViewById(R.id.descriptionOrderDetails)
        driverNotesLabel = findViewById(R.id.driverNotes)
        totalItemOrderDetails = findViewById(R.id.totalItemOrderDetails)
        phNo_sim = findViewById(R.id.phNo_sim)
        phNo_whatsapp = findViewById(R.id.phNo_whatsapp)
        trackingNumberLabel = findViewById(R.id.trackingNumberLabel)
        btnAcceptJob = findViewById(R.id.btnAcceptJob)
        btnReattemptJob = findViewById(R.id.btnReattemptJob)
        companyNameLabel = findViewById(R.id.companyName)
    }

    fun displayData(jobOrder: JobOrder?) {
        if (jobOrder!!.jobType != null && jobOrder.jobType.length > 0) {
            this.supportActionBar!!.title = order_number + " (" + jobOrder.jobType + ")"
        } else {
            this.supportActionBar!!.title = order_number
        }
        this.supportActionBar!!.title = order_number
        btnAcceptJob!!.visibility = View.GONE
        btnReattemptJob!!.visibility = View.GONE
        if ((jobOrder.dropOffContact == null && jobOrder.contactNum == null) || (jobOrder.dropOffContact.isEmpty() && jobOrder.contactNum.isEmpty())) {
            mClickCallLayout_sim!!.visibility = View.GONE
            mClickCallLayout_whatsapp!!.visibility = View.GONE
        }
        totalItemOrderDetails!!.text =
            jobOrder.orderDetailsArrayList.size.toString() + " type of items"
        phNo_whatsapp!!.text = jobOrder.dropOffContact
        phNo_sim!!.text = jobOrder.dropOffContact
        if (jobOrder.itemTrackingNumber.equals("", ignoreCase = true)) {
            // trackingNumberOrderDetails.setText("Tracking number not available");
            trackingNumberOrderDetails!!.visibility = View.GONE
            trackingNumberLabel!!.visibility = View.GONE
        } else {
            trackingNumberOrderDetails!!.text = jobOrder.itemTrackingNumber
        }
        companyNameLabel!!.text = jobOrder.recipientCompanyName
        if (jobOrder.dropOffDesc.equals("", ignoreCase = true)) {
            descriptionOrderDetails!!.visibility = View.GONE
        } else {
            descriptionOrderDetails!!.text = jobOrder.dropOffDesc
        }
        if (jobOrder.driverNotes.equals("", ignoreCase = true)) {
            driverNotesLabel!!.text = "-"
        } else {
            driverNotesLabel!!.text = jobOrder.driverNotes
        }
        mLayoutJobStepsDetails(jobOrder)

        /*Get order status id based on order status name*/assignedOrderStatusIDFromDB =
            dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                Constants.ASSIGNED_TEXT
            )
        ackowledgedOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.ACKNOWLEDGED_TEXT
        )
        inprogressOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.INPROGRESS_TEXT
        )
        completedOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.COMPLETED_TEXT
        )
        failedOrderStatusIDFromDB =
            dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.FAILED_TEXT)
        cancelledOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.CANCELLED_TEXT
        )
        selfcollectOrderStatusIDFromDB = dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
            Constants.SELFCOLLECT_TEXT
        )
        if (jobOrder.orderStatusId == assignedOrderStatusIDFromDB) {
            btnAcceptJob!!.visibility = View.VISIBLE
            orderstatusdetail!!.text = resources.getText(R.string.assignedCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.assignedColor))
        } else if (jobOrder.orderStatusId == ackowledgedOrderStatusIDFromDB) {
            orderstatusdetail!!.text = resources.getText(R.string.acknowledgedCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.acknowledgedColor))
        } else if (jobOrder.orderStatusId == inprogressOrderStatusIDFromDB) {
            orderstatusdetail!!.text = resources.getText(R.string.inProgressCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.inProgressColor))
        } else if (jobOrder.orderStatusId == completedOrderStatusIDFromDB) {
            orderstatusdetail!!.text = resources.getText(R.string.completedCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.completedColor))
        } else if (jobOrder.orderStatusId == failedOrderStatusIDFromDB) {
            btnReattemptJob!!.visibility = View.VISIBLE
            orderstatusdetail!!.text = resources.getText(R.string.failedCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.failedColor))
        } else if (jobOrder.orderStatusId == cancelledOrderStatusIDFromDB) {
            orderstatusdetail!!.text = resources.getText(R.string.cancelledCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.cancelledColor))
        } else if (jobOrder.orderStatusId == selfcollectOrderStatusIDFromDB) {
            orderstatusdetail!!.text = resources.getText(R.string.selfCollectCapital)
            layoutJobStatus!!.setBackgroundColor(resources.getColor(R.color.selfCollectColor))
        }
    }

    /*
     * This is for aleart dialog.
     * */
    /* ================================ SWEET ALERT DIALOG =================================== */
    fun mLayoutJobStepsDetails(jobOrder: JobOrder?) {
        mLayoutJobSteps!!.removeAllViews()
        scanRequiredForAllItem = false
        noBil = 0
        try {
            Collections.sort(jobOrder!!.jobStepsArrayList, object : Comparator<JobSteps?> {
                override fun compare(lhs: JobSteps?, rhs: JobSteps?): Int {
                    if (lhs == null && rhs == null) return 0
                    if (lhs == null) return -1
                    if (rhs == null) return 1
                    val lowOrderSequence = lhs.order_sequence
                    val rightOrderSequence = rhs.order_sequence
                    if (lowOrderSequence > rightOrderSequence) {
                        return 1
                    } else return if (lowOrderSequence < rightOrderSequence) {
                        -1
                    } else 0

                    //return compare(lhs.getOrder_sequence(), rhs.getOrder_sequence());
                }
            })
        } catch (e: Throwable) {
            LogCustom.e(e)
        }

        //if ( !JavaOp.isArrayNullOrEmpty( jobOrder.getOrderDetails() ) )
        if (jobOrder!!.jobStepsArrayList.size > 0) {
            for (i in jobOrder.jobStepsArrayList.indices) {
                val jobSteps = jobOrder.jobStepsArrayList[i]
                //                if(jobSteps.getJob_step_pic_contact().isEmpty()){
//                    continue;
//                }
                if (JavaOp.ifNullThenLog(jobSteps)) continue
                noBil = noBil + 1
                if (jobSteps.is_scan_required) {
                    scanRequiredForAllItem = true
                }
                val view = View.inflate(this, R.layout.view_job_step_items, null)
                mLayoutJobSteps!!.addView(view)
                val layoutSteps = view.findViewById<RelativeLayout>(R.id.layoutSteps)
                val buttonLayout = view.findViewById<LinearLayout>(R.id.buttonLayout)
                val layoutNumber = view.findViewById<RelativeLayout>(R.id.layoutNumber)
                val topLine = view.findViewById<View>(R.id.topLine)
                val bottomLine = view.findViewById<View>(R.id.bottomLine)
                val stepNumber = view.findViewById<Button>(R.id.stepNumber)
                val stepsName = view.findViewById<TextView>(R.id.stepsName)
                val stepsAddress = view.findViewById<TextView>(R.id.stepsAddress)
                val btnFailedJobStep = view.findViewById<Button>(R.id.btnFailedJobStep)
                val btnStartOrCompletedJobStep =
                    view.findViewById<Button>(R.id.btnStartOrCompletedJobStep)
                val btnDetailedJobStep = view.findViewById<Button>(R.id.btnDetailsJobStep)
                btnFailedJobStep.tag = jobSteps
                btnStartOrCompletedJobStep.tag = jobSteps
                btnDetailedJobStep.tag = jobSteps
                btnDetailedJobStep.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(view: View) {
                        layoutSteps.performClick()
                    }
                })
                btnFailedJobStep.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(view: View) {
                        val step = view.tag as JobSteps
                        val dialog = SweetAlertDialog(
                            this@ManyJobStepsActivity,
                            SweetAlertDialog.PROGRESS_TYPE
                        )
                        dialog.show()
                        OkHttpRequest.fetchRejectReasons(
                            applicationContext, object : OKHttpNetwork {
                                override fun onSuccess(body: String, responseCode: Int) {
                                    try {
                                        dialog.dismissWithAnimation()
                                        val data = JSONObject(body)
                                        if (data.has("status") && !(data.isNull("status"))) {
                                            if (!data.getBoolean("status")) {
                                                showErrorDialog(
                                                    resources.getString(R.string.globalErrorMessageTitle),
                                                    resources.getString(
                                                        R.string.errorMessageUsernamePassword
                                                    )
                                                ).show()
                                            } else {
                                                val reasonList = ArrayList<String>()
                                                reasonList.add("SELECT")
                                                for (i in 0 until data.getJSONArray("result")
                                                    .length()) {
                                                    reasonList.add(
                                                        data.getJSONArray("result").getJSONObject(i)
                                                            .optString("reject_reason")
                                                    )
                                                }
                                                runOnUiThread(
                                                    { addReason(false, step, reasonList) }
                                                )
                                            }
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }

                                override fun onFailure(
                                    body: String,
                                    requestCallBackError: Boolean,
                                    responseCode: Int
                                ) {
                                    try {
                                        dialog.dismissWithAnimation()
                                        errorHandlingFromResponseCode(
                                            body,
                                            requestCallBackError,
                                            responseCode
                                        )
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                }
                            })
                    }
                })
                btnStartOrCompletedJobStep.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        val step = v.tag as JobSteps
                        val jobStepStatusId = step.job_step_status_id
                        val jobStepId = step.id
                        LogCustom.i("in2222s", ": $jobStepStatusId")
                        settings =
                            PreferenceManager.getDefaultSharedPreferences(this@ManyJobStepsActivity)
                        val isOnline = settings?.getBoolean("isOnline", true)
                        val isManPower = settings?.getBoolean("isManPower", false)
                        val generalActivity = GeneralActivity()
                        if (!generalActivity.haveInternetConnected(applicationContext)) {
                            Log.i("internetConnection", "no")
                            if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                                wantProceedIfNoInternet(3)
                            } else {
                                wantProceedIfNoInternet(4)
                            }
                        } else {
                            if (checkGPSBeforeUpdate()) {
                                if (isOnline == true) {
                                    var totalOrderItem = 0
                                    if (jobOrder.total_package == 0 || jobOrder.total_package == null) {
                                        if (jobOrder.orderDetailsArrayList.size > 0) {
                                            for (ii in jobOrder.orderDetailsArrayList.indices) {
                                                val orderDetails =
                                                    jobOrder.orderDetailsArrayList[ii]
                                                totalOrderItem =
                                                    totalOrderItem + orderDetails.quantity
                                            }
                                        }
                                    } else {
                                        totalOrderItem = jobOrder.total_package
                                    }
                                    if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                                        // start job
                                        LogCustom.i("step pending", "11")
                                        LogCustom.i("totalOrderItem", "" + totalOrderItem)
                                        if (scanRequiredForAllItem) {
                                            if (jobOrder.itemTrackingNumber.equals(
                                                    "",
                                                    ignoreCase = true
                                                ) || jobOrder.itemTrackingNumber == null
                                            ) {
                                                if (orderDetailId != 0 && jobStepId != 0) {
                                                    if (isManPower == true) {
                                                        checkingFunctionBasedOnStatus(
                                                            Constants.JOB_STEP_PENDING,
                                                            Constants.IN_PROGRESS,
                                                            false,
                                                            "is_status_need_manpower"
                                                        )
                                                        isManPowerCanUpdateProcess = true
                                                        btnStartOrCompletedJobStep.isEnabled = false
                                                        jsonStructure(
                                                            Constants.JOB_STEP_INPROGRESS,
                                                            step
                                                        )
                                                    } else {
                                                        btnStartOrCompletedJobStep.isEnabled = false
                                                        jsonStructure(
                                                            Constants.JOB_STEP_INPROGRESS,
                                                            step
                                                        )
                                                    }
                                                } else {
                                                    showErrorDialog(
                                                        resources.getString(R.string.orderDetailErrorTitle),
                                                        resources.getString(
                                                            R.string.orderDetailError
                                                        )
                                                    ).show()
                                                }
                                            } else {
                                                if (orderDetailId != 0 && jobStepId != 0) {
                                                    if (isManPower == true) {
                                                        isManPowerCanUpdateProcess = false
                                                        checkingFunctionBasedOnStatus(
                                                            Constants.JOB_STEP_PENDING,
                                                            Constants.IN_PROGRESS,
                                                            false,
                                                            "is_status_need_manpower"
                                                        )
                                                        if (isManPowerCanUpdateProcess) {
                                                            if (totalOrderItem > 0) {
                                                                //need scan
                                                                scanProcessAllItem(
                                                                    true,
                                                                    jobOrder.itemTrackingNumber,
                                                                    totalOrderItem
                                                                )
                                                            } else {
                                                                btnStartOrCompletedJobStep.isEnabled =
                                                                    false
                                                                jsonStructure(
                                                                    Constants.JOB_STEP_INPROGRESS,
                                                                    step
                                                                )
                                                            }
                                                        } else {
                                                            // cannot update
                                                            showAlertManPower()
                                                            //showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                                        }
                                                    } else {
                                                        if (totalOrderItem > 0 || scanRequiredForAllItem) {
                                                            //need scan
                                                            scanProcessAllItem(
                                                                true,
                                                                jobOrder.itemTrackingNumber,
                                                                totalOrderItem
                                                            )
                                                        } else {
                                                            btnStartOrCompletedJobStep.isEnabled =
                                                                false
                                                            jsonStructure(
                                                                Constants.JOB_STEP_INPROGRESS,
                                                                step
                                                            )
                                                        }
                                                    }
                                                } else {
                                                    showErrorDialog(
                                                        resources.getString(R.string.orderDetailErrorTitle),
                                                        resources.getString(
                                                            R.string.orderDetailError
                                                        )
                                                    ).show()
                                                }
                                            }
                                        } else {
                                            btnStartOrCompletedJobStep.isEnabled = false
                                            jsonStructure(Constants.JOB_STEP_INPROGRESS, step)
                                        }
                                    } else if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                                        // complete job
                                        if (isManPower == true) {
                                            isManPowerCanUpdateProcess = false
                                            checkingFunctionBasedOnStatus(
                                                Constants.JOB_STEP_INPROGRESS,
                                                Constants.COMPLETE,
                                                false,
                                                "is_status_need_manpower"
                                            )
                                            if (isManPowerCanUpdateProcess) {
                                                completeThisJobProcess(step, totalOrderItem)
                                            } else {
                                                // cannot update
                                                showAlertManPower()
                                                // showErrorDialog(getResources().getString(R.string.manPowerErrorTitle),getResources().getString(R.string.manPowerErrorMessage)).show();
                                            }
                                        } else {
                                            completeThisJobProcess(step, totalOrderItem)
                                        }
                                    }
                                } else {
                                    if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                                        showNeedOnlineDialog(
                                            resources.getString(R.string.offlineStartJobTitle),
                                            resources.getString(
                                                R.string.offlineMessage
                                            )
                                        ).show()
                                    } else {
                                        if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                                            showNeedOnlineDialog(
                                                resources.getString(R.string.offlineCompleteJobTitle),
                                                resources.getString(
                                                    R.string.offlineMessage
                                                )
                                            ).show()
                                        }
                                    }
                                    //cannot update because not online
                                }
                            } else {
                                if (jobStepStatusId == Constants.JOB_STEP_PENDING) {
                                    showNeedOnlineDialog(
                                        resources.getString(R.string.offlineStartJobTitle),
                                        resources.getString(
                                            R.string.offlineMessage
                                        )
                                    ).show()
                                } else {
                                    if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                                        showNeedOnlineDialog(
                                            resources.getString(R.string.offlineCompleteJobTitle),
                                            resources.getString(
                                                R.string.offlineMessage
                                            )
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                })
                if (jobOrder.orderStatusId == assignedOrderStatusIDFromDB) {
                    buttonLayout.visibility = View.GONE
                }


                // need check either it is last steps, if last steps, and pending, then need start job but if inprogress need to completed, other status must hide
                if (jobOrder.jobStepsArrayList.size > 0) {
                    // need check either it is last steps, if last steps, and pending, then need start job but if inprogress need to completed, other status must hide
                    if (jobSteps.order_sequence > 1) {
                        val ii = jobSteps.order_sequence - 1
                        if (jobOrder.jobStepsArrayList[ii - 1].job_step_status_id == Constants.JOB_STEP_COMPLETED || jobOrder.jobStepsArrayList[ii - 1].job_step_status_id == Constants.JOB_STEP_FAILED) {
                            if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                                //start job
                                startJobStepsButton = true
                                showCompletedFailedButton = true
                                stepCompleteBoolean = false
                                LogCustom.i("in44", "in44")
                            } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                                //completed job
                                startJobStepsButton = false
                                showCompletedFailedButton = true
                                stepCompleteBoolean = false
                                LogCustom.i("in55", "in55")
                            } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                                //completed job
                                startJobStepsButton = false
                                showCompletedFailedButton = false
                                stepCompleteBoolean = true
                                LogCustom.i("in55", "in55")
                            } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                                //completed job
                                startJobStepsButton = false
                                showCompletedFailedButton = false
                                stepCompleteBoolean = true
                                LogCustom.i("in55", "in55")
                            } else {
                                //hide
                                startJobStepsButton = false
                                showCompletedFailedButton = false
                                stepCompleteBoolean = false
                                LogCustom.i("in66", "in66")
                            }
                        } else {
                            //hide because previous step not equalt to completed
                            startJobStepsButton = false
                            showCompletedFailedButton = false
                            stepCompleteBoolean = false
                            LogCustom.i("in77", "in77")
                        }
                    } else if (jobSteps.order_sequence == 1) {
                        // not need check previous step
                        if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                            //start job
                            startJobStepsButton = true
                            showCompletedFailedButton = true
                            stepCompleteBoolean = false
                            LogCustom.i("in88", "in88")
                        } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                            //completed job
                            startJobStepsButton = false
                            showCompletedFailedButton = true
                            stepCompleteBoolean = false
                            LogCustom.i("in99", "in99")
                        } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                            //completed job
                            startJobStepsButton = false
                            showCompletedFailedButton = false
                            stepCompleteBoolean = true
                            LogCustom.i("in99", "in99")
                        } else {
                            //hide
                            startJobStepsButton = false
                            showCompletedFailedButton = false
                            stepCompleteBoolean = false
                            LogCustom.i("in10", "in10")
                        }
                    }
                }
                if (!showCompletedFailedButton && !startJobStepsButton) {
                    if (stepCompleteBoolean) {
                        btnStartOrCompletedJobStep.visibility = View.INVISIBLE
                        btnFailedJobStep.visibility = View.GONE
                        btnDetailedJobStep.visibility = View.VISIBLE
                    }
                    btnStartOrCompletedJobStep.setBackgroundResource(R.drawable.ripple_effect_grey)
                    btnFailedJobStep.setBackgroundResource(R.drawable.ripple_effect_grey)
                    btnStartOrCompletedJobStep.isEnabled = false
                    btnFailedJobStep.isEnabled = false
                } else {
                    btnFailedJobStep.visibility = View.VISIBLE
                    btnStartOrCompletedJobStep.visibility = View.VISIBLE
                    btnDetailedJobStep.visibility = View.GONE
                    if (showCompletedFailedButton) {
                        if (startJobStepsButton) {
                            btnStartOrCompletedJobStep.text = "Start this Job Step"
                        } else {
                            // complete button
                            btnStartOrCompletedJobStep.text = "Complete this Job Step"
                        }
                        btnStartOrCompletedJobStep.setBackgroundResource(R.drawable.ripple_effect_green)
                        btnFailedJobStep.setBackgroundResource(R.drawable.ripple_effect_red)
                        btnStartOrCompletedJobStep.isEnabled = true
                        btnFailedJobStep.isEnabled = true
                    } else {
                        btnStartOrCompletedJobStep.setBackgroundResource(R.drawable.ripple_effect_grey)
                        btnFailedJobStep.setBackgroundResource(R.drawable.ripple_effect_grey)
                        btnStartOrCompletedJobStep.isEnabled = false
                        btnFailedJobStep.isEnabled = false
                    }
                }
                if (noBil != jobOrder.jobStepsArrayList.size) {
                    val nextJobSteps = jobOrder.jobStepsArrayList[i + 1]
                    if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                        bottomLine.setBackgroundColor(resources.getColor(R.color.pendingJobStepColor))
                        stepNumber.setTextColor(resources.getColor(R.color.black))
                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                        bottomLine.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColor))
                        stepNumber.setTextColor(resources.getColor(R.color.white_pure))
                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                        bottomLine.setBackgroundColor(resources.getColor(R.color.completedJobStepColor))
                        stepNumber.setTextColor(resources.getColor(R.color.white_pure))
                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_CANCELLED) {
                        stepNumber.setTextColor(resources.getColor(R.color.white_pure))
                        bottomLine.setBackgroundColor(resources.getColor(R.color.cancelledJobStepColor))
                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                        stepNumber.setTextColor(resources.getColor(R.color.white_pure))
                        bottomLine.setBackgroundColor(resources.getColor(R.color.failedJobStepColor))
                    }
                }
                if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                    //// pending
                    topLine.setBackgroundColor(resources.getColor(R.color.pendingJobStepColor))
                    stepNumber.setBackgroundResource(R.drawable.pending_steps_circle_view)
                    //  bottomLine.setBackgroundColor(getResources().getColor(R.color.pendingJobStepColor));

                    //set button layout
                    btnStartOrCompletedJobStep.text = "Start this Job Step"
                } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {

                    //set button layout
                    btnStartOrCompletedJobStep.text = "Complete this Job"
                    btnStartOrCompletedJobStep.visibility = View.VISIBLE
                    btnFailedJobStep.visibility = View.VISIBLE
                    btnDetailedJobStep.visibility = View.GONE


                    //// in progrss
                    topLine.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColor))
                    stepNumber.setBackgroundResource(R.drawable.inprogress_steps_circle_view)
                    //  bottomLine.setBackgroundColor(getResources().getColor(R.color.inProgressJobStepColor));
                    handler = Handler()
                    runnable = object : Runnable {
                        override fun run() {
                            indexTotal = indexTotal + 1
                            LogCustom.i("indexTotal", "" + indexTotal)
                            if ((indexTotal % 2) == 0) {
                                topLine.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColor))
                                stepNumber.setBackgroundResource(R.drawable.inprogress_steps_circle_view)
                                // bottomLine.setBackgroundColor(getResources().getColor(R.color.inProgressJobStepColor));
                                if (noBil != jobOrder.jobStepsArrayList.size) {
                                    val nextJobSteps = jobOrder.jobStepsArrayList[noBil]
                                    if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.pendingJobStepColor))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColor))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.completedJobStepColor))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_CANCELLED) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.cancelledJobStepColor))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.failedJobStepColor))
                                    }
                                }
                            } else {
                                topLine.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColorSecond))
                                stepNumber.setBackgroundResource(R.drawable.inprogress_steps_circle_view_second_color)
                                //bottomLine.setBackgroundColor(getResources().getColor(R.color.inProgressJobStepColorSecond));
                                if (noBil != jobOrder.jobStepsArrayList.size) {
                                    val nextJobSteps = jobOrder.jobStepsArrayList[noBil]
                                    if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.pendingJobStepColorSecond))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.inProgressJobStepColorSecond))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.completedJobStepColorSecond))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_CANCELLED) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.cancelledJobStepColorSecond))
                                    } else if (nextJobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                                        bottomLine.setBackgroundColor(resources.getColor(R.color.failedJobStepColorSecond))
                                    }
                                }
                            }
                            stepNumber.invalidate()
                            handler!!.postDelayed(this, 1000)
                        }
                    }
                    handler!!.postDelayed(runnable!!, 1000)
                } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                    //// completed
                    topLine.setBackgroundColor(resources.getColor(R.color.completedJobStepColor))
                    stepNumber.setBackgroundResource(R.drawable.completed_steps_circle_view)
                    //bottomLine.setBackgroundColor(getResources().getColor(R.color.completedJobStepColor));
                } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_CANCELLED) {
                    //// cancelled
                    topLine.setBackgroundColor(resources.getColor(R.color.cancelledJobStepColor))
                    stepNumber.setBackgroundResource(R.drawable.cancelled_steps_circle_view)
                    //  bottomLine.setBackgroundColor(getResources().getColor(R.color.cancelledJobStepColor));
                } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_FAILED) {
                    //// failed
                    topLine.setBackgroundColor(resources.getColor(R.color.failedJobStepColor))
                    stepNumber.setBackgroundResource(R.drawable.failed_step_circle_view)
                    //  bottomLine.setBackgroundColor(getResources().getColor(R.color.failedJobStepColor));
                }


                /*Check is it last step*/if (noBil == jobOrder.jobStepsArrayList.size) {
                    bottomLine.visibility = View.GONE
                    layoutNumber.setPadding(0, 0, 0, 15)
                }
                stepNumber.text = "" + noBil
                stepsName.text = jobSteps.job_step_name
                if (jobSteps.location.equals("", ignoreCase = true)) {
                    stepsAddress.text = " - "
                } else {
                    stepsAddress.text = jobSteps.location
                }
                layoutSteps.setOnClickListener(object : View.OnClickListener {
                    override fun onClick(v: View) {
                        var showCompletedFailedButton = true
                        var startJobStepsButton = true
                        var cannotStartActivity = false
                        val i = Intent(applicationContext, OneJobStepActivity::class.java)
                        Log.i("scan required", "scan required:$scanRequiredForAllItem")
                        var isLastJobStep = false
                        i.putExtra("jobStepId", jobSteps.id)
                        i.putExtra("jobStepName", jobSteps.job_step_name)
                        i.putExtra("OrderId", orderDetailId)
                        i.putExtra("jobStepStatusId", jobSteps.job_step_status_id)
                        i.putExtra("trackingNumber", jobOrder.itemTrackingNumber)
                        i.putExtra("companyName", jobOrder.companyName)
                        i.putExtra("OrderStatusId", jobOrder.orderStatusId)
                        i.putExtra("scanRequiredForAllItem", scanRequiredForAllItem)
                        LogCustom.i("job", "jobStepStatusId" + jobSteps.job_step_status_id)
                        LogCustom.i("jobName", "getCompanyName" + jobOrder.companyName)
                        if (jobOrder.total_package == 0 || jobOrder.total_package == null) {
                            var totalOrderItem = 0
                            if (jobOrder.orderDetailsArrayList.size > 0) {
                                for (ii in jobOrder.orderDetailsArrayList.indices) {
                                    val orderDetails = jobOrder.orderDetailsArrayList[ii]
                                    totalOrderItem = totalOrderItem + orderDetails.quantity
                                }
                            }
                            i.putExtra("totalOrderItem", totalOrderItem)
                            LogCustom.i("totalOrderItemMany", ": $totalOrderItem")
                        } else {
                            i.putExtra("totalOrderItem", jobOrder.total_package)
                            LogCustom.i("totalOrderItemPackage", ": " + jobOrder.total_package)
                        }
                        if (jobOrder.jobStepsArrayList.size == 1) {
                            if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                                //start job
                                startJobStepsButton = true
                                showCompletedFailedButton = true
                                LogCustom.i("in11", "in11")
                            } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                                //completed job
                                startJobStepsButton = false
                                showCompletedFailedButton = true
                                LogCustom.i("in22", "in22")
                            } else {
                                //hide
                                startJobStepsButton = false
                                showCompletedFailedButton = false
                                LogCustom.i("in33", "in33")
                            }
                            isLastJobStep = true
                        } else if (jobOrder.jobStepsArrayList.size > 0) {
                            // need check either it is last steps, if last steps, and pending, then need start job but if inprogress need to completed, other status must hide
                            if (jobSteps.order_sequence == jobOrder.jobStepsArrayList.size) {
                                isLastJobStep = true
                            }
                            if (jobSteps.order_sequence > 1) {
                                LogCustom.i("in3333", "in3333" + jobSteps.order_sequence)
                                val ii = jobSteps.order_sequence - 1
                                LogCustom.i("in3333", "in333333$ii")
                                if (jobOrder.jobStepsArrayList[ii - 1].job_step_status_id == Constants.JOB_STEP_COMPLETED) {
                                    if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                                        //start job
                                        startJobStepsButton = true
                                        showCompletedFailedButton = true
                                        LogCustom.i("in44", "in44")
                                    } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                                        //completed job
                                        startJobStepsButton = false
                                        showCompletedFailedButton = true
                                        LogCustom.i("in55", "in55")
                                    } else {
                                        //hide
                                        startJobStepsButton = false
                                        showCompletedFailedButton = false
                                        LogCustom.i("in66", "in66")
                                    }
                                } else {
                                    //hide because previous step not equalt to completed
                                    startJobStepsButton = false
                                    showCompletedFailedButton = false
                                    LogCustom.i("in77", "in77")
                                }
                            } else if (jobSteps.order_sequence == 1) {
                                // not need check previous step
                                if (jobSteps.job_step_status_id == Constants.JOB_STEP_PENDING) {
                                    //start job
                                    startJobStepsButton = true
                                    showCompletedFailedButton = true
                                    LogCustom.i("in88", "in88")
                                } else if (jobSteps.job_step_status_id == Constants.JOB_STEP_INPROGRESS) {
                                    //completed job
                                    startJobStepsButton = false
                                    showCompletedFailedButton = true
                                    LogCustom.i("in99", "in99")
                                } else {
                                    //hide
                                    startJobStepsButton = false
                                    showCompletedFailedButton = false
                                    LogCustom.i("in10", "in10")
                                }
                            }
                        } else if (jobOrder.jobStepsArrayList.size < 1) {
                            // never happen because at least job step must be one
                            cannotStartActivity = true
                        }
                        if ((jobOrder.orderStatusId == assignedOrderStatusIDFromDB) || (jobOrder.orderStatusId == ackowledgedOrderStatusIDFromDB) || (jobOrder.orderStatusId == inprogressOrderStatusIDFromDB)) {
                        } else {
                            showCompletedFailedButton = false
                        }
                        i.putExtra("startJobStepsButton", startJobStepsButton)
                        i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
                        i.putExtra("isLastJobStep", isLastJobStep)
                        i.putExtra("contact", jobOrder.dropOffContact)
                        i.putExtra("isSingleJobStep", jobOrder.jobStepsArrayList.size == 1)
                        if (!cannotStartActivity) {
                            if (handler != null) {
                                handler!!.removeCallbacks((runnable)!!)
                            }
                            startActivityForResult(i, Constants.INTENT_ACTIVITY_ONE_JOB_STEPS)
                        } else {
                            LogCustom.i("cannot start", "cannot start")
                            // cannot start
                        }
                    }
                })
            }
        } else {
            mLayoutJobSteps!!.visibility = View.GONE
        }
        //Update UI (order attempt layout)
    }

    /* ================================ ONLINE / OFFLINE LOCATION SETTINGS =================================== */
    fun checkingDriverStatusAndLocationStatus() {
        //if status online, location must on. else must be offline
        val isOnline = settings!!.getBoolean("isOnline", true)
        if (isOnline) {
            val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true")
                val editors = settings!!.edit()
                editors.putBoolean("isOnline", true).apply()
                startLocationServices()
            } else {
                // GPS not open
                LogCustom.i("GPS Not Enabled in your device true")
                val editors = settings!!.edit()
                editors.putBoolean("isOnline", false).apply()
                stopLocationServices()
            }
        } else {
            val editors = settings!!.edit()
            editors.putBoolean("isOnline", false).apply()
            stopLocationServices()
        }
    }

    fun startLocationServices() {
        val generalActivity = GeneralActivity()
        if (!generalActivity.isMyServiceLocationRunning(applicationContext)) {
            LogCustom.i("isMyServiceRunning need to start", "true")
            val intent = Intent(this@ManyJobStepsActivity, ForegroundLocationService::class.java)
            startService(intent)
        } else {
            LogCustom.i("isMyServiceRunning already, not need to start", "true")
        }
        updateStatusOnlineOffline(true)
    }

    fun checkingDriverStatusAndLocationStatusToggle() {
        //if status online, location must on. else must be offline
        val isOnline = settings!!.getBoolean("isOnline", true)
        if (isOnline) {
            val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LogCustom.i("GPS is Enabled in your device true")
                val editors = settings!!.edit()
                editors.putBoolean("isOnline", true).apply()
                val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(ii)
                startLocationServices()
            } else {
                showGPSDisabledAlertToUser()
            }
        } else {
            val editors = settings!!.edit()
            editors.putBoolean("isOnline", false).apply()
            stopLocationServices()
        }
    }

    fun updateStatusOnlineOffline(is_online: Boolean) {
        try {
            val jsonObject = JSONObject()
            LogCustom.i("updateStatusOnlineOffline", "is_online : $is_online")
            jsonObject.put("is_online", is_online)
            jsonObject.put("latitude", "")
            jsonObject.put("longitude", "")
            val okHttpRequest = OkHttpRequest()
            try {
                OkHttpRequest.driverUpdateProfile(
                    applicationContext, jsonObject.toString(), object : OKHttpNetwork {
                        override fun onSuccess(body: String, responseCode: Int) {
                            runOnUiThread(object : Runnable {
                                override fun run() {
                                    if (body == null || body.isEmpty()) {
                                        LogCustom.i("valueIsNull", "yes")
                                    } else {
                                        LogCustom.i("update profile is success", "yes")
                                    }
                                }
                            })
                        }

                        override fun onFailure(
                            body: String,
                            requestCallBackError: Boolean,
                            responseCode: Int
                        ) {
                            runOnUiThread(object : Runnable {
                                override fun run() {
                                    errorHandlingFromResponseCode(
                                        body,
                                        requestCallBackError,
                                        responseCode
                                    )
                                }
                            })
                        }
                    })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } catch (e: Exception) {
        }
    }

    fun stopLocationServices() {
        val generalActivity = GeneralActivity()
        if (generalActivity.isMyServiceLocationRunning(applicationContext)) {
            LogCustom.i("isMyServiceRunning need to stop", "true")
            val intent = Intent(this@ManyJobStepsActivity, ForegroundLocationService::class.java)
            stopService(intent)
        } else {
            LogCustom.i("isMyServiceRunning not need to stop", "true")
        }
        updateStatusOnlineOffline(false)
    }

    private fun showGPSDisabledAlertToUser() {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
            .setCancelable(false)
            .setPositiveButton("Settings",
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, id: Int) {
                        val callGPSSettingIntent = Intent(
                            Settings.ACTION_LOCATION_SOURCE_SETTINGS
                        )
                        startActivityForResult(
                            callGPSSettingIntent,
                            Constants.INTENT_ACTIVITY_GPS_SETTINGS
                        )
                    }
                })
        alertDialogBuilder.setNegativeButton("Cancel",
            object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, id: Int) {
                    dialog.cancel()
                    val editors = settings!!.edit()
                    editors.putBoolean("isOnline", false).apply()
                    stopLocationServices()
                }
            })
        val alert = alertDialogBuilder.create()
        alert.show()
    }

    fun showNeedOnlineDialog(titleMessage: String?, contentMessage: String?): SweetAlertDialog {
        val sweetAlertDialog = getNeedOnlineDialog(titleMessage, contentMessage)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    /* ================================ ONLINE / OFFLINE LOCATION SETTINGS =================================== */
    fun getNeedOnlineDialog(titleMessage: String?, contentMessage: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setContentText(contentMessage)
            .setTitleText(titleMessage)
            .setConfirmText("Yes")
            .setConfirmClickListener(object : OnSweetClickListener {
                override fun onClick(sDialog: SweetAlertDialog) {
                    val editors = settings!!.edit()
                    editors.putBoolean("isOnline", true).apply()
                    checkingDriverStatusAndLocationStatusToggle()
                    sDialog.dismissWithAnimation()
                }
            })
            .setCancelText("No")
            .setCancelClickListener(object : OnSweetClickListener {
                override fun onClick(sDialog: SweetAlertDialog) {
                    sDialog.dismissWithAnimation()
                }
            })
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    fun showErrorInternetDialog(): SweetAlertDialog {
        val sweetAlertDialog = errorInternetDialog
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    val errorInternetDialog: SweetAlertDialog
        get() {
            val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(object : OnSweetClickListener {
                    override fun onClick(sDialog: SweetAlertDialog) {
                        val intent = Intent(Intent.ACTION_MAIN, null)
                        intent.addCategory(Intent.CATEGORY_LAUNCHER)
                        val cn = ComponentName(
                            "com.android.settings",
                            "com.android.settings.wifi.WifiSettings"
                        )
                        intent.component = cn
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(object : OnSweetClickListener {
                    override fun onClick(sDialog: SweetAlertDialog) {
                        sDialog.dismissWithAnimation()
                    }
                })
            sweetAlertDialog.setCancelable(false)
            return sweetAlertDialog
        }

    fun showErrorDialog(titleError: String?, contextError: String?): SweetAlertDialog {
        val sweetAlertDialog = getErrorDialog(titleError, contextError)
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    fun getErrorDialog(titleError: String?, contextError: String?): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
            .setContentText(contextError)
            .setTitleText(titleError)
            .setConfirmText("OK")
            .setConfirmClickListener(object : OnSweetClickListener {
                override fun onClick(sDialog: SweetAlertDialog) {
                    sDialog.dismissWithAnimation()
                    if (unauthorizedNeedToLoginPage) {
                        val generalActivity = GeneralActivity()
                        if (generalActivity.isMyServiceLocationRunning(applicationContext)) {
                            LogCustom.i("isMyServiceRunning", "true")
                            val intent =
                                Intent(applicationContext, ForegroundLocationService::class.java)
                            stopService(intent)
                        }
                        settings!!.edit().remove("isOnline").apply()
                        settings!!.edit().remove("googleToken").apply()
                        settings!!.edit().putString("user_name", "").apply()
                        unauthorizedNeedToLoginPage = false
                        val editors = settings!!.edit()
                        editors.putBoolean("insideJobDetails", false).apply()
                        editors.apply()
                        val i = Intent(applicationContext, LoginActivity::class.java)
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        i.putExtra("EXIT", true)
                        startActivity(i)
                    }
                }
            })
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    fun refreshJobDialog(
        titleError: String?,
        contextError: String?,
        buttonText: String?,
        isJobRemoved: Boolean,
        job_destroyed: Boolean,
        job_attributes_updated: JSONArray?,
        icon: Drawable?
    ): SweetAlertDialog {
        val sweetAlertDialog = getRefreshJobDialog(
            titleError,
            contextError,
            buttonText,
            isJobRemoved,
            job_destroyed,
            job_attributes_updated,
            icon
        )
        sweetAlertDialog.show()
        return sweetAlertDialog
    }

    /* ================================ SWEET ALERT DIALOG =================================== */ /*========= ACTION AFTER GO ANOTHER PAGE =========== */ // After user click back then should reload again this page with new data ..
    fun getRefreshJobDialog(
        titleError: String?,
        contextError: String?,
        buttonText: String?,
        isJobRemoved: Boolean,
        job_destroyed: Boolean,
        job_attributes_updated: JSONArray?,
        icon: Drawable?
    ): SweetAlertDialog {
        val sweetAlertDialog = SweetAlertDialog(this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
            .setCustomImage(icon)
            .setContentText(contextError)
            .setTitleText(titleError)
            .setConfirmText(buttonText)
            .setConfirmClickListener(object : OnSweetClickListener {
                override fun onClick(sDialog: SweetAlertDialog) {
                    sDialog.dismissWithAnimation()
                    if (isJobRemoved || job_destroyed) {
                        onBackPressed()
                    } else if (job_attributes_updated!!.length() > 0) {
                        callGetJobStepDetails()
                    }
                }
            })
        sweetAlertDialog.setCancelable(false)
        return sweetAlertDialog
    }

    /*========= ACTION AFTER GO ANOTHER PAGE =========== */
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("onActivityResult", "on activity result:$requestCode")
        LogCustom.i("resultCode", "" + resultCode)
        when (requestCode) {
            Constants.INTENT_ACTIVITY_ONE_JOB_STEPS -> {
                if (data == null) {
                    return
                }
                orderDetailId = data.extras!!.getInt("OrderId")
                LogCustom.i("onActivity", "Result: $orderDetailId")
                val editors = settings!!.edit()
                editors.putBoolean("insideJobDetails", true).apply()
                editors.apply()
                try {
                    val generalActivity = GeneralActivity()
                    if (!generalActivity.haveInternetConnected(applicationContext)) {
                        Log.i("internetConnection", "no")
                        //showErrorInternetDialog().show();
                        wantProceedIfNoInternet(3)
                    } else {
                        if (orderDetailId != 0) {
                            jobStepDetails
                        } else {
                            showErrorDialog(
                                resources.getString(R.string.orderDetailErrorTitle),
                                resources.getString(
                                    R.string.orderDetailError
                                )
                            ).show()
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            Constants.INTENT_ACTIVITY_GPS_SETTINGS -> {
                checkingDriverStatusAndLocationStatus()
                val ii = Intent(ACTION_TOGGLE_ONLINE_OFFLINE)
                LocalBroadcastManager.getInstance(applicationContext)
                    .sendBroadcast(ii)
            }
        }
        if (resultCode != RESULT_OK) return
        when (requestCode) {
            AndroidOp.REQUEST_CODE_CAMERA -> {
                Log.i("requset", "request code camera")
                resultFromGallery = false
                if (imageDialog == null) {
                } else {
                    if (imageDialog!!.isShowing) {
                        imageDialog!!.dismiss()
                    }
                }
                addDescription(data, resultFromGallery)
            }
            AndroidOp.REQUEST_CODE_GALLERY -> {
                Log.i("", "request code gallery")
                resultFromGallery = true
                if (imageDialog == null) {
                } else {
                    if (imageDialog!!.isShowing) {
                        imageDialog!!.dismiss()
                    }
                }
                addDescription(data, resultFromGallery)
            }
            80 -> {}
        }
    }

    override fun onResume() {
        super.onResume()
        if (alert != null) if (alert!!.isShowing) {
            alert!!.dismiss()
        }
        try {
            LogCustom.i("onResume", "yes")
            val generalActivity = GeneralActivity()
            if (!generalActivity.haveInternetConnected(applicationContext)) {
                Log.i("internetConnection", "no")
                //showErrorInternetDialog().show();
                wantProceedIfNoInternet(3)
            } else {
                LogCustom.i("orderDetailId", ":$orderDetailId")
                if (orderDetailId != 0) {
                    jobStepDetails
                } else {
                    showErrorDialog(
                        resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                            R.string.orderDetailError
                        )
                    ).show()
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }//Convert this bimtapImage to byte array

    // orderAttemptImage.setBase64(photoBase64);
    //jobStepId = 183;
    @get:Throws(IOException::class)
    val jobStepDetails: Unit
        get() {

            //jobStepId = 183;
            contextProgressDialog!!.text = resources.getString(R.string.loadingupdateJobDetails)
            pdProcess!!.show()
            val okHttpRequest = OkHttpRequest()
            try {
                OkHttpRequest.getJobOrderDetails(
                    applicationContext, orderDetailId, object : OKHttpNetwork {
                        override fun onSuccess(body: String, responseCode: Int) {
                            runOnUiThread(object : Runnable {
                                override fun run() {
                                    if (pdProcess != null) {
                                        if (pdProcess!!.isShowing) {
                                            pdProcess!!.dismiss()
                                        }
                                    }
                                    if (body == null || body.isEmpty()) {
                                    } else {
                                        try {
                                            val data = JSONObject(body)
                                            LogCustom.i("dataaa", data.toString())
                                            jobOrder = JobOrder(data)
                                            LogCustom.i(
                                                "dataaaaaa",
                                                "" + jobOrder!!.itemTrackingNumber
                                            )
                                            displayData(jobOrder)
                                            if (jobOrder!!.jobStepsArrayList.size > 0) {
                                                for (ii in jobOrder!!.jobStepsArrayList.indices) {
                                                    val jobSteps = jobOrder!!.jobStepsArrayList[ii]
                                                    if (jobSteps.getmOrderAttemptsList().size > 0) {
                                                        for (j in jobSteps.getmOrderAttemptsList().indices) {
                                                            val orderAttempt =
                                                                jobSteps.getmOrderAttemptsList()[j]
                                                            orderAttempt.orderId =
                                                                jobOrder!!.orderId
                                                            orderAttempt.isAttemptUpdateWithNoInternet =
                                                                false
                                                            orderAttempt.jobStepID = jobSteps.id
                                                            dbUpdateJobs!!.addOrderAttemptDetails(
                                                                orderAttempt
                                                            )
                                                            if (orderAttempt.getmOrderAttemptImages().size > 0) {
                                                                for (k in orderAttempt.getmOrderAttemptImages().indices) {
                                                                    val orderAttemptImage =
                                                                        orderAttempt.getmOrderAttemptImages()[k]
                                                                    if (orderAttemptImage.url != null) {
                                                                        runOnUiThread {
                                                                            Glide.with(this@ManyJobStepsActivity)
                                                                                .load(
                                                                                    orderAttemptImage.url
                                                                                )
                                                                                .asBitmap()
                                                                                .into(object :
                                                                                    SimpleTarget<Bitmap?>() {
                                                                                    override fun onResourceReady(
                                                                                        resource: Bitmap?,
                                                                                        glideAnimation: GlideAnimation<in Bitmap?>
                                                                                    ) {
                                                                                        imageBitmap =
                                                                                            resource
                                                                                        //Convert this bimtapImage to byte array
                                                                                        val photoBase64 =
                                                                                            ImageOp.convert(
                                                                                                imageBitmap,
                                                                                                Bitmap.CompressFormat.JPEG,
                                                                                                80
                                                                                            )
                                                                                        // orderAttemptImage.setBase64(photoBase64);
                                                                                        orderAttemptImage.orderId =
                                                                                            jobOrder!!.orderId
                                                                                        orderAttemptImage.id =
                                                                                            orderAttemptImage.id
                                                                                        orderAttemptImage.note =
                                                                                            orderAttemptImage.note
                                                                                        orderAttemptImage.isSignature =
                                                                                            orderAttemptImage.isSignature
                                                                                        orderAttemptImage.isRemove =
                                                                                            false
                                                                                        orderAttemptImage.orderAttemptID =
                                                                                            orderAttempt.id
                                                                                        orderAttemptImage.isImageUpdateNoInternet =
                                                                                            false
                                                                                        orderAttemptImage.url =
                                                                                            orderAttemptImage.url
                                                                                        dbUpdateJobs!!.addImageDetails(
                                                                                            orderAttemptImage
                                                                                        )
                                                                                    }

                                                                                    override fun onLoadFailed(
                                                                                        e: Exception,
                                                                                        errorDrawable: Drawable
                                                                                    ) {
                                                                                        super.onLoadFailed(
                                                                                            e,
                                                                                            errorDrawable
                                                                                        )
                                                                                    }
                                                                                })
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } catch (e: JSONException) {
                                            LogCustom.e(e)
                                            try {
                                            } catch (t: Throwable) {
                                            }
                                        }
                                    }
                                }
                            })
                        }

                        override fun onFailure(
                            body: String,
                            requestCallBackError: Boolean,
                            responseCode: Int
                        ) {
                            val jsonResponse = body
                            runOnUiThread(object : Runnable {
                                override fun run() {
                                    if (pdProcess != null) {
                                        if (pdProcess!!.isShowing) {
                                            pdProcess!!.dismiss()
                                        }
                                    }
                                    errorHandlingFromResponseCode(
                                        body,
                                        requestCallBackError,
                                        responseCode
                                    )
                                }
                            })
                        }
                    })
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        menuInflater.inflate(R.menu.job_many_steps, menu)
        //  return true;
        val menuItemAdd = menu.findItem(R.id.menuItemAdd)
        menuItemAdd.actionView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                onOptionsItemSelected(menuItemAdd)
            }
        })
        return super.onCreateOptionsMenu(menu)

        //return true;
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {

        /*Get order status id based on order status name*/


//        if(showAddOrderItemIcon){
//         //   menu.findItem(R.id.menuItemAdd).setVisible(true);
//            LogCustom.i("in111","in111");
//        }else{
//         //   menu.findItem(R.id.menuItemAdd).setVisible(false);
//            LogCustom.i("in222","in222");
//        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> {
                if (handler != null) {
                    handler!!.removeCallbacks((runnable)!!)
                }
                onBackPressed()
                return true
            }
            R.id.menuItemAdd -> return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Your GPS seems to be disabled, please turn it on to update job status.")
            .setCancelable(false)
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, id: Int) {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            })
            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface, id: Int) {
                    dialog.cancel()
                }
            })
        val alert = builder.create()
        alert.show()
    }

    fun jsonStructure(nextJobStepStatusId: Int, jobStep: JobSteps) {
        try {
            val rootObject = JSONObject()
            val childArray = JSONArray()

            // put array of id in object ID
            val childObject = JSONObject()
            val idArray = JSONArray()
            idArray.put(orderDetailId.toString())
            childObject.put("id", idArray)
            var isLastJobStep = false
            if (jobStep.order_sequence == jobOrder!!.jobStepsArrayList.size) {
                isLastJobStep = true
            }
            if (isLastJobStep) {
                if (nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.COMPLETED_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.INPROGRESS_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_FAILED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.FAILED_TEXT
                        )
                    )
                }
            } else {
                if (nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.INPROGRESS_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.INPROGRESS_TEXT
                        )
                    )
                } else if (nextJobStepStatusId == Constants.JOB_STEP_FAILED) {
                    childObject.put(
                        "order_status_id", dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                            Constants.FAILED_TEXT
                        )
                    )
                }
            }
            val jobStepDetailsObject: JSONObject
            val orderDetailArray = JSONArray()
            jobStepDetailsObject = JSONObject()
            val orderAttempt = OrderAttempt()
            val locale = Locale("en", "US")
            val printFormat = SimpleDateFormat("hh:mm a", locale)
            if (nextJobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                val dropOffTime = printFormat.format(Date())
                LogCustom.i("time", "currentTime$dropOffTime")
                childObject.put("drop_off_time", dropOffTime)

                //  jobStepDetailsObject.put("job_step_id", jobSteps.getId());
                jobStepDetailsObject.put("job_step_id", jobStep.id)
                jobStepDetailsObject.put("job_step_status_id", nextJobStepStatusId)
                orderDetailArray.put(jobStepDetailsObject)
                childObject.put("job_steps", orderDetailArray)
                childArray.put(childObject)
                rootObject.put("data", childArray)
                LogCustom.i("JsonObjectStructure", rootObject.toString(4))
                updateBatchOrder(rootObject, jobStep)
            } else if (nextJobStepStatusId == Constants.JOB_STEP_FAILED || nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                val location = lastLocation
                //  LogCustom.i("location", "latitude" + location.getLatitude());
                if (location == null) {
                    buildAlertMessageNoGps()
                } else {
                    val dropOffTime = printFormat.format(Date())
                    LogCustom.i("time", "currentTime$dropOffTime")
                    childObject.put("drop_off_time_end", dropOffTime)
                    if (nextJobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                        orderAttempt.received_by = "-"
                    }
                    orderAttempt.setLatitude(location.latitude)
                    orderAttempt.setLongitude(location.longitude)
                    if (nextJobStepStatusId == Constants.JOB_STEP_FAILED) {
                        orderAttempt.reason = reasonFailed
                        orderAttempt.setmOrderAttemptImages(orderAttemptImagesForFailed)
                    }
                    val orderAttemptArray = JSONArray()
                    val DATEFORMAT = "yyyy-MM-dd HH:mm:ss"
                    val sdf = SimpleDateFormat(DATEFORMAT)
                    sdf.timeZone = TimeZone.getTimeZone("UTC")
                    val utcTime = sdf.format(Date())
                    orderAttempt.submitted_time = utcTime
                    orderAttemptArray.put(orderAttempt.jsonObjectJobStepFailed)
                    jobStepDetailsObject.put("step_attempts", orderAttemptArray)


                    //  jobStepDetailsObject.put("job_step_id", jobSteps.getId());
                    jobStepDetailsObject.put("job_step_id", jobStep.id)
                    jobStepDetailsObject.put("job_step_status_id", nextJobStepStatusId)
                    orderDetailArray.put(jobStepDetailsObject)
                    childObject.put("job_steps", orderDetailArray)
                    childArray.put(childObject)
                    rootObject.put("data", childArray)
                    LogCustom.i("JsonObjectStructure", rootObject.toString(4))
                    updateBatchOrder(rootObject, jobStep)
                }
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @Throws(IOException::class)
    fun updateBatchOrder(jsonObject: JSONObject, jobStep: JobSteps) {

        //jobStepId = 183;
        contextProgressDialog!!.text = resources.getString(R.string.updateJobSteps)
        if (pdUpdateProcess != null) {
            if (pdUpdateProcess!!.isShowing) {
            } else {
                pdUpdateProcess!!.show()
            }
        }
        val okHttpRequest = OkHttpRequest()
        try {
            OkHttpRequest.batchUpdate(
                applicationContext, jsonObject.toString(), object : OKHttpNetwork {
                    override fun onSuccess(body: String, responseCode: Int) {
                        runOnUiThread(object : Runnable {
                            override fun run() {
                                if (pdUpdateProcess != null) {
                                    if (pdUpdateProcess!!.isShowing) {
                                        pdUpdateProcess!!.dismiss()
                                    }
                                }
                                if (body == null || body.isEmpty()) {
                                } else {
                                    try {
                                        var jobSteps = JobSteps()
                                        var jobStepStatusId = 0
                                        val data = JSONObject(body)
                                        val resultData = data.getJSONArray("result")
                                        LogCustom.i("dataaaBatchUpdate", data.toString())
                                        for (i in 0 until resultData.length()) {
                                            val jsonObject2 = resultData[i] as JSONObject
                                            val jobStepsArray =
                                                jsonObject2.getJSONArray("job_steps")
                                            for (j in 0 until jobStepsArray.length()) {
                                                val jsonObject3 = jobStepsArray[j] as JSONObject
                                                if (jsonObject3.getInt("id") == jobStep.id) {
                                                    jobSteps = JobSteps(jsonObject3)
                                                    jobStepStatusId = jobSteps.job_step_status_id
                                                }
                                            }
                                        }
                                        var isLastJobStep = false
                                        if (jobSteps.order_sequence == jobOrder!!.jobStepsArrayList.size) {
                                            isLastJobStep = true
                                        }
                                        LogCustom.i("dataaaStatusId", jobSteps.job_step_status_id)
                                        if (jobStepStatusId == Constants.JOB_STEP_INPROGRESS) {
                                            showSuccessDialog(
                                                resources.getString(R.string.jobStepStartedTitle),
                                                resources.getString(
                                                    R.string.jobStepStartedMessage
                                                )
                                            ).show()
                                        } else if (jobStepStatusId == Constants.JOB_STEP_COMPLETED) {
                                            if (isLastJobStep) {
                                                showSuccessDialog(
                                                    resources.getString(R.string.jobLastStepCompletedTitle),
                                                    resources.getString(
                                                        R.string.jobLastStepCompletedMessage
                                                    )
                                                ).show()
                                            } else {
                                                showSuccessDialog(
                                                    resources.getString(R.string.jobStepCompletedTitle),
                                                    resources.getString(
                                                        R.string.jobStepCompletedMessage
                                                    )
                                                ).show()
                                            }
                                        } else if (jobStepStatusId == Constants.JOB_STEP_FAILED) {
                                            showSuccessDialog(
                                                resources.getString(R.string.jobFailedTitle),
                                                resources.getString(
                                                    R.string.jobFailedMessage
                                                )
                                            ).show()
                                        }

//                                            setupDisplayData(jobSteps);
//                                            invalidateOptionsMenu();
                                        //finish();
                                    } catch (e: JSONException) {
                                        LogCustom.e(e)
                                        try {
                                        } catch (t: Throwable) {
                                        }
                                    }
                                }
                            }
                        })
                    }

                    override fun onFailure(
                        body: String,
                        requestCallBackError: Boolean,
                        responseCode: Int
                    ) {
                        val jsonResponse = body
                        runOnUiThread(object : Runnable {
                            override fun run() {
                                if (pdUpdateProcess != null) {
                                    if (pdUpdateProcess!!.isShowing) {
                                        pdUpdateProcess!!.dismiss()
                                    }
                                }
                                errorHandlingFromResponseCode(
                                    body,
                                    requestCallBackError,
                                    responseCode
                                )
                            }
                        })
                    }
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun checkAllInputInserted(): Boolean {
        if (slider!!.visibility == View.GONE && orderAttemptImagesForFailed.isEmpty()) {
            Toast.makeText(
                this@ManyJobStepsActivity,
                "Please add at least one image",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }
        //        if (!isItemSelected) {
//            Toast.makeText(ManyJobStepsActivity.this, "Please select a reason", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if (notes != null && notes.getText().toString().isEmpty()) {
//            Toast.makeText(ManyJobStepsActivity.this, "Please enter a note", Toast.LENGTH_SHORT).show();
//            return false;
//        }
        return true
    }

    fun addReason(
        isNoInternetConnection: Boolean,
        jobSteps: JobSteps,
        reasonList: ArrayList<String>?
    ) {
        val builder = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val alertLayout = inflater.inflate(R.layout.add_reason_layout, null)
        builder.setView(alertLayout)
        builder.setCancelable(false)
        val dialog = builder.create()
        val textViewTitle = alertLayout.findViewById<TextView>(R.id.textViewTitle)
        notes = alertLayout.findViewById(R.id.notes)
        val reasonSpinner = alertLayout.findViewById<Spinner>(R.id.reasonSpinner)
        reasonSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                isItemSelected = !(adapterView.selectedItem.toString() == "SELECT")
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, reasonList!!)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        reasonSpinner.adapter = adapter
        val btnCamera = alertLayout.findViewById<LinearLayout>(R.id.btnCamera)
        slider = alertLayout.findViewById(R.id.slider)
        val btnOk = alertLayout.findViewById<Button>(R.id.btnOk)
        val btnCancel = alertLayout.findViewById<Button>(R.id.btnCancel)
        btnCancel.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                dialog.dismiss()
            }
        })
        btnOk.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                if (!checkAllInputInserted()) {
                    return
                }
                if (isNoInternetConnection) {
                    dialog.dismiss()
                    val location = lastLocation
                    if (location == null) {
                        buildAlertMessageNoGps()
                    } else {
                        val locale = Locale("en", "US")
                        val printFormat = SimpleDateFormat("hh:mm a", locale)
                        val dropOffTime = printFormat.format(Date())
                        val jobOrder = JobOrder()
                        jobOrder.orderId = orderDetailId
                        jobOrder.orderStatusId =
                            dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(
                                Constants.FAILED_TEXT
                            )
                        jobOrder.isUpdateWithNoInternet = true
                        jobOrder.dropOffTimeEnd = dropOffTime
                        jobOrder.isReattemptJob = false
                        dbUpdateJobs!!.addJobDetails(jobOrder)
                        jobSteps.job_step_status_id = Constants.JOB_STEP_FAILED
                        jobSteps.isJobStepUpdateWithNoInternet = true
                        dbUpdateJobs!!.addJobStepDetails(jobSteps)
                        val uniqueIDOrderAttempt = ((Date().time / 1000L) % Int.MAX_VALUE).toInt()
                        val orderAttempt = OrderAttempt()
                        orderAttempt.id = uniqueIDOrderAttempt
                        orderAttempt.setLatitude(location.latitude)
                        orderAttempt.setLongitude(location.longitude)
                        orderAttempt.reason = reasonSpinner.selectedItem.toString()
                        orderAttempt.note = notes?.text.toString()
                        orderAttempt.isAttemptUpdateWithNoInternet = true
                        orderAttempt.jobStepID = jobSteps.id
                        orderAttempt.orderId = orderDetailId
                        val format2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        val date = Date()
                        val currentDate = format2.format(date)
                        orderAttempt.submitted_time = currentDate
                        dbUpdateJobs!!.addOrderAttemptDetails(orderAttempt)


                        //Akmal help me check this.

//                            jobSteps = dbUpdateJobs.getJobStepsDetailsBasedOnID(jobSteps.getId());
//                            jobStepStatusId = jobSteps.getJob_step_status_id();
//                            setupDisplayData(jobSteps);
                    }
                } else {
                    reasonFailed = ""
                    dialog.dismiss()
                    reasonFailed = reasonSpinner.selectedItem.toString()
                    //btnFailedJobStep.setEnabled(false);
                    jsonStructure(Constants.JOB_STEP_FAILED, jobSteps)
                }
            }
        })
        textViewTitle.text = "Enter a reason to fail this job"
        btnCamera.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View) {
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    showErrorInternetDialog().show()
                } else {
                    addImages()
                }
            }
        })
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).visibility = View.GONE
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).visibility = View.GONE
    }

    fun completeThisJobProcess(step: JobSteps, totalOrderItem: Int) {
        LogCustom.i("totalOrderItem scan", "" + totalOrderItem)
        if (step.is_scan_required /*&& totalOrderItem != 0*/) {
            // need to scan first
            LogCustom.i("need scan", "11")
            scanProcessAllItem(false, jobOrder!!.itemTrackingNumber, totalOrderItem)
        } else {
            //not need to scan
            if (step.is_signature_required) {
                // need to sign
                LogCustom.i("need sign", "11")
                val generalActivity = GeneralActivity()
                if (!generalActivity.haveInternetConnected(applicationContext)) {
                    Log.i("internetConnection", "no")
                    openSignConfirmationPageWithNoInternet(step)
                } else {
                    openSignConfirmationPage(step)
                }
            } else {
                //not need to sign
                LogCustom.i("not need sign", "11")

                //need to uncomment and check
                if (orderDetailId != 0 && step.id != 0) {
                    val generalActivity = GeneralActivity()
                    if (!generalActivity.haveInternetConnected(applicationContext)) {
                        Log.i("internetConnection", "no")
                        jsonStructureCompleteWithNoInternet(
                            Constants.JOB_STEP_COMPLETED,
                            true,
                            step
                        )
                    } else {
                        jsonStructure(Constants.JOB_STEP_COMPLETED, step)
                    }
                } else {
                    showErrorDialog(
                        resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                            R.string.orderDetailError
                        )
                    ).show()
                }
            }
        }
    }

    fun openSignConfirmationPage(step: JobSteps) {
        //need to uncomment and check
        var isLastJobStep = false
        if (step.order_sequence == jobOrder!!.jobStepsArrayList.size) {
            isLastJobStep = true
        }
        if (orderDetailId != 0) {
            val i = Intent(applicationContext, JobStepSignConfirmation::class.java)
            i.putExtra("jobSteps", step)
            i.putExtra("OrderId", orderDetailId)
            i.putExtra("IsFromOrderDetails", true)
            i.putExtra("trackingNumber", jobOrder!!.itemTrackingNumber)
            i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
            i.putExtra("startJobStepsButton", startJobStepsButton)
            i.putExtra("isLastJobStep", isLastJobStep)
            startActivityForResult(i, Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION)
        } else {
            showErrorDialog(
                resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                    R.string.orderDetailError
                )
            ).show()
        }
    }

    /*===== SCAN PROCESS ======= */ //    public void scanProcess(){
    //
    //        List<String> trakingNumList = new ArrayList<String>();
    //
    //        if(trackingNumber.equalsIgnoreCase("")){
    //            trackingNumber = "1234567890";
    //        }
    //
    //        trakingNumList.add(trackingNumber);
    //
    //
    //        if (trakingNumList.size() != 0 ) {
    //            Intent intent = new Intent().setClass(this, ScanDoActivity.class);
    //            intent.putStringArrayListExtra("trakingNumList", (ArrayList<String>) trakingNumList);
    //            startActivityForResult(intent, Constants.INTENT_ACTIVITY_SCAN);
    //        }
    //    }
    fun openSignConfirmationPageWithNoInternet(step: JobSteps) {
        //need to uncomment and check
        var isLastJobStep = false
        if (step.order_sequence == jobOrder!!.jobStepsArrayList.size) {
            isLastJobStep = true
        }
        if (orderDetailId != 0) {
            val i = Intent(applicationContext, JobStepSignConfirmationWithNoInternet::class.java)
            i.putExtra("jobSteps", step)
            i.putExtra("OrderId", orderDetailId)
            i.putExtra("IsFromOrderDetails", true)
            i.putExtra("trackingNumber", jobOrder!!.itemTrackingNumber)
            i.putExtra("showCompletedFailedButton", showCompletedFailedButton)
            i.putExtra("startJobStepsButton", startJobStepsButton)
            i.putExtra("isLastJobStep", isLastJobStep)
            startActivityForResult(i, Constants.INTENT_ACTIVITY_JOB_STEP_SIGN_CONFIRMATION)
        } else {
            showErrorDialog(
                resources.getString(R.string.orderDetailErrorTitle), resources.getString(
                    R.string.orderDetailError
                )
            ).show()
        }
    }

    fun jsonStructureCompleteWithNoInternet(
        nextOrderStatusID: Int,
        isCompletedJobStep: Boolean,
        step: JobSteps
    ) {
        var isLastJobStep = false
        if (step.order_sequence == jobOrder!!.jobStepsArrayList.size) {
            isLastJobStep = true
        }
        if (pdUpdateProcess != null) {
            if (pdUpdateProcess!!.isShowing) {
            } else {
                pdUpdateProcess!!.show()
            }
        }
        var jobOrder = JobOrder()
        val locale = Locale("en", "US")
        val printFormat = SimpleDateFormat("hh:mm a", locale)
        val dropOffTime = printFormat.format(Date())
        jobOrder = dbUpdateJobs!!.getJobDetailsBasedOnID(orderDetailId)
        if (isLastJobStep) {
            if (isCompletedJobStep) {
                jobOrder.orderStatusId =
                    dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.COMPLETED_TEXT)
                jobOrder.dropOffTimeEnd = dropOffTime
            } else {
                jobOrder.orderStatusId =
                    dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT)
                jobOrder.dropOffTimeStart = dropOffTime
            }
        } else {
            jobOrder.orderStatusId =
                dbUpdateJobs!!.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT)
            jobOrder.dropOffTimeStart = dropOffTime
        }
        jobOrder.isUpdateWithNoInternet = true
        jobOrder.isReattemptJob = false
        dbUpdateJobs!!.addJobDetails(jobOrder)
        if (isCompletedJobStep) {
            step.job_step_status_id = Constants.JOB_STEP_COMPLETED
        } else {
            step.job_step_status_id = Constants.JOB_STEP_INPROGRESS
        }
        step.isJobStepUpdateWithNoInternet = true
        dbUpdateJobs!!.addJobStepDetails(step)
        if (isCompletedJobStep) {
            val uniqueIDOrderAttempt = ((Date().time / 1000L) % Int.MAX_VALUE).toInt()
            val orderAttempt = OrderAttempt()
            orderAttempt.id = uniqueIDOrderAttempt
            val location = lastLocation
            if (location != null) {
                orderAttempt.setLatitude(location.latitude)
                orderAttempt.setLongitude(location.longitude)
            }
            orderAttempt.received_by = "-"
            orderAttempt.reason = ""
            orderAttempt.isAttemptUpdateWithNoInternet = true
            orderAttempt.jobStepID = step.id
            orderAttempt.orderId = orderDetailId
            val format2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val date = Date()
            val currentDate = format2.format(date)
            orderAttempt.submitted_time = currentDate
            dbUpdateJobs!!.addOrderAttemptDetails(orderAttempt)
        }
        val jobSteps = dbUpdateJobs!!.getJobStepsDetailsBasedOnID(step.id)
        val jobStepStatusId = jobSteps.job_step_status_id
        // finish();
        if (pdUpdateProcess != null) {
            if (pdUpdateProcess!!.isShowing) {
                pdUpdateProcess!!.dismiss()
            }
        }

        //setupDisplayData(jobSteps);
    }

    fun scanProcessAllItem(
        changeToInprogress: Boolean,
        trackingNumber: String?,
        totalOrderItem: Int
    ) {
        val intent = Intent().setClass(this, ScanDoActivity::class.java)
        intent.putExtra("trackingNumber", trackingNumber)
        intent.putExtra("totalOrderItem", totalOrderItem)
        intent.putExtra("changeToInprogress", changeToInprogress)
        startActivityForResult(intent, Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM)
    }

    @SuppressLint("RestrictedApi")
    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority =
            LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if ((ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
                    == PackageManager.PERMISSION_GRANTED)
        ) {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback,
                Looper.myLooper()
            )

            // LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    override fun onConnectionSuspended(i: Int) {}
    override fun onConnectionFailed(connectionResult: ConnectionResult) {}
    override fun onLocationChanged(location: Location) {
        lastLocation = location

        //stop location updates
        if (mGoogleApiClient != null) {
            // LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
        }
    }

    override fun onSliderClick(slider: BaseSliderView) {
        Log.i("click slider", "yes" + slider.scaleType)
        if (slider.url != null || slider.scaleType == BaseSliderView.ScaleType.FitCenterCrop) {
            showEditDeletePhotoGaleryImage()
        }
    }

    fun fileFromBitmap(bitmap: Bitmap) {
        var file = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "temporary_file.jpg")
        CoroutineScope(Dispatchers.Main).launch {
            var baos = ByteArrayOutputStream()
            var quality = 100
            do {
                baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, baos)
                quality -= 5
            } while (baos.toByteArray().size > 4200000 && quality > 0)
            FileOutputStream(file).use {
                it.write(baos.toByteArray())
            }
        }.invokeOnCompletion {
            contextProgressDialog!!.text =
                resources.getString(R.string.loadingUploadImage)
            pdUpdateProcess!!.show()
            requestBody(file, file.path, false)
        }
    }

    companion object {
        @JvmField
        val ACTION_REFRESH_JOB_DETAIL = "refreshJobDetails"
        val ACTION_TOGGLE_ONLINE_OFFLINE = "toggleIsOnlineOffline"
        var FOLDER = ""
        fun getFullPathFromContentUri(context: Context, uri: Uri?): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                    // ExternalStorageProvider
                    if (("com.android.externalstorage.documents" == uri!!.authority)) {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":".toRegex()).toTypedArray()
                        val type = split[0]
                        if ("primary".equals(type, ignoreCase = true)) {
                            return FOLDER + "/" + split[1]
                        }

                        // TODO handle non-primary volumes
                    } else if (("com.android.providers.downloads.documents" == uri.authority)) {
                        val id = DocumentsContract.getDocumentId(uri)
                        val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            java.lang.Long.valueOf(id)
                        )
                        return getDataColumn(context, contentUri, null, null)
                    } else if (("com.android.providers.media.documents" == uri.authority)) {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":".toRegex()).toTypedArray()
                        val type = split[0]
                        var contentUri: Uri? = null
                        if (("image" == type)) {
                            contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        } else if (("video" == type)) {
                            contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                        } else if (("audio" == type)) {
                            contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                        }
                        val selection = "_id=?"
                        val selectionArgs = arrayOf(
                            split[1]
                        )
                        var cursor: Cursor? = null
                        val column = "_data"
                        val projection = arrayOf(
                            column
                        )
                        try {
                            cursor = context.contentResolver.query(
                                (uri), projection, selection, selectionArgs,
                                null
                            )
                            if (cursor != null && cursor.moveToFirst()) {
                                val column_index = cursor.getColumnIndexOrThrow(column)
                                return cursor.getString(column_index)
                            }
                        } finally {
                            cursor?.close()
                        }
                        return null
                    }
                } else if ("content".equals(uri!!.scheme, ignoreCase = true)) {
                    return getDataColumn(context, uri, null, null)
                } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                    return uri.path
                }
            }
            return null
        }

        private fun getDataColumn(
            context: Context, uri: Uri?, selection: String?,
            selectionArgs: Array<String>?
        ): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf(
                column
            )
            try {
                cursor = context.contentResolver.query(
                    (uri)!!, projection, selection, selectionArgs,
                    null
                )
                if (cursor != null && cursor.moveToFirst()) {
                    val column_index = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(column_index)
                }
            } finally {
                cursor?.close()
            }
            return null
        }
    }
}
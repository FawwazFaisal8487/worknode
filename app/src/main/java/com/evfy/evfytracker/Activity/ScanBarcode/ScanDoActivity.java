package com.evfy.evfytracker.Activity.ScanBarcode;

import static com.evfy.evfytracker.Constants.PREFS_NAME;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.google.zxing.Result;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanDoActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    /* =========== DECLARATION ================ */
    private ZXingScannerView mScannerView;
    // private ZBarScannerView mScannerView;

    /* ======== INTEGER ========== */
    private static final int ZBAR_CAMERA_PERMISSION = 1;
    int totalItemRemaining, companyId, totalOrderItem;
    boolean changeToInprogress;
    int leftBalance, totalAlreadyScanned = 0;
    /* ======== INTEGER ========== */
    TextView txt_itemRemaining;

    RelativeLayout qr_scan_parent_view;
    SharedPreferences settings;
    String accessToken, name_scan_setting, referenceNumber, trackingNumber;
    Toolbar toolbar;
    SweetAlertDialog processDialog;
    // List<String> trakingNumList = new ArrayList<String>();
    // List<String> trakingNumListTemp = new ArrayList<String>();
    Camera camera;
    boolean isFlashOpen = false;
    final MediaPlayer mp = new MediaPlayer();

    /* =========== DECLARATION ================ */


    @Override
    public void onCreate(Bundle state) {

        super.onCreate(state);

        setContentView(R.layout.activity_scan_do);

        initialize();
        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //deliveryOrderDetails = (DeliveryOrderDetails) getIntent().getSerializableExtra("deliveryOrderDetails");


        referenceNumber = getIntent().getExtras().getString("referenceNumber");
        trackingNumber = getIntent().getExtras().getString("trackingNumber");
        //  trakingNumList = getIntent().getExtras().getStringArrayList("trakingNumList");
        companyId = getIntent().getExtras().getInt("companyId");
        totalOrderItem = getIntent().getExtras().getInt("totalOrderItem");
        changeToInprogress = getIntent().getExtras().getBoolean("changeToInprogress");

        LogCustom.i("trakingNumList", "" + trackingNumber);

        toolbar.setBackgroundColor(getResources().getColor(R.color.black));
        toolbar.getBackground().setAlpha(40);
        this.getSupportActionBar().setTitle("Scan " + trackingNumber);

        leftBalance = totalOrderItem - totalAlreadyScanned;

        txt_itemRemaining.setText("" + leftBalance);

        isFlashOpen = false;

        addQRScanningView();

    }

    @Override
    public boolean onSupportNavigateUp() {
//        if(leftBalance != 0){
//            Intent i = new Intent();
//            i.putExtra("doneUpdateScan", false);
//            setResult(Constants.INTENT_ACTIVITY_SCAN,i);
//            finish();
//        }
//        else{
//            //loopScanOrder();
//            processDialog = showProcessDialog();
//        }
        Intent i = new Intent();
        i.putExtra("doneUpdateScan", false);
        setResult(Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM, i);
        finish();

        return true;
    }

    void initialize() {
        qr_scan_parent_view = findViewById(R.id.content_frame);
        txt_itemRemaining = findViewById(R.id.txt_itemRemaining);

        settings = this.getSharedPreferences(PREFS_NAME, 0);
        accessToken = settings.getString("access_token", "");
        name_scan_setting = settings.getString("name_scan_setting", "");

    }

    private void addQRScanningView() {
        mScannerView = new ZXingScannerView(this) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                //  return new CustomViewFinderView(context);
                return new ViewFinderView(context);
            }
        };
        qr_scan_parent_view.addView(mScannerView);

    }

    @Override
    public void onResume() {
        super.onResume();


        if (
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        } else {
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    mScannerView.startCamera();
                } else {
                    Toast.makeText(this, "Please grant camera permission to use the QR Scanner", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        //   Toast.makeText(this, "Contents = " + rawResult.getText() + ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();

//        for(int i = 0; i < trakingNumList.size(); i ++){
//
//            if(trakingNumListTemp.contains(trakingNumList.get(i))){
//                LogCustom.i("dah sama","dah");
//            }else{
//                if(trakingNumList.get(i).equalsIgnoreCase(rawResult.getText()) ){
//                    LogCustom.i("ok","dah");
//                    trakingNumListTemp.add(trakingNumList.get(i));
//
//                    totalAlreadyScanned = totalAlreadyScanned + 1;
//                    leftBalance = trakingNumList.size() - totalAlreadyScanned;
//                    txt_itemRemaining.setText(""+leftBalance);
//                }else{
//                    LogCustom.i("x sama","dah");
//                }
//
//            }
//
//
//        }

        if (trackingNumber.equalsIgnoreCase(rawResult.getText())) {
            LogCustom.i("ok", "dah");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mp.isPlaying()) {
                        mp.stop();
                    }

                    try {
                        mp.reset();
                        AssetFileDescriptor afd;
                        afd = getAssets().openFd("beep.mp3");
                        mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        mp.prepare();
                        mp.start();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }

                }


            });


            totalAlreadyScanned = totalAlreadyScanned + 1;
            leftBalance = totalOrderItem - totalAlreadyScanned;
            txt_itemRemaining.setText("" + leftBalance);
        } else {
            LogCustom.i("x sama", "dah");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mp.isPlaying()) {
                        mp.stop();
                    }

                    try {
                        mp.reset();
                        AssetFileDescriptor afd;
                        afd = getAssets().openFd("wrongscan.mp3");
                        mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        mp.prepare();
                        mp.start();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    showErrorDialog(getResources().getString(R.string.scanningWrongNumberInputTitle), getResources().getString(R.string.scanningWrongNumberInputError)).show();


                    Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        v.vibrate(500);
                    }

                }


            });
        }


//        for(int i = 0; i < totalOrderItem ; i ++){
//
//            if(trackingNumber.equalsIgnoreCase(rawResult.getText()) ){
//                LogCustom.i("ok","dah");
//
//                totalAlreadyScanned = totalAlreadyScanned + 1;
//                leftBalance = totalOrderItem - totalAlreadyScanned;
//                txt_itemRemaining.setText(""+leftBalance);
//            }else{
//                LogCustom.i("x sama","dah");
//            }
//
//
//        }

        //After all is scan then go previous activity with identifier scan true to process next process
        if (leftBalance == 0) {
            //processDialog = showProcessDialog();
//            SharedPreferences.Editor editors = settings.edit();
//            editors.putBoolean("doneScanToInProgress", true).commit();
//            editors.apply();

            Intent i = new Intent();
            i.putExtra("doneUpdateScan", true);
            i.putExtra("changeToInprogress", changeToInprogress);
            setResult(Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM, i);
            finish();
        }


        // Note:
        // * Wait 2 seconds to resume the preview.
        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
        // * I don't know why this is the case but I don't have the time to figure out.
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mScannerView.resumeCameraPreview(ScanDoActivity.this);
            }
        }, 2000);
    }

    /* ========= INPUT TRACKING NUMBER ======= */
    /* If user cant scan the barcode, user can insert manually tracking number*/

    public void insertTrackingNumber() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.items_insert_tracking_number_manually, null);
        final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        final EditText editTextTrackingNumber = alertLayout.findViewById(R.id.editTextTrackingNumber);


        textViewTitle.setText("Insert Tracking Number");


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        builder.setNegativeButton("CANCEL",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editTextTrackingNumber.getText().toString().equalsIgnoreCase("")) {
                    editTextTrackingNumber.setError("Required");
                } else {
                    dialog.dismiss();

                    if (trackingNumber.contains(editTextTrackingNumber.getText().toString().trim())) {
                        //  leftBalance = trakingNumList.size() - 1;
                        totalAlreadyScanned = totalAlreadyScanned + 1;
                        leftBalance = totalOrderItem - totalAlreadyScanned;
                        txt_itemRemaining.setText("" + leftBalance);
                    } else {
                        showErrorDialog(getResources().getString(R.string.scanningWrongNumberInputTitle), getResources().getString(R.string.scanningWrongNumberInputError)).show();
                    }

                    if (leftBalance == 0) {
                        //processDialog = showProcessDialog();

//                        SharedPreferences.Editor editors = settings.edit();
//                        editors.putBoolean("doneScanToInProgress", true).commit();
//                        editors.apply();

                        Intent i = new Intent();
                        i.putExtra("doneUpdateScan", true);
                        i.putExtra("changeToInprogress", changeToInprogress);
                        setResult(Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM, i);
                        finish();
                    }
                }


            }


        });

        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

            }


        });
    }

    /* ========= INPUT TRACKING NUMBER ======= */

    public void flashLight(boolean isFlashOpenFlash) {
        if (isFlashOpenFlash) {
            camera.stopPreview();
            camera.release();
            camera = null;
            isFlashOpen = false;
            invalidateOptionsMenu();
        } else {
            camera = Camera.open();
            Camera.Parameters p = camera.getParameters();
            p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
            camera.setParameters(p);
            camera.startPreview();
            isFlashOpen = true;
            invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        if (isFlashOpen) {
            menu.findItem(R.id.mMenuFlash).setIcon(R.drawable.lights_off);
        } else {
            menu.findItem(R.id.mMenuFlash).setIcon(R.drawable.lights_on);
        }


        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scan_do, menu);//Menu Resource, Menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mMenuInfo:
                insertTrackingNumber();
                return true;
            case R.id.mMenuFlash:
                if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                    flashLight(isFlashOpen);
                } else {
                    //cannot open flash
                    showErrorDialog("Flash Light cannot open", "This camera dont have features flash light").show();
                }


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        // Toast.makeText(this,"Back button is press", Toast.LENGTH_SHORT).show();
//        if(leftBalance != 0){
//            Intent i = new Intent();
//            i.putExtra("doneUpdateScan", false);
//            setResult(Constants.INTENT_ACTIVITY_SCAN,i);
//            finish();
//        }
//        else{
//            //loopScanOrder();
//            //showProcessDialog().show();
//            processDialog = showProcessDialog();
//
//        }

        Intent i = new Intent();
        i.putExtra("doneUpdateScan", false);
        setResult(Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM, i);
        finish();
        super.onBackPressed();

    }

    // AlertDialogProcess
    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ScanDoActivity.this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(true);
        return sweetAlertDialog;
    }

    SweetAlertDialog showProcessDialog() {
        final SweetAlertDialog sweetAlertDialog = getProcessDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getProcessDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ScanDoActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setContentText("You want to update scanner ?")
                .setTitleText("UPDATE")
                .showCancelButton(true)
                .setCancelText("CANCEL")
                .setConfirmText("OK")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        showCompletedDialog().show();
                    }
                });
        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    SweetAlertDialog showCompletedDialog() {
        final SweetAlertDialog sweetAlertDialog = getCompletedDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getCompletedDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(ScanDoActivity.this, SweetAlertDialog.SUCCESS_TYPE).
                setContentText("Success to update scanner")
                .setTitleText("UPDATED")
                .showCancelButton(false)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        Intent i = new Intent();
                        i.putExtra("doneUpdateScan", true);
                        i.putExtra("changeToInprogress", changeToInprogress);
                        setResult(Constants.INTENT_ACTIVITY_SCAN_ALL_ITEM, i);
                        finish();
                        sDialog.dismissWithAnimation();
                    }
                });
        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }
}


package com.evfy.evfytracker.Activity.OnBoarding;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.evfy.evfytracker.R;

import agency.tango.materialintroscreen.SlideFragment;

public class CustomSlideIntroOnBoarding extends SlideFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.activity_custom_slide_intro_on_boarding, container, false);

//        final SpannableString sassyDesc = new SpannableString("It allows you to go back, sometimes");
//        TapTargetView.showFor(getActivity(), TapTarget.forView(checkBox, "Hello, world!", sassyDesc)
//                .cancelable(false)
//                .drawShadow(true)
//                .tintTarget(false), new TapTargetView.Listener() {
//            @Override
//            public void onTargetClick(TapTargetView view) {
//                super.onTargetClick(view);
//                // .. which evidently starts the sequence we defined earlier
//                Toast.makeText(view.getContext(), "You clicked the !", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onOuterCircleClick(TapTargetView view) {
//                super.onOuterCircleClick(view);
//                Toast.makeText(view.getContext(), "You clicked the outer circle!", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
//                Log.d("TapTargetViewSample", "You dismissed me :(");
//                Toast.makeText(view.getContext(), "the outer circle!", Toast.LENGTH_SHORT).show();
//            }
//        });

        return view;
    }

    @Override
    public int backgroundColor() {
        return R.color.red;
    }

    @Override
    public int buttonsColor() {
        return R.color.darkGreyArrow;
    }


    @Override
    public boolean canMoveFurther() {
        return true;
    }

    @Override
    public String cantMoveFurtherErrorMessage() {
        // return getString(R.string.error_message);
        return "cannot proceeed";
    }
}


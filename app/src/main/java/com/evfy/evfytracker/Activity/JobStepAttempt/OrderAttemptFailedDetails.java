package com.evfy.evfytracker.Activity.JobStepAttempt;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

public class OrderAttemptFailedDetails extends AppCompatActivity {

    TextView reasonOrderFailed, timeOrderFailed;
    String time, reason, trackingNumber;
    int orderId, order_status_id, jobStepId, jobStepStatusId, numberOfPage;
    boolean showCompletedFailedButton, startJobStepsButton;
    OrderAttempt orderAttemptDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_attempt_failed_details);

        numberOfPage = 0;

        initialize();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            numberOfPage = extras.getInt("numberOfPage");
            reason = extras.getString("reason");
            time = extras.getString("time");

            orderId = extras.getInt("OrderId");
            order_status_id = extras.getInt("order_status_id", 0);
            jobStepId = extras.getInt("jobStepId");
            jobStepStatusId = extras.getInt("jobStepStatusId");
            orderAttemptDetail = (OrderAttempt) getIntent().getSerializableExtra("OrderAttemptImageSend");
            trackingNumber = extras.getString("trackingNumber");
            showCompletedFailedButton = extras.getBoolean("showCompletedFailedButton");
            startJobStepsButton = extras.getBoolean("startJobStepsButton");

        }

        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Attempt #" + numberOfPage);

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        LogCustom.i("getSubmitted_time111", time);


        timeOrderFailed.setText(time);
        reasonOrderFailed.setText(reason);
    }

    public void initialize() {
        //Initialize
        reasonOrderFailed = findViewById(R.id.reasonOrderFailed);
        timeOrderFailed = findViewById(R.id.timeOrderFailed);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        getMenuInflater().inflate(R.menu.menu_order_attempt_details, menu);
        //  return true;


        return super.onCreateOptionsMenu(menu);

        //return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        // menu.findItem(R.id.menuEditOrderAttempt).setVisible(true);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("jobStepId", jobStepId);
        ii.putExtra("jobStepStatusId", jobStepStatusId);
        ii.putExtra("OrderId", orderId);
        ii.putExtra("trackingNumber", trackingNumber);
        ii.putExtra("showCompletedFailedButton", showCompletedFailedButton);
        ii.putExtra("startJobStepsButton", startJobStepsButton);
        setResult(Constants.INTENT_ACTIVITY_LIST_ORDER_ATTEMPT, ii);
        finish();
        super.onBackPressed();
    }
}

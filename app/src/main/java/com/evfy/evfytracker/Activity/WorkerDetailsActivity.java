package com.evfy.evfytracker.Activity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.Utils.TextFitTextView;
import com.evfy.evfytracker.classes.Worker;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.pkmmte.view.CircularImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.exceptions.NoInternetConnectionException;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class WorkerDetailsActivity extends
        AppCompatActivity implements OnRESTResultCallback, OnHttpEventListener {


    SweetAlertDialog pd;
    final String PREFS_NAME = "MyPrefsFile";
    private RestManager restManager;
    HttpConnection connection;
    int workerId;
    @Nullable
    @BindView(R.id.workerName)
    TextView workerName;
    @Nullable
    @BindView(R.id.workerPhoneNum)
    TextView workerPhoneNum;
    @Nullable
    @BindView(R.id.workerEmail)
    TextFitTextView workerEmail;
    @Nullable
    @BindView(R.id.userProfileImage)
    CircularImageView userProfileImage;
    @Nullable
    @BindView(R.id.completedOrder)
    TextView completedOrder;
    @Nullable
    @BindView(R.id.currentOrder)
    TextView currentOrder;


    public static ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions options;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ActionBar actionBar = getSupportActionBar();
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#194e90"));
        actionBar.setBackgroundDrawable(colorDrawable);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(WorkerDetailsActivity.this, this);
        pd = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pd.setTitleText("Getting Details");
        pd.setCancelable(false);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            workerId = Integer.valueOf(extras.getString("WorkerId"));
        }
        setContentView(R.layout.activity_worker_details);
        ButterKnife.bind(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .memoryCacheSize(2 * 1024 * 1024)
                // 2 Mb
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO).build();
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config);
        this.options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.person)
                .showImageForEmptyUri(R.drawable.person)
                // .cacheInMemory()
                .cacheOnDisc().imageScaleType(ImageScaleType.EXACTLY)
                .resetViewBeforeLoading()
                // .displayer(new RoundedBitmapDisplayer(0))
                .build();

        try {
            JSONObject jsonObject = new JSONObject();
            SharedPreferences settings;
            settings = getSharedPreferences(
                    PREFS_NAME, 0);

            String accessToken = settings.getString("access_token", "");
            connection = new HttpConnection(Constants.SAAS_SERVER_LDS + "/api/workers/" + workerId, RESTIntentService.GET, jsonObject, this);
            List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();
            Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
            Log.i("", "accessToken 22:" + accessToken);
            header.add(pair);

            connection.setHeaders(header);
            restManager.sendRequest(connection);

        } catch (NoInternetConnectionException expected) {
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        onBackPressed();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }

    @Override
    public void onRequestInit() {
        pd.show();
    }

    @Override
    public void onRequestFinish() {
        try {
            if ((pd != null) && pd.isShowing()) {
                pd.dismiss();
            }
        } catch (final IllegalArgumentException e) {
            // Handle or log or ignore
        } catch (final Exception e) {
            // Handle or log or ignore
        }
    }

    @Override
    public void onRESTResult(int returnCode, int code, String result) {


        try {
            JSONObject data = new JSONObject(result);
            JSONObject workerObject = data.getJSONObject("worker");
            Worker worker = new Worker(workerObject);


            int completedCount = 0, currentCount = 0;

            if (data.has("orders") && !(data.isNull("orders"))) {


                JSONArray ordersArray = data.getJSONArray("orders");

                for (int i = 0; i < ordersArray.length(); i++) {
                    JSONObject orderObject = ordersArray.getJSONObject(i);
                    int orderStatusId = Integer.valueOf(orderObject.getString("order_status_id"));

                    int pickUpWorderId = 0;
                    int dropOffWorkerId = 0;


                    if (orderObject.has("pickup_worker_id") && !(orderObject.isNull("pickup_worker_id"))) {
                        pickUpWorderId = orderObject.getInt("pickup_worker_id");

                    }

                    if (orderObject.has("drop_off_worker_id") && !(orderObject.isNull("drop_off_worker_id"))) {
                        dropOffWorkerId = orderObject.getInt("drop_off_worker_id");

                    }
                    Log.i("", "order status id:" + orderStatusId + " dropoffworker id:" + dropOffWorkerId + " worker id:" + workerId + "pickup workerid:" + pickUpWorderId);

                    if (orderStatusId == 8 && dropOffWorkerId == workerId && pickUpWorderId == workerId) {
                        completedCount = completedCount + 2;
                    } else if ((orderStatusId == 8 && dropOffWorkerId == workerId) || (orderStatusId >= 4 && orderStatusId <= 8 && pickUpWorderId == workerId)) {
                        completedCount = completedCount + 1;
                    }


                    if ((orderStatusId < 4 && pickUpWorderId == workerId) || (orderStatusId > 4 && orderStatusId < 8 && dropOffWorkerId == workerId)) {
                        currentCount = currentCount + 1;
                    }


                }
            }


            String firstName = "";
            String lastName = "";
            if (worker.getFirstName() != null) {
                firstName = worker.getFirstName();
            }

            if (worker.getLastName() != null) {
                lastName = worker.getLastName();
            }

            workerName.setText(firstName + " " + lastName);

            workerPhoneNum.setText(String.valueOf(worker.getContactNo()));

            workerEmail.setText(worker.getEmail());


            imageLoader.displayImage(worker.getProfileImageUrlMedium(),
                    userProfileImage, options);


            completedOrder.setText(String.valueOf(completedCount));

            currentOrder.setText(String.valueOf(currentCount));


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}

package com.evfy.evfytracker.Activity.OrderItem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.ItemCategory;
import com.evfy.evfytracker.classes.OrderItem;
import com.evfy.evfytracker.classes.Promotion;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.DateTimeOp;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;
import com.lkh012349s.mobileprinter.Utils.StringOp;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;

public class ItemsListActivity extends AppCompatActivity implements OnRESTResultCallback, OnHttpEventListener {

	static final int RETURN_CODE_GET_PROMOTION = 0;
	static final int RETURN_CODE_GET_ITEMS = 1;
	static final String TIME_FORMAT = "dd/MM";

	final String PREFS_NAME = "MyPrefsFile";
	final List<Pair<String, String>> header = new ArrayList<>();

	RestManager restManager;
	RecyclerView mRecyclerView;
	ProgressDialog mProgressDialog;
	ItemAdapter mItemAdapter;

	@Override
	public void onRequestInit() {
	}

	@Override
	public void onRequestFinish() {
	}

	@Override
	public void onRESTResult(final int returnCode, final int code, final String result) {

		if (JavaOp.ifNullOrEmptyThenLog(result) || JavaOp.ifTrueThenLog(code != 200, " code != 200")) {
			Toast.makeText(this, R.string.msgFailedToRetrieveItemsList, Toast.LENGTH_SHORT).show();
			dismissProgressDialog();
			return;
		}

		switch (returnCode) {

			case RETURN_CODE_GET_PROMOTION:
				setPromotion(result);
				final HttpConnection connection = new HttpConnection(Constants.SAAS_SERVER_LDS + "/api/items", RESTIntentService.GET, (String) null,
						this);
				connection.setHeaders(header);

				try {
					restManager.sendRequestWithReturn(RETURN_CODE_GET_ITEMS, connection);
				} catch (final Throwable e) {
					LogCustom.e(e);
					break;
				}

				return;

			case RETURN_CODE_GET_ITEMS:
				dismissProgressDialog();
				setItems(result);
				return;

		}

		LogCustom.e("Wrong path.");
		Toast.makeText(this, R.string.msgFailedToRetrieveItemsList, Toast.LENGTH_SHORT).show();
		dismissProgressDialog();

	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {

		getMenuInflater().inflate(R.menu.items_list_activity, menu);
		MenuItem menuItemSearch = menu.findItem(R.id.menuItemSearch);
		RelativeLayout searchLayout = (RelativeLayout) MenuItemCompat.getActionView(menuItemSearch);
		final EditText editsearch = searchLayout.findViewById(R.id.searchEditText);

		final TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
			}

			@Override
			public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
				if (mItemAdapter != null) mItemAdapter.filter(s.toString());
			}

			@Override
			public void afterTextChanged(final Editable s) {
			}

		};

		editsearch.addTextChangedListener(textWatcher);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		final int id = item.getItemId();

		switch (id) {

			case android.R.id.home:
				onBackPressed();
				return true;

			case R.id.menuItemSearch:
				return true;

		}

		return super.onOptionsItemSelected(item);

	}

	void setPromotion(final String result) {

		final ObjectMapper objectMapper = new ObjectMapper();
		final Promotion promotion;
		final LinearLayout mLayoutPromotion = findViewById(R.id.mLayoutPromotion);

		try {
			promotion = objectMapper.readValue(result, Promotion.class);
		} catch (final Throwable e) {
			LogCustom.e(e);
			mLayoutPromotion.setVisibility(View.GONE);
			return;
		}

		if (!promotion.isEnabled()) {
			mLayoutPromotion.setVisibility(View.GONE);
			return;
		}

		final Date dateStart = promotion.getDateTimeStart();
		final Date dateEnd = promotion.getDateTimeEnd();

		if (JavaOp.ifNullThenLog(dateStart, dateEnd) || DateTimeOp.isFutureDay(dateStart) || DateTimeOp.isPastDay(dateEnd)) {
			mLayoutPromotion.setVisibility(View.GONE);
			return;
		}

		final TextView mTextViewPromotion = findViewById(R.id.mTextViewPromotion);
		final String dateEndStr = DateTimeOp.getString(TIME_FORMAT, dateEnd, null);
		final String promotionMsg = StringOp.formatGiveEmptyStringIfNull(this, R.string.msgPromotionValidation, promotion.getTitle(), dateEndStr);
		mTextViewPromotion.setText(promotionMsg);

	}

	void setItems(final String result) {

		final OrderItem[] orderItems;

		try {

			final JSONArray jsonArray = new JSONArray(result);
			orderItems = new OrderItem[jsonArray.length()];

			for (int i = 0; i < jsonArray.length(); i++) {
				final JSONObject jsonObject = jsonArray.getJSONObject(i);
				orderItems[i] = new OrderItem(jsonObject);
			}

		} catch (final Throwable e) {
			LogCustom.e(e);
			return;
		}

		mItemAdapter = new ItemAdapter(orderItems);
		mRecyclerView.setAdapter(mItemAdapter);

	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_items_list);

		// Configure ActionBar.
		ActionBar actionBar = getSupportActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#194e90"));
		actionBar.setBackgroundDrawable(colorDrawable);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Set up RecyclerView.
		mRecyclerView = findViewById(R.id.mRecyclerView);
		mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
		loadData();

	}

	void dismissProgressDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) mProgressDialog.dismiss();
	}

	void loadData() {

		restManager = RestManagerFactory.createRestManagerWithHttpEventListener(this, this);

		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ItemsListActivity.this);
		final String accessToken = settings.getString("access_token", "");
		mProgressDialog = new ProgressDialog(this);
		mProgressDialog.setMessage(getString(R.string.progressingMsgLoadingList));
		mProgressDialog.show();

		try {
			final HttpConnection connection = new HttpConnection(Constants.SAAS_SERVER_LDS + "/api/promotions/enabled", RESTIntentService.GET,
					(String) null, this);
			final Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
			header.add(pair);
			connection.setHeaders(header);
			restManager.sendRequestWithReturn(RETURN_CODE_GET_PROMOTION, connection);
		} catch (Throwable e) {
			LogCustom.e(e);
			dismissProgressDialog();
			Toast.makeText(this, R.string.msgUnableToLoadItemsList, Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	class ItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

		static final int VIEW_TYPE_HEADER = 0;
		static final int VIEW_TYPE_ITEM = 1;

		protected String filteredText;
		ArrayList dataDisplayed = new ArrayList();

		Object[] mHeadersAndItems;

		public ItemAdapter(final OrderItem[] items) {

			if (JavaOp.ifNullThenLog(items)) return;
			final List<ItemCategory> categories = ItemCategory.get(items);
			final int len = items.length + categories.size();
			mHeadersAndItems = new Object[len];
			int index = 0;

			for (final ItemCategory category : categories) {

				mHeadersAndItems[index++] = category;

				for (final OrderItem item : category.mItems) {
					if (JavaOp.ifNullThenLog(item)) continue;
					mHeadersAndItems[index++] = item;
				}

			}

			newDisplayData();

		}

		public void filter(final String text) {
			createDisplayData(text);
			notifyDataSetChanged();
		}

		@Override
		public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {

			final int layoutResId = viewType == VIEW_TYPE_HEADER ? R.layout.view_item_header : R.layout.view_item2;
			final View view = LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);

			switch (viewType) {

				case VIEW_TYPE_HEADER:
					return new ViewHolderHeader(view);
				case VIEW_TYPE_ITEM:

					// Set result and finish.
					view.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(final View v) {
							final int position = mRecyclerView.getChildAdapterPosition(v);
							final OrderItem orderItem = (OrderItem) dataDisplayed.get(position);
							if (JavaOp.ifNullThenLog(orderItem)) return;
							setResult(RESULT_OK, new Intent().putExtra("item", orderItem));
							finish();
						}

					});

					return new ViewHolderItem(view);

			}

			LogCustom.e("invalid viewType");
			return null;

		}

		@Override
		public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

			final int viewType = getItemViewType(position);

			switch (viewType) {

				case VIEW_TYPE_HEADER:
					setHeader((ItemCategory) dataDisplayed.get(position), (ViewHolderHeader) holder);
					break;

				case VIEW_TYPE_ITEM:
					setItem(((OrderItem) dataDisplayed.get(position)), ((ViewHolderItem) holder));
					break;

			}

		}

		@Override
		public int getItemViewType(final int position) {
			if (JavaOp.ifNullOrEmptyThenLog(dataDisplayed)) return -1;
			return dataDisplayed.get(position) instanceof ItemCategory ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
		}

		@Override
		public int getItemCount() {
			return dataDisplayed == null ? 0 : dataDisplayed.size();
		}

		protected void createDisplayData(String text) {

			if (text == null) return;

			if (mHeadersAndItems == null) {
				LogCustom.e("Null mHeadersAndItems");
				return;
			}

			text = text.toLowerCase();
			filteredText = text;

			if (JavaOp.ifNullThenLog(dataDisplayed)) return;
			dataDisplayed.clear();

			if (text.length() == 0) {

				Collections.addAll(dataDisplayed, mHeadersAndItems);

			} else {

				for (final Object ele : mHeadersAndItems) {

					if (ele == null) continue;

					if (ele instanceof ItemCategory) {
						dataDisplayed.add(ele);
					} else {
						final OrderItem orderItem = (OrderItem) ele;
						String textToCompareWithFilter = orderItem.getNameEn();
						if (textToCompareWithFilter == null) continue;
						textToCompareWithFilter = textToCompareWithFilter.toLowerCase();
						if (textToCompareWithFilter.contains(text)) dataDisplayed.add(ele);
					}

				}

				int i = 0;

				while (true) {

					final Object object = dataDisplayed.get(i);

					if (object == null) {
						i++;
						LogCustom.i("z");
						continue;
					}

					if (i == dataDisplayed.size() - 1) {

						// If the object is the last item and is a Category object, then remove it.
						if (object instanceof ItemCategory) dataDisplayed.remove(i);

							// If the object is the last item and is a OrderItem object, then indicate it as the last item for its Category.
						else ((OrderItem) object).setIsLast(true);

						LogCustom.i("oi");
						break;

					}

					// If the next object is a Category object, then remove it.
					final Object objectNext = dataDisplayed.get(i + 1);
					LogCustom.i(objectNext.getClass(), "objectNext.getClass()");

					if (objectNext == null || !(objectNext instanceof ItemCategory)) {
						i++;
						continue;
					}

					LogCustom.i(object.getClass(), "object");

					// If the next item of the current category is also a Category object => the current category does not contain any order item =>
					// remove
					// it.
					if (object instanceof ItemCategory) {
						LogCustom.i("remove category");
						dataDisplayed.remove(i);
						continue;
					}

					// If the next item of the current order item is a Category object => this order item is the last order item in its category.
					if (object instanceof OrderItem) {
						((OrderItem) object).setIsLast(true);
						LogCustom.i("set last item");
					}
					i++;

				}

			}

		}

		protected void newDisplayData() {
			dataDisplayed = new ArrayList<>();
			Collections.addAll(dataDisplayed, mHeadersAndItems);
			filter(filteredText);
		}

		void setHeader(final ItemCategory category, final ViewHolderHeader viewHolderHeader) {
            if (JavaOp.ifNullThenLog(category, viewHolderHeader)) return;
            Picasso.with(getApplicationContext()).load(Uri.parse(category.mUrlImage))
                    .into(viewHolderHeader.mImageView);
            viewHolderHeader.mTextView.setText(category.mName);
        }

		void setItem(final OrderItem item, final ViewHolderItem viewHolderItem) {

			if (JavaOp.ifNullThenLog(item, viewHolderItem) || item.isDisabled()) return;
			boolean isSale = item.isSale();

			// Show sale badge if sale.
			AndroidOp.setVisibleOrGone(isSale, viewHolderItem.mImageViewSale);

			// Set name.
			viewHolderItem.mTextViewItem.setText(item.getNameEn());

			final int paintFlagsWashIron = viewHolderItem.mTextViewPriceWashAndIron.getPaintFlags();

			// Set Wash & Iron prices.
			if (item.getWashIronPrice() == 0) {

				viewHolderItem.mTextViewPriceWashAndIron.setText(R.string.titleNA);
				viewHolderItem.mTextViewPriceWashAndIron.setPaintFlags(paintFlagsWashIron & (~Paint.STRIKE_THRU_TEXT_FLAG));
				viewHolderItem.mTextViewPriceWashAndIron.setTextColor(Color.WHITE);
				AndroidOp.setVisibleOrGone(false, viewHolderItem.mTextViewDiscountPriceWashAndIron);

			} else {

				viewHolderItem.mTextViewPriceWashAndIron.setText(formatCurrency(item.getWashIronPrice()));
				AndroidOp.setVisibleOrGone(item.isHaveDiscountWashIronPrice(), viewHolderItem.mTextViewDiscountPriceWashAndIron);

				if (item.isHaveDiscountWashIronPrice()) {
					viewHolderItem.mTextViewDiscountPriceWashAndIron.setText(formatCurrency(item.getDiscountWashironPrice()));
					viewHolderItem.mTextViewPriceWashAndIron.setPaintFlags(paintFlagsWashIron | Paint.STRIKE_THRU_TEXT_FLAG);
					viewHolderItem.mTextViewPriceWashAndIron.setTextColor(Color.GRAY);
				} else {
					viewHolderItem.mTextViewPriceWashAndIron.setPaintFlags(paintFlagsWashIron & (~Paint.STRIKE_THRU_TEXT_FLAG));
					viewHolderItem.mTextViewPriceWashAndIron.setTextColor(Color.WHITE);
				}

			}

			final int paintFlagsDryAndIron = viewHolderItem.mTextViewPriceDryAndIron.getPaintFlags();

			// Set Dry & Iron prices.
			if (item.getDryCleanPrice() == 0) {

				viewHolderItem.mTextViewPriceDryAndIron.setText(R.string.titleNA);
				viewHolderItem.mTextViewPriceDryAndIron.setPaintFlags(paintFlagsDryAndIron & (~Paint.STRIKE_THRU_TEXT_FLAG));
				viewHolderItem.mTextViewPriceDryAndIron.setTextColor(Color.WHITE);
				AndroidOp.setVisibleOrGone(false, viewHolderItem.mTextViewDiscountPriceDryAndIron);

			} else {

				viewHolderItem.mTextViewPriceDryAndIron.setText(formatCurrency(item.getDryCleanPrice()));
				AndroidOp.setVisibleOrGone(item.isHaveDiscountDryCleanPrice(), viewHolderItem.mTextViewDiscountPriceDryAndIron);

				if (item.isHaveDiscountDryCleanPrice()) {
					viewHolderItem.mTextViewDiscountPriceDryAndIron.setText(formatCurrency(item.getDiscountDryCleanPrice()));
					viewHolderItem.mTextViewPriceDryAndIron.setPaintFlags(paintFlagsDryAndIron | Paint.STRIKE_THRU_TEXT_FLAG);
					viewHolderItem.mTextViewPriceDryAndIron.setTextColor(Color.GRAY);
				} else {
					viewHolderItem.mTextViewPriceDryAndIron.setPaintFlags(paintFlagsDryAndIron & (~Paint.STRIKE_THRU_TEXT_FLAG));
					viewHolderItem.mTextViewPriceDryAndIron.setTextColor(Color.WHITE);
				}

			}

			final int paintFlagsIron = viewHolderItem.mTextViewPriceIron.getPaintFlags();

			// Set Iron prices.
			if (item.getIronPrice() == 0) {

				viewHolderItem.mTextViewPriceIron.setText(R.string.titleNA);
				viewHolderItem.mTextViewPriceIron.setTextColor(Color.WHITE);
				viewHolderItem.mTextViewPriceIron.setPaintFlags(paintFlagsIron & (~Paint.STRIKE_THRU_TEXT_FLAG));
				AndroidOp.setVisibleOrGone(false, viewHolderItem.mTextViewDiscountPriceIron);

			} else {

				viewHolderItem.mTextViewPriceIron.setText(formatCurrency(item.getIronPrice()));
				AndroidOp.setVisibleOrGone(item.isHaveDiscountIronPrice(), viewHolderItem.mTextViewDiscountPriceIron);

				if (item.isHaveDiscountIronPrice()) {
					viewHolderItem.mTextViewDiscountPriceIron.setText(formatCurrency(item.getDiscountIronPrice()));
					viewHolderItem.mTextViewPriceIron.setPaintFlags(paintFlagsIron | Paint.STRIKE_THRU_TEXT_FLAG);
					viewHolderItem.mTextViewPriceIron.setTextColor(Color.GRAY);
				} else {
					viewHolderItem.mTextViewPriceIron.setPaintFlags(paintFlagsIron & (~Paint.STRIKE_THRU_TEXT_FLAG));
					viewHolderItem.mTextViewPriceIron.setTextColor(Color.WHITE);
				}

			}

			// Hide divider if the item is the last item in the category.
			AndroidOp.setVisibleOrGone(!item.isLast(), viewHolderItem.mViewDivider);

		}

		String formatCurrency(final double value) {
			return new DecimalFormat("$0.0").format(value);
		}

		class ViewHolderItem extends RecyclerView.ViewHolder {

			public ImageView mImageViewSale;
			public TextView mTextViewItem;
			public TextView mTextViewDiscountPriceWashAndIron;
			public TextView mTextViewDiscountPriceDryAndIron;
			public TextView mTextViewDiscountPriceIron;
			public TextView mTextViewPriceWashAndIron;
			public TextView mTextViewPriceDryAndIron;
			public TextView mTextViewPriceIron;
			public View mViewDivider;

			public ViewHolderItem(final View itemView) {
				super(itemView);
				mImageViewSale = itemView.findViewById(R.id.mImageViewSale);
				mTextViewItem = itemView.findViewById(R.id.mTextViewItem);
				mTextViewDiscountPriceWashAndIron = itemView.findViewById(R.id.mTextViewDiscountPriceWashAndIron);
				mTextViewDiscountPriceDryAndIron = itemView.findViewById(R.id.mTextViewDiscountPriceDryAndIron);
				mTextViewDiscountPriceIron = itemView.findViewById(R.id.mTextViewDiscountPriceIron);
				mTextViewPriceWashAndIron = itemView.findViewById(R.id.mTextViewPriceWashAndIron);
				mTextViewPriceDryAndIron = itemView.findViewById(R.id.mTextViewPriceDryAndIron);
				mTextViewPriceIron = itemView.findViewById(R.id.mTextViewPriceIron);
				mViewDivider = itemView.findViewById(R.id.divider);
			}

		}

		class ViewHolderHeader extends RecyclerView.ViewHolder {

			public ImageView mImageView;
			public TextView mTextView;

			public ViewHolderHeader(final View itemView) {
				super(itemView);
				mImageView = itemView.findViewById(R.id.mImageView);
				mTextView = itemView.findViewById(R.id.mTextView);
			}

		}

	}

}

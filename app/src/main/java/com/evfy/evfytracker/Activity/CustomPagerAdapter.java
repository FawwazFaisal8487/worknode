package com.evfy.evfytracker.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.viewpager.widget.PagerAdapter;

import com.evfy.evfytracker.R;

import java.util.ArrayList;

/**
 * Created by 14040332 on 1/11/2016.
 */

public class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    ArrayList<Bitmap> bitmapList;
    Activity activity;

    public CustomPagerAdapter(Context context, ArrayList<Bitmap> bitmapList, Activity OrderDetailsActivity) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.bitmapList = bitmapList;
        this.activity = OrderDetailsActivity;
    }

    @Override
    public int getCount() {
        Log.i("countbitmap", bitmapList.size() + "");
        return bitmapList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        final ImageView imageView = itemView.findViewById(R.id.imageView);

        Log.i("", "custom adapter bitmaplist:" + bitmapList.get(position));
        final Bitmap bitmap = bitmapList.get(position);
        imageView.setImageBitmap(bitmap);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder imageDialog = new AlertDialog.Builder(mContext);
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View layout = inflater.inflate(R.layout.custom_fullimage_dialog,
                        activity.findViewById(R.id.layout_root));
                ImageView image = layout.findViewById(R.id.fullimage);
//                image.setImageDrawable(imageView.getDrawable());


                Display display = activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                int height = size.y;


                ImageView imageView = new ImageView(mContext);
                Bitmap bMapScaled = Bitmap.createScaledBitmap(bitmap, width, height, true);
                image.setImageBitmap(bMapScaled);


                imageDialog.setView(layout);
                imageDialog.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                });


                imageDialog.create();
                imageDialog.show();


//                Dialog builder = new Dialog(activity);
//                builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                builder.getWindow().setBackgroundDrawable(
//                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
//                builder.setOnDismissListener(new DialogInterface.OnDismissListener(){
//                    @Override
//                    public void onDismiss(DialogInterface dialogInterface) {
//                        //nothing;
//                    }
//                });
//

//                Display display = activity.getWindowManager().getDefaultDisplay();
//                Point size = new Point();
//                display.getSize(size);
//                int width = size.x;
//                int height = size.y;
//
//
//                ImageView imageView = new ImageView(mContext);
//                Bitmap bMapScaled = Bitmap.createScaledBitmap(bitmap, width, height, true);
//                imageView.setImageBitmap(bMapScaled);
//
//                builder.addContentView(imageView, new RelativeLayout.LayoutParams(
//                        ViewGroup.LayoutParams.MATCH_PARENT,
//                        ViewGroup.LayoutParams.MATCH_PARENT));
//                builder.show();

            }
        });

        container.addView(itemView);

        return itemView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}

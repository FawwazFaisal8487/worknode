package com.evfy.evfytracker.Activity.HomeActivity;

import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.evfy.evfytracker.Activity.OnBoarding.IntroOnBoardingActivity;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.Utils.GlowingText;
import com.evfy.evfytracker.Utils.SecretTextView;
import com.lkh012349s.mobileprinter.Utils.LogCustom;
//

/**
 * The launcher activity
 */
public class SplashActivity extends AppCompatActivity implements SecretTextView.OnShowingAnimationEndListener {

	public static final int ANIMTATION_TIME_TEXTVIEW = 1999;
	public static Activity context;
	private static final int INTERVAL_DOT_ANIMATION = 444;
	private static final int RESULT_GO_LOGIN = 0;
	private static final int RESULT_LOAD_DATA = 2;
	private static final int RESULT_DEACTIVATED = 3;
	private static final int RESULT_USER_NOT_FOUND = 4;
	private static final int RESULT_GO_TUTORIAL = 5;
	private static final int RESULT_GO_ON_BOARDING = 6;
	private static final String TITLE_USER_NOT_FOUND = "User Not Found";
	private static final String MSG_USER_NOT_FOUND = "User is not found. Please contact your administrator or reinstall the app.";
	SharedPreferences mPrefs;
	String userstatus, phoneno;
	private final Class loginClass = LoginActivity.class;
	private final Class drawerClass = DrawerActivity.class;

	final String PREFS_NAME = "MyPrefsFile";

	private boolean isOnShowingAnimationEndListenerSet = false;

	private SplashActivity activity;

	private TextView textViewLoadingData;
	private GlowingText glowingText;
	private boolean isForeground = false;
	private boolean splashAnimation = false;
	String orderID;
	boolean fromNotification;


	private int result = -1;

	private Handler handler;
	String settingsTAG = "AppSettings";

	private final Thread threadAnimateLoadData = new Thread() {

		@Override
		public void run() {

			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					if (!isForeground) return;
					String text = textViewLoadingData.getText().toString();
					int index = text.indexOf(".");
					int numDots = index == -1 ? 0 : text.length() - index;
					if (numDots == 9) textViewLoadingData.setText(text.substring(0, index));
					else textViewLoadingData.append(".");
					handler.postDelayed(threadAnimateLoadData, INTERVAL_DOT_ANIMATION);
				}

			});

		}

	};

	@Override
	public void onShowingAnimationEnd() {

		//gs.setSplashScreenAnimationEnded(true);

		splashAnimation = true;

		if (result == RESULT_LOAD_DATA) {
			textViewLoadingData.setVisibility(View.VISIBLE);
			glowingText = new GlowingText(activity, context, textViewLoadingData, 3f, 22f, 9f, Color.WHITE, 2, true, -1);
			//animateLoadingData();
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		LogCustom.i();
		final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			for (String key : extras.keySet()) {
				Object value = extras.get(key);
				Log.i("KEYS", "splash extras:" + String.format("%s %s (%s)", key,
						value.toString(), value.getClass().getName()));
			}
			try {
				for (String key : extras.keySet()) {
					Object value = extras.get(key);
					orderID = value.toString();

//						JSONObject value = new JSONObject( extras.get( key ).toString() );
//					Object value = extras.get(key);


					//                    Object value = extras.get(key);
					//                        Log.d("KEYS", String.format("%s %s (%s)", key,
					//                            value.toString(), value.getClass().getName()));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		Log.i("", "splash order id:" + orderID);
		super.onCreate(savedInstanceState);
		//		Pushbots.sharedInstance().init(this);
		//		Pushbots.sharedInstance().tag(settings.getString("worker_id", ""));
		String userId = settings.getString("user_name", "");
		String password = settings.getString("password", "");

		SharedPreferences.Editor editors = settings.edit();
		editors.putString("resultData", "").commit();
		editors.apply();

		setContentView(R.layout.activity_login_splash);
		context = this;
		splashAnimation = false;
		//gs.setSplashScreenAnimationEnded(false);
		//		ActionBar actionBar = getSupportActionBar();
		//
		//		actionBar.hide();
		//final TextView textViewCompany = (TextView) findViewById(R.id.textViewCompany);
		final TextView textViewSubTitle = findViewById(R.id.textViewSubTitle);
		textViewLoadingData = findViewById(R.id.textViewLoadingData);

		//		textViewCompany.getViewTreeObserver().addOnGlobalLayoutListener(
		//				new CustomOnGlobalLayoutListener(textViewCompany));
		textViewSubTitle.getViewTreeObserver().addOnGlobalLayoutListener(new CustomOnGlobalLayoutListener(textViewSubTitle));

		activity = this;
		//		boolean isIntentSentFromNotification = getIntent().getBooleanExtra(
		//				Constants.PREFS_IS_INTENT_LAUNCHED_BY_NOTIFICATION, false);
		//		gs.setFromNotification(isIntentSentFromNotification);


		//new GetNextActivityAsyncTask().execute();
		//handler = new Handler();

		int secondsDelayed = 1;
		new Handler().postDelayed(new Runnable() {
			public void run() {

				final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);

				String userId = settings.getString("user_name", "");
				boolean firstTimeUserOnBoarding = settings.getBoolean("firstTimeUserOnBoarding", true);
				LogCustom.i("firstTimeUserOnBoarding", "" + firstTimeUserOnBoarding);

				SharedPreferences.Editor editors = settings.edit();
				editors.putBoolean("neverShowMessageUpdateLatestVersion", false).commit();
				editors.apply();


				if (firstTimeUserOnBoarding) {
					result = RESULT_GO_ON_BOARDING;
				} else {
					if (userId.length() == 0 || userId.equalsIgnoreCase("")) {
						result = RESULT_GO_LOGIN;
					} else {
						result = RESULT_LOAD_DATA;
						LogCustom.i("userId", "" + userId);

					}
				}

				LogCustom.i("result splash", ": " + result);

				Intent intent = null;
				switch (result) {

					case RESULT_GO_LOGIN:
						intent = new Intent(activity, loginClass);
						break;
					case RESULT_LOAD_DATA:
						intent = new Intent(activity, drawerClass);
						break;
					case RESULT_GO_TUTORIAL:
						intent = new Intent(activity, drawerClass);
						break;
					case RESULT_GO_ON_BOARDING:
						TaskStackBuilder.create(context)
								//.addNextIntentWithParentStack(new Intent(context, LoginActivity.class))
								.addNextIntent(new Intent(context, IntroOnBoardingActivity.class))
								.startActivities();
						break;

				}

				if (intent != null) {
					intent.putExtra("order_id", orderID);
					LogCustom.i("", "splash intent order it before start activity:" + intent.getExtras().getString("order_id"));
					startActivity(intent);
					finish();
				}
			}
		}, secondsDelayed * 1000);

	}

	@Override
	protected void onPause() {
		super.onPause();
		isForeground = false;
		if (glowingText != null) glowingText.stopGlowing();
	}

	@Override
	protected void onResume() {
		super.onResume();
		isForeground = true;
		//if ( glowingText != null ) animateLoadingData();
	}

	private void animateLoadingData() {
		handler.postDelayed(threadAnimateLoadData, INTERVAL_DOT_ANIMATION);
	}

	private class CustomOnGlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {

		private final View view;

		public CustomOnGlobalLayoutListener(View view) {
			this.view = view;
		}

		@Override
		public void onGlobalLayout() {

			if (Build.VERSION.SDK_INT >= 16)
				view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
			else view.getViewTreeObserver().removeGlobalOnLayoutListener(this);

			if (view instanceof TextView) {

				SecretTextView secretTextView = new SecretTextView((TextView) view, ANIMTATION_TIME_TEXTVIEW);

				if (!isOnShowingAnimationEndListenerSet) {
					isOnShowingAnimationEndListenerSet = true;
					secretTextView.setOnShowingAnimationEndListener(activity);
				}

				secretTextView.show();

			} else if (view instanceof ImageView) {
				view.animate().alpha(0.6f).setDuration(ANIMTATION_TIME_TEXTVIEW);
			}

		}

	}

	private class GetNextActivityAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... arg0) {

			switch (result) {

				case RESULT_DEACTIVATED:
				case RESULT_USER_NOT_FOUND:
				case RESULT_GO_LOGIN:
				case RESULT_GO_TUTORIAL:
				case RESULT_LOAD_DATA:

					final long startTime = System.currentTimeMillis();
					while (!splashAnimation) ;
					final long curTime = System.currentTimeMillis();
					final long timeToSleep = SplashActivity.ANIMTATION_TIME_TEXTVIEW - curTime + startTime;

					if (timeToSleep > 0) {
						try {
							Thread.sleep(timeToSleep);
						} catch (InterruptedException ignored) {
						}
					}

					break;

			}

			return null;

		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this);

			String userId = settings.getString("user_name", "");
			boolean firstTimeUserOnBoarding = settings.getBoolean("firstTimeUserOnBoarding", true);
			LogCustom.i("firstTimeUserOnBoarding", "" + firstTimeUserOnBoarding);

			SharedPreferences.Editor editors = settings.edit();
			editors.putBoolean("neverShowMessageUpdateLatestVersion", false).commit();
			editors.apply();


			if (firstTimeUserOnBoarding) {
				result = RESULT_GO_ON_BOARDING;
			} else {
				if (userId.length() == 0 || userId.equalsIgnoreCase("")) {
					result = RESULT_GO_LOGIN;
				} else {
					result = RESULT_LOAD_DATA;
					LogCustom.i("userId", "" + userId);

				}
			}


			//boolean firstTimeBool = settings.getBoolean("first_time", false);

			//            mPrefs = Common.getPreferences(context);
			//
			//			if (mPrefs != null) {
			//
			//				// get previously stored login details
			//				userstatus = mPrefs.getString(Constants.PREFS_USER_STATUS, "");
			//				phoneno = mPrefs.getString(Constants.PREFS_PHONE_NUM, "");
			//				boolean isLoggedIn = mPrefs.getBoolean(Constants.PREFS_IS_LOGGED_IN, false);
			//
			//				if (userstatus != null) {
			//					if (userstatus.length() == 0 || !isLoggedIn) result = RESULT_GO_LOGIN;
			//					else if (userstatus.equalsIgnoreCase(Constants.activate) && isLoggedIn) result = RESULT_LOAD_DATA;
			//					else if (userstatus.toLowerCase().contains(c2dm_deactivate))
			//						Common.showDeactivateDialog(context);
			//				} else Toast.makeText(getBaseContext(), "User not found", Toast.LENGTH_SHORT).show();
			//
			//			} else result = RESULT_GO_LOGIN;


		}

		@Override
		protected void onPostExecute(Void arg) {

			super.onPostExecute(arg);
			Intent intent = null;

			LogCustom.i("result splash", ": " + result);

			switch (result) {

				case RESULT_GO_LOGIN:
					intent = new Intent(activity, loginClass);
					break;
				case RESULT_LOAD_DATA:
					intent = new Intent(activity, drawerClass);
					break;
				case RESULT_GO_TUTORIAL:
					intent = new Intent(activity, drawerClass);
					break;
				case RESULT_GO_ON_BOARDING:
					TaskStackBuilder.create(context)
							//.addNextIntentWithParentStack(new Intent(context, LoginActivity.class))
							.addNextIntent(new Intent(context, IntroOnBoardingActivity.class))
							.startActivities();
					break;

			}

			if (intent != null) {
				intent.putExtra("order_id", orderID);
				LogCustom.i("", "splash intent order it before start activity:" + intent.getExtras().getString("order_id"));
				startActivity(intent);
				finish();
			}
		}

	}

}

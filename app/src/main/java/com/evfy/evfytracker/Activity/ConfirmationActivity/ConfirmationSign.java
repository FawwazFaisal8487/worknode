package com.evfy.evfytracker.Activity.ConfirmationActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Transformers.BaseTransformer;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.OrderAttempt;
import com.evfy.evfytracker.classes.OrderAttemptImage;
import com.evfy.evfytracker.classes.WebService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.DateTimeOp;
import com.lkh012349s.mobileprinter.Utils.ImageOp;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;
import com.lkh012349s.mobileprinter.Utils.StringOp;
import com.simplify.ink.InkView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RestManager;
import cn.pedant.SweetAlert.SweetAlertDialog;
import id.zelory.compressor.Compressor;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ConfirmationSign extends AppCompatActivity implements OnHttpEventListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    //declare
    TextView txtLengthNote, txtAddPhoto;
    EditText editTextRecipientName, editTxtNote, editTextNRIC, editTextSerialNumber;
    Button btnSubmit, btnRecipientType, btnClear;
    private SliderLayout slider;
    LinearLayout btnCamera;
    InkView inkView;
    Runnable runnableGetReasons, runnableGetNotes, runnableGetRecipients;
    ArrayList<String> reasonArray, noteArray, recipientArray;
    ArrayAdapter<String> adpNotes, adpReasons, adpRecipient;
    JobOrder jobOrder;
    Activity mActivity = this;
    ListView lvNotes, lvReasons, lvRecipient;
    AlertDialog alertNotes, alertRecipient;
    RestManager restManager;
    Pair<String, String> pairHeader;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    boolean isFactory;
    boolean isFromJobFragment;
    int workerId;
    boolean isFromCompletePhoto = false;
    static String FOLDER = "";
    Uri uriPhotoTakenByCamera;
    boolean takeAnotherPhotoBool = false;
    boolean isCompleteStatusUpdate = false;
    double lat, lon;
    boolean mIsImageUploaded;
    Bitmap bitmapSignature;
    int totalImage = 0;
    HttpConnection connection;
    boolean mIsRefreshedFromAction;
    static final int REQUEST_CODE_GET_THIS_ORDER_DETAILS = 432;
    AlertDialog imageDialog, descriptionDialog;
    ArrayList<Bitmap> imagesUriList = new ArrayList<>();
    DatabaseHandlerJobs dbUpdateJobs;
    JSONArray newjobOrderIdArrays;
    boolean isFromOrderDetail = false;
    boolean requestTakeOtherPhoto = false;
    boolean resultFromGallery;
    int sizeCompressImage = 80;
    String imageBitmap, descriptionImageString;
    boolean needCompressAgain = false;
    ByteArrayOutputStream bytearrayoutputstream;
    ArrayList<String> descriptionImage = new ArrayList<>();
    Dialog pd;
    File compressedImage;
    public static final MediaType MEDIA_TYPE =
            MediaType.parse("application/json");

    static final int FAIL_COMPLETE_CODE = 100;
    int indexImageUpload;
    boolean changedStatusToIncomplete;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        LogCustom.i("InstallMultiDex");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        MultiDex.install(this);
//        LogCustom.i("InstallMultiDex");

        pd = new Dialog(this);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.custom_dialog_progress);
        pd.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pd.setCancelable(false);

        dbUpdateJobs = new DatabaseHandlerJobs(this);

        boolean isTableExist = dbUpdateJobs.isTableExists("UpdatedJobs", true);

        if (isTableExist == false) {
            dbUpdateJobs.onUpgrade(dbUpdateJobs.getWritableDatabase(), dbUpdateJobs.getWritableDatabase().getVersion(), dbUpdateJobs.getWritableDatabase().getVersion() + 1);
        }

        setContentView(R.layout.activity_confirmation_sign);
        FOLDER = getExternalFilesDir(Environment.DIRECTORY_PICTURES + "/" + "KnocKnocKapp").getAbsolutePath();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        jobOrder = (JobOrder) getIntent().getSerializableExtra("OrderJobSend");
        isFromOrderDetail = getIntent().getBooleanExtra("IsFromOrderDetails", false);
        changedStatusToIncomplete = getIntent().getBooleanExtra("changedStatusToIncomplete", false);

        String jsonArray = getIntent().getStringExtra("jobOrderIdArrays");

        if (jsonArray != null) {
            try {
                newjobOrderIdArrays = new JSONArray(jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        Log.i("jobID", "" + jobOrder.getOrderId());


        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Confirmation");

        // action bar for app

        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initialize();
        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(ConfirmationSign.this, this);

        //listener for each event
        editTextRecipientName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });

        //listener for each event
        editTextNRIC.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });

        editTextRecipientName.setText(jobOrder.getDropOffPic());

        editTxtNote.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkAllInputInserted();
            }
        });


        inkView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                btnClear.setVisibility(View.VISIBLE);
                checkAllInputInserted();
                return false;
            }
        });


        //button clear signature
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inkView.clear();
                btnClear.setVisibility(View.GONE);
                checkAllInputInserted();
            }
        });

        //button camera
        btnCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFromCompletePhoto = true;
                addImages();
            }
        });

        //button submit
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("", "btn submit click");
                SubmitConfirmation();
            }
        });


        // button list type of recipient
        btnRecipientType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                runnableGetRecipients = new Runnable() {

                    @Override
                    public void run() {
                        final OnRESTResultCallback onRESTResultCallback = new OnRESTResultCallback() {

                            @Override
                            public void onRESTResult(final int returnCode, final int code, final String result) {
                                LogCustom.i(result, "result");
                                recipientArray = new ArrayList<>();
                                JobOrder firstSelectedOrder = jobOrder;

                                if (firstSelectedOrder != null) {
                                    recipientArray.add("Consignee: " + firstSelectedOrder.getDropOffContactName());
                                }
                                try {
                                    JSONObject wholeObject = new JSONObject(result);
                                    JSONArray dataArray = wholeObject.getJSONArray("result");
                                    for (int j = 0; j < dataArray.length(); j++) {
                                        JSONObject jsonObject = (JSONObject) dataArray.get(j);
                                        Log.i("", "recipientjson: " + jsonObject.get("name").toString());
                                        recipientArray.add(jsonObject.get("name").toString());
                                    }
                                    Log.i("", "recipientlength:" + recipientArray.size());

                                    adpRecipient = new ArrayAdapter<String>(mActivity,
                                            android.R.layout.simple_list_item_1, recipientArray) {
                                        @Override
                                        public View getView(int position, View convertView, ViewGroup parent) {
                                            View row = super.getView(position, convertView, parent);

                                            TextView txt1 = row.findViewById(android.R.id.text1);

                                            if (position == 0) {
                                                txt1.setTextColor(Color.RED); // some color
                                            }

                                            return row;
                                        }
                                    };

                                    lvRecipient = new ListView(mActivity);
                                    lvRecipient.setAdapter(adpRecipient);

                                    final AlertDialog.Builder builderRecipient = new AlertDialog.Builder(mActivity);

                                    lvRecipient.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                            if (i == 0) {
                                                String consignee = recipientArray.get(i).substring(11);
                                                editTextRecipientName.setText(consignee);
                                                editTxtNote.setText("");
                                                alertRecipient.dismiss();
                                                checkAllInputInserted();
                                            } else {
                                                editTxtNote.setText(recipientArray.get(i));
                                                editTextRecipientName.setText("");
                                                alertRecipient.dismiss();
                                                checkAllInputInserted();
                                            }
                                        }
                                    });

                                    builderRecipient.setView(lvRecipient);
                                    builderRecipient.setTitle("Recipient Type");

                                    builderRecipient.setNegativeButton("Cancel", null);

                                    alertRecipient = builderRecipient.create();
                                    alertRecipient.show();

                                } catch (Exception e) {

                                }

                            }

                        };
                        callWebService(Constants.WEB_SERVICE_GET_RECIPIENTS, onRESTResultCallback);
                    }
                };
                AndroidOp.runOnMainThread(mActivity, runnableGetRecipients);

            }
        });

        getLocation();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    //    //inflate custom layout dialog
    public void addImages() {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.view_dialog_camera_gallery, null);
        final LinearLayout layoutClickCamera = alertLayout.findViewById(R.id.layoutClickCamera);
        final LinearLayout layoutClickGallery = alertLayout.findViewById(R.id.layoutClickGallery);

        layoutClickCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, AndroidOp.REQUEST_CODE_CAMERA);
//                uriPhotoTakenByCamera = AndroidOp.startCameraForResult(ConfirmationSign.this, FOLDER, null);

            }
        });

        layoutClickGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidOp.startGettingImageFromGalleryForResult(ConfirmationSign.this);
            }
        });


        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        if (requestTakeOtherPhoto) {
            alert.setTitle("Take another photo");
            alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //do nothing

                }
            });
        } else {
            alert.setTitle("Complete action using");
            alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }


        alert.setView(alertLayout);
        alert.setCancelable(false);


        imageDialog = alert.create();
        if (!imageDialog.isShowing()) {
            imageDialog.show();
        }
    }

    void callWebService(final WebService webService, final OnRESTResultCallback onRESTResultCallback) {

        LogCustom.i(webService, "webService");
        //LogCustom.i( uri.getPath(), "uri" );
        //   settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
        settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
        settings.edit().putBoolean("isUpdateNeeded", false).apply();
        isFactory = settings.getBoolean("is_factory", false);
        workerId = Integer.parseInt(settings.getString("worker_id", "0"));
        String accessToken = settings.getString("access_token", "");
        pairHeader = Pair.create("Authorization", "Bearer " + accessToken);
        final List<Pair<String, String>> header = new ArrayList<>();
        header.add(pairHeader);
        Pair<String, String> pairHeader2 = Pair.create("Content-Type", "text/plain");

        header.add(pairHeader2);

        Log.i("", "access token:" + accessToken);

        final String body = "";

        Log.i("", "header:" + header);

        AndroidOp.runInBackground(mActivity, new Runnable() {

            @Override
            public void run() {

                AndroidOp.runOnMainThread(mActivity, new Runnable() {

                    @Override
                    public void run() {

                        try {
                            restManager.sendRequest(webService.getHttpConnection(header, body, onRESTResultCallback, ""));
                        } catch (final Throwable e) {
                            LogCustom.e(e);
                            Log.i("", "msgNetworkError 22");

                        }

                    }

                });

            }

        });


    }


    @Override
    public void onRequestInit() {
    }

    @Override
    public void onRequestFinish() {
    }


    public void initialize() {

        slider = findViewById(R.id.slider);
        inkView = findViewById(R.id.inkView);
        btnCamera = findViewById(R.id.btnCamera);
        btnClear = findViewById(R.id.btnClear);
        btnRecipientType = findViewById(R.id.btnRecipientType);
        btnSubmit = findViewById(R.id.btnSubmit);
        editTextRecipientName = findViewById(R.id.editTextRecipientName);
        editTxtNote = findViewById(R.id.editTxtNote);
        editTextSerialNumber = findViewById(R.id.editTextSerialNumber);
        editTextNRIC = findViewById(R.id.editTextNRIC);
        txtAddPhoto = findViewById(R.id.txtAddPhoto);

    }

    public void checkAllInputInserted() {
        if (changedStatusToIncomplete) {
            btnSubmit.setEnabled(true);
            btnSubmit.setTextColor(Color.parseColor("#FFFFFF"));
            btnSubmit.setBackgroundColor(Color.parseColor("#39B54A"));
        } else {
            if (inkView.isDrawn() && editTextRecipientName.getText().toString().trim().length() != 0 && editTextNRIC.getText().toString().trim().length() != 0) {
                btnSubmit.setEnabled(true);
                btnSubmit.setTextColor(Color.parseColor("#FFFFFF"));
                btnSubmit.setBackgroundColor(Color.parseColor("#39B54A"));
            } else {
                btnSubmit.setEnabled(false);
                btnSubmit.setTextColor(Color.parseColor("#EE666666"));
                btnSubmit.setBackgroundColor(Color.parseColor("#D2CFD2"));
            }
        }

    }

    SweetAlertDialog showProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = getProgressDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getProgressDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE).setContentText(
                getString(R.string.msgProcessingProcessing)).setTitleText("");
        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }

    public void addDescription(final Intent data, final boolean resultFromGalleryDescription) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.items_layout_description_image, null);
        final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);


        textViewTitle.setText("Add Description for Image");


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

        builder.setCancelable(false);

        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                descriptionImage.add(editTextDescription.getText().toString());
                dialog.dismiss();


                if (!resultFromGalleryDescription) {
                    try {
                        resultFromGallery = false;
                        Bitmap pic = (Bitmap) data.getExtras().get("data");
                        final String curTime = DateTimeOp.getStringNow(DateTimeOp.TimeZone.LOCAL);
                        String filename = curTime.replace(":", ".") + ".jpg";
                        File file = new File(FOLDER + "/" + filename);
                        try {
                            FileOutputStream outputStream = new FileOutputStream(file);
                            pic.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        uriPhotoTakenByCamera = Uri.fromFile(file);
                        showImageCameraGallery(uriPhotoTakenByCamera);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        resultFromGallery = true;
                        Uri photoUri = data.getData();
                        showImageCameraGallery(photoUri);
                    } catch (IOException e) {
                        e.printStackTrace();

                    }
                }
            }


        });
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != RESULT_OK) return;

        switch (requestCode) {

            case AndroidOp.REQUEST_CODE_CAMERA:
                Log.i("requset", "request code camera");

                resultFromGallery = false;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }
                addDescription(data, resultFromGallery);


                break;

            case AndroidOp.REQUEST_CODE_GALLERY:
                Log.i("", "request code gallery");

                resultFromGallery = true;
                if (imageDialog == null) {

                } else {
                    if (imageDialog.isShowing()) {
                        imageDialog.dismiss();
                    }
                }
                addDescription(data, resultFromGallery);

                break;
            case 80:
                break;

        }

    }

    public String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    void showImageCameraGallery(final Uri uri) throws IOException {

        //Update show image first
        takeAnotherPhotoBool = false;
        Log.i("uriPhotoTakenByCamera", "" + uri.getPath());


        totalImage = totalImage + 1;
        HashMap<String, Bitmap> url_maps = new HashMap<String, Bitmap>();

        descriptionImageString = descriptionImage.get(totalImage - 1);

        if (resultFromGallery) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();

            File imgFile = new File(filePath);
            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));


            url_maps.put("" + descriptionImageString, bmp);

            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

            imagesUriList.add(compressedImageBitmap);

        } else {


            File imgFile = new File(uri.getPath());


            Log.i("", "image file url 00:" + uriPhotoTakenByCamera);
            Log.i("", "image file url 11:" + uri);

            Log.i("", "image file url get path:" + uri.getPath());

            Log.i("", "image file:" + imgFile);
            Log.i("", "image file absolute:" + imgFile.getAbsolutePath());

            Bitmap bmp = BitmapFactory.decodeFile(compressImage(imgFile.getAbsolutePath()));


            url_maps.put("" + descriptionImageString, bmp);
            // url_maps.put("Photo No " + totalImage, bmp);
            Bitmap compressedImageBitmap = new Compressor(this).compressToBitmap(imgFile);

            imagesUriList.add(compressedImageBitmap);

        }

        Log.i("InThis", "yes");


        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(ConfirmationSign.this);
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(ConfirmationSign.this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);


            slider.addSlider(textSliderView);
        }

        slider.setVisibility(View.VISIBLE);


        if (totalImage < 2) {
            Log.i("totalImage1", "" + totalImage);
            slider.stopAutoCycle();
            slider.setPagerTransformer(false, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
//
        } else if (totalImage > 1) {
            Log.i("totalImage2", "" + totalImage);
            slider.startAutoCycle();
            slider.setPagerTransformer(true, new BaseTransformer() {
                @Override
                protected void onTransform(View view, float v) {
                }
            });
        }
        //Update show image first

        requestTakeOtherPhoto = true;
        addImages();


    }


    @Override
    public void onLocationChanged(Location location) {

        lat = location.getLatitude();
        lon = location.getLongitude();
        lastLocation = location;

        Log.i("", "on location cahnged lat:" + lat + " lon:" + lon);


    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }


    Location lastLocation;
    private GoogleApiClient googleApiClient;
    FusedLocationProviderApi fusedLocationProviderApi;


    LocationRequest locationRequest;

    private void getLocation() {

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(30000);
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.i(ConfirmationSign.class.getSimpleName(), "Connected to Google Play Services!");

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            lastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(googleApiClient);
            Log.i("", "last location in on connected:" + lastLocation);
            if (lastLocation == null) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            }

        }
    }


    public Location getLastLocation() {

        return lastLocation;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(ConfirmationSign.class.getSimpleName(), "Can't connect to Google Play Services!");
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, please turn it on to update job status.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    boolean isRestServiceResultSuccessful(final int code, final String result) {

        if (code != 200) {
            LogCustom.i(StringOp.formatGiveEmptyStringIfNull("---|%s|---", result), "result");
            LogCustom.e("code == " + code);
            return false;
        }

        if (JavaOp.ifNullOrEmptyThenLog(result)) {
            Log.i("", "msgNetworkError 88");

            //showToast( R.string.msgNetworkError );
            return false;
        }

        LogCustom.i(result, "result");
        return true;

    }

    public String encodeTobase64(Bitmap image) {
        Bitmap bitmap = image;
        final int lnth = bitmap.getByteCount();
        ByteBuffer dst = ByteBuffer.allocate(lnth);
        bitmap.copyPixelsToBuffer(dst);
        byte[] b = dst.array();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }


    void stopProgressDialogAndShowMsg(final SweetAlertDialog progressDialog, final Integer msgResId) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismissWithAnimation();
        if (msgResId != null)
            Toast.makeText(ConfirmationSign.this, msgResId, Toast.LENGTH_SHORT).show();
    }

    public static float humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes;
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        return (float) (bytes / Math.pow(unit, exp));
    }

    public void SubmitConfirmation() {
        Log.i("", "btn submit click 22");

        if (!changedStatusToIncomplete) {
            if (!inkView.isDrawn()) {
                final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mActivity, SweetAlertDialog.ERROR_TYPE);
                sweetAlertDialog.setTitleText(getString(R.string.titleError));
                sweetAlertDialog.setContentText(getString(R.string.msgErrorSignFirst));
                sweetAlertDialog.setConfirmText(getString(R.string.titleOk));
                sweetAlertDialog.show();
                return;
            }

//            if (editTextRecipientName.getText().toString().trim().length() == 0) {
//                final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(mActivity, SweetAlertDialog.ERROR_TYPE);
//                sweetAlertDialog.setTitleText(getString(R.string.titleError));
//                sweetAlertDialog.setContentText(getString(R.string.msgErrorRecipient));
//                sweetAlertDialog.setConfirmText(getString(R.string.titleOk));
//                sweetAlertDialog.show();
//                return;
//            }
        }


        Log.i("", "btn submit click 33");


        bitmapSignature = inkView.getBitmap(getResources().getColor(R.color.white));


        try {
            Log.i("", "btn submit click 44");

            if (isFromOrderDetail == false) {
                OrderAttempt orderAttempt = new OrderAttempt();
                jobOrder.getmOrderAttemptsList().add(orderAttempt);
            }
            OrderAttempt orderAttempt = jobOrder.getmOrderAttemptsList().get(jobOrder.getmOrderAttemptsList().size() - 1);


            JSONObject jobOrderObject = new JSONObject();
            JSONArray jobOrderIdArray = new JSONArray();


            jobOrderIdArray.put(String.valueOf(jobOrder.getOrderId()));
            Log.i("jobOrderIdArray", "" + jobOrderIdArray);


            //Update Passing from JobFragement
            // jobOrderObject.put("id", jobOrderIdArray);
            if (newjobOrderIdArrays != null) {
                jobOrderObject.put("id", newjobOrderIdArrays);
            } else {
                jobOrderObject.put("id", jobOrderIdArray);
            }

            jobOrderObject.put("drop_off_description", jobOrder.getDropOffDesc());
            Locale locale = new Locale("en", "US");

            SimpleDateFormat printFormat = new SimpleDateFormat("hh:mm a", locale);
            String dropOffTimeEnd = printFormat.format(new Date());
            LogCustom.i("time", "currentTime" + dropOffTimeEnd);

            LogCustom.i("orderID", "is " + jobOrder.getOrderStatusId());

            if (jobOrder.getOrderStatusId() != Constants.INCOMPLETED) {
                jobOrderObject.put("drop_off_time_end", dropOffTimeEnd);
            }

            final String signature = ImageOp.convert(bitmapSignature, Bitmap.CompressFormat.JPEG, 80);

            final String[] body = new String[1];
            body[0] = "data:image/png;base64," + signature;

            Log.i("", "0101 signature base 64 length2:" + body[0].getBytes().length);
            Log.i("", "0101 signature base 64 readible size2:" + humanReadableByteCount(body[0].getBytes().length, true));


            if (changedStatusToIncomplete) {
                if (inkView.isDrawn()) {
                    OrderAttemptImage signatureOrderAttemptImage = new OrderAttemptImage(body[0], "Recipient’s Signature", true);
                    orderAttempt.getmOrderAttemptImages().add(signatureOrderAttemptImage);
                }
            } else {
                OrderAttemptImage signatureOrderAttemptImage = new OrderAttemptImage(body[0], "Recipient’s Signature", true);
                orderAttempt.getmOrderAttemptImages().add(signatureOrderAttemptImage);
            }


            Log.i("", "image uri list:" + imagesUriList.size());
            Log.i("", "image order attempt list 00:" + orderAttempt.getmOrderAttemptImages().size());

            if (imagesUriList.size() > 0) {

                if (changedStatusToIncomplete) {
                    jobOrderObject.put("order_status_id", Constants.INCOMPLETED);
                } else {
                    if (imagesUriList.size() == 1) {
                        jobOrderObject.put("order_status_id", Constants.COMPLETE);
                    } else {
                        jobOrderObject.put("order_status_id", Constants.INCOMPLETED);
                    }
                }

                indexImageUpload = 0;
                Bitmap bmp = imagesUriList.get(indexImageUpload);

                imageBitmap = ImageOp.convert(bmp, Bitmap.CompressFormat.JPEG, 80);

                String[] body2 = new String[1];
                body2[0] = "data:image/png;base64," + imageBitmap;
                Log.i("", "base 64 length2:" + body2[0].getBytes().length);
                Log.i("", "base 64 readible size2:" + humanReadableByteCount(body2[0].getBytes().length, true));


                Log.i("", "image order attempt list 11:" + orderAttempt.getmOrderAttemptImages().size());

                OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage(body2[0], descriptionImage.get(indexImageUpload), false);
                orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);

                LogCustom.i("descriptionImage", "is:" + descriptionImage.get(indexImageUpload));
            } else {
                if (changedStatusToIncomplete) {
                    if (inkView.isDrawn()) {
                        jobOrderObject.put("order_status_id", Constants.COMPLETE);
                    } else {
                        jobOrderObject.put("order_status_id", Constants.INCOMPLETED);
                    }
                } else {
                    jobOrderObject.put("order_status_id", Constants.COMPLETE);
                }

            }


            Log.i("", "order attempt image size 22:" + orderAttempt.getmOrderAttemptImages().size());


            //OrderAttempt orderAttempt = new OrderAttempt();

            Location location = getLastLocation();

            Log.i("LastLocation11", "" + location);


            if (location == null) {
                buildAlertMessageNoGps();

            } else {
                orderAttempt.setLatitude(location.getLatitude());
                orderAttempt.setLongitude(location.getLongitude());
                orderAttempt.setReceived_by(editTextRecipientName.getText().toString());
                orderAttempt.setNote(editTxtNote.getText().toString());
                orderAttempt.setNric(editTextNRIC.getText().toString());
                orderAttempt.setSrno(editTextSerialNumber.getText().toString());

                orderAttempt.setReason("");

                JSONArray orderAttemptArray = new JSONArray();
                orderAttemptArray.put(orderAttempt.getJSONObject());
                jobOrderObject.put("order_attempts", orderAttemptArray);


                Log.i("", "order attempt:" + orderAttempt);
                Log.i("", "order attempt json:" + orderAttempt.getJSONObject());
                Log.i("", "job order json:" + jobOrderObject);


                JSONObject dataObject = new JSONObject();
                JSONArray jobDataArray = new JSONArray();
                jobDataArray.put(jobOrderObject);
                dataObject.put("data", jobDataArray);

                Log.i("", "data object:" + dataObject);

                //   updateOrder(dataObject, jobOrderIdArray);

                postRequest(dataObject);
            }


        } catch (Exception e) {
            Log.i("", "exception :" + e);

        }
    }


    void repeatUploadImage(int indexImageUpload, int order_id, int orderAttemptId) {
        try {
            Log.i("", "btn submit click 44");


            //OrderAttempt orderAttempt = jobOrder.getmOrderAttemptsList().get(jobOrder.getmOrderAttemptsList().size() - 1);

            OrderAttempt orderAttempt = new OrderAttempt();

            JSONObject jobOrderObject = new JSONObject();
            JSONArray jobOrderIdArray = new JSONArray();


            jobOrderIdArray.put(order_id);
            Log.i("jobOrderIdArray", "" + jobOrderIdArray);


            jobOrderObject.put("id", jobOrderIdArray);

            if (indexImageUpload == imagesUriList.size() - 1) {
                if (changedStatusToIncomplete) {
                    if (inkView.isDrawn()) {
                        jobOrderObject.put("order_status_id", Constants.COMPLETE);
                    } else {
                        jobOrderObject.put("order_status_id", Constants.INCOMPLETED);
                    }
                } else {
                    jobOrderObject.put("order_status_id", Constants.COMPLETE);
                }

            } else {
                jobOrderObject.put("order_status_id", Constants.INCOMPLETED);
            }


            Bitmap bmp = imagesUriList.get(indexImageUpload);


            imageBitmap = ImageOp.convert(bmp, Bitmap.CompressFormat.JPEG, 80);

            String[] body2 = new String[1];
            body2[0] = "data:image/png;base64," + imageBitmap;
            Log.i("", "base 64 length2:" + body2[0].getBytes().length);
            Log.i("", "base 64 readible size2:" + humanReadableByteCount(body2[0].getBytes().length, true));


            Log.i("", "image order attempt list 11:" + orderAttempt.getmOrderAttemptImages().size());

            OrderAttemptImage imageOrderAttemptImage = new OrderAttemptImage(body2[0], descriptionImage.get(indexImageUpload), false);
            orderAttempt.getmOrderAttemptImages().add(imageOrderAttemptImage);

            orderAttempt.setId(orderAttemptId);

            JSONArray orderAttemptArray = new JSONArray();
            orderAttemptArray.put(orderAttempt.getJSONObjectWithIDRepeatImage());
            jobOrderObject.put("order_attempts", orderAttemptArray);


            Log.i("", "order attempt:" + orderAttempt);
            Log.i("", "order attempt json:" + orderAttempt.getJSONObjectWithIDRepeatImage());
            Log.i("", "job order json:" + jobOrderObject);


            JSONObject dataObject = new JSONObject();
            JSONArray jobDataArray = new JSONArray();
            jobDataArray.put(jobOrderObject);
            dataObject.put("data", jobDataArray);

            Log.i("", "data object:" + dataObject);

            postRequest(dataObject);


        } catch (Exception e) {
            Log.i("", "exception :" + e);

        }

    }


    void postRequest(final JSONObject postBody) throws IOException {

        // OkHttpClient client = new OkHttpClient();

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .build();

        // settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
        settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
        String accessToken = settings.getString("access_token", "");

        RequestBody bodys = RequestBody.create(MEDIA_TYPE,
                postBody.toString());


        String postUrl = Constants.SAAS_SERVER_LDS + "/api/orders/assign/order";
        LogCustom.i("urlPost", postUrl);

        Request request = new Request.Builder()
                .url(postUrl)
                .post(bodys)
                .addHeader("Content-Type", "application/json")
                .addHeader("Authorization", "Bearer " + accessToken)
                .build();

        pd.show();

        LogCustom.i("bodys", "" + bodys.toString());

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // String mMessage = e.getMessage().toString();
                //  LogCustom.i("requestFailure", ""+mMessage);
                LogCustom.i("in", "onFailure");
                pd.dismiss();
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String mMessage = response.body().string();

                if (response.isSuccessful()) {
                    try {
                        LogCustom.i("ResponseOkHttp", mMessage);

                        JSONObject resultObject = new JSONObject(mMessage);

                        if (resultObject.has("status") && !(resultObject.isNull("status"))) {
                            if (resultObject.getBoolean("status")) {

                                if (resultObject.has("attempts") && !(resultObject.isNull("attempts"))) {
                                    JSONArray data = resultObject.getJSONArray("attempts");
                                    for (int i = 0; i < data.length(); i++) {


                                        indexImageUpload = indexImageUpload + 1;
                                        if (imagesUriList.size() > 0) {
                                            if (indexImageUpload == imagesUriList.size()) {
                                                Log.i("", "msgNetworkError 00");
                                                pd.dismiss();
                                                Intent ii = new Intent();
                                                ii.putExtra("OrderId", String.valueOf(jobOrder.getOrderId()));
                                                ii.putExtra("needBackPress", true);
                                                setResult(FAIL_COMPLETE_CODE, ii);
                                                finish();
                                            } else {
                                                JSONObject jsonObject2 = (JSONObject) data.get(i);
                                                int order_id = Integer.valueOf(jsonObject2.getString("order_id"));
                                                int order_attempt_id = Integer.valueOf(jsonObject2.getString("id"));
                                                LogCustom.i("order id is : " + order_id, "orderAttemptId is : " + order_attempt_id);
                                                repeatUploadImage(indexImageUpload, order_id, order_attempt_id);
                                            }
                                        } else {
                                            Log.i("", "msgNetworkError 00");
                                            pd.dismiss();
                                            Intent ii = new Intent();
                                            ii.putExtra("OrderId", String.valueOf(jobOrder.getOrderId()));
                                            ii.putExtra("needBackPress", true);
                                            setResult(FAIL_COMPLETE_CODE, ii);
                                            finish();
                                        }


                                    }

                                }


                            }
                        } else {
                            pd.dismiss();
                            onBackPressed();


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i("", "time out need to store in databse 11");
                        pd.dismiss();
                        onBackPressed();
                    }

                }


            }
        });
    }


    SweetAlertDialog progressDialog;

    public void updateOrder(final JSONObject jobOrderObject, final JSONArray orderIdArray) {


        final boolean[] orderSuccesfullySent = {false};
        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                //updateSignature();
                //      progressDialog = showProgressDialog();

                pd.show();

                final OnRESTResultCallback onRESTResultCallback2 = new OnRESTResultCallback() {

                    @Override
                    public void onRESTResult(final int returnCode, final int code, final String result) {
                        LogCustom.i(result, "resultConfirmation");


                        try {
                            JSONObject resultObject = new JSONObject(result);

                            if (resultObject.has("status") && !(resultObject.isNull("status"))) {
                                if (resultObject.getBoolean("status")) {
                                    orderSuccesfullySent[0] = true;
                                    boolean isSuccessful = isRestServiceResultSuccessful(code, result);

                                    mIsRefreshedFromAction = true;

                                    isSuccessful = true;

                                    Log.i("", "msgNetworkError 00");
                                    //        stopProgressDialogAndShowMsg(progressDialog,isSuccessful ? R.string.msgSuccessfullyPickedUpPickUpOrder : R.string.msgNetworkError);
                                    pd.dismiss();
                                    onBackPressed();

                                }
                            } else {

                                Log.i("CompressAgain1", "Need1");
                                //   dbUpdateJobs.addOrder(orderIdArray, jobOrderObject);
                                Log.i("", "time out need to store in databse 00");
                                pd.dismiss();
                                //   stopProgressDialogAndShowMsg(progressDialog, R.string.msgNetworkTimeOut);
                                onBackPressed();


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
//                            needCompressAgain = true;
//                            Log.i("CompressAgain","Need");
//                            SubmitConfirmation();


                            // dbUpdateJobs.addOrder(orderIdArray, jobOrderObject);
                            Log.i("", "time out need to store in databse 11");
                            pd.dismiss();
                            //    stopProgressDialogAndShowMsg(progressDialog, R.string.msgNetworkTimeOut);
                            onBackPressed();


                        }


                    }

                };


                //settings = mActivity.getSharedPreferences(PREFS_NAME, 0);
                settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
                settings.edit().putBoolean("isUpdateNeeded", false).apply();

                isFactory = settings.getBoolean("is_factory", false);
                workerId = Integer.parseInt(settings.getString("worker_id", "0"));
                String accessToken = settings.getString("access_token", "");
                pairHeader = Pair.create("Authorization", "Bearer " + accessToken);
                final List<Pair<String, String>> header = new ArrayList<>();
                header.add(pairHeader);
                Pair<String, String> pairHeader2 = Pair.create("Content-Type", "application/json");

                header.add(pairHeader2);

                Log.i("", "access token:" + accessToken);


                try {
                    restManager.sendRequest(Constants.WEB_SERVICE_BATCH_UPDATE.getHttpConnection(header, jobOrderObject.toString(), onRESTResultCallback2, ""));
                } catch (final Throwable e) {
                    LogCustom.e(e);
                    Log.i("CompressAgain2", "Need2");
                    Log.i("", "msgNetworkError 11");
                    orderSuccesfullySent[0] = false;
                    Log.i("", "time out need to store in databse 22");

                    //    dbUpdateJobs.addOrder(orderIdArray, jobOrderObject);
                    pd.dismiss();
                    //         stopProgressDialogAndShowMsg(progressDialog, R.string.msgNetworkError);

                    mIsImageUploaded = true;
                    onBackPressed();


                }


            }


        };

        AndroidOp.runOnMainThread(ConfirmationSign.this, runnable);

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onBackPressed() {
        Intent ii = new Intent();
        ii.putExtra("needBackPress", false);
        ii.putExtra("OrderId", String.valueOf(jobOrder.getOrderId()));
        setResult(FAIL_COMPLETE_CODE, ii);
        finish();
        super.onBackPressed();
    }


    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(FOLDER);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

}

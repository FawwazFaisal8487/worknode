package com.evfy.evfytracker.Activity.WMSActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;
import com.evfy.evfytracker.classes.DeliveryOrderOutDetails;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class BalanceEnquiryActivity extends AppCompatActivity {

    DeliveryOrderOutDetails deliveryOrderOutDetails;
    JSONArray jsonArrays;
    LinearLayout mLayoutList;

    String accessToken;
    String name_scan_setting;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    TextView txt_stringLotOrSerial;
    Toolbar toolbar;
    Button btnScan;
    SweetAlertDialog pd;
    JSONArray resultData;
    String jsonArray;
    DeliveryOrderDetails deliveryOrderDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_enquiry);

        intialize();


        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Balance Enquiry");


        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        // jsonArray = getIntent().getStringExtra("resultData");

        jsonArray = settings.getString("resultData", "");


        if (jsonArray != null) {

            Log.i("deliveryOrderDetails", "as");

            displayItemList();

//            try {
//
//                final JSONObject data = new JSONObject(jsonArray);
//
//                resultData = data.getJSONArray("result");
//
//
////                for (int i = 0; i < resultData.length(); i++) {
////                    JSONObject jsonObject2 = (JSONObject) resultData.get(i);
////                    deliveryOrderDetails = new DeliveryOrderDetails(jsonObject2);
////                    Log.i("part_no",""+ deliveryOrderDetails.getPart_no());
////                    Log.i("lot_no",""+ deliveryOrderDetails.getLot_no());
////                    Log.i("serial_no",""+ deliveryOrderDetails.getSerial_no());
////                    Log.i("expiry_date",""+ deliveryOrderDetails.getExpiry_date());
////                    Log.i("container_receipt",""+ deliveryOrderDetails.getContainer_receipt());
////                    Log.i("balance",""+ deliveryOrderDetails.getBalance());
////
////                    for(int m = 0; m < deliveryOrderDetails.getDeliveryOrderOutDetails().size() ; m ++){
////                        deliveryOrderOutDetails = deliveryOrderDetails.getDeliveryOrderOutDetails().get(m);
////                        Log.i("reference_noDO",""+ deliveryOrderOutDetails.getReference_no_DOrderDetail());
////                        Log.i("out_date",""+ deliveryOrderOutDetails.getHandling_out_date_DOrderDetail());
////                        Log.i("qty",""+ deliveryOrderOutDetails.getQuantity());
////                        Log.i("deliveryTo",""+ deliveryOrderOutDetails.getDelivery_to_DOrderDetail());
////                    }
////                }
//
//               // displayReferenceList(resultData);
//
//
//            } catch (Exception e) {
//
//            }

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        settings.edit().putString("resultData", "").commit();
        super.onBackPressed();
    }


    void intialize() {
        mLayoutList = findViewById(R.id.mLayoutList);
        txt_stringLotOrSerial = findViewById(R.id.txt_stringLotOrSerial);
        toolbar = findViewById(R.id.toolbar);
        btnScan = findViewById(R.id.btnScan);

        settings = this.getSharedPreferences(PREFS_NAME, 0);
        accessToken = settings.getString("access_token", "");
        name_scan_setting = settings.getString("name_scan_setting", "");

        if (name_scan_setting.equalsIgnoreCase("lot_no")) {
            txt_stringLotOrSerial.setText("LOT NO");
        } else if (name_scan_setting.equalsIgnoreCase("serial_no")) {
            txt_stringLotOrSerial.setText("SERIAL NO");
        }

        pd = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));

    }

    void displayItemList() {
        mLayoutList.removeAllViews();
        try {
            final JSONObject data = new JSONObject(jsonArray);

            resultData = data.getJSONArray("result");

            Log.i("orderDetailListLength", "" + resultData.length());

            if (resultData.length() > 0) {
                for (int i = 0; i < resultData.length(); i++) {

                    try {
                        JSONObject jsonObject2 = (JSONObject) resultData.get(i);
                        final DeliveryOrderDetails doDetails = new DeliveryOrderDetails(jsonObject2);

                        final View view = View.inflate(this, R.layout.view_layout_list_delivery_number, null);
                        mLayoutList.addView(view);
                        final TextView txt_partNo = view.findViewById(R.id.txt_partNo);
                        final TextView txt_lotNo = view.findViewById(R.id.txt_lotNo);
                        final TextView txt_balance = view.findViewById(R.id.txt_balance);
                        final LinearLayout layoutList_delivery = view.findViewById(R.id.layoutList_delivery);

                        if ((i % 2) == 0) {
                            layoutList_delivery.setBackgroundColor(getResources().getColor(R.color.bgColorOdd));
                        } else {
                            layoutList_delivery.setBackgroundColor(getResources().getColor(R.color.bgColorEven));
                        }

                        int totalBalance = doDetails.getQuantity() - doDetails.getQuantity_scanned();

                        txt_partNo.setText(doDetails.getPart_no());

                        if (name_scan_setting.equalsIgnoreCase("lot_no")) {
                            txt_lotNo.setText(doDetails.getLot_no());
                        } else if (name_scan_setting.equalsIgnoreCase("serial_no")) {
                            txt_lotNo.setText(doDetails.getSerial_no());
                        }


                        txt_balance.setText("" + totalBalance);

                        Log.i("doOutSize", "" + doDetails.getDeliveryOrderOutDetails().size());
                        Log.i("lotNo", "" + doDetails.getLot_no());
                        Log.i("SerialNo", "" + doDetails.getSerial_no());
                        Log.i("PartNo", "" + doDetails.getPart_no());
                        Log.i("ID", "" + doDetails.getId());


                        layoutList_delivery.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent i = new Intent(getApplicationContext(), BalanceEnquiryItemDetailsActivity.class);
                                i.putExtra("deliveryOrderDetails", doDetails);
                                Log.i("doOutSize1", "" + doDetails.getDeliveryOrderOutDetails().size());
                                startActivity(i);
                            }
                        });

                    } catch (Exception e) {
                        LogCustom.e(e);
                    }

                }
            }
        } catch (Exception e) {

        }
    }
}





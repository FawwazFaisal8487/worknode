package com.evfy.evfytracker.Activity.ErrorHandling;

import android.content.res.Resources;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ErrorHandlingResponseCode {

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public String[] errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode, final Resources getResources) {
        String errorTitle = getResources.getString(R.string.globalErrorMessageTitle);
        String errorMessage = getResources.getString(R.string.globalErrorMessageContent);
        JSONObject data = null;

        if (!requestCallBackError) {
            try {

                if (body != null || !body.equalsIgnoreCase("")) {

                    try {
                        data = new JSONObject(body);
                    } catch (Exception e) {
                        errorMessage = getResources.getString(R.string.globalErrorMessageTitle);
                        errorTitle = getResources.getString(R.string.globalErrorMessageContent);
                    }

                    if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                        if (data.has("error") && !(data.isNull("error"))) {
                            try {
                                errorMessage = data.getString("error");

                                if (errorMessage.equalsIgnoreCase("Couldn't find OrderStatus with 'id'=0")) {

                                }

                            } catch (Exception e) {
                                JSONObject errorMessageArray = data.getJSONObject("error");
                                errorMessage = errorMessageArray.getString("error");
                            }
                        }

                        if (errorMessage.equalsIgnoreCase("")) {
                            errorMessage = getResources.getString(R.string.globalErrorMessageContent);
                        }

                        errorTitle = getResources.getString(R.string.globalErrorMessageTitleSorry);


                    } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {
                        if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                            if (data.getBoolean("device_not_found")) {
                                errorTitle = getResources.getString(R.string.deviceNotFoundTitle);
                                errorMessage = getResources.getString(R.string.pleaseContactAdmin);
                            }
                        } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                            if (data.getBoolean("device_is_banned")) {
                                errorTitle = getResources.getString(R.string.deviceBannedTitle);
                                errorMessage = getResources.getString(R.string.pleaseContactAdmin);
                            }
                        } else {
                            errorTitle = getResources.getString(R.string.errorLogin);
                            errorMessage = getResources.getString(R.string.pleaseCheckPassword);
                        }
                    } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {
                        if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                            if (data.getBoolean("unpaid_subscription")) {
                                errorTitle = getResources.getString(R.string.unpaidSubscriptionTitle);
                                errorMessage = getResources.getString(R.string.pleaseContactAdmin);
                            }
                        } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                            if (data.getBoolean("quota_reached")) {
                                errorTitle = getResources.getString(R.string.quotaReachedTitle);
                                errorMessage = getResources.getString(R.string.pleaseContactAdmin);
                            }
                        } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                            if (data.getBoolean("blacklist")) {
                                errorTitle = getResources.getString(R.string.blacklistTitle);
                                errorMessage = getResources.getString(R.string.pleaseContactAdmin);
                            }
                        } else {
                            errorTitle = getResources.getString(R.string.errorLogin);
                            errorMessage = getResources.getString(R.string.pleaseCheckPassword);
                        }
                    } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                        errorTitle = getResources.getString(R.string.globalErrorMessageTitleSorry);
                        errorMessage = getResources.getString(R.string.globalErrorMessageContent);

                    } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                        errorTitle = getResources.getString(R.string.globalErrorMessageTitleSorry);
                        errorMessage = getResources.getString(R.string.globalErrorMessageContent);

                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        errorTitle = getResources.getString(R.string.globalErrorMessageTitleSorry);
                        errorMessage = getResources.getString(R.string.globalErrorMessageContent);
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        errorTitle = getResources.getString(R.string.globalErrorMessageTitleSorry);
                        errorMessage = getResources.getString(R.string.globalErrorTimeoutMessageContent);
                    }

                } else {
                    errorTitle = getResources.getString(R.string.globalErrorMessageTitle);
                    errorMessage = getResources.getString(R.string.globalErrorMessageContent);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                errorTitle = getResources.getString(R.string.globalErrorMessageTitleSorry);
                errorMessage = getResources.getString(R.string.globalErrorMessageContent);
            } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                //   showErrorNoInternetDialog().show();
            } else {
                //   showErrorDialog(getResources().getString(R.string.serverErrorTitle),getResources().getString(R.string.pleaseContactAdmin)).show();
            }
            // this for request call back error. Maybe because cannot connect server.
        }

        String[] errorMessageHandling = new String[2];
        errorMessageHandling[0] = errorTitle;
        errorMessageHandling[1] = errorMessage;
        return errorMessageHandling;

    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */

}

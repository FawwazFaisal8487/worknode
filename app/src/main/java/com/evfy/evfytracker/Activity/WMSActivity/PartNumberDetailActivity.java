package com.evfy.evfytracker.Activity.WMSActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.DeliveryOrderDetails;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.juaagugui.httpService.RestManagerFactory;
import app.juaagugui.httpService.exceptions.NoInternetConnectionException;
import app.juaagugui.httpService.listeners.OnHttpEventListener;
import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;
import app.juaagugui.httpService.services.RESTIntentService;
import app.juaagugui.httpService.services.RestManager;

public class PartNumberDetailActivity extends AppCompatActivity implements OnRESTResultCallback, OnHttpEventListener {

    String accessToken;
    HttpConnection connection;
    private RestManager restManager;
    SharedPreferences settings;
    static final String PREFS_NAME = "MyPrefsFile";
    TextView txt_partNumber, txt_lotNumber, txt_quantity, txt_serialNumber, txt_remarks, txt_location, txt_quantityScanned;
    String lot_no, part_no, referenceNumber;
    int companyId;
    JSONArray orderDetails;
    DeliveryOrderDetails deliveryOrderDetails;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_part_number_detail);

        initialize();
//        part_no = getIntent().getExtras().getString("part_no");
//        lot_no = getIntent().getExtras().getString("lot_no");
//        referenceNumber = getIntent().getExtras().getString("referenceNumber");
//        companyId = getIntent().getExtras().getInt("companyId");

        deliveryOrderDetails = (DeliveryOrderDetails) getIntent().getSerializableExtra("deliveryOrderDetails");


        // getDetailListDO();

        showDetails();


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    void initialize() {
        restManager = RestManagerFactory.createRestManagerWithHttpEventListener(PartNumberDetailActivity.this, this);
        settings = this.getSharedPreferences(PREFS_NAME, 0);
        accessToken = settings.getString("access_token", "");

        txt_partNumber = findViewById(R.id.txt_partNumber);
        txt_lotNumber = findViewById(R.id.txt_lotNumber);
        txt_quantity = findViewById(R.id.txt_quantity);
        txt_quantityScanned = findViewById(R.id.txt_quantityScanned);
        txt_serialNumber = findViewById(R.id.txt_serialNumber);
        txt_remarks = findViewById(R.id.txt_remarks);
        txt_location = findViewById(R.id.txt_location);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        // action bar for app
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void onRequestInit() {

    }

    @Override
    public void onRequestFinish() {

    }

    void showDetails() {
        txt_partNumber.setText(deliveryOrderDetails.getPart_no());
        txt_lotNumber.setText(deliveryOrderDetails.getLot_no());
        txt_location.setText(deliveryOrderDetails.getLocation());
        txt_remarks.setText(deliveryOrderDetails.getRemarks());
        txt_quantity.setText("" + deliveryOrderDetails.getQuantity());
        txt_quantityScanned.setText("" + deliveryOrderDetails.getQuantity_scanned());
        txt_serialNumber.setText(deliveryOrderDetails.getSerial_no());

        //setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        this.getSupportActionBar().setTitle(deliveryOrderDetails.getPart_no());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRESTResult(int returnCode, int code, String result) {
        LogCustom.i(result, "resultgetReference");
        LogCustom.i(code, "code");

        if (returnCode == 2) {
            try {
                final JSONObject data = new JSONObject(result);
                orderDetails = data.getJSONArray("order_details");
                // ArrayList <String> listOfApp = new ArrayList<String>();
                for (int m = 0; m < orderDetails.length(); m++) {

                    JSONObject orderDetailsListJson = (JSONObject) orderDetails.get(m);
                    deliveryOrderDetails = new DeliveryOrderDetails(orderDetailsListJson);
                    LogCustom.i(deliveryOrderDetails.getPart_no(), "partNo delivery");
                }

                showDetails();
            } catch (Exception e) {
                LogCustom.e(e);
                Log.i("crash", "crash");
//                Crashlytics.log(returnCode, "getDetailListDOResult", result);
//                Crashlytics.logException(e);
            }
        }

    }

    public void getDetailListDO() {

        try {

            JSONObject jsonObject = new JSONObject();
            connection = new HttpConnection(Constants.SAAS_SERVER_WMS + "/worker/api/scan/do_out?customer_id=" + companyId + "&reference_no=" + referenceNumber + "&part_no=" + part_no + "&lot_not" + lot_no, RESTIntentService.GET, jsonObject, this);

            List<Pair<String, String>> header = new ArrayList<Pair<String, String>>();

            Pair<String, String> pair = Pair.create("Authorization", "Bearer " + accessToken);
            header.add(pair);
            connection.setHeaders(header);

            Log.i("", "access token:" + accessToken);
            Log.i("connectGetReferenceNo", " " + connection.toString());

            restManager.sendRequestWithReturn(2, connection);
        } catch (Exception e) {

            if (e instanceof NoInternetConnectionException) {

            } else {

                try {
//                    Crashlytics.logException(e);
                } catch (final Throwable t) {
                }

            }

        }

    }
}

package com.evfy.evfytracker.Activity.HomeActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.AppCompanySettings;
import com.evfy.evfytracker.classes.OrderStatus;
import com.facebook.stetho.Stetho;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.lkh012349s.mobileprinter.Utils.AndroidOp;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

//import com.google.firebase.iid.FirebaseInstanceId;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


	/* ================== DECLARATION ================== */
	//private AutoCompleteTextView mEmailView;
	private TextInputEditText mPasswordView, mEmailView;
	private TextInputLayout input_layout_password, input_layout_username;
	Button mEmailSignInButton;
	private View mProgressView;
	private View mLoginFormView;
	SweetAlertDialog pd;
	final String PREFS_NAME = "MyPrefsFile";
	boolean isWMS = false;
	String firstName;
	SharedPreferences settings;
	Boolean errorBool = null;
	String error = "";
	boolean haveScannedStatus;
	DatabaseHandlerJobs databaseHandlerJobs;
	private static LoginActivity instance;

	AnimationDrawable animationDrawable;
	LinearLayout linearLayout, layoutBelowPassword, layoutMessageForgetPassword;
	String googleToken = "";
	TextView txt_versionNumber, txt_forgetPassword, txt_adminLink;
	Dialog pdUpdateProcess;
	TextView contextProgressDialog;
	String username, verification_code, password, userId;
	boolean forgetPassword, isConfirmationCode = false;
	DatabaseHandlerJobs dbUpdateJobs;

	/* ================== DECLARATION ================== */


	/* ============================== LOGIN PROCESS =============================== */
	public void attemptLogin() {

		if (JavaOp.ifNullThenLog(mEmailView, mPasswordView)) return;

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		input_layout_username.setError(null);
		input_layout_password.setError(null);

		// Store values at the time of the login attempt.
		String userId = mEmailView.getText().toString().trim();
		String password = mPasswordView.getText().toString();


		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (TextUtils.isEmpty(password)) {
			input_layout_password.setError(getString(R.string.error_field_required));
			input_layout_password.requestFocus();
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(userId)) {
			input_layout_username.setError(getString(R.string.error_field_required));
			input_layout_username.requestFocus();
			focusView = mEmailView;
			cancel = true;
		}


		//        else if (!isEmailValid(userId)) {
		//            mEmailView.setError(getString(R.string.error_invalid_email));
		//            focusView = mEmailView;
		//            cancel = true;
		//        }

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			//focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.

			forgetPassword = false;
			login();

		}
	}
	/* ============================== LOGIN PROCESS =============================== */


	public void login() {
		try {

			userId = mEmailView.getText().toString().trim();
			String password = mPasswordView.getText().toString();


			String usernameTemp = settings.getString("usernameTemporary", "");
			if (!usernameTemp.equalsIgnoreCase("") && forgetPassword) {
				userId = settings.getString("usernameTemporary", "");
			}


			JSONObject jsonObject = new JSONObject();
			//userId = URLEncoder.encode( userId, "UTF-8" );  // this for convert space or character in url
			//password = URLEncoder.encode( password, "UTF-8" );

			//googleToken="";
//			try{
//
//				//Log.i("tokenObject","tokenObject");
//				JSONObject tokenObject = new JSONObject(FirebaseInstanceId.getInstance().getToken());
//				googleToken = tokenObject.getString("token");
//				Log.i("tokenObject",googleToken);
//
//
//			}catch (Exception e)
//			{
//				googleToken = FirebaseInstanceId.getInstance().getToken();
//			}

			//This just to make sure google token cannot be null.
			if (!settings.contains("googleToken")) {
				if (!googleToken.equalsIgnoreCase("")) {
					SharedPreferences.Editor editors = settings.edit();
					editors.putString("googleToken", googleToken).commit();
					editors.apply();
				}
			} else {
				if (googleToken.equalsIgnoreCase("")) {
					googleToken = settings.getString("googleToken", "");
				}
			}

			Log.i("", "google firebase token 22:" + googleToken);

			String android_id = Settings.Secure.getString(getContentResolver(),
					Settings.Secure.ANDROID_ID);

			String deviceModel = android.os.Build.MODEL;
			String deviceManufacturer = android.os.Build.MANUFACTURER;
			Log.i("deviceModel :", "" + deviceModel);
			Log.i("deviceManufacturer :", "" + deviceManufacturer);

			Log.i("android_id", "is : " + android_id);

			//jsonObject.put("username",userId).put("password",password).put("scope","worker").put("grant_type","password").put("google_token",token).put("require_design_settings",false);
			jsonObject.put("email", userId)
					.put("password", password)
					.put("scope", "worker")
					.put("grant_type", "password")
					//	.put("device_id","b4ac12952a2057e0")
					.put("device_id", android_id)
					.put("google_token", googleToken)
					.put("device_manufacturer", deviceManufacturer)
					.put("device_model", deviceModel)
					.put("device_platform", "Android")
					.put("require_design_settings", false);

			OkHttpRequest okHttpRequest = new OkHttpRequest();

			showProgress();

			try {
				OkHttpRequest.driverLogin(getApplicationContext(), jsonObject.toString()
						, new OkHttpRequest.OKHttpNetwork() {
							@Override
							public void onSuccess(final String body, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										try {

											final JSONObject data = new JSONObject(body);

											if (data.has("status") && !(data.isNull("status"))) {
												if (!data.getBoolean("status")) {
													dimissProgress();
													showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.errorMessageUsernamePassword)).show();
												} else {
													JSONObject tokenData = data.getJSONObject("token");

													SharedPreferences.Editor editors = settings.edit();
													editors.putString("user_name", userId).commit();
													editors.putString("password", mPasswordView.getText().toString()).commit();
													editors.putString("access_token", tokenData.getString("access_token")).commit();
													editors.putString("refresh_token", tokenData.getString("refresh_token")).commit();
													editors.putString("googleToken", googleToken).commit();
													editors.apply();

//
//														final JSONArray inTokenData = data.getJSONArray("lds_order_statuses");
//														for(int i =0; i<inTokenData.length();i++){
//															JSONObject jsonObject2 = (JSONObject) inTokenData.get(i);
//															String ldsStatusName = jsonObject2.getString("status");
//															Log.i("ldsStatus",""+ldsStatusName);
//
//															if(ldsStatusName.equalsIgnoreCase("Scanned")){
//																haveScannedStatus = true;
//																break;
//															}else{
//																haveScannedStatus = false;
//															}
//														}
//
//														settings.edit().putBoolean( "haveScannedStatus", haveScannedStatus ).commit();

													JSONObject resultData = data.getJSONObject("result");
													editors.putString("company_name", resultData.getString("company_name")).commit();
													editors.apply();

													errorBool = false;

													JSONArray orderStatusListing = data.getJSONArray("lds_order_statuses");

													Log.i("orderStatus", "array" + orderStatusListing.length());

													databaseHandlerJobs.deleteAllDataOrderStatus();

													for (int i = 0; i < orderStatusListing.length(); i++) {
														JSONObject orderStatusJsonObject = (JSONObject) orderStatusListing.get(i);
														OrderStatus orderStatus = new OrderStatus(orderStatusJsonObject);
														databaseHandlerJobs.updateListingOrderStatus(orderStatus);

													}

													getProfileDetail();
												}
											}


										} catch (JSONException e) {

											LogCustom.e(e);

											try {
//												Crashlytics.log(0, "Login", body);
//												Crashlytics.logException(e);
											} catch (final Throwable t) {
											}

										}


									}
								});
							}

							@Override
							public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
								dimissProgress();
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
									}
								});
							}
						});
			} catch (Exception e) {
				dimissProgress();
				e.printStackTrace();
			}
		} catch (Exception e) {

		}


	}


	/* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
	public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (!requestCallBackError) {
					try {
						String errorMessage = "";
						JSONObject data = null;
						if (body != null || !body.equalsIgnoreCase("")) {

							try {
								data = new JSONObject(body);
							} catch (Exception e) {
								showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
							}

							if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
								if (data.has("error") && !(data.isNull("error"))) {
									try {
										if (forgetPassword && !isConfirmationCode) {
											//errorMessage = data.getString("error");
											errorMessage = getResources().getString(R.string.usernameNotExist);
										} else {
											errorMessage = getResources().getString(R.string.errorMessageUsernamePassword);
										}
									} catch (Exception e) {
										JSONObject errorMessageArray = data.getJSONObject("error");
										errorMessage = errorMessageArray.getString("error");
									}
								}

								if (errorMessage.equalsIgnoreCase("")) {
									errorMessage = getResources().getString(R.string.pleaseContactAdmin);
								}

								showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


							} else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {
								if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
									if (data.getBoolean("device_not_found")) {
										showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
									}
								} else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
									if (data.getBoolean("device_is_banned")) {
										showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
									}
								} else {
									showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
								}
							} else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {
								if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
									if (data.getBoolean("unpaid_subscription")) {
										showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
									}
								} else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
									if (data.getBoolean("quota_reached")) {
										showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
									}
								} else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
									if (data.getBoolean("blacklist")) {
										showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
									}
								} else {
									showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
								}
							} else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
								showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

							} else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
								showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

							} else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
								showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
							} else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
								showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
							}

						} else {
							showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
						}


					} catch (JSONException e) {
						e.printStackTrace();
					}
				} else {
					if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
						showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
					} else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
						showErrorNoInternetDialog().show();
					} else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
						showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
					} else {
						showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
					}
					// this for request call back error. Maybe because cannot connect server.
				}
			}
		});


	}
	/* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */


	/* =============================== WORKER DETAILS ================================ */
	void getProfileDetail() {

		JSONObject jsonObject = new JSONObject();
		OkHttpRequest okHttpRequest = new OkHttpRequest();

		try {
			OkHttpRequest.profileWorker(getApplicationContext(), jsonObject.toString()
					, new OkHttpRequest.OKHttpNetwork() {
						@Override
						public void onSuccess(final String body, final int responseCode) {
							dimissProgress();
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									{

										boolean isError = false;
										try {
											final JSONObject data = new JSONObject(body);

											final JSONObject inData = data.getJSONObject("result");
											Log.i("getName", "" + inData.getString("first_name"));
											firstName = inData.getString("first_name");
											int resource_owner_id = Integer.parseInt(inData.getString("id"));
											Log.i("resource_owner_id", "" + resource_owner_id);
											boolean isTruckDriver = inData.getBoolean("is_truck_driver");
											boolean isManPower = inData.getBoolean("is_man_power");
											Log.i("isTruckDriver", "" + isTruckDriver);

											SharedPreferences.Editor editors = settings.edit();
											editors.putBoolean("isTruckDriver", isTruckDriver).commit();
											editors.putBoolean("isManPower", isManPower).commit();
											editors.putInt("resource_owner_id", resource_owner_id).commit();
											editors.putBoolean("isOnline", true).commit();
											editors.putBoolean("firstTimeUpdateLocation", true).commit();
											editors.apply();


											final JSONArray inTokenData = inData.getJSONArray("application_registrations");

											// ArrayList <String> listOfApp = new ArrayList<String>();
											for (int i = 0; i < inTokenData.length(); i++) {
												JSONObject jsonObject2 = (JSONObject) inTokenData.get(i);
												String nameOfApp = jsonObject2.getString("name");
												//     listOfApp.add(nameOfApp);
												Log.i("nameAPP", "" + nameOfApp);

												if (nameOfApp.equalsIgnoreCase("WMS")) {
													//show
													isWMS = true;
													editors.putBoolean("isWMS", isWMS).commit();
													editors.apply();
													break;
												}
											}

										} catch (JSONException e) {

											LogCustom.i(body, "result");
											LogCustom.e(e);

											isError = true;
											final Exception exception = new Exception(body);

											try {
//												Crashlytics.log(1, "After Login", body);
//												Crashlytics.logException(exception);
											} catch (final Throwable t) {
											}

										}

										if (!isError) {
											getAppCompanySettings();

//											Crashlytics.setUserName(firstName);
//											Crashlytics.setUserEmail(settings.getString( "user_name", "" ));
//											Intent intent = new Intent( LoginActivity.this, DrawerActivity.class );
//											intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
//											LoginActivity.this.startActivity( intent );
//											LoginActivity.this.overridePendingTransition( R.anim.push_left_in, R.anim.push_left_in );
//											finish();
										}

									}

								}
							});
						}

						@Override
						public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
							dimissProgress();
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
								}
							});
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	/* =============================== WORKER DETAILS ================================ */


	/*
	 *  This function is to get company setting
	 *  To check which status need to scan and manpower can update job for this company
	 * */
	/* ================================ APP COMPANY SETTINGS ================================== */
	void getAppCompanySettings() {

		JSONObject jsonObject = new JSONObject();
		OkHttpRequest okHttpRequest = new OkHttpRequest();

		try {
			OkHttpRequest.appCompanySetting(getApplicationContext(), jsonObject.toString()
					, new OkHttpRequest.OKHttpNetwork() {
						@Override
						public void onSuccess(final String body, final int responseCode) {
							dimissProgress();
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									{

										try {
											final JSONObject data = new JSONObject(body);

											JSONArray result = data.getJSONArray("result");

											for (int i = 0; i < result.length(); i++) {
												JSONObject jsonObject = (JSONObject) result.get(i);

												if (jsonObject.has("group_name") && !(jsonObject.isNull("group_name")) && jsonObject.getString("group_name").equalsIgnoreCase("mobile")) {
													LogCustom.i("ruleMobile", jsonObject.getString("rule"));
													AppCompanySettings appCompanySettings = new AppCompanySettings(jsonObject);
													databaseHandlerJobs.updateAppCompanySettings(appCompanySettings);
												}


											}

//											Crashlytics.setUserName(firstName);
//											Crashlytics.setUserEmail(settings.getString("user_name", ""));
											Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
											intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
											LoginActivity.this.startActivity(intent);
											LoginActivity.this.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_in);
											finish();


										} catch (JSONException e) {

											LogCustom.i(body, "result");


										}

									}

								}
							});
						}

						@Override
						public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
							dimissProgress();
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if (!requestCallBackError) {
										try {
											final JSONObject data = new JSONObject(body);
											if (data.has("error") || !data.isNull("error")) {
												error = data.getString("error");
												showErrorDialog("Error", error).show();
											}
										} catch (JSONException e) {
											e.printStackTrace();
										}
									} else {
										// this for request call back error. Maybe because cannot connect server.
										// Maybe cannot connect with server occur
										if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
											showErrorNoInternetDialog().show();
										} else {
											showErrorDialog("Error Server", body).show();
										}
									}


								}
							});
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}


	}
	/* ================================ APP COMPANY SETTINGS ================================== */


	/*
	 * This is for aleart dialog.
	 * */
	/* ================================ SWEET ALERT DIALOG =================================== */

	SweetAlertDialog showErrorDialog(String titleError, String contextError) {
		final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
		sweetAlertDialog.show();
		return sweetAlertDialog;
	}

	SweetAlertDialog getErrorDialog(String titleError, String contextError) {
		final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
				.setContentText(contextError)
				.setTitleText(titleError)
				.setConfirmText("OK")
				.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismissWithAnimation();
					}
				});


		sweetAlertDialog.setCancelable(false);
		return sweetAlertDialog;
	}


	void dimissProgress() {
		try {
			if ((pd != null) && pd.isShowing()) {
				pd.dismiss();
			}
		} catch (final IllegalArgumentException e) {
			// Handle or log or ignore
		} catch (final Exception e) {
			// Handle or log or ignore
		}
	}

	void showProgress() {
		try {
			pd.show();
		} catch (final Throwable e) {
			LogCustom.e(e);
		}
	}


	SweetAlertDialog showErrorNoInternetDialog() {
		final SweetAlertDialog sweetAlertDialog = getErrorNoInternetDialog();
		sweetAlertDialog.show();
		return sweetAlertDialog;
	}

	SweetAlertDialog getErrorNoInternetDialog() {
		final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
				.setContentText("Your device not connect with internet. Please check your internet connection")
				.setTitleText("No internet connection")
				.setConfirmText("Open settings")
				.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismissWithAnimation();
						final Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS, null);
						intent.addCategory(Intent.CATEGORY_LAUNCHER);
						intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(intent);
					}
				})
				.setCancelText("Cancel")
				.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					@Override
					public void onClick(SweetAlertDialog sDialog) {
						sDialog.dismissWithAnimation();
					}
				});


		sweetAlertDialog.setCancelable(false);
		return sweetAlertDialog;
	}

	/* ================================ SWEET ALERT DIALOG =================================== */


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
			@Override
			public void onComplete(@NonNull Task<InstanceIdResult> task) {
				if (!task.isSuccessful()) {
					Log.w("FIREBASE", "Fetching FCM registration token failed", task.getException());
					return;
				}

				// Get new FCM registration token
				String token = task.getResult().getToken();

				// Log and toast
				//String msg = getString(R.string.msg_token_fmt, token);
				Log.d("FIREBASE", token);

				googleToken = token;
			}
		});

		pd = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
		pd.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		pd.setTitleText("Signing In");
		pd.setCancelable(false);

		pdUpdateProcess = new Dialog(this);
		LayoutInflater inflater = getLayoutInflater();
		View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
		contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
		pdUpdateProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
		pdUpdateProcess.setContentView(content);
		pdUpdateProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		pdUpdateProcess.setCancelable(false);

		settings = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);

		try {
			setContentView(R.layout.activity_login);
		} catch (final Throwable e) {
			LogCustom.e(e);
			AndroidOp.restartApplication(this);
			return;
		}

		Stetho.initializeWithDefaults(this);

		//	linearLayout = (LinearLayout) findViewById(R.id.backgroundLogin);
		txt_versionNumber = findViewById(R.id.txt_versionNumber);
		txt_forgetPassword = findViewById(R.id.txt_forgetPassword);
		txt_adminLink = findViewById(R.id.admin_operation_link);
		forgetPassword = false;


		//login_gradient_first
		//login_gradient_second

//		AnimationDrawable animationDrawable =(AnimationDrawable)linearLayout.getBackground();
//		animationDrawable.setEnterFadeDuration(5000);
//		animationDrawable.setExitFadeDuration(2000);
//		animationDrawable.start();


//		Fabric.with(this, new Crashlytics());

//		Bundle params = new Bundle();
//		params.putString("nama", "akmal");
//		params.putString("umur", "17");
//		firebaseAnalytics.logEvent("info_diri", params);


		Context context = getApplicationContext(); // or activity.getApplicationContext()
		PackageManager packageManager = context.getPackageManager();
		String packageName = context.getPackageName();

		String myVersionName = "Not Available"; // initialize String
		try {
			myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}

		txt_versionNumber.setText("Version " + myVersionName);


		// Set up the login form.
		mEmailView = findViewById(R.id.email);
		// populateAutoComplete();

		mPasswordView = findViewById(R.id.password);

		input_layout_password = findViewById(R.id.input_layout_password);
		input_layout_username = findViewById(R.id.input_layout_username);
		mEmailSignInButton = findViewById(R.id.email_sign_in_button);
		layoutBelowPassword = findViewById(R.id.layoutBelowPassword);
		layoutMessageForgetPassword = findViewById(R.id.layoutMessageForgetPassword);


		//checking android version for change textcolor
//		if (android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
//			// only for JELLY_BEAN and newer versions
//			mEmailView.setTextColor(getResources().getColor(R.color.black));
//			mPasswordView.setTextColor(getResources().getColor(R.color.black));
//
//			LogCustom.i("JELLY_BEAN","YES");
//		}

		mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				//attemptLogin();
				return id == R.id.login || id == EditorInfo.IME_NULL;
			}

		});


		mEmailSignInButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				isConfirmationCode = false;

				GeneralActivity generalActivity = new GeneralActivity();
				if (!generalActivity.haveInternetConnected(getApplicationContext())) {
					showErrorNoInternetDialog().show();
				} else {
					if (mEmailSignInButton.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.resetPasswordButton))) {
						LogCustom.i("validate username", "yes");
						input_layout_username.setError(null);
						if (mEmailView.getText().toString().trim().equalsIgnoreCase("")) {
							input_layout_username.setError(getString(R.string.error_field_required));
							input_layout_username.requestFocus();
						} else {
							checkValidateEmail();
						}

					} else if (mEmailSignInButton.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.signInButton))) {
						LogCustom.i("signInButton", "yes");
						attemptLogin();
					} else if (mEmailSignInButton.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.setNewPasswordButton))) {
						LogCustom.i("resetPassword", "yes");

						input_layout_username.setError(null);
						input_layout_password.setError(null);
						if (mEmailView.getText().toString().trim().equalsIgnoreCase("")) {
							input_layout_username.setError(getString(R.string.error_field_required));
							input_layout_username.requestFocus();
						} else if (mPasswordView.getText().toString().trim().equalsIgnoreCase("")) {
							input_layout_password.setError(getString(R.string.error_field_required));
							input_layout_password.requestFocus();
						} else {
							confirmCode();
						}
					}
				}


			}

		});

		mLoginFormView = findViewById(R.id.login_form);
		mProgressView = findViewById(R.id.login_progress);


		/* ===== DATABASE INITIALISE ====== */
		databaseHandlerJobs = new DatabaseHandlerJobs(this);
		boolean isTableExist = databaseHandlerJobs.isTableExists("AppCompanySettings", true);

		if (isTableExist == false) {
			databaseHandlerJobs.onUpgrade(databaseHandlerJobs.getWritableDatabase(), databaseHandlerJobs.getWritableDatabase().getVersion(), databaseHandlerJobs.getWritableDatabase().getVersion() + 1);
		}
		/* ===== DATABASE INITIALISE ====== */

		txt_adminLink.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Uri webpage = Uri.parse("http://auth.taskk.me/login");
				Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
				if (intent.resolveActivity(getPackageManager()) != null) {
					startActivity(intent);
				} else {
					//Page not found
				}
			}
		});

		txt_forgetPassword.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (!forgetPassword) {
					forgetPassword = true;
					input_layout_password.setVisibility(View.GONE);
					SpannableString content = new SpannableString(getResources().getString(R.string.signIn));
					content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
					txt_forgetPassword.setText(content);
					mEmailSignInButton.setText(getResources().getString(R.string.resetPasswordButton));
					layoutBelowPassword.setVisibility(View.VISIBLE);
					layoutMessageForgetPassword.setVisibility(View.VISIBLE);
					//input_layout_username.setPadding(30,60,30,0);

				} else {
					forgetPassword = false;
					input_layout_password.setVisibility(View.VISIBLE);
					SpannableString content = new SpannableString(getResources().getString(R.string.forgetPassword));
					content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
					txt_forgetPassword.setText(content);
					//txt_forgetPassword.setText(Html.fromHtml(getResources().getString(R.string.forgetPassword)));
					mEmailSignInButton.setText(getResources().getString(R.string.signInButton));
					input_layout_username.setHint(getResources().getString(R.string.username));
					layoutBelowPassword.setVisibility(View.GONE);
					layoutMessageForgetPassword.setVisibility(View.GONE);
					//	input_layout_username.setPadding(30,80,30,0);

				}

			}
		});

		SharedPreferences.Editor editors = settings.edit();
		editors.putBoolean("insideFragmentPage", false).commit();
		editors.putBoolean("insideJobDetails", false).commit();
		editors.putBoolean("insideNotificationListing", false).commit();
		editors.apply();


		dbUpdateJobs = new DatabaseHandlerJobs(this);

		dbUpdateJobs.deleteAllDataNotification();
		dbUpdateJobs.deleteAllTable();
		dbUpdateJobs.deleteAllDataAppCompanySettings();
		Intent ii = new Intent(DrawerActivity.ACTION_REFRESH_NOTIFICATION_SIDE_MENU);
		LocalBroadcastManager.getInstance(getApplicationContext())
				.sendBroadcast(ii);

	}

	private boolean isEmailValid(String email) {
		//TODO: Replace this with your own logic
		return email.contains("@");
	}

//	private boolean isPasswordValid ( String password ) {
//		//TODO: Replace this with your own logic
//		return password.length() >= 6;
//	}

	private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
		//Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoginActivity.this, android.R.layout.simple_dropdown_item_1line,
				emailAddressCollection);

		//mEmailView.setAdapter( adapter );
	}

	@Override
	public void onBackPressed() {
		Log.i("Click Exit Button", "yes");
		finishAffinity();
		super.onBackPressed();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (animationDrawable != null && !animationDrawable.isRunning())
			animationDrawable.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (animationDrawable != null && animationDrawable.isRunning())
			animationDrawable.stop();
	}


	/* ===== FORGET PASSWORD ====== */

	public void checkValidateEmail() {
		try {
			JSONObject jsonObject = new JSONObject();


			username = mEmailView.getText().toString().trim();


			jsonObject.put("username", username);

			contextProgressDialog.setText(getResources().getString(R.string.validateUsername));
			pdUpdateProcess.show();

			OkHttpRequest okHttpRequest = new OkHttpRequest();

			try {
				OkHttpRequest.validateUsername(getApplicationContext(), jsonObject.toString()
						, new OkHttpRequest.OKHttpNetwork() {
							@Override
							public void onSuccess(final String body, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {

										if (body == null || body.isEmpty()) {

											if (pdUpdateProcess != null) {
												if (pdUpdateProcess.isShowing()) {
													pdUpdateProcess.dismiss();
												}
											}


											LogCustom.i("valueIsNull", "yes");
											showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
										} else {

											try {
												final JSONObject data = new JSONObject(body);
												if (data.has("status") && !(data.isNull("status"))) {
													if (data.getBoolean("status")) {
														// user exist
														resetPassword(username);

													} else {
														if (pdUpdateProcess != null) {
															if (pdUpdateProcess.isShowing()) {
																pdUpdateProcess.dismiss();
															}
														}

														showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.usernameNotExist)).show();
													}
												} else {
													if (pdUpdateProcess != null) {
														if (pdUpdateProcess.isShowing()) {
															pdUpdateProcess.dismiss();
														}
													}

													showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.usernameNotExist)).show();
												}
											} catch (JSONException e) {
												if (pdUpdateProcess != null) {
													if (pdUpdateProcess.isShowing()) {
														pdUpdateProcess.dismiss();
													}
												}

												e.printStackTrace();
											}

										}

									}
								});


							}

							@Override
							public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (pdUpdateProcess != null) {
											if (pdUpdateProcess.isShowing()) {
												pdUpdateProcess.dismiss();
											}
										}

										errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
									}
								});


							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
		}
	}

	public void resetPassword(final String username) {
		try {
			JSONObject jsonObject = new JSONObject();


			jsonObject.put("username", username);

			//contextProgressDialog.setText(getResources().getString(R.string.resetPassword));
			//pdUpdateProcess.show();

			OkHttpRequest okHttpRequest = new OkHttpRequest();

			try {
				OkHttpRequest.forgetPassword(getApplicationContext(), jsonObject.toString()
						, new OkHttpRequest.OKHttpNetwork() {
							@Override
							public void onSuccess(final String body, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (pdUpdateProcess != null) {
											if (pdUpdateProcess.isShowing()) {
												pdUpdateProcess.dismiss();
											}
										}

										if (body == null || body.isEmpty()) {
											LogCustom.i("valueIsNull", "yes");
											showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
										} else {

											try {
												final JSONObject data = new JSONObject(body);
												if (data.has("status") && !(data.isNull("status"))) {
													if (data.getBoolean("status")) {
														// user exist
														forgetPassword = true;
														input_layout_password.setVisibility(View.VISIBLE);
														mEmailSignInButton.setText(getResources().getString(R.string.setNewPasswordButton));
														input_layout_username.setHint(getResources().getString(R.string.verificationCode));
														mEmailView.setText("");
														layoutMessageForgetPassword.setVisibility(View.GONE);
														layoutBelowPassword.setVisibility(View.GONE);

														SharedPreferences.Editor editors = settings.edit();
														editors.putString("usernameTemporary", username).commit();
														editors.apply();

													} else {
														showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.usernameNotExist)).show();
													}
												} else {
													showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.usernameNotExist)).show();
												}
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}

									}
								});


							}

							@Override
							public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (pdUpdateProcess != null) {
											if (pdUpdateProcess.isShowing()) {
												pdUpdateProcess.dismiss();
											}
										}

										errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
									}
								});


							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
		}
	}


	public void confirmCode() {
		try {

			verification_code = mEmailView.getText().toString().trim();
			password = mPasswordView.getText().toString().trim();

			JSONObject jsonObject = new JSONObject();


			jsonObject.put("verification_code", verification_code);
			jsonObject.put("password", password);
			jsonObject.put("password_confirmation", password);

			//contextProgressDialog.setText(getResources().getString(R.string.resetPassword));
			//pdUpdateProcess.show();

			OkHttpRequest okHttpRequest = new OkHttpRequest();

			try {
				OkHttpRequest.resetPassword(getApplicationContext(), jsonObject.toString()
						, new OkHttpRequest.OKHttpNetwork() {
							@Override
							public void onSuccess(final String body, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (pdUpdateProcess != null) {
											if (pdUpdateProcess.isShowing()) {
												pdUpdateProcess.dismiss();
											}
										}

										if (body == null || body.isEmpty()) {
											LogCustom.i("valueIsNull", "yes");
											showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
										} else {

											try {
												final JSONObject data = new JSONObject(body);
												if (data.has("status") && !(data.isNull("status"))) {
													if (data.getBoolean("status")) {
														// user exist

														forgetPassword = true;

														String usernameTemp = settings.getString("usernameTemporary", "");
														if (!usernameTemp.equalsIgnoreCase("") && forgetPassword) {
															login();
														} else {
															forgetPassword = false;
															input_layout_password.setVisibility(View.VISIBLE);
															SpannableString content = new SpannableString(getResources().getString(R.string.forgetPassword));
															content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
															txt_forgetPassword.setText(content);
															//txt_forgetPassword.setText(Html.fromHtml(getResources().getString(R.string.forgetPassword)));
															mEmailSignInButton.setText(getResources().getString(R.string.signInButton));
															input_layout_username.setHint(getResources().getString(R.string.username));
															layoutBelowPassword.setVisibility(View.GONE);
															layoutMessageForgetPassword.setVisibility(View.GONE);
														}


													} else {
														showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.confirmationCodeInvalid)).show();
													}
												} else {
													showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.confirmationCodeInvalid)).show();
												}
											} catch (JSONException e) {
												e.printStackTrace();
											}

										}

									}
								});


							}

							@Override
							public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										if (pdUpdateProcess != null) {
											if (pdUpdateProcess.isShowing()) {
												pdUpdateProcess.dismiss();
											}
										}

										isConfirmationCode = true;

										errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
									}
								});


							}
						});
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
		}
	}

	/* ===== FORGET PASSWORD ====== */

}


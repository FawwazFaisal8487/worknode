package com.evfy.evfytracker.Activity.OnBoarding;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity;
import com.evfy.evfytracker.R;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

public class PermissionOnBoardingActivity extends AppCompatActivity {

    Button btnGrantPermission;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.permission_on_boarding_activity);


        btnGrantPermission = findViewById(R.id.btnGrantPermission);


        btnGrantPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.CAMERA}, 123);
                } else {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 456);
                    } else {
                        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 789);
                        } else {
                            // done
                            openLoginPage();
                        }
                    }
                }


            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        LogCustom.i("permission", "resultCode" + requestCode);

        if (requestCode == 123) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 456);

            } else {

                ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 456);

            }

        }

        if (requestCode == 456) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 789);

            } else {

                ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 789);

            }

        }

        if (requestCode == 789) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.FOREGROUND_SERVICE}, 999);


            } else {
                ActivityCompat.requestPermissions(PermissionOnBoardingActivity.this, new String[]{Manifest.permission.FOREGROUND_SERVICE}, 999);


            }

        }

        if (requestCode == 999) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // done
                openLoginPage();

            } else {
                // done
                openLoginPage();

            }

        }
    }

    public void openLoginPage() {
        final SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(PermissionOnBoardingActivity.this);

        SharedPreferences.Editor editors = settings.edit();
        editors.putBoolean("firstTimeUserOnBoarding", false).commit();
        editors.apply();

        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
    }
}

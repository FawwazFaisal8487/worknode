package com.evfy.evfytracker.Activity.Tracking;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class ForegroundLocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = ForegroundLocationService.class.getSimpleName();

    // the notification id for the foreground notification
    public static final int GPS_NOTIFICATION = 1;

    // the interval in seconds that gps updates are requested
    private static final int UPDATE_INTERVAL_IN_SECONDS = 8;

    // is this service currently running in the foreground?
    private boolean isForeground = false;

    // the google api client
    private GoogleApiClient googleApiClient;

    // the wakelock used to keep the app alive while the screen is off
    private PowerManager.WakeLock wakeLock;

    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask doTask;
    SharedPreferences settings;
    private Location lastLocation = null;
    NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    @Override
    public void onCreate() {
        super.onCreate();

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        handler = new Handler();
        timer = new Timer();

        // create google api client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // get a wakelock from the power manager
        final PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (!isForeground) {

            Log.v(TAG, "Starting the " + this.getClass().getSimpleName());

            startForeground(ForegroundLocationService.GPS_NOTIFICATION,
                    notifyUserThatLocationServiceStarted());
            isForeground = true;

            // connect to google api client
            googleApiClient.connect();

            timerCount();

            // acquire wakelock
            wakeLock.acquire();
        }

        return START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {

        Log.v(TAG, "Stopping the " + this.getClass().getSimpleName());

        stopForeground(true);
        isForeground = false;

        // disconnect from google api client
        googleApiClient.disconnect();

        if (timer != null) {
            timer.cancel();
        }

        if (doTask != null) {
            doTask.cancel();
        }

        // release wakelock if it is held
        if (null != wakeLock && wakeLock.isHeld()) {
            wakeLock.release();
        }

        super.onDestroy();
    }

    private LocationRequest getLocationRequest() {

        LocationRequest locationRequest = LocationRequest.create();

        // we always want the highest accuracy
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // we want to make sure that we get an updated location at the specified interval
        locationRequest.setInterval(TimeUnit.SECONDS.toMillis(8));

        // this sets the fastest interval that the app can receive updates from other apps accessing
        // the location service. for example, if Google Maps is running in the background
        // we can update our location from what it sees every five seconds
        locationRequest.setFastestInterval(TimeUnit.SECONDS.toMillis(0));
        locationRequest.setMaxWaitTime(TimeUnit.SECONDS.toMillis(UPDATE_INTERVAL_IN_SECONDS));

        return locationRequest;
    }

    private Notification notifyUserThatLocationServiceStarted() {

        LogCustom.i("notifyUserThatLocationServiceStarted");

        // pop up a notification that the location service is running
        Intent notificationIntent = new Intent(this, DrawerActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder = new NotificationCompat.Builder(this, "com.logisfleet.taskme");
        builder.setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.service_is_running))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setWhen(System.currentTimeMillis());
        builder.setAutoCancel(true);
        builder.getNotification().flags |= Notification.FLAG_FOREGROUND_SERVICE;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel("com.logisfleet.taskme", "My Notifications", NotificationManager.IMPORTANCE_MAX);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            mNotificationManager.createNotificationChannel(notificationChannel);
            // mNotificationManager.notify(0, builder.build());
        }

        //  int notificationIdNotify = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        // mNotificationManager.notify(notificationIdNotify, builder.build());


        final Notification notification;
        if (Build.VERSION.SDK_INT < 16) {
            notification = builder.getNotification();
        } else {
            notification = builder.build();
        }

        return notification;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        try {

            // request location updates from the fused location provider
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    googleApiClient, getLocationRequest(), this);

        } catch (SecurityException securityException) {
            Log.e(TAG, "Exception while requesting location updates", securityException);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Google API Client suspended.");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "Failed to connect to Google API Client.");
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        // Log.e(TAG, "onLocationChanged: " + location.toString());
    }

    public void timerCount() {
        LogCustom.i("timerCount", "yess");

        GeneralActivity generalActivity = new GeneralActivity();
        if (generalActivity.haveInternetConnected(getApplicationContext())) {
            doTask = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        @SuppressWarnings("unchecked")
                        public void run() {
                            try {
                                LogCustom.i("timerCount", "yess11 last locationL:" + lastLocation);
                                // new SelfHostedGPSTrackerRequest().execute(urlText + "lat=" + lastLocation.getLatitude() + "&lon=" + lastLocation.getLongitude());
                                if (lastLocation != null) {
                                    boolean isOnline = settings.getBoolean("isOnline", false);
                                    if (isOnline) {
                                        LogCustom.i("getLatitude", "" + lastLocation.getLatitude());
                                        LogCustom.i("getLongitude", "" + lastLocation.getLongitude());

                                        GeneralActivity generalActivity = new GeneralActivity();
                                        if (generalActivity.haveInternetConnected(getApplicationContext())) {
                                            uploadLocationToServer();
                                        }

                                    }

                                } else {
                                    if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                        // TODO: Consider calling


                                        return;
                                    }

                                    LogCustom.i("timerCount", "yess33");
                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                            }
                        }
                    });
                }
            };
            timer.schedule(doTask, 0, 10000);
        }


    }

    public void uploadLocationToServer() {
        try {
            Log.i("upload location", "upload location 00");

            int resource_owner_id = settings.getInt("resource_owner_id", 0);
            boolean isOnline = settings.getBoolean("isOnline", true);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("is_online", isOnline);
            jsonObject.put("latitude", String.valueOf(lastLocation.getLatitude()))
                    //.put("worker_id",resource_owner_id)
                    .put("longitude", String.valueOf(lastLocation.getLongitude()));
            Log.i("upload location", "upload location 00 JSON BDOY:" + jsonObject.toString());

            OkHttpRequest okHttpRequest = new OkHttpRequest();

            try {
                OkHttpRequest.driverUpdateLocation(getApplicationContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {
                                Log.i("upload location", "upload location body 00:" + body);

                                if (body == null || body.isEmpty()) {

                                    LogCustom.i("valueIsNull", "yes");
                                } else {

                                    try {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.post(new Runnable() {
                                            public void run() {
                                                // Toast.makeText(getApplicationContext(), "Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                            }
                                        });
                                        final JSONObject data = new JSONObject(body);
                                        Log.d("HTTP request done", " : ");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
                                Log.i("upload location", "upload location body error 00:" + body);

                                if (!requestCallBackError) {
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    handler.post(new Runnable() {
                                        public void run() {
                                            //  Toast.makeText(getApplicationContext(), "False Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }
}

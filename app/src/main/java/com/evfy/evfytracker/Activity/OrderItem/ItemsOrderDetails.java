package com.evfy.evfytracker.Activity.OrderItem;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.evfy.evfytracker.Activity.HomeActivity.LoginActivity;
import com.evfy.evfytracker.Activity.Tracking.ForegroundLocationService;
import com.evfy.evfytracker.Constants;
import com.evfy.evfytracker.Database.DatabaseHandlerJobs;
import com.evfy.evfytracker.GeneralActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.evfy.evfytracker.R;
import com.evfy.evfytracker.classes.JobOrder;
import com.evfy.evfytracker.classes.OrderDetails;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ItemsOrderDetails extends AppCompatActivity {

    LinearLayout mLayoutListItem;
    int orderDetailId;
    JobOrder jobOrder;
    DatabaseHandlerJobs dbUpdateJobs;
    boolean isAddItem = false;
    boolean isDoneUpdate = false;
    String descriptionUpdateItem, remarksUpdateItem;
    int quantityUpdateItem, orderDetailStatusId;
    double weightUpdateItem;
    int ackowledgedOrderStatusIDFromDB, inprogressOrderStatusIDFromDB, selfcollectOrderStatusIDFromDB;
    Dialog pdProcess;
    TextView contextProgressDialog;
    boolean unauthorizedNeedToLoginPage = false;
    SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_order_details);

        dbUpdateJobs = new DatabaseHandlerJobs(this);

        unauthorizedNeedToLoginPage = false;
        settings = PreferenceManager.getDefaultSharedPreferences(ItemsOrderDetails.this);

        ackowledgedOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.ACKNOWLEDGED_TEXT);
        inprogressOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.INPROGRESS_TEXT);
        selfcollectOrderStatusIDFromDB = dbUpdateJobs.getOrderStatusIDbasedOnOrderStatusName(Constants.SELFCOLLECT_TEXT);

        initialize();

        pdProcess = new Dialog(this);
        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.custom_progress_dialog_process, null);
        contextProgressDialog = content.findViewById(R.id.contextProgressDialog);
        contextProgressDialog.setText(getResources().getString(R.string.updateJobSteps));
        pdProcess.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pdProcess.setContentView(content);
        pdProcess.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pdProcess.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            orderDetailId = extras.getInt("OrderId");
            orderDetailStatusId = extras.getInt("orderDetailStatusId");
        }

        SharedPreferences.Editor editors = settings.edit();
        editors.putBoolean("insideJobDetails", false).commit();
        editors.putBoolean("insideNotificationListing", false).commit();
        editors.apply();


        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        this.getSupportActionBar().setTitle("Items in this order");

        // action bar for app
        ActionBar actionBar = getSupportActionBar();


        if (actionBar != null) {
            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#A664CCC9"));
            actionBar.setBackgroundDrawable(colorDrawable);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        try {
            GeneralActivity generalActivity = new GeneralActivity();
            if (!generalActivity.haveInternetConnected(getApplicationContext())) {
                Log.i("internetConnection", "no");
                showErrorInternetDialog().show();
            } else {
                getJobStepDetails();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initialize() {
        mLayoutListItem = findViewById(R.id.mLayoutListItem);
    }

    public void displayData(JobOrder jobOrder) {
        mLayoutJobStepsDetails(jobOrder);
    }

    public void getJobStepDetails() throws IOException {

        //jobStepId = 183;

        contextProgressDialog.setText(getResources().getString(R.string.loadingItemJobDetails));
        pdProcess.show();

        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.getJobOrderDetails(getApplicationContext(), orderDetailId
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pdProcess != null) {
                                        if (pdProcess.isShowing()) {
                                            pdProcess.dismiss();
                                        }
                                    }

                                    if (body == null || body.isEmpty()) {

                                    } else {
                                        try {
                                            final JSONObject data = new JSONObject(body);

                                            LogCustom.i("dataaa", data.toString());

                                            jobOrder = new JobOrder(data);
                                            LogCustom.i("dataaaaaa", "" + jobOrder.getItemTrackingNumber());
                                            invalidateOptionsMenu();
                                            displayData(jobOrder);

                                        } catch (JSONException e) {

                                            LogCustom.e(e);

                                            try {
//                                                Crashlytics.log(responseCode, "getJobOrderDetails", body);
//                                                Crashlytics.logException(e);
                                            } catch (final Throwable t) {
                                            }

                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            final String jsonResponse = body;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if (pdProcess != null) {
                                        if (pdProcess.isShowing()) {
                                            pdProcess.dismiss();
                                        }
                                    }

                                    errorHandlingFromResponseCode(body, requestCallBackError, responseCode);


                                }
                            });
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */
    public void errorHandlingFromResponseCode(final String body, final boolean requestCallBackError, final int responseCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!requestCallBackError) {

                    unauthorizedNeedToLoginPage = false;

                    try {
                        String errorMessage = "";
                        JSONObject data = null;
                        if (body != null || !body.equalsIgnoreCase("")) {

                            try {
                                data = new JSONObject(body);
                            } catch (Exception e) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                            }

                            if (responseCode == Constants.STATUS_RESPONSE_BAD_REQUEST) {
                                if (data.has("error") && !(data.isNull("error"))) {
                                    try {
                                        errorMessage = data.getString("error");
                                    } catch (Exception e) {
                                        JSONObject errorMessageArray = data.getJSONObject("error");
                                        errorMessage = errorMessageArray.getString("error");
                                    }
                                }

                                if (errorMessage.equalsIgnoreCase("")) {
                                    errorMessage = getResources().getString(R.string.pleaseContactAdmin);
                                }

                                showErrorDialog(getResources().getString(R.string.badRequestErrorTitle), errorMessage).show();


                            } else if (responseCode == Constants.STATUS_RESPONSE_UNAUTHORIZED) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("device_not_found") && !(data.isNull("device_not_found"))) {
                                    if (data.getBoolean("device_not_found")) {
                                        showErrorDialog(getResources().getString(R.string.deviceNotFoundTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("device_is_banned") && !(data.isNull("device_is_banned"))) {
                                    if (data.getBoolean("device_is_banned")) {
                                        showErrorDialog(getResources().getString(R.string.deviceBannedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_FORBIDDEN) {

                                unauthorizedNeedToLoginPage = true;

                                if (data.has("unpaid_subscription") && !(data.isNull("unpaid_subscription"))) {
                                    if (data.getBoolean("unpaid_subscription")) {
                                        showErrorDialog(getResources().getString(R.string.unpaidSubscriptionTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("quota_reached") && !(data.isNull("quota_reached"))) {
                                    if (data.getBoolean("quota_reached")) {
                                        showErrorDialog(getResources().getString(R.string.quotaReachedTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else if (data.has("blacklist") && !(data.isNull("blacklist"))) {
                                    if (data.getBoolean("blacklist")) {
                                        showErrorDialog(getResources().getString(R.string.blacklistTitle), getResources().getString(R.string.pleaseContactAdmin)).show();
                                    }
                                } else {
                                    showErrorDialog(getResources().getString(R.string.errorLogin), getResources().getString(R.string.pleaseCheckPassword)).show();
                                }
                            } else if (responseCode == Constants.STATUS_RESPONSE_NOT_FOUND) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_INTERNAL_SERVER_ERROR) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();

                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                            } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                                showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                            }

                        } else {
                            showErrorDialog(getResources().getString(R.string.globalErrorMessageTitle), getResources().getString(R.string.globalErrorMessageContent)).show();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    if (responseCode == Constants.STATUS_RESPONSE_SERVER_DOWN) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    } else if (responseCode == Constants.CANNOT_RESOLVE_HOST) {
                        showErrorInternetDialog().show();
                    } else if (responseCode == Constants.STATUS_RESPONSE_SERVER_GATEWAY_TIMEOUT) {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorTimeoutMessageContent)).show();
                    } else {
                        showErrorDialog(getResources().getString(R.string.globalErrorMessageTitleSorry), getResources().getString(R.string.globalErrorMessageContent)).show();
                    }
                    // this for request call back error. Maybe because cannot connect server.
                }
            }
        });


    }
    /* ============================== ERROR HANDLING FROM RESPONSE CODE =============================== */

    public void mLayoutJobStepsDetails(final JobOrder jobOrder) {
        mLayoutListItem.removeAllViews();
        int noBil = 0;
        try {

            Collections.sort(jobOrder.getOrderDetailsArrayList(), new Comparator<OrderDetails>() {

                @Override
                public int compare(final OrderDetails lhs, final OrderDetails rhs) {

                    if (lhs == null && rhs == null) return 0;
                    if (lhs == null) return -1;
                    if (rhs == null) return 1;

                    return rhs.getUpdated_at().compareTo(lhs.getUpdated_at());
                }

            });

        } catch (final Throwable e) {
            LogCustom.e(e);
        }

        if (jobOrder.getOrderDetailsArrayList().size() > 0) {

            for (int i = 0; i < jobOrder.getOrderDetailsArrayList().size(); i++) {
                final OrderDetails orderDetails = jobOrder.getOrderDetailsArrayList().get(i);

                if (JavaOp.ifNullThenLog(orderDetails)) continue;

                final View view = View.inflate(this, R.layout.view_items_order_details, null);
                mLayoutListItem.addView(view);
                final LinearLayout layoutItem = view.findViewById(R.id.layoutItem);
                final TextView itemName = view.findViewById(R.id.itemName);
                final TextView totalUnitItems = view.findViewById(R.id.totalUnitItems);

                itemName.setText(orderDetails.getOrderDescription());

                if (orderDetails.getUom() == null || orderDetails.getUom().equalsIgnoreCase("") || orderDetails.getUom().isEmpty()) {
                    totalUnitItems.setText(orderDetails.getQuantity() + " units");
                    LogCustom.i("uom empty", "yes");
                } else {
                    totalUnitItems.setText(orderDetails.getQuantity() + " " + orderDetails.getUom());
                    LogCustom.i("uom empty", "noo");
                }


                layoutItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        if(orderDetailStatusId == ackowledgedOrderStatusIDFromDB || orderDetailStatusId == inprogressOrderStatusIDFromDB || orderDetailStatusId == selfcollectOrderStatusIDFromDB){
//                            isAddItem = false;
//                            updateItemLayout(orderDetails.getOrderDescription(), orderDetails.getQuantity(), orderDetails.getWeight(), orderDetails.getOrderDetailsId(), orderDetails.getRemarks());
//                        }else{
//                            // already completed so cannot edit
//                        }
                        updateItemLayout(orderDetails.getOrderDescription(), orderDetails.getQuantity(), orderDetails.getWeight(), orderDetails.getOrderDetailsId(), orderDetails.getRemarks());


                    }
                });
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //Update UI (menu sign)
        getMenuInflater().inflate(R.menu.items_order_details, menu);
        //  return true;

        final MenuItem menuItemAdd = menu.findItem(R.id.menuItemAdd);

        menuItemAdd.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItemAdd);
            }
        });

        return super.onCreateOptionsMenu(menu);

        //return true;

    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {

        /*Get order status id based on order status name*/


        if (orderDetailStatusId == ackowledgedOrderStatusIDFromDB || orderDetailStatusId == inprogressOrderStatusIDFromDB || orderDetailStatusId == selfcollectOrderStatusIDFromDB) {
            menu.findItem(R.id.menuItemAdd).setVisible(true);
            LogCustom.i("in111", "in111");
        } else {
            menu.findItem(R.id.menuItemAdd).setVisible(false);
            LogCustom.i("in222", "in222");
        }

        menu.findItem(R.id.menuItemAdd).setVisible(false);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.menuItemAdd:
                //  isAddItem = true;
                //  updateItemLayout("", 0, 0, 0, "");
                return true;

        }

        return super.onOptionsItemSelected(item);

    }

    public void updateItemLayout(String descriptionUpdate, int quantityUpdate, double weightUpdate, final int order_detail_id, String remarks) {
        LayoutInflater inflater = getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.items_layout_dialog, null);
        final TextView textViewTitle = alertLayout.findViewById(R.id.textViewTitle);
        final EditText editTextDescription = alertLayout.findViewById(R.id.editTextDescription);
        final EditText editTextQuantity = alertLayout.findViewById(R.id.editTextQuantity);
        final EditText editTextWeight = alertLayout.findViewById(R.id.editTextWeight);
        final EditText editTextRemarks = alertLayout.findViewById(R.id.editTextRemarks);

        if (isAddItem) {
            textViewTitle.setText("Add Items");
        } else {
            editTextDescription.setText(descriptionUpdate);
            editTextRemarks.setText(remarks);
            editTextQuantity.setText("" + quantityUpdate);
            editTextWeight.setText("" + weightUpdate);

            textViewTitle.setText("Details Items");
        }


        editTextDescription.setFocusable(false);
        editTextDescription.setEnabled(false);
        editTextDescription.setCursorVisible(false);

        editTextRemarks.setFocusable(false);
        editTextRemarks.setEnabled(false);
        editTextRemarks.setCursorVisible(false);

        editTextQuantity.setFocusable(false);
        editTextQuantity.setEnabled(false);
        editTextQuantity.setCursorVisible(false);

        editTextWeight.setFocusable(false);
        editTextWeight.setEnabled(false);
        editTextWeight.setCursorVisible(false);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(alertLayout);

//        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                isAddItem = false;
//            }
//        });

        isAddItem = false;

        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do nothing here because we override this button later to change the close behaviour.
                        //However, we still need this because on older versions of Android unless we
                        //pass a handler the button doesn't get instantiated
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
//Overriding the handler immediately after show is probably a better approach than OnShowListener as described below
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                //Do stuff, possibly set wantToCloseDialog to true then...
//                if (editTextDescription.getText().toString().length() == 0) {
//                    editTextDescription.setError("Description is required!");
//                    Log.i("dessNull", "yes");
//                } else if (editTextQuantity.getText().toString().length() == 0) {
//                    editTextQuantity.setError("Quantity is required!");
//                    Log.i("Quantity", "yes");
//                } else if (editTextWeight.getText().toString().length() == 0) {
//                    editTextWeight.setError("Weight is required!");
//                    Log.i("Weight", "yes");
//                } else {
//                    Log.i("yesdone", "yes");
//                    isDoneUpdate = true;
//                    quantityUpdateItem = Integer.parseInt(editTextQuantity.getText().toString().trim());
//                    weightUpdateItem = Double.parseDouble(editTextWeight.getText().toString().trim());
//                    descriptionUpdateItem = editTextDescription.getText().toString().trim();
//                    remarksUpdateItem = editTextRemarks.getText().toString().trim();
//                    dialog.dismiss();
//
//
//                    if (isAddItem) {
//                        jsonStructure();
//                    } else {
//                        for (final OrderDetails orderDetails : jobOrder.getOrderDetails()) {
//                            if (JavaOp.ifNullThenLog(orderDetails)) continue;
//                            if (order_detail_id == orderDetails.getOrderDetailsId()) {
//                                orderDetails.setOrderDescription(descriptionUpdateItem);
//                                orderDetails.setQuantity(quantityUpdateItem);
//                                orderDetails.setWeight(weightUpdateItem);
//                                orderDetails.setRemarks(remarksUpdateItem);
//                            }
//
//                        }
//
//                        jsonStructure();
//                       // mLayoutJobStepsDetails(jobOrder);
//                    }
//
//                }

                dialog.dismiss();
            }


        });
    }

    public void jsonStructure() {
        try {
            JSONObject rootObject = new JSONObject();
            JSONArray childArray = new JSONArray();

            // put array of id in object ID
            JSONObject childObject = new JSONObject();
            JSONArray idArray = new JSONArray();
            idArray.put(String.valueOf(orderDetailId));
            childObject.put("id", idArray);

            childObject.put("drop_off_to", jobOrder.getDropOffTo());
            childObject.put("drop_off_description", jobOrder.getDropOffDesc());


            LogCustom.i("order_status_id", "" + jobOrder.getOrderStatusId());

            childObject.put("order_status_id", jobOrder.getOrderStatusId());


            JSONObject orderDetailObject;
            JSONArray orderDetailArray = new JSONArray();

            if (isAddItem) {
                if (descriptionUpdateItem != null || descriptionUpdateItem.length() != 0) {
                    orderDetailObject = new JSONObject();

                    orderDetailObject.put("description", descriptionUpdateItem);
                    orderDetailObject.put("quantity", quantityUpdateItem);
                    orderDetailObject.put("weight", weightUpdateItem);
                    orderDetailObject.put("remarks", remarksUpdateItem);

                    orderDetailArray.put(orderDetailObject);
                }

            } else {
                for (final OrderDetails orderDetails : jobOrder.getOrderDetails()) {
                    if (JavaOp.ifNullThenLog(orderDetails)) continue;

                    Log.i("quantity", "" + orderDetails.getQuantity());

                    orderDetailObject = new JSONObject();

                    orderDetailObject.put("order_detail_id", orderDetails.getOrderDetailsId());
                    orderDetailObject.put("description", orderDetails.getOrderDescription());
                    orderDetailObject.put("quantity", orderDetails.getQuantity());
                    orderDetailObject.put("weight", orderDetails.getWeight());
                    orderDetailObject.put("remarks", orderDetails.getRemarks());

                    orderDetailArray.put(orderDetailObject);

                }
            }


            childObject.put("order_details", orderDetailArray);
            childArray.put(childObject);
            rootObject.put("data", childArray);
            Log.e("JsonObjectStructure", rootObject.toString(4));

            updateBatchOrder(rootObject);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateBatchOrder(JSONObject jsonObject) throws IOException {

        //jobStepId = 183;

        OkHttpRequest okHttpRequest = new OkHttpRequest();

        try {
            OkHttpRequest.batchUpdate(getApplicationContext(), jsonObject.toString()
                    , new OkHttpRequest.OKHttpNetwork() {
                        @Override
                        public void onSuccess(final String body, final int responseCode) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (body == null || body.isEmpty()) {

                                    } else {
                                        try {
                                            final JSONObject data = new JSONObject(body);

                                            LogCustom.i("dataaaBatchUpdate", data.toString());

                                            final JSONArray resultData = data.getJSONArray("result");
                                            for (int i = 0; i < resultData.length(); i++) {
                                                JSONObject jsonObject2 = (JSONObject) resultData.get(i);
                                                jobOrder = new JobOrder(jsonObject2);
                                            }

                                            LogCustom.i("dataaaaaa", "" + jobOrder.getItemTrackingNumber());

                                            showSuccessDialog(getResources().getString(R.string.titleSuccess), getResources().getString(R.string.contextItemJobDetailsSuccess)).show();


                                            displayData(jobOrder);
                                            invalidateOptionsMenu();


                                        } catch (JSONException e) {

                                            LogCustom.e(e);

                                            try {
//                                                Crashlytics.log(responseCode, "getJobOrderDetails", body);
//                                                Crashlytics.logException(e);
                                            } catch (final Throwable t) {
                                            }

                                        }
                                    }

                                }
                            });
                        }

                        @Override
                        public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {

                            final String jsonResponse = body;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    errorHandlingFromResponseCode(body, requestCallBackError, responseCode);
                                }
                            });
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /* ======= ALERT DIALOG POPUP ========== */
    ///success dialog
    SweetAlertDialog showSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = getSuccessDialog(title, context);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getSuccessDialog(String title, String context) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setContentText(context)
                .setTitleText(title)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }


    SweetAlertDialog showErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = getErrorInternetDialog();
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorInternetDialog() {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText("Your device not connect with internet. Please check your internet connection")
                .setTitleText("No internet connection")
                .setConfirmText("Open settings")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        final Intent intent = new Intent(Intent.ACTION_MAIN, null);
                        intent.addCategory(Intent.CATEGORY_LAUNCHER);
                        final ComponentName cn = new ComponentName("com.android.settings", "com.android.settings.wifi.WifiSettings");
                        intent.setComponent(cn);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setCancelText("Cancel")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }


    SweetAlertDialog showErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = getErrorDialog(titleError, contextError);
        sweetAlertDialog.show();
        return sweetAlertDialog;
    }

    SweetAlertDialog getErrorDialog(String titleError, String contextError) {
        final SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setContentText(contextError)
                .setTitleText(titleError)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                        if (unauthorizedNeedToLoginPage) {
                            GeneralActivity generalActivity = new GeneralActivity();
                            if (generalActivity.isMyServiceLocationRunning(getApplicationContext())) {
                                LogCustom.i("isMyServiceRunning", "true");
                                Intent intent = new Intent(getApplicationContext(), ForegroundLocationService.class);

                                stopService(intent);
                            }

                            settings.edit().remove("isOnline").apply();
                            settings.edit().remove("googleToken").apply();
                            settings.edit().putString("user_name", "").commit();
                            unauthorizedNeedToLoginPage = false;

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            i.putExtra("EXIT", true);
                            startActivity(i);
                        }

                    }
                });


        sweetAlertDialog.setCancelable(false);
        return sweetAlertDialog;
    }
    /* ======= ALERT DIALOG POPUP ========== */

}

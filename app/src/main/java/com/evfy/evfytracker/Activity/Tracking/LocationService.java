package com.evfy.evfytracker.Activity.Tracking;

/**
 * Created by coolasia on 27/3/18.
 */

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.evfy.evfytracker.Activity.HomeActivity.DrawerActivity;
import com.evfy.evfytracker.OkHttpRequest.OkHttpRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Neel Kadia on 25-05-2016.
 * MIT licence
 */
public class LocationService extends Service implements com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static final String BROADCAST_ACTION = "Hello World";
    private static final int TWO_MINUTES = 1000 * 60 * 1;
    public LocationManager locationManager;
    public MyLocationListener listener;
    public Location previousBestLocation = null;
    Handler handler = new Handler();
    Timer timer = new Timer();
    TimerTask doTask;
    private Location lastLocation = null;

    Intent intent;
    int counter = 0;
    SharedPreferences settings;
    LocationRequest locationRequest;

    private GoogleApiClient googleApiClient;
    FusedLocationProviderApi fusedLocationProviderApi;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    public void onCreate() {
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);
    }

    @Override
    public void onStart(Intent intent, int startId) {

        //settings = this.getSharedPreferences("MyPrefsFile",0);
        settings = PreferenceManager.getDefaultSharedPreferences(this);
        handler = new Handler();
        timer = new Timer();

        Log.i("changeLocation", "onStart");

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // That have two way getting location (by akmal = comment this one)
        // locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //  listener = new MyLocationListener();
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Toast.makeText(getApplicationContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
        } else {
            // That have two way getting location (by akmal = comment this one)

            // locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 100, 0, listener);
            // locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, listener);


            getLocation();

            timerCount();
        }


    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else return isNewer && !isSignificantlyLessAccurate && isFromSameProvider;
    }


    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onDestroy() {
        // handler.removeCallbacks(sendUpdatesToUI);
        super.onDestroy();
        LogCustom.i("STOP_SERVICE", "DONE");
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Toast.makeText(getApplicationContext(),"Permission Denied",Toast.LENGTH_SHORT).show();
        }
        if (timer != null) {
            timer.cancel();
        }

        if (doTask != null) {
            doTask.cancel();
        }
        // That have two way getting location (by akmal = comment this one)
        // locationManager.removeUpdates(listener);
    }

    public static Thread performOnBackgroundThread(final Runnable runnable) {
        final Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    runnable.run();
                } finally {

                }
            }
        };
        t.start();
        return t;
    }

    public void timerCount() {
        LogCustom.i("timerCount", "yess");
        doTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @SuppressWarnings("unchecked")
                    public void run() {
                        try {
                            LogCustom.i("timerCount", "yess11");
                            // new SelfHostedGPSTrackerRequest().execute(urlText + "lat=" + lastLocation.getLatitude() + "&lon=" + lastLocation.getLongitude());
                            if (lastLocation != null) {
                                boolean isOnline = settings.getBoolean("isOnline", false);
                                if (isOnline) {
                                    LogCustom.i("getLatitude", "" + lastLocation.getLatitude());
                                    LogCustom.i("getLongitude", "" + lastLocation.getLongitude());
                                    uploadLocationToServer();
//                                    int resource_owner_id = settings.getInt("resource_owner_id", 0);
//                                    if (resource_owner_id != 0) {
//                                        LogCustom.i("getLatitude", "" + lastLocation.getLatitude());
//                                        LogCustom.i("getLongitude", "" + lastLocation.getLongitude());
//                                        uploadLocationToServer();
//                                    }
                                }

                            } else {
                                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling

                                    getLocation();
                                    return;
                                }

                                LogCustom.i("timerCount", "yess33");
                            }
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doTask, 0, 10000);
    }

    public void uploadLocationToServer() {
        try {

            int resource_owner_id = settings.getInt("resource_owner_id", 0);
            boolean isOnline = settings.getBoolean("isOnline", true);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("is_online", isOnline);
            jsonObject.put("latitude", String.valueOf(lastLocation.getLatitude()))
                    //.put("worker_id",resource_owner_id)
                    .put("longitude", String.valueOf(lastLocation.getLongitude()));

            OkHttpRequest okHttpRequest = new OkHttpRequest();

            try {
                OkHttpRequest.driverUpdateLocation(getApplicationContext(), jsonObject.toString()
                        , new OkHttpRequest.OKHttpNetwork() {
                            @Override
                            public void onSuccess(final String body, final int responseCode) {
                                if (body == null || body.isEmpty()) {
                                    LogCustom.i("valueIsNull", "yes");
                                } else {

                                    try {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.post(new Runnable() {
                                            public void run() {
                                                // Toast.makeText(getApplicationContext(), "Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                            }
                                        });
                                        final JSONObject data = new JSONObject(body);
                                        Log.d("HTTP request done", " : ");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(final String body, final boolean requestCallBackError, final int responseCode) {
                                if (!requestCallBackError) {
                                    Handler handler = new Handler(Looper.getMainLooper());
                                    handler.post(new Runnable() {
                                        public void run() {
                                            //  Toast.makeText(getApplicationContext(), "False Update location!"+ " lat:"+ lastLocation.getLatitude() + "long:" + lastLocation.getLongitude(),Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }


    private void getLocation() {

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(30000);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        LogCustom.i(googleApiClient, "google api client get location");
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                lastLocation = location;

            }
        }

    };

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LogCustom.i("last location111", "last location in on connected:" + lastLocation);
            if (lastLocation == null) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper());
            }
//			double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();
//
//			Log.i("","drawer activity on connected lat:"+lat+"lon:"+lon);

        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;

    }

    public class MyLocationListener implements LocationListener {

        public void onLocationChanged(final Location loc) {
            Log.i("*********************", "Location changed");
            if (isBetterLocation(loc, previousBestLocation)) {

                try {

                    lastLocation = loc;


                    Log.i("changeLocation", "in212");
                    loc.getLatitude();
                    loc.getLongitude();
                    intent.putExtra("Latitude", loc.getLatitude());
                    intent.putExtra("Longitude", loc.getLongitude());
                    intent.putExtra("Provider", loc.getProvider());
                    //sendBroadcast(intent);

                    Intent RTReturn = new Intent(DrawerActivity.RECEIVE_JSON);
                    RTReturn.putExtra("Latitude", loc.getLatitude());
                    RTReturn.putExtra("Longitude", loc.getLongitude());
                    RTReturn.putExtra("Provider", loc.getProvider());
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(RTReturn);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                }


            }
        }

        public void onProviderDisabled(String provider) {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                // Toast.makeText(getApplicationContext(), "Gps Disabled 111", Toast.LENGTH_SHORT).show();
                //locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 100, 0, listener);
                return;
            }

            Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }


        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }
}

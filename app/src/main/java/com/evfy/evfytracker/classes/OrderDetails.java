package com.evfy.evfytracker.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class OrderDetails implements Serializable, Cloneable {

    private int orderId, itemId, orderDetailsId, quantity, quantity_scanned;
    private String laundryType, trackingNumber, orderDescription, part_no, lot_no, balance, container_receipt, serial_no, expiry_date, remarks, updated_at, uom;
    private double price, weight, length, width, height;
    private OrderItem orderItem;

    public OrderDetails() {
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getOrderDescription() {
        return orderDescription;
    }

    public void setOrderDescription(String orderDescription) {
        this.orderDescription = orderDescription;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public OrderDetails(JSONObject data) throws JSONException {

        if (data.has("order_id") && !(data.isNull("order_id"))) {
            this.orderId = data.getInt("order_id");
        }

        if (data.has("id") && !(data.isNull("id"))) {
            this.orderDetailsId = data.getInt("id");
        }

        if (data.has("quantity") && !(data.isNull("quantity"))) {
            this.quantity = data.getInt("quantity");
        } else {
            this.quantity = 0;
        }

        if (data.has("tracking_number") && !(data.isNull("tracking_number"))) {
            this.trackingNumber = data.getString("tracking_number");
        }

        if (data.has("description") && !(data.isNull("description"))) {
            this.orderDescription = data.getString("description");
        } else {
            this.orderDescription = "";
        }

        if (data.has("weight") && !(data.isNull("weight"))) {
            this.weight = data.getDouble("weight");
        } else {
            this.weight = 0.0;
        }

        if (data.has("height") && !(data.isNull("height"))) {
            this.height = data.getDouble("height");
        } else {
            this.height = 0.0;
        }

        if (data.has("width") && !(data.isNull("width"))) {
            this.width = data.getDouble("width");
        } else {
            this.width = 0.0;
        }

        if (data.has("length") && !(data.isNull("length"))) {
            this.length = data.getDouble("length");
        } else {
            this.length = 0.0;
        }

        if (data.has("part_no") && !(data.isNull("part_no"))) {
            this.part_no = data.getString("part_no");
        } else {
            this.part_no = "";
        }

        if (data.has("lot_no") && !(data.isNull("lot_no"))) {
            this.lot_no = data.getString("lot_no");
        } else {
            this.lot_no = "";
        }

        if (data.has("expiry_date") && !(data.isNull("expiry_date"))) {
            this.expiry_date = data.getString("expiry_date");
        }

        if (data.has("serial_no") && !(data.isNull("serial_no"))) {
            this.serial_no = data.getString("serial_no");
        } else {
            this.serial_no = "";
        }

        if (data.has("remarks") && !(data.isNull("remarks"))) {
            this.remarks = data.getString("remarks");
        } else {
            this.remarks = "";
        }

        if (data.has("quantity_scanned") && !(data.isNull("quantity_scanned"))) {
            this.quantity_scanned = data.getInt("quantity_scanned");
        }

        if (data.has("balance") && !(data.isNull("balance"))) {
            this.balance = data.getString("balance");
        }

        if (data.has("container_receipt") && !(data.isNull("container_receipt"))) {
            this.container_receipt = data.getString("container_receipt");
        } else {
            this.container_receipt = "";
        }

        if (data.has("laundry_type") && !(data.isNull("laundry_type"))) {
            this.laundryType = data.getString("laundry_type");
        }

        if (data.has("price") && !(data.isNull("price"))) {
            this.price = data.getDouble("price");
        }

        if (data.has("item") && !(data.isNull("item"))) {
            JSONObject itemObject = data.getJSONObject("item");
            this.orderItem = new OrderItem(itemObject);
        }

        if (data.has("item_id") && !(data.isNull("item_id"))) {
            this.itemId = data.getInt("item_id");
        }

        if (data.has("updated_at") && !(data.isNull("updated_at"))) {
            this.updated_at = data.getString("updated_at");
        }

        if (data.has("uom") && !(data.isNull("uom"))) {
            this.uom = data.getString("uom");
        } else {
            this.uom = "";
        }

    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean equals(Object o) {

        if (o == null) return false;

        if (o instanceof OrderDetails) {
            OrderDetails orderDetails = (OrderDetails) o;
            return orderDetails.getLaundryType().equals(laundryType) && orderDetails.getItemId() == itemId;
        }

        return false;

    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public OrderItem getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(OrderItem orderItem) {
        this.orderItem = orderItem;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getLaundryType() {
        return laundryType;
    }

    public void setLaundryType(String laundryType) {
        this.laundryType = laundryType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPart_no() {
        return part_no;
    }

    public void setPart_no(String part_no) {
        this.part_no = part_no;
    }

    public String getLot_no() {
        return lot_no;
    }

    public void setLot_no(String lot_no) {
        this.lot_no = lot_no;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getContainer_receipt() {
        return container_receipt;
    }

    public void setContainer_receipt(String container_receipt) {
        this.container_receipt = container_receipt;
    }

    public int getQuantity_scanned() {
        return quantity_scanned;
    }

    public void setQuantity_scanned(int quantity_scanned) {
        this.quantity_scanned = quantity_scanned;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}

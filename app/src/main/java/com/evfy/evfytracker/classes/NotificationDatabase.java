package com.evfy.evfytracker.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;


@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationDatabase implements Serializable {

    private int indexDB;
    private String orderId;
    private String referenceNumber;
    private String titleNotification;
    private String contentNotification;
    private String attributeUpdated;
    private Integer jobUpdated;
    private Integer jobDestroy;
    private Integer doneRead;
    private String timeCreated;


    public NotificationDatabase() {

    }

    public NotificationDatabase(int indexDB, String orderId, String referenceNumber, String titleNotification, String contentNotification, String attributeUpdated, String timeCreated, Integer jobUpdated, Integer doneRead, Integer jobDestroy) {
        this.indexDB = indexDB;
        this.orderId = orderId;
        this.referenceNumber = referenceNumber;
        this.titleNotification = titleNotification;
        this.contentNotification = contentNotification;
        this.attributeUpdated = attributeUpdated;
        this.timeCreated = timeCreated;
        this.jobUpdated = jobUpdated;
        this.doneRead = doneRead;
        this.jobDestroy = jobDestroy;


    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public Integer isDoneRead() {
        return doneRead;
    }

    public void setDoneRead(Integer doneRead) {
        this.doneRead = doneRead;
    }

    public int getIndexDB() {
        return indexDB;
    }

    public void setIndexDB(int indexDB) {
        this.indexDB = indexDB;
    }


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getTitleNotification() {
        return titleNotification;
    }

    public void setTitleNotification(String titleNotification) {
        this.titleNotification = titleNotification;
    }

    public String getContentNotification() {
        return contentNotification;
    }

    public void setContentNotification(String contentNotification) {
        this.contentNotification = contentNotification;
    }

    public String getAttributeUpdated() {
        return attributeUpdated;
    }

    public void setAttributeUpdated(String attributeUpdated) {
        this.attributeUpdated = attributeUpdated;
    }

    public Integer isJobUpdated() {
        return jobUpdated;
    }

    public void setJobUpdated(Integer jobUpdated) {
        this.jobUpdated = jobUpdated;
    }

    public Integer isJobDestroy() {
        return jobDestroy;
    }

    public void setJobDestroy(Integer jobDestroy) {
        this.jobDestroy = jobDestroy;
    }
}

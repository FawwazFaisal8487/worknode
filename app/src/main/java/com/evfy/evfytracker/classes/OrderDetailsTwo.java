package com.evfy.evfytracker.classes;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia on 4/4/17.
 */

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetailsTwo implements Serializable {

    @JsonProperty("BinTrackId")
    private int binTrackId;
    @JsonProperty("BinId")
    private Integer binId;
    @JsonProperty("BinNumber")
    private String binNumber;
    @JsonProperty("TransportOrderId")
    private int transportOrderId;
    @JsonProperty("Action")
    private int action;
    @JsonProperty("DriverId")
    private int driverId;
    @JsonProperty("Latitude")
    private double latitude;
    @JsonProperty("Longitude")
    private double longitude;
    @JsonProperty("Address")
    private String address;
    @JsonProperty("Remarks")
    private String remarks;
    @JsonProperty("MarkAsUnavilable")
    private boolean markAsUnavilable;
    @JsonProperty("Timestamp")
    private String timestamp;


    private java.util.Date dateTime;

    public OrderDetailsTwo() {

    }

    public OrderDetailsTwo(int binTrackId, Integer binId, String binNumber, int transportOrderId, int action,
                           int driverId, double latitude, double longitude, String address, String remarks,
                           boolean markAsUnavilable, String timestamp, java.util.Date dateTime) {

        this.binTrackId = binTrackId;
        this.binId = binId;
        this.binNumber = binNumber;
        this.transportOrderId = transportOrderId;
        this.action = action;
        this.driverId = driverId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.remarks = remarks;
        this.markAsUnavilable = markAsUnavilable;
        this.timestamp = timestamp;
        this.dateTime = dateTime;
    }

    public OrderDetailsTwo(JSONObject data) throws JSONException {

        if (data.has("BinTrackId") && !(data.isNull("BinTrackId"))) {
            this.binTrackId = data.getInt("BinTrackId");
        }

        if (data.has("BinId") && !(data.isNull("BinId"))) {
            this.binId = data.getInt("BinId");
            Log.i("BinIDBin", "" + this.binId);
        } else if (this.binId == 0) {
            this.binId = null;
            Log.i("BinIDBin", "" + this.binId);
        }

        if (data.has("BinNumber") && !(data.isNull("BinNumber"))) {
            this.binNumber = data.getString("BinNumber");
        }

        if (data.has("TransportOrderId") && !(data.isNull("TransportOrderId"))) {
            this.transportOrderId = data.getInt("TransportOrderId");
        }

        if (data.has("Action") && !(data.isNull("Action"))) {
            this.action = data.getInt("Action");
        }

        if (data.has("DriverId") && !(data.isNull("DriverId"))) {
            this.driverId = data.getInt("DriverId");
        }

        if (data.has("Latitude") && !(data.isNull("Latitude"))) {
            this.latitude = data.getDouble("Latitude");
        } else {
            this.latitude = 0;
        }

        if (data.has("Longitude") && !(data.isNull("Longitude"))) {
            this.longitude = data.getDouble("Longitude");
        } else {
            this.longitude = 0;
        }

        if (data.has("Address") && !(data.isNull("Address"))) {
            this.address = data.getString("Address");
        }

        if (data.has("Remarks") && !(data.isNull("Remarks"))) {
            this.remarks = data.getString("Remarks");
        }

        if (data.has("MarkAsUnavilable") && !(data.isNull("MarkAsUnavilable"))) {
            this.markAsUnavilable = data.getBoolean("MarkAsUnavilable");
        }


    }


    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("BinTrackId", binTrackId);
            obj.put("BinId", binId);
            obj.put("BinNumber", binNumber);
            obj.put("TransportOrderId", transportOrderId);
            obj.put("Action", action);
            obj.put("DriverId", driverId);
            obj.put("Latitude", latitude);
            obj.put("Longitude", longitude);
            obj.put("Address", address);
            obj.put("Remarks", remarks);
            obj.put("MarkAsUnavilable", markAsUnavilable);
            obj.put("Timestamp", timestamp);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public int getBinTrackId() {
        return binTrackId;
    }

    public void setBinTrackId(int binTrackId) {
        this.binTrackId = binTrackId;
    }

    public Integer getBinId() {
        return binId;
    }

    public void setBinId(Integer binId) {
        this.binId = binId;
    }

    public String getBinNumber() {
        return binNumber;
    }

    public void setBinNumber(String binNumber) {
        this.binNumber = binNumber;
    }

    public int getTransportOrderId() {
        return transportOrderId;
    }

    public void setTransportOrderId(int transportOrderId) {
        this.transportOrderId = transportOrderId;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public boolean isMarkAsUnavilable() {
        return markAsUnavilable;
    }

    public void setMarkAsUnavilable(boolean markAsUnavilable) {
        this.markAsUnavilable = markAsUnavilable;
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}

package com.evfy.evfytracker.classes;

import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia-mac on 27/9/15.
 */
public class TrackingOrderCheckBox implements Serializable {


    private boolean isChecked;
    private String trackingNumber;

    public TrackingOrderCheckBox(JSONObject data) throws JSONException {


    }

    public TrackingOrderCheckBox() {

    }

    public TrackingOrderCheckBox(String trackingNumber, boolean isChecked) {

        this.trackingNumber = trackingNumber;
        this.isChecked = isChecked;
    }


    protected TrackingOrderCheckBox(Parcel in) {
        isChecked = in.readByte() != 0;
        trackingNumber = in.readString();
    }


    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }


}

package com.evfy.evfytracker.classes;

import com.lkh012349s.mobileprinter.Utils.JavaOp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ItemCategory implements Serializable {

    public final int mId;
    public final String mName;
    public final String mUrlImage;
    public final List<OrderItem> mItems = new LinkedList<>();

    public ItemCategory(final JSONObject object) throws JSONException {
        mId = object.getInt("id");
        mName = object.getString("name_en");
        mUrlImage = object.getString("image_url");
    }

    @Override
    public boolean equals(final Object o) {
        if (JavaOp.ifNullThenLog(o)) return false;
        if (!(o instanceof ItemCategory)) return false;
        final ItemCategory category = (ItemCategory) o;
        return category.mId == mId;
    }

    public static List<ItemCategory> get(final OrderItem[] items) {

        final List<ItemCategory> categories = new ArrayList<>();
        if (JavaOp.ifArrayNullOrEmptyThenLog(items)) return categories;

        for (final OrderItem item : items) {

            final int index = categories.indexOf(item.getCategory());
            final ItemCategory category;

            if (index == -1) {
                category = item.getCategory();
                categories.add(category);
            } else {
                category = categories.get(index);
            }

            category.mItems.add(item);

        }

        return categories;
    }

}
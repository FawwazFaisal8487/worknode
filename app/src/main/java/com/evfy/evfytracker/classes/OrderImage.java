package com.evfy.evfytracker.classes;

import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONObject;

import java.io.Serializable;

public class OrderImage implements Serializable {

    String mUrl;
    String mNote;

    public OrderImage(final JSONObject jsonObject) {

        try {
            if (jsonObject.has("order_image_url") && !jsonObject.isNull("order_image_url"))
                mUrl = jsonObject.getString("order_image_url");
            if (jsonObject.has("note") && !jsonObject.isNull("note"))
                mNote = jsonObject.getString("note");
        } catch (final Throwable e) {
            LogCustom.e(e);
        }

    }

    public String getNote() {
        return mNote;
    }

    public void setNote(final String note) {
        mNote = note;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(final String url) {
        mUrl = url;
    }

}

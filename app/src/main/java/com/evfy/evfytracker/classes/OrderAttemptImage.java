package com.evfy.evfytracker.classes;

import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class OrderAttemptImage implements Serializable {

    String mUrl;
    String mNote;
    String mBase64;
    boolean isSignature;
    boolean isRemove;
    int id, orderAttemptID, orderId;
    Boolean imageUpdateNoInternet;

    public OrderAttemptImage(final JSONObject jsonObject) {

        try {
            if (jsonObject.has("image_url") && !jsonObject.isNull("image_url"))
                mUrl = jsonObject.getString("image_url");
            if (jsonObject.has("description") && !jsonObject.isNull("description"))
                mNote = jsonObject.getString("description");
            if (jsonObject.has("is_signature") && !jsonObject.isNull("is_signature"))
                isSignature = jsonObject.getBoolean("is_signature");
            if (jsonObject.has("id") && !jsonObject.isNull("id")) id = jsonObject.getInt("id");
        } catch (final Throwable e) {
            LogCustom.e(e);
        }

    }

    public OrderAttemptImage() {

    }

    public OrderAttemptImage(String mUrl, String mNote) {
        this.mUrl = mUrl;
        this.mNote = mNote;
        this.mBase64 = "";

    }

    public OrderAttemptImage(String base64) {
        this.mUrl = "";
        this.mNote = "";
        this.mBase64 = base64;
    }

    public OrderAttemptImage(String base64, String mNote, boolean isSignature) {
        this.mUrl = base64;
        this.mNote = mNote;
        this.mBase64 = base64;
        this.isSignature = isSignature;
    }

    public OrderAttemptImage(String mNote, int id, boolean isRemove, boolean isSignature) {
        this.mNote = mNote;
        this.id = id;
        this.isRemove = isRemove;
        this.isSignature = isSignature;
    }

    public Boolean isImageUpdateNoInternet() {
        return imageUpdateNoInternet;
    }

    public void setImageUpdateNoInternet(Boolean imageUpdateNoInternet) {
        this.imageUpdateNoInternet = imageUpdateNoInternet;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderAttemptID() {
        return orderAttemptID;
    }

    public void setOrderAttemptID(int orderAttemptID) {
        this.orderAttemptID = orderAttemptID;
    }

    public String getBase64() {
        return mBase64;
    }

    public String getNote() {
        return mNote;
    }

    public void setBase64(final String base64) {
        mBase64 = base64;
    }

    public void setNote(final String note) {
        mNote = note;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(final String url) {
        mUrl = url;
    }

    public boolean isSignature() {
        return isSignature;
    }

    public void setSignature(boolean signature) {
        isSignature = signature;
    }

    public boolean isRemove() {
        return isRemove;
    }

    public void setRemove(boolean remove) {
        isRemove = remove;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("image_url", mUrl);
            obj.put("description", mNote);
            obj.put("is_signature", isSignature);
        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

}

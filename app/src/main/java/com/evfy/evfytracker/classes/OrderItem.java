package com.evfy.evfytracker.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class OrderItem implements Serializable {

    /**
     * Check if the item is the last item in its category. If so, a divider will not be shown in UI.
     */
    boolean mIsLast;

    private ItemCategory mCategory;
    private int orderItemId, subCatId;
    private String nameEn, nameCn, imageUrl, createdOn;
    private double washIronPrice, dryCleanPrice, ironPrice, discountWashironPrice, discountDryCleanPrice, discountIronPrice;
    private boolean haveDiscountDryCleanPrice, haveDiscountWashIronPrice, haveDiscountIronPrice, disabled;

    //	public enum Category {
    //
    //		GENERAL( "General", R.drawable.item_category_general, 1 ),
    //		LADIES( "Ladies", R.drawable.item_category_ladies, 2 ),
    //		TRADITIONALS( "Traditionals", R.drawable.item_category_traditionals,3 ),
    //		EVENT_DRESSES( "Event Dresses", R.drawable.item_category_event_dresses,4 ),
    //		BEDROOM( "Bedroom", R.drawable.item_category_bedroom ,5),
    //		CURTAIN( "Curtain", R.drawable.item_category_curtain ,6),
    //		LIVING_ROOM( "Living Room", R.drawable.item_category_living_room, 7 ),
    //		KITCHEN( "Kitchen", R.drawable.item_category_kitchen, 8 ),
    //		SPECIAL_ITEMS( "Special Items", R.drawable.item_category_special_items, 10 ),
    //		OTHER( "Other Personal Items", R.drawable.item_category_other, 11 ),
    //		LOAD_WASH( "Load Wash (Wash & Fold, No Ironing, Min. 3KG） ", R.drawable.item_category_load_wash ,12),
    //		PRICE_DIFF( "Price Difference", R.drawable.item_category_price_diff, 13 );
    //
    //		public static final Category[] CATEGORIES = values();
    //		public final String name;
    //		public final int iconResId;
    //		public final int id;
    //
    //		Category ( final String name, final int iconResId, final int id ) {
    //			this.name = name;
    //			this.iconResId = iconResId;
    //			this.id = id;
    //		}
    //
    //		public static Category get ( final int categoryId ) {
    //
    //			for ( final Category category : CATEGORIES ) {
    //				if ( categoryId == category.id  ) return category;
    //			}
    //
    //			LogCustom.e( "Category not found." );
    //			return null;
    //
    //		}
    //
    //	}

    public OrderItem(JSONObject data) throws JSONException {

        if (data.has("sub_category") && !(data.isNull("sub_category"))) {
            JSONObject itemObject = data.getJSONObject("sub_category");
            mCategory = new ItemCategory(itemObject);
        }

        if (data.has("id") && !(data.isNull("id"))) {
            this.orderItemId = data.getInt("id");
        }

        if (data.has("sub_category_id") && !(data.isNull("sub_category_id"))) {
            this.subCatId = data.getInt("sub_category_id");
        }

        if (data.has("name_en") && !(data.isNull("name_en"))) {
            this.nameEn = data.getString("name_en");
        }

        if (data.has("name_ch") && !(data.isNull("name_ch"))) {
            this.nameCn = data.getString("name_ch");
        }

        if (data.has("wash_iron_price") && !(data.isNull("wash_iron_price"))) {
            this.washIronPrice = data.getDouble("wash_iron_price");
        }

        if (data.has("dry_clean_price") && !(data.isNull("dry_clean_price"))) {
            this.dryCleanPrice = data.getDouble("dry_clean_price");
        }

        if (data.has("iron_price") && !(data.isNull("iron_price"))) {
            this.ironPrice = data.getDouble("iron_price");
        }

        if (data.has("discount_wash_iron_price") && !(data.isNull("discount_wash_iron_price"))) {
            this.discountWashironPrice = data.getDouble("discount_wash_iron_price");
        }

        if (data.has("discount_dry_clean_price") && !(data.isNull("discount_dry_clean_price"))) {
            this.discountDryCleanPrice = data.getDouble("discount_dry_clean_price");
        }
        if (data.has("discount_iron_price") && !(data.isNull("discount_iron_price"))) {
            this.discountIronPrice = data.getDouble("discount_iron_price");
        }
        if (data.has("have_discount_dry_clean_price") && !(data.isNull("have_discount_dry_clean_price"))) {
            this.haveDiscountDryCleanPrice = data.getBoolean("have_discount_dry_clean_price");
        }

        if (data.has("have_discount_wash_iron_price") && !(data.isNull("have_discount_wash_iron_price"))) {
            this.haveDiscountWashIronPrice = data.getBoolean("have_discount_wash_iron_price");
        }

        if (data.has("have_discount_iron_price") && !(data.isNull("have_discount_iron_price"))) {
            this.haveDiscountIronPrice = data.getBoolean("have_discount_iron_price");
        }
        if (data.has("image_url") && !(data.isNull("image_url"))) {
            this.imageUrl = data.getString("image_url");
        }
        if (data.has("created_on") && !(data.isNull("created_on"))) {
            this.createdOn = data.getString("created_on");
        }

        if (data.has("disabled") && !(data.isNull("disabled"))) {
            this.disabled = data.getBoolean("disabled");
        }
    }

    public boolean isSale() {
        return haveDiscountDryCleanPrice || haveDiscountWashIronPrice || haveDiscountIronPrice;
    }

    public ItemCategory getCategory() {
        return mCategory;
    }

    public boolean isLast() {
        return mIsLast;
    }

    public void setIsLast(final boolean isLast) {
        mIsLast = isLast;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(final String createdOn) {
        this.createdOn = createdOn;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(final boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isHaveDiscountDryCleanPrice() {
        return haveDiscountDryCleanPrice;
    }

    public void setHaveDiscountDryCleanPrice(final boolean haveDiscountDryCleanPrice) {
        this.haveDiscountDryCleanPrice = haveDiscountDryCleanPrice;
    }

    public boolean isHaveDiscountIronPrice() {
        return haveDiscountIronPrice;
    }

    public void setHaveDiscountIronPrice(final boolean haveDiscountIronPrice) {
        this.haveDiscountIronPrice = haveDiscountIronPrice;
    }

    public boolean isHaveDiscountWashIronPrice() {
        return haveDiscountWashIronPrice;
    }

    public void setHaveDiscountWashIronPrice(final boolean haveDiscountWashIronPrice) {
        this.haveDiscountWashIronPrice = haveDiscountWashIronPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(final String nameCn) {
        this.nameCn = nameCn;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(final String nameEn) {
        this.nameEn = nameEn;
    }

    public int getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(final int orderItemId) {
        this.orderItemId = orderItemId;
    }

    public int getSubCatId() {
        return subCatId;
    }

    public void setSubCatId(final int subCatId) {
        this.subCatId = subCatId;
    }

    public double getDiscountDryCleanPrice() {
        return discountDryCleanPrice;
    }

    public void setDiscountDryCleanPrice(final double discountDryCleanPrice) {
        this.discountDryCleanPrice = discountDryCleanPrice;
    }

    public double getDiscountIronPrice() {
        return discountIronPrice;
    }

    public void setDiscountIronPrice(final double discountIronPrice) {
        this.discountIronPrice = discountIronPrice;
    }

    public double getDiscountWashironPrice() {
        return discountWashironPrice;
    }

    public void setDiscountWashironPrice(final double discountWashironPrice) {
        this.discountWashironPrice = discountWashironPrice;
    }

    public double getDryCleanPrice() {
        return dryCleanPrice;
    }

    public void setDryCleanPrice(final double dryCleanPrice) {
        this.dryCleanPrice = dryCleanPrice;
    }

    public double getIronPrice() {
        return ironPrice;
    }

    public void setIronPrice(final double ironPrice) {
        this.ironPrice = ironPrice;
    }

    public double getWashIronPrice() {
        return washIronPrice;
    }

    public void setWashIronPrice(final double washIronPrice) {
        this.washIronPrice = washIronPrice;
    }

    public double getDryCleanPrice(final boolean isExpress) {
        return (haveDiscountDryCleanPrice ? discountDryCleanPrice : dryCleanPrice) * (isExpress ? 2 : 1);
    }

    public double getIronPrice(final boolean isExpress) {
        return (haveDiscountIronPrice ? discountIronPrice : ironPrice) * (isExpress ? 2 : 1);
    }

    public double getWashIronPrice(final boolean isExpress) {
        return (haveDiscountWashIronPrice ? discountWashironPrice : washIronPrice) * (isExpress ? 2 : 1);
    }

}

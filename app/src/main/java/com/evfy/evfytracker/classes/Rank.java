package com.evfy.evfytracker.classes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by coolasia-mac on 27/9/15.
 */
public class Rank {

    private int rankId, minPoint, maxPoint, level;
    private String imageUrl, createdOn;


    public Rank(JSONObject data) throws JSONException {


        if (data.has("id")) {
            this.rankId = data.getInt("id");
        }


        if (data.has("min_point") && !(data.isNull("min_point"))) {
            this.minPoint = data.getInt("min_point");
        }

        if (data.has("max_point") && !(data.isNull("max_point"))) {
            this.maxPoint = data.getInt("max_point");
        }

        if (data.has("level") && !(data.isNull("level"))) {
            this.level = data.getInt("level");
        }

        if (data.has("image_url") && !(data.isNull("image_url"))) {
            this.imageUrl = data.getString("image_url");
        }

        if (data.has("created_on") && !(data.isNull("created_on"))) {
            this.createdOn = data.getString("created_on");
        }

    }

    public int getRankId() {
        return rankId;
    }

    public void setRankId(int rankId) {
        this.rankId = rankId;
    }

    public int getMinPoint() {
        return minPoint;
    }

    public void setMinPoint(int minPoint) {
        this.minPoint = minPoint;
    }

    public int getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(int maxPoint) {
        this.maxPoint = maxPoint;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}

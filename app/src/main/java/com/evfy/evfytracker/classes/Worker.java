package com.evfy.evfytracker.classes;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by coolasia-mac on 27/9/15.
 */
public class Worker {

    private int workerId, points, orderCount, factoryId;
    private String firstName, lastName, contactNo, profileImageUrlSmall, profileImageUrlMedium, profileImageUrlBig, note, createdOn, email, encryptedPassword, resetPasswordToken, resetPasswordSentAt, colorHash, lastKnownLocation, locationUpdatedAt;
    double lat, lon;
    boolean disabled, canViewWorker, isFactoryWorker;


    public Worker(JSONObject data) throws JSONException {


        if (data.has("id")) {

            this.workerId = data.getInt("id");
        }


        if (data.has("first_name") && !(data.isNull("first_name"))) {
            this.firstName = data.getString("first_name");
        }

        if (data.has("last_name") && !(data.isNull("last_name"))) {
            this.lastName = data.getString("last_name");
        }

        if (data.has("contact_no") && !(data.isNull("contact_no"))) {
            this.contactNo = data.getString("contact_no");
        }

        if (data.has("profile_image_url_small") && !(data.isNull("profile_image_url_small"))) {
            this.profileImageUrlSmall = data.getString("profile_image_url_small");
        }

        if (data.has("profile_image_url_medium") && !(data.isNull("profile_image_url_medium"))) {
            this.profileImageUrlMedium = data.getString("profile_image_url_medium");
        }

        if (data.has("profile_image_url_big") && !(data.isNull("profile_image_url_big"))) {
            this.profileImageUrlBig = data.getString("profile_image_url_big");
        }

        if (data.has("can_view_worker") && !(data.isNull("can_view_worker"))) {

            String canViewWorkerString = data.getString("can_view_worker");
            this.canViewWorker = !canViewWorkerString.equalsIgnoreCase("f");
        }

        if (data.has("disabled") && !(data.isNull("disabled"))) {

            String disabledString = data.getString("disabled");
            this.disabled = !disabledString.equalsIgnoreCase("f");
        }
        if (data.has("created_on") && !(data.isNull("created_on"))) {
            this.createdOn = data.getString("created_on");
        }

        if (data.has("note") && !(data.isNull("note"))) {
            this.note = data.getString("note");
        }


        if (data.has("created_on") && !(data.isNull("created_on"))) {
            this.createdOn = data.getString("created_on");
        }

        if (data.has("email") && !(data.isNull("email"))) {
            this.email = data.getString("email");
        }

        if (data.has("encrypted_password") && !(data.isNull("encrypted_password"))) {
            this.email = data.getString("encrypted_password");
        }

        if (data.has("reset_password_token") && !(data.isNull("reset_password_token"))) {
            this.resetPasswordToken = data.getString("reset_password_token");
        }

        if (data.has("reset_password_sent_at") && !(data.isNull("reset_password_sent_at"))) {
            this.resetPasswordSentAt = data.getString("reset_password_sent_at");
        }

        if (data.has("color_hash") && !(data.isNull("color_hash"))) {
            this.colorHash = data.getString("color_hash");
        }

        if (data.has("last_known_location") && !(data.isNull("last_known_location"))) {
            this.lastKnownLocation = data.getString("last_known_location");
        }


        if (data.has("location_updated_at") && !(data.isNull("location_updated_at"))) {
            this.locationUpdatedAt = data.getString("location_updated_at");
        }

        if (data.has("latitude") && !(data.isNull("latitude"))) {
            this.lat = data.getDouble("latitude");
        }

        if (data.has("longitude") && !(data.isNull("longitude"))) {
            this.lat = data.getDouble("longitude");
        }

        if (data.has("longitude") && !(data.isNull("longitude"))) {
            this.lat = data.getDouble("longitude");
        }

        if (data.has("factory_id") && !(data.isNull("factory_id"))) {
            this.factoryId = data.getInt("factory_id");
        }

        if (data.has("is_factory_worker") && !(data.isNull("is_factory_worker"))) {
            String isFactoryWorkerString = data.getString("is_factory_worker");
            this.isFactoryWorker = !isFactoryWorkerString.equalsIgnoreCase("f");

        }

        if (data.has("count") && !(data.isNull("count"))) {
            this.orderCount = data.getInt("count");
        }

    }

    public int getWorkerId() {
        return workerId;
    }

    public void setWorkerId(int workerId) {
        this.workerId = workerId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getProfileImageUrlSmall() {
        return profileImageUrlSmall;
    }

    public void setProfileImageUrlSmall(String profileImageUrlSmall) {
        this.profileImageUrlSmall = profileImageUrlSmall;
    }

    public String getProfileImageUrlMedium() {
        return profileImageUrlMedium;
    }

    public void setProfileImageUrlMedium(String profileImageUrlMedium) {
        this.profileImageUrlMedium = profileImageUrlMedium;
    }

    public String getProfileImageUrlBig() {
        return profileImageUrlBig;
    }

    public void setProfileImageUrlBig(String profileImageUrlBig) {
        this.profileImageUrlBig = profileImageUrlBig;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }

    public String getResetPasswordSentAt() {
        return resetPasswordSentAt;
    }

    public void setResetPasswordSentAt(String resetPasswordSentAt) {
        this.resetPasswordSentAt = resetPasswordSentAt;
    }

    public String getColorHash() {
        return colorHash;
    }

    public void setColorHash(String colorHash) {
        this.colorHash = colorHash;
    }

    public String getLastKnownLocation() {
        return lastKnownLocation;
    }

    public void setLastKnownLocation(String lastKnownLocation) {
        this.lastKnownLocation = lastKnownLocation;
    }

    public String getLocationUpdatedAt() {
        return locationUpdatedAt;
    }

    public void setLocationUpdatedAt(String locationUpdatedAt) {
        this.locationUpdatedAt = locationUpdatedAt;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isCanViewWorker() {
        return canViewWorker;
    }

    public void setCanViewWorker(boolean canViewWorker) {
        this.canViewWorker = canViewWorker;
    }

    public boolean isFactoryWorker() {
        return isFactoryWorker;
    }

    public void setIsFactoryWorker(boolean isFactoryWorker) {
        this.isFactoryWorker = isFactoryWorker;
    }
}

package com.evfy.evfytracker.classes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Akmal on 14-May-17.
 */

@SuppressWarnings("unused")
public class DeliveryOrderOutDetails implements Serializable {

    private int id, order_id, quantity, quantity_scanned;
    private String part_no, lot_no, location, remarks, serial_no, container_receipt;
    private String expiry_date;
    private double balance;
    private boolean handling_in;
    int id_DOrderDetail;
    String reference_no_DOrderDetail, handling_in_date_DOrderDetail, delivery_to_DOrderDetail, handling_out_date_DOrderDetail;


    public DeliveryOrderOutDetails() {
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public DeliveryOrderOutDetails(JSONObject data) throws JSONException {

        Log.i("", "Delivery order OUT details:" + data);

        if (data.has("id")) {
            this.id = data.getInt("id");
        }

        if (data.has("order_id")) {
            this.order_id = data.getInt("order_id");
        }

        if (data.has("handling_in")) {
            this.handling_in = data.getBoolean("handling_in");
        }

        if (data.has("part_no") && !(data.isNull("part_no"))) {
            this.part_no = data.getString("part_no");
        }

        if (data.has("lot_no") && !(data.isNull("lot_no"))) {
            this.lot_no = data.getString("lot_no");
        }

        if (data.has("expiry_date") && !(data.isNull("expiry_date"))) {
            this.expiry_date = data.getString("expiry_date");
        }

        if (data.has("serial_no") && !(data.isNull("serial_no"))) {
            this.serial_no = data.getString("serial_no");
        }


        if (data.has("quantity")) {
            this.quantity = data.getInt("quantity");
        }

        if (data.has("location") && !(data.isNull("location"))) {
            this.location = data.getString("location");
        }

        if (data.has("remarks") && !(data.isNull("remarks"))) {
            this.remarks = data.getString("remarks");
        }

        if (data.has("balance") && !(data.isNull("balance"))) {
            this.balance = data.getDouble("balance");
        }

        if (data.has("container_receipt") && !(data.isNull("container_receipt"))) {
            this.container_receipt = data.getString("container_receipt");
        }

        if (data.has("quantity_scanned")) {
            this.quantity_scanned = data.getInt("quantity_scanned");
        }

        if (data.has("order") && !(data.isNull("order"))) {

            JSONObject orderJSON = data.getJSONObject("order");
            if (orderJSON.has("id") && !(orderJSON.isNull("id"))) {
                this.id_DOrderDetail = orderJSON.getInt("id");
            }

            if (orderJSON.has("reference_no") && !(orderJSON.isNull("reference_no"))) {
                this.reference_no_DOrderDetail = orderJSON.getString("reference_no");
            }

            if (orderJSON.has("handling_in_date") && !(orderJSON.isNull("handling_in_date"))) {
                this.handling_in_date_DOrderDetail = orderJSON.getString("handling_in_date");
            }

            if (orderJSON.has("delivery_to") && !(orderJSON.isNull("delivery_to"))) {
                this.delivery_to_DOrderDetail = orderJSON.getString("delivery_to");
            }

            if (orderJSON.has("handling_out_date") && !(orderJSON.isNull("handling_out_date"))) {
                this.handling_out_date_DOrderDetail = orderJSON.getString("handling_out_date");
            }

        }

    }


    public String getContainer_receipt() {
        return container_receipt;
    }

    public void setContainer_receipt(String container_receipt) {
        this.container_receipt = container_receipt;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public boolean isHandling_in() {
        return handling_in;
    }

    public void setHandling_in(boolean handling_in) {
        this.handling_in = handling_in;
    }

    public int getId_DOrderDetail() {
        return id_DOrderDetail;
    }

    public void setId_DOrderDetail(int id_DOrderDetail) {
        this.id_DOrderDetail = id_DOrderDetail;
    }

    public String getReference_no_DOrderDetail() {
        return reference_no_DOrderDetail;
    }

    public void setReference_no_DOrderDetail(String reference_no_DOrderDetail) {
        this.reference_no_DOrderDetail = reference_no_DOrderDetail;
    }

    public String getHandling_in_date_DOrderDetail() {
        return handling_in_date_DOrderDetail;
    }

    public void setHandling_in_date_DOrderDetail(String handling_in_date_DOrderDetail) {
        this.handling_in_date_DOrderDetail = handling_in_date_DOrderDetail;
    }

    public String getDelivery_to_DOrderDetail() {
        return delivery_to_DOrderDetail;
    }

    public void setDelivery_to_DOrderDetail(String delivery_to_DOrderDetail) {
        this.delivery_to_DOrderDetail = delivery_to_DOrderDetail;
    }

    public String getHandling_out_date_DOrderDetail() {
        return handling_out_date_DOrderDetail;
    }

    public void setHandling_out_date_DOrderDetail(String handling_out_date_DOrderDetail) {
        this.handling_out_date_DOrderDetail = handling_out_date_DOrderDetail;
    }

    public int getQuantity_scanned() {
        return quantity_scanned;
    }

    public void setQuantity_scanned(int quantity_scanned) {
        this.quantity_scanned = quantity_scanned;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPart_no() {
        return part_no;
    }

    public void setPart_no(String part_no) {
        this.part_no = part_no;
    }

    public String getLot_no() {
        return lot_no;
    }

    public void setLot_no(String lot_no) {
        this.lot_no = lot_no;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }
}


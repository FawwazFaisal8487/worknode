package com.evfy.evfytracker.classes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by coolasia on 17/7/17.
 */

public class JobSteps implements Serializable, Cloneable {

    private int id, orderId, worker_id, job_step_type_id, order_sequence, job_step_status_id;
    private String location, order_detail_ids, updated_at, description, job_step_name, job_step_pic, job_step_pic_contact, updated_atDB;
    private double latitude, longitude;
    private boolean is_transfer_done, is_signature_required, is_scan_required;
    private Boolean isJobStepUpdateWithNoInternet;
    private List<OrderAttempt> mOrderAttemptsList = new ArrayList<>();

    private String dropOffTime, dropOffTimeEnd;

    // job_step_status

    public JobSteps() {
    }

    public JobSteps(JSONObject data) throws JSONException {

        if (data.has("id") && !(data.isNull("id"))) {
            this.id = data.getInt("id");
        }

        if (data.has("order_id") && !(data.isNull("order_id"))) {
            this.orderId = data.getInt("order_id");
        }

        if (data.has("worker_id") && !(data.isNull("worker_id"))) {
            this.worker_id = data.getInt("worker_id");
        }

        if (data.has("location") && !(data.isNull("location"))) {
            this.location = data.getString("location");
        } else {
            this.location = "";
        }

        if (data.has("latitude") && !(data.isNull("latitude"))) {
            this.latitude = data.getDouble("latitude");
        } else {
            this.latitude = 0.0;
        }

        if (data.has("longitude") && !(data.isNull("longitude"))) {
            this.longitude = data.getDouble("longitude");
        } else {
            this.longitude = 0.0;
        }

        if (data.has("is_transfer_done") && !(data.isNull("is_transfer_done"))) {
            this.is_transfer_done = data.getBoolean("is_transfer_done");
        }

        if (data.has("updated_at") && !(data.isNull("updated_at"))) {
            this.updated_at = data.getString("updated_at");
        } else {
            this.updated_at = "";
        }

        if (data.has("order_sequence") && !(data.isNull("order_sequence"))) {
            this.order_sequence = data.getInt("order_sequence");
        }

        if (data.has("job_step_status_id") && !(data.isNull("job_step_status_id"))) {
            this.job_step_status_id = data.getInt("job_step_status_id");
        }

        if (data.has("job_step_type_id") && !(data.isNull("job_step_type_id"))) {
            this.job_step_type_id = data.getInt("job_step_type_id");
        }

        if (data.has("description") && !(data.isNull("description"))) {
            this.description = data.getString("description");
        } else {
            this.description = "";
        }

        if (data.has("order_detail_ids") && !(data.isNull("order_detail_ids"))) {
            this.order_detail_ids = data.getString("order_detail_ids");
        } else {
            this.order_detail_ids = "";
        }

        if (data.has("job_step_name") && !(data.isNull("job_step_name"))) {
            this.job_step_name = data.getString("job_step_name");
        } else {
            this.job_step_name = "";
        }

        if (data.has("job_step_pic") && !(data.isNull("job_step_pic"))) {
            this.job_step_pic = data.getString("job_step_pic");
        } else {
            this.job_step_pic = "";
        }

        if (data.has("job_step_pic_contact") && !(data.isNull("job_step_pic_contact"))) {
            this.job_step_pic_contact = data.getString("job_step_pic_contact");
        } else {
            this.job_step_pic_contact = "";
        }

        if (data.has("is_signature_required") && !(data.isNull("is_signature_required"))) {
            this.is_signature_required = data.getBoolean("is_signature_required");
        } else {
            this.is_signature_required = false;
        }

        if (data.has("is_scan_required") && !(data.isNull("is_scan_required"))) {
            this.is_scan_required = data.getBoolean("is_scan_required");
        } else {
            this.is_scan_required = false;
        }

        if (data.has("order_attempts") && !(data.isNull("order_attempts"))) {

            JSONArray jsonArray = data.getJSONArray("order_attempts");

            for (int i = 0; i < jsonArray.length(); i++) {
                //Log.i("attempt_length", jsonArray.length() + "");
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                mOrderAttemptsList.add(new OrderAttempt(jsonObject));
            }

        }


    }


    public int getId() {
        return id;
    }

    public String getDropOffTime() {
        return dropOffTime;
    }

    public void setDropOffTime(String dropOffTime) {
        this.dropOffTime = dropOffTime;
    }

    public String getDropOffTimeEnd() {
        return dropOffTimeEnd;
    }

    public void setDropOffTimeEnd(String dropOffTimeEnd) {
        this.dropOffTimeEnd = dropOffTimeEnd;
    }

    public String getUpdated_atDB() {
        return updated_atDB;
    }

    public Boolean isJobStepUpdateWithNoInternet() {
        return isJobStepUpdateWithNoInternet;
    }

    public void setJobStepUpdateWithNoInternet(Boolean jobStepUpdateWithNoInternet) {
        isJobStepUpdateWithNoInternet = jobStepUpdateWithNoInternet;
    }

    public void setUpdated_atDB(String updated_atDB) {
        this.updated_atDB = updated_atDB;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public int getJob_step_type_id() {
        return job_step_type_id;
    }

    public void setJob_step_type_id(int job_step_type_id) {
        this.job_step_type_id = job_step_type_id;
    }

    public int getOrder_sequence() {
        return order_sequence;
    }

    public void setOrder_sequence(int order_sequence) {
        this.order_sequence = order_sequence;
    }

    public int getJob_step_status_id() {
        return job_step_status_id;
    }

    public void setJob_step_status_id(int job_step_status_id) {
        this.job_step_status_id = job_step_status_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrder_detail_ids() {
        return order_detail_ids;
    }

    public void setOrder_detail_ids(String order_detail_ids) {
        this.order_detail_ids = order_detail_ids;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJob_step_name() {
        return job_step_name;
    }

    public void setJob_step_name(String job_step_name) {
        this.job_step_name = job_step_name;
    }

    public String getJob_step_pic() {
        return job_step_pic;
    }

    public void setJob_step_pic(String job_step_pic) {
        this.job_step_pic = job_step_pic;
    }

    public String getJob_step_pic_contact() {
        return job_step_pic_contact;
    }

    public void setJob_step_pic_contact(String job_step_pic_contact) {
        this.job_step_pic_contact = job_step_pic_contact;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean is_transfer_done() {
        return is_transfer_done;
    }

    public void setIs_transfer_done(boolean is_transfer_done) {
        this.is_transfer_done = is_transfer_done;
    }

    public boolean is_signature_required() {
        return is_signature_required;
    }

    public void setIs_signature_required(boolean is_signature_required) {
        this.is_signature_required = is_signature_required;
    }

    public boolean is_scan_required() {
        return is_scan_required;
    }

    public void setIs_scan_required(boolean is_scan_required) {
        this.is_scan_required = is_scan_required;
    }

    public List<OrderAttempt> getmOrderAttemptsList() {
        return mOrderAttemptsList;
    }

    public void setmOrderAttemptsList(List<OrderAttempt> mOrderAttemptsList) {
        this.mOrderAttemptsList = mOrderAttemptsList;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getJSONObjectUpdateOrderSequence() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("job_step_id", id);
            obj.put("order_sequence", order_sequence);
            obj.put("job_step_status_id", job_step_status_id);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectJobSteps() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("job_step_id", id);
            obj.put("job_step_status_id", job_step_status_id);
            obj.put("order_sequence", order_sequence);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

}






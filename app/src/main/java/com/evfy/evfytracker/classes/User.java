package com.evfy.evfytracker.classes;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia-mac on 27/9/15.
 */
public class User implements Serializable {

    private int userId, points, age, orderCount;
    private String firstName, lastName, contactNo, profileImageUrlSmall, profileImageUrlMedium, profileImageUrlBig, gender, code, maritalStatus,
            occupation, nationality, createdOn, email;
    double credit;
    boolean haveChild, haveMaid, disabled, firstLogin;
    boolean isImported;

    public User(JSONObject data) throws JSONException {


        Log.i("", "order detail response:" + data);
        if (data.has("id")) {
            this.userId = data.getInt("id");
        }

        if (data.has("points") && !(data.isNull("points"))) {
            this.points = data.getInt("points");
        }

        if (data.has("first_name") && !(data.isNull("first_name"))) {
            this.firstName = data.getString("first_name");
        }

        if (data.has("last_name") && !(data.isNull("last_name"))) {
            this.lastName = data.getString("last_name");
        }

        if (data.has("contact_no") && !(data.isNull("contact_no"))) {
            this.contactNo = data.getString("contact_no");
        }

        if (data.has("credit") && !(data.isNull("credit"))) {
            this.credit = data.getDouble("credit");
        }

        if (data.has("profile_image_url_small") && !(data.isNull("profile_image_url_small"))) {
            this.profileImageUrlSmall = data.getString("profile_image_url_small");
        }

        if (data.has("profile_image_url_medium") && !(data.isNull("profile_image_url_medium"))) {
            this.profileImageUrlMedium = data.getString("profile_image_url_medium");
        }

        if (data.has("profile_image_url_big") && !(data.isNull("profile_image_url_big"))) {
            this.profileImageUrlBig = data.getString("profile_image_url_big");
        }

        if (data.has("gender") && !(data.isNull("gender"))) {
            this.gender = data.getString("gender");
        }

        if (data.has("code") && !(data.isNull("code"))) {
            this.code = data.getString("code");
        }

        if (data.has("age") && !(data.isNull("age"))) {
            this.age = data.getInt("age");
        }

        if (data.has("marital_status") && !(data.isNull("marital_status"))) {
            this.maritalStatus = data.getString("marital_status");
        }

        if (data.has("have_child") && !(data.isNull("have_child"))) {
            this.haveChild = data.getBoolean("have_child");
        }
        if (data.has("have_maid") && !(data.isNull("have_maid"))) {
            this.haveMaid = data.getBoolean("have_maid");
        }

        if (data.has("occupation") && !(data.isNull("occupation"))) {
            this.occupation = data.getString("occupation");
        }

        if (data.has("nationality") && !(data.isNull("nationality"))) {
            this.nationality = data.getString("nationality");
        }

        if (data.has("orders_count") && !(data.isNull("orders_count"))) {
            this.orderCount = data.getInt("orders_count");
        }

        if (data.has("disabled") && !(data.isNull("disabled"))) {
            this.disabled = data.getBoolean("disabled");
        }

        if (data.has("first_login") && !(data.isNull("first_login"))) {
            this.firstLogin = data.getBoolean("first_login");
        }

        if (data.has("created_on") && !(data.isNull("created_on"))) {
            this.createdOn = data.getString("created_on");
        }

        if (data.has("email") && !(data.isNull("email"))) {
            this.email = data.getString("email");
        }

        if (data.has("is_imported")) {

            if (data.isNull("is_imported")) {
                this.isImported = false;

            } else {
                this.isImported = data.getBoolean("is_imported");

            }
        }

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(int orderCount) {
        this.orderCount = orderCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getProfileImageUrlSmall() {
        return profileImageUrlSmall;
    }

    public void setProfileImageUrlSmall(String profileImageUrlSmall) {
        this.profileImageUrlSmall = profileImageUrlSmall;
    }

    public String getProfileImageUrlMedium() {
        return profileImageUrlMedium;
    }

    public void setProfileImageUrlMedium(String profileImageUrlMedium) {
        this.profileImageUrlMedium = profileImageUrlMedium;
    }

    public String getProfileImageUrlBig() {
        return profileImageUrlBig;
    }

    public void setProfileImageUrlBig(String profileImageUrlBig) {
        this.profileImageUrlBig = profileImageUrlBig;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMarital_status() {
        return maritalStatus;
    }

    public void setMarital_status(String marital_status) {
        this.maritalStatus = marital_status;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public boolean isHaveChild() {
        return haveChild;
    }

    public void setHaveChild(boolean haveChild) {
        this.haveChild = haveChild;
    }

    public boolean isHaveMaid() {
        return haveMaid;
    }

    public void setHaveMaid(boolean haveMaid) {
        this.haveMaid = haveMaid;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(boolean firstLogin) {
        this.firstLogin = firstLogin;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public boolean isImported() {
        return isImported;
    }

    public void setIsImported(boolean isImported) {
        this.isImported = isImported;
    }

}

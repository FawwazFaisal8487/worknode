package com.evfy.evfytracker.classes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia-mac on 27/9/15.
 */
public class OrderStatus implements Serializable {


    private int orderStatusId, stage, orderGeneralId;
    private String orderStatusName, orderViewName;
    private boolean orderIsActive;

    public OrderStatus(JSONObject data) throws JSONException {


        if (data.has("id") && !(data.isNull("id"))) {
            this.orderStatusId = data.getInt("id");
        }

        if (data.has("status") && !(data.isNull("status"))) {
            this.orderStatusName = data.getString("status");
        }

        if (data.has("stage") && !(data.isNull("stage"))) {
            this.stage = data.getInt("stage");
        }

        if (data.has("is_active") && !(data.isNull("is_active"))) {
            this.orderIsActive = data.getBoolean("is_active");
        }

        if (data.has("general_id") && !(data.isNull("general_id"))) {
            this.orderGeneralId = data.getInt("general_id");
        }

        if (data.has("view_name") && !(data.isNull("view_name"))) {
            this.orderViewName = data.getString("view_name");
        }

    }


    public int getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(int orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }


    public int getOrderGeneralId() {
        return orderGeneralId;
    }

    public void setOrderGeneralId(int orderGeneralId) {
        this.orderGeneralId = orderGeneralId;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public String getOrderViewName() {
        return orderViewName;
    }

    public void setOrderViewName(String orderViewName) {
        this.orderViewName = orderViewName;
    }

    public boolean isOrderIsActive() {
        return orderIsActive;
    }

    public void setOrderIsActive(boolean orderIsActive) {
        this.orderIsActive = orderIsActive;
    }
}

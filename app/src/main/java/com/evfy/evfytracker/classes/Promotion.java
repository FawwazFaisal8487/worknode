package com.evfy.evfytracker.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lkh012349s.mobileprinter.Utils.DateTimeOp;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Promotion {

    final static String FORMAT_DATE_TIME = "yyyy-MM-dd HH:mm:ss";

    @JsonProperty("enabled")
    boolean mIsEnabled;
    @JsonProperty("title")
    String mTitle;
    @JsonProperty("start_date")
    String mTimeStart;
    @JsonProperty("end_date")
    String mTimeEnd;

    public Promotion() {
    }

    public boolean isEnabled() {
        return mIsEnabled;
    }

    public String getTimeEnd() {
        return mTimeEnd;
    }

    public String getTimeStart() {
        return mTimeStart;
    }

    public String getTitle() {
        return mTitle;
    }

    public Date getDateTimeStart() {
        return DateTimeOp.getDate(FORMAT_DATE_TIME, mTimeStart, null);
    }

    public Date getDateTimeEnd() {
        return DateTimeOp.getDate(FORMAT_DATE_TIME, mTimeEnd, null);
    }

}

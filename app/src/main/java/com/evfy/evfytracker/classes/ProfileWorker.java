package com.evfy.evfytracker.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia on 22/1/18.
 */

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileWorker implements Serializable, Cloneable {

    private int id;

    private boolean isManPowerWorker;

    public ProfileWorker() {
    }


    public ProfileWorker(JSONObject data) throws JSONException {

        if (data.has("id") && !(data.isNull("id"))) {
            this.id = data.getInt("id");
        }


        if (data.has("is_man_power_worker") && !(data.isNull("is_man_power_worker"))) {
            this.isManPowerWorker = data.getBoolean("is_man_power_worker");
        } else {
            this.isManPowerWorker = false;
        }

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public boolean isManPowerWorker() {
        return isManPowerWorker;
    }

    public void setManPowerWorker(boolean manPowerWorker) {
        isManPowerWorker = manPowerWorker;
    }

//    @Override
//    public Object clone () {
//        try {
//            return super.clone();
//        } catch ( CloneNotSupportedException e ) {
//            e.printStackTrace();
//            return null;
//        }
//    }
}

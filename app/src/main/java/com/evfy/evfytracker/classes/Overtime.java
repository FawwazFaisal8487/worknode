package com.evfy.evfytracker.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia on 20/10/17.
 */

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Overtime implements Serializable, Cloneable {

    private int id, worker_id;
    private String start, end;

    public Overtime() {
    }


    public Overtime(JSONObject data) throws JSONException {

        if (data.has("id") && !(data.isNull("id"))) {
            this.id = data.getInt("id");
        }

        if (data.has("worker_id") && !(data.isNull("worker_id"))) {
            this.worker_id = data.getInt("worker_id");
        }

        if (data.has("start") && !(data.isNull("start"))) {
            this.start = data.getString("start");
        } else {
            this.start = "";
        }

        if (data.has("end") && !(data.isNull("end"))) {
            this.end = data.getString("end");
        } else {
            this.end = "";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
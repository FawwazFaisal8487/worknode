package com.evfy.evfytracker.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by coolasia on 27/10/16.
 */
@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDatabase implements Serializable {

    private int databaseId;
    private String orderId;
    private String orderJson;


    public OrderDatabase() {

    }

    public OrderDatabase(String orderId, String orderJson, Integer databaseId) {
        this.orderId = orderId;
        this.orderJson = orderJson;
        this.databaseId = databaseId;

    }


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderJson() {
        return orderJson;
    }

    public void setOrderJson(String orderJson) {
        this.orderJson = orderJson;
    }

    public Integer getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Integer databaseId) {
        this.databaseId = databaseId;
    }


}

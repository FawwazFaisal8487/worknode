package com.evfy.evfytracker.classes;

import android.util.Log;

import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by Akmal on 14-May-17.
 */

@SuppressWarnings("unused")
public class CustomerDetails implements Serializable {

    private int customer_id, idScanSetting;
    private String reference_no, nameScanSetting, company_name;
    JSONObject scan_settingJSON;

    public CustomerDetails() {
    }


    public CustomerDetails(JSONObject data) throws JSONException {

        Log.i("", "Delivery order:" + data);

//        if ( data.has( "id" ) ) {
//            this.id = data.getInt( "id" );
//        }

        if (data.has("id") && !(data.isNull("id"))) {
            this.customer_id = data.getInt("id");
        } else {
            this.customer_id = 0;

        }

        if (data.has("company_name") && !(data.isNull("company_name"))) {
            this.company_name = data.getString("company_name");
        } else {
            LogCustom.i("CustomerName", "is : " + company_name);
            this.company_name = "";
        }

        if (data.has("reference_no") && !(data.isNull("reference_no"))) {
            this.reference_no = data.getString("reference_no");
        }

        if (data.has("scan_setting") && !(data.isNull("scan_setting"))) {

            scan_settingJSON = data.getJSONObject("scan_setting");
            if (scan_settingJSON.has("id") && !(scan_settingJSON.isNull("id"))) {
                this.idScanSetting = scan_settingJSON.getInt("id");
            }

            if (scan_settingJSON.has("name") && !(scan_settingJSON.isNull("name"))) {
                this.nameScanSetting = scan_settingJSON.getString("name");
            }

        }

    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public int getIdScanSetting() {
        return idScanSetting;
    }

    public void setIdScanSetting(int idScanSetting) {
        this.idScanSetting = idScanSetting;
    }

    public String getNameScanSetting() {
        return nameScanSetting;
    }

    public void setNameScanSetting(String nameScanSetting) {
        this.nameScanSetting = nameScanSetting;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }
}


package com.evfy.evfytracker.classes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by coolasia on 17/1/18.
 */

@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class AppCompanySettings implements Serializable, Cloneable {

    private int id;

    private int currentStatus, toStatus;
    private String rule;
    private boolean enable, isManPowerWorker, isScanRequired;

    public AppCompanySettings() {
    }


    public AppCompanySettings(JSONObject data) throws JSONException {

        if (data.has("id") && !(data.isNull("id"))) {
            this.id = data.getInt("id");
        }

        if (data.has("rule") && !(data.isNull("rule"))) {
            this.rule = data.getString("rule");
        } else {
            this.rule = "";
        }

        if (data.has("enable") && !(data.isNull("enable"))) {
            this.enable = data.getBoolean("enable");
        } else {
            this.enable = false;
        }

        if (data.has("current_status_general_id") && !(data.isNull("current_status_general_id"))) {
            this.currentStatus = data.getInt("current_status_general_id");
        } else {
            this.currentStatus = 0;
        }
        if (data.has("to_status_general_id") && !(data.isNull("to_status_general_id"))) {
            this.toStatus = data.getInt("to_status_general_id");
        } else {
            this.toStatus = 0;
        }
        if (data.has("is_man_power_worker") && !(data.isNull("is_man_power_worker"))) {
            this.isManPowerWorker = data.getBoolean("is_man_power_worker");
        } else {
            this.isManPowerWorker = false;
        }
        if (data.has("is_scan_required") && !(data.isNull("is_scan_required"))) {
            this.isScanRequired = data.getBoolean("is_scan_required");
        } else {
            this.isScanRequired = false;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(int currentStatus) {
        this.currentStatus = currentStatus;
    }

    public int getToStatus() {
        return toStatus;
    }

    public void setToStatus(int toStatus) {
        this.toStatus = toStatus;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public boolean isManPowerWorker() {
        return isManPowerWorker;
    }

    public void setManPowerWorker(boolean manPowerWorker) {
        isManPowerWorker = manPowerWorker;
    }

    public boolean isScanRequired() {
        return isScanRequired;
    }

    public void setScanRequired(boolean scanRequired) {
        isScanRequired = scanRequired;
    }

//    @Override
//    public Object clone () {
//        try {
//            return super.clone();
//        } catch ( CloneNotSupportedException e ) {
//            e.printStackTrace();
//            return null;
//        }
//    }
}

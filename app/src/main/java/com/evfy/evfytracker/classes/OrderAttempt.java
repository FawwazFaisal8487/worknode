package com.evfy.evfytracker.classes;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by coolasia on 27/10/16.
 */
@SuppressWarnings("unused")
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderAttempt implements Serializable {

    @JsonProperty("id")
    private int id;
    @JsonProperty("received_by")
    private String received_by;
    @JsonProperty("nric")
    private String nric;
    @JsonProperty("srno")
    private String srno;


    @JsonProperty("reason")
    private String reason;
    @JsonProperty("note")
    private String note;
    @JsonProperty("submitted_time")
    private String submitted_time;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("order_attempt_image")
    private List<OrderAttemptImage> mOrderAttemptImages = new ArrayList<>();
    private List<OrderAttemptImage> mOrderAttemptImagesEditRemove = new ArrayList<>();


    private Boolean isAttemptUpdateWithNoInternet;
    private int jobStepID, orderId;

    public OrderAttempt() {

    }

    public OrderAttempt(int id, String received_by, String nric, String srno, String reason, String note, String submitted_time, Double latitude,
                        Double longitude, List<OrderAttemptImage> order_attempt_image, List<OrderAttemptImage> mOrderAttemptImagesEditRemove) {
        this.id = id;
        this.received_by = received_by;
        this.nric = nric;
        this.srno = srno;
        this.reason = reason;
        this.note = note;
        this.submitted_time = submitted_time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mOrderAttemptImages = order_attempt_image;
        this.mOrderAttemptImagesEditRemove = mOrderAttemptImagesEditRemove;
    }

    public OrderAttempt(JSONObject data) throws JSONException {

        if (data.has("id")) {
            this.id = data.getInt("id");
        }

        if (data.has("received_by") && !(data.isNull("received_by"))) {
            this.received_by = data.getString("received_by");
        } else {
            this.received_by = "";
        }

        if (data.has("nric") && !(data.isNull("nric"))) {
            this.nric = data.getString("nric");
        } else {
            this.nric = "";
        }

        if (data.has("srno") && !(data.isNull("srno"))) {
            this.srno = data.getString("srno");
        } else {
            this.srno = "";
        }

        if (data.has("reason") && !(data.isNull("reason"))) {
            this.reason = data.getString("reason");
        } else {
            this.reason = "";
        }

        if (data.has("note") && !(data.isNull("note"))) {
            this.note = data.getString("note");
        } else {
            this.note = "";
        }

        if (data.has("submitted_time") && !(data.isNull("submitted_time"))) {
            this.submitted_time = data.getString("submitted_time");
        }

        if (data.has("latitude") && !(data.isNull("latitude"))) {
            this.latitude = data.getDouble("latitude");
        }

        if (data.has("longitude") && !(data.isNull("longitude"))) {
            this.longitude = data.getDouble("longitude");
        }

        if (data.has("order_attempts_images") && !(data.isNull("order_attempts_images"))) {
            JSONArray itemArray = data.getJSONArray("order_attempts_images");
            Log.i("itemarray", itemArray + "");
            for (int i = 0; i < itemArray.length(); i++) {
                this.mOrderAttemptImages.add(new OrderAttemptImage(itemArray.getJSONObject(i)));
            }

        }

    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getJobStepID() {
        return jobStepID;
    }

    public void setJobStepID(int jobStepID) {
        this.jobStepID = jobStepID;
    }

    public Boolean isAttemptUpdateWithNoInternet() {
        return isAttemptUpdateWithNoInternet;
    }

    public void setAttemptUpdateWithNoInternet(Boolean attemptUpdateWithNoInternet) {
        isAttemptUpdateWithNoInternet = attemptUpdateWithNoInternet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReceived_by() {
        return received_by;
    }

    public void setReceived_by(String received_by) {
        this.received_by = received_by;
    }

    public String getNric() {
        return nric;
    }

    public void setNric(String nric) {
        this.nric = nric;
    }

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSubmitted_time() {
        return submitted_time;
    }

    public void setSubmitted_time(String submitted_time) {
        this.submitted_time = submitted_time;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<OrderAttemptImage> getmOrderAttemptImages() {
        return mOrderAttemptImages;
    }

    public void setmOrderAttemptImages(List<OrderAttemptImage> mOrderAttemptImages) {
        this.mOrderAttemptImages = mOrderAttemptImages;
    }

    public JSONObject getJSONObjectJobStepFailed() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("reason", reason);
            obj.put("submitted_time", submitted_time);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);


            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("image_url", getmOrderAttemptImages().get(i).getUrl());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectJobSteps() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("received_by", received_by);
            obj.put("srno", srno);
            obj.put("nric", nric);
            obj.put("reason", reason);
            obj.put("note", note);
            obj.put("submitted_time", submitted_time);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);


            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("image_url", getmOrderAttemptImages().get(i).getUrl());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectJobStepsWithoutImage() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("received_by", received_by);
            obj.put("srno", srno);
            obj.put("nric", nric);
            obj.put("reason", reason);
            obj.put("note", note);
            obj.put("submitted_time", submitted_time);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("received_by", received_by);
            obj.put("srno", srno);
            obj.put("nric", nric);
            obj.put("reason", reason);
            obj.put("note", note);
            obj.put("submitted_time", submitted_time);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);


            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("raw", getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image_base64", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectURLWithID() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("order_attempt_id", id);
            obj.put("received_by", received_by);
            obj.put("srno", srno);
            obj.put("nric", nric);
            obj.put("reason", reason);
            obj.put("note", note);
            obj.put("submitted_time", submitted_time);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);


            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("image_url", getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectWithID() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("order_attempt_id", id);
            obj.put("received_by", received_by);
            obj.put("srno", srno);
            obj.put("nric", nric);
            obj.put("reason", reason);
            obj.put("note", note);
            obj.put("submitted_time", submitted_time);
            obj.put("latitude", latitude);
            obj.put("longitude", longitude);


            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("raw", getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image_base64", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }


    public JSONObject getJSONObjectWithIDRepeatImage() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("order_attempt_id", id);

            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("raw", getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image_base64", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectEditRemove() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("order_attempt_id", id);

            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());


                orderAttemtImageDetailObject.put("attempt_image_id", getmOrderAttemptImages().get(i).getId());
                orderAttemtImageDetailObject.put("remove", getmOrderAttemptImages().get(i).isRemove());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());


                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image_base64", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }

    public JSONObject getJSONObjectImageOnly() {
        JSONObject obj = new JSONObject();
        try {
            JSONArray array = new JSONArray();

            JSONObject orderAttemtImageDetailObject;
            JSONArray orderAttemtImageDetailArray = new JSONArray();

            for (int i = 0; i < getmOrderAttemptImages().size(); i++) {

                orderAttemtImageDetailObject = new JSONObject();
                //array.put(getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("raw", getmOrderAttemptImages().get(i).getBase64());
                orderAttemtImageDetailObject.put("is_signature", getmOrderAttemptImages().get(i).isSignature());
                orderAttemtImageDetailObject.put("description", getmOrderAttemptImages().get(i).getNote());

                orderAttemtImageDetailArray.put(orderAttemtImageDetailObject);

            }

            obj.put("order_attempt_image_base64", orderAttemtImageDetailArray);

        } catch (JSONException e) {
            //trace("DefaultListItem.toString JSONException: "+e.getMessage());
        }
        return obj;
    }
}

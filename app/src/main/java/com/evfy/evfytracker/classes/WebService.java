package com.evfy.evfytracker.classes;

import android.util.Log;
import android.util.Pair;

import com.evfy.evfytracker.Constants;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.LogCustom;

import org.json.JSONObject;

import java.util.List;

import app.juaagugui.httpService.listeners.OnRESTResultCallback;
import app.juaagugui.httpService.model.HttpConnection;

public class WebService {

	public final String mUrl;
	public final int mMethod;

	/**
	 * Constants header.
	 */
	public final Pair<String, String>[] mPairs;

	public WebService(final String url, final int method, final Pair<String, String>... pairs) {
		mMethod = method;
		mPairs = pairs;
		mUrl = Constants.SAAS_SERVER_LDS + url;
	}

	public WebService(final String url, final int method) {
		mMethod = method;
		mPairs = null;
		mUrl = Constants.SAAS_SERVER_LDS + url;
		LogCustom.i(mUrl, "mUrl");
		LogCustom.i(Constants.SAAS_SERVER_LDS, "Constants.SAAS_SERVER_LDS");
	}

	public HttpConnection getHttpConnection(final List<Pair<String, String>> header, final String body,
											final OnRESTResultCallback onRESTResultCallback, final Object... params) {

		if (JavaOp.ifNullThenLog(onRESTResultCallback)) return null;
		final String url = JavaOp.isArrayNullOrEmpty(params) ? mUrl : String.format(mUrl, params);
		LogCustom.i(url, "url");
		final HttpConnection httpConnection = new HttpConnection(url, mMethod, body, onRESTResultCallback);


		Log.i("", "http connection body:" + httpConnection.getBodyData());
		if (header != null) {

			if (!JavaOp.isNullOrEmpty(mPairs)) {
				for (final Pair<String, String> pair : mPairs) {
					if (JavaOp.ifNullThenLog(pair)) continue;
					header.add(pair);
				}
			}

			httpConnection.setHeaders(header);

		}

		return httpConnection;

	}

	public HttpConnection getHttpConnection(final List<Pair<String, String>> header, final OnRESTResultCallback onRESTResultCallback,
											final String filePath, final Object... params) {

		if (JavaOp.ifNullOrEmptyThenLog(onRESTResultCallback, filePath)) return null;
		final String url = JavaOp.isArrayNullOrEmpty(params) ? mUrl : String.format(mUrl, params);
		LogCustom.i(url, "url");
		final HttpConnection httpConnection = new HttpConnection(url, filePath, mMethod, onRESTResultCallback);

		if (header != null) {

			if (!JavaOp.isNullOrEmpty(mPairs)) {
				for (final Pair<String, String> pair : mPairs) {
					if (JavaOp.ifNullThenLog(pair)) continue;
					header.add(pair);
				}
			}

			httpConnection.setHeaders(header);

		}

		return httpConnection;

	}

	public HttpConnection getHttpConnection(final List<Pair<String, String>> header, final JSONObject body,
											final OnRESTResultCallback onRESTResultCallback, final Object... params) {

		final HttpConnection httpConnection = new HttpConnection(JavaOp.isArrayNullOrEmpty(params) ? mUrl : String.format(mUrl, params), mMethod,
				body, onRESTResultCallback);

		if (header != null) {

			if (!JavaOp.isNullOrEmpty(mPairs)) {
				for (final Pair<String, String> pair : mPairs) {
					if (JavaOp.ifNullThenLog(pair)) continue;
					header.add(pair);
				}
			}

			httpConnection.setHeaders(header);

		}

		return httpConnection;

	}

	@Override
	public String toString() {
		return String.format("url: %s --- method: %d", mUrl, mMethod);
	}

}

package com.evfy.evfytracker.classes;

import android.util.Log;

import com.evfy.evfytracker.Constants;
import com.lkh012349s.mobileprinter.Utils.DateTimeOp;
import com.lkh012349s.mobileprinter.Utils.JavaOp;
import com.lkh012349s.mobileprinter.Utils.StringOp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class JobOrder implements Serializable {

    private int orderId, orderStatusId, userId, pickUpWorkerId, dropOffWorkerId, dropOffDistrictId, pickUpDistrictId, factoryId, speedRating,
            atitudeRating, wmsOrderId, orderSequenceTotal;
    private String description;
    private String pickUpAddress;
    private String pickUpPostalCode;
    private String pickUpApartmentType;
    private String extraData = "";
    private String dropOffAddress;
    private String dropOffPostalCode;
    private String dropOffApartmentType;
    private String createdOn;
    private String pickUpDate;
    private String pickUpTime;
    private String dropOffDate;
    private String dropOffTime;
    private String review;
    private String payPalRefNo;
    private String paymentMode;
    private String urlQRCode;
    private String pickUpWorker;
    private String dropOffWorker;
    private String source;
    private String recipientCompanyName;
    private String factoryReceivedDate;
    private String factoryCompleteDate;
    private String dropOffDateES;
    private String contactNum = "";
    private String pickUpContact;
    private String dropOffContact = "";
    private String pickUpDesc;
    private String dropOffDesc;
    private String pickUpContactName;
    private String dropOffContactName;
    private String masterTrackingNumber;
    private String itemTrackingNumber;
    private String senderName;
    private String mawb;
    private String reference_no;
    private String dropOffPic;
    private String pickupPic;
    private String dropOffTo;
    private String companyName;
    private String updatedAt;
    private String dropOffTimeEnd;
    private String orderNumber;
    private String dropOffTimeStart;
    private String driverNotes;

    public String getDriverNotes() {
        return driverNotes;
    }

    public void setDriverNotes(String driverNotes) {
        this.driverNotes = driverNotes;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    private String jobType;
    private double totalPrice, toPayPrice, voucher, total_kg;
    private boolean lazyOrder, expressOrder, pickUpChanged, deliverChanged, paid, workerChecked, userChecked, payLater, free;
    private boolean isPickUp;
    private boolean isUpdateNeeded;
    private boolean isMerged;
    private boolean isImported;
    private boolean isDelivery;
    private Boolean isUpdateWithNoInternet, isReattemptJob, isRemoved;
    private Integer reattemptJobStepId, total_package;

    private String updatedAtDB, dropOffTimePlanned;

    public boolean isDelivery() {
        return isDelivery;
    }

    public void setIsDelivery(boolean isDelivery) {
        this.isDelivery = isDelivery;
    }

    private boolean isSelected;
    private com.evfy.evfytracker.classes.OrderStatus orderStatusObject;
    private com.evfy.evfytracker.classes.CustomerDetails customerObject;
    private User user;
    private OrderDetails[] orderDetails;
    private JobSteps[] jobSteps;
    private OrderImage[] mOrderImages;
    private String[] mergedOrderIds;
    private List<JobOrder> mJobOrderMergeds = new ArrayList<>();
    private List<OrderAttempt> mOrderAttemptsList = new ArrayList<>();
    private List<OrderDetails> orderDetailsArrayList = new ArrayList<>();
    private List<JobSteps> jobStepsArrayList = new ArrayList<>();
    private List<Overtime> mOvertimeList = new ArrayList<>();

    public JobOrder() {
    }

    public Boolean getRemoved() {
        return isRemoved;
    }

    public void setRemoved(Boolean removed) {
        isRemoved = removed;
    }

    public Integer getReattemptJobStepId() {
        return reattemptJobStepId;
    }

    public void setReattemptJobStepId(Integer reattemptJobStepId) {
        this.reattemptJobStepId = reattemptJobStepId;
    }

    public String getDropOffTimeStart() {
        return dropOffTimeStart;
    }

    public void setDropOffTimeStart(String dropOffTimeStart) {
        this.dropOffTimeStart = dropOffTimeStart;
    }

    public Boolean isReattemptJob() {
        return isReattemptJob;
    }

    public void setReattemptJob(Boolean reattemptJob) {
        isReattemptJob = reattemptJob;
    }

    public Boolean isUpdateWithNoInternet() {
        return isUpdateWithNoInternet;
    }

    public void setUpdateWithNoInternet(Boolean updateWithNoInternet) {
        isUpdateWithNoInternet = updateWithNoInternet;
    }

    public String getDropOffDesc() {
        return dropOffDesc;
    }

    public void setDropOffDesc(String dropOffDesc) {
        this.dropOffDesc = dropOffDesc;
    }

    public String getRecipientCompanyName() {
        return recipientCompanyName;
    }

    public void setRecipientCompanyName(String recipientCompanyName) {
        this.recipientCompanyName = recipientCompanyName;
    }

    public String getPickUpContactName() {
        return pickUpContactName;
    }

    public void setPickUpContactName(String pickUpContactName) {
        this.pickUpContactName = pickUpContactName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public String getDropOffContactName() {
        return dropOffContactName;
    }

    public void setDropOffContactName(String dropOffContactName) {
        this.dropOffContactName = dropOffContactName;
    }

    public String getMasterTrackingNumber() {
        return masterTrackingNumber;
    }

    public void setMasterTrackingNumber(String masterTrackingNumber) {
        this.masterTrackingNumber = masterTrackingNumber;
    }

    public String getItemTrackingNumber() {
        return itemTrackingNumber;
    }

    public void setItemTrackingNumber(String itemTrackingNumber) {
        this.itemTrackingNumber = itemTrackingNumber;
    }

    public String getUpdatedAtDB() {
        return updatedAtDB;
    }

    public void setUpdatedAtDB(String updatedAtDB) {
        this.updatedAtDB = updatedAtDB;
    }

    public String getMawb() {
        return mawb;
    }

    public void setMawb(String mawb) {
        this.mawb = mawb;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getOrderSequenceTotal() {
        return orderSequenceTotal;
    }

    public void setOrderSequenceTotal(int orderSequenceTotal) {
        this.orderSequenceTotal = orderSequenceTotal;
    }

    public void setExtraData(String extra_data) {
        this.extraData = extra_data;
    }

    public String getExtraData() {
        return extraData;
    }

    public JobOrder(JSONObject data) throws JSONException {

        Log.i("", "job order details:" + data);
        if (data.has("extra_data") && !(data.isNull("extra_data"))) {
            String[] extraDataJson = data.getString("extra_data").replace("{\"", "").replace("\"}", "").replace("\":\"", ":").replace("\",\"", ", ").split(",");
            for (String a : extraDataJson) {
                if (a.split(":").length > 1 && a.split(":")[1] != null && !a.split(":")[1].equals("")) {
                    extraData += a;
                }
            }
        } else {
            extraData = "";
        }
        if (data.has("id") && !(data.isNull("id"))) {
            this.orderId = data.getInt("id");
        } else {
            this.orderId = 0;

        }

        if (data.has("order_sequence") && !(data.isNull("order_sequence"))) {
            this.orderSequenceTotal = data.getInt("order_sequence");
        } else {
            this.orderSequenceTotal = 0;

        }

        if (data.has("pickup_worker_id") && !(data.isNull("pickup_worker_id"))) {
            this.pickUpWorkerId = data.getInt("pickup_worker_id");
        }

        if (data.has("drop_off_worker_id") && !(data.isNull("drop_off_worker_id"))) {
            this.dropOffWorkerId = data.getInt("drop_off_worker_id");
        } else {
            this.dropOffWorkerId = 0;

        }

        if (data.has("user_id") && !(data.isNull("user_id"))) {
            this.userId = data.getInt("user_id");
        } else {
            this.userId = 0;
        }

        if (data.has("order_status_id")) {
            this.orderStatusId = data.getInt("order_status_id");
        }

        if (data.has("wms_order_id") && !(data.isNull("wms_order_id"))) {
            this.wmsOrderId = data.getInt("wms_order_id");
        } else {
            this.wmsOrderId = 0;
        }

        if (data.has("drop_off_district_id") && !(data.isNull("drop_off_district_id"))) {
            this.dropOffDistrictId = data.getInt("drop_off_district_id");
        } else {
            this.dropOffDistrictId = 0;
        }

        if (data.has("pickup_district_id") && !(data.isNull("pickup_district_id"))) {
            this.pickUpDistrictId = data.getInt("pickup_district_id");
        } else {
            this.pickUpDistrictId = 0;

        }

        if (data.has("factory_id") && !(data.isNull("factory_id"))) {
            this.factoryId = data.getInt("factory_id");
        } else {
            this.factoryId = 0;

        }

        if (data.has("pickup_pic") && !(data.isNull("pickup_pic"))) {
            this.pickupPic = data.getString("pickup_pic");
        } else {
            this.pickupPic = "";

        }

        if (data.has("drop_off_pic") && !(data.isNull("drop_off_pic"))) {
            this.dropOffPic = data.getString("drop_off_pic");
        } else {
            this.dropOffPic = "";

        }

        if (data.has("drop_off_contact_name") && !(data.isNull("drop_off_contact_name"))) {
            this.recipientCompanyName = data.getString("drop_off_contact_name");
        } else {
            this.recipientCompanyName = "";

        }

        if (data.has("driver_notes") && !(data.isNull("driver_notes"))) {
            this.driverNotes = data.getString("driver_notes");
        } else {
            this.driverNotes = "";

        }

        if (data.has("job_type") && !(data.isNull("job_type"))) {
            this.jobType = data.getString("job_type");
        } else {
            this.jobType = "";

        }

        if (data.has("pickup_address") && !(data.isNull("pickup_address"))) {
            this.pickUpAddress = data.getString("pickup_address");
        } else {
            this.pickUpAddress = "";
        }

        if (data.has("pickup_postal_code") && !(data.isNull("pickup_postal_code"))) {
            this.pickUpPostalCode = data.getString("pickup_postal_code");
        }

        if (data.has("pickup_apartment_type") && !(data.isNull("pickup_apartment_type"))) {
            this.pickUpApartmentType = data.getString("pickup_apartment_type");
        }

        if (data.has("drop_off_address") && !(data.isNull("drop_off_address"))) {
            this.dropOffAddress = data.getString("drop_off_address");
        } else {
            this.dropOffAddress = "";
        }

        if (data.has("drop_off_to") && !(data.isNull("drop_off_to"))) {
            this.dropOffTo = data.getString("drop_off_to");
        } else {
            this.dropOffTo = "";
        }

        if (data.has("drop_off_postal_code") && !(data.isNull("drop_off_postal_code"))) {
            this.dropOffPostalCode = data.getString("drop_off_postal_code");
        }

        if (data.has("drop_off_apartment_type") && !(data.isNull("drop_off_apartment_type"))) {
            this.dropOffApartmentType = data.getString("drop_off_apartment_type");
        }

        if (data.has("drop_off_apartment_type") && !(data.isNull("drop_off_apartment_type"))) {
            this.dropOffApartmentType = data.getString("drop_off_apartment_type");
        }

        if (data.has("speed_rating") && !(data.isNull("speed_rating"))) {
            this.speedRating = data.getInt("speed_rating");
        }

        if (data.has("attitude_rating") && !(data.isNull("attitude_rating"))) {
            this.atitudeRating = data.getInt("attitude_rating");
        }
        if (data.has("created_on") && !(data.isNull("created_on"))) {
            this.createdOn = data.getString("created_on");
        }

        if (data.has("pickup_time") && !(data.isNull("pickup_time"))) {
            this.pickUpTime = data.getString("pickup_time");
        }

        if (data.has("drop_off_date") && !(data.isNull("drop_off_date"))) {
            this.dropOffDate = data.getString("drop_off_date");
        }

        if (data.has("updated_at") && !(data.isNull("updated_at"))) {
            this.updatedAt = data.getString("updated_at");
        }

        if (data.has("drop_off_time_end") && !(data.isNull("drop_off_time_end"))) {
            this.dropOffTimeEnd = data.getString("drop_off_time_end");
        } else {
            this.dropOffTimeEnd = "";
        }

        if (data.has("drop_off_time") && !(data.isNull("drop_off_time"))) {
            this.dropOffTime = data.getString("drop_off_time");
        } else {
            this.dropOffTime = "";
        }

        if (data.has("review") && !(data.isNull("review"))) {
            this.review = data.getString("review");
        }

        if (data.has("pickup_changed") && !(data.isNull("pickup_changed"))) {
            this.pickUpChanged = data.getBoolean("pickup_changed");
        }

        if (data.has("deliver_changed") && !(data.isNull("deliver_changed"))) {
            this.deliverChanged = data.getBoolean("deliver_changed");
        }

        if (data.has("worker_checked") && !(data.isNull("worker_checked"))) {
            this.workerChecked = data.getBoolean("worker_checked");
        }

        if (data.has("user_checked") && !(data.isNull("user_checked"))) {
            this.userChecked = data.getBoolean("user_checked");
        }

        if (data.has("order_source") && !data.isNull("order_source")) {
            final JSONObject jsonObject = data.getJSONObject("order_source");
            source = jsonObject.getString("name");
        }

        if (data.has("qr_code_url") && !(data.isNull("qr_code_url"))) {
            this.urlQRCode = data.getString("qr_code_url");
        }

        //signature_url
        //factory_worker_id

        if (data.has("factory_received_date") && !(data.isNull("factory_received_date"))) {
            this.factoryReceivedDate = data.getString("factory_received_date");
        }

        if (data.has("factory_completed_date") && !(data.isNull("factory_completed_date"))) {
            this.factoryCompleteDate = data.getString("factory_completed_date");
        }

        if (data.has("is_merged") && !(data.isNull("is_merged"))) {
            this.isMerged = data.getBoolean("is_merged");
        }

        //order_number

        if (data.has("drop_off_name") && !(data.isNull("drop_off_name"))) {
            this.dropOffContactName = data.getString("drop_off_name");
        } else {
            this.dropOffContactName = "";
        }

        if (data.has("order_number") && !(data.isNull("order_number"))) {
            this.orderNumber = data.getString("order_number");
        } else {
            this.orderNumber = "";
        }


        if (data.has("is_imported") && !(data.isNull("is_imported"))) {
            this.isImported = data.getBoolean("is_imported");
        }

        if (data.has("master_tracking_number") && !(data.isNull("master_tracking_number"))) {
            this.masterTrackingNumber = data.getString("master_tracking_number");
        }

        if (data.has("sender_name") && !(data.isNull("sender_name"))) {
            this.senderName = data.getString("sender_name");
        }

        if (data.has("mawb") && !(data.isNull("mawb"))) {
            this.mawb = data.getString("mawb");
        }

        if (data.has("item_tracking_number") && !(data.isNull("item_tracking_number"))) {
            this.itemTrackingNumber = data.getString("item_tracking_number");
        } else {
            this.itemTrackingNumber = "";
        }

        if (data.has("pickup_contact_name") && !(data.isNull("pickup_contact_name"))) {
            this.pickUpContactName = data.getString("pickup_contact_name");
        } else {
            this.pickUpContactName = "";

        }

        if (data.has("reference_no") && !(data.isNull("reference_no"))) {
            this.reference_no = data.getString("reference_no");
            Log.i("reference is : ", this.reference_no);
        } else {
            this.reference_no = "";
        }

        if (data.has("total_kg") && !(data.isNull("total_kg"))) {
            this.total_kg = data.getDouble("total_kg");
        } else {
            this.total_kg = 0.0;
        }

        if (data.has("total_package") && !(data.isNull("total_package"))) {
            this.total_package = data.getInt("total_package");
        } else {
            this.total_package = 0;
        }


        if (data.has("description")) {
            this.description = data.getString("description");
        } else {
            this.description = "";
        }

        if (data.has("drop_off_time_planned")) {
            if (data.getString("drop_off_time_planned").equalsIgnoreCase("null")) {
                this.dropOffTimePlanned = "";
            } else {
                this.dropOffTimePlanned = data.getString("drop_off_time_planned");
            }

        } else {
            this.dropOffTimePlanned = "";
        }

        if (data.has("drop_off_description") && !(data.isNull("drop_off_description"))) {
            this.dropOffDesc = data.getString("drop_off_description");
            Log.i("remarks is : ", this.dropOffDesc);
        } else {
            this.dropOffDesc = "";
        }

        if (data.has("company_name") && !(data.isNull("company_name"))) {
            this.companyName = data.getString("company_name");
        } else {
            this.companyName = "";
        }

        // TODO
        if (data.has("whatisthis")) {
            this.contactNum = data.getString("whatisthis");
        }

        if (data.has("pickup_contact_no")) {
            this.pickUpContact = data.getString("pickup_contact_no");
        }

        if (data.has("drop_off_contact_no") && !(data.isNull("drop_off_contact_no"))) {
            this.dropOffContact = data.getString("drop_off_contact_no");
        } else {
            this.dropOffContact = "";

        }


        if (data.has("lazy_order") && !(data.isNull("lazy_order"))) {
            this.lazyOrder = data.getBoolean("lazy_order");
        } else {
            this.lazyOrder = false;

        }


        if (data.has("express_order") && !(data.isNull("express_order"))) {
            this.expressOrder = data.getBoolean("express_order");
        } else {
            this.expressOrder = false;

        }

        if (data.has("total_price") && !(data.isNull("total_price"))) {
            this.totalPrice = data.getDouble("total_price");
        }

        if (data.has("paypal_ref_no") && !(data.isNull("paypal_ref_no"))) {
            this.payPalRefNo = data.getString("paypal_ref_no");
        }

        if (data.has("paid") && !(data.isNull("paid"))) {
            this.paid = data.getBoolean("paid");
        }

        if (data.has("pay_later") && !(data.isNull("pay_later"))) {
            this.payLater = data.getBoolean("pay_later");
        }

        if (data.has("payment_mode") && !(data.isNull("payment_mode"))) {
            this.paymentMode = data.getString("payment_mode");
        }

        if (data.has("to_pay_price") && !(data.isNull("to_pay_price"))) {
            this.toPayPrice = data.getDouble("to_pay_price");
        }

        if (data.has("free") && !(data.isNull("free"))) {
            this.free = data.getBoolean("free");
        }


        if (data.has("order_status") && !(data.isNull("order_status"))) {
            JSONObject orderStatusObject = data.getJSONObject("order_status");
            Log.i("", "order status object:" + orderStatusObject);
            this.orderStatusObject = new OrderStatus(orderStatusObject);
        }

        if (data.has("user") && !(data.isNull("user"))) {
            JSONObject userObject = data.getJSONObject("user");
            this.user = new User(userObject);
        }

        if (data.has("voucher") && !(data.isNull("voucher"))) {
            JSONObject jsonObject = data.getJSONObject("voucher");
            voucher = jsonObject.getDouble("value");
        }

        if (data.has("pickup_worker") && !data.isNull("pickup_worker")) {
            final JSONObject jsonObject = data.getJSONObject("pickup_worker");
            pickUpWorker = StringOp.formatGiveEmptyStringIfNull("%s %s", jsonObject.getString("first_name"), jsonObject.getString("last_name"));
        }

        if (data.has("drop_off_worker") && !data.isNull("drop_off_worker")) {
            final JSONObject jsonObject = data.getJSONObject("drop_off_worker");
            dropOffWorker = StringOp.formatGiveEmptyStringIfNull("%s %s", jsonObject.getString("first_name"), jsonObject.getString("last_name"));
        }


        if (data.has("pickup_date") && !(data.isNull("pickup_date"))) {

            this.pickUpDate = data.getString("pickup_date");
            final Date date = DateTimeOp.getDate(Constants.TIME_FORMAT_FROM_WEB_SERVICE, pickUpDate, null);

            if (date != null) {

                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                final int count = expressOrder ? 1 : 4;
                calendar.add(Calendar.DATE, count);

				/*//				LogCustom.i("===============================");
s
				final int count = expressOrder ? 2 : 5;
				//				LogCustom.i( count , "count" );
				//				LogCustom.i( calendar.getTime() , "calendar start" );

				for ( int i = 0 ; i < count ; i++ ) {
					calendar.add( Calendar.DATE, 1 );
					while ( JavaOp.contain( calendar.get( Calendar.DAY_OF_WEEK ), Calendar.SATURDAY, Calendar.SUNDAY ) ) calendar.add( Calendar.DATE, 1 );
					//					LogCustom.i( calendar.getTime() , "calendar" );
				}*/

                dropOffDateES = DateTimeOp.getString(Constants.TIME_FORMAT_FROM_WEB_SERVICE, calendar.getTime(), null);
                //				LogCustom.i( calendar.getTime() , "calendar end" );

            }

        }

        if (data.has("order_details") && !(data.isNull("order_details"))) {

            JSONArray orderDetailsArray = data.getJSONArray("order_details");
            orderDetails = new OrderDetails[orderDetailsArray.length()];

            for (int i = 0; i < orderDetailsArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) orderDetailsArray.get(i);
                orderDetails[i] = new OrderDetails(jsonObject);

                orderDetailsArrayList.add(new OrderDetails(jsonObject));
            }

        }

        if (data.has("job_steps") && !(data.isNull("job_steps"))) {

            JSONArray jobStepsArray = data.getJSONArray("job_steps");
            jobSteps = new JobSteps[jobStepsArray.length()];

            for (int i = 0; i < jobStepsArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jobStepsArray.get(i);
                jobSteps[i] = new JobSteps(jsonObject);
                jobStepsArrayList.add(new JobSteps(jsonObject));

            }

        }

        if (data.has("order_attempts") && !(data.isNull("order_attempts"))) {

            JSONArray jsonArray = data.getJSONArray("order_attempts");

            for (int i = 0; i < jsonArray.length(); i++) {
                //Log.i("attempt_length", jsonArray.length() + "");
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                mOrderAttemptsList.add(new OrderAttempt(jsonObject));
            }

        }

        Log.i("attempt_size", mOrderAttemptsList.size() + "");


        // Overtime

        if (data.has("commissions_and_overtimes") && !(data.isNull("commissions_and_overtimes"))) {

            JSONArray jsonArray = data.getJSONArray("commissions_and_overtimes");

            for (int i = 0; i < jsonArray.length(); i++) {
                //Log.i("attempt_length", jsonArray.length() + "");
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                mOvertimeList.add(new Overtime(jsonObject));
            }

        }

        Log.i("overtimes", mOvertimeList.size() + "");

        // Overtime


        if (data.has("order_images") && !(data.isNull("order_images"))) {

            JSONArray jsonArray = data.getJSONArray("order_images");
            mOrderImages = new OrderImage[jsonArray.length()];

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                mOrderImages[i] = new OrderImage(jsonObject);
            }

        }


        if (data.has("customer") && !(data.isNull("customer"))) {
            JSONObject customerObject = data.getJSONObject("customer");
            Log.i("", "customer object:" + customerObject);
            this.customerObject = new CustomerDetails(customerObject);
        }


        if (data.has("merged_order_ids") && !(data.isNull("merged_order_ids"))) {

            String orderIds = data.getString("merged_order_ids");

            if (!JavaOp.isNullOrEmpty(orderIds)) {
                orderIds = orderIds.replaceAll("[\\[\\] ]", "");
                final ArrayList<String> mergedOrderIdsList = new ArrayList<>(Arrays.asList(orderIds.split(",")));
                mergedOrderIdsList.remove(String.valueOf(orderId));
                this.mergedOrderIds = mergedOrderIdsList.toArray(new String[mergedOrderIdsList.size()]);
            }

        }

        isPickUp = !JavaOp.contain(orderStatusId, Constants.LAUNDRY_DROPPED_OFF, Constants.DISPATCHED, Constants.DROP_OFF_FAILED, Constants.AWAITING_DROP_OFF,
                Constants.COMPLETE) || JavaOp.isNullOrEmpty(dropOffTime, dropOffDate);

    }


    public String getDropOffTimePlanned() {
        return dropOffTimePlanned;
    }

    public void setDropOffTimePlanned(String dropOffTimePlanned) {
        this.dropOffTimePlanned = dropOffTimePlanned;
    }

    public List<JobSteps> getJobStepsArrayList() {
        return jobStepsArrayList;
    }

    public void setJobStepsArrayList(List<JobSteps> jobStepsArrayList) {
        ArrayList<JobSteps> resultList = new ArrayList<>();
        for (JobSteps jobSteps : jobStepsArrayList) {
            if (!jobSteps.getJob_step_pic_contact().isEmpty()) {
                resultList.add(jobSteps);
            }
        }
        this.jobStepsArrayList = resultList;
    }

    public JobSteps[] getJobSteps() {
        return jobSteps;
    }

    public void setJobSteps(JobSteps[] jobSteps) {
        this.jobSteps = jobSteps;
    }

    public String getDropOffTimeEnd() {
        return dropOffTimeEnd;
    }

    public void setDropOffTimeEnd(String dropOffTimeEnd) {
        this.dropOffTimeEnd = dropOffTimeEnd;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public boolean isImported() {
        return isImported;
    }

    public void setIsImported(final boolean isImported) {
        this.isImported = isImported;
    }

    public List<JobOrder> getJobOrderMergeds() {
        return mJobOrderMergeds;
    }

    public void setJobOrderMergeds(final List<JobOrder> jobOrderMergeds) {
        mJobOrderMergeds = jobOrderMergeds;
    }

    public boolean isMerged() {
        return isMerged;
    }

    public void setIsMerged(final boolean isMerged) {
        this.isMerged = isMerged;
    }

    public String[] getMergedOrderIds() {
        return mergedOrderIds;
    }

    public void setMergedOrderIds(final String[] mergedOrderIds) {
        this.mergedOrderIds = mergedOrderIds;
    }

    public void setIsUpdateNeeded(final boolean isUpdateNeeded) {
        this.isUpdateNeeded = isUpdateNeeded;
    }

    public String getContactNum() {
        return contactNum;
    }

    public void setContactNum(final String contactNum) {
        this.contactNum = contactNum;
    }


    public String getPickUpContact() {
        return pickUpContact;
    }

    public void setPickUpContact(String pickUpContact) {
        this.pickUpContact = pickUpContact;
    }

    public String getDropOffContact() {
        return dropOffContact;
    }

    public void setDropOffContact(String dropOffContact) {
        this.dropOffContact = dropOffContact;
    }

    public String getDropOffDateES() {
        return dropOffDateES;
    }

    public void setDropOffDateES(final String dropOffDateES) {
        this.dropOffDateES = dropOffDateES;
    }

    public List<OrderAttempt> getmOrderAttemptsList() {
        return mOrderAttemptsList;
    }

    public void setmOrderAttemptsList(List<OrderAttempt> mOrderAttemptsList) {
        this.mOrderAttemptsList = mOrderAttemptsList;
    }

    public OrderImage[] getOrderImages() {
        return mOrderImages;
    }

    public void setOrderImages(final OrderImage[] orderImages) {
        mOrderImages = orderImages;
    }

    public boolean isUpdateNeeded() {
        return isUpdateNeeded;
    }

    public String getFactoryReceivedDate() {
        return factoryReceivedDate;
    }

    public void setFactoryReceivedDate(String factoryReceivedDate) {
        this.factoryReceivedDate = factoryReceivedDate;
    }

    public String getFactoryCompleteDate() {
        return factoryCompleteDate;
    }

    public void setFactoryCompleteDate(String factoryCompleteDate) {
        this.factoryCompleteDate = factoryCompleteDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    public String getDropOffWorker() {
        return dropOffWorker;
    }

    public void setDropOffWorker(final String dropOffWorker) {
        this.dropOffWorker = dropOffWorker;
    }

    public String getPickUpWorker() {
        return pickUpWorker;
    }

    public void setPickUpWorker(final String pickUpWorker) {
        this.pickUpWorker = pickUpWorker;
    }

    public boolean isPickUp() {
        return isPickUp;
    }

    public void setIsPickUp(final boolean isPickUp) {
        this.isPickUp = isPickUp;
    }

    public double getVoucher() {
        return voucher;
    }

    public void setVoucher(final double voucher) {
        this.voucher = voucher;
    }

    public String getUrlQRCode() {
        return urlQRCode;
    }

    public void setUrlQRCode(final String urlQRCode) {
        this.urlQRCode = urlQRCode;
    }

    public boolean isLazyOrder() {
        return lazyOrder;
    }

    public void setLazyOrder(boolean lazyOrder) {
        this.lazyOrder = lazyOrder;
    }

    public boolean isPayLater() {
        return payLater;
    }

    public boolean isFree() {
        return free;
    }

    public OrderStatus getOrderStatusObject() {
        return orderStatusObject;
    }

    public void setOrderStatusObject(OrderStatus orderStatusObject) {
        this.orderStatusObject = orderStatusObject;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderDetails[] getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(final OrderDetails[] orderDetails) {
        this.orderDetails = orderDetails;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public boolean isStatus(final int... statuses) {

        for (final int status : statuses) {
            if (status == orderStatusId) return true;
        }

        return false;

    }

    public int getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(int orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPickUpWorkerId() {
        return pickUpWorkerId;
    }

    public void setPickUpWorkerId(int pickUpWorkerId) {
        this.pickUpWorkerId = pickUpWorkerId;
    }

    public int getDropOffWorkerId() {
        return dropOffWorkerId;
    }

    public void setDropOffWorkerId(int dropOffWorkerId) {
        this.dropOffWorkerId = dropOffWorkerId;
    }

    public int getDropOffDistrictId() {
        return dropOffDistrictId;
    }

    public void setDropOffDistrictId(int dropOffDistrictId) {
        this.dropOffDistrictId = dropOffDistrictId;
    }

    public int getPickUpDistrictId() {
        return pickUpDistrictId;
    }

    public void setPickUpDistrictId(int pickUpDistrictId) {
        this.pickUpDistrictId = pickUpDistrictId;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getPickUpPostalCode() {
        return pickUpPostalCode;
    }

    public void setPickUpPostalCode(String pickUpPostalCode) {
        this.pickUpPostalCode = pickUpPostalCode;
    }

    public String getPickUpApartmentType() {
        return pickUpApartmentType;
    }

    public void setPickUpApartmentType(String pickUpApartmentType) {
        this.pickUpApartmentType = pickUpApartmentType;
    }

    public String getDropOffAddress() {
        return dropOffAddress;
    }

    public void setDropOffAddress(String dropOffAddress) {
        this.dropOffAddress = dropOffAddress;
    }

    public String getDropOffPostalCode() {
        return dropOffPostalCode;
    }

    public void setDropOffPostalCode(String dropOffPostalCode) {
        this.dropOffPostalCode = dropOffPostalCode;
    }

    public String getDropOffApartmentType() {
        return dropOffApartmentType;
    }

    public void setDropOffApartmentType(String dropOffApartmentType) {
        this.dropOffApartmentType = dropOffApartmentType;
    }

    public int getSpeedRating() {
        return speedRating;
    }

    public void setSpeedRating(int speedRating) {
        this.speedRating = speedRating;
    }

    public int getAtitudeRating() {
        return atitudeRating;
    }

    public void setAtitudeRating(int atitudeRating) {
        this.atitudeRating = atitudeRating;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getPickUpDate() {
        return pickUpDate;
    }

    public void setPickUpDate(String pickUpDate) {
        this.pickUpDate = pickUpDate;
    }

    public String getPickUpTime() {
        return pickUpTime;
    }

    public void setPickUpTime(String pickUpTime) {
        this.pickUpTime = pickUpTime;
    }

    public String getDropOffDate() {
        return dropOffDate;
    }

    public void setDropOffDate(String dropOffDate) {
        this.dropOffDate = dropOffDate;
    }

    public String getDropOffTime() {
        return dropOffTime;
    }

    public void setDropOffTime(String dropOffTime) {
        this.dropOffTime = dropOffTime;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getPayPalRefNo() {
        return payPalRefNo;
    }

    public void setPayPalRefNo(String payPalRefNo) {
        this.payPalRefNo = payPalRefNo;
    }

    public boolean getPayLater() {
        return payLater;
    }

    public void setPayLater(boolean payLater) {
        this.payLater = payLater;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public double getToPayPrice() {
        return toPayPrice;
    }

    public void setToPayPrice(double toPayPrice) {
        this.toPayPrice = toPayPrice;
    }

    public boolean getFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public boolean isLastOrder() {
        return lazyOrder;
    }

    public void setLastOrder(boolean lastOrder) {
        this.lazyOrder = lastOrder;
    }

    public boolean isExpressOrder() {
        return expressOrder;
    }

    public void setExpressOrder(boolean expressOrder) {
        this.expressOrder = expressOrder;
    }

    public boolean isPickUpChanged() {
        return pickUpChanged;
    }

    public void setPickUpChanged(boolean pickUpChanged) {
        this.pickUpChanged = pickUpChanged;
    }

    public boolean isDeliverChanged() {
        return deliverChanged;
    }

    public void setDeliverChanged(boolean deliverChanged) {
        this.deliverChanged = deliverChanged;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public boolean isWorkerChecked() {
        return workerChecked;
    }

    public void setWorkerChecked(boolean workerChecked) {
        this.workerChecked = workerChecked;
    }

    public boolean isUserChecked() {
        return userChecked;
    }

    public void setUserChecked(boolean userChecked) {
        this.userChecked = userChecked;
    }

    //    @Override
    //    public int describeContents() {
    //        // TODO Auto-generated method stub
    //        return 0;
    //    }
    //
    //    @Override
    //    public void writeToParcel(Parcel dest, int flags) {
    ////        dest.writeInt(this.jobId);
    ////        dest.writeInt(this.applicationId);
    ////        dest.writeInt(this.jobStateId);
    ////        dest.writeString(this.description);
    ////        dest.writeString(this.startLocation);
    ////        dest.writeString(this.endLocation);
    ////        dest.writeString(this.jobScheduledTime);
    ////
    ////        dest.writeDouble(this.startLat);
    ////        dest.writeDouble(this.startLon);
    ////        dest.writeDouble(this.endLat);
    ////        dest.writeDouble(this.endLon);
    ////        dest.writeDouble(this.distance);
    ////        dest.writeInt(this.workerId);
    ////        dest.writeString(this.workerName);
    ////        dest.writeString(this.jobDate);
    //
    //    }
    //
    //
    //    public static final Creator<Class.Order> CREATOR = new Creator<Class.Order>() {
    //
    //        @Override
    //        public Class.Order createFromParcel(Parcel source) {
    //            return new Class.Order(source); // RECREATE VENUE GIVEN SOURCE
    //        }
    //
    //        @Override
    //        public Class.Order[] newArray(int size) {
    //            return new Class.Order[size]; // CREATING AN ARRAY OF VENUES
    //        }
    //
    //    };
    //
    //    public Order(Parcel source) {
    //        // TODO IMPLEMENT
    //        this.jobId = source.readInt();
    //        this.applicationId = source.readInt();
    //        this.jobStateId = source.readInt();
    //        this.description = source.readString();
    //        this.startLocation = source.readString();
    //        this.endLocation = source.readString();
    //        this.jobScheduledTime = source.readString();
    //        this.startLat = source.readDouble();
    //        this.startLon = source.readDouble();
    //        this.endLat = source.readDouble();
    //        this.endLon = source.readDouble();
    //        this.distance = source.readDouble();
    //        this.workerId = source.readInt();
    //        this.workerName = source.readString();
    //        this.jobDate = source.readString();
    //
    //    }
    //


    public List<OrderDetails> getOrderDetailsArrayList() {
        return orderDetailsArrayList;
    }

    public void setOrderDetailsArrayList(List<OrderDetails> orderDetailsArrayList) {
        this.orderDetailsArrayList = orderDetailsArrayList;
    }

    public CustomerDetails getCustomerObject() {
        return customerObject;
    }

    public void setCustomerObject(CustomerDetails customerObject) {
        this.customerObject = customerObject;
    }

    public int getWmsOrderId() {
        return wmsOrderId;
    }

    public void setWmsOrderId(int wmsOrderId) {
        this.wmsOrderId = wmsOrderId;
    }

    public String getDropOffPic() {
        return dropOffPic;
    }

    public void setDropOffPic(String dropOffPic) {
        this.dropOffPic = dropOffPic;
    }

    public String getPickupPic() {
        return pickupPic;
    }

    public void setPickupPic(String pickupPic) {
        this.pickupPic = pickupPic;
    }

    public String getDropOffTo() {
        return dropOffTo;
    }

    public void setDropOffTo(String dropOffTo) {
        this.dropOffTo = dropOffTo;
    }

    public Integer getTotal_package() {
        return total_package;
    }

    public void setTotal_package(Integer total_package) {
        this.total_package = total_package;
    }

    public double getTotal_kg() {
        return total_kg;
    }

    public void setTotal_kg(double total_kg) {
        this.total_kg = total_kg;
    }


    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }


    public List<Overtime> getmOvertimeList() {
        return mOvertimeList;
    }

    public void setmOvertimeList(List<Overtime> mOvertimeList) {
        this.mOvertimeList = mOvertimeList;
    }
}

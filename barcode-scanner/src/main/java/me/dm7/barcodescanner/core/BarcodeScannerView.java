package me.dm7.barcodescanner.core;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;

public abstract class BarcodeScannerView extends FrameLayout implements Camera.PreviewCallback {

	protected Camera mCamera;
	private CameraPreview mPreview;
	private ImageView mImageView;
	// private ViewFinderView mViewFinderView;
	private Rect mFramingRectInPreview;
	private boolean isCameraStarted = false;

	private boolean isFrontCamera;

	public BarcodeScannerView ( Context context ) {
		super( context );
		setupLayout();
	}

	public BarcodeScannerView ( Context context, AttributeSet attributeSet ) {
		super( context, attributeSet );
		setupLayout();
	}

	public void setupLayout () {
		mPreview = new CameraPreview( getContext() );
		mImageView = new ImageView( getContext() );
		// mViewFinderView = new ViewFinderView(getContext());
		addView( mPreview );
		addView( mImageView );
		mImageView.setBackgroundColor( Color.WHITE );
		mImageView.setVisibility( View.GONE );
		// addView(mViewFinderView);
	}

	public void startCamera () {
		startCamera( false );
	}

	public boolean isFrontCamera () {
		return isFrontCamera;
	}

	public void switchCamera () {
		stopCamera();
		startCamera( !isFrontCamera() );
	}

	public void startCamera ( final boolean isFrontCamera ) {

		this.isFrontCamera = isFrontCamera;

		mCamera = CameraUtils.getCameraInstance( isFrontCamera );

		if ( mCamera != null ) {
			// mViewFinderView.setupViewFinder();
			mPreview.setCamera( mCamera, this );
			mPreview.initCameraPreview();
			isCameraStarted = true;
		}

	}

	public void stopCamera () {
		if ( mCamera != null ) {
			mPreview.stopCameraPreview();
			mPreview.setCamera( null, null );
			mCamera.release();
			mCamera = null;
			isCameraStarted = false;
		}
	}

	public boolean isCameraStarted () {
		return isCameraStarted;
	}

	public boolean getFlash () {
		if ( mCamera != null && CameraUtils.isFlashSupported( mCamera ) ) {
			Camera.Parameters parameters = mCamera.getParameters();
			if ( parameters.getFlashMode().equals( Camera.Parameters.FLASH_MODE_TORCH ) ) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	// public synchronized Rect getFramingRectInPreview(int width, int height) {
	// if (mFramingRectInPreview == null) {
	// Rect framingRect = mViewFinderView.getFramingRect();
	// if (framingRect == null) { return null; }
	// Rect rect = new Rect(framingRect);
	// Point screenResolution = DisplayUtils.getScreenResolution(getContext());
	// Point cameraResolution = new Point(width, height);
	//
	// if (cameraResolution == null || screenResolution == null) {
	// // Called early, before init even finished
	// return null;
	// }
	//
	// rect.left = rect.left * cameraResolution.x / screenResolution.x;
	// rect.right = rect.right * cameraResolution.x / screenResolution.x;
	// rect.top = rect.top * cameraResolution.y / screenResolution.y;
	// rect.bottom = rect.bottom * cameraResolution.y / screenResolution.y;
	//
	// mFramingRectInPreview = rect;
	// }
	// return mFramingRectInPreview;
	// }

	public void setFlash ( boolean flag ) {
		if ( mCamera != null && CameraUtils.isFlashSupported( mCamera ) ) {
			Camera.Parameters parameters = mCamera.getParameters();
			if ( flag ) {
				if ( parameters.getFlashMode().equals( Camera.Parameters.FLASH_MODE_TORCH ) ) { return; }
				parameters.setFlashMode( Camera.Parameters.FLASH_MODE_TORCH );
			} else {
				if ( parameters.getFlashMode().equals( Camera.Parameters.FLASH_MODE_OFF ) ) { return; }
				parameters.setFlashMode( Camera.Parameters.FLASH_MODE_OFF );
			}
			mCamera.setParameters( parameters );
		}
	}

	public void shutterFlash () {
		flash( mImageView );
	}

	public void toggleFlash () {
		if ( mCamera != null && CameraUtils.isFlashSupported( mCamera ) ) {
			Camera.Parameters parameters = mCamera.getParameters();
			if ( parameters.getFlashMode().equals( Camera.Parameters.FLASH_MODE_TORCH ) ) {
				parameters.setFlashMode( Camera.Parameters.FLASH_MODE_OFF );
			} else {
				parameters.setFlashMode( Camera.Parameters.FLASH_MODE_TORCH );
			}
			mCamera.setParameters( parameters );
		}
	}

	public void setAutoFocus ( boolean state ) {
		if ( mPreview != null ) {
			mPreview.setAutoFocus( state );
		}
	}

	void fade ( final View view, final float opacityStart, final float opacityEnd, int time, final boolean toggleVisibility ) {

		view.setAlpha( opacityStart );

		if ( toggleVisibility ) {
			if ( view.getVisibility() == View.GONE ) view.setVisibility( View.VISIBLE );
			else view.setVisibility( View.GONE );
		}

		final Animation animation = new Animation() {

			@Override
			public boolean willChangeBounds () {
				return true;
			}

			@Override
			protected void applyTransformation ( float interpolatedTime, Transformation t ) {

				if ( interpolatedTime == 1 ) {

					view.setAlpha( opacityEnd );

					if ( toggleVisibility ) {
						if ( view.getVisibility() == View.GONE ) view.setVisibility( View.VISIBLE );
						else view.setVisibility( View.GONE );
					}

				} else {

					final float opacity = ( float ) ( opacityStart + ( interpolatedTime * ( opacityEnd - opacityStart ) ) );
					view.setAlpha( opacity );
					view.requestLayout();

				}
			}
		};

		animation.setDuration( time );
		view.startAnimation( animation );

	}

	void flash ( final ImageView imageView ) {

		final int flashTime = 10;
		final int disappearingTime = 2000;
		imageView.setVisibility( View.VISIBLE );
		fade( imageView, 0, 1, flashTime, false );

		imageView.postDelayed( new Runnable() {

			@Override
			public void run () {
				fade( imageView, 1, 0, disappearingTime, true );
			}

		}, flashTime + 1 );

	}

}

package me.dm7.barcodescanner.core;

import android.hardware.Camera;

import java.util.List;

public class CameraUtils {

	/**
	 * A safe way to get an instance of the Camera object.
	 */
	public static Camera getCameraInstance () {
		return getCameraInstance( false );
	}
	/**
	 * A safe way to get an instance of the Camera object.
	 */
	public static Camera getCameraInstance (final boolean isFrontCamera) {

		Camera c = null;

		try {

			if ( isFrontCamera ) {
				final Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
				final int count = Camera.getNumberOfCameras();

				for ( int i = 0 ; i < count ; i++ ) {

					Camera.getCameraInfo( i, cameraInfo );

					if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT ) {
						c = Camera.open( i );
						break;
					}

				}

				if ( c == null ) c = Camera.open();

			} else {

				c = Camera.open();

			}

		} catch ( Exception e ) {
			// Camera is not available (in use or does not exist)
		}

		return c; // returns null if camera is unavailable

	}



	public static boolean isFlashSupported ( Camera camera ) {
	    /* Credits: Top answer at http://stackoverflow.com/a/19599365/868173 */
		if ( camera != null ) {
			Camera.Parameters parameters = camera.getParameters();

			if ( parameters.getFlashMode() == null ) {
				return false;
			}

			List< String > supportedFlashModes = parameters.getSupportedFlashModes();
			if ( supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get( 0 )
			                                                                                                                           .equals(
					                                                                                                                           Camera
							                                                                                                                           .Parameters.FLASH_MODE_OFF ) ) {
				return false;
			}
		} else {
			return false;
		}

		return true;
	}
}

package app.juaagugui.httpService.model;

import android.util.Pair;

import java.util.List;

import app.juaagugui.httpService.listeners.OnRESTResultCallback;

public interface IHttpConnection {

    String getUrl();

    void setUrl(String url);

    int getHttpVerb();

    void setHttpVerb(int httpVerb);

    String getBodyData();

    void setBodyData(String bodyData);

    Boolean getLoginRequired();

    void setLoginRequired(Boolean loginRequired);

    List<Pair<String, String>> getHeaders();

    void setHeaders(List<Pair<String, String>> headers);

    /**
     * Some servers require several params attached to the URL, even if the verb
     * of the httpRequest is not GET
     *
     * @return Default params to be attached to the URL following GET way
     */
    List<Pair<String, String>> getDefaultUriQueryParams();

    /**
     * Some servers require several params attached to the URL, even if the verb
     * of the httpRequest is not GET
     *
     * @param defaultUriQueryParams
     */
    void setDefaultUriQueryParams(List<Pair<String, String>> defaultUriQueryParams);

    /**
     * Gets the listener which will process the result of the httpRequest
     *
     * @return callback
     */

    OnRESTResultCallback getCallback();

    /**
     * Sets the listener which will process the result of the httpRequest
     *
     * @param callback
     */
    void setCallback(OnRESTResultCallback callback);

    String getFilePath();

}
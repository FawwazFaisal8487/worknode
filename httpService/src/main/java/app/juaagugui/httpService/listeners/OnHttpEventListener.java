package app.juaagugui.httpService.listeners;

public interface OnHttpEventListener {

    void onRequestInit();

    void onRequestFinish();
}

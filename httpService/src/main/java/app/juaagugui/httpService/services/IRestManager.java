package app.juaagugui.httpService.services;

import android.content.Context;
import android.os.ResultReceiver;

import app.juaagugui.httpService.exceptions.NoInternetConnectionException;
import app.juaagugui.httpService.model.IHttpConnection;

/**
 * @author Juan Aguilar Guisado
 * @since 1.0
 */
public interface IRestManager {

    /**
     * @return Handler which will process the result and call the object waiting
     * for it.
     */
    ResultReceiver getResultReceiver();

    /**
     * Make an HttpRequest
     *
     * @param connection Wrapper with request settings
     * @throws NoInternetConnectionException Exception thrown in case network is not available
     */
    void sendRequest(IHttpConnection connection) throws NoInternetConnectionException;

    /**
     * Make an HttpRequest with a return code
     *
     * @param returnCode Code to be returned with the callback
     * @param connection Wrapper with request settings
     * @throws NoInternetConnectionException Exception thrown in case network is not available
     * @since 1.0.1
     */
    void sendRequestWithReturn(int returnCode, IHttpConnection connection) throws NoInternetConnectionException;

    /**
     * @return if the user is currently logged in server side
     */
    boolean isLogged();

    /**
     * Execute the logout action of client side.
     *
     * @return the success of the logout action
     */
    boolean logout();


    /**
     * Check network connection for this device
     *
     * @param context
     * @return
     */
    Boolean hasDeviceConnectionToInternet(Context context);

}

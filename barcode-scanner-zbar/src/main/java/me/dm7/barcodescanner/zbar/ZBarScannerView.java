package me.dm7.barcodescanner.zbar;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.util.Collection;
import java.util.List;

import me.dm7.barcodescanner.core.BarcodeScannerView;
import me.dm7.barcodescanner.core.DisplayUtils;

/**
 * Step to use this class:
 * <p/>
 * Manifest file:
 * <br/>
 * <p/>
 * &lt;uses-permission android:name = "android.permission.CAMERA" &#47;>
 * <p/>
 * &lt;uses-feature
 * <br/>
 * android:name = "android.hardware.camera"
 * <br/>
 * android:required = "false" &#47;>
 * <p/>
 * &lt;uses-feature
 * <br/>
 * android:name = "android.hardware.camera.autofocus"
 * <br/>
 * android:required = "false" &#47;>
 * <p/>
 * layout file:
 * <p/>
 * &lt;me.dm7.barcodescanner.zbar.ZBarScannerView
 * <br/>
 * android:id = "@+id/mZBarScannerView"
 * <br/>
 * android:layout_width = "match_parent"
 * <br/>
 * android:layout_height = "match_parent" &#47;>
 * <p/>
 * uses the following method to make it work:
 * <br/>
 * {@link #setResultHandler(ResultHandler)}
 * <br/>
 * {@link #startCamera(boolean)}
 * <br/>
 * {@link #stopCamera()}
 * <br/>
 * {@link #setIsScanning(boolean)}
 */
public class ZBarScannerView extends BarcodeScannerView {

	private static final int TIME_SLEEP_BEFORE_START_CAMERA_AGAIN = 2222;
	private ImageScanner mScanner;
	private List< BarcodeFormat > mFormats;
	private ResultHandler mResultHandler;
	private long mTimeLastShot = -1;
	private boolean mIsScanning = true;

	public interface ResultHandler {

		public void handleResult ( Result rawResult );

	}

	public ZBarScannerView ( Context context ) {
		super( context );
		setupScanner();
	}

	public ZBarScannerView ( Context context, AttributeSet attributeSet ) {
		super( context, attributeSet );
		setupScanner();
	}

	public void setIsScanning ( final boolean isScanning ) {
		this.mIsScanning = isScanning;
		if ( isScanning && mCamera != null ) mCamera.setOneShotPreviewCallback( this );
	}

	public void setResultHandler ( ResultHandler resultHandler ) {
		mResultHandler = resultHandler;
	}

	public Collection< BarcodeFormat > getFormats () {
		if ( mFormats == null ) { return BarcodeFormat.ALL_FORMATS; }
		return mFormats;
	}

	public void setFormats ( List< BarcodeFormat > formats ) {
		mFormats = formats;
		setupScanner();
	}

	public void setupScanner () {
		mScanner = new ImageScanner();
		mScanner.setConfig( 0, Config.X_DENSITY, 3 );
		mScanner.setConfig( 0, Config.Y_DENSITY, 3 );

		mScanner.setConfig( Symbol.NONE, Config.ENABLE, 0 );
		for ( BarcodeFormat format : getFormats() ) {
			mScanner.setConfig( format.getId(), Config.ENABLE, 1 );
		}
	}

	@Override
	public void onPreviewFrame ( byte[] data, Camera camera ) {

		long timeCur = System.currentTimeMillis();

		if ( mTimeLastShot != -1 && timeCur - mTimeLastShot < TIME_SLEEP_BEFORE_START_CAMERA_AGAIN ) {
			camera.setOneShotPreviewCallback( this );
			return;
		}

		if ( !mIsScanning ) return;
		Camera.Parameters parameters = camera.getParameters();
		Camera.Size size = parameters.getPreviewSize();
		int width = size.width;
		int height = size.height;

		if ( DisplayUtils.getScreenOrientation( getContext() ) == Configuration.ORIENTATION_PORTRAIT ) {

			byte[] rotatedData;

			try {
				rotatedData = new byte[ data.length ];
			} catch ( final Throwable e ) {
				Log.e( "AndroidRuntime", e.getMessage() != null ? e.getMessage() : e.getClass().getSimpleName() );
				return;
			}

			for ( int y = 0 ; y < height ; y++ ) {
				for ( int x = 0 ; x < width ; x++ )
					rotatedData[ x * height + height - y - 1 ] = data[ x + y * width ];
			}

			int tmp = width;
			width = height;
			height = tmp;
			data = rotatedData;

		}

		Image barcode = new Image( width, height, "Y800" );
		barcode.setData( data );

		int result = mScanner.scanImage( barcode );

		if ( result != 0 ) {

			mTimeLastShot = timeCur;

			if ( mResultHandler != null ) {

				SymbolSet syms = mScanner.getResults();
				Result rawResult = new Result();

				for ( Symbol sym : syms ) {

					String symData = sym.getData();

					if ( !TextUtils.isEmpty( symData ) ) {
						rawResult.setContents( symData );
						rawResult.setBarcodeFormat( BarcodeFormat.getFormatById( sym.getType() ) );
						break;
					}

				}

				shutterFlash();
				playSound();
				mResultHandler.handleResult( rawResult );

			}
		}

		camera.setOneShotPreviewCallback( this );

	}

	void playSound () {
		final MediaPlayer mediaPlayer = MediaPlayer.create( getContext(), R.raw.beep );
		mediaPlayer.start();
	}

	static {
		System.loadLibrary( "iconv" );
	}

}


package com.hollowsoft.library.fontdroid;

/**
 * @author Igor Morais
 * @author mor41s.1gor@gmail.com
 */
public final class StringUtility {

    /**
     *
     */
    public static final String EMPTY = "";

    /**
     * Default private constructor.
     */
    private StringUtility() {

    }

    /**
     *
     * @param stringValue
     * @return
     */
    public static boolean isNullOrEmpty(final String stringValue) {
        return stringValue == null || stringValue.isEmpty();
    }
}

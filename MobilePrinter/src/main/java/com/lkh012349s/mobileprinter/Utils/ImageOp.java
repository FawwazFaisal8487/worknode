package com.lkh012349s.mobileprinter.Utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Various Image Operation. Mostly Format conversion.
 */
public class ImageOp {

	//	public static final int LIMIT_IMAGE_SIZE = 22 * 1024;
	public static final int DIMEN_MAX_BITMAP = 300;

	public interface OnImageLoadedListener {

		void onImageLoaded();

	}

	/**
	 * @return True if the save is successful.
	 */
	public static boolean saveBitmapToFile(final Bitmap bitmap, final String fileName) {

		FileOutputStream out = null;

		try {
			out = new FileOutputStream(fileName);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
			return true;
			// PNG is a lossless format, the compression factor (100) is ignored
		} catch (Exception e) {
			LogCustom.e(e);
		} finally {

			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
			}

		}

		return false;

	}

	public static int[] getImageSize(final Context context, final Uri uri) {

		if (JavaOp.ifNullThenLog(uri, context)) return null;

		try {

			InputStream is = context.getContentResolver().openInputStream(uri);
			BitmapFactory.Options dbo = new BitmapFactory.Options();
			dbo.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, dbo);
			is.close();
			final int orientation = getOrientation(context, uri);
			int rotatedWidth, rotatedHeight;

			if (orientation == 90 || orientation == 270) {
				rotatedWidth = dbo.outHeight;
				rotatedHeight = dbo.outWidth;
			} else {
				rotatedWidth = dbo.outWidth;
				rotatedHeight = dbo.outHeight;
			}

			return new int[]{rotatedWidth, rotatedHeight};

		} catch (final Throwable e) {
			LogCustom.e(e);
			return null;
		}

	}

	public static String convert(final Uri uri, final Context context) {
		return convert(uri, DIMEN_MAX_BITMAP, context);
	}

	public static String convert(final Uri uri, final int maxDimen, final Context context) {
		final Bitmap bitmap = convert(context, uri, maxDimen);
		return convert(bitmap);
	}

	/**
	 * Convert an image uri to a Bitmap object with standard size.
	 */
	public static Bitmap convertStandard(final Context context, final Uri uri) {
		return convert(context, uri, DIMEN_MAX_BITMAP);
	}

	public static Bitmap convert(final Context context, final Uri uri, final int maxDimen) {

		if (JavaOp.ifNullThenLog(uri, context)) return null;

		try {

			InputStream is = context.getContentResolver().openInputStream(uri);
			BitmapFactory.Options dbo = new BitmapFactory.Options();
			dbo.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, dbo);
			is.close();
			final int orientation = getOrientation(context, uri);
			int rotatedWidth, rotatedHeight;

			if (orientation == 90 || orientation == 270) {
				rotatedWidth = dbo.outHeight;
				rotatedHeight = dbo.outWidth;
			} else {
				rotatedWidth = dbo.outWidth;
				rotatedHeight = dbo.outHeight;
			}

			Bitmap bitmap;
			is = context.getContentResolver().openInputStream(uri);

			if (maxDimen > 0 && (rotatedWidth > maxDimen || rotatedHeight > maxDimen)) {

				float widthRatio = ((float) rotatedWidth) / ((float) maxDimen);
				float heightRatio = ((float) rotatedHeight) / ((float) maxDimen);
				float maxRatio = Math.max(widthRatio, heightRatio);

				// Create the bitmap from file
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = (int) maxRatio;
				bitmap = BitmapFactory.decodeStream(is, null, options);

			} else {

				bitmap = BitmapFactory.decodeStream(is);

			}

			is.close();

			// if the orientation is not 0 (or -1, which means we don't know), we have to do a rotation.
			if (orientation > 0) {
				Matrix matrix = new Matrix();
				matrix.postRotate(orientation);
				bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
			}

			return bitmap;

		} catch (final Throwable e) {
			LogCustom.e(e);
			return null;
		}

	}

	/**
	 * Convert a view to a Bitmap object.
	 */
	public static Bitmap convert(final View view) {
		if (JavaOp.ifNullThenLog(view)) return null;
		return convert(view, view.getWidth(), view.getHeight());
	}

	/**
	 * Convert a view to a Bitmap object.
	 */
	public static Bitmap convert(final View view, final int width, final int height) {
		LogCustom.i(width, "width");
		LogCustom.i(height, "height");
		if (JavaOp.ifNullThenLog(view) || JavaOp.ifTrueThenLog(width <= 0 || height <= 0, "width <= 0 || height <= 0"))
			return null;
		final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		final Canvas canvas = new Canvas(bitmap);
		final Drawable drawable = view.getBackground();
		if (drawable != null) drawable.draw(canvas);
		else canvas.drawColor(Color.WHITE);
		view.draw(canvas);
		return bitmap;
	}

	/**
	 * This method should not be run in Main UI Thread.
	 */
	public static String retrieveBase64String(final ImageView imageView, final int maxDimen) {
		if (JavaOp.ifNullThenLog(imageView)) return null;
		Bitmap bitmap = getBitmap(imageView);
		if (bitmap == null) return null;
		return convert(bitmap, maxDimen);
	}

	/**
	 * This method should not be run in Main UI Thread.
	 */
	public static String retrieveBase64String(final ImageView imageView) {
		if (JavaOp.ifNullThenLog(imageView)) return null;
		Bitmap bitmap = getBitmap(imageView);
		if (bitmap == null) return null;
		return convert(bitmap);
	}

	public static Bitmap getBitmap(final ImageView imageView) {
		if (JavaOp.ifNullThenLog(imageView)) return null;
		final BitmapDrawable bitmapDrawable = (BitmapDrawable) imageView.getDrawable();
		if (bitmapDrawable == null) return null;
		return bitmapDrawable.getBitmap();
	}

	/**
	 * Load Bitmap into an ImageView. The Bitmap is resized to standard size before being loaded into ImageView.
	 */
	public static void loadStandard(final ImageView imageView, final OnImageLoadedListener listener, final Bitmap bitmap) {

		if (JavaOp.ifNullThenLog(imageView, bitmap)) return;

		final Runnable runnable = new Runnable() {

			@Override
			public void run() {
				final Bitmap bitmapResized = resizeStandard(bitmap);
				if (bitmapResized != null) setBitmapOnMainThread(imageView, listener, bitmap);
			}

		};

		AsyncTask.execute(runnable);

	}

	/**
	 * Convert byte array to Bitmap.
	 */
	public static Bitmap convert(final byte[] arr) {
		if (JavaOp.ifArrayNullOrEmptyThenLog(arr)) return null;
		return BitmapFactory.decodeByteArray( arr, 0, arr.length );
	}

	/**
	 * Convert byte array to Bitmap with specified width and height.
	 */
	//	public static Bitmap convert ( final byte[] arr, final int widthRequired, final int heightRequired ) {
	//		if ( JavaOp.ifArrayNullOrEmptyThenLog( arr ) ) return null;
	//		final BitmapFactory.Options options = new BitmapFactory.Options();
	//		options.inJustDecodeBounds = true;
	//		BitmapFactory.decodeByteArray( arr, 0, arr.length, options );
	//		options.inSampleSize = calculateInSampleSize( options, widthRequired, heightRequired );
	//		options.inJustDecodeBounds = false;
	//		return BitmapFactory.decodeByteArray( arr, 0, arr.length, options );
	//	}
	public static Bitmap copy(final Bitmap bitmap) {

		if (JavaOp.ifNullThenLog(bitmap)) return null;

		if (bitmap.isRecycled()) {
			LogCustom.e("bitmap is already recycled.");
			return null;
		}

		return bitmap.copy(bitmap.getConfig(), true);

	}

	public static Bitmap convert(final String base64String) {
		if (JavaOp.isNullOrEmpty(base64String)) return null;
		final byte[] bytes = Base64.decode(base64String, Base64.DEFAULT);
		if (JavaOp.isArrayNullOrEmpty(bytes)) return null;
		return convert(bytes);
	}

	/**
	 * Convert a Bitmap object into the Base64 String.
	 */
	public static String convert(final Bitmap bitmap) {
		return convert(bitmap, Bitmap.CompressFormat.PNG, 100);
	}

	/**
	 * Convert a Bitmap object into the Base64 String. the Bitmap object resized before convertion.
	 *
	 * @param maxDimen Must be > 0.
	 */
	public static String convert(Bitmap bitmap, final int maxDimen) {
		if (JavaOp.ifNullThenLog(bitmap)) return null;
		if (maxDimen < 1) LogCustom.e("maxDimen < 1 ");
		bitmap = resize(bitmap, maxDimen);
		if (bitmap == null) return null;
		return convert(bitmap);
	}

	/**
	 * Convert a Bitmap object into the Base64 String. the Bitmap object resized to standard size before convertion.
	 */
	public static String convertStandard(final Bitmap bitmap) {
		return convert(bitmap, DIMEN_MAX_BITMAP);
	}

	//	public static Bitmap convert ( final String base64String, final int widthRequired, final int heightRequired ) {
	//		if ( JavaOp.isNullOrEmpty( base64String ) ) return null;
	//		final byte[] bytes = Base64.decode( base64String, Base64.DEFAULT );
	//		if ( JavaOp.isArrayNullOrEmpty( bytes ) ) return null;
	//		return convert( bytes, widthRequired, heightRequired );
	//	}

	public static Bitmap resize(final Bitmap bitmap, final int width, final int height) {

		try {
			return Bitmap.createScaledBitmap(bitmap, width, height, true);
		} catch (Throwable e) {
			LogCustom.e(e);
			return null;
		}

	}

	/**
	 * Return Bitmap of standard size.
	 */
	public static Bitmap resizeStandard( final Bitmap bitmap ) {
		return resize( bitmap, DIMEN_MAX_BITMAP );
	}

	//	public static Bitmap convert ( final Context context, final Uri uri, final int maxDimen ) throws IOException {
	//
	//		if ( JavaOp.ifNullThenLog( context, uri ) ) throw new NullPointerException( "context or uri is null" );
	//		final InputStream is = new BufferedInputStream( context.getContentResolver().openInputStream( uri ) );
	//
	//		try {
	//
	//			is.mark( is.available() );
	//			final BitmapFactory.Options dbo = new BitmapFactory.Options();
	//			dbo.inJustDecodeBounds = true;
	//			BitmapFactory.decodeStream( is, null, dbo );
	//			is.reset();
	//			int orientation = getOrientation( context, uri );
	//			int rotatedWidth, rotatedHeight;
	//
	//			if ( orientation == 90 || orientation == 270 ) {
	//				rotatedWidth = dbo.outHeight;
	//				rotatedHeight = dbo.outWidth;
	//			} else {
	//				rotatedWidth = dbo.outWidth;
	//				rotatedHeight = dbo.outHeight;
	//			}
	//
	//			Bitmap bitmap;
	//			//		final InputStream is = context.getContentResolver().openInputStream( uri );
	//
	//			if ( maxDimen > 0 && ( rotatedWidth > maxDimen || rotatedHeight > maxDimen ) ) {
	//
	//				final int scale = ( int ) Math.pow( 2,
	//						( int ) Math.ceil( Math.log( maxDimen / ( double ) Math.max( dbo.outHeight, dbo.outWidth ) ) / Math.log( 0.5 ) ) );
	//
	//				// Create the bitmap from file
	//				dbo.inJustDecodeBounds = false;
	//				dbo.inSampleSize = scale;
	//				bitmap = BitmapFactory.decodeStream( is, null, dbo );
	//
	//			} else {
	//
	//				bitmap = BitmapFactory.decodeStream( is );
	//
	//			}
	//
	//			// if the orientation is not 0 (or -1, which means we don't know), we have to do a rotation.
	//			if ( orientation > 0 ) {
	//				Matrix matrix = new Matrix();
	//				matrix.postRotate( orientation );
	//				bitmap = Bitmap.createBitmap( bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true );
	//			}
	//
	//			return bitmap;
	//
	//		} catch ( final Throwable e ) {
	//			LogCustom.e( e );
	//		} finally {
	//
	//			try {
	//				is.close();
	//			} finally {}
	//
	//		}
	//
	//		return null;
	//
	//	}

	//	public static String convert ( final Context context, final Uri uri ) {
	//
	//		if ( JavaOp.ifNullThenLog( context, uri ) ) throw new NullPointerException( "context or uri is null" );
	//		final InputStream inputStream;
	//
	//		try {
	//			inputStream = context.getContentResolver().openInputStream( uri );
	//		} catch ( Throwable e ) {
	//			LogCustom.e( e );
	//			return null;
	//		}
	//
	//		final byte[] buffer = new byte[ 8192 ];
	//		int bytesRead;
	//		final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	//
	//		try {
	//			while ( ( bytesRead = inputStream.read( buffer ) ) != -1 ) outputStream.write( buffer, 0, bytesRead );
	//		} catch ( IOException e ) {
	//			LogCustom.e( e );
	//			return null;
	//		}
	//
	//		final byte[] bytes = outputStream.toByteArray();
	//		return Base64.encodeToString( bytes, Base64.DEFAULT );
	//
	//	}

	/**
	 * Resize the Bitmap and load it into the ImageView.
	 * <p/>
	 * Note: this method can be run on Main Thread.
	 *
	 * @param maxDimen If this value is <= 0, resizing will not be performed.
	 */
	public static void load(final ImageView imageView, final OnImageLoadedListener listener, final Bitmap bitmap, final int maxDimen) {

		if (JavaOp.ifNullThenLog(imageView, bitmap)) return;

		final Runnable runnable = new Runnable() {

			@Override
			public void run() {
				final Bitmap bitmapResized = resize(bitmap, maxDimen);
				if (bitmapResized != null) setBitmapOnMainThread(imageView, listener, bitmap);
			}

		};

		AsyncTask.execute(runnable);

	}

	/**
	 * Note: this method can be run on Main Thread.
	 */
	public static void load(final ImageView imageView, final OnImageLoadedListener listener, final String base64String) {
		load(imageView, listener, base64String, -1);
	}

	/**
	 * Convert base64 string into a Bitmap of standard size, then load the Bitmap into the ImageView.
	 * <p/>
	 * Note: this method can be run on Main Thread.
	 */
	public static void loadStardard(final ImageView imageView, final OnImageLoadedListener listener, final String base64String) {
		load(imageView, listener, base64String, DIMEN_MAX_BITMAP);
	}

	/**
	 * Convert base64 string into a Bitmap of specific size, then load the Bitmap into the ImageView.
	 * <p/>
	 * Note: this method can be run on Main Thread.
	 *
	 * @param maxDimen If this value is <= 0, resizing will not be performed.
	 */
	public static void load(final ImageView imageView, final OnImageLoadedListener listener, final String base64String, final int maxDimen) {

		if (JavaOp.ifNullOrEmptyThenLog(imageView, base64String)) return;

		final Runnable runnable = new Runnable() {

			@Override
			public void run() {

				LogCustom.i(base64String, "base64String");
				Bitmap bitmap = convert(base64String);
				LogCustom.i(bitmap == null, "bitmap is null");
				if (bitmap == null) return;

				if (maxDimen > 0 && (bitmap.getWidth() > maxDimen || bitmap.getHeight() > maxDimen)) {
					bitmap = resize(bitmap, maxDimen);
				}

				LogCustom.i();
				setBitmapOnMainThread(imageView, listener, bitmap);

			}

		};

		AndroidOp.runInBackground(imageView.getContext(), runnable);

	}

	public static Bitmap resize(final Bitmap bitmap, final int maxDimen) {

		if (JavaOp.ifNullThenLog(bitmap)) return null;

		if (maxDimen < 1) {
			LogCustom.e("maxDimen < 1");
			return bitmap;
		}

		final float ratio = (float) bitmap.getHeight() / bitmap.getWidth();
		if (ratio == 1) return bitmap;

		int height;
		int width;

		if (ratio > 1) {
			if (bitmap.getHeight() == maxDimen) return bitmap;
			height = maxDimen;
			width = (int) (maxDimen / ratio);
		} else {
			if (bitmap.getWidth() == maxDimen) return bitmap;
			width = maxDimen;
			height = (int) (maxDimen * ratio);
		}

		return resize(bitmap, width, height);

	}

	private static int getOrientation(final Context context, final Uri uri) {

		if (uri.toString().contains("content:")) {

			Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

			if (cursor == null) {
				LogCustom.e("cursor is null.");
				return 0;
			}

			if (cursor.getCount() != 1) return -1;
			cursor.moveToFirst();
			return cursor.getInt(0);

		} else if (uri.toString().contains("file:")) {

			ExifInterface exif = null;

			try {
				exif = new ExifInterface(uri.getPath());
			} catch (IOException e) {
				LogCustom.e(e);
				return 0;
			}

			int value = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);

			switch (value) {
				case 1:
					return 0;
				case 6:
					return 90;
				case 8:
					return 270;
				default:
					return 0;
			}

		}

		LogCustom.e("Error uri");
		return -1;

	}

	static void setBitmapOnMainThread(final ImageView imageView, final OnImageLoadedListener listener, final Bitmap bitmap) {

		LogCustom.i();
		if (JavaOp.ifNullThenLog(imageView, bitmap)) return;

		final Runnable runnableMain = new Runnable() {

			@Override
			public void run() {
				LogCustom.i();
				imageView.setImageBitmap(bitmap);
				if (listener != null) listener.onImageLoaded();
			}

		};

		AndroidOp.runOnMainThread(imageView.getContext(), runnableMain);

	}

	/**
	 * Convert Bitmap to Base64 String.
	 */
	public static String convert(final Bitmap bitmap, final Bitmap.CompressFormat compressFormat, final int quality) {
		if (JavaOp.isNull(bitmap)) return null;
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(compressFormat, quality, byteArrayOutputStream);
		final byte[] bytes = byteArrayOutputStream.toByteArray();

		Log.i("image byte size", "byte size:" + bytes.length); // check size first, quality is imageSizeCompress

		if (JavaOp.isNull(bytes)) return null;
		return Base64.encodeToString(bytes, Base64.NO_WRAP);
	}

	public static String convertDefault(final Bitmap bitmap, final Bitmap.CompressFormat compressFormat, final int quality) {
		if (JavaOp.isNull(bitmap)) return null;
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(compressFormat, quality, byteArrayOutputStream);
		final byte[] bytes = byteArrayOutputStream.toByteArray();
		if (JavaOp.isNull(bytes)) return null;
		return Base64.encodeToString(bytes, Base64.DEFAULT);
	}

	//	private static int getOrientation ( final Context context, final Uri uri ) {
	//
	//		if ( uri.toString().contains( "content:" ) ) {
	//
	//			Cursor cursor = context.getContentResolver().query( uri, new String[] { MediaStore.Images.ImageColumns.ORIENTATION }, null, null, null );
	//
	//			if ( cursor == null ) {
	//				LogCustom.e( "cursor is null." );
	//				return 0;
	//			}
	//
	//			if ( cursor.getCount() != 1 ) return -1;
	//			cursor.moveToFirst();
	//			return cursor.getInt( 0 );
	//
	//		} else if ( uri.toString().contains( "file:" ) ) {
	//
	//			ExifInterface exif = null;
	//
	//			try {
	//				exif = new ExifInterface( uri.getPath() );
	//			} catch ( IOException e ) {
	//				LogCustom.e( e );
	//				return 0;
	//			}
	//
	//			int value = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, 0 );
	//
	//			switch (value) {
	//				case 1:
	//					return 0;
	//				case 6:
	//					return 90;
	//				case 8:
	//					return 270;
	//				default:
	//					return 0;
	//			}
	//
	//		}
	//
	//		LogCustom.e( "Error uri" );
	//		return -1;
	//
	//	}

	//	private static int calculateInSampleSize ( final BitmapFactory.Options options, final int reqWidth, final int reqHeight ) {
	//
	//		// Raw height and width of image
	//		final int height = options.outHeight;
	//		final int width = options.outWidth;
	//		int inSampleSize = 1;
	//
	//		if ( height > reqHeight || width > reqWidth ) {
	//
	//			final int halfHeight = height / 2;
	//			final int halfWidth = width / 2;
	//
	//			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
	//			// height and width larger than the requested height and width.
	//			while ( ( halfHeight / inSampleSize ) > reqHeight && ( halfWidth / inSampleSize ) > reqWidth ) {
	//				inSampleSize *= 2;
	//			}
	//
	//		}
	//
	//		return inSampleSize;
	//
	//	}


	public static Bitmap toBitmapStandard(final Context context, final Uri uri) {
		return toBitmap(context, uri, DIMEN_MAX_BITMAP);
	}

	/**
	 * Create Bitmap with standard size.
	 */
	public static Bitmap toBitmapStandard(final Context context, final String url) {
		if (JavaOp.ifNullOrEmptyThenLog(url)) return null;
		return toBitmapStandard(context, Uri.parse(url));
	}


	/**
	 * Create Bitmap with specific size.
	 *
	 * @param size If this value is <= 0, resizing will not be performed.
	 */
	public static Bitmap toBitmap(final Context context, final Uri uri, int size) {

		if (JavaOp.ifNullThenLog(uri, context)) return null;
		size = size > 0 ? size : Target.SIZE_ORIGINAL;

		try {
			return Glide.with(context).load(uri).asBitmap().into(size, size).get();
		} catch (final Throwable e) {
			//AdvancedLog.e( e );
		}

		return null;

	}

	/**
	 * Create Bitmap with specific size.
	 *
	 * @param dimen If this value is <= 0, resizing will not be performed.
	 */
	public static Bitmap toBitmap(final Context context, final String url, int size) {

		if (JavaOp.ifNullOrEmptyThenLog(url, context)) return null;
		size = size > 0 ? size : Target.SIZE_ORIGINAL;

		try {
			return Glide.with(context).load(Uri.parse(url)).asBitmap().into(size, size).get();
		} catch (Throwable e) {
			//AdvancedLog.e( e );
		}

		return null;

	}


	public static String toBase64StringStandard(final Context context, final String url) {
		if (JavaOp.ifNullOrEmptyThenLog(url)) return null;
		return toBase64String(context, Uri.parse(url), DIMEN_MAX_BITMAP);
	}

	public static String toBase64StringStandard(final Context context, final Uri uri) {
		return toBase64String(context, uri, DIMEN_MAX_BITMAP);
	}

	public static String toBase64String(final Context context, final String url, final int maxDimen) {
		if (JavaOp.ifNullOrEmptyThenLog(url)) return null;
		final Bitmap bitmap = toBitmap(context, Uri.parse(url), maxDimen);
		return toBase64String(bitmap);
	}

	public static String toBase64String(final Context context, final Uri uri, final int maxDimen) {
		final Bitmap bitmap = toBitmap(context, uri, maxDimen);
		return toBase64String(bitmap);
	}

	public static String toBase64String(final Bitmap bitmap) {
		return toBase64String(bitmap, Bitmap.CompressFormat.PNG, 100);
	}

	/**
	 * Convert Bitmap to Base64 String.
	 */
	static String toBase64String(final Bitmap bitmap, final Bitmap.CompressFormat compressFormat, final int quality) {
		if (JavaOp.ifNullThenLog(bitmap)) return null;
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(compressFormat, quality, byteArrayOutputStream);
		final byte[] bytes = byteArrayOutputStream.toByteArray();
		if ( JavaOp.isArrayNullOrEmpty( bytes ) ) return null;
		return Base64.encodeToString( bytes, Base64.DEFAULT );
	}




}

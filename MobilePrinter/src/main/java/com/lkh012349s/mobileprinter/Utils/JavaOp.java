package com.lkh012349s.mobileprinter.Utils;

import android.text.Editable;

import java.math.BigDecimal;
import java.util.List;

public class JavaOp {

	/**
	 * Log (ERROR) if the expression is True.
	 *
	 * @param isTrue     The expression.
	 * @param expression The message to be logged.
	 * @return isTrue.
	 */
	public static boolean ifTrueThenLog(final boolean isTrue, final String expression) {
		if (isTrue) LogCustom.e(expression);
		return isTrue;
	}

	public static <T> T getOr(final T value, final T valueDefault) {

		if (value == null) {
			JavaOp.ifNullThenLog(valueDefault);
			return valueDefault;
		}

		return value;

	}

	public static boolean isFlagAdded(final int flag, final int flags) {
		return (flags & flag) != 0;
	}

	public static int addFlag(final int flag, final int flags) {
		return flag | flags;
	}

	public static int removeFlag(final int flag, final int flags) {
		return flags & ~flag;
	}

	/**
	 * @return float from Float object. Return 0 if null.
	 */
	public static float get(final Float f) {
		return f == null ? 0 : f;
	}

	/**
	 * @return int from Integer object. Return 0 if null.
	 */
	public static int get(final Integer i) {
		return i == null ? 0 : i;
	}

	/**
	 * @return Get boolean from {@link Boolean} object. Return false if null.
	 */
	public static boolean getBooleanOrFalse(final Boolean b) {
		return b != null && b;
	}

	/**
	 * @return Get boolean from {@link Boolean} object. Return true if null.
	 */
	public static boolean getBooleanOrTrue(final Boolean b) {
		return b == null || b;
	}

	/**
	 * @return Get double from Double object. Return 0 if null.
	 */
	public static double get(final Double d) {
		return d == null ? 0 : d;
	}

	public static float getFloatWithGivenDecimalPlace(final float f, final int decimalPlace) {
		BigDecimal bigDecimal = new BigDecimal(Float.toString(f));
		bigDecimal = bigDecimal.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bigDecimal.floatValue();
	}

	/**
	 * @return true if one of the objects is equal to the object.
	 */
	public static boolean contain(final Object object, final Object... objects) {

		if (object == null) {
			LogCustom.e("object is null.");
			return false;
		}

		for (final Object o : objects) {
			if (JavaOp.ifNullThenLog(o)) continue;
			if (o != null && o.equals(object)) return true;
		}

		return false;

	}

	/**
	 * Check if a number is between a range.
	 *
	 * @return if the value is between min and max.
	 */
	public static boolean between(final double value, final double min, final double max, final boolean isMinMaxInclusive) {
		return isMinMaxInclusive ? value >= min && value <= max : value > min && value < max;
	}

	/**
	 * Check if a number is between a range.
	 *
	 * @return if the value is between min and max.
	 */
	public static boolean between(final float value, final float min, final float max, final boolean isMinMaxInclusive) {
		return isMinMaxInclusive ? value >= min && value <= max : value > min && value < max;
	}

	/**
	 * Check if a number is between a range.
	 *
	 * @return if the value is between min and max.
	 */
	public static boolean between(final int value, final int min, final int max, final boolean isMinMaxInclusive) {
		return isMinMaxInclusive ? value >= min && value <= max : value > min && value < max;
	}

	/**
	 * @return empty string if the provided string is null.
	 */
	public static String getEmptyStringIfNull(final String str) {
		return str == null ? "" : str;
	}

	/**
	 * Check if two number have very closed value
	 *
	 * @param v0
	 * @param v1
	 * @param tol A tolerance value.
	 * @return False if their value are very closed to each other.
	 */
	public static boolean isDiffLarge(final double v0, final double v1, final double tol) {
		return Math.abs(v0 - v1) > tol;
	}

	/**
	 * Check if two number have very closed value
	 *
	 * @param v0
	 * @param v1
	 * @param tol A tolerance value.
	 * @return False if their value are very closed to each other.
	 */
	public static boolean isDiffLarge(final long v0, final long v1, final long tol) {
		return Math.abs(v0 - v1) > tol;
	}

	/**
	 * @return true if one of the objects is null or of zero length / size ( if it has length / size attribute ).
	 */
	public static boolean isNullOrEmpty(final Object... objects) {
		return ifNull(false, true, objects);
	}

	/**
	 * @return true if one of the objects is null.
	 */
	public static boolean isNull(final Object... objects) {
		return ifNull(false, false, objects);
	}

	/**
	 * @return value or 0 if value < 0.
	 */
	public static int equalsAtLeastZero(int value) {
		if (value < 0) return 0;
		return value;
	}

	public static boolean isNumOdd(final int num) {
		return num % 2 != 0;
	}

	/**
	 * Sleep for at least specified time.
	 *
	 * @param timeMin   Minimum time to sleep.
	 * @param timeStart The starting time.
	 */
	public static void sleepAtLeast(final long timeMin, final long timeStart) {
		sleep(timeMin - (System.currentTimeMillis() - timeStart));
	}

	/**
	 * Sleep for a specified time.
	 *
	 * @param milllisSeconds If this value is <= 0, nothing will happen.
	 */
	public static void sleep(final long milllisSeconds) {

		if (milllisSeconds <= 0) return;

		try {
			Thread.sleep(milllisSeconds);
		} catch (InterruptedException e) {
		}

	}

	/**
	 * Division that always return rounded-up value.
	 */
	public static int divideRoundUp(final float num, final int divisor) {
		return (int) ((num + divisor) / num);
	}

	/**
	 * Log if any object is null.
	 *
	 * @return true if any object is null.
	 */
	public static boolean ifNullThenLog(final Object... objects) {
		return ifNull(true, false, objects);
	}

	/**
	 * Log if any object is null or of zero length / size ( if it has length / size attribute ).
	 *
	 * @return true if any object is null or of zero length / size ( if it has length / size attribute ).
	 */
	public static boolean ifNullOrEmptyThenLog(final Object... objects) {
		return ifNull(true, true, objects);
	}

	/**
	 * @return true if the array is null or of zero length / size ( if it has length / size attribute ).
	 */
	public static boolean isArrayNullOrEmpty(final Object array) {
		return ifArrayNull(false, true, array);
	}

	/**
	 * Log if the array is null.
	 *
	 * @return true if the array is null.
	 */
	public static boolean ifArrayNullThenLog(final Object array) {
		return ifArrayNull(true, false, array);
	}

	/**
	 * Log if the array is null or of zero length / size ( if it has length / size attribute ).
	 *
	 * @return true if the array is null or of zero length / size ( if it has length / size attribute ).
	 */
	public static boolean ifArrayNullOrEmptyThenLog(final Object array) {
		return ifArrayNull(true, true, array);
	}

	/**
	 * @return false if all objects are not null and the length / size of each object ( if it has length / size attribute ) is not zero.
	 */
	private static boolean ifNull(final boolean isLoggingNeeded, final boolean isCheckingEmptyNeeded, final Object... objects) {

		if (objects == null) {
			if (isLoggingNeeded) LogCustom.e("objects is null");
			return true;
		}

		for (int i = 0; i < objects.length; i++) {

			final Object object = objects[i];

			if (object == null) {
				if (isLoggingNeeded) LogCustom.e(String.format("object %d is null", i));
				return true;
			}

			if (!isCheckingEmptyNeeded) continue;

			if (object instanceof String) {

				final String s = (String) object;

				if (s.length() == 0) {
					if (isLoggingNeeded) LogCustom.e("object " + i + " is zero-length.");
					return true;
				}

			} else if (object instanceof Editable) {

				final Editable editable = (Editable) object;

				if (editable.length() == 0) {
					if (isLoggingNeeded) LogCustom.e("object " + i + " is zero-length.");
					return true;
				}

			} else if (object instanceof CharSequence) {

				final CharSequence charSequence = (CharSequence) object;

				if (charSequence.length() == 0) {
					if (isLoggingNeeded) LogCustom.e("object " + i + " is zero-length.");
					return true;
				}

			} else if (object.getClass().isArray()) {

				return ifArrayNull(isLoggingNeeded, isCheckingEmptyNeeded, object);

			} else if (object instanceof List) {

				final List list = (List) object;

				if (list.size() == 0) {
					if (isLoggingNeeded) LogCustom.e("Object " + i + " is zero-length.");
					return true;
				}

			}
		}

		return false;

	}

	/**
	 * @return true if the array is null or of zero length / size ( if it has length / size attribute ).
	 */
	private static boolean ifArrayNull(final boolean isLoggingNeeded, final boolean isCheckingEmptyNeeded, final Object array) {

		if (array == null) {
			if (isLoggingNeeded) LogCustom.e("array is null.");
			return true;
		}

		if (array.getClass().isArray()) {

			if (array instanceof int[]) {

				final int[] ints = (int[]) array;

				if (ints.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof float[]) {

				final float[] floats = (float[]) array;

				if (floats.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof double[]) {

				final double[] doubles = (double[]) array;

				if (doubles.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof byte[]) {

				final byte[] bytes = (byte[]) array;

				if (bytes.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof short[]) {

				final short[] shorts = (short[]) array;

				if (shorts.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof long[]) {

				final long[] longs = (long[]) array;

				if (longs.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof boolean[]) {

				final boolean[] booleans = (boolean[]) array;

				if (booleans.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof char[]) {

				final char[] chars = (char[]) array;

				if (chars.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else if (array instanceof Object[]) {

				final Object[] objectss = (Object[]) array;

				if (objectss.length == 0) {
					if (isLoggingNeeded) LogCustom.e("array is of zero-length.");
					return true;
				}

				return false;

			} else {

				if (isLoggingNeeded) LogCustom.e("Invalid array type.");
				return true;

			}

		}

		LogCustom.e("The array is not of array type.");
		return true;

	}

}

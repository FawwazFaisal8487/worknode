package com.lkh012349s.mobileprinter.Utils;

import android.content.Context;

import androidx.annotation.Nullable;

import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringOp {

    /**
     * Capitalize the first letter of the input string.
     */
    public static String capFirstLetter(final String input) {
        if (JavaOp.ifNullOrEmptyThenLog(input)) return null;
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public static boolean containsDigits(final String str) {
        if (JavaOp.ifNullOrEmptyThenLog(str)) return false;
        return str.matches(".*\\d+.*");
    }

    /**
     * @return The string of given value in money format.
     */
    public static String formatCurrency(final float value) {
        return new DecimalFormat("$0.00").format(value);
    }

    /**
     * @return The string of given value in money format.
     */
    public static String formatCurrency(final double value) {
        return new DecimalFormat("$0.00").format(value);
    }

    public static boolean isAlphaNumeric(final String s) {
        if (JavaOp.ifNullOrEmptyThenLog(s)) return false;
        String pattern = "^[+/=a-zA-Z0-9]*$";
        return s.matches(pattern);
    }

    /**
     * Extract string with pattern.
     *
     * @param regex The regular expression of the pattern.
     * @param group Explicit capturing groups in the pattern are numbered left to right in order of their opening parenthesis, starting at 1. The special
     *              group 0 represents the entire match (as if the entire pattern is surrounded by an implicit capturing group). For example, "a((b)c)
     *              " matching "abc" would give the following groups:<p/>
     *              0 "abc"<br/>
     *              1 "bc"<br/>
     *              2 "b"<p/>
     *              An optional capturing group that failed to match as part of an overall successful match (for example, "a(b)?c" matching "ac") returns
     *              null. A capturing group that matched the empty string (for example, "a(b?)c" matching "ac") returns the empty string.
     * @return The extracted string.
     */
    public static String extractString(final String input, final String regex, final int group) {

        if (JavaOp.isNullOrEmpty(input, regex)) return null;
        final Pattern pattern;

        try {
            pattern = Pattern.compile(regex);
        } catch (final Throwable e) {
            LogCustom.e(e);
            LogCustom.i(input, "input");
            LogCustom.i(regex, "regex");
            return null;
        }

        Matcher matcher = pattern.matcher(input);
        if (matcher.find()) return matcher.group(group);
        LogCustom.e(String.format("There is no string with the pattern %s in the string \"%s\"", regex, input));
        return null;

    }

    /**
     * Extract string with pattern.
     *
     * @param regex The regular expression of the pattern.
     * @return The extracted string.
     */
    public static String extractString(final String input, final String regex) {
        return extractString(input, regex, 0);
    }

    /**
     * Same as {@link String#format(String, Object...)}, but if any object is null, it will be converted to empty string.
     *
     * @param objects Only String object is allowed.
     */
    public static String formatGiveEmptyStringIfNull(final String format, @Nullable final Object... objects) {

        if (JavaOp.ifNullOrEmptyThenLog(format, objects)) return null;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] == null) objects[i] = "";
        }

        return String.format(format, objects);

    }

    /**
     * Same as {@link String#format(String, Object...)}, but if any object is null, it will be converted to empty string.
     *
     * @param objects Only String object is allowed.
     */
    public static String formatGiveEmptyStringIfNull(final Context context, final int resId, final Object... objects) {

        if (JavaOp.ifNullThenLog(context)) return null;
        final String format = context.getString(resId);

        if (JavaOp.ifNullOrEmptyThenLog(format, objects)) return null;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] == null) objects[i] = "";
        }

        return String.format(format, objects);

    }

}

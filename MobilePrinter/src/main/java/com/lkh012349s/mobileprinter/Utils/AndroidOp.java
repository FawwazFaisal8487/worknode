package com.lkh012349s.mobileprinter.Utils;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Locale;

public class AndroidOp {

	public final static int REQUEST_CODE_CAMERA = 3113;
	public final static int REQUEST_CODE_GALLERY = 7112;
	private static final double MIN_LOCATIONS_DISTANCE = 0.001;
	private static final int PADDING_ZOOMING_BOUND = 199;

	private static final String NAME_PACKAGE_PLAY_SERVICES = "com.google.android.gms";

	public static String getFileNameWithoutExtension(final String fileName) {
		final File file = new File(fileName);
		return file.exists() ? file.getName().replaceFirst("[.][^.]+$", "") : null;
	}

	/**
	 * Must be run on Main UI thread.
	 */
	public static String getPathFromUri(final Context context, final Uri uri) {
		if (JavaOp.ifNullThenLog(context, uri)) return null;
		final String[] proj = {MediaStore.Images.Media.DATA};
		final CursorLoader loader = new CursorLoader(context, uri, proj, null, null, null);
		final Cursor cursor = loader.loadInBackground();
		if (cursor == null) return uri.getPath();
		final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		final String result = cursor.getString(column_index);
		cursor.close();
		return result;
	}

	public static void makeTextViewsUnderlined(TextView... textViews) {

		if (JavaOp.ifArrayNullOrEmptyThenLog(textViews)) return;

		for (final TextView textView : textViews) {
			if (JavaOp.ifNullThenLog(textView)) continue;
			textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		}

	}

	public static void setTextIfValueNotNull(final TextView textView, final Object value, final String format) {

		if (JavaOp.ifNullThenLog(textView)) return;

		if (value == null) {
			textView.setText(null);
		} else {
			if (JavaOp.ifNullOrEmptyThenLog(format)) return;
			final String s = String.format(format, value);
			textView.setText(s);
		}

	}

	/**
	 * Get the size to make the layout square.
	 * <br/>
	 * Steps:
	 * <br/>
	 * 1) Override {@link View#onMeasure(int, int)} and call this method to get the size. Pass the parameters in {@link View#onMeasure(int, int)} to
	 * this method.
	 * <br/>
	 * 2) call {@link View#setMeasuredDimension(int, int)} and put the size as both parameters.
	 */
	public static int getViewSquareDimension(final int widthMeasureSpec, final int heightMeasureSpec) {
		int width = View.MeasureSpec.getSize(widthMeasureSpec);
		int height = View.MeasureSpec.getSize(heightMeasureSpec);
		return width > height && height > 0 ? height : width;
	}

	public static void runOnMainThread(final Context context, final Runnable runnable) {

		if (JavaOp.ifNullThenLog(context, runnable)) return;

		if (Looper.myLooper() == Looper.getMainLooper()) {
			runnable.run();
		} else {
			final Handler handler = new Handler(context.getMainLooper());
			handler.post(runnable);
		}

	}

	public static void runInBackground(final Context context, final Runnable runnable) {
		if (JavaOp.ifNullThenLog(context, runnable)) return;
		if (Looper.myLooper() == Looper.getMainLooper()) AsyncTask.execute(runnable);
		else runnable.run();
	}

	public static Object getActivityMetaData(final Activity activity, final Object keyOrItsResId) {

		if (JavaOp.ifNullThenLog(activity, keyOrItsResId)) return null;

		try {
			final ActivityInfo activityInfo = activity.getPackageManager()
					.getActivityInfo(activity.getComponentName(),
							PackageManager.GET_ACTIVITIES | PackageManager.GET_META_DATA);
			final Bundle metaData = activityInfo.metaData;
			final String key = getString(activity, keyOrItsResId);
			return metaData.get(key);
		} catch (final Throwable e) {
			LogCustom.e(e);
			return null;
		}

	}

	/**
	 * @return Solid StateListDrawable that will change color (darker or lighter) on pressed or on disabled.
	 */
	public static StateListDrawable getSolidBackground(final int color) {
		final int colorDim = getSelectionColor(color);
		final Drawable drawableNormal = new ColorDrawable(color);
		final Drawable drawableDim = new ColorDrawable(colorDim);
		final StateListDrawable drawable = new StateListDrawable();
		drawable.addState(new int[]{android.R.attr.state_pressed}, drawableDim);
		drawable.addState(new int[]{-android.R.attr.state_enabled}, drawableDim);
		drawable.addState(new int[]{}, drawableNormal);
		return drawable;
	}

	public static int getSelectionColor(final int color) {

		if (color == 0) {

			return Color.GRAY;

		} else {

			final float[] hsv = new float[3];
			final int alpha = Color.alpha(color);

			if (alpha == 255) {
				Color.colorToHSV(color, hsv);
				final float value = hsv[2];
				hsv[2] = value * 0.8f;
				if (value < 0.5) hsv[2] = value + 0.3f;
				else hsv[2] = value - 0.3f;
				return Color.HSVToColor(hsv);
			} else {
				final int alphaNew = (int) (alpha * 0.4f);
				final int red = Color.red(color);
				final int green = Color.green(color);
				final int blue = Color.blue(color);
				return Color.argb(alphaNew, red, green, blue );
			}

		}

	}

	//	/**
	//	 * Add a marker to a Google Map
	//	 *
	//	 * @param googleMap Google Map V2.
	//	 * @param idResIcon Resouce id of the marker icon.
	//	 * @param uAnchor   Anchor of the marker.
	//	 * @param vAnchor   Anchor of the marker.
	//	 * @param title     Title of the marker. If null, it will not be set.
	//	 * @param snippet   Snippet of the marker. If null, it will not be set.
	//	 * @return Marker added to the Google Map.
	//	 */
	//	public static Marker addMarkerToGoogleMap ( final GoogleMap googleMap, final double lat, final double lng, final int idResIcon, final float
	// uAnchor,
	//			final float vAnchor, final String title, final String snippet ) {
	//		return addMarkerToGoogleMap( googleMap, new LatLng( lat, lng ), idResIcon, uAnchor, vAnchor, title, snippet );
	//	}
	//
	//	/**
	//	 * Add a marker to a Google Map
	//	 *
	//	 * @param googleMap Google Map V2.
	//	 * @param idResIcon Resouce id of the marker icon.
	//	 * @param uAnchor   Anchor of the marker.
	//	 * @param vAnchor   Anchor of the marker.
	//	 * @param title     Title of the marker. If null, it will not be set.
	//	 * @param snippet   Snippet of the marker. If null, it will not be set.
	//	 * @return Marker added to the Google Map.
	//	 */
	//	public static Marker addMarkerToGoogleMap ( final GoogleMap googleMap, final LatLng latLng, final int idResIcon, final float uAnchor,
	//			final float vAnchor, final String title, final String snippet ) {
	//
	//		if ( JavaOp.ifNullThenLog( googleMap, latLng ) ) return null;
	//		final BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromResource( idResIcon );
	//		final MarkerOptions markerOptions = new MarkerOptions().position( latLng ).icon( bitmapDescriptor ).anchor( uAnchor, vAnchor );
	//		if ( title != null ) markerOptions.title( title );
	//		if ( snippet != null ) markerOptions.snippet( snippet );
	//		return googleMap.addMarker( markerOptions );
	//
	//	}

	/**
	 * Convert an object to {@link String}. If the object is already {@link String}, the object will be simply returned; If the
	 * object is a String Resource Id, the corresponding String will be returned. Else, null will be returned.
	 */
	public static String getString(final Context context, final Object textOrItsResId) {

		if (JavaOp.ifNullThenLog(context)) return null;
		if (textOrItsResId == null) return null;

		if (textOrItsResId instanceof String) {
			return (String) textOrItsResId;
		} else if (textOrItsResId instanceof Integer) {
			final int idRes = (int) textOrItsResId;
			return context.getString(idRes);
		}

		LogCustom.e("The text variable must be either int or String !");
		return null;

	}

	/**
	 * Make a TextView visible and set text.
	 */
	public static void makeVisibleAndSetText(final TextView textView, final String text) {
		if (JavaOp.ifNullThenLog(textView)) return;
		textView.setVisibility(View.VISIBLE);
		textView.setText(text);
	}

	public static int generateRequestCode(final int prefix, final long code) {

		final String prefixStr = String.valueOf(prefix);
		int multiplier = 1;

		for (int i = 0; i < prefixStr.length(); i++) {
			multiplier *= 10;
		}

		int requestCode = 0;

		try {
			requestCode = (int) (code % (Integer.MAX_VALUE / multiplier));
			final String requestCodeStr = prefixStr + requestCode;
			requestCode = Integer.parseInt(requestCodeStr);
		} catch (NumberFormatException e) {
			LogCustom.e(e);
		}

		return requestCode;

	}

	public static int getActionBarHeight(final Activity activity) {
		if (JavaOp.ifNullThenLog(activity)) return 0;
		final TypedValue tv = new TypedValue();
		final boolean isActionBarExisted = activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true);
		return isActionBarExisted ? TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics()) : -1;
	}

	/**
	 * Retrieve address from lat lng.
	 *
	 * @return Null on error.
	 */
	public static String getAddressFromLatLng(final Context context, final double lat, final double lng) {

		if (JavaOp.ifNullThenLog(context)) return null;
		final Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		List<Address> addresses = null;

		try {
			addresses = geocoder.getFromLocation(lat, lng, 1);
		} catch (IOException e) {
			LogCustom.e(e);
			return null;
		}

		if (JavaOp.ifNullOrEmptyThenLog(addresses)) return null;

		try {
			return addresses.get(0).getAddressLine(0);
		} catch (Throwable ex) {
			LogCustom.e(ex);
			return null;
		}

	}

	public static String getAppVersionName(final Activity activity) {

		if (JavaOp.ifNullThenLog(activity)) return null;

		try {
			return activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).versionName;
		} catch (PackageManager.NameNotFoundException e) {
			LogCustom.e(e);
			return "";
		}

	}

	public static String getCountryISO(final Context context) {
		if (JavaOp.ifNullThenLog(context)) return null;
		final TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return manager.getSimCountryIso();
	}

	public static void showSoftInput(final Context context) {
		final InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
	}

	public static String getDeviceId(final Context context) {

		if (JavaOp.ifNullThenLog(context)) return null;
		String deviceId = null;

		try {
			final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			deviceId = telephonyManager.getDeviceId();
		} catch (Throwable e) {
			LogCustom.e(e);
		}

		if (deviceId == null) {

			try {
				deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
			} catch (Throwable e) {
				LogCustom.e(e);
			}

		}

		return deviceId;

	}

	/**
	 * Flash an ImageView.
	 */
	public static void flash(final ImageView imageView) {

		final int flashTime = 10;
		final int disappearingTime = 2000;
		imageView.setVisibility(View.VISIBLE);
		fade(imageView, 0, 1, flashTime, false);

		imageView.postDelayed(new Runnable() {

			@Override
			public void run() {
				fade(imageView, 1, 0, disappearingTime, true);
			}

		}, flashTime + 1);

	}

	public static Intent getIntentSetLocationServiceEnabled() {
		return new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	}

	/**
	 * @param name The name of the folder.
	 * @return The path of the folder in external storage.
	 */
	public static String getPathExternalStorage(final String name) {
		if (JavaOp.ifNullThenLog(name)) return null;
		final String state = Environment.getExternalStorageState();
		if (!Environment.MEDIA_MOUNTED.equals(state) && !Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
			LogCustom.e("Error from getting external storage path: no external storage ?");
		return Environment.getExternalStorageDirectory().toString() + "/" + name;
	}

	/**
	 * Convert dp to px.
	 */
	public static float getPx(final Resources mResources, final int dp) {
		if (JavaOp.ifNullThenLog(mResources)) return 0f;
		return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, mResources.getDisplayMetrics());
	}

	/**
	 * Call this method before adding content.
	 */
	public static void hideActionBar(final Activity mActivity) {
		if (JavaOp.ifNullThenLog(mActivity)) return;
		mActivity.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		final ActionBar actionBar = mActivity.getActionBar();
		if (!JavaOp.ifNullThenLog(actionBar)) actionBar.hide();
	}

	/**
	 * Check if GPS or location service with network is enabled.
	 */
	public static boolean isAnyLocationServiceProviderEnabled(final Context context) {
		if (JavaOp.ifNullThenLog(context)) return false;
		final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

	public static boolean isAppForeground(final Context context) {
		if (JavaOp.ifNullThenLog(context)) return false;
		final ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		if (JavaOp.ifNullThenLog(manager)) return false;
		final List<ActivityManager.RunningTaskInfo> services = manager.getRunningTasks(Integer.MAX_VALUE);
		if (JavaOp.ifNullThenLog(services, services.get(0), services.get(0).topActivity, services.get(0).topActivity.getPackageName()))
			return false;
		return services.get(0).topActivity.getPackageName().equalsIgnoreCase(context.getPackageName());
	}

	public static boolean isInternetConnected(final Context context) {

		final Runtime runtime = Runtime.getRuntime();

		try {
			final Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
			return (ipProcess.waitFor() == 0);
		} catch (final Throwable e) {
			LogCustom.e(e);
		}

		return false;

	}

	public static boolean launchMarket(final Context context, final String namePackage) {

		final Uri uri = Uri.parse("market://details?id=" + namePackage);
		final Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);

		try {
			context.startActivity(myAppLinkToMarket);
		} catch (ActivityNotFoundException e) {
			LogCustom.e(e);
			return false;
		}

		return true;

	}

	/**
	 * Add TextWatcher to all EditTexts so that the button will only be enabled when all editTexts contain value.
	 */
	public static void enableButtonOnlyWhenAllEditTextsAreFilled(final Button button, final EditText... editTexts) {

		if (JavaOp.ifNullOrEmptyThenLog(button, editTexts)) return;

		final TextWatcher textWatcher = new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

				boolean isEmpty = false;

				for (final EditText editText : editTexts) {
					if (JavaOp.ifNullThenLog(editText)) continue;
					if (JavaOp.isNullOrEmpty(editText.getText())) isEmpty = true;
				}

				if (isEmpty && button.isEnabled()) button.setEnabled(false);
				else if (!isEmpty && !button.isEnabled()) button.setEnabled(true);

			}

			@Override
			public void afterTextChanged(Editable s) {
			}

		};

		button.setEnabled(false);

		for (final EditText editText : editTexts) {
			editText.addTextChangedListener(textWatcher);
		}

	}

	public static void openAppInfo(final Context context, final String namePackage) {

		if (JavaOp.ifNullThenLog(context, namePackage)) return;
		final Intent intent = new Intent();

		if (Build.VERSION.SDK_INT >= 9) { //this supports all API levels
			intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
			intent.setData(Uri.parse("package:" + namePackage));
		} else {
			LogCustom.e("API level is smaller than 9.");
		}

		context.startActivity(intent);

	}

	public static boolean launchMarket(final Activity activity) {
		return launchMarket(activity, activity.getPackageName());
	}

	/**
	 * Check if the done button on the soft input is pressed.
	 */
	public static boolean isOkPressed(final int id, final KeyEvent keyEvent) {
		return (keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (id == EditorInfo.IME_ACTION_DONE);
	}

	/**
	 * Quit the application.
	 */
	public static void quit() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public static int getAppVersion(final Context context) {

		if (JavaOp.ifNullThenLog(context)) return 0;

		try {

			final PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;

		} catch (Throwable e) {
			LogCustom.e(e);
			return -1;
		}

	}

	public static void restartApplication(final Context context) {
		if (JavaOp.ifNullThenLog(context)) return;
		final Intent i = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(i);
		System.exit(0);
	}

	/**
	 * Set view(s) visible/gone.
	 */
	public static void setVisibleOrGone(final boolean isVisible, final View... views) {

		if (JavaOp.ifArrayNullOrEmptyThenLog(views)) return;

		for (final View view : views) {
			if (JavaOp.ifNullThenLog(view)) continue;
			view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
		}

	}

	/**
	 * Set view(s) visible/invisible.
	 */
	public static void setVisible(final boolean isVisible, final View... views) {

		if (JavaOp.ifArrayNullOrEmptyThenLog(views)) return;

		for (final View view : views) {
			if (JavaOp.ifNullThenLog(view)) continue;
			view.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
		}

	}

	/**
	 * Start camera acitivity for result.
	 *
	 * @param folder   If null, this method will return null as well.
	 * @param filename If null, timestamp will be used as filename.
	 */
	public static Uri startCameraForResult(final Activity activity, final String folder, final String filename) {

		File folderPhoto = null;

		if (!JavaOp.isNullOrEmpty(folder)) {
			folderPhoto = new File(folder);
			createFolderIfNotExist(folderPhoto);
		}

		return startCameraForResult(activity, folderPhoto, filename);

	}

	/**
	 * Start camera acitivity for result.
	 *
	 * @param folder   If null, this method will return null as well.
	 * @param filename If null, timestamp will be used as filename.
	 */
	public static Uri startCameraForResult(final androidx.fragment.app.Fragment fragment, final String folder, final String filename) {

		File folderPhoto = null;

		if (!JavaOp.isNullOrEmpty(folder)) {
			folderPhoto = new File( folder );
			createFolderIfNotExist( folderPhoto );
		}

		return startCameraForResult( fragment, folderPhoto, filename );

	}

	/**
	 * Start camera acitivity for result.
	 *
	 * @param folder   If null, this method will return null as well.
	 * @param filename If null, timestamp will be used as filename.
	 */
//	public static Uri startCameraForResult ( final androidx.fragment.app.Fragment  fragment, final String folder, final String filename ) {
//
//		File folderPhoto = null;
//
//		if ( !JavaOp.isNullOrEmpty( folder ) ) {
//			folderPhoto = new File( folder );
//			createFolderIfNotExist( folderPhoto );
//		}
//
//		return startCameraForResult( fragment, folderPhoto, filename );
//
//	}
	public static void createFolderIfNotExist(final File folder) {
		if (JavaOp.ifNullThenLog(folder)) return;
		if (!folder.exists()) folder.mkdirs();
	}

	/**
	 * Start camera acitivity for result.
	 *
	 * @param folder   If null, this method will return null as well.
	 * @param filename If null, timestamp will be used as filename.
	 */
	public static Uri startCameraForResult(final Activity activity, final File folder, String filename) {
		if (JavaOp.ifNullThenLog(activity)) return null;
		final Intent intent = startCameraForResult((Context) activity, folder, filename);
		if (intent == null) return null;
		activity.startActivityForResult(intent, REQUEST_CODE_CAMERA);
		return intent.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
	}

	/**
	 * Start camera acitivity for result.
	 *
	 * @param folder   If null, this method will return null as well.
	 * @param filename If null, timestamp will be used as filename.
	 */
	public static Uri startCameraForResult(final androidx.fragment.app.Fragment fragment, final File folder, String filename) {
		if (JavaOp.ifNullThenLog(fragment)) return null;
		final Intent intent = startCameraForResult((Context) fragment.getActivity(), folder, filename);
		if (intent == null) return null;
		fragment.startActivityForResult(intent, REQUEST_CODE_CAMERA);
		return intent.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
	}

	/**
	 * Start camera acitivity for result.
	 *
	 * @param folder   If null, this method will return null as well.
	 * @param filename If null, timestamp will be used as filename.
	 */
	public static Uri startCameraForResult(final Fragment fragment, final File folder, String filename) {
		if (JavaOp.ifNullThenLog(fragment)) return null;
		final Intent intent = startCameraForResult((Context) fragment.getActivity(), folder, filename);
		if (intent == null) return null;
		fragment.startActivityForResult(intent, REQUEST_CODE_CAMERA);
		return intent.getParcelableExtra(MediaStore.EXTRA_OUTPUT);
	}

	/**
	 * Expand the view with animation.
	 *
	 * @return the duration needed for the animation.
	 */
	public static int expandView(final View viewToExpand, final Animation.AnimationListener listener) {

		if (JavaOp.ifNullThenLog(viewToExpand)) return -1;
		viewToExpand.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		final int targetHeight = viewToExpand.getMeasuredHeight();
		viewToExpand.getLayoutParams().height = 0;
		viewToExpand.setVisibility(View.VISIBLE);

		final Animation animation = new Animation() {

			@Override
			public boolean willChangeBounds() {
				return true;
			}

			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {

				viewToExpand.getLayoutParams().height = interpolatedTime == 1
						? ViewGroup.LayoutParams.WRAP_CONTENT
						: (int) (targetHeight * interpolatedTime);
				viewToExpand.requestLayout();

			}

		};

		if (listener != null) animation.setAnimationListener(listener);
		animation.setDuration(targetHeight);
		viewToExpand.startAnimation(animation);
		return targetHeight;

	}

	/**
	 * Collapse the view with animation.
	 *
	 * @return duration needed for the collapse animation.
	 */
	public static int collapseView(final View viewToCollapse, final Animation.AnimationListener listener) {

		if (JavaOp.ifNullThenLog(viewToCollapse)) return -1;
		final int initialHeight = viewToCollapse.getMeasuredHeight();

		final Animation animation = new Animation() {

			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {

				if (interpolatedTime == 1) {
					viewToCollapse.setVisibility(View.GONE);
				} else {
					viewToCollapse.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
					viewToCollapse.requestLayout();
				}

			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}

		};

		if (listener != null) animation.setAnimationListener(listener);
		animation.setDuration(initialHeight);
		viewToCollapse.startAnimation(animation);
		return initialHeight;

	}

	/**
	 * Start gallery for result and allow user to choose an image.
	 */
	public static void startGettingImageFromGalleryForResult(final Activity activity) {
		if (JavaOp.ifNullThenLog(activity)) return;
		final Intent mIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		activity.startActivityForResult(mIntent, REQUEST_CODE_GALLERY);
	}

	/**
	 * Start gallery for result and allow user to choose an image.
	 */
	public static void startGettingImageFromGalleryForResult(final Fragment fragment) {
		if (JavaOp.ifNullThenLog(fragment)) return;
		final Intent mIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		fragment.startActivityForResult(mIntent, REQUEST_CODE_GALLERY);
	}

	/**
	 * Start gallery for result and allow user to choose an image.
	 */
	public static void startGettingImageFromGalleryForResult(final androidx.fragment.app.Fragment fragment) {
		if (JavaOp.ifNullThenLog(fragment)) return;
		final Intent mIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		fragment.startActivityForResult(mIntent, REQUEST_CODE_GALLERY);
	}

	/**
	 * For those activity started for result, return OK-result and finish.
	 */
	public static void setActivityResultAndFinish ( final Activity activity, final Intent intentReturn ) {
		if ( JavaOp.ifNullThenLog( activity ) ) return;
		activity.setResult( Activity.RESULT_OK, intentReturn );
		activity.finish();
	}

	//	/**
	//	 * Zoom in/out Google map camera so that all the markers can be seen just nicely.
	//	 *
	//	 * @param latLngs The location of the markers.
	//	 */
	//	public static void zoomGoogleMapCamera ( final GoogleMap googleMap, final boolean isAnimationNeeded, final LatLng... latLngs ) {
	//
	//		if ( googleMap == null ) {
	//			LogCustom.e( "Google map is null." );
	//			return;
	//		}
	//
	//		if ( JavaOp.ifNullThenLog( latLngs ) ) return;
	//
	//		boolean isLatDiffSmall = true;
	//		boolean isLngDiffSmall = true;
	//		double minLat = Double.MAX_VALUE, maxLat = -Double.MAX_VALUE, minLng = Double.MAX_VALUE, maxLng = -Double.MAX_VALUE;
	//
	//		if ( latLngs.length > 1 ) {
	//
	//			for ( LatLng latLng : latLngs ) {
	//				if ( latLng.latitude < minLat ) minLat = latLng.latitude;
	//				else if ( latLng.latitude > maxLat ) maxLat = latLng.latitude;
	//				if ( latLng.longitude < minLng ) minLng = latLng.longitude;
	//				else if ( latLng.longitude > maxLng ) maxLng = latLng.longitude;
	//			}
	//
	//			isLatDiffSmall = !JavaOp.isDiffLarge( minLat, maxLat, MIN_LOCATIONS_DISTANCE );
	//			isLngDiffSmall = !JavaOp.isDiffLarge( minLng, maxLng, MIN_LOCATIONS_DISTANCE );
	//
	//		} else {
	//			minLat = latLngs[ 0 ].latitude;
	//			maxLat = minLat;
	//			minLng = latLngs[ 0 ].longitude;
	//			maxLng = minLng;
	//		}
	//
	//		CameraUpdate cameraUpdate = null;
	//
	//		if ( isLatDiffSmall && isLngDiffSmall ) {
	//
	//			final double centerLat = ( minLat + maxLat ) / 2;
	//			final double centerlng = ( minLng + maxLng ) / 2;
	//			cameraUpdate = CameraUpdateFactory.newLatLngZoom( new LatLng( centerLat, centerlng ), 13 );
	//
	//		} else {
	//
	//			final LatLngBounds.Builder builder = new LatLngBounds.Builder();
	//
	//			for ( LatLng latLng : latLngs ) {
	//				builder.include( latLng );
	//			}
	//
	//			cameraUpdate = CameraUpdateFactory.newLatLngBounds( builder.build(), PADDING_ZOOMING_BOUND );
	//
	//		}
	//
	//		if ( isAnimationNeeded ) {
	//			googleMap.animateCamera( cameraUpdate );
	//		} else {
	//			googleMap.moveCamera( cameraUpdate );
	//		}
	//
	//	}

	static Intent startCameraForResult(final Context context, final File folder, String filename) {

		if (JavaOp.ifNullThenLog(context)) return null;
		File pathToStorePhoto = null;

		if (filename == null) {
			final String curTime = DateTimeOp.getStringNow(DateTimeOp.TimeZone.LOCAL);
			filename = curTime.replace(":", ".") + ".jpg";
		}

		if (folder != null) {

			createFolderIfNotExist(folder);
			pathToStorePhoto = new File(folder, filename);

			try {
				pathToStorePhoto.createNewFile();
			} catch (IOException e) {
				LogCustom.e(e);
				return null;
			}

		}

		final Intent mIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		Uri uriImage = null;

		if (pathToStorePhoto != null) {
			//(akmal) comment this code in android 7.0 not working smoothly
			//uriImage = Uri.fromFile( pathToStorePhoto );

			//(akmal) for make android 7.0 working smoothly

			if (Build.VERSION.SDK_INT >= 21) {

				// FileProvider required for Android 7.  Sending a file URI throws exception.
				uriImage = FileProvider.getUriForFile(context, "com.evfy.evfytracker.fileprovider", pathToStorePhoto);
			} else {
				// For older devices:
				// Samsung Galaxy Tab 7" 2 (Samsung GT-P3113 Android 4.2.2, API 17)
				// Samsung S3
				uriImage = Uri.fromFile(pathToStorePhoto);
			}


		} else {
			ContentValues values = new ContentValues();
			values.put( MediaStore.Images.Media.TITLE, filename );
			uriImage = context.getContentResolver().insert( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values );
		}

		mIntent.putExtra( MediaStore.EXTRA_OUTPUT, uriImage );
		return mIntent;

	}

	//	private static String getDirectionURL ( final LatLng latLngSource, final LatLng latLngDestination ) {
	//
	//		if ( latLngSource == null || latLngDestination == null ) {
	//			LogCustom.e( "LatLng(s) is null." );
	//			return null;
	//		}
	//
	//		// Origin of route
	//		final String str_origin = "origin=" + latLngSource.latitude + "," + latLngSource.longitude;
	//
	//		// Destination of route
	//		final String str_dest = "destination=" + latLngDestination.latitude + "," + latLngDestination.longitude;
	//
	//		// Sensor enabled
	//		final String sensor = "sensor=false";
	//
	//		// mode specified
	//		final String mode = "&mode=driving";
	//
	//		// Building the parameters to the web service
	//		final String parameters = str_origin + "&" + str_dest + "&" + sensor + mode;
	//
	//		// Output format
	//		final String output = "json";
	//		return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
	//
	//	}

	//	/**
	//	 * (Google Analytics) send timing.
	//	 *
	//	 * @param category Cannot be null.
	//	 * @param value    Cannot be null.
	//	 */
	//	public static void trackerSendTiming ( Context context, String category, long value, String name, String label ) {
	//
	//		if ( !JavaOp.hasValueForAll( context, category, value ) ) {
	//			LogCustom.e( "context, category, or value is null." );
	//			return;
	//		}
	//
	//		GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance( context );
	//		Tracker tracker = googleAnalytics.newTracker( MyConstants.ID_GOOGLE_ANALYTICS );
	//		HitBuilders.TimingBuilder timingBuilder = new HitBuilders.TimingBuilder().setCategory( category ).setValue( value );
	//		if ( JavaOp.hasValue( name ) ) timingBuilder.setVariable( name );
	//		if ( JavaOp.hasValue( label ) ) timingBuilder.setLabel( label );
	//		tracker.send( timingBuilder.build() );
	//
	//	}
	private static String getRoute(final String strUrl) {

		if (JavaOp.ifNullOrEmptyThenLog(strUrl)) return null;
		String data = "";
		InputStream iStream = null;
		HttpURLConnection urlConnection = null;

		try {

			final URL url = new URL(strUrl);

			// Creating an http connection to communicate with url
			urlConnection = (HttpURLConnection) url.openConnection();

			// Connecting to url
			urlConnection.connect();

			// Reading mData from url
			iStream = urlConnection.getInputStream();

			BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
			final StringBuffer sb = new StringBuffer();
			String line = "";
			while ((line = br.readLine()) != null) {
				sb.append(line );
			}

			data = sb.toString();
			br.close();

		} catch (Throwable e) {
			LogCustom.e(e);
		} finally {

			try {
				iStream.close();
			} catch (Throwable e) {
			}

			try {
				urlConnection.disconnect();
			} catch (Throwable e) {
			}

		}

		return data;

	}

	/**
	 * Fade in or fade out an ImageView.
	 *
	 * @param time             Time in milliseconds.
	 * @param toggleVisibility Whether to toggle the visibility of the ImageView after the fade.
	 */
	private static void fade(final View view, final float opacityStart, final float opacityEnd, int time, final boolean toggleVisibility) {

		view.setAlpha(opacityStart);

		if (toggleVisibility) {
			if (view.getVisibility() == View.GONE) view.setVisibility(View.VISIBLE);
			else view.setVisibility(View.GONE);
		}

		final Animation animation = new Animation() {

			@Override
			public boolean willChangeBounds() {
				return true;
			}

			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {

				if (interpolatedTime == 1) {

					view.setAlpha(opacityEnd);

					if (toggleVisibility) {
						if (view.getVisibility() == View.GONE) view.setVisibility(View.VISIBLE);
						else view.setVisibility(View.GONE);
					}

				} else {

					final float opacity = opacityStart + ( interpolatedTime * ( opacityEnd - opacityStart ) );
					view.setAlpha( opacity );
					view.requestLayout();

				}
			}
		};

		animation.setDuration( time );
		view.startAnimation( animation );

	}

}

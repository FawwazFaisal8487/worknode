package com.lkh012349s.mobileprinter.Utils;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Various date operation: format exchange, time zone exchange, etc.
 */
public class DateTimeOp {

    public final static String FORMAT_DATE_TIME_STANDARD = "yyyy-MM-dd'T'HH:mm:ss";
    public final static String FORMAT_DATE = "yyyy-MM-dd";
    public final static String FORMAT_TIME = "hh:mm a";
    public final static String FORMAT_DATE_TIME_DISPLAY = FORMAT_DATE + "  " + FORMAT_TIME;
    public final static String FORMAT_WEEK_DAY_MONTH = "EEE, dd MMM";
    public final static String FORMAT_WEEK_DAY_MONTH_TIME = "EEE, dd MMMM, hh:mm a";
    public final static String FORMAT_DATE_WEEK = "dd-MM-yyyy, EEE";

    private static final java.util.TimeZone TIMEZONE_UTC = java.util.TimeZone.getTimeZone("UTC");

    public enum TimeZone {UTC, LOCAL}

    /**
     * @param month January is 0, and so on.
     */
    public static String getString(final String format, final int year, final int month, final int day) {

        if (JavaOp.ifNullOrEmptyThenLog(format)) return null;
        final Calendar calendar = Calendar.getInstance();

        try {
            calendar.set(year, month, day);
        } catch (final Throwable e) {
            LogCustom.e(e);
            return null;
        }

        return getString(format, calendar.getTime(), TimeZone.LOCAL);

    }

    /**
     * @param month January is 0, and so on.
     * @param hour  Hour of day. For example, 10pm is 22.
     */
    public static String getString(final String format, final int year, final int month, final int day, final int hour, final int min) {

        if (JavaOp.ifNullOrEmptyThenLog(format)) return null;
        final Calendar calendar = Calendar.getInstance();

        try {
            calendar.set(year, month, day, hour, min);
        } catch (final Throwable e) {
            LogCustom.e(e);
            return null;
        }

        return getString(format, calendar.getTime(), TimeZone.LOCAL);

    }

    /**
     * @return Date of this month. the value of day will be 1, and the value for smaller unit will be 0.
     */
    public static Date getCurrentMonth() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Convert String date with format "yyyy-MM-dd'T'HH:mm:ss" into a Date object.
     *
     * @param timeZone Time zone of dateStr.
     */
    public static Date getDate(final String dateStr, final TimeZone timeZone) {
        return getDate(FORMAT_DATE_TIME_STANDARD, dateStr, timeZone);
    }

    /**
     * Convert String date with given format into a Date object.
     *
     * @param timeZone Time zone of dateStr.
     */
    public static Date getDate(final String format, final String dateStr, final TimeZone timeZone) {

        if (JavaOp.ifNullOrEmptyThenLog(format, dateStr)) return null;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        if (timeZone != null && timeZone == TimeZone.UTC)
            simpleDateFormat.setTimeZone(TIMEZONE_UTC);
        Date date = null;

        try {
            date = simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            LogCustom.e(e);
        }

        return date;

    }

    /**
     * @param month    January is 0, and so on.
     * @param hour     Hour of day. For example, 10pm is 22.
     * @param timeZone Time zone of the result date.
     */
    public static Date getDate(final int year, final int month, final int day, final int hour, final int min, final TimeZone timeZone) {
        return getDate(year, month, day, hour, min, 0, 0, timeZone);
    }

    /**
     * @param month    January is 0, and so on.
     * @param hour     Hour of day. For example, 10pm is 22.
     * @param timeZone Time zone of the result date.
     */
    public static Date getDate(final int year, final int month, final int day, final int hour, final int min, final int second, final int milliSecond,
                               final TimeZone timeZone) {
        if (JavaOp.ifNullThenLog(timeZone)) return null;
        final Calendar calendar = Calendar.getInstance();
        if (timeZone == TimeZone.UTC) calendar.setTimeZone(TIMEZONE_UTC);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DATE, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, second);
        calendar.set(Calendar.MILLISECOND, milliSecond);
        return calendar.getTime();
    }

    /**
     * @return Date of today, the value from hour to milli-second will be 0.
     */
    public static Date getDateToday() {
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * @return Date containing year, month and day only.
     */
    public static Date getExactDate(final Date date) {

        if (JavaOp.ifNullThenLog(date)) return null;

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();

    }

    /**
     * Get the given date time in given format.
     *
     * @param formatGivenStr    String format of the given date string.
     * @param formatResultStr   String format of the result date string.
     * @param dateStr           Date time string in the given date string format.
     * @param timeZoneGivenStr  Timezone of the given date string.
     * @param timeZoneResultStr Time zone of the result date string.
     * @return Given date time in given format.
     */
    public static String getString(final String formatGivenStr, final String formatResultStr, final String dateStr, final TimeZone timeZoneGivenStr,
                                   final TimeZone timeZoneResultStr) {

        if (JavaOp.ifNullOrEmptyThenLog(formatGivenStr, dateStr)) return null;
        final SimpleDateFormat simpleDateFormatResult = new SimpleDateFormat(formatResultStr);
        final SimpleDateFormat simpleDateFormatGiven = new SimpleDateFormat(formatGivenStr);
        if (timeZoneGivenStr != null && timeZoneGivenStr == TimeZone.UTC)
            simpleDateFormatResult.setTimeZone(TIMEZONE_UTC);
        if (timeZoneResultStr != null && timeZoneResultStr == TimeZone.UTC)
            simpleDateFormatGiven.setTimeZone(TIMEZONE_UTC);

        try {
            Date dateUTCDateTime = simpleDateFormatGiven.parse(dateStr);
            return simpleDateFormatResult.format(dateUTCDateTime);
        } catch (Throwable e) {
            LogCustom.e(e);
            return null;
        }

    }

    /**
     * Get the given date time in given format.
     *
     * @param formatGivenStr  String format of the given date string.
     * @param formatResultStr String format of the result date string.
     * @param dateStr         Date time string in the given date string format.
     * @return Given date time in given format.
     */
    public static String getString(final String formatGivenStr, final String formatResultStr, final String dateStr) {
        return getString(formatGivenStr, formatResultStr, dateStr, null, null);
    }

    /**
     * Get the given date time in given format.
     *
     * @param format  String format of the result date string.
     * @param dateStr Date time string in the format "yyyy-MM-dd'T'HH:mm:ss".
     * @return Given date time in given format.
     */
    public static String getString(final String format, final String dateStr) {
        return getString(format, FORMAT_DATE_TIME_STANDARD, dateStr, null, null);
    }

    /**
     * Get the given date time in given format.
     *
     * @param format            String format of the result date string.
     * @param dateStr           Date time string in the format "yyyy-MM-dd'T'HH:mm:ss".
     * @param timeZoneGivenStr  Timezone of the given date string.
     * @param timeZoneResultStr Time zone of the result date string.
     * @return Given date time in given format.
     */
    public static String getString(final String format, final String dateStr, final TimeZone timeZoneGivenStr, final TimeZone timeZoneResultStr) {
        return getString(format, FORMAT_DATE_TIME_STANDARD, dateStr, timeZoneGivenStr, timeZoneResultStr);
    }

    /**
     * Get current local datetime with specified format.
     *
     * @param timeZone Time zone of the result date string.
     */
    public static String getStringNow(final String format, final TimeZone timeZone) {

        if (JavaOp.ifNullOrEmptyThenLog(format, timeZone)) return null;
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (timeZone == TimeZone.UTC) sdf.setTimeZone(TIMEZONE_UTC);
        final long curTimeMillis = System.currentTimeMillis();

        try {
            return sdf.format(new Date(curTimeMillis));
        } catch (Exception e) {
            LogCustom.e(e);
            return null;
        }

    }

    /**
     * @param timeZone Time zone of the result date string.
     * @return Current date time in the format "yyyy-MM-dd'T'HH:mm:ss".
     */
    public static String getStringNow(final TimeZone timeZone) {
        return getStringNow(FORMAT_DATE_TIME_STANDARD, timeZone);
    }

    /**
     * @param timeZone Time zone of the result string.
     * @return Given date time in given format.
     */
    public static String getString(final String format, final Date date, final TimeZone timeZone) {

        if (JavaOp.ifNullOrEmptyThenLog(format, date)) return null;
        final SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (timeZone != null && timeZone == TimeZone.UTC) sdf.setTimeZone(TIMEZONE_UTC);

        try {
            return sdf.format(date);
        } catch (Exception e) {
            LogCustom.e(e);
            return null;
        }

    }

    /**
     * @param timeZone Time zone of the result string.
     * @return Given date time in format "yyyy-MM-dd'T'HH:mm:ss".
     */
    public static String getString(final Date date, final TimeZone timeZone) {
        return getString(FORMAT_DATE_TIME_STANDARD, date, timeZone);
    }

    /**
     * @return If the given date is later than today.
     */
    public static boolean isFutureDay(final Date date) {
        if (JavaOp.ifNullThenLog(date)) return false;
        final Date dateToday = new Date();
        if (date.before(dateToday)) return false;
        return !DateUtils.isToday(date.getTime());
    }

    /**
     * @return If the given date is earlier than today.
     */
    public static boolean isPastDay(final Date date) {
        if (JavaOp.ifNullThenLog(date)) return false;
        final Date dateToday = new Date();
        if (date.after(dateToday)) return false;
        return !DateUtils.isToday(date.getTime());
    }

    /**
     * @return If the given date is today.
     */
    public static boolean isToday(final Date date) {
        if (JavaOp.ifNullThenLog(date)) return false;
        return DateUtils.isToday(date.getTime());
    }

    public static boolean isSameDay(final Date date0, final Date date1) {

        if (JavaOp.ifNullThenLog(date0, date1)) return false;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE);

        try {
            return simpleDateFormat.format(date1).equals(simpleDateFormat.format(date0));
        } catch (Exception e) {
            LogCustom.e(e);
            return false;
        }

    }

    /**
     * @return true if date is within dateStart and dateEnd (both inclusive). Only year, month and day are checked.
     */
    public static boolean isWithinDays(final Date date, final Date dateStart, final Date dateEnd) {
        if (isSameDay(date, dateStart) || isSameDay(date, dateEnd)) return true;
        return date.after(dateStart) && date.before(dateEnd);
    }

}


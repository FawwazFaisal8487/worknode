package com.lkh012349s.mobileprinter.Utils;

import android.content.Context;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Custom Log class. Other than normal logging, it can be used to print variable's value easily.
 */
public class LogCustom {

    static final boolean IS_IN_DEVELOPMENT = true;
    static final String NAME_PROJECT = "com.logisfleet.taskme";

    private static final String TAG_FIXED = "AndroidRuntime";
    private static final String NAME_CLASS = "LogCustom";
    private static final String NAME_METHOD_LOG = ".LogCustom.";
    private static final String PATTERN_NULL_0 = "com.lkh012349s.op.JavaOp.if";
    private static final String PATTERN_NULL_1 = "com.lkh012349s.op.JavaOp.is";
    private static final String PATTERN_NULL_2 = "Null";
    private static final String MSG_CALLING = "This method has been called.";
    private static final String PATTERN_METHOD = "[^\\.]*\\(.*";
    private static final int INDEX_METHOD = 5;
    private static final int MAX_TAG_LEN = 66;

    private static long lastTime = 0;

    /**
     * Append log to file "sdcard/log-filename.txt".
     *
     * @param isAppendMode Assign true if you want to append to the log; Assign false if you want to remove the
     *                     previous log.
     */
    public static void appendLog(final String filename, final boolean isAppendMode, final String... texts) {

        if (!IS_IN_DEVELOPMENT) return;
        final File logFile = new File("sdcard/log-" + filename + ".txt");

        try {

            if (!logFile.exists()) logFile.createNewFile();
            final BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, isAppendMode));
            String finalText = DateTimeOp.getStringNow(DateTimeOp.FORMAT_DATE_TIME_DISPLAY, DateTimeOp.TimeZone.LOCAL) + "\t";
            final String separator = " --- ";
            for (String text : texts)
                finalText += separator + text;

            if (isAppendMode) {
                buf.append(finalText + "\n");
                buf.newLine();
            } else {
                buf.write(finalText);
            }

            buf.close();

        } catch (Throwable e) {
            e(e);
        }

    }

    /**
     * Log (error) an error message.
     */
    public static void e(final Throwable e) {
        if (!IS_IN_DEVELOPMENT) return;
        final String tag = generateTag();
        Log.e(tag, getErrorMsg(e));
        logStackTrace(e);
    }

    /**
     * Get the name of the method that calls one of the methods in this class.
     */
    public static String getMethod(final String thisMethod, final int index_method) {

        final StackTraceElement[] stacktrace = Thread.currentThread().getStackTrace();

        for (int i = index_method; i < stacktrace.length; i++) {
            String method = stacktrace[i].toString();
            if (method.contains(thisMethod)) continue;
            method = StringOp.extractString(method, PATTERN_METHOD);
            return method;
        }

        Log.e(TAG_FIXED + " " + "Log", "Could not find correct method name.");
        return "";

    }

    /**
     * Used to check (debug) if the method calling this gets called.	 *
     */
    public static void i() {
        i(MSG_CALLING);
    }

    /**
     * Log (info) a message.
     */
    public static void i(String msg) {
        if (!IS_IN_DEVELOPMENT) return;
        msg = msg.replace("(", "{").replace(")", "}");
        Log.i(generateTag(), msg);
    }

    /**
     * Log (info) a variable's value.
     */
    public static void i(final Object field, final String fieldName) {
        if (JavaOp.ifNullThenLog(fieldName)) return;
        i(fieldName + ": " + field);
    }

    /**
     * Log (info) values.
     */
    public static void i(final String name, final Object... fields) {

        if (JavaOp.ifArrayNullOrEmptyThenLog(fields) || JavaOp.ifNullThenLog(name)) return;
        String msg = "";

        for (final Object field : fields) {
            msg += ", " + (field == null ? "null" : String.valueOf(field));
        }

        i(msg.substring(1), name);

    }

    /**
     * Log (debug) a variable's value.
     */
    public static void i(final String field, final String fieldName) {
        i(fieldName + ": " + field);
    }

    /**
     * Log the time diffence between current call of this method and previous call of this method.
     */
    public static void logTimeEnd(final String stage) {
        logTimeEnd(stage, null);
    }

    /**
     * Log the time diffence between current call of this method and previous call of this method and send to Google Analytics.
     */
    public static void logTimeEnd(final Context context) {
        logTimeEnd("", context);
    }

    /**
     * Log the time diffence between current call of this method and previous call of this method and send to Google Analytics.
     */
    public static void logTimeEnd(String stage, final Context context) {

        final long cur = System.currentTimeMillis();
        final long diff = cur - lastTime;
        lastTime = cur;
        if (!IS_IN_DEVELOPMENT) return;
        if (stage.length() != 0) stage = getMethod(".logTimeEnd", 4) + ": " + stage;
        else stage = getMethod(".logTimeEnd", 4);
        if (diff > 999) e(String.format("%5d", diff) + "   " + stage);
        else if (diff > 444) w(String.format("%5d", diff) + "   " + stage);
        else i(String.format("%5d", diff) + "   " + stage);

        //		if (context != null) {
        //			EasyTracker tracker = EasyTracker.getInstance(context);
        //			tracker.send(MapBuilder.createTiming(stage, diff, "", null).build());
        //		}

    }

    /**
     * Log the time diffence between current call of this method and previous call of this method. *
     */
    public static void logTimeEnd() {
        logTimeEnd("", null);
    }

    public static void logTimeStart() {
        lastTime = System.currentTimeMillis();
    }

    /**
     * Send an error message to Crashlytics.
     */
    public static void sendErrorToCrashlytics(final Throwable e) {

        if (IS_IN_DEVELOPMENT) {

            e(e);

        } else {

            //			try {
            //				Crashlytics.logException( new Throwable( generateTag() + ": " + getErrorMsg( e ) ) );
            //			} finally {}

        }

    }

    /**
     * Send an error message to Crashlytics.
     */
    public static void sendErrorToCrashlytics(final Object value, final String fieldName, final String msg) {
        sendErrorToCrashlytics(msg + " \n" + fieldName + ": " + value);
    }

    /**
     * Send an error message to Crashlytics.
     *
     * @param msg
     */
    public static void sendErrorToCrashlytics(final String msg) {

        if (IS_IN_DEVELOPMENT) {

            e(msg);

        } else {

            //			try {
            //				Crashlytics.logException( new Throwable( generateTag() + ": " + msg ) );
            //			} finally {}

        }

    }

    /**
     * Log (error) an error message.
     *
     * @param msg
     */
    public static void e(String msg) {
        if (!IS_IN_DEVELOPMENT) return;
        if (JavaOp.ifNullOrEmptyThenLog(msg)) return;
        final String tag = generateTag();
        msg = msg.replace("(", "{").replace(")", "}");
        Log.e(tag, msg);
        logStackTrace();
    }

    /**
     * Send an error message to Crashlytics.
     */
    public static void sendErrorToCrashlytics(final String value, final String fieldName, final String msg) {
        sendErrorToCrashlytics(msg + " \n" + fieldName + ": " + value);
    }

    /**
     * Send an error message to Crashlytics.
     */
    public static void sendErrorToCrashlytics(final Object value, final String fieldName, final Throwable e) {
        sendErrorToCrashlytics(getErrorMsg(e) + " \n" + fieldName + ": " + value);
    }

    /**
     * Send an error message to Crashlytics.
     */
    public static void sendErrorToCrashlytics(final String value, final String fieldName, final Throwable e) {
        sendErrorToCrashlytics(getErrorMsg(e) + " \n" + fieldName + ": " + value);
    }

    public static void w(String msg) {
        if (!IS_IN_DEVELOPMENT) return;
        msg = msg.replace("(", "{").replace(")", "}");
        final String tag = generateTag();
        Log.w(tag, msg);
    }

    /**
     * Generate tag with format "app_name: FIXED_TAG: method_name_0.method_name_1.........method_name_n".
     */
    private static String generateTag() {
        String method = getMethod(NAME_METHOD_LOG, INDEX_METHOD);
        if (method.length() > MAX_TAG_LEN)
            method = "..." + method.substring(method.length() - MAX_TAG_LEN);
        return TAG_FIXED + ": " + method;
    }

    /**
     * Get error message from Throwable.
     *
     * @param e
     * @return Error message. If the Throwable is a NullPointerException, return specified message.
     */
    private static String getErrorMsg(final Throwable e) {
        return e.getMessage() == null ? e.getClass().getSimpleName() : e.getMessage();
    }

    /**
     * Print stack trace of exception.
     */
    private static void logStackTrace(final Throwable e) {

        final StackTraceElement[] eles = e.getStackTrace();
        boolean isCheckingNeeded = false;

        for (final StackTraceElement ele : eles) {
            final String method = ele.toString();
            if (method.contains(NAME_PROJECT)) isCheckingNeeded = true;
            if (isCheckingNeeded && isNotUsefulMethod(method)) return;
            Log.e(TAG_FIXED, "at " + method);
        }

    }

    /**
     * Print stack trace of the method calling this class.
     */
    private static void logStackTrace() {

        StackTraceElement[] eles = Thread.currentThread().getStackTrace();

        for (final StackTraceElement ele : eles) {
            final String method = ele.toString();
            if (isNotUsefulMethod(method)) continue;
            Log.e(TAG_FIXED, "at " + method);
        }

    }

    /**
     * @return True if the method is not desirable to be seen in LogCat.
     */
    static boolean isNotUsefulMethod(final String method) {

        if (method == null) {
            Log.e(String.format("%s %s", TAG_FIXED, NAME_CLASS), "method is null");
            return false;
        }

        //		i( method, "method" );

        return method.contains(NAME_METHOD_LOG) || (!method.contains(NAME_PROJECT) && (!method.contains("lkh012349s")) && (!method.contains(
                "com.evfy.evfytracker.Utils"))) || ((method.contains(PATTERN_NULL_0) || method.contains(PATTERN_NULL_1)) && method.contains(PATTERN_NULL_2));

    }

}